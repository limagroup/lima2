#include <pybind11/pybind11.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace py = pybind11;

/// This is a minimalist binding of Boost.PropertyTree
PYBIND11_MODULE(ptree, m)
{
    m.doc() = "Boost.PropertyTree binding"; // optional module docstring

    using ptree = boost::property_tree::ptree;

    py::class_<ptree::path_type>(m, "PathType").def(py::init<std::string>());

    py::class_<ptree>(m, "Ptree")
        .def(py::init<>())
        .def("size", &ptree::size, "The number of direct children of this node.")
        .def("empty", &ptree::empty, "Whether there are any direct children.")
        .def("setProp", &ptree::put<int>,
             "Set the value of the node at the given path to the supplied value, translated to the tree's data type. "
             "If the node doesn't exist, it is created, including all its missing parents.")
        .def("setProp", &ptree::put<double>,
             "Set the value of the node at the given path to the supplied value, translated to the tree's data type. "
             "If the node doesn't exist, it is created, including all its missing parents.")
        .def("setProp", &ptree::put<std::string>,
             "Set the value of the node at the given path to the supplied value, translated to the tree's data type. "
             "If the node doesn't exist, it is created, including all its missing parents.")
        .def("getPropInt", py::overload_cast<const ptree::path_type&>(&ptree::get<int>, py::const_),
             "Get the value of the node at the given path to the supplied value, translated to the tree's data type.")
        .def("getPropDouble", py::overload_cast<const ptree::path_type&>(&ptree::get<double>, py::const_),
             "Get the value of the node at the given path to the supplied value, translated to the tree's data type.")
        .def("getPropString", py::overload_cast<const ptree::path_type&>(&ptree::get<std::string>, py::const_),
             "Get the value of the node at the given path to the supplied value, translated to the tree's data type.");

    py::implicitly_convertible<std::string, ptree::path_type>();
}
