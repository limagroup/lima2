# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

pybind11_add_module(py_ptree ptree.cpp)

target_link_libraries(py_ptree PRIVATE Boost::boost)

target_precompile_headers(py_ptree
    REUSE_FROM lima_core)

set_target_properties(py_ptree PROPERTIES LIBRARY_OUTPUT_NAME "ptree")

install(TARGETS py_ptree
  LIBRARY DESTINATION ${Python_SITEARCH}
  RUNTIME DESTINATION ${Python_SITEARCH}
)
