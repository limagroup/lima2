import ptree
import gil
import lima

import msgpack
import msgpack_lima as m
m.patch()

import zerorpc

cam = zerorpc.Client()
cam.connect("tcp://127.0.0.1:4242")

cam.setProp("shutter.expo_time", 100000);
cam.setProp("specific.latency_time", 100000);

# Prepare camera object
cam.prepareAcq()
cam.startAcq()

while (cam.getAcqStatus() == lima.Simulator.AcquisitionStatus.AcqRunning):
    print("Acquisition running")
    cam.wait()
