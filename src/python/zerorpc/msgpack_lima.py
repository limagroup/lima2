#!/usr/bin/env python

"""
Support for serialization of lima data types with msgpack.
"""

# Copyright (c) 2013-2017, Lev Givon
# All rights reserved.
# Distributed under the terms of the BSD license:
# http://www.opensource.org/licenses/bsd-license

import os
import sys
import functools

import ptree
import lima
import msgpack


# Fall back to pure Python
if os.environ.get('MSGPACK_PUREPYTHON'):
    import msgpack.fallback as _packer
    import msgpack.fallback as _unpacker
else:
    try:
        import msgpack._packer as _packer
        import msgpack._unpacker as _unpacker
    except:
        import msgpack.fallback as _packer
        import msgpack.fallback as _unpacker

def encode(obj, chain=None):
    """
    Data encoder for serializing lima data types.
    """

    if isinstance(obj, lima.Simulator.AcquisitionStatus):
        return {b'acqstat': True,
                b'val': obj.name}
    #elif isinstance(obj, complex):
    #    return {b'complex': True,
    #            b'data': obj.__repr__()}
    else:
        return obj if chain is None else chain(obj)

def tostr(x):
    if sys.version_info >= (3, 0):
        if isinstance(x, bytes):
            return x.decode()
        else:
            return str(x)
    else:
        return x

def decode(obj, chain=None):
    """
    Decoder for deserializing lima data types.
    """
    
    try:
        if b'acqstat' in obj:
            return getattr(lima.Simulator.AcquisitionStatus, tostr(obj[b'val']))
            
        else:
            return obj if chain is None else chain(obj)
    except KeyError:
        return obj if chain is None else chain(obj)

# Maintain support for msgpack < 0.4.0:
if msgpack.version < (0, 4, 0):
    class Packer(_packer.Packer):
        def __init__(self, default=None,
                     encoding='utf-8',
                     unicode_errors='strict',
                     use_single_float=False,
                     autoreset=1):
            default = functools.partial(encode, chain=default)
            super(Packer, self).__init__(default=default,
                                         encoding=encoding,
                                         unicode_errors=unicode_errors,
                                         use_single_float=use_single_float,
                                         autoreset=autoreset)
    class Unpacker(_unpacker.Unpacker):
        def __init__(self, file_like=None, read_size=0, use_list=None,
                     object_hook=None,
                     object_pairs_hook=None, list_hook=None, encoding='utf-8',
                     unicode_errors='strict', max_buffer_size=0):
            object_hook = functools.partial(decode, chain=object_hook)
            super(Unpacker, self).__init__(file_like=file_like,
                                           read_size=read_size,
                                           use_list=use_list,
                                           object_hook=object_hook,
                                           object_pairs_hook=object_pairs_hook,
                                           list_hook=list_hook,
                                           encoding=encoding,
                                           unicode_errors=unicode_errors,
                                           max_buffer_size=max_buffer_size)

else:
    class Packer(_packer.Packer):
        def __init__(self, default=None,
                     encoding='utf-8',
                     unicode_errors='strict',
                     use_single_float=False,
                     autoreset=1,
                     use_bin_type=0):
            default = functools.partial(encode, chain=default)
            super(Packer, self).__init__(default=default,
                                         #encoding=encoding,
                                         unicode_errors=unicode_errors,
                                         use_single_float=use_single_float,
                                         autoreset=autoreset,
                                         use_bin_type=use_bin_type)

    class Unpacker(_unpacker.Unpacker):
        def __init__(self, file_like=None, read_size=0, use_list=None,
                     object_hook=None,
                     object_pairs_hook=None, list_hook=None, encoding=None,
                     unicode_errors='strict', max_buffer_size=0,
                     ext_hook=msgpack.ExtType):
            object_hook = functools.partial(decode, chain=object_hook)
            super(Unpacker, self).__init__(file_like=file_like,
                                           read_size=read_size,
                                           use_list=use_list,
                                           object_hook=object_hook,
                                           object_pairs_hook=object_pairs_hook,
                                           list_hook=list_hook,
                                           encoding=encoding,
                                           unicode_errors=unicode_errors,
                                           max_buffer_size=max_buffer_size,
                                           ext_hook=ext_hook)

def pack(o, stream, **kwargs):
    """
    Pack an object and write it to a stream.
    """

    packer = Packer(**kwargs)
    stream.write(packer.pack(o))

def packb(o, **kwargs):
    """
    Pack an object and return the packed bytes.
    """

    return Packer(**kwargs).pack(o)

def unpack(stream, **kwargs):
    """
    Unpack a packed object from a stream.
    """

    object_hook = kwargs.get('object_hook')
    kwargs['object_hook'] = functools.partial(decode, chain=object_hook)
    return _unpacker.unpack(stream, **kwargs)

def unpackb(packed, **kwargs):
    """
    Unpack a packed object.
    """

    object_hook = kwargs.get('object_hook')
    kwargs['object_hook'] = functools.partial(decode, chain=object_hook)
    return _unpacker.unpackb(packed, **kwargs)

load = unpack
loads = unpackb
dump = pack
dumps = packb

def patch():
    """
    Monkey patch msgpack module to enable support for serializing lima types.
    """

    setattr(msgpack, 'Packer', Packer)
    setattr(msgpack, 'Unpacker', Unpacker)
    setattr(msgpack, 'load', unpack)
    setattr(msgpack, 'loads', unpackb)
    setattr(msgpack, 'dump', pack)
    setattr(msgpack, 'dumps', packb)
    setattr(msgpack, 'pack', pack)
    setattr(msgpack, 'packb', packb)
    setattr(msgpack, 'unpack', unpack)
    setattr(msgpack, 'unpackb', unpackb)

if __name__ == '__main__':
    try:
        range = xrange # Python 2
    except NameError:
        pass # Python 3

    from unittest import main, TestCase, TestSuite

    class ThirdParty(object):

        def __init__(self, foo='bar'):
            self.foo = foo

        def __eq__(self, other):
            return isinstance(other, ThirdParty) and self.foo == other.foo


    class test_lima_msgpack(TestCase):
        def setUp(self):
             patch()
             
        def encode_decode(self, x, use_bin_type=False, encoding=None):
            x_enc = msgpack.packb(x, use_bin_type=use_bin_type)
            return msgpack.unpackb(x_enc, encoding=encoding)

        def encode_thirdparty(self, obj):
            return dict(__thirdparty__=True, foo=obj.foo)

        def decode_thirdparty(self, obj):
            if b'__thirdparty__' in obj:
                return ThirdParty(foo=obj['foo'])
            return obj

        def encode_decode_thirdparty(self, x, use_bin_type=False, encoding=None):
            x_enc = msgpack.packb(x, default=self.encode_thirdparty,
                                  use_bin_type=use_bin_type)
            return msgpack.unpackb(x_enc, object_hook=self.decode_thirdparty,
                                   encoding=encoding)

        def test_bin(self):
            # Since bytes == str in Python 2.7, the following
            # should pass on both 2.7 and 3.*
            self.assertEqual(type(self.encode_decode(b'foo')), bytes)

        def test_str(self):
            self.assertEqual(type(self.encode_decode('foo')), bytes)
            if sys.version_info.major == 2:
                self.assertEqual(type(self.encode_decode(u'foo')), str)

                # Test non-default string encoding/decoding:
                self.assertEqual(type(self.encode_decode(u'foo', True, 'utf=8')), unicode)

        def test_lima_acq_status(self):
            x = lima.Simulator.AcqReady
            x_rec = self.encode_decode(x)
            self.assertEqual(x, x_rec)
            self.assertEqual(type(x), type(x_rec))
            x = lima.Simulator.AcqRunning
            x_rec = self.encode_decode(x)
            self.assertEqual(x, x_rec)
            self.assertEqual(type(x), type(x_rec))

    main()
