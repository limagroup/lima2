import numpy as np

import ptree
import gil
import lima

import msgpack
import msgpack_lima as m
m.patch()

import zerorpc

init_config = ptree.Ptree()
init_config.setProp("name", "My Funky Simulator")

name = init_config.getPropString(ptree.PathType("name"))

# Create camera object
cam = lima.Simulator(init_config);

s = zerorpc.Server(cam)
s.bind("tcp://0.0.0.0:4242")
s.run()
