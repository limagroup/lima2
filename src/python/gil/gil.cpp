// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>

#include <pybind11/pybind11.h>

#include <boost/gil.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
#include <boost/mp11.hpp>

#include <fmt/format.h>

namespace py = pybind11;

//namespace pybind11 {
//	namespace detail {
//
//template <typename T>
//struct is_pixel;
//
//template <typename ChannelValue, typename Layout>
//struct is_pixel<boost::gil::pixel<ChannelValue, Layout>> : std::true_type {};
//
//// This is unfortunate but we need a specialiation of is_copy_constructible for pixel types that look like containers but are not really
//// error: is_copy_constructible<boost::gil::pixel<uint8_t,boost::gil::gray_layout_t>,void>': a class cannot be its own base class
//template <typename ChannelValue, typename Layout>
//struct is_copy_constructible<boost::gil::pixel<ChannelValue, Layout>, void> : std::is_copy_constructible<ChannelValue> {};
//
//	}
//}

template <typename Image>
void py_image(py::module& m, const char* name)
{
    using view_t = typename Image::view_t;
    using pixel_type = typename view_t::value_type;
    using x_coord_t = typename view_t::x_coord_t;
    using y_coord_t = typename view_t::y_coord_t;

    py::class_<Image>(m, name, py::buffer_protocol())
        .def(py::init<x_coord_t, y_coord_t, std::size_t>())
        .def(py::init<x_coord_t, y_coord_t, const pixel_type&, std::size_t>())
        .def("recreate", (void(Image::*)(x_coord_t, y_coord_t, std::size_t)) & Image::recreate,
             "Recreate an image with the given dimensions.")
        .def("recreate", (void(Image::*)(x_coord_t, y_coord_t, const pixel_type&, std::size_t)) & Image::recreate,
             "Recreate an image with the given dimensions.")
        .def("dimensions", &Image::dimensions, "The dimensions of an image.")
        .def("width", &Image::width, "The width of an image.")
        .def("height", &Image::height, "The height of an image.")
        .def("__repr__",
             [](const Image& img) { return fmt::format("<GIL.Image '{}, {}'>", img.width(), img.height()); });
}

template <typename View>
void py_image_view(py::module& m, const char* name)
{
    using pixel_type = typename View::value_type;
    using channel_type = typename std::decay_t<typename boost::gil::channel_type<pixel_type>::type>;

    py::class_<View>(m, name, py::buffer_protocol())
        .def(py::init<>())
        .def("dimensions", &View::dimensions, "The dimensions of an image.")
        .def("width", &View::width, "The width of an image.")
        .def("height", &View::height, "The height of an image.")
        .def_buffer([](const View& m) -> py::buffer_info {
            return py::buffer_info(
                &m[0],                                         // Pointer to buffer
                sizeof(channel_type),                          // Size of one scalar
                py::format_descriptor<channel_type>::format(), // Python struct-style format descriptor
                View::num_dimensions,                          // Number of dimensions
                {m.width(), m.height()},                       // Buffer dimensions
                {sizeof(channel_type) * m.width(),             // Strides (in bytes) for each index
                 sizeof(channel_type)});
        })
        .def("__repr__", [](const View& v) { return fmt::format("<GIL.ImageView '{}, {}'>", v.width(), v.height()); });
}

template <typename Pixel>
void py_pixel(py::module& m, const char* name)
{
    using channel_type = typename boost::gil::channel_type<Pixel>::type;

    py::class_<Pixel>(m, name).def(py::init<channel_type const&>());
}

struct buffer_info_obj
{
    typedef py::buffer_info result_type;

    template <typename View>
    result_type operator()(const View& m) const
    {
        using pixel_type = typename View::value_type;
        using channel_type = typename boost::gil::channel_type<pixel_type>::type;

        return py::buffer_info((void*) &m[0],                                 // Pointer to buffer
                               sizeof(channel_type),                          // Size of one scalar
                               py::format_descriptor<channel_type>::format(), // Python struct-style format descriptor
                               View::num_dimensions,                          // Number of dimensions
                               {m.width(), m.height()},                       // Buffer dimensions
                               {sizeof(channel_type) * m.width(),             // Strides (in bytes) for each index
                                sizeof(channel_type)});
    }
};

template <typename View>
void print(View const& v)
{
    boost::gil::for_each_pixel(v, [](auto const& p) { std::cout << std::dec << p << ", "; });
}

template <typename View, typename Pixel>
void fill(View const& v, Pixel const& val)
{
    boost::gil::fill_pixels(v, [&val](auto& p) { p = val; });
}

/// This is a minimalist binding of Boost.GIL
PYBIND11_MODULE(gil, m)
{
    m.doc() = "Boost.GIL binding"; // optional module docstring

    // Add point
    using point_t = boost::gil::point<std::ptrdiff_t>;
    py::class_<point_t>(m, "Point")
        .def(py::init<>())
        .def_readwrite("x", &point_t::x, "The pixel coordinate in the width dimension.")
        .def_readwrite("y", &point_t::y, "The pixel coordinate in the height dimension.")
        .def("__repr__", [](const point_t& p) { return fmt::format("<GIL.Point '{}, {}'>", p.x, p.y); });

    py_image<boost::gil::gray8_image_t>(m, "Gray8Image");
    py_image<boost::gil::gray16_image_t>(m, "Gray16Image");
    py_image<boost::gil::gray32_image_t>(m, "Gray32Image");

    py_image_view<boost::gil::gray8_view_t>(m, "Gray8ImageView");
    py_image_view<boost::gil::gray16_view_t>(m, "Gray16ImageView");
    py_image_view<boost::gil::gray32_view_t>(m, "Gray32ImageView");

    py_pixel<boost::gil::gray8_pixel_t>(m, "Gray8Pixel");
    py_pixel<boost::gil::gray16_pixel_t>(m, "Gray16Pixel");
    py_pixel<boost::gil::gray32_pixel_t>(m, "Gray32Pixel");

    py::implicitly_convertible<py::int_, boost::gil::gray8_pixel_t>();
    py::implicitly_convertible<py::int_, boost::gil::gray16_pixel_t>();
    py::implicitly_convertible<py::int_, boost::gil::gray32_pixel_t>();

    m.def("view", &boost::gil::view<boost::gil::gray8_pixel_t, false, std::allocator<unsigned char>>,
          "Returns the non-constant-pixel view of an image");
    m.def("view", &boost::gil::view<boost::gil::gray16_pixel_t, false, std::allocator<unsigned char>>,
          "Returns the non-constant-pixel view of an image");
    m.def("view", &boost::gil::view<boost::gil::gray32_pixel_t, false, std::allocator<unsigned char>>,
          "Returns the non-constant-pixel view of an image");

    // Print from the C++ side for debugging
    m.def("print", &print<boost::gil::gray8_view_t>, "Print pixel values");
    m.def("print", &print<boost::gil::gray16_view_t>, "Print pixel values");
    m.def("print", &print<boost::gil::gray32_view_t>, "Print pixel values");

    // STL-like Algorithms on GIL.View
    m.def("fill_pixels", &boost::gil::fill_pixels<boost::gil::gray8_view_t, boost::gil::gray8_pixel_t>,
          "Fill with the given pixel value");
    m.def("fill_pixels", &boost::gil::fill_pixels<boost::gil::gray16_view_t, boost::gil::gray16_pixel_t>,
          "Fill with the given pixel value");
    m.def("fill_pixels", &boost::gil::fill_pixels<boost::gil::gray32_view_t, boost::gil::gray32_pixel_t>,
          "Fill with the given pixel value");

    using any_image_view_t =
        boost::gil::any_image_view<boost::gil::gray8_view_t, boost::gil::gray16_view_t, boost::gil::gray32_view_t>;

    // Add any_view
    py::class_<any_image_view_t>(m, "AnyImageView", py::buffer_protocol())
        .def(py::init<boost::gil::gray8_view_t const&>())
        .def(py::init<boost::gil::gray16_view_t const&>())
        .def(py::init<boost::gil::gray32_view_t const&>())
        .def("dimensions", &any_image_view_t::dimensions, "The dimensions of an image.")
        .def("width", &any_image_view_t::width, "The width of an image.")
        .def("height", &any_image_view_t::height, "The height of an image.")
        .def_buffer(
            [](const any_image_view_t& m) -> py::buffer_info { return boost::variant2::visit(buffer_info_obj(), m); })
        .def("__repr__", [](const any_image_view_t& v) {
            return fmt::format("<GIL.AnyImageView '{}, {}'>", v.width(), v.height());
        });
}
