// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include <pybind11/chrono.h> // For metadata timestamp
#include <pybind11/stl.h>    // For attributes

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/filesystem.hpp>
#include <fmt/ranges.h>
#include <fmt/describe.hpp>

#include <boost/mp11.hpp>

#include <lima/core/enums.describe.hpp>
#include <lima/core/frame.hpp>
#include <lima/core/frame_info.hpp>
#include <lima/core/point.describe.hpp>
#include <lima/core/rectangle.describe.hpp>

#include <lima/hw/info.describe.hpp>
#include <lima/hw/capabilities.describe.hpp>
#include <lima/hw/params.describe.hpp>

#include <lima/io/params.describe.hpp>

#include <lima/utils/type_name.hpp>
#include <lima/utils/variant_algo.hpp>

namespace gil = boost::gil;
namespace py = pybind11;
namespace describe = boost::describe;

using namespace lima;

/// Returns the sizeof of a pixel in byte
inline std::string format_descriptor(pixel_enum p)
{
    std::string res;

    switch (p) {
    case pixel_enum::gray8:
        res = py::format_descriptor<bpp8_pixel_t>::format();
        break;
    case pixel_enum::gray8s:
        res = py::format_descriptor<bpp8s_pixel_t>::format();
        break;
    case pixel_enum::gray16:
        res = py::format_descriptor<bpp16_pixel_t>::format();
        break;
    case pixel_enum::gray16s:
        res = py::format_descriptor<bpp16s_pixel_t>::format();
        break;
    case pixel_enum::gray32:
        res = py::format_descriptor<bpp32_pixel_t>::format();
        break;
    case pixel_enum::gray32s:
        res = py::format_descriptor<bpp32s_pixel_t>::format();
        break;
    case pixel_enum::gray32f:
        res = py::format_descriptor<bpp32f_pixel_t>::format();
        break;
    case pixel_enum::gray64f:
        res = py::format_descriptor<bpp64f_pixel_t>::format();
        break;
    default:
        assert(false);
    }

    return res;
}

PYBIND11_MODULE(lima, m)
{
    using namespace pybind11::literals;

    m.doc() = "Lima binding"; // optional module docstring

    using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

    auto described_class = [](auto& class_) {
        using T = typename std::decay_t<decltype(class_)>::type;
        static_assert(boost::describe::has_describe_members<T>::value);

        // Constructor
        class_.def(py::init<>());

        // Member accessors
        boost::mp11::mp_for_each<describe::describe_members<T, describe::mod_any_access | describe::mod_inherited>>(
            [&class_](auto D) {
                using A = boost::describe::annotate_member<decltype(D)>;
                auto doc = boost::describe::annotation_by_name_v<A, Doc>;
                class_.def_readwrite(D.name, D.pointer);
            });

        // Repr
        class_.def("__repr__", [&class_](T const& t) { return fmt::format("{}", t); });
    };

    // Automatic binding for described enums
    auto described_enum = [](auto& enum_) {
        using E = typename std::decay_t<decltype(enum_)>::type;
        static_assert(boost::describe::has_describe_enumerators<E>::value);

        boost::mp11::mp_for_each<boost::describe::describe_enumerators<E>>(
            [&](auto D) { enum_.value(D.name, D.value); });
        enum_.export_values();
    };

    // Define pixel enumeration
    py::enum_<pixel_enum> pixel(m, "Pixel");
    described_enum(pixel);

    // Define metadata
    py::class_<frame::metadata_t>(m, "Metadata")
        .def_readwrite("idx", &frame::metadata_t::idx, "Index in the frame sequence (starting from 0).")
        .def_readwrite("recv_idx", &frame::metadata_t::recv_idx,
                       "Index in the receiver sequence (starting from 0 and continuous).")
        .def_readwrite("timestamp", &frame::metadata_t::timestamp, "Acquisition timestamp from camera backend.")
        .def_readwrite("is_final", &frame::metadata_t::is_final, "True if the last frame in the sequence.")
        .def_readwrite("is_valid", &frame::metadata_t::is_valid, "True if the frame is valid (properly transfered).")
        .def("__repr__", [](const frame::metadata_t& meta) { return fmt::format("<Lima.Metadata #{}>", meta.idx); });

    // Define frame
    py::class_<frame>(m, "Frame", py::buffer_protocol())
        .def(py::init([](frame::coord_t width, frame::coord_t height, std::size_t nb_channels, pixel_enum pixel) {
                 // Construct the frame according to the pixel type
                 return frame(width, height, nb_channels, pixel);
             }),
             "width"_a, "height"_a, "nb_channels"_a, "pixel"_a)
        .def(py::init([](frame::coord_t width, frame::coord_t height, std::size_t nb_channels, pixel_enum pixel,
                         std::size_t alignment) {
                 // Construct the frame according to the pixel type
                 return frame(width, height, nb_channels, pixel, alignment);
             }),
             "width"_a, "height"_a, "nb_channels"_a, "pixel"_a, "alignment"_a)
        .def_property_readonly("dimensions", &frame::dimensions, "The dimensions of a frame.")
        .def_property_readonly("width", &frame::width, "The width of a frame.")
        .def_property_readonly("height", &frame::height, "The height of a frame.")
        .def_property_readonly("pixel_type", &frame::pixel_type, "The type of pixel.")
        .def_property_readonly("nb_channels", &frame::nb_channels, "The number of channels.")
        .def_readwrite("metadata", &frame::metadata, "The metadata associated with a frame.")
        .def_readwrite("attributes", &frame::attributes, "The attributes associated with a frame.")
        .def_buffer([](const frame& frm) -> py::buffer_info {
            py::ssize_t ndim = frame::num_dimensions;
            py::ssize_t itemsize = sizeof_pixel(frm.pixel_type());
            py::ssize_t rowsize = frm.row_size_in_bytes();
            py::ssize_t channelsize = frm.channel_size_in_bytes();
            py::ssize_t nb_channels = frm.nb_channels();
            py::ssize_t height = frm.height();
            py::ssize_t width = frm.width();

            return py::buffer_info((void*) frm.data,                    // Pointer to buffer
                                   itemsize,                            // Size of one scalar
                                   format_descriptor(frm.pixel_type()), // Python struct-style format descriptor
                                   ndim,                                // Number of dimensions
                                   {nb_channels, height, width},        // Buffer dimensions
                                   {channelsize, rowsize, itemsize}     // Strides (in bytes) for each index
            );
        })
        .def("__repr__", [](const frame& frm) {
            return fmt::format("<Lima.Frame '{}x{}x{}'>", frm.width(), frm.height(), frm.nb_channels());
        });

    // Define point
    using point_t = point<std::ptrdiff_t>;
    py::class_<point_t>(m, "Point")
        .def(py::init<>())
        .def(py::init<std::ptrdiff_t, std::ptrdiff_t>(), "x"_a, "y"_a)
        .def_readwrite("x", &point_t::x, "The pixel coordinate in the width dimension.")
        .def_readwrite("y", &point_t::y, "The pixel coordinate in the height dimension.")
        .def("__repr__", [](const point_t& p) { return fmt::format("<Lima.Point '{}, {}'>", p.x, p.y); })
        .def(pybind11::self == pybind11::self)
        .def(pybind11::self != pybind11::self);

    // Define rectangle
    using rectangle_t = rectangle<std::ptrdiff_t>;
    py::class_<rectangle_t>(m, "Rectangle")
        .def(py::init<>())
        .def(py::init<std::ptrdiff_t, std::ptrdiff_t, std::ptrdiff_t, std::ptrdiff_t>(), "x"_a, "y"_a, "width"_a,
             "height"_a)
        .def(py::init<point_t, point_t>(), "topleft"_a, "dimensions"_a)
        .def_readwrite("top_left", &rectangle_t::topleft, "The coordinates of the top left corner of the rectangle.")
        .def_readwrite("dimensions", &rectangle_t::dimensions, "The dimensions of the rectangle.")
        .def("__repr__",
             [](const rectangle_t& r) {
                 return fmt::format("<Lima.Rectangle '{}, {} - {}x{}'>", r.topleft.x, r.topleft.y, r.dimensions.x,
                                    r.dimensions.y);
             })
        .def(pybind11::self == pybind11::self)
        .def(pybind11::self != pybind11::self);

    // Define the frame_info class
    py::class_<frame_info>(m, "FrameInfo")
        .def(py::init<>())
        .def(py::init<>([](frame_info::coord_t width, frame_info::coord_t height, std::size_t nb_channels,
                           pixel_enum pixel) { return frame_info(width, height, nb_channels, pixel); }),
             "width"_a, "height"_a, "nb_channels"_a, "pixel"_a)
        .def(py::init<>([](point_t dimensions, std::size_t nb_channels, pixel_enum pixel) {
                 return frame_info(dimensions, nb_channels, pixel);
             }),
             "dimensions"_a, "nb_channels"_a, "pixel"_a)
        .def_property_readonly("dimensions", &frame_info::dimensions, "The dimensions of a frame.")
        .def_property_readonly("width", &frame_info::width, "The width of a frame.")
        .def_property_readonly("height", &frame_info::height, "The height of a frame.")
        .def_property_readonly("nb_channels", &frame_info::nb_channels, "The number of channels.")
        .def_property_readonly("pixel_type", &frame_info::pixel_type, "The type of pixel.")
        .def("__repr__",
             [](const frame_info& frm) {
                 return fmt::format("<Lima.FrameInfo '{}x{}x{}'>", frm.width(), frm.height(), frm.nb_channels());
             })
        .def(pybind11::self == pybind11::self)
        .def(pybind11::self != pybind11::self);

    // Define the hw::info class
    py::class_<hw::info> info(m, "DetInfo");
    described_class(info);

    // Define the hw::capabilities class
    py::class_<hw::capabilities> capabilities(m, "DetCapabilities");
    described_class(capabilities);

    // Define the io:file_exists_policy enumeration
    py::enum_<io::file_exists_policy_enum> file_exists_policy(m, "FileExistsPolicy");
    described_enum(file_exists_policy);

    // Define the io:saving_params enumeration
    py::class_<io::saving_params> saving_params(m, "SavingParams");
    described_class(saving_params);
}
