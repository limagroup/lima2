// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cassert>

#include <algorithm>
#include <span>

//#include <boost/gil.hpp>
#include <boost/smart_ptr/make_shared.hpp> // For allocate_shared

#include <lima/core/frame.hpp>

using namespace lima;
//namespace gil = boost::gil;

namespace detail
{
///  computing size with alignment
template <typename T>
inline T align(T val, std::size_t alignment)
{
    return val + (alignment - val % alignment) % alignment;
}
} // namespace detail

// Recreate without Allocator
void frame::recreate(const point_t& dims, std::size_t nb_channels, const pixel_enum& pixel, std::size_t alignment)
{
    if (dims == m_dimensions && m_nb_channels == nb_channels && m_pixel == pixel && m_align_in_bytes == alignment)
        return;

    m_dimensions = dims;
    m_nb_channels = nb_channels;
    m_align_in_bytes = alignment;

    // If we don't have enough memory allocated already
    if (m_allocated_bytes < total_allocated_size_in_bytes(dims, nb_channels, pixel)) {
        frame tmp(dims, nb_channels, pixel, alignment);
        swap(tmp);
    }
}

// Recreate with Allocator
void frame::recreate(const point_t& dims, std::size_t nb_channels, const pixel_enum& pixel, std::size_t alignment,
                     const allocator_t& alloc_in)
{
    if (dims == m_dimensions && m_nb_channels == nb_channels && m_pixel == pixel && m_align_in_bytes == alignment
        && alloc_in == m_alloc)
        return;

    m_dimensions = dims;
    m_nb_channels = nb_channels;
    m_align_in_bytes = alignment;

    // If we don't have enough memory allocated already
    if (m_allocated_bytes < total_allocated_size_in_bytes(dims, nb_channels, pixel)) {
        frame tmp(dims, nb_channels, pixel, alignment, alloc_in);
        swap(tmp);
    }
}

std::ptrdiff_t frame::get_row_size_in_bytes(coord_t width, const pixel_enum& pixel) const
{
    std::ptrdiff_t size = width * sizeof_pixel(pixel);
    if (m_align_in_bytes > 0)
        return detail::align(size, m_align_in_bytes);

    return size;
}

std::ptrdiff_t frame::total_allocated_size_in_bytes(point_t const& dims, std::size_t nb_channels,
                                                    pixel_enum const& pixel) const
{
    std::size_t allocated_bytes = get_row_size_in_bytes(dims.x, pixel) * dims.y * nb_channels;

    // return the size rounded up to the nearest byte
    return allocated_bytes
           + (m_align_in_bytes > 0 ? m_align_in_bytes - 1
                                   : 0); // add extra padding in case we need to align the first image pixel
}

void frame::allocate(point_t const& dims, std::size_t nb_channels, pixel_enum const& pixel)
{
    // if it throws and m_memory!=0 the client must deallocate m_memory
    m_allocated_bytes = total_allocated_size_in_bytes(dims, nb_channels, pixel);

    // In C++20
    m_memory = boost::allocate_shared<std::byte[]>(m_alloc, m_allocated_bytes);
    //m_memory = std::shared_ptr<unsigned char>(m_alloc.allocate(m_allocated_bytes));

    if (m_memory) {
        data = (m_align_in_bytes > 0) ? (std::byte*) detail::align((std::size_t) m_memory.get(), m_align_in_bytes)
                                      : m_memory.get();
    }

    rowsize = get_row_size_in_bytes(dims.x, pixel);
}

void frame::swap(frame& frm)
{
    using std::swap;
    swap(data, frm.data);
    swap(rowsize, frm.rowsize);

    swap(m_dimensions, frm.m_dimensions);
    swap(m_nb_channels, frm.m_nb_channels);
    swap(m_pixel, frm.m_pixel);
    swap(m_align_in_bytes, frm.m_align_in_bytes);
    swap(m_memory, frm.m_memory);
    //swap(m_alloc, frm.m_alloc);
    swap(m_allocated_bytes, frm.m_allocated_bytes);
    swap(metadata, frm.metadata);
    swap(attributes, frm.attributes);
}

#include <cstdint>

template <typename T>
bool impl_is_zero(std::byte const* const data, std::size_t len)
{
    auto s = std::span(reinterpret_cast<T const* const>(data), len);
    return std::all_of(s.begin(), s.end(), [](auto p) -> bool { return p == 0; });
}

bool frame::is_zero() const
{
    bool res;

    std::size_t len = m_dimensions.x * m_dimensions.y * m_nb_channels;

    switch (m_pixel) {
    case pixel_enum::gray8:
        res = impl_is_zero<std::uint8_t>(data, len);
        break;
    case pixel_enum::gray8s:
        res = impl_is_zero<std::int8_t>(data, len);
        break;
    case pixel_enum::gray16:
        res = impl_is_zero<std::uint16_t>(data, len);
        break;
    case pixel_enum::gray16s:
        res = impl_is_zero<std::int16_t>(data, len);
        break;
    case pixel_enum::gray32:
        res = impl_is_zero<std::uint32_t>(data, len);
        break;
    case pixel_enum::gray32s:
        res = impl_is_zero<std::int32_t>(data, len);
        break;
    case pixel_enum::gray32f:
        res = impl_is_zero<float>(data, len);
        break;
    case pixel_enum::gray64f:
        res = impl_is_zero<double>(data, len);
        break;
    default:
        assert(false);
    }

    return res;
}
