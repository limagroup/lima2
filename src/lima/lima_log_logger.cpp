// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem.hpp>

#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

#include <lima/log/severity_level.describe.hpp>
#include <lima/log/channel.hpp>
#include <lima/log/color_formatter.hpp>
#include <lima/log/file_formatter.hpp>
#include <lima/utils/lengthof.hpp>

#include <lima/logging.hpp>
#include <lima/log/logger.hpp>
#include <lima/exceptions.hpp>

namespace lima
{
namespace log
{
    void init()
    {
        // Adding attributes
        boost::log::add_common_attributes();

        // Adding scop attribute
        boost::log::core::get()->add_global_attribute("Scope", boost::log::attributes::named_scope());
    }

    void add_console_log()
    {
        // Setting up console sink
        boost::log::add_console_log(std::clog)->set_formatter(&coloring_formatter);
    }

    void add_file_log(std::filesystem::path const& log_file_path, std::filesystem::path const& log_file_prefix)
    {
        namespace sinks = boost::log::sinks;
        namespace keywords = boost::log::keywords;

        // Setting up file sink
        auto sink_ptr = boost::log::add_file_log(
            keywords::file_name = log_file_path / log_file_prefix, keywords::open_mode = std::ios_base::app,
            keywords::rotation_size = 10 * 1024 * 1024,
            keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0));
        sink_ptr->set_formatter(&file_formatter);

        // Check that specified path is OK for logging: attempt log message
        try {
            auto rec = lima::log::logger::get().open_record(
                (::boost::log::keywords::severity = ::lima::log::severity_level::fatal,
                 ::boost::log::keywords::channel = ::lima::log::app_domain::core));
            sink_ptr->locked_backend()->consume(rec.lock(), "Initialising log file\n");
        } catch (boost::exception& e) {
            boost::log::core::get()->remove_sink(sink_ptr);
            LIMA_LOG(error, core) << "Failed to initialize log file (check existing file permission): "
                                  << boost::diagnostic_information(e);
        }
    }

    void set_filter(severity_level sev, channel_name chan)
    {
        boost::log::core::get()->set_filter([sev, chan](const boost::log::attribute_value_set& attr_set) {
            return attr_set["Severity"].extract<severity_level>() >= sev
                   && attr_set["Channel"].extract<channel_name>() == chan;
        });
    }

} // namespace log
} //namespace lima
