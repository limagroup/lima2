// pch.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#if defined(LIMA_USE_PCH)

// Std C
#include <cassert>
#include <cmath>

// Std C++
#include <array>
#include <chrono>
#include <condition_variable>
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <ranges>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

// Boost
#include <boost/circular_buffer.hpp>
#include <boost/gil.hpp>
#include <boost/hana.hpp>
#include <boost/mp11.hpp>
#if defined(LIMA_ENABLE_MPI)
#include <boost/mpi.hpp>
#endif //defined(LIMA_ENABLE_MPI)

// HDF5
#if defined(LIMA_ENABLE_HDF5)
#include <hdf5.h>
#endif //defined(LIMA_ENABLE_HDF5)

// FMTLIB
#include <fmt/format.h>

#endif //defined(LIMA_USE_PCH)
