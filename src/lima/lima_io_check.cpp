// Copyright (C) 2024 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/exception/info.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

#include <lima/io/check.hpp>

namespace lima
{
namespace io
{
    void check_base_path(std::filesystem::path const& base_path)
    {
        // Check output path exists
        if (not std::filesystem::exists(base_path)) {
            LIMA_LOG(error, io) << "Invalid file path for saving (does not exist) " << base_path;
            LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid file path for saving (does not exist)")
                                 << boost::errinfo_base_path(base_path));
        }

        // Check output path is directory
        if (not std::filesystem::is_directory(base_path)) {
            LIMA_LOG(error, io) << "Invalid file path for saving (not a directory) " << base_path;
            LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid file path for saving (not a directory)")
                                 << boost::errinfo_base_path(base_path));
        }

        // Check output space
        std::filesystem::space_info space = std::filesystem::space(base_path);
        const std::uintmax_t min_space_available = 1 << 30; // 1 GiB
        if (space.available < min_space_available) {
            LIMA_LOG(error, io) << "Invalid filesystem space for saving (" << space.available << " < "
                                << min_space_available << ") " << base_path;
            LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid filesystem space for saving (less than 1 GiB)")
                                 << boost::errinfo_base_path(base_path)
                                 << boost::errinfo_available_space(space.available));
        }

        // Check output write permissions
        std::filesystem::perms perms = std::filesystem::status(base_path).permissions();

        using std::filesystem::perms;
        auto format_perms = [](std::filesystem::perms const& p) {
            using std::filesystem::perms;
            std::ostringstream os;
            auto show = [&os, p](char op, perms perm) { os << (perms::none == (perm & p) ? '-' : op); };
            show('r', perms::owner_read);
            show('w', perms::owner_write);
            show('x', perms::owner_exec);
            show('r', perms::group_read);
            show('w', perms::group_write);
            show('x', perms::group_exec);
            show('r', perms::others_read);
            show('w', perms::others_write);
            show('x', perms::others_exec);
            return os.str();
        };

        if ((perms & perms::owner_write) == perms::none) {
            LIMA_LOG(error, io) << "Invalid filesystem permissions for saving (require owner write) " << base_path
                                << " " << format_perms(perms);
            LIMA_THROW_EXCEPTION(
                lima::invalid_argument("Invalid filesystem permissions for saving (require owner write)")
                << boost::errinfo_base_path(base_path) << boost::errinfo_filesystem_perms(format_perms(perms)));
        }
    }

} // namespace io
} // namespace lima