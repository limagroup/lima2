# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_library(MPI::MPI_C INTERFACE IMPORTED GLOBAL)
#add_library(MPI::MPI_C ALIAS MPI)

file(GLOB_RECURSE mpi_header_files "${CMAKE_SOURCE_DIR}/include/mpi/*.hpp")

#if(POLICY CMP0076)
#    cmake_policy(SET CMP0076 OLD)
#endif(POLICY CMP0076)

target_sources(MPI::MPI_C
    INTERFACE $<BUILD_INTERFACE:${mpi_header_files}>
)

target_include_directories(MPI::MPI_C
    INTERFACE $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/include/mpi>
)
