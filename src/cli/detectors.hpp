// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(LIMA_CLI_DETECTORS_HPP)
#define LIMA_CLI_DETECTORS_HPP

#include <variant>

#if defined(LIMA_HAVE_CAMERA_PSI)
#include <psi/detector.hpp>
#include <psi/processing/graph/lima1.hpp>
#endif //defined(LIMA_HAVE_CAMERA_PSI)

#if defined(LIMA_HAVE_CAMERA_SMARTPIX)
#include <smartpix/detector.hpp>
#include <smartpix/processing/graph/lima1.hpp>
#endif //defined(LIMA_HAVE_CAMERA_SMARTPIX)

#include <simulator/detector.hpp>
#include <simulator/processing/graph/lima1.hpp>

#include <simulator/hw/params.describe.hpp>
#include <simulator/processing/graph/lima1/params.describe.hpp>

// A pair that associate a detector with a variant of supported processing
template <class Detector, class Processing>
struct detector_processing
{
    using detector_t = Detector;
    using processing_t = Processing;
};

// The list of supported detector / processing combinations
// clang-format off
using any_detector_t = std::variant<
    detector_processing<lima::simulator::detector, lima::simulator::processing::lima1::graph>
#if defined(LIMA_HAVE_CAMERA_SMARTPIX)
  , detector_processing<lima::smartpix::detector, lima::smartpix::processing::lima1::graph>
#endif //defined(LIMA_HAVE_CAMERA_PSI)
#if defined(LIMA_HAVE_CAMERA_PSI)
  , detector_processing<lima::psi::detector, lima::psi::processing::lima1::graph>
#endif //defined(LIMA_HAVE_CAMERA_PSI)
>;
// clang-format on

// Construct detector by name
class by_names
{
    std::string m_det_name;
    std::string m_proc_name;

  public:
    explicit by_names(std::string detector_name, std::string processing_name) :
        m_det_name(std::move(detector_name)), m_proc_name(std::move(processing_name))
    {
    }

    template <typename Type>
    [[nodiscard]] bool apply() const
    {
        return Type::detector_t::name == m_det_name && Type::processing_t::name == m_proc_name;
    };
};

// Print detector names and its available processings
class print_detector
{
  protected:
    std::ostream& m_out;

  public:
    explicit print_detector(std::ostream& out) : m_out(out) {}

    template <typename Type>
    void apply() const
    {
        m_out << "  - " << Type::detector_t::name << " with processing " << Type::processing_t::name << "\n";
    };
};

#endif //!defined(LIMA_CLI_DETECTORS_HPP)
