// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <string>
#include <thread>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/json.hpp>
#include <boost/json/src.hpp>

#include <lima/logging.hpp>
#include <lima/core/io.hpp>

#include <lima/utils/variant_algo.hpp>
#include <lima/utils/base64.hpp>

#include "detectors.hpp"

int main(int argc, char* argv[])
{
    lima::log::init();

    // broadcast command line parameters

    try {
        any_detector_t detector;

        std::string detector_name;
        std::string processing_name;

        // Get the detector type according to detector and processing names
        bool res = lima::construct_matched(detector, by_names(detector_name, processing_name));
        if (!res) {
            std::cerr << "[RCV] Specified detector " << detector_name << " with " << processing_name
                      << " is not supported." << std::endl;
            return 1;
        }

        //From there on, use the specified detector and the specified processing
        return std::visit(
            [](auto detector) {
                using detector_processing_t = std::decay_t<decltype(detector)>;
                using detector_descriptor_t = typename detector_processing_t::detector_t;
                using processing_descriptor_t = typename detector_processing_t::processing_t;
                using processing_t = typename processing_descriptor_t::processing_t;
                using receiver_t = typename detector_descriptor_t::template receiver_t<processing_t>;

                std::cout << "[RCV] Using detector <" << detector_descriptor_t::name << ">\n";
                std::cout << "[RCV] Using processing <" << processing_descriptor_t::name << ">\n";

                lima::uuid uuid;
                typename receiver_t::acq_params_t acq_params;
                typename receiver_t::proc_params_t proc_params;

                receiver_t recv;

                // Loop for commands

                return 0;
            },
            detector);
    } catch (std::exception& e) {
        std::cerr << "Error: " << boost::diagnostic_information(e, true) << std::endl;
    }
}
