// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <type_traits>
#include <vector>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/program_options.hpp>
#include <boost/serialization/stdexcept.hpp>
#include <boost/hana.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <fmt/chrono.h>

#include <cli/remotecli.h>
#include <cli/cli.h>

#include <lima/logging.hpp>
#include <lima/core/io.hpp>

#include <boost/describe/io_enums.hpp>
#include <boost/describe/serialization.hpp>

#include <lima/hw/params.describe.hpp>
#include <lima/utils/variant_algo.hpp>
#include <lima/utils/type_traits.hpp>

#include "detectors.hpp"

namespace po = boost::program_options;

using namespace std::string_literals; // enables s-suffix for std::string literals
using namespace std::chrono_literals; // enables s-suffix for seconds and more

// TODO Make a PR to improve CLI to have default implementation for TypeDesc trait using something like a utils/type_name.hpp
namespace cli
{
template <>
struct TypeDesc<lima::acq_mode_enum>
{
    static const char* Name() { return "<acq_mode_enum>"; }
};
template <>
struct TypeDesc<lima::trigger_mode_enum>
{
    static const char* Name() { return "<trigger_mode_e>"; }
};
template <>
struct TypeDesc<lima::simulator::hw::generator_type_enum>
{
    static const char* Name() { return "<generator_type_enum>"; }
};
template <>
struct TypeDesc<std::filesystem::path>
{
    static const char* Name() { return "<path>"; }
};
template <>
struct TypeDesc<lima::io::file_exists_policy_enum>
{
    static const char* Name() { return "<file_exists_policy_enum>"; }
};
template <>
struct TypeDesc<lima::io::h5::compression_enum>
{
    static const char* Name() { return "<compression_enum>"; }
};
template <>
struct TypeDesc<lima::rectangle<std::ptrdiff_t>>
{
    static const char* Name() { return "<rectangle>"; }
};
} //namespace cli

//// Overloading Lambdas in C++17
//template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; }; // (1)
//template<class... Ts> overloaded(Ts...)->overloaded<Ts...>;  // (2)

namespace std
{
// Vector support
template <typename T>
ostream& operator<<(ostream& os, vector<T> const& v)
{
    os << "[";
    if (!v.empty()) {
        os << v[0];
        for (size_t i = 1; i < v.size(); ++i) {
            os << ", " << v[i];
        }
    }
    os << "]";
    return os;
}

} //namespace std

namespace lima
{
template <typename T>
std::istream& operator>>(std::istream& is, rectangle<T>& r)
{
    is >> r.topleft.x >> r.topleft.y >> r.dimensions.x >> r.dimensions.y;
    return is;
}
} // namespace lima

namespace std
{
// Array support
template <typename T, std::size_t N>
ostream& operator<<(ostream& os, array<T, N> const& v)
{
    os << "[";
    if (!v.empty()) {
        os << v[0];
        for (size_t i = 1; i < v.size(); ++i) {
            os << ", " << v[i];
        }
    }
    os << "]";
    return os;
}

template <typename T, std::size_t N>
istream& operator>>(istream& is, array<T, N>& v)
{
    // TODO
    return is;
}

// Boost.Array support
template <typename T, std::size_t N>
ostream& operator<<(ostream& os, boost::array<T, N> const& v)
{
    os << "[";
    if (!v.empty()) {
        os << v[0];
        for (size_t i = 1; i < v.size(); ++i) {
            os << ", " << v[i];
        }
    }
    os << "]";
    return os;
}

template <typename T, std::size_t N>
istream& operator>>(istream& is, boost::array<T, N>& v)
{
    // TODO
    return is;
}

// Chrono duration support
template <typename Rep, typename Period>
ostream& operator<<(ostream& os, chrono::duration<Rep, Period> const& d)
{
    os << fmt::format("{}", d);
    return os;
}

template <typename Rep, typename Period>
istream& operator>>(istream& is, chrono::duration<Rep, Period>& d)
{
    Rep duration;
    is >> duration;
    d = chrono::duration<Rep, Period>(duration);
    return is;
}

} //namespace std

// Annotations
using Desc = BOOST_DESCRIBE_MAKE_NAME(desc);
using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

int main(int argc, char* argv[])
{
    lima::log::init();

    // Declare the supported options.
    po::options_description global("Allowed options");
    // clang-format off
    global.add_options()
        ("help", "Produce help message")
        ("debug", "Stop the program at the beginning")
        ("log-level",po::value<lima::log::severity_level>()->default_value(lima::log::severity_level::warning), "Logging level [trace=0, debug, info, warning, error, fatal=5]")
        ("address", po::value<std::string>()->default_value("127.0.0.1"), "IP to bind the telnet server")
        ("port", po::value<short>()->default_value(5000), "Port of the telnet server")
        ("detector", po::value<std::string>(), "Name of the detector (see --help for the available options)")
        ("processing", po::value<std::string>(), "Name of the processing (see --help for the available options)")
        ("subargs", po::value<std::vector<std::string>>(), "Arguments for detector")
        ;
    // clang-format on

    po::positional_options_description pos;
    pos.add("detector", 1).add("processing", 1).add("subargs", -1);

    // Parse command line
    po::parsed_options parsed =
        po::command_line_parser(argc, argv).options(global).positional(pos).allow_unregistered().run();

    po::variables_map vm;
    po::store(parsed, vm);

    if (!vm.count("detector") || vm.count("help")) {
        std::cout << global << "\n";
        std::cout << "Available detectors:\n";
        lima::for_each<any_detector_t>(print_detector(std::cout));
        return 0;
    }

    if (vm.count("log-level"))
        // Set log filter according to the log level
        lima::log::set_filter(vm["log-level"].as<lima::log::severity_level>(), lima::log::app_domain::all);

    try {
        any_detector_t detector;
        if (vm.count("detector") && vm.count("processing")) {
            std::string detector_name = vm["detector"].as<std::string>();
            std::string processing_name = vm["processing"].as<std::string>();

            // Get the detector type according to detector and processing names
            bool res = lima::construct_matched(detector, by_names(detector_name, processing_name));
            if (!res) {
                std::cerr << "[CTL] Specified detector " << detector_name << " with " << processing_name
                          << " is not supported." << std::endl;
                return 1;
            }
        }

        //From there on, use the specified detector
        return std::visit(
            [&parsed, &vm](auto detector) {
                bool debug = vm.count("debug");

                using detector_processing_t = std::decay_t<decltype(detector)>;
                using detector_descriptor_t = typename detector_processing_t::detector_t;
                using processing_descriptor_t = typename detector_processing_t::processing_t;
                using processing_t = typename processing_descriptor_t::processing_t;
                using controller_t = typename detector_descriptor_t::controller_t;

                std::cout << "[CTL] Using detector <" << detector_descriptor_t::name << ">\n";
                std::cout << "[CTL] Using processing <" << processing_descriptor_t::name << ">\n";
                std::cout << "[CTL] Debug is set to <" << (debug ? "true" : "false") << ">\n";

                lima::uuid uuid;
                typename controller_t::init_params_t init_params;
                typename controller_t::acq_params_t acq_params;

                // A lambda that construct a set of command options from a parameter struct
                auto construct_option = [](po::options_description& opts, auto& params) {
                    using params_t = typename std::decay_t<decltype(params)>;
                    boost::mp11::mp_for_each<
                        boost::describe::describe_members<params_t, boost::describe::mod_any_access>>([&](auto D) {
                        using A = boost::describe::annotate_member<decltype(D)>;
                        auto doc = boost::describe::annotation_by_name_v<A, Doc>;
                        opts.add_options()(
                            D.name, po::value(&(params.*D.pointer))->required()->default_value(params.*D.pointer), doc);
                    });
                };

                // Add detector init options
                po::options_description init_opts("Camera init options");
                construct_option(init_opts, init_params);

                // Add acquisition options
                po::options_description acq_opts("Detector acquisition options");
                construct_option(acq_opts, acq_params.acq);

                // Add detector capabilities options
                po::options_description det_cap_opts("Detector image options");
                construct_option(det_cap_opts, acq_params.img);

                // Add detector specific options
                po::options_description det_spec_opts("Detector specific options");
                construct_option(det_spec_opts, acq_params.det);

                //TODO: Add more options

                // Add help option
                po::options_description help_opts;
                help_opts.add_options()("help", "produce detector help message");

                po::options_description detector_opts;
                detector_opts.add(help_opts).add(init_opts).add(acq_opts).add(det_cap_opts).add(det_spec_opts);

                // Collect all the unrecognized options from the first pass. This will include the
                // (positional) detector name, so we need to erase that.
                std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
                if (!opts.empty())
                    opts.erase(opts.begin());

                // Parse again with the detector options
                //po::variables_map vm;
                po::store(po::command_line_parser(opts).options(detector_opts).run(), vm);
                po::notify(vm);

                if (vm.count("help")) {
                    std::cout << init_opts << "\n" << acq_opts << "\n" << det_cap_opts << "\n" << det_spec_opts << "\n";
                    return 0;
                }

                // Construct the control
                controller_t ctrl(init_params, debug);

                {
                    using namespace cli;

                    std::mutex cout_mutex;
                    auto root_menu = std::make_unique<Menu>("control");

                    // Controler commands
                    root_menu->Insert(
                        "prepare",
                        [&ctrl, &uuid, &acq_params](std::ostream& out) { ctrl.prepare_acq(uuid, acq_params); },
                        "Prepare the acquisition");
                    root_menu->Insert(
                        "start", [&ctrl](std::ostream& out) { ctrl.start_acq(); }, "Start the acquisition");
                    root_menu->Insert(
                        "stop", [&ctrl](std::ostream& out) { ctrl.stop_acq(); }, "Stop the acquisition");
                    root_menu->Insert(
                        "reset", [&ctrl](std::ostream& out) { ctrl.reset_acq(lima::reset_level_enum::soft); },
                        "Reset the detector and get back to idle");
                    root_menu->Insert(
                        "nb_frames_acquired", [&ctrl](std::ostream& out) { out << ctrl.nb_frames_acquired(); },
                        "Get the number of frame acquired");

                    // A lambda that construct a menu from a parameter struct
                    const auto construct_menu = [](auto& menu, auto& params) {
                        // Pattern for recursive lambda
                        auto construct_menu_impl = [](auto& menu, auto& params, auto& construct_menu_ref) mutable {
                            using params_t = typename std::decay_t<decltype(params)>;

                            // Recursively add base classes
                            boost::mp11::mp_for_each<
                                boost::describe::describe_bases<params_t, boost::describe::mod_any_access>>(
                                [&](auto D) {
                                    using B = typename decltype(D)::type;
                                    construct_menu_ref(menu, (B&) params, construct_menu_ref);
                                });

                            boost::mp11::mp_for_each<
                                boost::describe::describe_members<params_t, boost::describe::mod_any_access>>(
                                [&](auto D) {
                                    using A = boost::describe::annotate_member<decltype(D)>;
                                    using param_t = lima::member_type<decltype(D.pointer)>;

                                    auto desc = boost::describe::annotation_by_name_v<A, Desc>;
                                    auto doc = boost::describe::annotation_by_name_v<A, Doc>;

                                    menu->Insert(
                                        D.name, {desc},
                                        [&](std::ostream& out, param_t val) { params.*D.pointer = val; }, doc);
                                });

                            menu->Insert(
                                "show",
                                [&params](std::ostream& out) {
                                    boost::mp11::mp_for_each<
                                        boost::describe::describe_members<params_t, boost::describe::mod_any_access>>(
                                        [&](auto D) { out << D.name << " = " << params.*D.pointer << "\n"; });
                                },
                                "Display every parameters");
                        };
                        return construct_menu_impl(menu, params, construct_menu_impl);
                    };

                    // Add acquisition menu
                    auto acq_menu = std::make_unique<Menu>("acquisition");
                    construct_menu(acq_menu, acq_params.acq);
                    root_menu->Insert(std::move(acq_menu));

                    // Add detector menu
                    auto det_cap_menu = std::make_unique<Menu>("image capabilities");
                    construct_menu(det_cap_menu, acq_params.img);
                    root_menu->Insert(std::move(det_cap_menu));

                    auto det_spec_menu = std::make_unique<Menu>("detector specific");
                    construct_menu(det_spec_menu, acq_params.det);
                    root_menu->Insert(std::move(det_spec_menu));

                    SetColor();

                    Cli cli(std::move(root_menu));
                    boost::asio::io_context io;

                    short port = vm["port"].as<short>();
                    std::string address = vm["address"].as<std::string>();
                    std::cout << "Telnet server listening on " << address << ":" << port << std::endl;

                    CliTelnetServer server(io, address, port, cli);
                    server.ExitAction([](auto& out) { out << "Terminating this session...\n"; });

                    // Run the I/O loop
                    io.run();
                }

                return 0;
            },
            detector);

    } catch (std::exception& e) {
        std::cerr << "Error: " << boost::diagnostic_information(e, true) << std::endl;
    }
}
