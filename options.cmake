# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

# Tests
option(LIMA_ENABLE_TESTS "compile test directories?" OFF)
option(LIMA_ENABLE_TEST_HEADERS "test if headers are self-contained?" OFF)

# Benchmarks
option(LIMA_ENABLE_BENCHMARKS "compile bench directories?" OFF)

# Sanitizers
option(LIMA_ENABLE_ASAN "compile with address and undefined sanitizers?" OFF) 

# Saving formats
if(DEFINED ENV{LIMA_ENABLE_TIFF})
	set(LIMA_ENABLE_TIFF "$ENV{LIMA_ENABLE_TIFF}" CACHE BOOL "compile TIFF saving code?" FORCE)
else()
	option(LIMA_ENABLE_TIFF "compile TIFF saving code?" OFF)
endif()
if(DEFINED ENV{LIMA_ENABLE_HDF5})
    set(LIMA_ENABLE_HDF5 "$ENV{LIMA_ENABLE_HDF5}" CACHE BOOL "compile HDF5 saving code?" FORCE)
else()
    option(LIMA_ENABLE_HDF5 "compile HDF5 saving code?" OFF)
endif()

# Python binding
IF(DEFINED ENV{LIMA_ENABLE_PYTHON})
    set(LIMA_ENABLE_PYTHON "$ENV{LIMA_ENABLE_PYTHON}" CACHE BOOL "compile python modules?" FORCE)
else()
    option(LIMA_ENABLE_PYTHON "compile python modules?" OFF)
endif()

IF(DEFINED ENV{LIMA_ENABLE_TANGO})
    set(LIMA_ENABLE_TANGO "$ENV{LIMA_ENABLE_TANGO}" CACHE BOOL "compile tango device server?" FORCE)
else()
    option(LIMA_ENABLE_TANGO "compile tango device server?" ON)
endif()

# MPI
option(LIMA_ENABLE_MPI "enable MPI?" ON)

# Boost.Log
option(LIMA_ENABLE_LOGGING "enable Logging?" ON)

# Numa support
option(LIMA_ENABLE_NUMA "enable Numa?" OFF)

# With ZLIB
option(LIMA_ENABLE_ZLIB "enable Zlib?" OFF)

# With Bitshuffle
option(LIMA_ENABLE_BSHUF_LZ4 "enable Bitshuffle LZ4?" OFF)

# With Blosc2
option(LIMA_ENABLE_BLOSC2 "enable Blosc2?" OFF)

# With CUDA
option(LIMA_ENABLE_CUDA "enable CUDA?" OFF)

# With OpenCL
option(LIMA_ENABLE_OPENCL "enable OpenCL?" OFF)

# Precompiled header 
option(LIMA_USE_PCH "Use precompiled headers to speedup compilation?" ON)

# CLI
option(LIMA_ENABLE_CLI "compile the command line interface?" OFF)
