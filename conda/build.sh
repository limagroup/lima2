#!/bin/bash
cmake -Bbuild -H. -G "Ninja" -DCMAKE_INSTALL_PREFIX=$PREFIX -DCMAKE_FIND_ROOT_PATH=$PREFIX -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=ONLY -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=ONLY -DLIMA_ENABLE_TESTS=ON -DLIMA_ENABLE_TEST_HEADERS=ON -DLIMA_ENABLE_BENCHMARKS=ON -DLIMA_ENABLE_MPI=ON -DLIMA_ENABLE_PYTHON=ON -DLIMA_ENABLE_TIFF=ON -DLIMA_ENABLE_HDF5=ON -DLIMA_ENABLE_NUMA=ON -DLIMA_ENABLE_BSHUF_LZ4=ON -DLIMA_ENABLE_ZLIB=ON -DLIMA_ENABLE_OPENCL=ON -DLIMA_PIPELINE_XPCS=ON
cmake --build build --target install

# Copy the [de]activate scripts to $PREFIX/etc/conda/[de]activate.d.
# This will allow them to be run on environment activation.
for CHANGE in "activate" "deactivate"
do
    mkdir -p "${PREFIX}/etc/conda/${CHANGE}.d"
    cp "${RECIPE_DIR}/${CHANGE}.sh" "${PREFIX}/etc/conda/${CHANGE}.d/${PKG_NAME}_${CHANGE}.sh"
done

# Run unit tests, allowing fails (they will be displayed in the Gitlab report)
BOOST_TEST_COLOR_OUTPUT=0 ctest --test-dir build --output-junit "$RECIPE_DIR/../unit-test-report.xml" || true
