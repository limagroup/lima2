// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_DESCRIBE_ANNOTATIONS_HPP)
#define BOOST_DESCRIBE_ANNOTATIONS_HPP

#include <type_traits>

#include <boost/describe/class.hpp>
#include <boost/describe/descriptor_by_name.hpp>
#include <boost/mp11.hpp>

namespace boost
{
namespace describe
{
namespace detail
{
    template <auto N, auto P> struct ND {};

    constexpr unsigned cx_hash( char const* s )
    {
        unsigned r = 0;

        for( ; *s; ++s )
        {
            r = r * 31 + (unsigned char)*s;
        }

        return r;
    }
} // namespace detail

// Normalized descriptor
template<class D> using normalize_descriptor = detail::ND<detail::cx_hash(D::name), D::pointer>;

namespace detail
{
    template <class Pm>
    struct class_type_impl {};

    template <class M, class C>
    struct class_type_impl<M C::*const> { using type = C; };
}

/// Returns the type of class for a given pointer to member type
template <class Pm>
using class_type = typename detail::class_type_impl<Pm>::type;

namespace detail
{
    template <typename A>
    struct member_annotation
    {
        static constexpr decltype(A::name()) name = A::name();
        static constexpr decltype(A::value()) value = A::value();
    };

    template <typename... T>
    auto member_annotation_fn_impl(int, T...)
    {
        return list<member_annotation<T>...>();
    }

#define BOOST_ANNOTATE_MEMBER_IMPL(C, n, v)                                                \
    , [] {                                                                                 \
        struct _boost_note                                                                 \
        {                                                                                  \
            static constexpr auto name() noexcept { return BOOST_DESCRIBE_PP_NAME(n); }    \
            static constexpr auto value() noexcept { return BOOST_DESCRIBE_PP_EXPAND(v); } \
        };                                                                                 \
        return _boost_note();                                                              \
    }()

#define BOOST_ANNOTATE_MEMBER_IMPL_(a) BOOST_ANNOTATE_MEMBER_IMPL(a)

#define BOOST_ANNOTATE_MEMBER_IMPL__(m, Annotation) BOOST_ANNOTATE_MEMBER_IMPL_(m BOOST_DESCRIBE_PP_UNPACK Annotation)

#define BOOST_DESCRIBE_NORMALIZED_MEMBER_DESCRIPTOR(C, m) \
    boost::describe::detail::ND<boost::describe::detail::cx_hash(#m), &C::m>

#if defined(_MSC_VER) && !defined(__clang__)

#define BOOST_ANNOTATE_MEMBER(C, m, ...)                                                             \
    static_assert(std::is_class_v<C>, "BOOST_ANNOTATE_MEMBER should only be used with class types"); \
    inline auto boost_annotate_fn(C*, BOOST_DESCRIBE_NORMALIZED_MEMBER_DESCRIPTOR(C, m)*)            \
    {                                                                                                \
        return boost::describe::detail::member_annotation_fn_impl(                                   \
            0 BOOST_DESCRIBE_PP_FOR_EACH(BOOST_ANNOTATE_MEMBER_IMPL__, C, __VA_ARGS__));           \
    }

#else

#define BOOST_ANNOTATE_MEMBER(C, m, ...)                                                             \
    static_assert(std::is_class_v<C>, "BOOST_ANNOTATE_MEMBER should only be used with class types"); \
    inline auto boost_annotate_fn(C*, BOOST_DESCRIBE_NORMALIZED_MEMBER_DESCRIPTOR(C, m)*)            \
    {                                                                                                \
        return boost::describe::detail::member_annotation_fn_impl(                                   \
            0 BOOST_DESCRIBE_PP_FOR_EACH(BOOST_ANNOTATE_MEMBER_IMPL__, C, ##__VA_ARGS__));           \
    }

#endif

    inline auto boost_annotate_fn(auto*, auto*) { return list<>(); }

} // namespace detail

template <class Md, class C = class_type<decltype(Md::pointer)>, class Nd = normalize_descriptor<Md>>
using annotate_member = decltype(boost_annotate_fn(static_cast<C*>(0), static_cast<Nd*>(0)));

template <typename A, typename N>
using annotation_by_name = mp11::mp_at<A, mp11::mp_find_if_q<A, mp11::mp_bind_back<detail::match_by_name, N>>>;

template <typename A, typename N>
inline constexpr auto annotation_by_name_v = annotation_by_name<A, N>::value;

template <typename A, typename N>
using has_annotation_by_name = mp11::mp_any_of_q<A, mp11::mp_bind_back<detail::match_by_name, N>>;

template <typename A, typename N>
inline constexpr auto has_annotation_by_name_v = has_annotation_by_name<A, N>::value;

} //namespace describe
} //namespace boost

#endif //!defined(BOOST_DESCRIBE_ANNOTATIONS_HPP)
