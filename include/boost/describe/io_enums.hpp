// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_DESCRIBE_IO_ENUMS_HPP)
#define BOOST_DESCRIBE_IO_ENUMS_HPP

#include <cstring>

#include <iostream>
#include <stdexcept>
#include <typeinfo>
#include <string>
#include <optional>

#include <boost/describe.hpp>
#include <boost/mp11.hpp>
#include <boost/algorithm/string/predicate.hpp> // For boost::iequals

namespace boost::describe
{
template <class E>
char const* enum_to_string(E e)
{
    char const* r = "(unnamed)";

    boost::mp11::mp_for_each<boost::describe::describe_enumerators<E>>([&](auto D) {
        if (e == D.value)
            r = D.name;
    });

    return r;
}

[[noreturn]] inline void throw_invalid_name(char const* name, char const* type)
{
    throw std::runtime_error(std::string("Invalid enumerator name '") + name + "' for enum type '" + type + "'");
}

template <class E>
E string_to_enum(char const* const name)
{
    bool found = false;
    E r = {};

    boost::mp11::mp_for_each<boost::describe::describe_enumerators<E>>([&](auto D) {
        if (!found && boost::iequals(D.name, name)) {
            found = true;
            r = D.value;
        }
    });

    if (found) {
        return r;
    } else {
        throw_invalid_name(name, typeid(E).name());
    }
}

template <class E>
std::optional<E> string_to_enum_nothrow(char const* const name)
{
    bool found = false;
    std::optional<E> res;

    boost::mp11::mp_for_each<boost::describe::describe_enumerators<E>>([&](auto D) {
        if (!found && boost::iequals(D.name, name)) {
            found = true;
            res = D.value;
        }
    });

    return res;
}

template <typename E, typename Ed = boost::describe::describe_enumerators<E>>
std::ostream& operator<<(std::ostream& os, E const& e)
{
    os << boost::describe::enum_to_string(e);

    return os;
}

template <typename E, typename Ed = boost::describe::describe_enumerators<E>>
std::istream& operator>>(std::istream& is, E& e)
{
    std::string name;
    is >> name;

    e = boost::describe::string_to_enum<E>(name.c_str());

    return is;
}

} // namespace boost::describe

#endif //!defined(BOOST_DESCRIBE_IO_ENUMS_HPP)