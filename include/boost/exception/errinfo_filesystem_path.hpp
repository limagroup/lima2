// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>

namespace
boost
	{
	template <class Tag, class T> class error_info;

	typedef error_info<struct errinfo_filesystem_path_, std::filesystem::path> errinfo_filesystem_path;
	}
