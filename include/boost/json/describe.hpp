// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_JSON_DESCRIBE_HPP)
#define BOOST_JSON_DESCRIBE_HPP

#include <type_traits>

#include <boost/describe.hpp>
#include <boost/describe/enum_from_string.hpp>
#include <boost/describe/enum_to_string.hpp>
#include <boost/describe/annotations.hpp>
#include <boost/mp11.hpp>
#include <boost/json.hpp>

namespace boost {
namespace json {

// Conversion to JSON
template <
    class T,
    class D1 = boost::describe::describe_members<T,
        boost::describe::mod_public | boost::describe::mod_protected | boost::describe::mod_inherited>,
    class D2 = boost::describe::describe_members<T, boost::describe::mod_private>,
    class En = std::enable_if_t<boost::mp11::mp_empty<D2>::value>>
void tag_invoke(value_from_tag const&, value& v, T const& t)
{
    using Name = BOOST_DESCRIBE_MAKE_NAME(name);

    auto& obj = v.emplace_object();

    boost::mp11::mp_for_each<D1>([&](auto d) {
        using D = std::decay_t<decltype(d)>;
        using La = boost::describe::annotate_member<D>;
        if constexpr (describe::has_annotation_by_name_v<La, Name>) {
            auto name = describe::annotation_by_name_v<La, Name>;
            obj[name] = value_from(t.*d.pointer);
        }
        else
            obj[d.name] = value_from(t.*d.pointer);
    });
}

template <typename E, typename D1 = boost::describe::describe_enumerators<E>>
void tag_invoke(value_from_tag const&, value& v, E const& e)
{
    char const* name = boost::describe::enum_to_string(e, nullptr);

    if (!name)
        throw std::runtime_error(std::string("Invalid enumerator description for enum type '") + typeid(E).name() + "'");

    v = name;
}

// Conversion from JSON
template<class T> void extract( object const & obj, char const * name, T & value )
{
    value = value_to<T>( obj.at( name ) );
}

template<class T,
    class D1 = boost::describe::describe_members<T,
        boost::describe::mod_public | boost::describe::mod_protected | boost::describe::mod_inherited>,
    class D2 = boost::describe::describe_members<T, boost::describe::mod_private>,
    class En = std::enable_if_t<boost::mp11::mp_empty<D2>::value> >
T tag_invoke( value_to_tag<T> const&, value const& v )
{
    using Name = BOOST_DESCRIBE_MAKE_NAME(name);

    auto const& obj = v.as_object();

    T t{};

    boost::mp11::mp_for_each<D1>([&](auto d) {
        using D = std::decay_t<decltype(d)>;
        using La = boost::describe::annotate_member<D>;
        if constexpr (describe::has_annotation_by_name_v<La, Name>) {
            auto name = describe::annotation_by_name_v<La, Name>;
            extract( obj, name, t.*d.pointer );
        }
        else
            extract( obj, d.name, t.*d.pointer );
    });

    return t;
}

template <typename E, typename D1 = boost::describe::describe_enumerators<E>>
E tag_invoke(value_to_tag<E> const&, value const& v)
{
    E e;
    char const* name = v.as_string().c_str();
    bool found = boost::describe::enum_from_string(name, e);

    if (!found)
        throw std::runtime_error(std::string("Invalid enumerator name '") + name + "' for enum type '" + typeid(E).name() + "'");
        
    return e;
}

}  // namespace json
}  // namespace boost

#endif //!defined(BOOST_JSON_DESCRIBE_HPP)