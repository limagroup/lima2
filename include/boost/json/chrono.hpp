// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <chrono>

#include <boost/json.hpp>

namespace boost {
namespace json {

template <typename Rep, typename Period>
void tag_invoke(value_from_tag const&, value& v, std::chrono::duration<Rep, Period> const& t)
{
    v = t.count();
}
template <typename Rep, typename Period>
std::chrono::duration<Rep, Period> tag_invoke(value_to_tag<std::chrono::duration<Rep, Period>> const&, value const& v)
{
    return std::chrono::duration<Rep, Period>(v.as_int64());
}

}  // namespace json
}  // namespace boost
