// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <atomic>

#include <boost/json.hpp>

namespace boost {
namespace json {

template <class T>
void tag_invoke(boost::json::value_from_tag const&, boost::json::value& v, std::atomic<T> const& t)
{
    v = (T) t;
}

}  // namespace json
}  // namespace boost