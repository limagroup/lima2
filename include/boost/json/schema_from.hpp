// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_JSON_SCHEMA_FROM_HPP)
#define BOOST_JSON_SCHEMA_FROM_HPP

#include <chrono>
#include <filesystem>
#include <type_traits>
#include <utility>
#include <variant>

#include <boost/json/value.hpp>
#include <boost/json/conversion.hpp>

#include <boost/describe/enum_to_string.hpp>
#include <boost/describe/annotations.hpp>
#include <boost/mp11.hpp>

namespace boost {
namespace json {

// https://github.com/boostorg/json/issues/867
template<class T>
struct is_described_class2
    : mp11::mp_and<
        describe::has_describe_members<T>,
        mp11::mp_not< std::is_union<T> >,
        mp11::mp_empty<
            mp11::mp_eval_or<
                mp11::mp_list<>, detail::described_non_public_members, T>>>
{ };

namespace detail {

    /// member_type traits
    template <class Pm>
    struct member_type_impl
    {
    };

    template <class M, class C>
    struct member_type_impl<M C::*const>
    {
        using type = M;
    };

    /// Returns the type of member for a given pointer to member type
    template <class Pm>
    using member_type = typename detail::member_type_impl<Pm>::type;

    struct empty_descriptor {};

    ///// class_type traits
    //template <class Pm>
    //struct class_type_impl {};

    //template <class M, class C>
    //struct class_type_impl<M C::*const> { using type = C; };

    ///// Returns the type of class for a given pointer to member type
    //template <class Pm>
    //using class_type = typename class_type_impl<Pm>::type;

    ///// Given a Member Descriptor of a DefaultContructible type
    ///// return the default (constructor) value
    //template <typename D>
    //auto member_default_value(D d)
    //{
    //    using T = class_type<decltype(d.pointer)>;
    //    return T{}.*d.pointer;
    //}

    /// Returns the extend of a builtin-array or std::array
    template<typename T>
    struct array_size : std::extent<T> { };

    template<typename T, size_t N>
    struct array_size<std::array<T,N> > : std::tuple_size<std::array<T,N> > { };

    // Annotations
    using Name = BOOST_DESCRIBE_MAKE_NAME(name);
    using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

    // Forward declaration
    template <typename T, typename D>
    object schema_from_impl(D d, T&& default_value);

    template <typename T>
    struct schema_from_struct
    {
        //template <typename D>
        object operator()(auto, T&& default_value) const {
            object properties;
            mp11::mp_for_each<describe::describe_members<T, describe::mod_any_access | describe::mod_inherited>>(
                [&](auto d) {
                    using Md = std::decay_t<decltype(d)>;
                    using La = boost::describe::annotate_member<Md>;
                    using member_t = member_type<decltype(d.pointer)>;

                    // If alternate name given to member
                    if constexpr (describe::has_annotation_by_name_v<La, Name>) {
                        auto name = describe::annotation_by_name_v<La, Name>;
                        properties[name] = schema_from_impl<member_t>(d, std::forward<member_t>(default_value.*d.pointer));
                    }
                    else
                        properties[d.name] = schema_from_impl<member_t>(d, std::forward<member_t>(default_value.*d.pointer));
                }
            );
            return {
                {"type", "object"},
                {"properties", properties},
                {"additionalProperties", false}
            };
        }
    };

    template <typename E>
    struct schema_from_enum
    {
        object operator()(empty_descriptor, E&& default_value) const {
            array names;
            mp11::mp_for_each< describe::describe_enumerators<E> >([&](auto d){
                names.emplace_back(d.name);
            });

            return {
                { "type", "string" },
                { "enum", names },
                { "default", describe::enum_to_string<E>(default_value, "unknown") }
            };
        };

        template <typename D>
        object operator()(D d, E&& default_value) const {
            using A = describe::annotate_member<D>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            array names;
            mp11::mp_for_each< describe::describe_enumerators<E> >([&](auto d){
                names.emplace_back(d.name);
            });

            return {
                { "type", "string" },
                { "enum", names },
                { "description", doc },
                { "default", describe::enum_to_string<E>(default_value, "unknown") }
            };
        }
    };

    struct schema_from_boolean
    {
        object operator()(empty_descriptor, bool default_value) const {
            return {
                { "type", "boolean" },
                { "default", default_value }
            };
        };

        template <typename D>
        object operator()(D d, bool default_value) const {
            using A = describe::annotate_member<D>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            return {
                { "type", "boolean" },
                { "description", doc },
                { "default", default_value }
            };
        }
    };

    struct schema_from_integer
    {
        template <typename I>
        object operator()(empty_descriptor, I&& default_value) const {
            return {
                { "type", "integer" },
                { "default", default_value }
            };
        };

        template <typename D, typename I>
        object operator()(D d, I default_value) const {
            using A = describe::annotate_member<D>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            return {
                { "type", "integer" },
                { "description", doc },
                { "default", default_value }
            };
        }
    };

    struct schema_from_number
    {
        template <typename N>
        object operator()(empty_descriptor, N&& default_value) const {
            return {
                { "type", "number" },
                { "default", default_value }
            };
        };

        template <typename D, typename N>
        object operator()(D d, N default_value) const {
            using A = describe::annotate_member<decltype(d)>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            return {
                { "type", "number" },
                { "description", doc },
                { "default", default_value }
            };
        }
    };

    template <typename T>
    struct schema_from_duration
    {
        template <typename Rep, typename Period>
        object operator()(empty_descriptor, std::chrono::duration<Rep, Period>&& default_value) const {
            auto unit = "microseconds";

            return {
                { "type", "number" },
                { "default", default_value.count() },
                { "unit", unit }
            };
        };

        template <typename D, typename Rep, typename Period>
        object operator()(D d, std::chrono::duration<Rep, Period>&& default_value) const {
            using A = describe::annotate_member<decltype(d)>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            auto unit = "microseconds"; // TODO

            return {
                { "type", "integer" },
                { "description", doc },
                { "default", default_value.count() },
                { "unit", unit }
            };
        }
    };

    struct schema_from_path
    {

        template <typename D, typename T>
        object operator()(empty_descriptor, T&& default_value) const {
            return {
                { "type", "string" },
                { "default", default_value }
            };
        };

        template <typename D>
        object operator()(D d, std::filesystem::path&& default_value) const {
            using A = describe::annotate_member<decltype(d)>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            return {
                { "type", "string" },
                { "description", doc },
                { "default", default_value.string() }
            };
        }
    };

    struct schema_from_nullptr
    {
        template <typename D>
        void operator()(D) const { /* Do nothing */}
    };

    struct schema_from_string
    {
        template <typename T>
        object operator()(empty_descriptor, T&& default_value) const {
            return {
                { "type", "string" },
                { "default", default_value }
            };
        };

        template <typename D, typename T>
        object operator()(D d, T&& default_value) const {
            using A = describe::annotate_member<decltype(d)>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            return {
                { "type", "string" },
                { "description", doc },
                { "default", default_value }
            };
        }
    };

    struct schema_from_map
    {
        template <typename D>
        object operator()(D d) const {
            // Object
            return {{ "type", "object" }};
        }
    };

    template <typename T>
    struct schema_from_sequence
    {
        template <typename D>
        object operator()(D d, T&& default_value) const {
            using value_type = typename std::iterator_traits<decltype(std::begin(std::declval<T&>()))>::value_type;
            object items;
            if constexpr (is_described_class2<value_type>::value) {
                items = schema_from_impl(describe::describe_members<value_type, describe::mod_any_access | describe::mod_inherited>(), value_type{});
            } else {
                items = schema_from_impl(empty_descriptor{}, value_type{});
            }

            using A = describe::annotate_member<decltype(d)>;
            auto doc = describe::annotation_by_name_v<A, Doc>;

            object res = {
                {"type", "array"},
                {"items", items},
                {"description", doc},
                {"default", boost::json::value_from(default_value)}
            };

            // if extend is know at compile time
            if constexpr (array_size<T>::value > 0)
            {
                res["minItems"] = array_size<T>::value;
                res["maxItems"] = array_size<T>::value;
            }                

            return res;
        }
    };

    struct schema_from_tuple
    {
        template <typename D>
        object operator()(D d) const {
            // Object
            return {{ "type", "array" }};
        }
    };

    template <typename T>
    struct schema_from_variant
    {
        template <typename D>
        object operator()(D, T&& default_value) const {
            array res;
            mp11::mp_for_each<mp11::mp_iota_c<std::variant_size_v<T>>>([&]( auto I ) {
                using alternative_t = std::variant_alternative_t<I, T>;
                res.emplace_back(schema_from_impl<alternative_t>(empty_descriptor{}, alternative_t{}));
            });            
            return {{ "oneOf", res }};
        }
    };

    template <typename T>
    struct is_duration : mp11::mp_false {};

    template <typename Rep, typename Period>
    struct is_duration<std::chrono::duration<Rep, Period>> : mp11::mp_true {};

    template <typename... T>
    struct is_variant : mp11::mp_false {};

    template <typename... T>
    struct is_variant<std::variant<T...>> : mp11::mp_true {};

    template <typename T>
    using is_nullptr = mp11::mp_bool<
        std::is_same<detail::remove_cvref<T>, std::nullptr_t>::value
    >;

    template <typename T>
    using schema_from = mp11::mp_cond<
        // Native conversion 
        std::is_same<T, bool>, schema_from_boolean,
        std::is_integral<T>, schema_from_integer,
        std::is_floating_point<T>, schema_from_number,
        is_variant<T>, schema_from_variant<T>,
        std::is_same<T, std::filesystem::path>, schema_from_path,
        is_duration<T>, schema_from_duration<T>,
        // Generic conversions
        is_null_like<T>, schema_from_nullptr,
        // string-like types
        is_string_like<T>, schema_from_string,
        // map-like and other ranges
        is_map_like<T>, schema_from_map,
        // sequences
        is_sequence_like<T>, schema_from_sequence<T>,
        // tuple-like types
        is_tuple_like<T>, schema_from_tuple,
        // User-provided conversion
        is_described_class2<T>, schema_from_struct<T>,
        is_described_enum<T>, schema_from_enum<T>
    >;

    template <typename T, typename D>
    object schema_from_impl(D d, T&& default_value)
    {
        schema_from<T> impl;
        return impl(d, std::forward<T>(default_value));
    }

} //namespace detail

// T is DefaultConstructible, Described
template <typename T>
object schema_from(std::string const& title, T&& default_value = T{})
{
    object schema  = detail::schema_from_impl<T>(detail::empty_descriptor{}, std::forward<T>(default_value));
    
    schema["$schema"] = "http://json-schema.org/draft-06/schema";
    schema["$id"] = "https://esrf.fr/lima2.schema.json";
    schema["title"] = title;

    return schema;
}

}  // namespace json
}  // namespace boost

#endif //!defined(BOOST_JSON_SCHEMA_FROM_HPP)