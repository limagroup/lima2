// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>

#include <boost/json.hpp>

namespace boost {
namespace json {

inline void tag_invoke(value_from_tag const&, value& v, std::filesystem::path const& p)
{
    v = p.string();
}
inline std::filesystem::path tag_invoke(value_to_tag<std::filesystem::path> const&, value const& v)
{
    return std::filesystem::path((std::string_view) v.as_string());
}

}  // namespace json
}  // namespace boost
