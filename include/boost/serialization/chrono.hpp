// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_SERIALIZATION_CHRONO_HPP)
#define BOOST_SERIALIZATION_CHRONO_HPP

#include <chrono>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_free.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for std::runtime_error
    template <class Archive>
    void save(Archive& ar, const std::chrono::microseconds& us, unsigned int)
    {
        ar << us.count();
    }

    template <class Archive>
    void load(Archive& ar, std::chrono::microseconds& us, unsigned int)
    {
        std::uint64_t ticks;
        ar >> ticks;

        us = std::chrono::microseconds(ticks);
    }

} // namespace serialization
} // namespace boost

// outside of any namespace
BOOST_SERIALIZATION_SPLIT_FREE(std::chrono::microseconds)

#endif //!defined(BOOST_SERIALIZATION_CHRONO_HPP)
