// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_SERIALIZATION_GIL_HPP)
#define BOOST_SERIALIZATION_GIL_HPP

#include <boost/gil/extension/dynamic_image/any_image.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/array_wrapper.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/variant2.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for Boost.Gil any_image_view
    template <typename Archive, typename... ImageViews>
    void serialize(Archive& ar, gil::any_image_view<ImageViews...>& view, const unsigned int version)
    {
        using parent_t = boost::variant2::variant<ImageViews...>;
        ar& boost::serialization::base_object<parent_t>(view);
    }

    // Boost.Gil image_view is a wrapper, data can be loaded in a const object
    // See boost/serialization/wrapper.hpp
    template <typename Locator>
    struct is_wrapper<const gil::image_view<Locator>> : mpl::true_
    {
    };

    template <typename Archive, typename Locator>
    void serialize(Archive& ar, gil::image_view<Locator>& view, const unsigned int version)
    {
        assert(view.is_1d_traversable());
        ar& boost::serialization::make_binary_object(&view(0, 0), view.size() * sizeof(typename Locator::value_type));
    }

    template <typename Archive, typename... Images>
    void serialize(Archive& ar, gil::any_image<Images...>& img, const unsigned int version)
    {
        using parent_t = boost::variant2::variant<Images...>;
        ar& boost::serialization::base_object<parent_t>(img);
    }

    template <typename Archive, typename Pixel, bool IsPlanar, typename Alloc>
    void serialize(Archive& ar, gil::image<Pixel, IsPlanar, Alloc>& img, const unsigned int version)
    {
        split_free(ar, img, version);
    }

    template <typename Archive, typename Pixel, bool IsPlanar, typename Alloc>
    void save(Archive& ar, gil::image<Pixel, IsPlanar, Alloc> const& img, unsigned int)
    {
        ar << img.dimensions();
        ar << gil::const_view(img);
    }

    template <typename Archive, typename Pixel, bool IsPlanar, typename Alloc>
    void load(Archive& ar, gil::image<Pixel, IsPlanar, Alloc>& img, unsigned int)
    {
        typename gil::image<Pixel, IsPlanar, Alloc>::point_t size;
        ar >> size;

        img.recreate(size);

        ar >> gil::view(img);
    }

} // namespace serialization
} // namespace boost

#endif //!defined(BOOST_SERIALIZATION_GIL_HPP)
