// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_SERIALIZATION_DESCRIBE_HPP)
#define BOOST_SERIALIZATION_DESCRIBE_HPP

#include <boost/describe.hpp>
#include <boost/mp11.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/core/nvp.hpp>

namespace boost::serialization
{
template <class Archive, class T, //
          class D1 = boost::describe::describe_bases<T, boost::describe::mod_public>,
          class D2 = boost::describe::describe_bases<T, boost::describe::mod_protected | boost::describe::mod_private>,
          class D3 = boost::describe::describe_members<T, boost::describe::mod_public | boost::describe::mod_protected>,
          class D4 = boost::describe::describe_members<T, boost::describe::mod_private>,
          class En = std::enable_if_t<boost::mp11::mp_empty<D2>::value && boost::mp11::mp_empty<D4>::value>>
void serialize(Archive& ar, T& t, boost::serialization::version_type)
{
    int k = 0;

    // public bases: use base_object

    boost::mp11::mp_for_each<D1>([&](auto D) {
        using B = typename decltype(D)::type;

        char name[32];
        std::sprintf(name, "base.%d", ++k);

        ar& boost::make_nvp(name, boost::serialization::base_object<B>(t));
    });

    // public (and protected) members

    boost::mp11::mp_for_each<D3>([&](auto D) { ar& boost::make_nvp(D.name, t.*D.pointer); });
}

} // namespace boost::serialization

#endif //!defined(BOOST_SERIALIZATION_DESCRIBE_HPP)