// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_SERIALIZATION_FILESYSTEM_HPP)
#define BOOST_SERIALIZATION_FILESYSTEM_HPP

#include <filesystem>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_free.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for std::runtime_error
    template <class Archive>
    void save(Archive& ar, const std::filesystem::path& p, unsigned int)
    {
        ar << p.generic_string();
    }

    template <class Archive>
    void load(Archive& ar, std::filesystem::path& p, unsigned int)
    {
        std::string path;
        ar >> path;

        p.assign(path);
    }

} // namespace serialization
} // namespace boost

// outside of any namespace
BOOST_SERIALIZATION_SPLIT_FREE(std::filesystem::path)

#endif //!defined(BOOST_SERIALIZATION_FILESYSTEM_HPP)
