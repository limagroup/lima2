// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(BOOST_SERIALIZATION_STDEXCEPT_HPP)
#define BOOST_SERIALIZATION_STDEXCEPT_HPP

#include <stdexcept>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/split_free.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for std::runtime_error
    template <class Archive>
    void save(Archive& ar, const std::runtime_error& ex, unsigned int)
    {
        // hugly
        std::string what = ex.what();
        ar << what;
    }

    template <class Archive>
    void load(Archive& ar, std::runtime_error& ex, unsigned int)
    {
        std::string what;
        ar >> what;

        ex = std::runtime_error{what};
    }

} // namespace serialization
} // namespace boost

// outside of any namespace
BOOST_SERIALIZATION_SPLIT_FREE(std::runtime_error)

#endif //!defined(BOOST_SERIALIZATION_STDEXCEPT_HPP)
