//
// Copyright 2005-2007 Adobe Systems Incorporated
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_FOR_EACH2_HPP
#define BOOST_GIL_FOR_EACH2_HPP

#include <boost/assert.hpp>

namespace boost::gil {

template <typename View1, typename View2, typename F>
F for_each_pixel(View1 const& view1, View2 const& view2, F fun)
{
    BOOST_ASSERT(view1.dimensions() == view2.dimensions());
    for (std::ptrdiff_t y = 0; y < view1.height(); ++y)
    {
        auto begin1 = view1.row_begin(y), end1 = view1.row_end(y);
        auto begin2 = view2.row_begin(y), end2 = view2.row_end(y);
        for (; begin1 != end1; ++begin1, ++begin2)
            fun(*begin1, *begin2);
    }
    return fun;
}

template <typename View1, typename View2, typename BinaryPred>
bool all_off_pixel(View1 const& view1, View2 const& view2, BinaryPred fun)
{
    BOOST_ASSERT(view1.dimensions() == view2.dimensions());
    for (std::ptrdiff_t y = 0; y < view1.height(); ++y)
    {
        auto begin1 = view1.row_begin(y), end1 = view1.row_end(y);
        auto begin2 = view2.row_begin(y), end2 = view2.row_end(y);
        for (; begin1 != end1; ++begin1, ++begin2)
            if (!fun(*begin1, *begin2))
                return false;
    }
    return true;
}

}  // namespace boost::gil

#endif // BOOST_GIL_FOR_EACH2_HPP