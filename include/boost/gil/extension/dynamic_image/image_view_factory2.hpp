//Fix for https://github.com/boostorg/gil/pull/756

#include <boost/gil/extension/dynamic_image/any_image_view.hpp>

#include <boost/gil/image_view_factory.hpp>

namespace boost { namespace gil {

// Methods for constructing any image views from other any image views
// Extends image view factory to runtime type-specified views (any_image_view)

namespace detail {

template <typename ResultView>
struct transposed_view2_fn
{
    using result_type = ResultView;

    template <typename View>
    auto operator()(View const& src) const -> result_type
    {
        return result_type{transposed_view(src)};
    }
};

} // namespace detail

/// \ingroup ImageViewTransformationsTransposed
/// \tparam Views Models Boost.MP11-compatible list of models of ImageViewConcept
template <typename ...Views>
inline
auto transposed_view2(any_image_view<Views...> const& src)
    -> typename dynamic_xy_step_transposed_type<any_image_view<Views...>>::type
{
    using result_view_t = typename dynamic_xy_step_transposed_type<any_image_view<Views...>>::type;
        return variant2::visit(detail::transposed_view2_fn<result_view_t>(), src);
}

}}  // namespace boost::gil
