//
// Copyright 2005-2007 Adobe Systems Incorporated
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt
//
#ifndef BOOST_GIL_DUAL_HPP
#define BOOST_GIL_DUAL_HPP

#include <boost/gil/metafunctions.hpp>
#include <boost/gil/planar_pixel_iterator.hpp>
#include <boost/gil/detail/mp11.hpp>

#include <cstddef>
#include <type_traits>

namespace boost { namespace gil {

/// \addtogroup ColorNameModel
/// \{

/// \brief Threshold 1
struct th1_t {};

/// \brief Threshold 2
struct th2_t {};

/// \}

/// \ingroup ColorSpaceModel
using dual_t = mp11::mp_list<th1_t, th2_t>;

/// \ingroup LayoutModel
using dual_layout_t = layout<dual_t>;

/// \ingroup ImageViewConstructors
/// \brief from raw thresholds planar data
template <typename IC>
inline auto planar_dual_view(
    std::size_t width, std::size_t height,
    IC th1, IC th2,
    std::ptrdiff_t rowsize_in_bytes)
    -> typename type_from_x_iterator<planar_pixel_iterator<IC, dual_t> >::view_t
{
    using view_t = typename type_from_x_iterator<planar_pixel_iterator<IC, dual_t>>::view_t;

    return view_t(
        width, height,
        typename view_t::locator(
            planar_pixel_iterator<IC, dual_t>(th1, th2),
            rowsize_in_bytes));
}

}}  // namespace boost::gil

#endif
