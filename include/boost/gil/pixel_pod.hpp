// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef BOOST_GIL_PIXEL_POD_HPP
#define BOOST_GIL_PIXEL_POD_HPP

#include <boost/mp11/integral.hpp>

#include <cstdint>
#include <type_traits>

namespace boost { namespace gil {

// Forward-declare gray_t
struct pod_color_t;
using pod_t = mp11::mp_list<pod_color_t>;
template <typename T> struct channel_type;
template <typename T> struct color_space_type;
template <typename T> struct channel_mapping_type;
template <typename T> struct is_planar;
template <typename T> struct num_channels;
template <typename T> struct size;
template <typename P1, typename P2> struct pixels_are_compatible;
template <typename C1, typename C2> struct default_color_converter_impl;

#define LIMA_TYPEDEF_POD_PIXEL(P)																		   \
template <> struct color_space_type<P>  { using type = pod_t;};					                           \
template <> struct channel_mapping_type<P>  { using type = mp11::mp_list<std::integral_constant<int,0>>;}; \
template <> struct channel_type<P>  { using type = P;};                                                    \
template <> struct is_planar<P> : mp11::mp_false {};													   \
template <> struct num_channels<P> : mp11::mp_int<1> {};												   \
template <> struct size<P> : mp11::mp_int<1> {};                                                           \
template <typename P2> struct pixels_are_compatible<P, P2> : std::is_same<P, P2> {};

template <>
struct default_color_converter_impl<pod_t, pod_t> {
    template <typename P1, typename P2>
    void operator()(const P1& src, P2& dst) const {
        dst = channel_convert<P2>(src);
    }
};

LIMA_TYPEDEF_POD_PIXEL(std::uint8_t)
LIMA_TYPEDEF_POD_PIXEL(std::int8_t)
LIMA_TYPEDEF_POD_PIXEL(std::uint16_t)
LIMA_TYPEDEF_POD_PIXEL(std::int16_t)
LIMA_TYPEDEF_POD_PIXEL(std::uint32_t)
LIMA_TYPEDEF_POD_PIXEL(std::int32_t)
LIMA_TYPEDEF_POD_PIXEL(float)
LIMA_TYPEDEF_POD_PIXEL(double)

}}  // namespace boost::gil

#endif
