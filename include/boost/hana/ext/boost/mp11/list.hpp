/*!
@file
Adapts `boost::mp11::list` for use with Hana.

@copyright Louis Dionne 2013-2017
Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.md or copy at http://boost.org/LICENSE_1_0.txt)
 */

#ifndef BOOST_HANA_EXT_BOOST_MP11_MP_LIST_HPP
#define BOOST_HANA_EXT_BOOST_MP11_MP_LIST_HPP

#include <boost/hana/concept/foldable.hpp>
#include <boost/hana/config.hpp>
#include <boost/hana/core/when.hpp>
#include <boost/hana/fwd/at.hpp>
#include <boost/hana/fwd/core/to.hpp>
#include <boost/hana/fwd/core/tag_of.hpp>
#include <boost/hana/fwd/drop_front.hpp>
#include <boost/hana/fwd/equal.hpp>
#include <boost/hana/fwd/is_empty.hpp>
#include <boost/hana/fwd/less.hpp>
#include <boost/hana/integral_constant.hpp>
#include <boost/hana/length.hpp>
#include <boost/hana/type.hpp>
#include <boost/hana/unpack.hpp>

#include <boost/mp11/list.hpp>

#include <cstddef>
#include <type_traits>
#include <utility>

#ifdef BOOST_HANA_DOXYGEN_INVOKED
namespace boost
{
namespace mp11
{
    //! @ingroup group-ext-mp11
    //! Adapter for Boost.MP11 lists.
    //!
    //! Conversion from any `Foldable`
    //! ------------------------------
    //! A MPL list can be created from any `Foldable`. More precisely,
    //! for a `Foldable` `xs` whose linearization is `[x1, ..., xn]`,
    //! @code
    //!     to<ext::boost::mp11::list_tag>(xs) == mp11::list<t1, ..., tn>{}
    //! @endcode
    //! where `tk` is the type of `xk`, or the type contained in `xk` if
    //! `xk` is a `hana::type`.
    //! @warning
    //! The limitations on the size of `mp11::list`s are inherited by
    //! this conversion utility, and hence trying to convert a `Foldable`
    //! containing more than [BOOST_MPL_LIMIT_LIST_SIZE][1] elements is
    //! an error.
    //! @include example/ext/boost/mp11/list/conversion.cpp
    //!
    //! [1]: http://www.boost.org/doc/libs/release/libs/mp11/doc/refmanual/limit-list-size.html
    template <typename... T>
    struct list
    {
    };
} // namespace mp11
} // namespace boost
#endif

BOOST_HANA_NAMESPACE_BEGIN
namespace ext
{
namespace boost
{
    namespace mp11
    {
        struct list_tag
        {
        };
    } // namespace mp11
} // namespace boost
} // namespace ext

namespace detail
{
template <typename T>
struct is_mp_list
{
    static const bool value = false;
};

template <typename... T>
struct is_mp_list<mp11::mp_list<T...>>
{
    static const bool value = true;
};

} //namespace detail

template <typename T>
struct tag_of<T, when<detail::is_mp_list<T>::value>>
{
    using type = ext::boost::mp11::list_tag;
};

//////////////////////////////////////////////////////////////////////////
// Conversion from a Foldable
//////////////////////////////////////////////////////////////////////////
template <typename F>
struct to_impl<ext::boost::mp11::list_tag, F, when<hana::Foldable<F>::value>>
{
    template <typename Xs>
    static constexpr decltype(auto) apply(Xs&& xs)
    {
        auto list_type = hana::unpack(static_cast<Xs&&>(xs), hana::template_<::boost::mp11::mp_list>);
        return typename decltype(list_type)::type{};
    }
};
BOOST_HANA_NAMESPACE_END

#endif // !BOOST_HANA_EXT_BOOST_MP11_MP_LIST_HPP
