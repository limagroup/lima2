// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <stdexcept>

#include <boost/exception/exception.hpp>
#include <boost/exception/info.hpp>
#include <boost/throw_exception.hpp>
#include <boost/stacktrace.hpp>

namespace boost
{
using errinfo_stacktrace = boost::error_info<struct tag_stacktrace, boost::stacktrace::stacktrace>;
};

#define LIMA_THROW_EXCEPTION(x) \
    BOOST_THROW_EXCEPTION(boost::enable_error_info(x) << boost::errinfo_stacktrace(boost::stacktrace::stacktrace()))

namespace lima
{
/// Invalid argument exception
struct invalid_argument : public std::invalid_argument, virtual boost::exception
{
    using std::invalid_argument::invalid_argument;
};

/// Defines a type of object to be thrown as exception. It reports errors that are due
/// to events beyond the scope of the program and can not be easily predicted.
struct runtime_error : public std::runtime_error, virtual boost::exception
{
    using std::runtime_error::runtime_error;
};

// IO related exceptions
struct io_error : public runtime_error
{
    using runtime_error::runtime_error;
};
struct hdf5_error : public io_error
{
    using io_error::io_error;
};
struct edf_error : public io_error
{
    using io_error::io_error;
};

// Processing exceptions
class process_error : public runtime_error
{
    using runtime_error::runtime_error;
};

class acq_buffer_overflow_error : public runtime_error
{
    using runtime_error::runtime_error;
};

class acq_frame_drop_error : public runtime_error
{
    using runtime_error::runtime_error;
};

} // namespace lima
