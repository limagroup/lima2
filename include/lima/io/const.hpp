// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

namespace lima
{
namespace io
{
    // literals namespaces are special and they are guaranteed to only contain symbols defined
    // only by the standard library so safe to bind here
    using std::string_literals::operator"" s;

    /// A default filename format string
    inline static const std::string default_filename_format =
        "{filename_prefix}_{filename_rank}_{file_number:05d}{filename_suffix}"s;

} //namespace io
} //namespace lima
