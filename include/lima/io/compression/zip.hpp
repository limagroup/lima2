// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if defined(LIMA_ENABLE_ZLIB)

#include <zlib.h>

#include <boost/exception/errinfo_errno.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

#include <lima/io/compression/buffer.hpp>

namespace lima
{
namespace io
{
    namespace comp
    {
        class zip_compression
        {
          public:
            inline static const char* description = "Zip Compression";

            zip_compression(int level = 8)
            {
                m_zstream.next_in = NULL;
                m_zstream.avail_in = 0;
                m_zstream.total_in = 0;

                m_zstream.next_out = NULL;
                m_zstream.avail_out = 0;
                m_zstream.total_out = 0;

                m_zstream.zalloc = NULL;
                m_zstream.zfree = NULL;

                int res = deflateInit(&m_zstream, level);

                if (res != Z_OK)
                    LIMA_THROW_EXCEPTION(std::runtime_error("Can't init z_stream_s struct"));
            }

            ~zip_compression() { deflateEnd(&m_zstream); }

            // Returns compression buffer size
            std::size_t bound_size(std::size_t const size, std::size_t const elem_size)
            {
                return compressBound(size * elem_size);
            }

            // Static version
            static void compression(const void* in_buffer, int size, std::vector<std::byte>& out, int level = 6)
            {
                out.resize(compressBound(size));
                uLongf in_size = size;
                const Bytef* in_data = (const Bytef*) in_buffer;
                uLongf comp_size = out.size();
                Bytef* comp_data = (unsigned char*) out.data();
                int res = compress2(comp_data, &comp_size, in_data, in_size, level);
                if (res != Z_OK)
                    LIMA_THROW_EXCEPTION(lima::runtime_error("compress2 error") << boost::errinfo_errno(res));

                out.resize(comp_size);

                LIMA_LOG(trace, io) << "GZ compression size = " << comp_size
                                    << " (ratio=" << ((double) size / (double) comp_size) << ")";
            }

            // Streaming version
            void compression(const void* in_buffer, int size, buffers_t& out_buffers)
            {
                m_zstream.next_in = (Bytef*) in_buffer;
                m_zstream.avail_in = size;

                while (m_zstream.avail_in) {
                    if (!m_zstream.avail_out) {
                        buffer& new_buffer = out_buffers.emplace_back();
                        m_zstream.next_out = (Bytef*) new_buffer.ptr.get();
                        m_zstream.avail_out = buffer::capacity;
                    }

                    int res = deflate(&m_zstream, Z_NO_FLUSH);
                    if (res != Z_OK)
                        LIMA_THROW_EXCEPTION(lima::runtime_error("deflate error") << boost::errinfo_errno(res));

                    out_buffers.back().used_size = buffer::capacity - m_zstream.avail_out;
                }
            }

            void end_compression(buffers_t& out_buffers)
            {
                int res = Z_OK;
                while (res == Z_OK) {
                    if (!m_zstream.avail_out) {
                        buffer& new_buffer = out_buffers.emplace_back();
                        m_zstream.next_out = (Bytef*) new_buffer.ptr.get();
                        m_zstream.avail_out = buffer::capacity;
                    }

                    res = deflate(&m_zstream, Z_FINISH);
                    out_buffers.back().used_size = buffer::capacity - m_zstream.avail_out;
                }
                if (res != Z_STREAM_END)
                    LIMA_THROW_EXCEPTION(lima::runtime_error("deflate error") << boost::errinfo_errno(res));
            }

          private:
            z_stream_s m_zstream;
        };

        class zip_decompression
        {
          public:
            zip_decompression()
            {
                m_zstream.next_in = NULL;
                m_zstream.avail_in = 0;
                m_zstream.total_in = 0;

                m_zstream.next_out = NULL;
                m_zstream.avail_out = 0;
                m_zstream.total_out = 0;

                m_zstream.zalloc = NULL;
                m_zstream.zfree = NULL;

                int res = inflateInit2(&m_zstream, 15);

                if (res != Z_OK)
                    LIMA_THROW_EXCEPTION(std::runtime_error("Can't init z_stream_s struct"));
            }

            ~zip_decompression() { inflateEnd(&m_zstream); }

            // Static version
            static void decompression(std::vector<std::byte> const& in, void* out_buffer, int size)
            {
                uLongf comp_size = in.size();
                Bytef* comp_data = (unsigned char*) in.data();
                uLongf out_size = size;
                Bytef* out_data = (Bytef*) out_buffer;
                int res = uncompress2(out_data, &out_size, comp_data, &comp_size);
                if (res != Z_OK)
                    LIMA_THROW_EXCEPTION(lima::runtime_error(zError(res)) << boost::errinfo_errno(res));
            }

            // Streaming version
            void decompression(buffers_t const& in_buffers, const void* out_buffer, int size)
            {
                // TODO
            }

          private:
            z_stream_s m_zstream;
        };

    } //namespace comp
} //namespace io
} //namespace lima

#endif //defined(LIMA_ENABLE_ZLIB)
