// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstdint>

#include <algorithm>
#include <memory>
#include <vector>

namespace lima
{
namespace io
{
    namespace comp
    {
        /// A buffer used for compression
        struct buffer
        {
            buffer() { ptr = std::make_unique<std::byte[]>(buffer::capacity); }

            static constexpr std::size_t capacity = 4 * 1024; // Assume 4 KB pages?
            std::size_t used_size = 0;
            std::unique_ptr<std::byte[]> ptr;
        };

        /// A vector of buffers
        using buffers_t = std::vector<buffer>;

        /// Utility function that concatenates a vector of buffers into a single buffer
        inline void concatenate(buffers_t const& in, std::vector<std::byte>& out)
        {
            std::for_each(in.begin(), in.end(),
                          [&out](auto&& buf) { out.insert(out.end(), buf.ptr.get(), buf.ptr.get() + buf.used_size); });
        }

    } //namespace comp
} //namespace io
} //namespace lima
