// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if defined(LIMA_ENABLE_BSHUF_LZ4)

#include <bitshuffle.h>

#include <boost/exception/errinfo_errno.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

extern "C" {

// Prototypes from bitshuffle_core.c
void bshuf_write_uint32_BE(void* buf, uint32_t num);
void bshuf_write_uint64_BE(void* buf, uint64_t num);
}

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

#include <lima/io/compression/buffer.hpp>

namespace lima
{
namespace io
{
    namespace comp
    {
        class bshuf_lz4_compression
        {
            inline static const char* description = "BS/LZ4 Compression";

          public:
            bshuf_lz4_compression()
            {
                LIMA_LOG(trace, io) << "BitShuffle using SSE2=" << bshuf_using_SSE2() << " AVX2=" << bshuf_using_AVX2();
            }

            // Returns compression buffer size
            std::size_t bound_size(std::size_t const size, std::size_t const elem_size)
            {
                unsigned int block_size =
                    0; //Process in blocks of this many elements. Pass 0 to select automatically(recommended).
                return bshuf_compress_lz4_bound(size, elem_size, block_size);
            }

            void compression(const void* in_buffer, int size, int depth, buffers_t& out_buffers)
            {
                unsigned int bs_block_size = 0;
                unsigned int bs_in_size = (unsigned int) (size / depth);

                buffer& new_buffer = out_buffers.emplace_back();
                char* bs_buffer = (char*) new_buffer.ptr.get();

                bshuf_write_uint64_BE(bs_buffer, size);
                bshuf_write_uint32_BE(bs_buffer + 8, bs_block_size);
                unsigned int bs_out_size =
                    bshuf_compress_lz4(in_buffer, bs_buffer + 12, bs_in_size, depth, bs_block_size);
                if (bs_out_size < 0)
                    LIMA_THROW_EXCEPTION(lima::runtime_error("BSLZ4 Compression failed")
                                         << boost::errinfo_errno(bs_out_size));
                else
                    LIMA_LOG(trace, io) << "BitShuffle Compression IN[" << size << "] OUT[" << bs_out_size << "]";

                out_buffers.back().used_size = bs_out_size + 12;
            }
        };

    } //namespace comp
} //namespace io
} //namespace lima

#endif //defined(LIMA_ENABLE_BSHUF_LZ4)
