// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cassert>

#include <atomic>
#include <filesystem>
#include <functional>
#include <memory>
#include <unordered_map>
#include <type_traits>

#include <boost/exception/info.hpp>
#include <boost/exception/errinfo_file_name.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/core/frame.hpp>
#include <lima/io/params.hpp>

namespace lima
{
namespace io
{
    inline std::filesystem::path filename(std::filesystem::path base_path, std::string filename_format,
                                          std::string filename_prefix, std::string filename_suffix, int file_number,
                                          int filename_rank)
    {
        using namespace fmt::literals;

        auto name = fmt::format(fmt::runtime(filename_format), "filename_prefix"_a = filename_prefix,
                                "filename_rank"_a = filename_rank, "file_number"_a = file_number,
                                "filename_suffix"_a = filename_suffix);
        return base_path / name;
    }

    /// An I/O Writer adapter that splits a frame sequence into multiple files
    template <typename Writer>
    class multi_writer
    {
        using writer_t = Writer;
        using params_t = typename Writer::params_t;

        // An underlying writter and a counter of the number of frames written
        struct writer_counter
        {
            writer_counter(Writer&& d) : writer(std::move(d)) {}
            writer_counter(writer_counter&& c) = default;

            std::atomic_int nb_frames_saved = 0;
            Writer writer;
        };

      public:
        template <typename... Args>
        multi_writer(params_t const& params, Args... args) : m_params(params)
        {
            if (m_params.nb_frames_per_file < 1)
                LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid nb_frames_per_file"));

            // Create the factory (bind the writer specific arguments)
            m_writer_factory = [params, args...](std::string const& filename) {
                return writer_t{filename, params, args...};
            };
        }

        ~multi_writer() { close(); }

        // Apply the function for the given frame_idx
        template <typename Callable>
        void apply(int frame_idx, Callable func)
        {
            // Check if the writer is available
            auto file_idx = frame_idx / m_params.nb_frames_per_file;
            if (!is_writer_available(file_idx))
                // Create the next writer
                open(file_idx);

            // Get the writer counter
            auto& dcounter = m_writers.at(file_idx);

            // Write the data
            func(dcounter->writer, frame_idx % m_params.nb_frames_per_file);
            //dcounter->writer.write(frame_idx % m_params.nb_frames_per_file, )

            // Increment counter
            dcounter->nb_frames_saved += m_params.nb_frames_per_chunk;
            LIMA_LOG(trace, io) << "Writer has saved " << dcounter->nb_frames_saved << " / "
                                << m_params.nb_frames_per_file << " frames";

            // Release the writer if we are done with it
            if (dcounter->nb_frames_saved >= m_params.nb_frames_per_file)
                m_writers.erase(file_idx);
        }

        /// Write a frame view
        template <typename View>
        void write_view(const View& src)
        {
            apply(src.idx(), 1, [&](auto& writer, int frame_idx) {
                // Write the data
                writer.write_view(src, frame_idx);
            });
        }

        /// Write a chunk of data
        /// \param chunk_idx is the frame idx of the first frame in the chunk
        void write_chunk(const void* chunk_data, std::size_t chunk_size, hsize_t frame_idx = 0U,
                         hsize_t channel_idx = 0U)

        {
            apply(frame_idx, [&](auto& writer, hsize_t frame_idx) {
                // Write the data
                writer.write_chunk(chunk_data, chunk_size, frame_idx, channel_idx);
            });
        }

        /// Close the writer
        void close()
        {
            // Get the writer to the latest file
            auto final_writer = std::max_element(m_writers.begin(), m_writers.end(),
                                                 [](auto lhs, auto rhs) { return lhs.first < rhs.first; });

            // Check if the writer is available
            if (final_writer != m_writers.end()) {
                auto& dcounter = final_writer->second;
                LIMA_LOG(trace, io) << "Closing I/O writer " << dcounter->writer.filename();
                // If dataset is not full
                if (dcounter->nb_frames_saved < m_params.nb_frames_per_file) {
                    // Resize it to the actual number of frames
                    LIMA_LOG(trace, io) << "Resizing dataset to " << dcounter->nb_frames_saved;
                    dcounter->writer.resize(dcounter->nb_frames_saved);
                }

                // Close writer
                m_writers.erase(final_writer);
            }
        }

      protected:
        bool is_writer_available(int file_idx) { return m_writers.find(file_idx) != m_writers.end(); }

        writer_t& get_writer(int file_idx) { return m_writers.at(file_idx)->writer; }
        writer_t const& get_writer(int file_idx) const { return m_writers.at(file_idx)->writer; }

        /// Open and initialize a file
        void open(int file_idx)
        {
            if (is_writer_available(file_idx)) {
                LIMA_LOG(warning, io) << "Unexpected I/O writer already opened";
                return;
            }

            std::filesystem::path filepath =
                filename(m_params.base_path, m_params.filename_format, m_params.filename_prefix,
                         m_params.filename_suffix, int(m_params.start_number + file_idx), m_params.filename_rank);
            LIMA_LOG(trace, io) << "Opening file " << filepath;

            // Check whether the file exists, etc
            if (std::filesystem::exists(filepath) && m_params.file_exists_policy == file_exists_policy_enum::abort)
                LIMA_THROW_EXCEPTION(io_error("File exists and policy is abort")
                                     << boost::errinfo_file_name(filepath.string()));

            // Create the writer
            m_writers.emplace(file_idx, std::make_shared<writer_counter>(m_writer_factory(filepath)));
        }

      private:
        saving_params m_params; //!< The writer parameters

        std::unordered_map<int,
                           std::shared_ptr<writer_counter>> m_writers; //!< Maps of writers indexed by file index
        std::function<writer_t(std::filesystem::path const&)> m_writer_factory;
    };

    /// An I/O Read adapter that reads a frame sequence from multiple files
    template <typename Reader>
    class multi_reader
    {
        using reader_t = Reader;
        using params_t = typename Reader::params_t;

      public:
        template <typename... Args>
        multi_reader(params_t const& params, int rank, Args... args) : m_params(params), m_rank(rank)
        {
            if (m_params.nb_frames_per_file < 1)
                LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid nb_frames_per_file"));

            // Create the factory (bind the driver specific arguments)
            m_driver_factory = [params, args...](std::string const& filename) {
                return reader_t{filename, params, args...};
            };
        }

        /// Reader dimensions
        auto dimensions(int frame_idx = 0) { return get_reader(frame_idx)->dimensions(); }

        /// Reader pixel type
        auto pixel_type(int frame_idx = 0) { return get_reader(frame_idx)->pixel_type(); }

        /// Read the next_frame
        lima::frame read_frame(int frame_idx = 0) { return get_reader(frame_idx)->read_frame(frame_idx); }

      private:
        // Settings
        params_t m_params; //!< The driver parameters
        int m_rank;        //!< The MPI rank of the process

        std::unordered_map<int, std::shared_ptr<reader_t>> m_drivers; //!< Maps of drivers indexed by file index
        std::function<reader_t(std::filesystem::path const&)> m_driver_factory;

        bool is_reader_available(int driver_idx) { return m_drivers.find(driver_idx) != m_drivers.end(); }

        std::shared_ptr<reader_t> get_reader(int frame_idx)
        {
            // Check if the driver is available
            auto file_idx = frame_idx / m_params.nb_frames_per_file;
            if (!is_reader_available(file_idx))
                // Create the next driver
                open(file_idx);

            // Get the driver
            return m_drivers.at(file_idx);
        }

        /// Open and initialize a file
        void open(int driver_idx)
        {
            if (is_reader_available(driver_idx)) {
                LIMA_LOG(warning, io) << "Unexpected I/O driver index";
                return;
            }

            std::filesystem::path filepath =
                filename(m_params.base_path, m_params.filename_format, m_params.filename_prefix,
                         m_params.filename_suffix, int(m_params.start_number + driver_idx), m_rank);
            LIMA_LOG(trace, io) << "Opening file " << filepath;

            //Check whether the file exists
            if (!std::filesystem::exists(filepath))
                LIMA_THROW_EXCEPTION(io_error("File does not exist") << boost::errinfo_file_name(filepath.string()));

            // Create the driver (should call filename.native() but HDF5 does not support unicode path yet on Windows)
            m_drivers.emplace(driver_idx, std::make_shared<reader_t>(m_driver_factory(filepath)));
        }
    };

} //namespace io
} //namespace lima
