// Copyright (C) 2024 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>

namespace boost
{
template <class Tag, class T>
class error_info;

using errinfo_base_path = error_info<struct errinfo_base_path_, std::filesystem::path>;

using errinfo_available_space = error_info<struct errinfo_available_space_, std::uintmax_t>;

using errinfo_filesystem_perms = error_info<struct errinfo_filesystem_perms_, std::string>;

} // namespace boost

namespace lima
{
namespace io
{
    /// Check file system path (exists, available space, write permissions)
    void check_base_path(std::filesystem::path const& base_path);

} // namespace io
} // namespace lima