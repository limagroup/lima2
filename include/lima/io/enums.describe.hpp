// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe.hpp>
#include <boost/describe/io_enums.hpp>

#include <lima/io/enums.hpp>

namespace lima
{
namespace io
{
    BOOST_DESCRIBE_ENUM(file_exists_policy_enum, abort, overwrite, append, multiset)

    using boost::describe::operator<<;
    using boost::describe::operator>>;

} //namespace io
} //namespace lima
