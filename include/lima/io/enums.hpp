// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace lima
{
namespace io
{
    enum class file_exists_policy_enum : int
    {
        abort,     //!< Abort acquisition if file already exist
        overwrite, //!< Overwrite old file
        append,    //!< Append new data at the end of an already existing file
        multiset   //!< Like append but doesn't use file counter
    };

} //namespace io
} //namespace lima
