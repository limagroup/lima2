// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#if _MSC_VER > 1800
#pragma once
#endif

#include <exception>
#include <iostream>
#include <sstream>
#include <fstream>
#include <filesystem>
#include <map>

#include <boost/config.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/hana/assert.hpp>
#include <boost/hana/type.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/core/point.hpp>

namespace lima
{
namespace io
{
    namespace detail
    {
        enum image_type
        {
            unknown,
            bpp8,
            bpp8s,
            bpp10,
            bpp10s,
            bpp12,
            bpp12s,
            bpp14,
            bpp14s,
            bpp16,
            bpp16s,
            bpp32,
            bpp32s,
            bpp32f,
            bpp1,
            bpp4,
            bpp6,
            bpp24,
            bpp24s
        };

        class edf_reader
        {
          public:
            edf_reader(std::filesystem::path const& filename)
            {
                // Open the EDF file
                m_file.open(filename, std::ios::binary);
                if (!m_file.is_open())
                    throw std::runtime_error("Failed to open EDF file");

                std::map<std::string, std::string> headers;
                m_data_pos = parse_edf_header(m_file, headers);

                m_dim = point<std::ptrdiff_t>(std::stoi(headers["Dim_1"]), std::stoi(headers["Dim_2"]));

                //Interpret header
                auto val = headers.find("DataType");
                if (val != headers.end())
                    m_type = image_type_from_string(val->second);
                else
                    throw std::runtime_error("Missing DataType header in EDF file");
            }

            point<std::ptrdiff_t> get_dimensions() const { return m_dim; }
            image_type get_image_type() const { return m_type; }

            template <typename View>
            void read_view(const View& view)
            {
                m_file.seekg(m_data_pos);

                std::streamsize image_size = m_dim.x * m_dim.y * sizeof(typename View::value_type);

                m_file.read(reinterpret_cast<char*>(&view[0]), image_size);
                if (m_file.fail())
                    throw std::runtime_error("Failed to read data section of EDF file");
            }

            void read_frame(frame& frm)
            {
                switch (m_type) {
                case image_type::bpp8:
                    frm.recreate(get_dimensions(), pixel_enum::gray8);
                    break;
                case image_type::bpp8s:
                    frm.recreate(get_dimensions(), pixel_enum::gray8s);
                    break;
                case image_type::bpp10:
                case image_type::bpp12:
                case image_type::bpp16:
                    frm.recreate(get_dimensions(), pixel_enum::gray16);
                    break;
                case image_type::bpp10s:
                case image_type::bpp12s:
                case image_type::bpp16s:
                    frm.recreate(get_dimensions(), pixel_enum::gray16s);
                    break;
                case image_type::bpp32:
                    frm.recreate(get_dimensions(), pixel_enum::gray32);
                    break;
                case image_type::bpp32s:
                    frm.recreate(get_dimensions(), pixel_enum::gray32s);
                case image_type::bpp32f:
                    frm.recreate(get_dimensions(), pixel_enum::gray32f);
                default:
                    LIMA_THROW_EXCEPTION(lima::edf_error("Unsupported pixel type"));
                }

                boost::variant2::visit([this](auto v) { read_view(v); }, view(frm));
            }

          protected:
            point<std::ptrdiff_t> m_dim;
            image_type m_type;

          private:
            // Parse EDF header and add the result to headers map
            static std::streamsize parse_edf_header(std::istream& input_file,
                                                    std::map<std::string, std::string>& headers)
            {
                std::stringstream ss;
                std::string buffer(512, '\0');

                input_file.read(&buffer[0], buffer.size());
                if (buffer[0] != '{')
                    throw std::runtime_error("Invalid EDF file");

                ss << buffer;

                // More buffer in header?
                while ((buffer.rfind('}') == std::string::npos) && !input_file.eof()) {
                    input_file.read(&buffer[0], buffer.size());
                    ss << buffer;
                }

                // Parse header
                while (!ss.eof()) {
                    std::string line;
                    std::getline(ss, line);

                    // Get rid of any comment
                    std::string::size_type comment = line.rfind(';');
                    if (comment != std::string::npos)
                        line.resize(comment);

                    // Tokenize
                    std::string::size_type token_pos = line.find('=');
                    if (token_pos != std::string::npos)
                        headers.insert(std::make_pair(boost::trim_copy(line.substr(0, token_pos)),
                                                      boost::trim_copy(line.substr(token_pos + 1))));
                }

                return input_file.tellg();
            }

            static image_type image_type_from_string(const std::string& type)
            {
                image_type res;

                if (type == "UnsignedByte")
                    res = image_type::bpp8;
                else if (type == "SignedByte")
                    res = image_type::bpp8s;
                else if (type == "UnsignedShort")
                    res = image_type::bpp16;
                else if (type == "SignedShort")
                    res = image_type::bpp16s;
                else if (type == "UnsignedInteger")
                    res = image_type::bpp32;
                else if (type == "SignedInteger")
                    res = image_type::bpp32s;
                else if (type == "Unsigned64")
                    res = image_type::bpp32;
                else if (type == "Signed64")
                    res = image_type::bpp32;
                else if (type == "FloatValue")
                    res = image_type::bpp32f;
                else if (type == "DoubleValue")
                    res = image_type::bpp32;
                else
                    throw std::runtime_error("Unsupported pixel type in EDF file format");

                return res;
            }

            std::ifstream m_file;
            std::ios::pos_type m_data_pos;
        };

    } //namespace detail

    /// \ingroup EDF_IO
    /// Allocates a new image whose dimensions are determined by the given edf image file, and loads the pixels into it.
    inline void edf_read_image(std::filesystem::path const& filename, frame& frm)
    {
        detail::edf_reader m(filename);
        m.read_frame(frm);
    }

} //namespace io
} //namespace lima
