// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>
#include <string>
#include <type_traits>

#include <boost/utility/base_from_member.hpp>

#include <lima/exceptions.hpp>
#include <lima/utils/lengthof.hpp>

#include <lima/io/h5/wrapper/base.hpp>
#include <lima/io/h5/wrapper/errinfo.hpp>
#include <lima/io/h5/wrapper/link.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        class file;
        class group;
        class dataset;

        /// Represents an HDF5 file
        class file : private boost::base_from_member<shared_file_hid_t>,
                     public detail::attrs_base<file>,
                     public detail::dset_base<file>
        {
            friend detail::attrs_base<file>; //gives access to protected hid

            using pbase_t = boost::base_from_member<shared_file_hid_t>;
            using attrs_base_t = detail::attrs_base<file>;

          public:
            using handle_t = shared_file_hid_t;

            file() = default;

            /// Creates a H5 file, flags determines the action if file exists:
            /// H5F_ACC_TRUNC: tructate, H5F_ACC_EXCL: raise an exception
            static file create(std::filesystem::path const& name, unsigned flags = H5F_ACC_TRUNC,
                               hid_t fcpl_id = H5P_DEFAULT, hid_t fapl_id = H5P_DEFAULT)
            {
                if (name.empty())
                    LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid empty H5 file name"));

                hid_t res = H5Fcreate(name.string().c_str(), flags, fcpl_id, fapl_id);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 File")
                                         << boost::errinfo_filesystem_path(name) << make_errinfo_stack());

                return res;
            }

            /// Opens an existing H5 file, flags determines the access mode:
            /// H5F_ACC_RDONLY / H5F_ACC_RDWR
            static file open(std::filesystem::path const& name, unsigned flags = H5F_ACC_RDONLY,
                             hid_t fapl_id = H5P_DEFAULT)
            {
                if (name.empty())
                    LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid empty H5 file name"));

                hid_t res = H5Fopen(name.string().c_str(), flags, fapl_id);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to open H5 File")
                                         << boost::errinfo_filesystem_path(name) << make_errinfo_stack());

                return res;
            }

            /// Returns true if the file is open (aka the hid is valid)
            bool is_open() const { return (bool) member; }

            /// Creates and returns a new group in the file.
            group create_group(path const& name, hid_t lcpl_id = H5P_DEFAULT, hid_t gcpl_id = H5P_DEFAULT,
                               hid_t gapl_id = H5P_DEFAULT);

            /// Opens or creates a group in the file
            group require_group(path const& name);

            operator hid_t() const { return member.get(); }

          protected:
            shared_file_hid_t hid() const { return member; }

          private:
            file(hid_t fid) : pbase_t(fid), attrs_base_t(member) {}
        };

        /// Represents an HDF5 group
        class group : private boost::base_from_member<shared_group_hid_t>,
                      public detail::attrs_base<group>,
                      public detail::dset_base<group>
        {
            friend detail::attrs_base<group>; //gives access to protected hid

            using pbase_t = boost::base_from_member<shared_group_hid_t>;
            using attrs_base_t = detail::attrs_base<group>;

          public:
            using handle_t = shared_group_hid_t;

            group() = default;

            /// Creates a new group and links it into the file
            template <typename Location>
            static group create(Location const& loc, path const& name, hid_t lcpl_id = H5P_DEFAULT,
                                hid_t gcpl_id = H5P_DEFAULT, hid_t gapl_id = H5P_DEFAULT)
            {
                hid_t res = H5Gcreate2(loc, name.c_str(), lcpl_id, gcpl_id, gapl_id);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Group")
                                         << boost::errinfo_h5_path(name) << make_errinfo_stack());

                return res;
            }

            /// Opens an existing group
            template <typename Location>
            static group open(Location const& loc, path const& name, hid_t gapl_id = H5P_DEFAULT)
            {
                hid_t res = H5Gopen2(loc, name.c_str(), gapl_id);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to open H5 Group")
                                         << boost::errinfo_h5_path(name) << make_errinfo_stack());

                return res;
            }

            /// Returns true if the group is open (aka the hid is valid)
            bool is_open() const { return (bool) member; }

            operator hid_t() const { return member.get(); }

          protected:
            shared_group_hid_t hid() const { return member; }

          private:
            group(hid_t gid) : pbase_t(gid), attrs_base_t(member) {}
        };

        /// Represents an HDF5 dataset
        class dataset : private boost::base_from_member<shared_dataset_hid_t>, public detail::attrs_base<dataset>
        {
            friend detail::attrs_base<dataset>; //gives access to protected hid

            using pbase_t = boost::base_from_member<shared_dataset_hid_t>;
            using attrs_base_t = detail::attrs_base<dataset>;

          public:
            using handle_t = shared_dataset_hid_t;

            dataset() = default;

            template <typename Location, typename Datatype>
            static dataset create(Location const& loc, path const& name, Datatype const& dtype_id,
                                  dataspace dspace = dataspace::create(), hid_t lcpl_id = H5P_DEFAULT,
                                  hid_t dcpl_id = H5P_DEFAULT, hid_t dapl_id = H5P_DEFAULT)
            {
                hid_t res = H5Dcreate2(loc, name.c_str(), dtype_id, dspace, lcpl_id, dcpl_id, dapl_id);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Dataset")
                                         << boost::errinfo_h5_path(name) << make_errinfo_stack());

                return res;
            }

            template <typename Location>
            static dataset open(Location const& loc, path const& name, hid_t dapl_id = H5P_DEFAULT)
            {
                hid_t res = H5Dopen2(loc, name.c_str(), dapl_id);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to open H5 Dataset")
                                         << boost::errinfo_h5_path(name) << make_errinfo_stack());

                return res;
            }

            /// Returns true if the dataset is open (aka the hid is valid)
            bool is_open() const { return (bool) member; }

            ///  Returns a copy of the dataspace of the dataset
            dataspace space() const { return H5Dget_space(member.get()); }

            ///  Returns the datatype of the dataset
            predef_datatype datatype() const { return H5Dget_type(member.get()); }

            /// Writes data to the dataset
            void write(const void* buffer, predef_datatype dtype, hid_t mem_dspace = H5S_ALL,
                       hid_t file_dspace = H5S_ALL, hid_t dtpl_id = H5P_DEFAULT)
            {
                herr_t res = H5Dwrite(member.get(), dtype, mem_dspace, file_dspace, dtpl_id, buffer);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to write to H5 Dataset") << make_errinfo_stack());
            }

            /// Writes chunk to the dataset
            void write_chunk(const void* buffer, uint32_t filter_mask, hsize_t offset[], size_t data_size,
                             hid_t dtpl_id = H5P_DEFAULT)
            {
                herr_t res = H5Dwrite_chunk(member.get(), dtpl_id, filter_mask, offset, data_size, buffer);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to write chunk to H5 Dataset")
                                         << make_errinfo_stack());
            }

            /// Reads from the dataset
            void read(void* buffer, predef_datatype dtype, hid_t mem_dspace = H5S_ALL, hid_t file_dspace = H5S_ALL,
                      hid_t dtpl_id = H5P_DEFAULT)
            {
                herr_t res = H5Dread(member.get(), dtype, mem_dspace, file_dspace, dtpl_id, buffer);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to read from H5 Dataset") << make_errinfo_stack());
            }

            /// Changes the sizes of a dataset's dimensions.
            herr_t extent(const hsize_t size[]) { return H5Dset_extent(member.get(), size); }

            operator hid_t() const { return member.get(); }

          protected:
            shared_dataset_hid_t hid() const { return member; }

          private:
            dataset(hid_t did) : pbase_t(did), attrs_base_t(member) {}
        };

        inline group file::create_group(path const& name, hid_t lcpl_id, hid_t gcpl_id, hid_t gapl_id)
        {
            return group::create(member.get(), name, lcpl_id, gcpl_id, gapl_id);
        }

        inline group file::require_group(path const& name)
        {
            // If the link exists
            if (link_exists(member.get(), name))
                return group::open(member.get(), name);
            else
                return group::create(member.get(), name);
        }

    } //namespace h5
} //namespace io
} //namespace lima
