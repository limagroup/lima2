// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>

#include <hdf5.h>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/io/h5/params.hpp>
#include <lima/io/h5/nexus.hpp>

namespace lima
{
namespace io::h5
{
    class reader
    {
      public:
        using dimensions_t = lima::point_t;
        using params_t = loading_params;

        reader(std::filesystem::path const& filename, params_t const& params = {}) : m_frame_slice(params.frame_slice)
        {
            bool is_nexus = (params.file_type == file_type_enum::nexus);
            const char* type_str = is_nexus ? "Nexus" : "HDF5";
            LIMA_LOG(trace, io) << "Constructing " << type_str << " reader for file " << filename;

            // Open the HDF5 file
            const char* nx_class = is_nexus ? "NXroot" : nullptr;
            auto f = nx::file::open(filename, nx_class);

            // Get Dataset
            auto&& dataset_path = params.dataset_path;
            m_dset = dataset::open(f, dataset_path);

            // Query dataspace dimensions
            auto dspace = m_dset.space();
            m_nb_dims = dspace.ndims();
            if ((m_nb_dims < 2) || (m_nb_dims > 4))
                LIMA_THROW_EXCEPTION(lima::hdf5_error("Invalid H5 dataset dimensions")
                                     << boost::errinfo_h5_path(dataset_path));

            hsize_t dims[] = {0, 0, 0, 0};
            if (dspace.dims(dims, nullptr) < 0)
                LIMA_THROW_EXCEPTION(lima::hdf5_error("Error querying H5 dataset dimensions")
                                     << boost::errinfo_h5_path(dataset_path));

            m_frame_slice.count = m_frame_slice.calc_count(dims[0]);

            if (params.nb_frames_per_file != 0) {
                if (params.nb_frames_per_file > m_frame_slice.count)
                    LIMA_THROW_EXCEPTION(lima::invalid_argument("Reader frame_slice.count / frames_per_file mismatch"));
                m_frame_slice.count = std::min(params.nb_frames_per_file, m_frame_slice.count);
            }

            m_frame_dims = dimensions_t(dims[m_nb_dims - 1], dims[m_nb_dims - 2]);

            // Query dataset type
            m_pixel = m_dset.datatype().pixel_type();
        }

        reader(reader const&) = default;
        reader& operator=(reader const&) = default;

        reader(reader&& w) = default;

        dimensions_t dimensions() const { return m_frame_dims; }

        pixel_enum pixel_type() const { return m_pixel; }

        // Read a frame
        frame read_frame(hsize_t frame_idx = 0)
        {
            frame out(m_frame_dims, m_pixel);
            read_view(view(out), frame_idx);
            return out;
        }

        template <typename... Views>
        void read_view(frame_view<Views...> const& v, hsize_t frame_idx = 0)
        {
            boost::variant2::visit([&](auto&& view) { this->read_view(view, frame_idx); }, v);
        }

        template <typename View>
        void read_view(View const& v, hsize_t frame_idx = 0)
        {
            if (frame_idx >= m_frame_slice.count)
                LIMA_THROW_EXCEPTION(lima::hdf5_error("Frame number out of bound") << boost::errinfo_h5_idx(frame_idx));
            else if (v.dimensions() != m_frame_dims)
                LIMA_THROW_EXCEPTION(lima::hdf5_error("Frame dimensions mismatch"));

            dataspace mem_space = m_dset.space();
            dataspace file_space = m_dset.space();
            if (m_nb_dims == 3) {
                hsize_t mem_offset[] = {0, 0, 0};
                hsize_t file_idx = frame_idx * m_frame_slice.stride + m_frame_slice.start;
                hsize_t file_offset[] = {file_idx, 0, 0};
                hsize_t count[] = {1, 1, 1};
                hsize_t block[] = {1, hsize_t(m_frame_dims.y), hsize_t(m_frame_dims.x)};
                mem_space.select_hyperslab(H5S_SELECT_SET, mem_offset, nullptr, count, block);
                file_space.select_hyperslab(H5S_SELECT_SET, file_offset, nullptr, count, block);
            }

            auto dtype = predef_datatype::create<typename View::value_type>();
            m_dset.read(&v(0, 0), dtype, mem_space, file_space);
        }

      protected:
        dataset m_dset; //!< The dataset

        dimensions_t m_frame_dims;      //!< The dimensions of data frame
        hsize_t m_nb_dims;              //!< The number of dimensions of the dataset
        file_frame_slice m_frame_slice; //!< The frame slice in the dataset
        pixel_enum m_pixel;             //!< The pixel type
    };

} //namespace io::h5
} //namespace lima
