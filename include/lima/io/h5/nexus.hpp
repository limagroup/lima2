// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/describe/io_enums.hpp>

#include <fmt/format.h>

#include <lima/core/enums.describe.hpp>
#include <lima/core/frame_info.hpp>

#include <lima/hw/info.hpp>
#include <lima/hw/params.hpp>

#include <lima/io/h5/wrapper.hpp>
#include <lima/io/h5/params.hpp>

namespace lima
{
namespace io::h5::nx
{
    namespace detail
    {
        // Returns the current universal time formated in ISO
        inline std::string format_current_time()
        {
            // C++20 has std::chrono::utc_clock but for now
            using namespace boost::posix_time;
            ptime t = microsec_clock::universal_time();
            return to_iso_extended_string(t) + 'Z';
        }

    } // namespace detail

    class group;

    class file : public h5::file
    {
      public:
        file() = default;

        /// Construct a NEXUS file
        static file create(std::filesystem::path const& name, const char* nx_class = "NXroot",
                           h5::path nx_default = "/entry_0000", unsigned flags = H5F_ACC_TRUNC,
                           hid_t fcpl_id = H5P_DEFAULT, hid_t fapl_id = H5P_DEFAULT)
        {
            return file(h5::file::create(name, flags, fcpl_id, fapl_id), name, nx_class, nx_default);
        }

        /// Opens an existing NEXUS file
        static file open(std::filesystem::path const& name, const char* nx_class = "NXroot",
                         unsigned flags = H5F_ACC_RDONLY, hid_t fapl_id = H5P_DEFAULT)
        {
            return file(h5::file::open(name, flags, fapl_id), nx_class);
        }

        /// Construct a NEXUS entry
        group create_entry(h5::path entry_name, h5::path instrument_name, h5::path detector_name);

        /// Finalize a NEXUS entry
        void finalize_entry(h5::path entry_name);

        /// Returns the latest NXentry in the file
        int last_entry()
        {
            int res = 0;

            while (H5Lexists(hid().get(), fmt::format("entry_{:04d}", res).c_str(), H5P_DEFAULT) > 0)
                res++;

            return res;
        }

      private:
        file(h5::file h5_file, std::filesystem::path const& name, const char* nx_class, h5::path nx_default) :
            h5::file(std::move(h5_file))
        {
            using namespace detail;
            using namespace std::string_literals;

            attrs.create("NX_class", nx_class);
            attrs.create("creator", "LIMA2-1.0.0"s);
            attrs.create("default", nx_default);
            attrs.create("file_name", name.c_str());
            attrs.create("file_time", format_current_time());
        }

        file(h5::file h5_file, const char* nx_class) : h5::file(std::move(h5_file))
        {
            if (nx_class && nx_class[0] && (attrs["NX_class"].string() != nx_class))
                LIMA_THROW_EXCEPTION(lima::io_error("HDF5 NX_class Attribute mismatch"));
        }
    };

    class group : public h5::group
    {
      public:
        template <typename Location>
        static group create(Location const& loc, h5::path const& name, const char* nx_class,
                            hid_t lcpl_id = H5P_DEFAULT, hid_t gcpl_id = H5P_DEFAULT, hid_t gapl_id = H5P_DEFAULT)
        {
            return group(h5::group::create(loc, name.c_str(), lcpl_id, gcpl_id, gapl_id), nx_class);
        }

        template <typename Location>
        static group open(Location const& loc, h5::path const& name, hid_t gapl_id = H5P_DEFAULT)
        {
            return group(h5::group::open(loc, name.c_str(), gapl_id));
        }

      private:
        group(h5::group h5_group, const char* nx_class) : h5::group(std::move(h5_group))
        {
            attrs.create("NX_class", nx_class);
        }

        group(h5::group h5_group) : h5::group(std::move(h5_group)) {}
    };

    inline group file::create_entry(h5::path entry_name, h5::path instrument_name, h5::path detector_name)
    {
        using namespace detail;
        using namespace std::string_literals;
        using namespace h5::path_literals;

        herr_t res;

        // Create the entry group
        auto entry = nx::group::create(hid().get(), entry_name, "NXentry");
        entry.create_dataset("title", "Lima 2D detector acquisition"s);
        entry.create_dataset("start_time", format_current_time());
        entry.create_dataset("program_name", "Lima2"s);

        // Add attribute "default" for path to the NXdata
        entry.attrs.create("default", instrument_name / detector_name / "plot"_p);

        // Create beamline/instrument group
        auto instrument = nx::group::create(entry, instrument_name, "NXinstrument");
        instrument.attrs.create("default", detector_name / "plot"_p);

        // Create detector group
        auto detector = nx::group::create(instrument, detector_name, "NXdetector");
        detector.attrs.create("default", "plot"s);

        return entry;
    }

    inline void file::finalize_entry(h5::path entry_name)
    {
        using namespace detail;

        h5::group entry = require_group(entry_name);
        entry.create_dataset("end_time", format_current_time());
    }

} // namespace io::h5::nx
} //namespace lima
