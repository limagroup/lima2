// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe.hpp>
#include <boost/describe/io_enums.hpp>

#include <lima/io/h5/enums.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        BOOST_DESCRIBE_ENUM(compression_enum, none, zip, bshuf_lz4)

        BOOST_DESCRIBE_ENUM(file_type_enum, hdf5, nexus)

        using boost::describe::operator<<;
        using boost::describe::operator>>;

    } //namespace h5
} //namespace io
} //namespace lima
