// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/io/params.describe.hpp>
#include <lima/io/h5/enums.describe.hpp>
#include <lima/io/h5/params.hpp>

#include <boost/describe/annotations.hpp>

namespace lima
{
namespace io::h5
{
    BOOST_DESCRIBE_STRUCT(saving_params, (lima::io::saving_params),
                          (compression, nx_entry_name, nx_instrument_name, nx_detector_name))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(saving_params, compression,
        (desc, "compression filter"),
        (doc, "The compression filter [none, zip, bshuf_lz4]"))

    BOOST_ANNOTATE_MEMBER(saving_params, nx_entry_name,
        (desc, "nx entry name"),
        (doc, "The NX entry name"))

    BOOST_ANNOTATE_MEMBER(saving_params, nx_instrument_name,
        (desc, "nx instrument name"),
        (doc, "The NX instrument name"))

    BOOST_ANNOTATE_MEMBER(saving_params, nx_detector_name,
        (desc, "nx detector name"),
        (doc, "The NX detector name"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(loading_params, (lima::io::loading_params), (file_type, dataset_path))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(loading_params, file_type,
        (desc, "file type [hdf5, nexus]"),
        (doc, "The expected type format to load"))

    BOOST_ANNOTATE_MEMBER(loading_params, dataset_path,
        (desc, "dataset_path"),
        (doc, "The HDF5 path of the dataset"))
    // clang-format on

} //namespace io::h5
} //namespace lima
