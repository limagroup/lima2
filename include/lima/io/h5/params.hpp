// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/io/params.hpp>
#include <lima/io/h5/enums.hpp>

namespace lima
{
namespace io::h5
{
    struct saving_params : io::saving_params
    {
        compression_enum compression = compression_enum::none;

        std::string nx_entry_name = "/entry_0000";
        std::string nx_instrument_name = "ESRF-ID00";
        std::string nx_detector_name = "Unknown";
    };

    struct loading_params : io::loading_params
    {
        file_type_enum file_type = file_type_enum::nexus;
        std::string dataset_path = "/entry_0000/measurement/data"s;
    };

} //namespace io::h5
} //namespace lima
