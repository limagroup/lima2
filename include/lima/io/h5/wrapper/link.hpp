// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/io/h5/wrapper/path.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        // Determine whether a link with the specified name exists in a group
        template <typename Location>
        inline bool link_exists(Location link_loc, path name, hid_t lapl_id = H5P_DEFAULT)
        {
            return H5Lexists(link_loc, name.c_str(), lapl_id) > 0;
        }

        /// Creates a soft link to an object (allowed to dangle)
        template <typename Location>
        inline herr_t link_soft(path target_path, Location link_loc, path link_name, hid_t lcpl_id = H5P_DEFAULT,
                                hid_t lapl_id = H5P_DEFAULT)
        {
            return H5Lcreate_soft(target_path.c_str(), link_loc, link_name.c_str(), lcpl_id, lapl_id);
        }

        /// Creates a hard link to an object
        template <typename Location1, typename Location2>
        inline herr_t link_hard(Location1 obj_loc, path obj_name, Location2 link_loc, path link_name,
                                hid_t lcpl_id = H5P_DEFAULT, hid_t lapl_id = H5P_DEFAULT)
        {
            return H5Lcreate_hard(obj_loc, obj_name.c_str(), link_loc, link_name.c_str(), lcpl_id, lapl_id);
        }

    } //namespace h5
} //namespace io
} //namespace lima
