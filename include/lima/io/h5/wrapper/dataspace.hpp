// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>
#include <type_traits>

#include <hdf5.h>

#include <lima/exceptions.hpp>

#include <lima/io/h5/wrapper/errinfo.hpp>
#include <lima/io/h5/wrapper/errstack.hpp>
#include <lima/io/h5/wrapper/handle.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        struct dataspace
        {
            dataspace(hid_t hid) : m_hid(hid) {}

            /// Create a dataspace from a given class (H5S_xxxx)
            static dataspace create(H5S_class_t type = H5S_SCALAR)
            {
                hid_t res = H5Screate(type);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Dataspace") << make_errinfo_stack());
                return res;
            }

            /// Create a simple dataspace and opens it for access
            static dataspace create_simple(int rank, const hsize_t dims[], const hsize_t maxdims[] = NULL)
            {
                hid_t res = H5Screate_simple(rank, dims, maxdims);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Dataspace") << make_errinfo_stack());
                return res;
            }

            /// Determines the dimensionality of a dataspace.
            int ndims() { return H5Sget_simple_extent_ndims(m_hid.get()); }

            /// Retrieves dataspace dimension size and maximum size.
            int dims(hsize_t dims[], hsize_t maxdims[])
            {
                return H5Sget_simple_extent_dims(m_hid.get(), dims, maxdims);
            }

            /// Selects a hyperslab of a dataspace.
            herr_t select_hyperslab(H5S_seloper_t op, const hsize_t start[], const hsize_t stride[],
                                    const hsize_t count[], const hsize_t block[])
            {
                return H5Sselect_hyperslab(m_hid.get(), op, start, stride, count, block);
            }

            operator hid_t() const { return m_hid.get(); }

            shared_dataspace_hid_t m_hid;
        };

    } //namespace h5
} //namespace io
} //namespace lima
