// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>
#include <type_traits>

#include <hdf5.h>

#include <lima/exceptions.hpp>

#include <lima/io/h5/wrapper/errinfo.hpp>
#include <lima/io/h5/wrapper/errstack.hpp>
#include <lima/io/h5/wrapper/handle.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        struct prop_list
        {
            prop_list(hid_t hid) : m_hid(hid) {}

            /// Create property list from a class type (H5P_xxxx_yyyy)
            static prop_list create(hid_t cls_id)
            {
                hid_t res = H5Pcreate(cls_id);
                if (res < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Property List") << make_errinfo_stack());
                return res;
            }

            void set_deflate(uint level)
            {
                if (H5Pset_deflate(m_hid.get(), level) < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to set H5 Property List deflate level")
                                         << make_errinfo_stack());
            }

            void set_filter(H5Z_filter_t filter_id, unsigned int flags, size_t cd_nelmts,
                            const unsigned int cd_values[])
            {
                if (H5Pset_filter(m_hid.get(), filter_id, flags, cd_nelmts, cd_values) < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to set H5 Property List filter")
                                         << make_errinfo_stack());
            }

            void set_chunk(int ndims, const hsize_t dim[])
            {
                if (H5Pset_chunk(m_hid.get(), ndims, dim) < 0)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to set H5 Property List chunk")
                                         << make_errinfo_stack());
            }

            operator hid_t() const { return m_hid.get(); }

            shared_prop_list_hid_t m_hid;
        };

    } //namespace h5
} //namespace io
} //namespace lima
