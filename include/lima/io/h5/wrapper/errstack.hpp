// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <sstream>

#include <lima/io/h5/wrapper/errinfo.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        //inline herr_t walk_stack(unsigned, const H5E_error2_t* err_desc, void* client_data)
        //{
        //    std::ostringstream& os = *static_cast<std::ostringstream*>(client_data);

        //    const char* major_err = H5Eget_major(err_desc->maj_num);
        //    const char* minor_err = H5Eget_minor(err_desc->min_num);

        //    os << '(' << major_err << ") " << minor_err << std::endl;

        //    return 0;
        //}

        /// Returns an errinfo object with the current HDF5 error stack
        inline boost::errinfo_h5_stack make_errinfo_stack()
        {
            std::ostringstream os;

            hid_t err_stack = H5Eget_current_stack();
            if (err_stack >= 0) {
                std::ostringstream os;

                H5Ewalk2(
                    err_stack, H5E_WALK_UPWARD,
                    [](unsigned, const H5E_error2_t* err_desc, void* client_data) -> herr_t {
                        std::ostringstream& os = *static_cast<std::ostringstream*>(client_data);

                        const char* major_err = H5Eget_major(err_desc->maj_num);
                        const char* minor_err = H5Eget_minor(err_desc->min_num);

                        os << '(' << major_err << ") " << minor_err << std::endl;

                        return 0;
                    },
                    &os);
                H5Eclear2(err_stack);
            }

            return os.str();
        }

        /// Utility class to disable HDF5 stack printing inside a scope.
        class silence_stack
        {
          public:
            inline silence_stack(bool enable = true) : _client_data(nullptr)
            {
                H5Eget_auto2(H5E_DEFAULT, &_func, &_client_data);
                if (enable)
                    H5Eset_auto2(H5E_DEFAULT, NULL, NULL);
            }

            inline ~silence_stack() { H5Eset_auto2(H5E_DEFAULT, _func, _client_data); }

          private:
            H5E_auto2_t _func;
            void* _client_data;
        };

    } // namespace h5
} // namespace io
} // namespace lima