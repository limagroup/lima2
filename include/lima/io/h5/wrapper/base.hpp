// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

#include <lima/exceptions.hpp>

#include <lima/io/h5/wrapper/errinfo.hpp>
#include <lima/io/h5/wrapper/errstack.hpp>
#include <lima/io/h5/wrapper/handle.hpp>
#include <lima/io/h5/wrapper/dataspace.hpp>
#include <lima/io/h5/wrapper/datatype.hpp>
#include <lima/io/h5/wrapper/path.hpp>
#include <lima/io/h5/wrapper/prop_list.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        class file;
        class group;
        class dataset;

        namespace detail
        {
            // clang-format off
            template <typename T> struct handle_traits;
            template <> struct handle_traits<file> { using handle_t = shared_file_hid_t; };
            template <> struct handle_traits<group> { using handle_t = shared_group_hid_t; };
            template <> struct handle_traits<dataset> { using handle_t = shared_dataset_hid_t; };
            // clang-format on

            /// Attribute provider common to file, group and dataset
            template <typename Derived>
            struct attrs_base
            {
                using handle_t = typename handle_traits<Derived>::handle_t;

                attrs_base() = default;
                attrs_base(handle_t hid) : attrs(hid) {}

                class attrs_
                {
                  private:
                    struct attr_proxy
                    {
                        template <typename T>
                        attr_proxy& operator=(T const& attr_data)
                        {
                            predef_datatype dtype = predef_datatype::create<T>();

                            // Write the attribute data
                            herr_t res = H5Awrite(m_hid.get(), dtype, &attr_data);
                            if (res < 0)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to get H5 Attribute info")
                                                     << make_errinfo_stack());

                            return *this;
                        }

                        attr_proxy& operator=(std::string const& attr_data)
                        {
                            string_datatype dtype;
                            const char* str = attr_data.c_str();

                            // Write the attribute data
                            herr_t res = H5Awrite(m_hid.get(), dtype, &str);
                            if (res < 0)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to get H5 Attribute info")
                                                     << make_errinfo_stack());

                            return *this;
                        }

                        std::string string() const
                        {
                            H5A_info_t attr_info;

                            // Get the attribute info
                            if (H5Aget_info(m_hid.get(), &attr_info) < 0)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to get H5 Attribute info")
                                                     << make_errinfo_stack());

                            // Add one to fit NULL-terminator
                            char* str = nullptr;
                            string_datatype dtype;

                            // Read the attribute data
                            if (H5Aread(m_hid.get(), dtype, &str) < 0)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to read H5 Attribute string data")
                                                     << make_errinfo_stack());

                            std::string res(str);
                            free(str);

                            return res;
                        }

                        shared_attribute_hid_t m_hid;
                    };

                  public:
                    attrs_() = default;
                    attrs_(handle_t hid) : m_hid(hid)
                    { /*assert(hid);*/
                    }

                    template <typename T>
                    void create(const char* attr_name, T const& attr_data)
                    {
                        if (H5Aexists(m_hid.get(), attr_name) == 0) {
                            predef_datatype dtype = predef_datatype::create<T>();
                            auto dspace = dataspace::create();
                            // Create an attribute
                            unique_attribute_hid_t attribute_id(
                                H5Acreate2(m_hid.get(), attr_name, dtype, dspace, H5P_DEFAULT, H5P_DEFAULT));
                            if (!attribute_id)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Attribute")
                                                     << make_errinfo_stack());

                            // Write the attribute data
                            herr_t res = H5Awrite(attribute_id.get(), dtype, &attr_data);
                            if (res < 0)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to write H5 Attribute")
                                                     << make_errinfo_stack());
                        } else
                            LIMA_THROW_EXCEPTION(lima::hdf5_error("H5 Attribute already exists")
                                                 << make_errinfo_stack());
                    }

                    inline void create(const char* attr_name, const char* const attr_data)
                    {
                        if (H5Aexists(m_hid.get(), attr_name) == 0) {
                            string_datatype dtype;
                            auto dspace = dataspace::create();
                            // Create an attribute
                            unique_attribute_hid_t attribute_id(
                                H5Acreate2(m_hid.get(), attr_name, dtype, dspace, H5P_DEFAULT, H5P_DEFAULT));
                            if (!attribute_id)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Attribute")
                                                     << make_errinfo_stack());

                            // Write the attribute data
                            herr_t res = H5Awrite(attribute_id.get(), dtype, &attr_data);
                            if (res < 0)
                                LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to write H5 Attribute")
                                                     << make_errinfo_stack());
                        } else
                            LIMA_THROW_EXCEPTION(lima::hdf5_error("H5 Attribute already exists")
                                                 << make_errinfo_stack());
                    }

                    inline void create(const char* attr_name, std::string const& attr_data)
                    {
                        create(attr_name, attr_data.c_str());
                    }

                    inline void create(const char* attr_name, path const& attr_data)
                    {
                        create(attr_name, attr_data.c_str());
                    }

                    // Access the attribute by name
                    attr_proxy operator[](const char* attr_name) const
                    {
                        shared_attribute_hid_t attribute_id(H5Aopen(m_hid.get(), attr_name, H5P_DEFAULT));

                        return attr_proxy{attribute_id};
                    }

                  private:
                    handle_t m_hid;

                } attrs;

                auto impl() const { return static_cast<Derived const*>(this); }
            };

            /// Dataset provider common to file and group
            template <typename Derived>
            struct dset_base
            {
                template <typename T>
                void create_dataset(path ds_name, T const& ds_data)
                {
                    if (H5Lexists(impl(), ds_name.c_str(), H5P_DEFAULT) == 0) {
                        // Create a dataset
                        predef_datatype dtype = predef_datatype::create<T>();
                        auto dspace = dataspace::create();
                        unique_dataset_hid_t dataset_id(
                            H5Dcreate2(impl(), ds_name.c_str(), dtype, dspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT));
                        if (!dataset_id)
                            LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Dataset")
                                                 << make_errinfo_stack());

                        // Write the scalar dataset data
                        herr_t res = H5Dwrite(dataset_id.get(), dtype, dspace, dspace, H5P_DEFAULT, &ds_data);
                        if (res < 0)
                            LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to write H5 Dataset")
                                                 << make_errinfo_stack());
                    } else
                        LIMA_THROW_EXCEPTION(lima::hdf5_error("H5 Dataset already exists") << make_errinfo_stack());
                }

                void create_dataset(path ds_name, const char* const ds_data)
                {
                    if (H5Lexists(impl(), ds_name.c_str(), H5P_DEFAULT) == 0) {
                        // Create a dataset
                        string_datatype dtype;
                        auto dspace = dataspace::create();
                        unique_dataset_hid_t dataset_id(
                            H5Dcreate2(impl(), ds_name.c_str(), dtype, dspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT));
                        if (!dataset_id)
                            LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to create H5 Dataset")
                                                 << make_errinfo_stack());

                        // Write the scalar dataset data
                        herr_t res = H5Dwrite(dataset_id.get(), dtype, dspace, dspace, H5P_DEFAULT, &ds_data);
                        if (res < 0)
                            LIMA_THROW_EXCEPTION(lima::hdf5_error("Failed to write H5 Dataset")
                                                 << make_errinfo_stack());
                    } else
                        LIMA_THROW_EXCEPTION(lima::hdf5_error("H5 Dataset already exists") << make_errinfo_stack());
                }

                void create_dataset(path ds_name, std::string const& ds_data)
                {
                    create_dataset(ds_name, ds_data.c_str());
                }

                struct dset_proxy
                {
                    dset_proxy& operator=(std::string const& attr_data)
                    {
                        //m_base->create(attr_name, attr_data);
                        return *this;
                    }

                    template <typename T>
                    dset_proxy& operator=(T const& attr_data)
                    {
                        //m_base->create(attr_name, attr_data);
                        return *this;
                    }

                    shared_dataset_hid_t m_hid;
                };

                //dset_proxy operator[](const char* dset_name) { return dset_proxy{m_base, dset_name}; }

              private:
                auto impl() const { return static_cast<Derived const&>(*this); }
            };

        } //namespace detail
    }     //namespace h5
} //namespace io
} //namespace lima
