// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>
#include <type_traits>

namespace lima
{
namespace io
{
    namespace h5
    {
        namespace path_traits
        {
            template <typename T>
            struct is_pathable
            {
                static const bool value = false;
            };

            template <>
            struct is_pathable<char*>
            {
                static const bool value = true;
            };
            template <>
            struct is_pathable<const char*>
            {
                static const bool value = true;
            };
            template <>
            struct is_pathable<std::string>
            {
                static const bool value = true;
            };

            template <typename T>
            inline constexpr bool is_pathable_v = is_pathable<T>::value;

            template <typename Container>
            inline bool empty(const Container& c)
            {
                return c.begin() == c.end();
            }

            template <typename T>
            inline bool empty(T* const& c_str)
            {
                BOOST_ASSERT(c_str);
                return !*c_str;
            }

            template <typename T, size_t N>
            inline bool empty(T (&x)[N])
            {
                return !x[0];
            }
        } // namespace path_traits

        class path
        {
          public:
            static const char separator = '/';

            using value_type = char;
            using string_type = std::string;

            using const_iterator = string_type::const_iterator;

            // Contructors
            path() noexcept {}
            path(const path& p) : m_path(p.m_path) {}
            template <typename Source, typename = std::enable_if_t<path_traits::is_pathable_v<std::decay_t<Source>>>>
            path(Source const& s) : m_path(s)
            {
            }

            auto c_str() const { return m_path.c_str(); }

            //path(const value_type* s) : m_path(s) {}
            //path(value_type* s) : m_path(s) {}
            //path(const string_type& s) : m_path(s) {}
            //path(string_type& s) : m_path(s) {}

            path(path&& p) noexcept : m_path(std::move(p.m_path)) {}

            // Assignement
            path& operator=(path&& p) noexcept
            {
                m_path = std::move(p.m_path);
                return *this;
            }

            path& operator=(const path& p)
            {
                m_path = p.m_path;
                return *this;
            }

            // Swap
            void swap(path& rhs) noexcept { m_path.swap(rhs.m_path); }

            // Append
            path& operator/=(path const& rhs) { return append(rhs.m_path); }

            template <typename Source, typename = std::enable_if_t<path_traits::is_pathable_v<std::decay_t<Source>>>>
            path& operator/=(Source const& source)
            {
                return append(source);
            }

            // Query
            bool empty() const noexcept { return m_path.empty(); }

            bool is_relative() const { return !is_absolute(); }
            bool is_absolute() const { return !empty() && m_path[0] == separator; }

            // Compare
            int compare(path const& p) const noexcept
            {
                return std::lexicographical_compare(begin(), end(), p.begin(), p.end());
            }
            int compare(std::string const& s) const { return compare(path(s)); }
            int compare(const value_type* s) const { return compare(path(s)); }

            // Iterators
            const_iterator begin() const { return m_path.begin(); }
            const_iterator end() const { return m_path.begin(); }

          private:
            std::string m_path;

            template <class Source>
            path& append(Source const& source)
            {
                if (path_traits::empty(source))
                    return *this;

                string_type::size_type sep_pos(append_separator_if_needed());
                m_path += source;
                if (sep_pos)
                    erase_redundant_separator(sep_pos);
                return *this;
            }

            string_type::size_type append_separator_if_needed()
            {
                if (!m_path.empty() && !is_separator(*(m_path.end() - 1))) {
                    string_type::size_type tmp(m_path.size());
                    m_path += separator;
                    return tmp;
                }
                return 0;
            }

            void erase_redundant_separator(string_type::size_type sep_pos)
            {
                if (sep_pos                              // a separator was added
                    && sep_pos < m_path.size()           // and something was appended
                    && (m_path[sep_pos + 1] == separator // and it was also separator
                        )) {
                    m_path.erase(m_path.begin() + sep_pos); // erase the added separator
                }
            }

            bool is_separator(char c) { return c == separator; }
        };

        // Append operator
        inline path operator/(const path& lhs, const path& rhs)
        {
            path p = lhs;
            p /= rhs;
            return p;
        }

        inline path operator/(path&& lhs, const path& rhs)
        {
            lhs /= rhs;
            return std::move(lhs);
        }

        // Comparison operators
        inline bool operator==(path const& lhs, path const& rhs) { return lhs.compare(rhs) == 0; }
        inline bool operator==(path const& lhs, path::string_type const& rhs) { return lhs.compare(rhs) == 0; }
        inline bool operator==(path::string_type const& lhs, path const& rhs) { return rhs.compare(lhs) == 0; }
        inline bool operator==(path const& lhs, const path::value_type* rhs) { return lhs.compare(rhs) == 0; }
        inline bool operator==(const path::value_type* lhs, path const& rhs) { return rhs.compare(lhs) == 0; }

        inline bool operator!=(path const& lhs, path const& rhs) { return lhs.compare(rhs) != 0; }
        inline bool operator!=(path const& lhs, path::string_type const& rhs) { return lhs.compare(rhs) != 0; }
        inline bool operator!=(path::string_type const& lhs, path const& rhs) { return rhs.compare(lhs) != 0; }
        inline bool operator!=(path const& lhs, const path::value_type* rhs) { return lhs.compare(rhs) != 0; }
        inline bool operator!=(const path::value_type* lhs, path const& rhs) { return rhs.compare(lhs) != 0; }

        inline bool operator<(path const& lhs, path const& rhs) { return lhs.compare(rhs) < 0; }
        inline bool operator<=(path const& lhs, path const& rhs) { return !(rhs < lhs); }
        inline bool operator>(path const& lhs, path const& rhs) { return rhs < lhs; }
        inline bool operator>=(path const& lhs, path const& rhs) { return !(lhs < rhs); }

        // Swap
        inline void swap(path& lhs, path& rhs) noexcept { lhs.swap(rhs); }

        // Literals
        namespace path_literals
        {
            inline path operator"" _p(const char* str, std::size_t) { return path{str}; }
        } // namespace path_literals

    } //namespace h5
} //namespace io
} //namespace lima
