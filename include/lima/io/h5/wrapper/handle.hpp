// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <hdf5.h>

#include <lima/utils/smart_handle.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        using unique_file_hid_t = unique_handle<hid_t, decltype(&::H5Fclose), ::H5Fclose, raii::not_negative>;
        using unique_dataset_hid_t = unique_handle<hid_t, decltype(&::H5Dclose), ::H5Dclose, raii::not_negative>;
        using unique_dataspace_hid_t = unique_handle<hid_t, decltype(&::H5Sclose), ::H5Sclose, raii::not_negative>;
        using unique_group_hid_t = unique_handle<hid_t, decltype(&::H5Gclose), ::H5Gclose, raii::not_negative>;
        using unique_attribute_hid_t = unique_handle<hid_t, decltype(&::H5Aclose), ::H5Aclose, raii::not_negative>;
        using unique_type_hid_t = unique_handle<hid_t, decltype(&::H5Tclose), ::H5Tclose, raii::not_negative>;
        using unique_prop_list_hid_t = unique_handle<hid_t, decltype(&::H5Pclose), ::H5Pclose, raii::not_negative>;

        using shared_file_hid_t = shared_handle<unique_file_hid_t>;
        using shared_dataset_hid_t = shared_handle<unique_dataset_hid_t>;
        using shared_dataspace_hid_t = shared_handle<unique_dataspace_hid_t>;
        using shared_group_hid_t = shared_handle<unique_group_hid_t>;
        using shared_attribute_hid_t = shared_handle<unique_attribute_hid_t>;
        using shared_type_hid_t = shared_handle<unique_type_hid_t>;
        using shared_prop_list_hid_t = shared_handle<unique_prop_list_hid_t>;

    } //namespace h5
} //namespace io
} //namespace lima
