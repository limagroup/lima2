// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <sstream>

#include <boost/exception/errinfo_errno.hpp>
#include <boost/exception/errinfo_file_name.hpp>
#include <boost/exception/errinfo_filesystem_path.hpp>

#include <lima/io/h5/wrapper/path.hpp>

namespace boost
{
using errinfo_h5_idx = error_info<struct errinfo_h5_idx_, hsize_t>;
using errinfo_h5_path = error_info<struct errinfo_h5_path_, lima::io::h5::path>;
using errinfo_h5_stack = error_info<struct errinfo_h5_stack_, std::string>;

} // namespace boost
