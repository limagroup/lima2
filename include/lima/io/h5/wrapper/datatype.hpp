// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>
#include <type_traits>

#include <hdf5.h>

#include <lima/exceptions.hpp>
#include <lima/core/enums.hpp>
#include <lima/io/h5/wrapper/handle.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {
        namespace detail
        {
            // clang-format off
            template <typename T> struct deduct;
            template <> struct deduct<bool>               { static const hid_t type() { return H5T_NATIVE_HBOOL; } };
            template <> struct deduct<unsigned char>      { static const hid_t type() { return H5T_NATIVE_UINT8; } };
            template <> struct deduct<signed char>        { static const hid_t type() { return H5T_NATIVE_INT8; } };
            template <> struct deduct<char>               { static const hid_t type() { return H5T_NATIVE_INT8; } };
            template <> struct deduct<unsigned short>     { static const hid_t type() { return H5T_NATIVE_UINT16; } };
            template <> struct deduct<short>              { static const hid_t type() { return H5T_NATIVE_INT16; } };
            template <> struct deduct<unsigned int>       { static const hid_t type() { return H5T_NATIVE_UINT32; } };
            template <> struct deduct<int>                { static const hid_t type() { return H5T_NATIVE_INT32; } };
            template <> struct deduct<unsigned long int>  { static const hid_t type() { return H5T_NATIVE_UINT64; } };
            template <> struct deduct<long int>           { static const hid_t type() { return H5T_NATIVE_INT64; } };
            template <> struct deduct<unsigned long long> { static const hid_t type() { return H5T_NATIVE_UINT64; } };
            template <> struct deduct<long long>          { static const hid_t type() { return H5T_NATIVE_INT64; } };
            template <> struct deduct<float>              { static const hid_t type() { return H5T_NATIVE_FLOAT; } };
            template <> struct deduct<double>             { static const hid_t type() { return H5T_NATIVE_DOUBLE; } };
            // clang-format on

            inline pixel_enum pixel_type(hid_t datatype)
            {
                // H55_NATIVE_xxxx are not constants (expand to function calls): cannot use switch
                if (H5Tequal(datatype, H5T_NATIVE_UINT8) > 0)
                    return pixel_enum::gray8;
                else if (H5Tequal(datatype, H5T_NATIVE_INT8) > 0)
                    return pixel_enum::gray8s;
                else if (H5Tequal(datatype, H5T_NATIVE_UINT16) > 0)
                    return pixel_enum::gray16;
                else if (H5Tequal(datatype, H5T_NATIVE_INT16) > 0)
                    return pixel_enum::gray16s;
                else if (H5Tequal(datatype, H5T_NATIVE_UINT32) > 0)
                    return pixel_enum::gray32;
                else if (H5Tequal(datatype, H5T_NATIVE_INT32) > 0)
                    return pixel_enum::gray32s;
                else if (H5Tequal(datatype, H5T_NATIVE_FLOAT) > 0)
                    return pixel_enum::gray32f;
                else
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Unsupported H5 data type"));
            }
        } // namespace detail

        struct predef_datatype
        {
          public:
            predef_datatype(hid_t hid) : m_hid(hid) {}

            template <typename T>
            static predef_datatype create()
            {
                return detail::deduct<std::decay_t<T>>::type();
            }

            pixel_enum pixel_type() { return detail::pixel_type(m_hid); }

            size_t size() { return H5Tget_size(m_hid); }

            operator hid_t() const { return m_hid; }

          private:
            // It is illegal to close an immutable transient datatype (e.g. predefined types)
            hid_t m_hid;
        };

        class string_datatype
        {
          public:
            string_datatype() : m_hid(H5Tcopy(H5T_C_S1))
            {
                H5Tset_cset(m_hid.get(), H5T_CSET_UTF8);
                H5Tset_size(m_hid.get(), H5T_VARIABLE);
            }

            operator hid_t() const { return m_hid.get(); }

          private:
            shared_type_hid_t m_hid;
        };

    } //namespace h5
} //namespace io
} //namespace lima
