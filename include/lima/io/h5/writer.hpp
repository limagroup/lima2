// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstring>

#include <memory>
#include <optional>
#include <filesystem>

#include <boost/gil.hpp>
//#include <boost/gil/io/detail/dynamic.hpp>

#include <fmt/format.h>

#include <hdf5.h>
#if defined(LIMA_ENABLE_ZLIB)
#include <zlib.h>
#endif //defined(LIMA_ENABLE_ZLIB)
#if defined(LIMA_ENABLE_BSHUF_LZ4)
#include <bshuf_h5filter.h>
#endif //defined(LIMA_ENABLE_BSHUF_LZ4)

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/core/frame_info.hpp>
#include <lima/io/compression/zip.hpp>
#include <lima/io/compression/bshuf_lz4.hpp>
#include <lima/io/h5/enums.hpp>
#include <lima/io/h5/nexus.hpp>
#include <lima/io/h5/params.hpp>
#include <lima/hw/params.hpp>
#include <lima/hw/info.hpp>

namespace lima
{
namespace io
{
    namespace h5
    {

//inline static const H5Z_filter_t H5Z_FILTER_DEFLATE = 1;
#if defined(LIMA_ENABLE_BSHUF_LZ4)
        inline static const H5Z_filter_t H5Z_FILTER_BSHUF = BSHUF_H5FILTER;
#endif //defined(LIMA_ENABLE_BSHUF_LZ4)

        /// Cast dimensions from point2<...> to point2<hsize_t>
        template <typename Point1, typename Point2>
        constexpr auto cast_dimensions(Point2 const& p)
        {
            return Point1(p.x, p.y);
        }

        template <typename View>
        predef_datatype datatype(View const&)
        {
            return predef_datatype(detail::deduct<typename View::value_type>::type());
        }

        template <typename... Views>
        predef_datatype datatype(lima::frame_view<Views...> const& v)
        {
            return boost::variant2::visit([](auto&& v) { return datatype(v); }, v);
        }

        inline predef_datatype datatype(pixel_enum px)
        {
            switch (px) {
            case pixel_enum::gray8:
                return predef_datatype::create<bpp8_pixel_t>();
            case pixel_enum::gray8s:
                return predef_datatype::create<bpp8s_pixel_t>();
            case pixel_enum::gray16:
                return predef_datatype::create<bpp16_pixel_t>();
            case pixel_enum::gray16s:
                return predef_datatype::create<bpp16s_pixel_t>();
            case pixel_enum::gray32:
                return predef_datatype::create<bpp32_pixel_t>();
            case pixel_enum::gray32s:
                return predef_datatype::create<bpp32s_pixel_t>();
            case pixel_enum::gray32f:
                return predef_datatype::create<bpp32f_pixel_t>();
            case pixel_enum::gray64f:
                return predef_datatype::create<bpp64f_pixel_t>();
            default:
                LIMA_THROW_EXCEPTION(std::runtime_error("Unsupported pixel type"));
            }
        }

        struct no_metadata_writer
        {
            void write(nx::group detector) const {}
        };

        struct nx_metadata_writer
        {
            void write(nx::group detector) const
            {
                using namespace h5::path_literals;

                // Detector info
                auto detector_info = nx::group::create(detector, "detector_information"_p, "NXcollection");
                detector_info.create_dataset("model", det_info.model);
                detector_info.create_dataset("name", det_info.name);
                detector_info.create_dataset("plugin", det_info.plugin);

                auto max_image_size = nx::group::create(detector_info, "max_image_size"_p, "NXcollection");
                max_image_size.create_dataset("xsize", det_info.dimensions.x);
                max_image_size.create_dataset("ysize", det_info.dimensions.y);

                auto pixel_size = nx::group::create(detector_info, "pixel_size"_p, "NXcollection");
                pixel_size.create_dataset("xsize", det_info.pixel_size.x);
                pixel_size.create_dataset("ysize", det_info.pixel_size.y);

                // Acquisition info
                double exposure_time = std::chrono::duration<double>(acq_params.expo_time).count();
                double latency_time = std::chrono::duration<double>(acq_params.latency_time).count();

                auto acquisition = nx::group::create(detector, "acquisition"_p, "NXcollection");
                acquisition.create_dataset("exposure_time", exposure_time);
                acquisition.create_dataset("latency_time", latency_time);
                acquisition.create_dataset("nb_frames", acq_params.nb_frames);
                acquisition.create_dataset("trigger_mode", boost::describe::enum_to_string(acq_params.trigger_mode));
                acquisition.create_dataset("nb_frames_per_trigger", acq_params.nb_frames_per_trigger);
            }

            hw::acquisition_params acq_params; // Acquisition
            hw::info det_info;                 // Detector
        };

        class writer
        {
          public:
            using dimensions_t = lima::point<hsize_t>;
            using params_t = saving_params;

            template <typename MetadataWriter = no_metadata_writer>
            writer(std::filesystem::path const& filename, // File name
                   saving_params const& sav_params,       // Saving params
                   frame_info const& frame_info,          // Frame size, pixel type
                   MetadataWriter const& metadata_writer = MetadataWriter()) :
                m_filename(filename),
                m_dimensions(cast_dimensions<dimensions_t>(frame_info.dimensions())),
                m_nb_channels((hsize_t) frame_info.nb_channels()),
                m_dtype(h5::datatype(frame_info.pixel_type())),
                m_nb_frames((hsize_t) sav_params.nb_frames_per_file),
                m_compression(sav_params.compression)
            {
                using namespace detail;
                using namespace std::string_literals;
                using namespace h5::path_literals;

                assert(m_dimensions.x > 0 && m_dimensions.y > 0);
                assert(m_nb_channels > 0);
                assert(m_nb_frames > 0);

                LIMA_LOG(trace, io) << "Constructing HDF5 writer for file " << filename;

                //#if defined(LIMA_ENABLE_MPIIO)
                //                //Init HDF5 MPI/IO
                //                MPI_Comm comm = MPI_COMM_WORLD;
                //                MPI_Info info = MPI_INFO_NULL;
                //
                //                int mpi_size;
                //                MPI_Comm_size(comm, &mpi_size);
                //                int mpi_rank;
                //                MPI_Comm_rank(comm, &mpi_rank);
                //#endif //defined(LIMA_ENABLE_MPIIO)
                //
                //                hid_t fapl_id = H5Pcreate(H5P_FILE_ACCESS);
                //#if defined(LIMA_ENABLE_MPIIO)
                //                H5Pset_fapl_mpio(fapl_id, comm, info);
                //#endif //defined(LIMA_ENABLE_MPIIO)

                // Open the HDF5 file
                auto f = nx::file::create(filename);

                // Create NEXUS entry
                nx::group entry = f.create_entry(sav_params.nx_entry_name, sav_params.nx_instrument_name,
                                                 sav_params.nx_detector_name);

                // Write metadata
                nx::group detector =
                    nx::group::open(entry, sav_params.nx_instrument_name / sav_params.nx_detector_name);
                metadata_writer.write(detector);

                h5::path data_path = sav_params.nx_instrument_name / sav_params.nx_detector_name / "data"_p;

                //#if defined(LIMA_ENABLE_MPIIO)
                //              m_dxpl_id = H5Pcreate(H5P_DATASET_XFER);
                //              H5Pset_dxpl_mpio(m_dxpl_id, H5FD_MPIO_COLLECTIVE);
                //#endif //defined(LIMA_ENABLE_MPIIO)

                //		m_dset = H5Dcreate(m_file_id.get(), "mydataset", H5T_NATIVE_INT, m_filespace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                //
                ////		hsize_t offset[3];
                ////		offset[0] = 0;
                ////		offset[1] = 0;
                ////#if defined(LIMA_ENABLE_MPIIO)
                ////		offset[2] = mpi_rank;
                ////#else
                ////		offset[2] = 0;
                ////#endif //defined(LIMA_ENABLE_MPIIO)
                ////		herr_t status = H5Sselect_hyperslab(m_filespace_id, H5S_SELECT_SET, offset, NULL, memdims, NULL);

                //std::size_t size = m_dimensions.x * m_dimensions.y;

                // Dataset Creation Property List
                auto dcpl = prop_list::create(H5P_DATASET_CREATE);

                // Set the compression_enum
                switch (m_compression) {
                case compression_enum::zip: {
#if defined(LIMA_ENABLE_ZLIB)
                    htri_t avail = H5Zfilter_avail(H5Z_FILTER_DEFLATE);
                    if (!avail) {
                        LIMA_LOG(warning, io) << "gzip filter not available.";
                        break;
                    }

                    const unsigned int compression_level = 6;
                    dcpl.set_deflate(compression_level);
#else
                    LIMA_THROW_EXCEPTION(std::runtime_error("Not built with zlib library"));
#endif //defined(LIMA_ENABLE_ZLIB)
                    break;
                }
                case compression_enum::bshuf_lz4: {
#if defined(LIMA_ENABLE_BSHUF_LZ4)
                    // Register filter
                    int ret = bshuf_register_h5filter();
                    if (ret < 0)
                        LIMA_THROW_EXCEPTION(std::runtime_error("Cannot register H5BSHUF filter"));

                    unsigned int c_values[2] = {0, BSHUF_H5_COMPRESS_LZ4};
                    dcpl.set_filter(H5Z_FILTER_BSHUF, H5Z_FLAG_MANDATORY, lengthof(c_values), c_values);
#else
                    LIMA_THROW_EXCEPTION(std::runtime_error("Not built with bitshuffle library"));
#endif //defined(LIMA_ENABLE_BSHUF_LZ4)
                    break;
                }
                case compression_enum::none:
                default:
                    break;
                }

                // Set dimensions
                hsize_t dims[] = {m_nb_frames, m_nb_channels, m_dimensions.y, m_dimensions.x};

                // Set chunk size
                hsize_t nb_frames_per_chunk;
                hsize_t nb_channels_per_chunk;

                if (sav_params.nb_frames_per_chunk) {
                    nb_frames_per_chunk = sav_params.nb_frames_per_chunk;
                    nb_channels_per_chunk = m_nb_channels;
                } else {
                    nb_frames_per_chunk = 1;
                    nb_channels_per_chunk = 1;
                }

                hsize_t chunk[] = {nb_frames_per_chunk, nb_channels_per_chunk, m_dimensions.y, m_dimensions.x};
                dcpl.set_chunk(lengthof(chunk), chunk);

                // Create dataspace
                auto file_space = dataspace::create_simple(lengthof(dims), dims);

                // Create dataset
                m_dset = dataset::create(entry, data_path, m_dtype, file_space, H5P_DEFAULT, dcpl, H5P_DEFAULT);

                // Add interpretation attributes
                m_dset.attrs.create("interpretation", "image"s);

                // Create NEXUS plot group and add a link to data from plot
                auto plot = nx::group::create(
                    entry, sav_params.nx_instrument_name / sav_params.nx_detector_name / "plot"_p, "NXdata");
                plot.attrs.create("signal", "data"s);
                link_hard(entry, data_path, plot, "data"_p);

                // Create NEXUS measurement group and add a link to data from measurement
                auto measurement = nx::group::create(entry, "measurement"_p, "NXcollection");
                link_hard(entry, data_path, measurement, "data"_p);
            }

            writer(writer const&) = delete;
            writer const& operator=(writer const&) = delete;

            writer(writer&& w) = default;

            //~writer() { LIMA_LOG(trace, io) << "Closing HDF5 writer"; }

            std::filesystem::path filename() const { return m_filename; }

            // Write the given view at a given idx
            template <typename View>
            void write_view(const View& v, hsize_t frame_idx = 0)
            {
                if (frame_idx >= m_nb_frames)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Frame number out of bound")
                                         << boost::errinfo_h5_idx(frame_idx));

                hsize_t offset[] = {frame_idx, 0U, 0U, 0U};
                uint32_t filter_mask = 0;

                using data_type = typename View::value_type;
                size_t elem_size = sizeof(data_type);
                size_t nb_elems = v.size();
                size_t data_size = nb_elems * elem_size; //TODO Make this alignement aware but how?
                const data_type* data_ptr = &v(0, 0);

                if (elem_size != m_dtype.size())
                    LIMA_THROW_EXCEPTION(lima::runtime_error("View pixel / H5Tget_size mismatch"));

                std::vector<unsigned char> comp_buffer;

                switch (m_compression) {
                case compression_enum::none: {
                    m_dset.write_chunk(data_ptr, filter_mask, offset, data_size);
                    break;
                }
                case compression_enum::zip: {
#if defined(LIMA_ENABLE_ZLIB)
                    // Resize compression_enum buffer
                    uLong bound_size = compressBound(nb_elems * elem_size);
                    comp_buffer.resize(bound_size);

                    uLongf comp_size = comp_buffer.size();
                    Bytef* comp_data = comp_buffer.data();
                    const unsigned int compression_level = 6;

                    // Compress
                    int ret =
                        compress2(comp_data, &comp_size, (const Bytef*) data_ptr, (uLong) data_size, compression_level);
                    if (ret != Z_OK)
                        LIMA_THROW_EXCEPTION(lima::runtime_error("compress2 error") << boost::errinfo_errno(ret));

                    LIMA_LOG(trace, io) << "GZ compression_enum size = " << comp_size
                                        << " (ratio=" << ((double) data_size / (double) comp_size) << ")";

                    // Write chunk
                    m_dset.write_chunk(comp_data, filter_mask, offset, comp_size);

#endif //defined(LIMA_ENABLE_ZLIB)
                    break;
                }
                case compression_enum::bshuf_lz4:
#if defined(LIMA_ENABLE_BSHUF_LZ4)
                    // Process in blocks of this many elements. Pass 0 to select automatically (recommended)
                    unsigned int block_size = 0;
                    // Resize compression_enum buffer
                    size_t bound_size = bshuf_compress_lz4_bound(nb_elems, elem_size, block_size);
                    comp_buffer.resize(bound_size);

                    size_t comp_size = comp_buffer.size();
                    unsigned char* comp_data = comp_buffer.data();

                    // Compress
                    bshuf_write_uint64_BE(comp_data, data_size);
                    bshuf_write_uint32_BE(comp_data + 8, block_size);

                    comp_size = bshuf_compress_lz4(data_ptr, comp_data + 12, nb_elems, elem_size, block_size);

                    LIMA_LOG(trace, io) << "BS/LZ4 compression_enum size = " << comp_size
                                        << " (ratio=" << ((double) data_size / (double) comp_size) << ")";

                    // Write chunk
                    m_dset.write_chunk(comp_data, filter_mask, offset, comp_size + 12);

#endif //defined(LIMA_ENABLE_BSHUF_LZ4)
                    break;
                }
            }

            // Write the given dynamic view at a given idx
            template <typename... Views>
            void write_view(const frame_view<Views...>& v, hsize_t frame_idx = 0)
            {
                boost::variant2::visit([&](auto view) { this->write_view(view, frame_idx); }, v);
            }

            // Write the given chunk at a given idx
            void write_chunk(const void* comp_data, size_t comp_size, hsize_t frame_idx = 0U, hsize_t channel_idx = 0U)
            {
#if !defined(NDEBUG)
                LIMA_LOG(trace, io) << "Writing HDF5 chunk " << frame_idx;
#endif

                hsize_t offset[] = {frame_idx, channel_idx, 0U, 0U};
                uint32_t filter_mask = 0;

                // Write chunk
                m_dset.write_chunk(comp_data, filter_mask, offset, comp_size);
            }

            // Resize the extent of the dataset
            void resize(hsize_t nb_frames)
            {
                hsize_t dims[] = {nb_frames, m_nb_channels, m_dimensions.y, m_dimensions.x};
                m_dset.extent(dims);
            }

          protected:
            std::filesystem::path m_filename; //!< The filename of the writer
            dataset m_dset;                   //!< The dataset

            compression_enum m_compression; //!< The current compression_enum setting
            dimensions_t m_dimensions;      //!< The dimensions of data frame
            hsize_t m_nb_channels;          //!< The number of channels in the dataset
            hsize_t m_nb_frames;            //!< The number of frames in the dataset
            predef_datatype m_dtype;        //!< The H5Type of the dataset
        };

        class writer_sparse
        {
          public:
            using dimensions_t = lima::point<hsize_t>;
            using params_t = saving_params;

            template <typename MetadataWriter = no_metadata_writer>
            writer_sparse(std::filesystem::path const& filename, // File name
                          saving_params const& sav_params,       // Saving params
                          frame_info const& frame_info,          // Frame size, pixel type
                          MetadataWriter const& metadata_writer = MetadataWriter()) :
                m_filename(filename),
                m_dimensions(cast_dimensions<dimensions_t>(frame_info.dimensions())),
                m_intensity_dtype(h5::datatype(frame_info.pixel_type())),
                m_index_dtype(H5T_NATIVE_UINT32),
                m_frame_dtype(H5T_NATIVE_UINT64),
                m_nb_frames(sav_params.nb_frames_per_file),
                m_compression(sav_params.compression)
            {
                using namespace detail;
                using namespace std::string_literals;
                using namespace h5::path_literals;

                LIMA_LOG(trace, io) << "Constructing HDF5 sparse writer for file " << filename;

                // Open the HDF5 file
                auto f = nx::file::create(filename);

                auto nb_frames = sav_params.nb_frames_per_file;
                auto nb_frames_per_chunk = sav_params.nb_frames_per_chunk;

                // Create NEXUS entry
                nx::group entry = f.create_entry(sav_params.nx_entry_name, sav_params.nx_instrument_name,
                                                 sav_params.nx_detector_name);

                h5::path data_path = sav_params.nx_instrument_name / sav_params.nx_detector_name / "data"_p;

                std::size_t size = m_dimensions.x * m_dimensions.y;

                // Dataset Creation Property List
                auto dcpl = prop_list::create(H5P_DATASET_CREATE);

                // Set the compression_enum
                switch (m_compression) {
                case compression_enum::zip: {
#if defined(LIMA_ENABLE_ZLIB)
                    htri_t avail = H5Zfilter_avail(H5Z_FILTER_DEFLATE);
                    if (!avail) {
                        LIMA_LOG(warning, io) << "gzip filter not available.";
                        break;
                    }

                    const unsigned int compression_level = 6;
                    dcpl.set_deflate(compression_level);
#else
                    LIMA_THROW_EXCEPTION(std::runtime_error("Not built with zlib library"));
#endif //defined(LIMA_ENABLE_ZLIB)
                    break;
                }
                case compression_enum::bshuf_lz4: {
#if defined(LIMA_ENABLE_BSHUF_LZ4)
                    // Register filter
                    int ret = bshuf_register_h5filter();
                    if (ret < 0)
                        LIMA_THROW_EXCEPTION(std::runtime_error("Cannot register H5BSHUF filter"));

                    unsigned int c_values[2] = {0, BSHUF_H5_COMPRESS_LZ4};
                    dcpl.set_filter(H5Z_FILTER_BSHUF, H5Z_FLAG_MANDATORY, lengthof(c_values), c_values);
#else
                    LIMA_THROW_EXCEPTION(std::runtime_error("Not built with bitshuffle library"));
#endif //defined(LIMA_ENABLE_BSHUF_LZ4)
                    break;
                }
                case compression_enum::none:
                default:
                    break;
                }

                auto data = nx::group::create(entry, data_path, "NXdata");
                data.attrs.create("dataformat", "sparsify-Bragg"s);
                data.attrs.create("width", m_dimensions.x);
                data.attrs.create("height", m_dimensions.y);

                // Frame idx
                {
                    // Set dimensions
                    hsize_t dims[] = {hsize_t(nb_frames) + 1};
                    hsize_t max_dims[] = {H5S_UNLIMITED};

                    // Set chunk size
                    hsize_t chunk[] = {512};
                    dcpl.set_chunk(lengthof(chunk), chunk);

                    // Create dataspace
                    auto file_space = dataspace::create_simple(lengthof(dims), dims, max_dims);

                    // Create datasets
                    m_frame_idx =
                        dataset::create(data, "frame_ptr"_p, m_frame_dtype, file_space, H5P_DEFAULT, dcpl, H5P_DEFAULT);

                    // Add initial 0 index
                    write_frame_idx(0, 0);
                }

                // Pixels
                {
                    // Set dimensions
                    hsize_t dims[] = {0};
                    hsize_t max_dims[] = {H5S_UNLIMITED};

                    // Set chunk size
                    hsize_t chunk[] = {4096};
                    dcpl.set_chunk(lengthof(chunk), chunk);

                    // Create dataspace
                    auto file_space = dataspace::create_simple(lengthof(dims), dims, max_dims);

                    // Create datasets
                    m_pixel_idx =
                        dataset::create(data, "index"_p, m_index_dtype, file_space, H5P_DEFAULT, dcpl, H5P_DEFAULT);
                    m_pixel_val = dataset::create(data, "intensity"_p, m_intensity_dtype, file_space, H5P_DEFAULT, dcpl,
                                                  H5P_DEFAULT);
                }

                //// Create NEXUS plot link
                //link_hard(entry, data_path, entry,
                //          sav_params.nx_instrument_name / sav_params.nx_detector_name / "plot"_p);
                //data.attrs.create("signal", "background_avg"_p);

                // Create NEXUS measurement group and add a link to data from measurement
                auto measurement = nx::group::create(entry, "measurement"_p, "NXcollection");
                link_hard(entry, data_path, measurement, "data"_p);
            }

            std::filesystem::path filename() const { return m_filename; }

            void write_frame_idx(hsize_t frame_idx, size_t val)
            {
                // The dimension of the frame_idx dataset is (nb_frames + 1)
                if (frame_idx > m_nb_frames)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Frame number out of bound")
                                         << boost::errinfo_h5_idx(frame_idx));

                hsize_t dims[] = {1};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_frame_idx.space();

                hsize_t offset[] = {frame_idx};
                hsize_t stride[] = {1};
                hsize_t count[] = {1};
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                m_frame_idx.write(&val, m_frame_dtype, mem_space, file_space);
            }

            void write_pixel_indices(std::vector<int> const& pixel_indices)
            {
                hsize_t dims[] = {pixel_indices.size()};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_pixel_idx.space();

                hsize_t file_dims[] = {0};
                hsize_t max_file_dims[] = {0};
                file_space.dims(file_dims, max_file_dims);

                hsize_t newdims[] = {file_dims[0] + pixel_indices.size()};
                m_pixel_idx.extent(newdims);

                file_space = m_pixel_idx.space();

                hsize_t offset[] = {file_dims[0]};
                hsize_t stride[] = {1};
                hsize_t count[] = {pixel_indices.size()};
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                predef_datatype index_dtype(H5T_NATIVE_INT32);
                m_pixel_idx.write(&pixel_indices[0], index_dtype, mem_space, file_space);
            }

            void write_pixel_values(std::vector<std::uint16_t> const& pixel_val)
            {
                hsize_t dims[] = {pixel_val.size()};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_pixel_val.space();

                hsize_t file_dims[] = {0};
                hsize_t max_file_dims[] = {0};
                file_space.dims(file_dims, max_file_dims);

                hsize_t newdims[] = {file_dims[0] + pixel_val.size()};
                m_pixel_val.extent(newdims);

                file_space = m_pixel_val.space();

                hsize_t offset[] = {file_dims[0]};
                hsize_t stride[] = {1};
                hsize_t count[] = {pixel_val.size()};
                predef_datatype sparse_dtype(H5T_NATIVE_UINT16);
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                m_pixel_val.write(&pixel_val[0], sparse_dtype, mem_space, file_space);
            }

            void write_frame(hsize_t frame_idx, std::vector<int> const& pixel_indices,
                             std::vector<std::uint16_t> const& pixel_values)
            {
                assert(pixel_indices.size() == pixel_values.size());

                m_frame_ptr += pixel_indices.size();
                write_frame_idx(frame_idx + 1, m_frame_ptr);
                if (!pixel_indices.empty())
                    write_pixel_indices(pixel_indices);
                if (!pixel_values.empty())
                    write_pixel_values(pixel_values);
            }

            // Resize the extent of the dataset
            void resize(hsize_t nb_frames)
            {
                m_nb_frames = nb_frames;
                hsize_t dims[] = {nb_frames + 1};
                m_frame_idx.extent(dims);
            }

          protected:
            std::filesystem::path m_filename; //!< The filename of the writer
            dataset m_frame_idx;              //!< The frame index dataset
            dataset m_pixel_idx;              //!< The pixel index dataset
            dataset m_pixel_val;              //!< The pixel intensity dataset

            compression_enum m_compression;    //!< The current compression_enum setting
            dimensions_t m_dimensions;         //!< The dimensions of data frame
            hsize_t m_nb_frames;               //!< The number of frames in the dataset
            predef_datatype m_intensity_dtype; //!< The H5Type of the pixel values
            predef_datatype m_index_dtype;     //!< The H5Type of the pixel indexes
            predef_datatype m_frame_dtype;     //!< The H5Type of the frame indexes
            size_t m_frame_ptr{0};             //!< The next frame starting index of pixel_idx/val
        };

        class writer_fai_sparse
        {
          public:
            using dimensions_t = lima::point<hsize_t>;
            using params_t = saving_params;

            template <typename MetadataWriter = no_metadata_writer>
            writer_fai_sparse(std::filesystem::path const& filename, // File name
                              saving_params const& sav_params,       // Saving params
                              frame_info const& frame_info,          // Frame size, pixel type
                              int nb_bins,                           // Sparse specific
                              lima::frame radius1d,                  //
                              lima::frame radius2d_mask,             //
                              MetadataWriter const& metadata_writer = MetadataWriter()) :
                m_filename(filename),
                m_dimensions(cast_dimensions<dimensions_t>(frame_info.dimensions())),
                m_bckg_dtype(H5T_NATIVE_FLOAT),
                m_peak_value_dtype(H5T_NATIVE_FLOAT),
                m_index_dtype(H5T_NATIVE_UINT32),
                m_nb_frames(sav_params.nb_frames_per_file),
                m_nb_bins(nb_bins),
                m_compression(sav_params.compression)
            {
                using namespace detail;
                using namespace std::string_literals;
                using namespace h5::path_literals;

                // Open the HDF5 file
                auto f = nx::file::create(filename);

                auto nb_frames = sav_params.nb_frames_per_file;
                auto nb_frames_per_chunk = sav_params.nb_frames_per_chunk;

                // Create NEXUS entry
                nx::group entry = f.create_entry(sav_params.nx_entry_name, sav_params.nx_instrument_name,
                                                 sav_params.nx_detector_name);

                h5::path data_path = sav_params.nx_instrument_name / sav_params.nx_detector_name / "data"_p;

                std::size_t size = m_dimensions.x * m_dimensions.y;

                // Dataset Creation Property List
                auto dcpl = prop_list::create(H5P_DATASET_CREATE);

                // Set the compression_enum
                switch (m_compression) {
                case compression_enum::zip: {
#if defined(LIMA_ENABLE_ZLIB)
                    htri_t avail = H5Zfilter_avail(H5Z_FILTER_DEFLATE);
                    if (!avail) {
                        LIMA_LOG(warning, io) << "gzip filter not available.";
                        break;
                    }

                    const unsigned int compression_level = 6;
                    dcpl.set_deflate(compression_level);
#else
                    LIMA_THROW_EXCEPTION(std::runtime_error("Not built with zlib library"));
#endif //defined(LIMA_ENABLE_ZLIB)
                    break;
                }
                case compression_enum::bshuf_lz4: {
#if defined(LIMA_ENABLE_BSHUF_LZ4)
                    // Register filter
                    int ret = bshuf_register_h5filter();
                    if (ret < 0)
                        LIMA_THROW_EXCEPTION(std::runtime_error("Cannot register H5BSHUF filter"));

                    unsigned int c_values[2] = {0, BSHUF_H5_COMPRESS_LZ4};
                    dcpl.set_filter(H5Z_FILTER_BSHUF, H5Z_FLAG_MANDATORY, lengthof(c_values), c_values);
#else
                    LIMA_THROW_EXCEPTION(std::runtime_error("Not built with bitshuffle library"));
#endif //defined(LIMA_ENABLE_BSHUF_LZ4)
                    break;
                }
                case compression_enum::none:
                default:
                    break;
                }

                auto data = nx::group::create(entry, data_path, "NXdata");
                data.attrs.create("dataformat", "sparsify-Bragg"s);

                // Radius1d
                {
                    // Validation
                    if (radius1d.pixel_type() != pixel_enum::gray32f) {
                        LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid radius1d type: must be gray32f"));
                    } else if (radius1d.dimensions() != point_t(m_nb_bins, 1)) {
                        std::ostringstream error;
                        auto&& [x, y] = radius1d.dimensions();
                        error << "Invalid radius1d size: " << x << "x" << y << ", "
                              << "expected " << m_nb_bins << "x" << 1;
                        LIMA_THROW_EXCEPTION(lima::invalid_argument(error.str()));
                    }
                    auto v = const_view<bpp32fc_view_t>(radius1d);

                    // Set dimensions
                    hsize_t dims[] = {m_nb_bins};
                    auto file_space = dataspace::create_simple(lengthof(dims), dims);
                    auto mem_space = file_space;
                    predef_datatype dtype = datatype(v);

                    // Create dataset
                    auto radius = dataset::create(data, "radius"_p, dtype, file_space, H5P_DEFAULT, dcpl, H5P_DEFAULT);

                    // Add attributes
                    radius.attrs.create("interpretation", "spectrum"s);
                    radius.attrs.create("long_name", "Bin radius"s);

                    // Write radius
                    radius.write(&v(0, 0), dtype, mem_space, file_space);
                }

                // Radius2d-Mask
                {
                    // Validation
                    if (radius2d_mask.pixel_type() != pixel_enum::gray32f) {
                        LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid radius2d-mask type: must be gray32f"));
                    } else if (radius2d_mask.dimensions() != frame_info.dimensions()) {
                        std::ostringstream error;
                        auto&& [x, y] = radius2d_mask.dimensions();
                        auto&& [dim_x, dim_y] = frame_info.dimensions();
                        error << "Invalid radius2d-mask size: " << x << "x" << y << ", "
                              << "expected " << dim_x << "x" << dim_y;
                        LIMA_THROW_EXCEPTION(lima::invalid_argument(error.str()));
                    }
                    auto v = const_view<bpp32fc_view_t>(radius2d_mask);

                    // Set dimensions
                    hsize_t dims[] = {m_dimensions.y, m_dimensions.x};
                    auto file_space = dataspace::create_simple(lengthof(dims), dims);
                    auto mem_space = file_space;
                    predef_datatype dtype = datatype(v);

                    // Create dataset
                    auto mask = dataset::create(data, "mask"_p, dtype, file_space, H5P_DEFAULT, dcpl, H5P_DEFAULT);

                    // Add attributes
                    mask.attrs.create("interpretation", "image"s);
                    mask.attrs.create("long_name", "Detector masked radius2d"s);

                    // Write mask
                    mask.write(&v(0, 0), dtype, mem_space, file_space);
                }

                // Background
                {
                    // Set dimensions
                    hsize_t dims[] = {hsize_t(m_nb_frames), m_nb_bins};

                    // Set chunk size
                    hsize_t chunk[] = {hsize_t(nb_frames_per_chunk), m_nb_bins};
                    dcpl.set_chunk(lengthof(chunk), chunk);

                    // Create dataspace
                    auto file_space = dataspace::create_simple(lengthof(dims), dims);

                    // Create datasets
                    m_background_avg = dataset::create(data, "background_avg"_p, m_bckg_dtype, file_space, H5P_DEFAULT,
                                                       dcpl, H5P_DEFAULT);
                    m_background_avg.attrs.create("interpretation", "spectrum"s);
                    m_background_avg.attrs.create("long_name", "Average value of background"s);
                    m_background_avg.attrs.create("signal", 1);

                    m_background_std = dataset::create(data, "background_std"_p, m_bckg_dtype, file_space, H5P_DEFAULT,
                                                       dcpl, H5P_DEFAULT);
                    m_background_std.attrs.create("interpretation", "spectrum"s);
                    m_background_std.attrs.create("long_name", "Standard deviation of background"s);

                    // The standard deviations of the data are to be stored in a data set of the same rank and dimensions, with the name "errors".
                    link_hard(data, "background_std"_p, data, "errors"_p);
                }

                // Frame idx
                {
                    // Set dimensions
                    hsize_t dims[] = {hsize_t(nb_frames) + 1};

                    // Create dataspace
                    auto file_space = dataspace::create_simple(lengthof(dims), dims);

                    // Create datasets
                    m_frame_idx = dataset::create(data, "frame_ptr"_p, m_index_dtype, file_space);

                    // Add initial 0 index
                    write_frame_idx(0, 0);
                }

                // Peaks
                {
                    // Set dimensions
                    hsize_t dims[] = {0};
                    hsize_t max_dims[] = {H5S_UNLIMITED};

                    // Set chunk size
                    hsize_t chunk[] = {4096};
                    dcpl.set_chunk(lengthof(chunk), chunk);

                    // Create dataspace
                    auto file_space = dataspace::create_simple(lengthof(dims), dims, max_dims);

                    // Create datasets
                    m_peak_idx =
                        dataset::create(data, "index"_p, m_index_dtype, file_space, H5P_DEFAULT, dcpl, H5P_DEFAULT);
                    m_peak_val = dataset::create(data, "intensity"_p, m_peak_value_dtype, file_space, H5P_DEFAULT, dcpl,
                                                 H5P_DEFAULT);
                }

                // Create NEXUS plot link
                link_hard(entry, data_path, entry,
                          sav_params.nx_instrument_name / sav_params.nx_detector_name / "plot"_p);
                data.attrs.create("signal", "background_avg"_p);

                // Create NEXUS measurement group and add a link to data from measurement
                auto measurement = nx::group::create(entry, "measurement"_p, "NXcollection");
                link_hard(entry, data_path, measurement, "data"_p);
            }

            std::filesystem::path filename() const { return m_filename; }

            void write_frame_idx(hsize_t frame_idx, unsigned int val)
            {
                // The dimension of the frame_idx dataset is (nb_frames + 1)
                if (frame_idx > m_nb_frames)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Frame number out of bound")
                                         << boost::errinfo_h5_idx(frame_idx));

                hsize_t dims[] = {1};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_frame_idx.space();

                hsize_t offset[] = {frame_idx};
                hsize_t stride[] = {1};
                hsize_t count[] = {1};
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                m_frame_idx.write(&val, m_index_dtype, mem_space, file_space);
            }

            void write_peak_indices(std::vector<int> const& peak_indices)
            {
                hsize_t dims[] = {peak_indices.size()};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_peak_idx.space();

                hsize_t file_dims[] = {0};
                hsize_t max_file_dims[] = {0};
                file_space.dims(file_dims, max_file_dims);

                hsize_t newdims[] = {file_dims[0] + peak_indices.size()};
                m_peak_idx.extent(newdims);

                file_space = m_peak_idx.space();

                hsize_t offset[] = {file_dims[0]};
                hsize_t stride[] = {1};
                hsize_t count[] = {peak_indices.size()};
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                predef_datatype index_dtype(H5T_NATIVE_INT32);
                m_peak_idx.write(&peak_indices[0], index_dtype, mem_space, file_space);
            }

            void write_peak_values(std::vector<float> const& peak_val)
            {
                hsize_t dims[] = {peak_val.size()};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_peak_val.space();

                hsize_t file_dims[] = {0};
                hsize_t max_file_dims[] = {0};
                file_space.dims(file_dims, max_file_dims);

                hsize_t newdims[] = {file_dims[0] + peak_val.size()};
                m_peak_val.extent(newdims);

                file_space = m_peak_idx.space();

                hsize_t offset[] = {file_dims[0]};
                hsize_t stride[] = {1};
                hsize_t count[] = {peak_val.size()};
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                m_peak_val.write(&peak_val[0], m_peak_value_dtype, mem_space, file_space);
            }

            void write_frame_peaks(hsize_t frame_idx, std::vector<int> const& peak_indices,
                                   std::vector<float> const& peak_values)
            {
                assert(peak_indices.size() == peak_values.size());

                m_frame_ptr += peak_indices.size();
                write_frame_idx(frame_idx + 1, m_frame_ptr);
                if (!peak_indices.empty())
                    write_peak_indices(peak_indices);
                if (!peak_values.empty())
                    write_peak_values(peak_values);
            }

            void write_background_avg(hsize_t frame_idx, std::vector<float> const& background_avg)
            {
                if (frame_idx >= m_nb_frames)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Frame number out of bound")
                                         << boost::errinfo_h5_idx(frame_idx));

                hsize_t dims[] = {m_nb_bins};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_background_avg.space();

                hsize_t offset[] = {frame_idx, 0};
                hsize_t stride[] = {1, 1};
                hsize_t count[] = {1, m_nb_bins};
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                m_background_avg.write(&background_avg[0], m_bckg_dtype, mem_space, file_space);
            }

            void write_background_std(hsize_t frame_idx, std::vector<float> const& background_std)
            {
                if (frame_idx >= m_nb_frames)
                    LIMA_THROW_EXCEPTION(lima::hdf5_error("Frame number out of bound")
                                         << boost::errinfo_h5_idx(frame_idx));

                hsize_t dims[] = {m_nb_bins};
                auto mem_space = dataspace::create_simple(1, dims);

                dataspace file_space = m_background_std.space();

                hsize_t offset[] = {frame_idx, 0};
                hsize_t stride[] = {1, 1};
                hsize_t count[] = {1, m_nb_bins};
                file_space.select_hyperslab(H5S_SELECT_SET, offset, stride, count, nullptr);

                m_background_std.write(&background_std[0], m_bckg_dtype, mem_space, file_space);
            }

            // Resize the extent of the dataset
            void resize(hsize_t nb_frames)
            {
                // TODO
                m_nb_frames = nb_frames;
                hsize_t dims[] = {nb_frames + 1};
                m_frame_idx.extent(dims);
            }

          protected:
            std::filesystem::path m_filename; //!< The filename of the writer
            dataset m_background_avg;         //!< The background_avg dataset
            dataset m_background_std;         //!< The background_std dataset
            dataset m_frame_idx;              //!< The frame index dataset
            dataset m_peak_idx;               //!< The peak index dataset
            dataset m_peak_val;               //!< The peak intensity dataset

            compression_enum m_compression;     //!< The current compression_enum setting
            dimensions_t m_dimensions;          //!< The dimensions of data frame
            hsize_t m_nb_frames;                //!< The number of frames in the dataset
            hsize_t m_nb_bins;                  //!< The number of bins in the azimuthal integration
            predef_datatype m_bckg_dtype;       //!< The H5Type of the background
            predef_datatype m_peak_value_dtype; //!< The H5Type of the peak values
            predef_datatype m_index_dtype;      //!< The H5Type of the frame and pixel indexes
            hsize_t m_frame_ptr{0};             //!< The next frame starting index of pixel_idx/val
        };

    } //namespace h5
} //namespace io
} //namespace lima
