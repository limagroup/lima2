// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace lima
{
namespace io
{
    namespace h5
    {
        enum class compression_enum
        {
            none,     //!< Uncompressed
            zip,      //!< zip compression
            bshuf_lz4 //!< bitshuffle filter and lz4 compression
        };

        /// Used by file reader
        enum class file_type_enum
        {
            hdf5, //!< Bare HDF5 file
            nexus //!< Nexus HDF5 file
        };

    } //namespace h5
} //namespace io
} //namespace lima
