// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>
#include <string>

#include <lima/exceptions.hpp>

#include <lima/io/const.hpp>
#include <lima/io/enums.hpp>

#include <fmt/format.h>

namespace lima
{
namespace io
{
    // literals namespaces are special and they are guaranteed to only contain symbols defined
    // only by the standard library so safe to bind here
    using std::string_literals::operator"" s;

    struct filename_params
    {
        std::filesystem::path base_path = std::filesystem::temp_directory_path();
        std::string filename_format = default_filename_format;
        std::string filename_prefix = "lima2";
        int filename_rank = 0;
        std::string filename_suffix = ".h5"s;
    };

    struct saving_params : filename_params
    {
        int start_number = 0;
        file_exists_policy_enum file_exists_policy = file_exists_policy_enum::abort;
        int nb_frames_per_file = 1;
        int nb_frames_per_chunk = 1;
    };

    struct file_frame_slice
    {
        int start = 0;
        int count = 0; //!< End of slice given by container
        int stride = 1;

        void check() const
        {
            if ((start < 0) || (count < 0) || (stride < 1))
                LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid file frame slice"));
        }

        int calc_count(int nb_frames) const
        {
            check();
            if (start >= nb_frames)
                LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid file frame slice start/nb_frames"));
            auto slice_frames = (nb_frames - start - 1) / stride + 1;
            if (count == 0)
                return slice_frames;
            else if (count <= slice_frames)
                return count;
            else
                LIMA_THROW_EXCEPTION(lima::invalid_argument("Invalid file frame slice count/nb_frames"));
        }

        void apply_sub_slice(file_frame_slice const& o)
        {
            check();
            o.check();

            start += o.start * stride;
            stride *= o.stride;
            if (o.count == 0)
                return;
            if ((count == 0) || (count >= o.count))
                count = o.count;
            else
                LIMA_THROW_EXCEPTION(lima::invalid_argument("Sub-slice larger than original one"));
        }
    };

    struct loading_params : filename_params
    {
        int start_number = 0;
        int nb_frames_per_file = 0; //!< Automatic detection
        file_frame_slice frame_slice;
    };

} //namespace io
} //namespace lima
