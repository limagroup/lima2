// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>
#include <string>

#include <boost/describe/annotations.hpp>

#include <lima/io/params.hpp>
#include <lima/io/enums.describe.hpp>

namespace lima
{
namespace io
{
    BOOST_DESCRIBE_STRUCT(filename_params, (),
                          (base_path, filename_format, filename_prefix, filename_rank, filename_suffix))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(filename_params, base_path, (desc, "base path"), (doc, "The base path of the files"))
                                                                                                                   
    BOOST_ANNOTATE_MEMBER(filename_params, filename_format,
        (desc, "filename format"), 
        (doc, "The format of the generated filename [default: {filename_prefix}_{rank}_{file_number:05d}{filename_suffix}]"))
                                                                                                                   
    BOOST_ANNOTATE_MEMBER(filename_params, filename_prefix,
        (desc, "filename prefix"),
        (doc, "The prefix of the filename, see <filename_format> for usage description"))

    BOOST_ANNOTATE_MEMBER(filename_params, filename_rank,
        (desc, "filename rank"),
        (doc, "The rank of the process, see <filename_format> for usage description"))
                                                                                                                   
    BOOST_ANNOTATE_MEMBER(filename_params, filename_suffix,
        (desc, "filename suffix"),
        (doc, "The suffix of the filename, see <filename_format> for usage description"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(saving_params, (filename_params),
                          (start_number, file_exists_policy, nb_frames_per_file, nb_frames_per_chunk))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(saving_params, start_number,
        (desc, "start number"),
        (doc, "Start number in the file sequence"))

    BOOST_ANNOTATE_MEMBER(saving_params, file_exists_policy,
        (desc, "file exists policy"),
        (doc, "Policy if target filename exists [abort, overwrite, append, multiset]"))

    BOOST_ANNOTATE_MEMBER(saving_params, nb_frames_per_file,
        (desc, "nb frames per file"),
        (doc, "Number of frames per file"))

    BOOST_ANNOTATE_MEMBER(saving_params, nb_frames_per_chunk,
        (desc, "nb frames per chunk"),
        (doc, "The number of frames per chunk"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(file_frame_slice, (), (start, count, stride))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(file_frame_slice, start,
        (desc, "start"),
        (doc, "First frame of the slice"))

    BOOST_ANNOTATE_MEMBER(file_frame_slice, count,
        (desc, "count"),
        (doc, "Number of frames in the slice [0 = container-limited]"))

    BOOST_ANNOTATE_MEMBER(file_frame_slice, stride,
        (desc, "stride"),
        (doc, "Frame index step the slice"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(loading_params, (filename_params), (start_number, nb_frames_per_file, frame_slice))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(loading_params, start_number,
        (desc, "start number"),
        (doc, "Start number in the file sequence"))

    BOOST_ANNOTATE_MEMBER(loading_params, nb_frames_per_file,
        (desc, "nb frames per file"),
        (doc, "Number of frames per file [0 = autodetect]"))

    BOOST_ANNOTATE_MEMBER(loading_params, frame_slice,
        (desc, "frame slice"),
        (doc, "Slice selecting the frames to load"))
    // clang-format on

} //namespace io
} //namespace lima
