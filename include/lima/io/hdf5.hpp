// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>

#include <hdf5.h>
#if defined(LIMA_ENABLE_ZLIB)
#include <zlib.h>
#endif //defined(LIMA_ENABLE_ZLIB)
#if defined(LIMA_ENABLE_BSHUF_LZ4)
#include <bshuf_h5filter.h>
#endif //defined(LIMA_ENABLE_BSHUF_LZ4)

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/io/h5/writer.hpp>
#include <lima/io/h5/reader.hpp>

namespace lima
{
namespace io
{
    /// \ingroup H5_IO
    /// Saves the currently instantiated frame to an HDF5 file.
    inline void h5_write_frame(std::filesystem::path const& filename, frame const& frm)
    {
        h5::saving_params params{}; // default constructor: 1 frame/file, 1 frame/chunk, no comp
        h5::writer m(filename, params, {frm.dimensions(), frm.nb_channels(), frm.pixel_type()});
        m.write_view(const_view(frm));
    }

    /// \ingroup H5_IO
    /// Saves the currently instantiated frame to an HDF5 file with zip compression.
    inline void h5_zip_write_frame(std::filesystem::path const& filename, frame const& frm)
    {
        h5::saving_params params{}; // default constructor: 1 frame/file, 1 frame/chunk
        params.compression = h5::compression_enum::zip;
        h5::writer m(filename, params, {frm.dimensions(), frm.nb_channels(), frm.pixel_type()});
        m.write_view(const_view(frm));
    }

    /// \ingroup H5_IO
    /// Saves the currently instantiated frame to an HDF5 file with bshuf/LZ4 compression.
    inline void h5_bshuf_lz4_write_frame(std::filesystem::path const& filename, frame const& frm)
    {
        h5::saving_params params{}; // default constructor: 1 frame/file, 1 frame/chunk
        params.compression = h5::compression_enum::bshuf_lz4;
        h5::writer m(filename, params, {frm.dimensions(), frm.nb_channels(), frm.pixel_type()});
        m.write_view(const_view(frm));
    }

} //namespace io
} //namespace lima
