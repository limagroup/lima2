// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <chrono>
#include <string>
#include <boost/container/small_vector.hpp>

namespace lima
{
namespace hw
{
    struct channel_metadata
    {
        std::string name;
    };

    class frame_metadata
    {
      public:
        using timestamp_t =
            std::chrono::steady_clock::time_point; // The steady time since the beginning of the acquisition
        using channels_t = boost::container::small_vector<channel_metadata,
                                                          3>; // The steady time since the beginning of the acquisition

        std::size_t idx = 0;      //!< Index in the frame sequence (starting from 0)
        std::size_t recv_idx = 0; //!< Index in the frame sequence (starting from 0 and monotone)
        timestamp_t timestamp;    //!< Acquisition timestamp from camera backend
        bool is_final = false;    //!< true if the frame is the last one in the sequence
        bool is_valid = true;     //!< true if the frame is valid (e.g. no transmission error)
    };
} // namespace hw
} // namespace lima
