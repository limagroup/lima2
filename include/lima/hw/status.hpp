// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

namespace lima
{
namespace hw
{
    struct status
    {
        double humidity = 0.0;          //!< Relative humidity reported by humidity sensor
        double temperature = 0.0;       //!< Temperature reported by temperature sensor
        std::string high_voltage_state; //!< High voltage state of target
        std::string state;              //!< Detector state of target
        std::string time;               //!< Current system time
    };
} // namespace hw
} // namespace lima
