// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/hw/status.hpp>

namespace lima
{
namespace hw
{
    BOOST_DESCRIBE_STRUCT(status, (), (humidity, temperature, high_voltage_state, state, time))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(status, humidity,
        (desc, "humidity"),
        (doc, "The relative humidity reported by humidity sensor"))

    BOOST_ANNOTATE_MEMBER(status, temperature,
        (desc, "temperature"),
        (doc, "The temperature reported by temperature sensor"))

    BOOST_ANNOTATE_MEMBER(status, high_voltage_state,
        (desc, "high voltage state"),
        (doc, "The high voltage state of target"))        

    BOOST_ANNOTATE_MEMBER(status, state,
        (desc, "state"),
        (doc, "The detector state of target"))

    BOOST_ANNOTATE_MEMBER(status, time,
        (desc, "time"),
        (doc, "Current system time"))
    // clang-format on

} // namespace hw
} // namespace lima
