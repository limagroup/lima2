// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <type_traits>

#include <lima/hw/info.hpp>

namespace lima
{
namespace hw
{
    template <typename T>
    concept Config = requires(T t)
    {
        typename T::init_params_t;
        typename T::acq_params_t;
        typename T::data_t;
    };

    template <typename T>
    concept Control = requires(T t)
    {
        typename T::init_params_t;
        typename T::acq_params_t;
        typename T::data_t;

        {
            t.get_info()
            } -> std::convertible_to<hw::info>;
        t.prepare(typename T::acq_params_t{});
        t.start();
        t.stop();
    };

} // namespace hw
} // namespace lima
