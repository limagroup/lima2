// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/hw/capabilities.hpp>

namespace lima
{
namespace hw
{
    BOOST_DESCRIBE_STRUCT(capabilities, (),
                          (expo_time_range, latency_time_range, frame_time_range, trigger_modes,
                           nb_frames_per_trigger_range))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(capabilities, expo_time_range, (desc, "exposure time range"),
                          (doc, "The validity range of the exposure time [\u03BCs]"))

    BOOST_ANNOTATE_MEMBER(capabilities, latency_time_range, (desc, "latency time range"),
                          (doc, "The validity range of the latency time [\u03BCs]"))

    BOOST_ANNOTATE_MEMBER(capabilities, frame_time_range, (desc, "frame time range"),
                          (doc, "The validity range of the frame (exposure + latency) time [\u03BCs]"))

    BOOST_ANNOTATE_MEMBER(capabilities, trigger_modes, (desc, "trigger modes"),
                          (doc, "The supported trigger modes [internal, software, external, external_gate]"))

    BOOST_ANNOTATE_MEMBER(capabilities, nb_frames_per_trigger_range, (desc, "nb frames per trigger range"),
                          (doc, "The validity range of the nb frames per trigger [0, n]"))
    // clang-format on

} // namespace hw
} // namespace lima
