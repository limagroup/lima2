// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include "malloc.h" // for malloc_trim

#include <boost/exception_ptr.hpp>
#include <boost/exception/info.hpp>
#include <boost/exception/errinfo_fsm.hpp>
#include <boost/exception/errinfo_nested_exception.hpp>

#include <boost/sml.hpp>

#include <lima/logging.hpp>
#include <lima/exceptions.hpp>
#include <lima/core/enums.hpp>
#include <lima/hw/enums.hpp>

#include <lima/project_version.h>

namespace lima
{
namespace hw
{
    // clang-format off
    template <typename T> struct state_to_enum { static constexpr acq_state_enum value = acq_state_enum::idle; };
    template <> struct state_to_enum<struct prepared> { static constexpr acq_state_enum value = acq_state_enum::prepared; };
    template <> struct state_to_enum<struct running> { static constexpr acq_state_enum value = acq_state_enum::running; };
    template <> struct state_to_enum<struct stopped> { static constexpr acq_state_enum value = acq_state_enum::stopped; };
    template <> struct state_to_enum<struct fault> { static constexpr acq_state_enum value = acq_state_enum::fault; };
    // clang-format on

    /// A CRTP base class that adds the FSM implementation
    template <typename Derived, typename Config>
    class acquisition_fsm
    {
        using data_t = typename Config::data_t;
        using init_params_t = typename Config::init_params_t;
        using acq_params_t = typename Config::acq_params_t;
        using acq_info_t = typename Config::acq_info_t;

        // Finite State Machine

        /// \{
        /// \name States
        static constexpr auto idle = boost::sml::state<struct idle>;
        static constexpr auto prepared = boost::sml::state<struct prepared>;
        static constexpr auto running = boost::sml::state<struct running>;
        static constexpr auto stopped = boost::sml::state<struct stopped>;
        static constexpr auto fault = boost::sml::state<struct fault>;
        static constexpr auto any = boost::sml::state<boost::sml::_>;
        /// \}

        /// \{
        /// \name Events
        // clang-format off
        struct prepare_evt {
            static auto& c_str() { return "prepare"; }
            acq_params_t acq_params;
        };
        struct start_evt { static auto& c_str() { return "start"; } };
        struct trigger_evt { static auto& c_str() { return "trigger"; } };
        struct stop_evt { static auto& c_str() { return "stop"; } };
        struct close_evt{ static auto& c_str() { return "close"; } };
        struct reset_evt { static auto& c_str() { return "reset"; } };
        struct error_evt { static auto& c_str() { return "error"; } };
        // clang-format on
        /// \}

        struct logger
        {
            template <typename SM, typename Event>
            void log_process_event(const Event&) const
            {
            }

            template <typename SM, typename Guard, typename Event>
            void log_guard(const Guard&, const Event&, bool result) const
            {
            }

            template <typename SM, typename Action, typename Event>
            void log_action(const Action&, const Event&) const
            {
            }

            template <typename SM, typename TSrcState, typename TDstState>
            void log_state_change(const TSrcState& src, const TDstState& dst) const
            {
                LIMA_LOG(trace, acq) << "Transition " << src.c_str() << " -> " << dst.c_str() << " successful";
                if (on_state_change)
                    on_state_change(state_to_enum<typename TDstState::type>::value);
            }

            std::function<void(acq_state_enum)> on_state_change;
        };

        struct transition_table
        {
            auto operator()() noexcept
            {
                using namespace boost::sml;

                //Gards implementation

                //Actions implementation
                auto on_prepare = [this](Derived& acq, acq_info_t& acq_info, prepare_evt const& evt) {
                    acq_info = acq.acq_prepare(evt.acq_params);
                };
                constexpr auto on_start = [](Derived& acq) { acq.acq_start(); };
                constexpr auto on_stop = [](Derived& acq) { acq.acq_stop(); };
                constexpr auto on_close = [](Derived& acq) {
                    acq.acq_close();
                    malloc_trim(0);
                };
                constexpr auto on_reset = [](Derived& acq) { acq.acq_reset(); };
                constexpr auto on_exception = [](Derived& acq, boost::exception_ptr& eptr) {
                    LIMA_LOG(error, acq) << "Acquisition FSM exception in state [" << acq.state() << "]";
                    // To be rethrown in process_event after transition is handled
                    eptr = std::current_exception();
                };

                // clang-format off

                // Define the transition table
                return make_transition_table(
                    // Acquisition transition table
                     *idle + event<prepare_evt> / on_prepare     = prepared
                    , idle + event<stop_evt>                     = idle
                    , idle + event<reset_evt>                    = idle
                    , prepared + exception<_> / on_exception     = idle
                    , prepared + event<prepare_evt> / on_prepare
                    , prepared + event<start_evt> / on_start     = running
                    , running + event<stop_evt> / on_stop        = stopped
                    , running + exception<_> / on_exception      = fault
                    , running + event<close_evt> / on_close      = idle
                    , stopped + event<close_evt> / on_close      = idle
                    , running + event<error_evt>                 = fault
                    , idle + exception<_> / on_exception         = fault
                    , any + event<reset_evt> / on_reset          = idle
                );

                // clang-format on
            }
        };

        using fsm_t =
            boost::sml::sm<transition_table, boost::sml::logger<logger>, boost::sml::thread_safe<std::recursive_mutex>>;

      public:
        acquisition_fsm() : m_fsm(std::make_unique<fsm_t>(implementation(), m_acq_info, m_eptr, m_logger)) {}

        /// Prepare acquisition (e.g. allocate buffers)
        acq_info_t prepare(acq_params_t const& acq_params)
        {
            process_event(prepare_evt{acq_params});

            implementation().register_on_error([this]() { on_error(); });

            return m_acq_info;
        }

        /// Start acquisition
        void start() { process_event(start_evt{}); }

        /// Stop acquisition
        void stop() { process_event(stop_evt{}); }

        /// Close acquisition
        void close() { process_event(close_evt{}); }

        /// Reset acquisition
        void reset() { process_event(reset_evt{}); }

        /// Error
        void on_error() { process_event(error_evt{}); }

        /// \{
        /// \name State
        /// Returns the current state
        acq_state_enum state() const noexcept
        {
            acq_state_enum res = acq_state_enum::idle;
            m_fsm->visit_current_states([&res](auto s) { res = state_to_enum<typename decltype(s)::type>::value; });
            return res;
        }

        /// Register a callack for a change of state event
        void register_on_state_change(std::function<void(acq_state_enum)> cbk) { m_logger.on_state_change = cbk; }
        /// \}

      private:
        // Access the derived class
        Derived& implementation() { return *static_cast<Derived* const>(this); }
        Derived const& implementation() const { return *static_cast<Derived const* const>(this); }

        // FSM backend
        logger m_logger; //!< The state machine logger
        std::unique_ptr<fsm_t>
            m_fsm; //!< The state machine. Use unique_ptr to avoid using an incomplete type in the transition table

        boost::exception_ptr m_eptr; //!< Used to retrieve exceptions thrown inside a transition callback
        acq_info_t m_acq_info;

        /// Events
        template <typename Event>
        void process_event(Event const& evt)
        {
            LIMA_LOG_SCOPE(evt.c_str());

            // Defer the processing of the event to FSM
            bool is_handled = m_fsm->process_event(evt);

            // The event cannot be handled in the current state
            if (!is_handled) {
                LIMA_LOG(error, acq) << "Unexpected event given current state: [" << state() << " + " << evt.c_str()
                                     << "]";

                LIMA_THROW_EXCEPTION(lima::runtime_error("Processing event failed")
                                     << boost::errinfo_fsm_event(evt.c_str()));
            }

            // An exception was thrown from within the transition callback
            if (m_eptr) {
                LIMA_LOG(error, acq) << "Exception during transition [" << state() << " + " << evt.c_str() << "]";

                // Note: reset the value of m_eptr before rethrowing
                boost::exception_ptr tmp;
                std::swap(tmp, m_eptr);

                LIMA_THROW_EXCEPTION(lima::runtime_error("Exception during state transition")
                                     << boost::errinfo_nested_exception(tmp) << boost::errinfo_fsm_event(evt.c_str()));
            }
        }
    };

} // namespace hw
} // namespace lima
