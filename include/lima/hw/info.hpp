// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

#include <lima/core/enums.hpp>
#include <lima/core/point.hpp>

namespace lima
{
namespace hw
{
    // HW properties
    struct info
    {
        std::string plugin;    //!< The name of the camera plugin (e.g. "SmartPix")
        std::string model;     //!< The model of the camera (as returned by the SDK)
        std::string name;      //!< User detector name
        std::string sn;        //!< The S/N of the camera (as returned by the SDK)
        point<int> pixel_size; //!< The pixel size in um
        point<int> dimensions; //<! Detector dimensions in pixel
    };
} // namespace hw
} // namespace lima
