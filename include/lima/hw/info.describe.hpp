// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/hw/info.hpp>

namespace lima
{
namespace hw
{
    BOOST_DESCRIBE_STRUCT(info, (), (plugin, model, sn, pixel_size, dimensions))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(info, plugin,
        (desc, "plugin name"),
        (doc, "The name of the detector plugin"))

    BOOST_ANNOTATE_MEMBER(info, model,
        (desc, "detector model"),
        (doc, "The model of the detector"))

    BOOST_ANNOTATE_MEMBER(info, sn,
        (desc, "detector serial number"),
        (doc, "The S/N of the detector"))        

    BOOST_ANNOTATE_MEMBER(info, pixel_size,
        (desc, "pixel size"),
        (doc, "Dimensions of a pixel [\u03BCm]"))

    BOOST_ANNOTATE_MEMBER(info, dimensions,
        (desc, "detector dimensions"),
        (doc, "The hardware dimensions of the detector [px]"))
    // clang-format on

} // namespace hw
} // namespace lima
