// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/enums.hpp>
#include <lima/core/rectangle.hpp>

namespace lima
{
namespace hw
{
    using std::chrono_literals::operator"" ms;

    struct acquisition_params
    {
        int nb_frames = 1;
        std::chrono::microseconds expo_time = 1ms;
        std::chrono::microseconds latency_time = 0ms;
        trigger_mode_enum trigger_mode = trigger_mode_enum::internal;
        int nb_frames_per_trigger = 1;
        acq_mode_enum acq_mode = acq_mode_enum::normal;
    };

    /// HW image capabilities of the detector
    struct image_params
    {
        point<std::ptrdiff_t> binning = {1, 1}; //!< HW Binning
        rectangle<std::ptrdiff_t> roi;          //!< HW ROI
        flip_enum flip = flip_enum::none;       //!< HW flip
    };

    /// HW shutter capabilities of the detector
    struct shutter_params
    {
        shutter_mode_enum mode = shutter_mode_enum::manual;
    };

    /// HW accumulation capabilities of the detector
    struct accumulation_params
    {
        int nb_frames = 1;
        std::chrono::microseconds expo_time = 1ms;
        int saturated_threshold = 0;
    };

    /// HW video capabilities of the detector
    struct video_params
    {
        auto_exposure_mode_enum auto_exposure_mode = auto_exposure_mode_enum::off; //!< HW auto exposure
    };

    struct xfer_alignment
    {
        ssize_t col_alignment = 1; //<! Alignment in number of rows for the full frame (min 1)
        uint row_alignment = 8;    //<! Alignment in bits for each row of data (min 1)
        ssize_t header = 0;        //<! Gap in bytes before each ROI
        ssize_t footer = 0;        //<! Gap in bytes after each ROI
    };

    struct xfer_slice
    {
        int start = 0;  //<! Start index of the frame
        int stride = 1; //<! Stride index of the frame
    };

    /// HW transfer capabilities of the detector
    struct xfer_params
    {
        xfer_alignment alignment;
        xfer_slice slice;
    };

    /// All-in-one acquisition param structure to be inherited by plugin
    struct acq_params
    {
        acquisition_params acq;   //!< Acquisition parameters
        image_params img;         //!< Image parameters
        shutter_params shut;      //!< Shutter parameters
        accumulation_params accu; //!< Accumulation parameters
        video_params vid;         //!< Video parameters
        xfer_params xfer;         //!< Transfer parameters
    };

} // namespace hw
} // namespace lima
