// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/core/frame_metadata.hpp>

namespace lima
{
namespace hw
{
    BOOST_DESCRIBE_STRUCT(frame_metadata, (), (recv_idx, timestamp, is_valid))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(frame_metadata, recv_idx,
        (doc, "recv idx"),
        (desc, "Index in the frame sequence (starting from 0 and monotone)"))

    BOOST_ANNOTATE_MEMBER(frame_metadata, timestamp,
        (doc, "timestamp"),
        (desc, "Acquisition timestamp from camera backend"))

    BOOST_ANNOTATE_MEMBER(frame_metadata, is_valid,
        (doc, "valid"),
        (desc, "True if the frame is valid (e.g. no transmission error)"))
    // clang-format on
} // namespace hw
} // namespace lima
