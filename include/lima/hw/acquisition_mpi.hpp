// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/mpi.hpp>
#include <boost/serialization/describe.hpp>

#include <lima/logging.hpp>

namespace lima
{
namespace hw
{
    // A base class that ensure that MPI initialization is done prior to construction of the other members
    template <typename Derived, typename Config>
    class acquisition_init_mpi
    {
        using init_params_t = typename Config::init_params_t;
        using acq_params_t = typename Config::acq_params_t;
        using det_info_t = typename Config::det_info_t;

        const int master_rank = 0;

      public:
        acquisition_init_mpi() : m_env(boost::mpi::threading::level::multiple)
        {
            // Group comm for controls (color = 0) and receivers (color = 1)
            m_sub = m_world.split(1);

            // Broadcast init_params
            boost::mpi::broadcast(m_world, m_init_params, master_rank);

            LIMA_LOG(trace, acq) << fmt::format("Starting acquisition process {} of {}", m_world.rank(),
                                                m_world.size());
        }

        void bcast_det_info()
        {
            // Broadcast det_info
            boost::mpi::broadcast(m_world, m_det_info, master_rank);
        }

        int world_rank() const { return m_world.rank(); }
        int recv_rank() const { return m_sub.rank(); }
        int nb_receivers() const { return m_sub.size(); }

      protected:
        // MPI Communicators
        boost::mpi::environment m_env; // Should come first to make sure that MPI is initialized
        boost::mpi::communicator m_world;
        boost::mpi::communicator m_sub;

        init_params_t m_init_params;
        det_info_t m_det_info;
    };
} // namespace hw

} //namespace lima
