// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <chrono>
#include <vector>

#include <lima/core/enums.hpp>
#include <lima/core/point.hpp>

namespace lima
{
namespace hw
{
    // HW capabilities and validity ranges of the parameters
    struct capabilities
    {
        std::array<std::chrono::nanoseconds, 2> expo_time_range;    //<! Range of exposure time [min, max[
        std::array<std::chrono::nanoseconds, 2> latency_time_range; //<! Range of latency time [min, max[
        std::array<std::chrono::nanoseconds, 2> frame_time_range;   //<! Range of frame time [min, max[
        std::vector<trigger_mode_enum> trigger_modes;               //<! List of the supported trigger modes
        std::array<int, 2> nb_frames_per_trigger_range;             //<! Range of nb frames per trigger [min, max[
    };
} // namespace hw
} // namespace lima
