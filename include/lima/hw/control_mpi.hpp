// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/mpi.hpp>
#include <boost/serialization/describe.hpp>

#include <lima/logging.hpp>

namespace lima
{
namespace hw
{
    // A base class that ensure that MPI initialization is done prior to construction of the other members
    template <typename Derived, typename Config>
    class control_init_mpi
    {
        using init_params_t = typename Config::init_params_t;
        using acq_params_t = typename Config::acq_params_t;
        using det_info_t = typename Config::det_info_t;

      public:
        control_init_mpi(init_params_t const& init_params)
        {
            // Group comm for controls (color = 0) and receivers (color = 1)
            m_world.split(0);

            // Broadcast init_params
            boost::mpi::broadcast(m_world, const_cast<init_params_t&>(init_params), m_world.rank());

            LIMA_LOG(trace, ctl) << fmt::format("Starting control process {} of {}", m_world.rank(), m_world.size());
        }

        void bcast_det_info()
        {
            // Broadcast det_info
            auto det_info = implementation().det_info();
            boost::mpi::broadcast(m_world, det_info, m_world.rank());
        }

        int world_rank() const { return m_world.rank(); }

      protected:
        // Access the derived class
        Derived& implementation() { return *static_cast<Derived* const>(this); }
        Derived const& implementation() const { return *static_cast<const Derived* const>(this); }

        // MPI Communicators
        boost::mpi::communicator m_world;
    };
} // namespace hw

} //namespace lima
