// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/logging.hpp>

namespace lima
{
namespace hw
{
    // A base class that ensure that MPI initialization is done prior to construction of the other members
    template <typename Config>
    class control_init_nompi
    {
        using init_params_t = typename Config::init_params_t;
        using acq_params_t = typename Config::acq_params_t;

      public:
        control_init_nompi(init_params_t const& init_params)
        {
            // Broadcast init_params
            //boost::mpi::broadcast(m_world, const_cast<init_params_t&>(init_params), m_world.rank());

            LIMA_LOG(trace, ctl) << "Starting control process";
        }

        int world_rank() const { return 0; }

      protected:
    };
} // namespace hw

} //namespace lima
