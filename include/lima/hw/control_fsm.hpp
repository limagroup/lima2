// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/exception_ptr.hpp>
#include <boost/exception/info.hpp>
#include <boost/exception/errinfo_fsm.hpp>
#include <boost/exception/errinfo_nested_exception.hpp>

#include <boost/sml.hpp>

#include <lima/logging.hpp>
#include <lima/exceptions.hpp>
#include <lima/core/enums.hpp>

#include <lima/project_version.h>

namespace lima
{
namespace hw
{
    // clang-format off
    template <typename T> struct state_to_enum { static constexpr acq_state_enum value = acq_state_enum::idle; };
    template <> struct state_to_enum<struct prepared> { static constexpr acq_state_enum value = acq_state_enum::prepared; };
    template <> struct state_to_enum<struct running> { static constexpr acq_state_enum value = acq_state_enum::running; };
    template <> struct state_to_enum<struct stopped> { static constexpr acq_state_enum value = acq_state_enum::stopped; };
    template <> struct state_to_enum<struct fault> { static constexpr acq_state_enum value = acq_state_enum::fault; };
    // clang-format on

    /// A CRTP base class that adds the FSM implementation
    template <typename Derived, typename Config>
    class control_fsm
    {
        using init_params_t = typename Config::init_params_t;
        using acq_params_t = typename Config::acq_params_t;

        // Finite State Machine

        /// \{
        /// \name States
        static constexpr auto idle = boost::sml::state<struct idle>;
        static constexpr auto prepared = boost::sml::state<struct prepared>;
        static constexpr auto running = boost::sml::state<struct running>;
        static constexpr auto stopped = boost::sml::state<struct stopped>;
        static constexpr auto fault = boost::sml::state<struct fault>;
        static constexpr auto any = boost::sml::state<boost::sml::_>;
        /// \}

        /// \{
        /// \name Events
        // clang-format off
        struct prepare_evt {
            static auto& c_str() { return "prepare"; }
            acq_params_t acq_params;
        };
        struct start_evt { static auto& c_str() { return "start"; } };
        struct trigger_evt { static auto& c_str() { return "trigger"; } };
        struct stop_evt { static auto& c_str() { return "stop"; } };
        struct reset_evt {
        static auto& c_str() { return "reset"; }
            reset_level_enum level;
        };
        struct close_evt { static auto& c_str() { return "close"; } };
        // clang-format on
        /// \}

        // State Machine Logger
        struct logger
        {
            template <typename SM, typename Event>
            void log_process_event(const Event& evt) const
            {
            }

            template <typename SM, typename Guard, typename Event>
            void log_guard(const Guard&, const Event&, bool result) const
            {
            }

            template <typename SM, typename Action, typename Event>
            void log_action(const Action&, const Event&) const
            {
            }

            template <typename SM, typename TSrcState, typename TDstState>
            void log_state_change(const TSrcState& src, const TDstState& dst) const
            {
                LIMA_LOG(trace, ctl) << "Transition " << src.c_str() << " -> " << dst.c_str() << " successful";
                if (on_state_change)
                    on_state_change(state_to_enum<typename TDstState::type>::value);
            }

            std::function<void(acq_state_enum)> on_state_change;
        };

        struct transition_table
        {
            auto operator()() noexcept
            {
                using namespace boost::sml;

                //Gards implementation
                constexpr auto guard_validate_params = [](Derived const& ctrl, const prepare_evt& evt) {
                    return ctrl.hw_validate_acq_params(evt.acq_params);
                };
                auto gard_trigger_mode = [this]() { return trigger_mode == trigger_mode_enum::software; };

                //Actions implementation
                auto on_prepare = [this](Derived& ctrl, const prepare_evt& evt) {
                    trigger_mode = evt.acq_params.acq.trigger_mode;
                    ctrl.hw_prepare(evt.acq_params);
                };
                constexpr auto on_start = [](Derived& ctrl) { ctrl.hw_start(); };
                constexpr auto on_soft_trigger = [](Derived& ctrl) { ctrl.hw_trigger(); };
                constexpr auto on_stop = [](Derived& ctrl) { ctrl.hw_stop(); };
                constexpr auto on_close = [](Derived& ctrl) { ctrl.hw_close(); };
                constexpr auto on_reset = [](Derived& ctrl) { ctrl.hw_reset(); };
                constexpr auto on_exception = [](Derived& ctrl, boost::exception_ptr& eptr) {
                    LIMA_LOG(error, ctl) << "Control FSM exception in state [" << ctrl.state() << "]";
                    eptr = std::current_exception(); // To be rethrown after transition is handeld
                };

                // clang-format off

                // Define the transition table
                return make_transition_table(
                    // Control transition table
                     *idle + event<prepare_evt>[guard_validate_params] / on_prepare      = prepared
                    , idle + event<stop_evt>                                             = idle
                    , idle + event<reset_evt>                                            = idle
                    , prepared + event<prepare_evt>[guard_validate_params] / on_prepare
                    , prepared + event<start_evt> / on_start                             = running
                    , prepared + exception<_> / on_exception                             = idle
                    , running + event<trigger_evt>[gard_trigger_mode] / on_soft_trigger
                    , running + event<stop_evt> / on_stop                                = stopped
                    , running + exception<_> / on_exception                              = fault
                    , running + event<close_evt> / on_close                              = idle
                    , stopped + event<close_evt> / on_close                              = idle
                    , any + event<reset_evt> / on_reset                                  = idle
                );

                // clang-format on
            }

            trigger_mode_enum trigger_mode = trigger_mode_enum::internal;
        };

        using fsm_t =
            boost::sml::sm<transition_table, boost::sml::logger<logger>, boost::sml::thread_safe<std::recursive_mutex>>;

      public:
        control_fsm() : m_fsm(std::make_unique<fsm_t>(implementation(), m_eptr, m_logger)) {}

        /// Prepare acquisition
        void prepare(acq_params_t const& acq_params) { process_event(prepare_evt{acq_params}); }

        /// Start acquisition
        void start() { process_event(start_evt{}); }

        /// Software trigger if the camera supports it
        void trigger() { process_event(trigger_evt{}); }

        /// Stop acquisition
        void stop() { process_event(stop_evt{}); }

        /// Called when all receivers acquisition has ended
        void close() { process_event(close_evt{}); }

        /// Reset camera
        void reset(reset_level_enum const& level) { process_event(reset_evt{level}); }

        /// \{
        /// \name State
        /// Returns the current state
        acq_state_enum state() const noexcept
        {
            acq_state_enum res = acq_state_enum::idle;
            m_fsm->visit_current_states([&res](auto s) { res = state_to_enum<typename decltype(s)::type>::value; });
            return res;
        }

        /// Register a callack for a change of state event
        void register_on_state_change(std::function<void(acq_state_enum)> cbk) { m_logger.on_state_change = cbk; }
        /// \}

      private:
        // Access the derived class
        Derived& implementation() { return *static_cast<Derived* const>(this); }
        Derived const& implementation() const { return *static_cast<const Derived* const>(this); }

        // FSM backend
        logger m_logger;              //!< The state machine logger
        std::unique_ptr<fsm_t> m_fsm; //!<  Use unique_ptr to avoid using an incomplete type in the transition table

        boost::exception_ptr m_eptr; //!< Used to retrieve exceptions thrown inside a transition callback

        /// Events
        template <typename Event>
        void process_event(Event const& evt)
        {
            LIMA_LOG_SCOPE(evt.c_str());

            // Defer the processing of the event to FSM
            bool is_handled = m_fsm->process_event(evt);

            // The event cannot be handled in the current state
            if (!is_handled) {
                LIMA_LOG(error, ctl) << "Unexpected event given current state: [" << state() << " + " << evt.c_str()
                                     << "]";

                LIMA_THROW_EXCEPTION(lima::runtime_error("Processing event failed")
                                     << boost::errinfo_fsm_event(evt.c_str()));
            }

            // An exception was thrown from within the transition callback (on_prepare, on_start, ...)
            if (m_eptr) {
                LIMA_LOG(error, ctl) << "Exception during transition [" << state() << " + " << evt.c_str() << "]";

                // Note: reset the value of m_eptr before rethrowing
                boost::exception_ptr tmp;
                std::swap(tmp, m_eptr);

                LIMA_THROW_EXCEPTION(lima::runtime_error("Exception during state transition")
                                     << boost::errinfo_nested_exception(tmp) << boost::errinfo_fsm_event(evt.c_str()));
            }
        }
    };

} // namespace hw
} // namespace lima
