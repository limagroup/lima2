// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe.hpp>
#include <boost/describe/io_enums.hpp>

#include <lima/hw/enums.hpp>

namespace lima
{
namespace hw
{
    BOOST_DESCRIBE_ENUM(acq_error_enum, dropped_frame, buffer_overflow)

    using boost::describe::operator<<;
    using boost::describe::operator>>;

} // namespace hw
} // namespace lima
