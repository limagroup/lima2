// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <thread>
#include <vector>

#include <tbb/parallel_pipeline.h>
#include <tbb/task_arena.h>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/core/enums.hpp>
#include <lima/core/uuid.hpp>
#include <lima/hw/enums.hpp>
#include <lima/hw/params.hpp>
#include <lima/concepts/hw.hpp>
#include <lima/utils/math.hpp>
#include <lima/utils/vector_traits.hpp>

namespace lima
{
namespace hw
{
    /// A CRTP base class that adds the acquisition thread implementation
    template <typename Derived, typename Config>
    class acquisition_thread_pipeline
    {
        // Necessary to keep the implementation private
        friend lima::hw::acquisition_fsm<Derived, Config>;

        using hw_data_t = typename Config::hw_data_t;
        using data_t = typename Config::data_t;
        using init_params_t = typename Config::init_params_t;
        using acq_params_t = typename Config::acq_params_t;
        using acq_info_t = typename Config::acq_info_t;

        // Internal thread definition that run the acquition loop
        class thread
        {
          public:
            thread(Derived& hw, int nb_frames, xfer_slice slice, int nb_pipeline_threads, bool sched_fifo = false) :
                m_hw(hw),
                m_nb_frames(nb_frames),
                m_slice(slice),
                m_nb_pipeline_threads(nb_pipeline_threads),
                m_sched_fifo(sched_fifo)
            {
            }

            ~thread()
            {
                if (m_acq_loop.joinable()) {
                    // Stop the acquisition loop first
                    m_cancel = true;
                    m_acq_loop.join();
                }
            }

            /// Start acquisition
            void start()
            {
                LIMA_LOG_SCOPE("acquisition_thread_pipeline")

                m_nb_frames_xferred = 0;
                m_nb_frames_preprocessed = 0;

                // Start the acquisition thread
                m_acq_loop = std::thread([this]() {
                    if (on_start_acq) {
                        LIMA_LOG(trace, acq) << "Acquisition thread signal on_start_acq";
                        on_start_acq();
                    } else
                        LIMA_LOG(trace, acq) << "Acquisition thread has NO on_start_acq callback registered";

                    try {
                        LIMA_LOG(trace, acq) << "Starting acquisition loop";

                        acquisition_loop();

                        LIMA_LOG(trace, acq) << "Stopping acquisition loop";

                        m_hw.close();
                    } catch (...) {
                        LIMA_LOG(error, acq) << "Acquisition thread caught exception:\n"
                                             << boost::current_exception_diagnostic_information();

                        // Send empty frame to close the pipeline
                        data_t frm;
                        frm.metadata.idx = -1;
                        frm.metadata.is_final = true;
                        on_frame_ready(frm);

                        // Signal error
                        on_error();
                    }

                    if (on_end_acq) {
                        LIMA_LOG(trace, acq) << "Acquisition thread signal on_end_acq";
                        on_end_acq(m_nb_frames_xferred);
                    } else
                        LIMA_LOG(warning, acq) << "Acquisition thread has NO on_end_acq callback registered";

                    // Reset callbacks (to release resources eventually captured)
                    on_start_acq = {};
                    on_frame_ready = {};
                    on_end_acq = {};
                    on_error = {};
                });

#if defined(__unix__)
                if (m_sched_fifo) {
                    sched_param sch_param;
                    memset(&sch_param, 0, sizeof(sched_param));
                    sch_param.sched_priority = 10;
                    pthread_setschedparam(m_acq_loop.native_handle(), SCHED_FIFO, &sch_param);
                }
#endif
            }

            /// Stop acquisition
            void stop()
            {
                if (m_acq_loop.joinable()) {
                    // Stop the acquisition thread first
                    m_cancel = true;
                    m_acq_loop.join();
                }
            }

            /// Returns the number of frames transfered
            int nb_frames_xferred() const { return m_nb_frames_xferred; }

            /// Returns the number of frames preprocessed
            int nb_frames_preprocessed() const { return m_nb_frames_preprocessed; }

            /// Callback on image ready
            std::function<void(data_t const&)> on_frame_ready;

            /// Callback on start acquisition
            std::function<void()> on_start_acq;

            /// Callback on end acquisition
            std::function<void(int)> on_end_acq;

            /// Callback on acquisition error
            std::function<void()> on_error;

          private:
            void acquisition_loop()
            {
                assert(on_frame_ready); // Forgot to register the callback?

                auto numa_nodes = oneapi::tbb::info::numa_nodes();
                tbb::numa_node_id numa_index = numa_nodes[0];

                unsigned reserved_for_masters = 1;
                tbb::task_arena::priority priority = tbb::task_arena::priority::high;

                tbb::task_arena arena(
                    tbb::task_arena::constraints(numa_index, m_nb_pipeline_threads).set_max_threads_per_core(1),
                    reserved_for_masters, priority);

                bool is_final = false;
                arena.execute([&]() {
                    //tbb::this_task_arena::isolate([&]() {
                    // Start the acquisition pipeline
                    tbb::parallel_pipeline(
                        m_nb_pipeline_threads,
                        tbb::make_filter<void, hw_data_t>(
                            oneapi::tbb::filter_mode::serial_in_order,
                            [this, &is_final](oneapi::tbb::flow_control& fc) -> hw_data_t {
                                if (is_final) {
                                    fc.stop();
                                    return hw_data_t{};
                                }

                                // Get a frame from the HW
                                hw_data_t frm = m_hw.hw_get_frame();

                                // Limit the number of log traces given the order of magnitude of nb frames
                                if (!(m_nb_frames_xferred % order_of_magnitude_base10(m_nb_frames_xferred)))
                                    LIMA_LOG(trace, acq) << "Acquisition thread got frame " << m_nb_frames_xferred;

                                if (!(frm.empty() && frm.metadata.is_final))
                                    frm.metadata.recv_idx = m_nb_frames_xferred++;

                                // If last frames of the transfer (and not continuous acquisition)
                                // Or acquisition is stopped
                                if (frm.metadata.is_final || m_cancel) {
                                    is_final = true;

                                    LIMA_LOG(info, acq) << "Acquisition thread got final frame";
                                }

                                return frm;
                            })
                            & m_hw.hw_get_filters()
                            & tbb::make_filter<data_t, void>(
                                oneapi::tbb::filter_mode::serial_in_order, [this](data_t const& frm) {
                                    assert(frm.metadata.recv_idx == m_nb_frames_preprocessed);
                                    m_nb_frames_preprocessed++;

                                    // Send the data to the pipeline
                                    on_frame_ready(frm);
                                }));
                    //});
                });
            }

            /// Signal the acquisition thread that it is time to exit
            std::atomic_bool m_cancel = false;
            std::thread m_acq_loop;

            int m_nb_pipeline_threads = 1; //<! Number of processing threads of the pipeline

            /// Number of frames transferred since the beginning of the acquisition
            std::atomic_int m_nb_frames_xferred = 0;
            /// Number of frames preprocessed since the beginning of the acquisition
            std::atomic_int m_nb_frames_preprocessed = 0;

            int m_refresh_rate = 1;    //<! Refresh rate of the on_image_ready notification (in frames)
            int m_nb_frames;           //<! The number of frames to acquire
            xfer_slice m_slice;        //<! The slice of frames to acquire
            bool m_sched_fifo = false; //<! True if using SCHED_FIFO and priority = 10

            Derived& m_hw; //<! The underlying HW interface, for hw_get_frame()
        };

      public:
        /// Returns the number of frames transferred (on this receiver)
        int nb_frames_xferred() const
        {
            if (m_acquisition_thread)
                return m_acquisition_thread->nb_frames_xferred();

            return 0;
        }

        /// Register a callack for on acquisition start event
        void register_on_start_acq(std::function<void()> cbk)
        {
            assert(m_acquisition_thread);
            m_acquisition_thread->on_start_acq = cbk;
        }

        /// Register a callack for on frame ready event
        void register_on_frame_ready(std::function<void(data_t)> cbk)
        {
            assert(m_acquisition_thread);
            m_acquisition_thread->on_frame_ready = cbk;
        }

        /// Register a callack for on end of acquisition event
        void register_on_end_acq(std::function<void(int)> cbk)
        {
            assert(m_acquisition_thread);
            m_acquisition_thread->on_end_acq = cbk;
        }

        /// Register a callack for error event
        void register_on_error(std::function<void()> cbk)
        {
            assert(m_acquisition_thread);
            m_acquisition_thread->on_error = cbk;
        }

      private:
        /// Prepare acquisition (e.g. allocate buffers)
        /// \return The result of the detector preparation
        acq_info_t acq_prepare(acq_params_t const& acq_params)
        {
            LIMA_LOG_SCOPE("acq_prepare");

            // Prepare the detector
            acq_info_t acq_info = implementation().hw_prepare(acq_params);

            // Construct the acquisition thread
            m_acquisition_thread.emplace(implementation(), acq_params.acq.nb_frames, acq_params.xfer.slice,
                                         acq_params.det.nb_pipeline_threads);

            return acq_info;
        }

        /// Start acquisition
        void acq_start()
        {
            LIMA_LOG_SCOPE("acq_start");

            if (m_acquisition_thread) {
                m_acquisition_thread->start();
                implementation().hw_start();
            } else
                LIMA_THROW_EXCEPTION(lima::runtime_error("Acquisition not prepared"));
        }

        /// Stop acquisition
        void acq_stop()
        {
            LIMA_LOG_SCOPE("acq_stop");

            // Stop the hardware (should generate a final frame to stop the acquisition loop)
            implementation().hw_stop();
        }

        /// Cancel acquisition loop
        void acq_cancel()
        {
            LIMA_LOG_SCOPE("acq_cancel");

            if (m_acquisition_thread)
                m_acquisition_thread->stop();
        }

        /// Close acquisition
        void acq_close()
        {
            LIMA_LOG_SCOPE("acq_close");

            implementation().hw_close();
        }

        /// Reset acquisition
        void acq_reset() { LIMA_LOG_SCOPE("acq_reset"); }

        // Access the derived class
        Derived& implementation() { return *static_cast<Derived* const>(this); }
        Derived const& implementation() const { return *static_cast<const Derived* const>(this); }

        /// Callback on start acquisition
        std::function<void()> on_start;

        /// The acquisition thread
        std::optional<thread> m_acquisition_thread;
    };

} // namespace hw
} // namespace lima
