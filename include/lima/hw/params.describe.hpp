// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/core/enums.describe.hpp>
#include <lima/core/rectangle.describe.hpp>

#include <lima/hw/params.hpp>

namespace lima
{
namespace hw
{
    BOOST_DESCRIBE_STRUCT(acquisition_params, (),
                          (nb_frames, expo_time, latency_time, trigger_mode, nb_frames_per_trigger, acq_mode))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(acquisition_params, nb_frames,
        (desc, "number of frames"),
        (doc, "The number of frames to acquire (0 = continuous acquisition)"))

    BOOST_ANNOTATE_MEMBER(acquisition_params, expo_time,
        (desc, "exposure time"),
        (doc, "The exposure time [\u03BCs]"))

    BOOST_ANNOTATE_MEMBER(acquisition_params, latency_time,
        (desc, "latency time"),
        (doc, "The latency time [\u03BCs]"))

    BOOST_ANNOTATE_MEMBER(acquisition_params, trigger_mode,
        (desc, "trigger mode"),
        (doc, "The trigger mode [internal, software, external, gate]"))

    BOOST_ANNOTATE_MEMBER(acquisition_params, nb_frames_per_trigger,
        (desc, "nb frames per trigger"),
        (doc, "The number of acquired frames per trigger when [software, external]"))

    BOOST_ANNOTATE_MEMBER(acquisition_params, acq_mode,
        (desc, "acquisition mode"),
        (doc, "The acquisition mode [normal, concat]"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(image_params, (), (binning, roi, flip))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(image_params, binning,
        (desc, "binning"),
        (doc, "The pixel binning"))

    BOOST_ANNOTATE_MEMBER(image_params, roi,
        (desc, "region of interest"),
        (doc, "The region of interest to transfer"))

    BOOST_ANNOTATE_MEMBER(image_params, flip,
        (desc, "frame flip"),
        (doc, "Flip the frame [none, left_right, up_down]"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(shutter_params, (), (mode))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(shutter_params, mode,
        (desc, "mode"),
        (doc, "The shutter mode [manual, auto_frame, auto_sequence]"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(accumulation_params, (), (nb_frames, expo_time, saturated_threshold))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(accumulation_params, nb_frames,
        (desc, "nb frames"),
        (doc, "The calculated accumulation number of frames"))

    BOOST_ANNOTATE_MEMBER(accumulation_params, expo_time,
        (desc, "expo time"),
        (doc, "The effective accumulation total exposure"))

    BOOST_ANNOTATE_MEMBER(accumulation_params, saturated_threshold,
        (desc, "saturated threshold"),
        (doc, "The threshold for counting saturated pixels"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(video_params, (), (auto_exposure_mode))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(video_params, auto_exposure_mode,
        (desc, "auto exposure"),
        (doc, "Auto exposure mode for video detectors [on, off]"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(xfer_alignment, (), (col_alignment, row_alignment, header, footer))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(xfer_alignment, col_alignment,
        (desc, "column alignment"),
        (doc, "The column alignemnt of the transfered data"))

    BOOST_ANNOTATE_MEMBER(xfer_alignment, row_alignment,
        (desc, "row alignment"),
        (doc, "The row alignemnt of the transfered data"))

    BOOST_ANNOTATE_MEMBER(xfer_alignment, header,
        (desc, "header padding"),
        (doc, "The header padding of the transfered data"))

    BOOST_ANNOTATE_MEMBER(xfer_alignment, footer,
        (desc, "footer padding"),
        (doc, "The footer padding of the transfered data"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(xfer_slice, (), (start, stride))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(xfer_slice, start,
        (desc, "start index"),
        (doc, "The start index of the stride"))

    BOOST_ANNOTATE_MEMBER(xfer_slice, stride,
        (desc, "stride"),
        (doc, "The step between two adjacent index in the stride"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(xfer_params, (), (alignment, slice))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(xfer_params, alignment,
        (desc, "data alignment"),
        (doc, "The data alignemnt of the transfered data"))

    BOOST_ANNOTATE_MEMBER(xfer_params, slice,
        (desc, "data slice"),
        (doc, "The data slice of the transfered data (aka round-robin)"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(acq_params, (), (acq, img, shut, accu, vid, xfer))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(acq_params, acq,
        (desc, "acquisition"),
        (doc, "The HW acquisition parameters of the detector"))

    BOOST_ANNOTATE_MEMBER(acq_params, img,
        (desc, "image"),
        (doc, "The HW image parameters of the detector (ROI, Flip, Binning)"))

    BOOST_ANNOTATE_MEMBER(acq_params, shut,
        (desc, "shutter"),
        (doc, "The HW shutter parameters of the detector"))

    BOOST_ANNOTATE_MEMBER(acq_params, accu,
        (desc, "accumulation"),
        (doc, "The HW accumulation parameters of the detector"))

    BOOST_ANNOTATE_MEMBER(acq_params, vid,
        (desc, "video"),
        (doc, "The HW vid parameters of the detector"))

    BOOST_ANNOTATE_MEMBER(acq_params, xfer,
        (desc, "transfer"),
        (doc, "The HW transfer parameters of the detector"))
    // clang-format on

} // namespace hw
} // namespace lima
