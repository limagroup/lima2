// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

// A few addition type traits for convenience

namespace lima
{
/// member_type traits
namespace detail
{
    template <class Pm>
    struct member_type_impl
    {
    };

    template <class M, class C>
    struct member_type_impl<M C::*>
    {
        using type = M;
    };

    template <class M, class C>
    struct member_type_impl<M C::*const>
    {
        using type = M;
    };
} // namespace detail

/// Returns the type of member for a given pointer to member type
template <class Pm>
using member_type = typename detail::member_type_impl<Pm>::type;

} //namespace lima
