// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <iostream>
#include <functional>
#include <string>
#include <type_traits>
#include <vector>

namespace lima
{
template <typename T>
class observable
{
  public:
    using value_type = T;
    using callback_t = std::function<void(const T&)>;

    observable() = default;

    observable(const T& initial_val) noexcept(std::is_nothrow_move_constructible<T>::value) : m_val(initial_val) {}

    /// Observable values are **not** copy-constructible.
    observable(const observable& rhs) = delete;

    /// Observable values are move-constructible.
    template <typename = std::enable_if_t<std::is_move_constructible<T>::value>>
    observable(observable&& other) noexcept(std::is_nothrow_move_constructible<T>::value) :
        m_val(std::move(other.m_val)), m_callbacks(std::move(other.m_callbacks))
    {
    }

    /// Observable values are **not** copy-assignable.
    observable& operator=(const observable& rhs) = delete;

    /// Observable values are move-assignable.
    template <typename = std::enable_if_t<std::is_move_assignable<T>::value>>
    observable& operator=(observable&& rhs) noexcept(std::is_nothrow_move_assignable<T>::value)
    {
        m_val = std::move(rhs.m_val);
        m_callbacks = std::move(rhs.m_callbacks);
    }

    /// Set a new value. Will just call set(const T&) or set(T&&),
    /// depending on the passed argument
    ///
    /// \see set(const T&)
    template <typename T0, typename = std::enable_if_t<std::is_convertible<T0, T>::value>>
    observable& operator=(T0&& rhs)
    {
        set(std::forward<T0>(rhs));
        return *this;
    }

    /// Retrieve the stored value.
    T const& get() const noexcept { return m_val; }

    /// Set a new value, possibly notifying any subscribed observers.
    ///
    /// If the new value compares equal to the existing value, this method has no
    /// effect.
    ///
    /// \param new_val The new value to set.
    /// \throw readonly_value if the value has an associated updater.
    /// \see subject<void(Args ...)>::notify()
    void set(const T& new_val)
    {
        if (m_val == new_val)
            return;

        m_val = new_val;
        notify(m_val);
    }

    /// Set a new, movable value
    ///
    /// \see set(const T&)
    void set(T&& new_val)
    {
        if (m_val == new_val)
            return;

        m_val = std::move(new_val);
        notify(m_val);
    }

    /// Subscribe to changes to the observable value.
    template <typename Callable>
    void subscribe(Callable&& callback) const
    {
        m_callbacks.emplace_back(std::forward<Callable>(callback));
    }

    /// Convert the observable value to its stored value type.
    explicit operator T const&() const { return m_val; }

  private:
    void notify(const T& val)
    {
        for (auto&& callback : m_callbacks)
            callback(val);
    }

    T m_val;
    mutable std::vector<callback_t> m_callbacks;
};

template <typename OutputStreamable>
inline std::ostream& operator<<(std::ostream& os, const lima::observable<OutputStreamable>& o)
{
    os << o.get();
    return os;
}

} //namespace lima
