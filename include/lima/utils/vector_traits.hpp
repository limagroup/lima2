// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <type_traits>

// A vector type traits for convenience

namespace std
{
template <typename T, typename Allocator>
class vector;
}

namespace lima
{
template <typename T>
struct is_vector : std::false_type
{
};

template <typename T, typename Allocator>
struct is_vector<std::vector<T, Allocator>> : std::true_type
{
};

template <typename T>
inline constexpr bool is_vector_v = is_vector<T>::value;

} //namespace lima
