

// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <iterator>
#include <filesystem>
#include <sstream>
#include <string>

#include <boost/algorithm/string/trim.hpp>
#include <boost/regex.hpp>

namespace lima
{
inline std::string glob_to_regex(std::string val)
{
    boost::trim(val);
    const char* expression = "(\\*)|(\\?)|([[:blank:]])|(\\.|\\+|\\^|\\$|\\[|\\]|\\(|\\)|\\{|\\}|\\\\)";
    const char* format = "(?1\\\\w+)(?2\\.)(?3\\\\s*)(?4\\\\$&)";

    std::stringstream final;
    final << "^";

    std::ostream_iterator<char, char> oi(final);
    boost::regex re;
    re.assign(expression);
    boost::regex_replace(oi, val.begin(), val.end(), re, format, boost::match_default | boost::format_all);

    final << "$";
    return final.str();
}

// Iteratate over the file in the given directory and insert the path to the ouput iterator
//for the ones that matches the given pattern
template <typename OutputIterator>
inline void glob(std::filesystem::path const& path, std::string pattern, OutputIterator out)
{
    const boost::regex r(glob_to_regex(pattern));

    std::filesystem::directory_iterator end_itr; // Default ctor yields past-the-end
    for (std::filesystem::directory_iterator itr(path); itr != end_itr; ++itr) {
        // Skip if not a file
        if (!std::filesystem::is_regular_file(itr->status()))
            continue;

        if (!boost::regex_match(itr->path().filename().string(), r))
            continue;

        // File matches, store it
        out = itr->path();
    }
}

} //namespace lima
