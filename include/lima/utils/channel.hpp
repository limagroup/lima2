// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cassert>
#include <condition_variable>
#include <exception>
#include <memory>
#include <optional>
#include <queue>
#include <type_traits>
#include <utility>

namespace lima
{
namespace detail
{
    template <typename T>
    class unbuffered_storage_policy
    {
      public:
        using value_type = typename std::remove_reference<T>::type;

        template <typename U>
        void set(U&& value)
        {
            m_value.emplace(std::forward<U>(value));
        }

        T get()
        {
            T val(std::move(*m_value));
            m_value.reset();
            return val;
        }

        operator bool() const { return bool(m_value); }

        void reset() { m_value.reset(); }

      private:
        std::optional<value_type> m_value;
    };

    template <typename T>
    class buffered_storage_policy
    {
      public:
        using value_type = typename std::remove_reference<T>::type;

        template <typename U>
        void set(U&& value)
        {
            m_values.emplace(std::forward<U>(value));
        }

        T get()
        {
            T val(std::move(m_values.front()));
            m_values.pop();
            return val;
        }

        operator bool() const { return !m_values.empty(); }

        void reset() { m_values.clear(); }

      private:
        std::queue<value_type> m_values;
    };

} //namespace detail

namespace channel
{
    /// Encaspsulates either the value or exception transmitted through the channel
    ///
    /// \tparam T The type of data send through the channel
    /// \tparam StoragePolicy The storage policy (could be Unbuffered or Buffered)
    template <typename T, template <typename> typename StoragePolicy>
    class state
    {
      public:
        using value_type = typename StoragePolicy<T>::value_type;
        using storage_t = StoragePolicy<T>;

        void reset()
        {
            lock_guard_t lock(m_mutex);
            m_value.reset();
            m_error = nullptr;
        }

        /// Check whether a value is available
        bool is_ready() const
        {
            lock_guard_t lock(m_mutex);
            return is_ready_unguarded();
        }

        /// Blocking variant of get
        T get()
        {
            unique_lock_t ul(m_mutex);
            m_available.wait(ul, [&] { return is_ready_unguarded(); });

            return get_or_throw();
        }

        /// Non-blocking variant of get
        std::optional<T> try_get()
        {
            std::optional<T> res;

            lock_guard_t lock(m_mutex);
            if (!is_ready_unguarded())
                return res;

            res.emplace(get_or_throw());
            return res;
        }

        /// Wait for a value to be available
        void wait() const
        {
            unique_lock_t ul(m_mutex);
            m_available.wait(ul, [&] { return is_ready_unguarded(); });
        }

        /// Wait for a value to be available with a given timeout
        template <typename Rep, typename Ratio>
        bool wait_for(std::chrono::duration<Rep, Ratio> rel_time) const
        {
            unique_lock_t ul(m_mutex);
            return m_available.wait_for(ul, rel_time, [&] { return is_ready_unguarded(); });
        }

        template <typename U>
        void set_value(U&& value)
        {
            lock_guard_t lock(m_mutex);
            m_value.set(std::forward<U>(value));
            m_available.notify_all();
        }

        void set_exception(std::exception_ptr e)
        {
            lock_guard_t lock(m_mutex);
            m_error = e;
            m_available.notify_all();
        }

      private:
        using lock_guard_t = std::lock_guard<std::mutex>;
        using unique_lock_t = std::unique_lock<std::mutex>;

        bool is_ready_unguarded() const { return m_value || m_error; }

        T get_or_throw()
        {
            if (m_value)
                return m_value.get();

            std::exception_ptr error = m_error;
            m_error = nullptr;
            std::rethrow_exception(error);
        }

        mutable std::mutex m_mutex;
        mutable std::condition_variable m_available;
        storage_t m_value;
        std::exception_ptr m_error;
    };

    /// Unbuffered state alias
    template <typename T>
    using unbuffered_state = state<T, detail::unbuffered_storage_policy>;

    /// Buffered state alias
    template <typename T>
    using buffered_state = state<T, detail::buffered_storage_policy>;

    // Forward definition
    template <typename T, typename State>
    class sender;

    /// The receiver side of the channel
    template <typename T, typename State = unbuffered_state<T>>
    class receiver
    {
        using shared_state_t = std::shared_ptr<State>;

      public:
        receiver() noexcept = default;
        receiver(receiver&&) noexcept = default;
        receiver(receiver const& other) = delete;

        ~receiver() = default;

        receiver& operator=(receiver&& other) noexcept = default;
        receiver& operator=(receiver const& other) = delete;

        /// Try to returns a received value if any (non-blocking)
        std::optional<T> try_get() { return m_state->try_get(); }

        /// Returns a received value if any (blocking)
        T get() { return m_state->get(); }

        /// Returns true if this receiver has a valid state shared with the sender
        bool valid() const noexcept { return m_state; }

        /// Returns true if a value is available
        bool is_ready() const { return m_state->is_ready(); }

        /// Wait for a value to be available
        void wait() const { m_state->wait(); }

        /// Wait for a value to be available with a given timeout
        template <typename Rep, typename Ratio>
        bool wait_for(std::chrono::duration<Rep, Ratio> rel_time) const
        {
            return m_state->wait_for(rel_time);
        }

        /// Reset the state
        void reset() { m_state->reset(); }

      private:
        friend class sender<T, State>;
        receiver(shared_state_t const& state) : m_state(state) {}

        shared_state_t m_state;
    };

    /// The sender side of the channel
    template <typename T, typename State = unbuffered_state<T>>
    class sender
    {
        using shared_state_t = std::shared_ptr<State>;

      public:
        sender() : m_state(std::make_shared<State>()) {}
        template <typename Alloc>
        sender(std::allocator_arg_t, Alloc const& alloc) : m_state(std::allocate_shared<State>(alloc))
        {
        }
        sender(sender&& other) noexcept = default;
        sender(sender const& other) = delete;

        ~sender() = default;

        sender& operator=(sender&& other) noexcept = default;
        sender& operator=(sender const& rhs) = delete;

        /// Returns a receiver that shares state with this sender
        receiver<T, State> get_receiver() { return {m_state}; }

        /// Send a value through the channel
        template <typename U>
        void set_value(U&& value)
        {
            m_state->set_value(std::forward<U>(value));
        }

      private:
        shared_state_t m_state;
    };

    /// Create an unbuffered channel (a pair of sender, receiver)
    template <typename T>
    inline auto make_unbuffered_channel()
    {
        sender<T, unbuffered_state<T>> res;
        return std::make_pair(std::move(res), res.get_receiver());
    }

    /// Create a buffered channel (a pair of sender, receiver)
    template <typename T>
    inline auto make_buffered_channel()
    {
        sender<T, buffered_state<T>> res;
        return std::make_pair(std::move(res), res.get_receiver());
    }

} // namespace channel
} //namespace lima

namespace std
{
template <typename T, typename Alloc>
struct uses_allocator<lima::channel::sender<T>, Alloc> : true_type
{
};

} //namespace std
