// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstddef>

namespace lima
{
/// Returns the length of a C style array (that has not decayed into a pointer)
template <typename T, std::size_t sz>
inline constexpr std::size_t lengthof(T (&)[sz])
{
    return sz;
}

} //namespace lima
