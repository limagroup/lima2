// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <variant>

namespace lima
{
namespace detail
{
    template <std::size_t N>
    struct construct_matched_t
    {
        template <typename Variant, typename Pred>
        static bool apply(Variant& v, Pred pred)
        {
            if (pred.template apply<typename std::variant_alternative<N - 1, Variant>::type>()) {
                v.template emplace<N - 1>();
                return true;
            } else
                return construct_matched_t<N - 1>::apply(v, pred);
        }
    };

    template <>
    struct construct_matched_t<0>
    {
        template <typename Variant, typename Pred>
        static bool apply(Variant&, Pred)
        {
            return false;
        }
    };

    template <typename Variant, std::size_t N>
    struct for_each_t
    {
        template <typename Function>
        static Function apply(Function func)
        {
            typedef typename std::variant_alternative<N - 1, Variant>::type x;
            func.template apply<x>();
            return for_each_t<Variant, N - 1>::apply(func);
        }
    };

    template <typename Variant>
    struct for_each_t<Variant, 0>
    {
        template <typename Function>
        static Function apply(Function func)
        {
            return func;
        }
    };

} // namespace detail

/// Construct a variant in-place according to the given predicate
template <typename Variant, typename Pred>
bool construct_matched(Variant& v, Pred pred)
{
    return detail::construct_matched_t<std::variant_size<Variant>::value>::apply(v, pred);
}

/// For each type of the variant apply a given nullary function
template <typename Variant, typename Function>
Function for_each(Function func)
{
    return detail::for_each_t<Variant, std::variant_size<Variant>::value>::apply(func);
}

} //namespace lima
