// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>
#include <cstdint>

namespace lima
{
namespace base64
{
    inline static const char encode_lut[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    inline static const char pad_char = '=';

    inline std::string encode(std::uint8_t const* const input, std::size_t size)
    {
        std::string encoded;
        encoded.reserve(((size / 3) + (size % 3 > 0)) * 4);

        std::uint32_t temp{};
        auto it = input;

        for (std::size_t i = 0; i < size / 3; ++i) {
            temp = (*it++) << 16;
            temp += (*it++) << 8;
            temp += (*it++);
            encoded.append(1, encode_lut[(temp & 0x00FC0000) >> 18]);
            encoded.append(1, encode_lut[(temp & 0x0003F000) >> 12]);
            encoded.append(1, encode_lut[(temp & 0x00000FC0) >> 6]);
            encoded.append(1, encode_lut[(temp & 0x0000003F)]);
        }

        switch (size % 3) {
        case 1:
            temp = (*it++) << 16;
            encoded.append(1, encode_lut[(temp & 0x00FC0000) >> 18]);
            encoded.append(1, encode_lut[(temp & 0x0003F000) >> 12]);
            encoded.append(2, pad_char);
            break;
        case 2:
            temp = (*it++) << 16;
            temp += (*it++) << 8;
            encoded.append(1, encode_lut[(temp & 0x00FC0000) >> 18]);
            encoded.append(1, encode_lut[(temp & 0x0003F000) >> 12]);
            encoded.append(1, encode_lut[(temp & 0x00000FC0) >> 6]);
            encoded.append(1, pad_char);
            break;
        }

        return encoded;
    }

} // namespace base64
} // namespace lima
