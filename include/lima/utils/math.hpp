// Copyright (C) 2022 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace lima
{
inline unsigned int digits_base2(unsigned int x)
{
    return x ? 32 - __builtin_clz(x) : 0;
}

/// Returns 0 for x = 0, 1 for x = 1...9, 2 for 10...99, 3 for 100...999
inline unsigned int digits_base10(unsigned int x)
{
    static const unsigned char guess[33] = {0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4,
                                            5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9};
    static const unsigned int ten_to_the[] = {
        1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000,
    };
    unsigned int digits = guess[digits_base2(x)];
    return digits + (x >= ten_to_the[digits]);
}

inline unsigned int order_of_magnitude_base10(unsigned int x)
{
    static const unsigned int ten_to_the[] = {
        1, 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000,
    };

    int digits = digits_base10(x);

    return ten_to_the[digits];
}

} // namespace lima
