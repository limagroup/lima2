// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <functional>
#include <memory>
#include <type_traits>
#include <utility>

namespace lima
{
namespace raii
{
    template <typename T>
    struct not_nullptr
    {
        static bool is_valid(T const& h) noexcept { return h != nullptr; }
        static std::nullptr_t invalid_value() noexcept { return nullptr; }
    };

    template <typename T>
    struct not_negative
    {
        static bool is_valid(T const& h) noexcept { return h >= 0; }
        static T invalid_value() noexcept { return -1; }
    };
} //namespace raii

/// Smart handle with unique ownership
/// \tparam T A handle type
/// \tparam CloseFunctionType The handle close function type
/// \tparam close The close function pointer
/// \tparam ValidPolicy A policy that specify the validity domain of the handle
template <typename T, typename CloseFunctionType, CloseFunctionType close,
          template <class> typename ValidPolicy = raii::not_nullptr>
class unique_handle final
{
    static_assert(std::is_trivial_v<T>, "handle type must be trivial");

  public:
    /// Alias for handle type
    using handle_t = T;

    /// Alias for ValidPolicy
    using valid = ValidPolicy<T>;

    /// Returns true if handle is valid
    explicit operator bool() const { return valid::is_valid(m_handle); }

    /// Gets a copy of the handle's value
    [[nodiscard]] handle_t get() const noexcept { return m_handle; }

    /// Copy semantics
    unique_handle(unique_handle const& rhs) = delete;
    unique_handle& operator=(unique_handle const& rhs) = delete;

    /// Move semantics
    unique_handle(unique_handle&& rhs) : m_handle(std::move(rhs.m_handle)) { rhs.m_handle = valid::invalid_value(); }

    unique_handle& operator=(unique_handle&& rhs) noexcept
    {
        if (this != std::addressof(rhs)) {
            if (valid::is_valid(m_handle))
                std::invoke(close, m_handle);
            m_handle = std::move(rhs.m_handle);
            rhs.m_handle = valid::invalid_value();
        }
        return *this;
    }

    /// Default construction
    unique_handle() noexcept : m_handle(valid::invalid_value()){};

    /// Construction
    unique_handle(handle_t other) noexcept : m_handle(other) {}

    /// Close the handle if valid
    ~unique_handle() noexcept
    {
        if (valid::is_valid(m_handle))
            std::invoke(close, m_handle);
    }

  private:
    /// handle to the resource
    handle_t m_handle;
};

/// Smart handle with shared ownership
/// \tparam Unique A unique_handle type
template <typename Unique>
class shared_handle final
{
    using unique_t = Unique;

  public:
    /// Alias for handle type
    using handle_t = typename Unique::handle_t;

    /// Alias for ValidPolicy
    using valid = typename unique_t::valid;

    /// Returns true is handle is valid
    explicit operator bool() const { return (m_ptr && (bool) *m_ptr); }

    /// Gets a copy of the handle's value
    [[nodiscard]] handle_t get() const noexcept { return (m_ptr ? m_ptr->get() : valid::invalid_value()); }

    /// Returns number of coowners of this handle
    [[nodiscard]] long use_count() const noexcept { return m_ptr.use_count(); }

    /// Copy semantics
    shared_handle(shared_handle const& rhs) = default;
    shared_handle& operator=(shared_handle const& rhs) = default;

    /// Move semantics
    shared_handle(shared_handle&& rhs) = default;
    shared_handle& operator=(shared_handle&& rhs) = default;

    /// Default construction
    shared_handle() = default;

    /// Construction from handle type
    shared_handle(handle_t h)
    {
        if (valid::is_valid(h))
            m_ptr = std::make_shared<unique_t>(h);
    }

    /// Construction from unique handle
    shared_handle(unique_t&& rhs)
    {
        if (rhs)
            m_ptr = std::make_shared<unique_t>(std::move(rhs));
    }

  private:
    /// A pointer to a handle to the resource
    std::shared_ptr<unique_t> m_ptr;
};

} //namespace lima
