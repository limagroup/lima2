// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/hana.hpp>

namespace lima
{
namespace hana = boost::hana;

// Warning: boost::hana::for_each() makes copies of the members of the struct
// Use accessors instead
template <class Foldable, class F>
void reflect_traverse(Foldable&& r, F f)
{
    constexpr auto accessors = hana::accessors<std::decay_t<Foldable>>();
    hana::for_each(accessors, [&](auto&& x) {
        auto getter = hana::second(x);
        f(hana::to<const char*>(hana::first(x)), getter(std::forward<Foldable>(r)));
    });
}

namespace details
{
    constexpr auto is_struct = hana::trait<hana::Struct>;

    const auto visit_pre_order = hana::fix([](auto self, auto id, auto name, auto value, auto fn) {
        using namespace hana::literals;

        if constexpr (is_struct(hana::typeid_(value))) {
            //fn(id, hana::to<const char *>(name), value);

            using type = decltype(value);
            constexpr auto fields = hana::accessors<type>();
            constexpr auto n_fields = hana::size(fields);

            return hana::fold_left(hana::make_range(0_c, n_fields), id + 1_c, [&](auto id_j, auto j) {
                const auto field = hana::at(fields, j);
                const auto field_name = hana::first(field);
                const auto field_value = hana::second(field)(value);

                return self(id_j, field_name, field_value, fn);
            });
        } else {
            fn(id, hana::to<const char*>(name), value);
            return id + 1_c;
        }
    });
} // namespace details

/// Recursively traverse a tree of Hana.Struct
template <class Foldable, class F>
void deep_reflect_traverse(Foldable&& r, F f)
{
    using namespace hana::literals;

    details::visit_pre_order(0_c, BOOST_HANA_STRING("root"), std::forward<Foldable>(r), f);
}

} //namespace lima
