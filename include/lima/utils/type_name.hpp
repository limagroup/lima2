// Is it possible to print a variable's type in standard C++?
// https://stackoverflow.com/questions/81870/is-it-possible-to-print-a-variables-type-in-standard-c
//
// Solution from Howard Hinnant

#pragma once

#include <string_view>

namespace lima
{
template <class T>
constexpr std::string_view type_name()
{
    using namespace std;
#ifdef __clang__
    string_view p = __PRETTY_FUNCTION__;
    return string_view(p.data() + 34, p.size() - 34 - 1);
#elif defined(__GNUC__)
    string_view p = __PRETTY_FUNCTION__;
#if __cplusplus < 201402
    return string_view(p.data() + 36, p.size() - 36 - 1);
#else
    return string_view(p.data() + 49, p.find(';', 49) - 49);
#endif
#elif defined(_MSC_VER)
    string_view p = __FUNCSIG__;
    return string_view(p.data() + 90, p.size() - 90 - 7);
#endif
}

} //namespace lima
