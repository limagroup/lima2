// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <stdexcept>
#include <utility>

namespace lima
{
namespace detail
{
    template <typename Base>
    struct wrapper_base
    {
        using return_type = Base;
        virtual ~wrapper_base() {}
    };

    template <typename Base, typename... Args>
    class wrapper : public wrapper_base<Base>
    {
      public:
        using function_type = std::function<typename wrapper_base<Base>::return_type(Args...)>;

        template <typename T>
        wrapper(T&& func) : m_func(std::forward<T>(func))
        {
        }

        template <typename... Args2>
        typename function_type::result_type operator()(Args2&&... args) const
        {
            return m_func(std::forward<Args2>(args)...);
        }

      protected:
        function_type m_func;
    };

    // For generic types that are functors, delegate to its 'operator()'
    template <typename T>
    struct function_traits : public function_traits<decltype(&T::operator())>
    {
    };

    // for pointers to member function
    template <typename ClassType, typename ReturnType, typename... Args>
    struct function_traits<ReturnType (ClassType::*)(Args...) const>
    {
        //enum { arity = sizeof...(Args) };
        typedef std::function<ReturnType(Args...)> f_type;
    };

    // for pointers to member function
    template <typename ClassType, typename ReturnType, typename... Args>
    struct function_traits<ReturnType (ClassType::*)(Args...)>
    {
        typedef std::function<ReturnType(Args...)> f_type;
    };

    // for function pointers
    template <typename ReturnType, typename... Args>
    struct function_traits<ReturnType (*)(Args...)>
    {
        typedef std::function<ReturnType(Args...)> f_type;
    };

    // make_function from Callable like lambdas
    template <typename L>
    typename function_traits<L>::f_type make_function(L l)
    {
        return (typename function_traits<L>::f_type)(l);
    }

} //namespace detail

template <typename Base, typename Key = std::string>
struct factory
{
    typedef Base return_type;

    template <typename... Args>
    return_type create(Key const& key, Args&&... args) const
    {
        auto ret = m_map.find(key);
        if (ret == m_map.end())
            throw std::runtime_error("unknown type");

        using wrapper_t = detail::wrapper<Base, Args...>;
        try {
            wrapper_t const& wrapper = dynamic_cast<wrapper_t const&>(*(ret->second));
            return return_type(wrapper(std::forward<Args>(args)...));
        } catch (std::bad_cast&) {
            throw std::runtime_error("Bad factory arguments");
        }
    }

    /// registers lvalue std::functions
    template <typename Return, typename... Args>
    void reg(Key const& key, std::function<Return(Args... args)> const& delegate)
    {
        m_map.emplace(key, std::make_unique<detail::wrapper<Base, Args...>>(delegate));
    }

    /// registers rvalue std::functions
    template <typename Return, typename... Args>
    void reg(Key const& key, std::function<Return(Args... args)>&& delegate)
    {
        m_map.emplace(key, std::make_unique<detail::wrapper<Base, Args...>>(delegate));
    }

    /// registers function pointer
    template <typename Return, typename... Args>
    void reg(Key const& key, Return (*delegate)(Args... args))
    {
        m_map.emplace(key, std::make_unique<detail::wrapper<Base, Args...>>(delegate));
    }

    /// registers lambdas
    template <typename Lambda>
    void reg(Key const& key, Lambda const& lambda)
    {
        reg(key, detail::make_function(lambda));
    }

  protected:
    using key_type = Key;
    using value_type = std::unique_ptr<detail::wrapper_base<Base>>;

    std::map<key_type, value_type> m_map;
};

} //namespace lima
