// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/hana.hpp>

namespace lima
{
namespace concepts
{
    // Metadata should define data member idx
    auto has_idx = boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).idx) {});

    // Metadata should define data member is_final
    auto has_is_final = boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).is_final) {});

    // Metadata should define data member is_valid
    auto has_is_valid = boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).is_valid) {});

    // Metadata
    auto is_metadata = [](auto t) { return has_idx(t) && has_is_valid(t) && has_is_final(t); };

} // namespace concepts
} //namespace lima
