// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/hana.hpp>

namespace lima
{
namespace concepts
{
    //template <typename T>
    //concept HwInterface = requires(T t)
    //{
    //    typename T::acq_params_t;
    //    typename T::xfer_params_t;
    //    typename T::proc_params_t;

    //    {
    //        t.get_info()
    //        } -> std::convertible_to<hw::info>;
    //    t.prepare(typename T::acq_params_t{});
    //    t.start();
    //    t.stop();
    //};

    // Type should define properties nested type
    constexpr auto has_properties =
        boost::hana::is_valid([](auto t) -> boost::hana::type<typename decltype(t)::type::properties> {});

    // Type should define init_params_t nested type
    constexpr auto has_init_params =
        boost::hana::is_valid([](auto t) -> boost::hana::type<typename decltype(t)::type::init_params_t> {});

    // Type should define acq_params_t nested type
    constexpr auto has_acq_params =
        boost::hana::is_valid([](auto t) -> boost::hana::type<typename decltype(t)::type::acq_params_t> {});

    // Type should define data_t nested type
    constexpr auto has_data_type =
        boost::hana::is_valid([](auto t) -> boost::hana::type<typename decltype(t)::type::data_t> {});

    // Type should define pixel_types static member
    constexpr auto has_pixel_types =
        boost::hana::is_valid([](auto t) -> decltype((void) decltype(t)::type::pixel_types) {});

    // Type should define capabilities static member
    constexpr auto has_capabilities =
        boost::hana::is_valid([](auto t) -> decltype((void) decltype(t)::type::capabilities) {});

    // Type should define info() member function
    constexpr auto has_info = boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).info()) {});

    // Type should define prepare(arg1, arg2, arg3) member function
    constexpr auto has_prepare = boost::hana::is_valid(
        [](auto t, auto p1) -> decltype(boost::hana::traits::declval(t).prepare(boost::hana::traits::declval(p1))) {});

    // Type should define start() member function
    constexpr auto has_start =
        boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).start()) {});

    // Type should define stop() member function
    constexpr auto has_stop = boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).stop()) {});

    // Type should define reset() member function
    constexpr auto has_reset =
        boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).reset()) {});

    // Type can define close() member function
    constexpr auto has_close =
        boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).close()) {});

    // Type should define validate_acq_params() member function
    constexpr auto has_validate_acq_params = boost::hana::is_valid(
        [](auto t, auto p1, auto p2) -> decltype(boost::hana::traits::declval(t).validate_acq_params(
                                         boost::hana::traits::declval(p1), boost::hana::traits::declval(p2))) {});

    // Type should define get_image_dimensions() member function
    constexpr auto has_get_image_dimensions =
        boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).get_image_dimensions()) {});

    // Type should define nb_images_acquired() member function
    constexpr auto has_nb_images_acquired =
        boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).nb_images_acquired()) {});

    /// Everything that is common to the control and acquisition parts of the camera.
    /// As of now, it is mostly defining types.
    template <typename Config>
    struct config
    {
        // Concept checking
        static_assert(has_properties(boost::hana::type_c<Config>), "Config requires properties nested type");
        static_assert(has_init_params(boost::hana::type_c<Config>), "Config requires init_params_t nested type");
        static_assert(has_acq_params(boost::hana::type_c<Config>), "Config requires acq_params_t nested type");
        static_assert(has_data_type(boost::hana::type_c<Config>), "Config requires data_t static member");
        static_assert(has_capabilities(boost::hana::type_c<Config>), "Config requires capabilities static member");
    };

} // namespace concepts
} // namespace lima
