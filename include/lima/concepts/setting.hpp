// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/hana.hpp>

namespace lima
{
namespace concepts
{
    /// Synopsis
    // struct Setting : Serializable
    // {
    // 	using value_type = int;
    //
    // 	inline static const value_type default_value = 1;
    // 	inline static const char * short_name = "nb_frames";
    // 	inline static const char* long_name = "number of frames";
    // 	inline static const char* doc = "The number of frames to acquire (-1 = continuous acquisition)";
    //
    // 	bool validate() const;
    // };

    // Setting should define nested type value_type
auto has_value_type = boost::hana::is_valid([](auto t) -> hana::type<
	typename decltype(t)::type::value_type)
> {});

// Setting should define member function validate()
auto has_validate = boost::hana::is_valid([](auto t) -> decltype(boost::hana::traits::declval(t).validate()) {});

// Setting should define static member default_value
auto has_default_value = hana::is_valid([](auto t) -> decltype((void) decltype(t)::type::default_value) {});

// Setting should define static member short_name
auto has_short_name = hana::is_valid([](auto t) -> decltype((void) decltype(t)::type::short_name) {});

// Setting should define static member long_name
auto has_long_name = hana::is_valid([](auto t) -> decltype((void) decltype(t)::type::long_name) {});

// Setting should define static member doc
auto has_doc = hana::is_valid([](auto t) -> decltype((void) decltype(t)::type::doc) {});

} // namespace concepts
} //namespace lima
