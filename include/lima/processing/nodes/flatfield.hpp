// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tbb/flow_graph.h>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/processing/serial/flatfield_correction.hpp>

namespace lima
{
namespace processing
{
    class flatfield_node : public tbb::flow::function_node<frame, frame>
    {
        using parent_t = tbb::flow::function_node<frame, frame>;

      public:
        flatfield_node(tbb::flow::graph& g, size_t concurrency, const frame& flatfield) :
            parent_t(g, concurrency, [flatfield](frame in) {
                for (int i = 0; i < in.nb_channels(); i++)
                    flatfield_correction(lima::view(in, i), lima::const_view<bpp32fc_view_t>(flatfield));
                return in;
            })
        {
        }
    };

} // namespace processing
} // namespace lima
