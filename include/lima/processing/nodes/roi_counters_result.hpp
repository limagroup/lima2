// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

#include <lima/processing/serial/counters.hpp>

namespace lima
{
namespace processing
{
    struct roi_counters_result
    {
        std::size_t frame_idx;             //!< Frame idx
        std::size_t recv_idx;              //!< Receiver idx required to re-sequence the frame
        std::vector<counters_result> rois; //<! A vector of counters_result for each ROIs
    };

} // namespace processing
} // namespace lima
