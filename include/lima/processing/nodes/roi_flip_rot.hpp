// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <optional>

#include <boost/mp11/algorithm.hpp>

#include <boost/gil/step_iterator.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>

#include <tbb/flow_graph.h>

#include <lima/core/frame_view.hpp>
#include <lima/core/rectangle.hpp>

#include <lima/processing/serial/roi_flip_rot.hpp>

namespace lima
{
namespace processing
{
    template <typename Frame>
    class roi_flip_rot_node : public tbb::flow::function_node<Frame, Frame>
    {
        using parent_t = tbb::flow::function_node<Frame, Frame>;

        struct roi_flip_rot_body
        {
            roi_flip_rot_body(std::optional<rectangle_t> const& roi, flip_t const& flip, rotation_t const& rotation) :
                m_roi(roi), m_flip(flip), m_rotation(rotation)
            {
            }

            Frame operator()(Frame const& in) const
            {
                auto transformed_view = roi_flip_rot(lima::const_view(in), m_roi, m_flip, m_rotation);

                //return boost::variant2::visit([in](auto&& v) {

                //    frame res(v.dimensions(), in.pixel_type());

                //    boost::gil::copy_pixels(v, lima::view(res));

                //    return res;
                //}, transformed_view);

                Frame res(transformed_view.dimensions(), in.pixel_type());

                boost::gil::copy_pixels(transformed_view, lima::view(res));

                return res;
            }

            std::optional<rectangle_t> m_roi;
            flip_t m_flip;
            rotation_t m_rotation;
        };

      public:
        roi_flip_rot_node(tbb::flow::graph& g, std::size_t concurrency, std::optional<rectangle_t> const& roi,
                          flip_t const& flip, rotation_t const& rotation) :
            parent_t(g, concurrency, roi_flip_rot_body(roi, flip, rotation))
        {
        }
    };

} // namespace processing
} // namespace lima
