// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tbb/flow_graph.h>

#include <boost/gil/extension/dynamic_image/algorithm.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/processing/serial/sum.hpp>

namespace lima
{
namespace processing
{
    class accumulation_node : public tbb::flow::multifunction_node<frame, std::tuple<frame>>
    {
        using parent_t = tbb::flow::multifunction_node<frame, std::tuple<frame>>;

      public:
        accumulation_node(tbb::flow::graph& g, int nb_frames, pixel_enum pixel) :
            parent_t(g, tbb::flow::serial,
                     [res = frame(), n = 0, nb_frames, pixel](frame in,
                                                              typename parent_t::output_ports_type& ports) mutable {
                         if (n == 0) {
                             res = frame(in.dimensions(), in.nb_channels(), pixel);
                             boost::gil::copy_and_convert_pixels(lima::const_view(in), lima::view(res));
                             res.metadata.idx = in.metadata.idx / nb_frames;
                         } else
                             sum(lima::const_view(in), lima::view(res));

                         n++;

                         if (n % nb_frames == 0 || in.metadata.is_final) {
                             // TODO Reduction operation
                             res.metadata.is_final = in.metadata.is_final;
                             std::get<0>(ports).try_put(res);
                             n = 0;
                         }
                     })
        {
        }
    };

} // namespace processing
} // namespace lima
