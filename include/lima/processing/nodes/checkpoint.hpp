// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <atomic>

#include <tbb/flow_graph.h>

namespace lima
{
namespace processing
{
    template <typename Input>
    class checkpoint_node : public tbb::flow::function_node<Input, int, tbb::flow::lightweight>
    {
        using parent_t = tbb::flow::function_node<Input, int, tbb::flow::lightweight>;

      public:
        checkpoint_node(tbb::flow::graph& g, size_t concurrency, std::atomic_int& counter, int increment = 1) :
            parent_t(g, concurrency, [&counter, increment](auto) mutable { return counter += increment; })
        {
        }
    };

} // namespace processing
} // namespace lima
