// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tbb/flow_graph.h>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/processing/serial/mask.hpp>

namespace lima
{
namespace processing
{
    class mask_node : public tbb::flow::function_node<frame, frame>
    {
        using parent_t = tbb::flow::function_node<frame, frame>;

      public:
        mask_node(tbb::flow::graph& g, size_t concurrency, const frame& mask_arg) :
            parent_t(g, concurrency, [mask_arg](frame in) {
                for (int i = 0; i < in.nb_channels(); i++)
                    mask(lima::view(in, i), lima::const_view<bpp8c_view_t>(mask_arg));

                return in;
            })
        {
        }
    };

} // namespace processing
} // namespace lima
