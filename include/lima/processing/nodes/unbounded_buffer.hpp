// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <mutex>
#include <optional>
#include <vector>

#include <tbb/flow_graph.h>

namespace lima
{
namespace processing
{
    // Thread safe vector
    template <typename Input>
    struct ts_vector
    {
        using vector_t = std::vector<Input>;

        ts_vector(std::size_t capacity) { buffer.reserve(capacity); }

        void push_back(Input const& in)
        {
            const std::lock_guard<std::mutex> lock(mutex);
            buffer.push_back(in);
        }

        std::optional<Input> front() const
        {
            std::optional<Input> res;

            const std::lock_guard<std::mutex> lock(mutex);

            // If buffer is empty, returns early
            if (buffer.empty())
                return res;

            return buffer.front();
        }

        std::optional<Input> back() const
        {
            std::optional<Input> res;

            const std::lock_guard<std::mutex> lock(mutex);

            // If buffer is empty, returns early
            if (buffer.empty())
                return res;

            return buffer.back();
        }

        template <typename UnaryPredicate>
        std::optional<Input> find(UnaryPredicate predicate) const
        {
            std::optional<Input> res;

            const std::lock_guard<std::mutex> lock(mutex);

            // If buffer is empty, returns early
            if (buffer.empty())
                return res;

            // TODO binary search?
            auto it = std::find_if(buffer.begin(), buffer.end(), predicate);
            if (it != buffer.end())
                res = *it;

            return res;
        }

        std::size_t size() const
        {
            const std::lock_guard<std::mutex> lock(mutex);
            return buffer.size();
        }

        vector_t buffer;
        mutable std::mutex mutex;
    };

    template <typename Input>
    class unbounded_buffer_node : public tbb::flow::function_node<Input, Input>
    {
        using parent_t = tbb::flow::function_node<Input, Input>;
        using vector_t = ts_vector<Input>;

      public:
        unbounded_buffer_node(tbb::flow::graph& g, std::size_t concurrency, vector_t& buffer) :
            m_buffer(buffer), parent_t(g, concurrency, [this](Input const& in) {
                m_buffer.push_back(in);
                return in;
            })
        {
        }

        vector_t& m_buffer;
    };

} // namespace processing
} // namespace lima
