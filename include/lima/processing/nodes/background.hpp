// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tbb/flow_graph.h>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/processing/serial/background_substraction.hpp>

namespace lima
{
namespace processing
{
    class background_node : public tbb::flow::function_node<frame, frame>
    {
        using parent_t = tbb::flow::function_node<frame, frame>;

      public:
        background_node(tbb::flow::graph& g, size_t concurrency, const frame& background) :
            parent_t(g, concurrency, [background](frame in) {
                for (int i = 0; i < in.nb_channels(); i++)
                    background_substraction(lima::view(in, i), lima::const_view(background));
                return in;
            })
        {
        }
    };

} // namespace processing
} // namespace lima
