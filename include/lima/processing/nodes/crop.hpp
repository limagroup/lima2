// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tbb/flow_graph.h>

#include <boost/gil/extension/dynamic_image/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/image_view_factory.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/core/rectangle.hpp>

namespace lima
{
namespace processing
{
    class crop_node : public tbb::flow::function_node<frame, frame>
    {
        using parent_t = tbb::flow::function_node<frame, frame>;

      public:
        crop_node(tbb::flow::graph& g, size_t concurrency, rectangle_t const& roi) :
            parent_t(g, concurrency, [roi](frame in) {
                frame res(roi.dimensions, in.nb_channels(), in.pixel_type() /*, TODO alignement */);
                for (int i = 0; i < in.nb_channels(); i++)
                    boost::gil::copy_pixels(
                        boost::gil::subimage_view(lima::const_view(in, i), roi.topleft, roi.dimensions),
                        lima::view(res, i));

                return res;
            })
        {
        }
    };

} // namespace processing
} // namespace lima
