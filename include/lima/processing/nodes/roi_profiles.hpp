// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <ranges>
#include <vector>

#include <tbb/flow_graph.h>

#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/extension/dynamic_image/image_view_factory2.hpp> // For transposed_view2

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/core/arc.hpp>
#include <lima/core/enums.hpp>
#include <lima/core/rectangle.hpp>
#include <lima/processing/serial/reduction_1d.hpp>
#include <lima/processing/nodes/roi_profiles_result.hpp>

namespace lima
{
namespace processing
{
    template <typename Frame>
    class roi_profiles_node : public tbb::flow::function_node<Frame, roi_profiles_result>
    {
        using parent_t = tbb::flow::function_node<Frame, roi_profiles_result>;

        struct roi_profiles_body
        {
            roi_profiles_body(std::vector<rectangle_t> const& rois, std::vector<direction_enum> const& dirs) :
                m_rois(rois), m_dirs(dirs)
            {
                assert(m_rois.size() == m_dirs.size());
            }

            roi_profiles_result operator()(Frame const& in)
            {
                std::vector<reduction_1d_result> res(m_rois.size());

                std::ranges::copy(std::ranges::zip_transform_view(
                                      [v = lima::const_view(in)](auto&& roi, auto&& dir) {
                                          if (dir == direction_enum::vertical)
                                              return reduction_1d(
                                                  boost::gil::subimage_view(v, roi.topleft, roi.dimensions));
                                          if (dir == direction_enum::horizontal)
                                              return reduction_1d(boost::gil::transposed_view2(
                                                  boost::gil::subimage_view(v, roi.topleft, roi.dimensions)));

                                          assert(false);
                                      },
                                      m_rois, m_dirs),
                                  res.begin());

                return {in.metadata.idx, in.metadata.recv_idx, res};
            }

            std::vector<rectangle_t> const& m_rois;    //!< Rectangle ROIs
            std::vector<direction_enum> const& m_dirs; //!< Reduction directions
        };

      public:
        roi_profiles_node(tbb::flow::graph& g, size_t concurrency, std::vector<rectangle_t> const& rois,
                          std::vector<direction_enum> const& directions) :
            parent_t(g, concurrency, roi_profiles_body(rois, directions))
        {
        }
    };

} // namespace processing
} // namespace lima
