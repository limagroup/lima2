// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <mutex>
#include <optional>

#include <boost/circular_buffer.hpp>

#include <tbb/flow_graph.h>

namespace lima
{
namespace processing
{
    // Thread safe circular_buffer
    template <typename Input>
    struct ts_circular_buffer
    {
        using circular_buffer_t = boost::circular_buffer<Input>;

        ts_circular_buffer(std::size_t count) : buffer(count) {}

        void set_capacity(circular_buffer_t::capacity_type new_capacity) { buffer.set_capacity(new_capacity); }

        void push_back(Input const& in)
        {
            const std::lock_guard<std::mutex> lock(mutex);
            buffer.push_back(in);
        }

        std::vector<Input> pop(std::size_t nb_frames)
        {
            std::vector<Input> res;

            const std::lock_guard<std::mutex> lock(mutex);

            // If buffer is empty, returns early
            if (buffer.empty())
                return res;

            std::size_t nb_elements = std::min(nb_frames, buffer.size());
            res.resize(nb_elements);

            // Copy to results and erase from buffer
            std::copy_n(buffer.begin(), nb_elements, res.begin());
            buffer.erase_begin(nb_elements);

            return res;
        }

        std::optional<Input> front() const
        {
            std::optional<Input> res;

            const std::lock_guard<std::mutex> lock(mutex);

            // If buffer is empty, returns early
            if (buffer.empty())
                return res;

            return buffer.front();
        }

        std::optional<Input> back() const
        {
            std::optional<Input> res;

            const std::lock_guard<std::mutex> lock(mutex);

            // If buffer is empty, returns early
            if (buffer.empty())
                return res;

            return buffer.back();
        }

        template <typename UnaryPredicate>
        std::optional<Input> find(UnaryPredicate predicate) const
        {
            std::optional<Input> res;

            const std::lock_guard<std::mutex> lock(mutex);

            // If buffer is empty, returns early
            if (buffer.empty())
                return res;

            // TODO binary search?
            auto it = std::find_if(buffer.begin(), buffer.end(), predicate);
            if (it != buffer.end())
                res = *it;

            return res;
        }

        std::size_t size() const
        {
            const std::lock_guard<std::mutex> lock(mutex);
            return buffer.size();
        }

        circular_buffer_t buffer;
        mutable std::mutex mutex;
    };

    template <typename Input>
    class circular_buffer_node : public tbb::flow::function_node<Input, Input>
    {
        using parent_t = tbb::flow::function_node<Input, Input>;
        using circular_buffer_t = ts_circular_buffer<Input>;

      public:
        circular_buffer_node(tbb::flow::graph& g, std::size_t concurrency, circular_buffer_t& buffer) :
            m_buffer(buffer), parent_t(g, concurrency, [this](Input const& in) {
                m_buffer.push_back(in);
                return in;
            })
        {
        }

        circular_buffer_t& m_buffer;
    };

} // namespace processing
} // namespace lima
