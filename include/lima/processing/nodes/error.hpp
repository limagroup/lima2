// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <atomic>

#include <boost/throw_exception.hpp>

#include <tbb/flow_graph.h>

namespace lima
{
namespace processing
{
    struct any
    {
        any() = default;
        template <typename T>
        any(T)
        {
        }
        any(any const&) = default;
    };

    class error_node : public tbb::flow::function_node<any, tbb::flow::continue_msg, tbb::flow::lightweight>
    {
        using parent_t = tbb::flow::function_node<any, tbb::flow::continue_msg, tbb::flow::lightweight>;

      public:
        error_node(tbb::flow::graph& g, size_t concurrency, std::atomic_int& counter, int threshold) :
            parent_t(g, concurrency, [&counter, threshold](auto) mutable {
                counter += 1;
                if (counter >= threshold)
                    BOOST_THROW_EXCEPTION(std::runtime_error("Processing error"));

                return tbb::flow::continue_msg{};
            })
        {
        }
    };

} // namespace processing
} // namespace lima
