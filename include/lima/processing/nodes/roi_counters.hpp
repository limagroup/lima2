// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

#include <tbb/flow_graph.h>

#include <boost/gil/extension/dynamic_image/any_image_view.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/core/arc.hpp>
#include <lima/core/rectangle.hpp>
#include <lima/processing/serial/counters.hpp>
#include <lima/processing/serial/generator.hpp>
#include <lima/processing/nodes/roi_counters_result.hpp>

namespace lima
{
namespace processing
{
    struct roi_counters_arc_bb_mask
    {
        rectangle_t bb;    //!< Bounding boxes of arc ROIs
        bpp8_image_t mask; //!< Mask of arc ROIs
    };

    // Base-from-Member Idiom
    struct roi_counters_node_pbase
    {
        roi_counters_node_pbase(std::vector<rectangle_t> const& rect_rois, std::vector<arc_t> const& arc_rois) :
            m_rect_rois(rect_rois), m_arc_rois(arc_rois.size())
        {
            std::transform(
                arc_rois.begin(), arc_rois.end(), m_arc_rois.begin(), [](auto&& arc) -> roi_counters_arc_bb_mask {
                    // Compute bounding box
                    rectangle_t bb = bounding_box(arc);
                    bpp8_image_t mask(bb.dimensions);

                    // Generate the mask
                    generator::generate_arc generator(arc);
                    boost::gil::copy_and_convert_pixels(
                        generator::make_generated_view(bb.topleft, bb.dimensions, generator), boost::gil::view(mask));

                    return {bb, mask};
                });
        }

        std::vector<rectangle_t> m_rect_rois;             //!< Rectangle ROIs
        std::vector<roi_counters_arc_bb_mask> m_arc_rois; //!< Arc ROIs
    };

    template <typename Frame>
    class roi_counters_node : private roi_counters_node_pbase,
                              public tbb::flow::function_node<Frame, roi_counters_result>
    {
        using parent_t = tbb::flow::function_node<Frame, roi_counters_result>;

        struct roi_counters_body
        {
            roi_counters_body(std::vector<rectangle_t> const& rect_rois,
                              std::vector<roi_counters_arc_bb_mask> const& arc_rois) :
                m_rect_rois(rect_rois), m_arc_rois(arc_rois)
            {
            }

            roi_counters_result operator()(Frame const& in)
            {
                std::vector<counters_result> res(m_rect_rois.size() + m_arc_rois.size());

                std::transform(m_rect_rois.begin(), m_rect_rois.end(), res.begin(),
                               [v = lima::const_view(in)](auto&& roi) {
                                   return counters(boost::gil::subimage_view(v, roi.topleft, roi.dimensions));
                               });

                std::transform(m_arc_rois.begin(), m_arc_rois.end(), res.begin() + m_rect_rois.size(),
                               [v = lima::const_view(in)](auto&& arc) {
                                   return counters(boost::gil::subimage_view(v, arc.bb.topleft, arc.bb.dimensions),
                                                   boost::gil::const_view(arc.mask));
                               });

                return {in.metadata.idx, in.metadata.recv_idx, res};
            }

            std::vector<rectangle_t> const& m_rect_rois;             //!< Rectangle ROIs
            std::vector<roi_counters_arc_bb_mask> const& m_arc_rois; //!< Arc ROIs
        };

      public:
        roi_counters_node(tbb::flow::graph& g, size_t concurrency, std::vector<rectangle_t> const& rect_rois,
                          std::vector<arc_t> const& arc_rois) :
            roi_counters_node_pbase(rect_rois, arc_rois),
            parent_t(g, concurrency, roi_counters_body(m_rect_rois, m_arc_rois))
        {
        }
    };

} // namespace processing
} // namespace lima
