// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>
#include <optional>
#include <atomic>

#include <tbb/flow_graph.h>

#include <boost/contract.hpp>
#include <boost/exception/errinfo_errno.hpp>
#include <boost/smart_ptr/make_shared.hpp> // For allocate_shared

#include <boost/gil/extension/dynamic_image/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/image_view_factory.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>

#include <lima/io/hdf5.hpp>
#include <lima/io/multi.hpp>

#include <lima/processing/serial/generator.hpp>

namespace lima
{
namespace processing
{
    template <typename Frame>
    class io_hdf5_node
        : private boost::contract::constructor_precondition<io_hdf5_node<Frame>>, // Contract (must come first)
          public tbb::flow::composite_node<
              std::tuple<Frame>, std::tuple<tbb::flow::continue_msg, tbb::flow::continue_msg>> // Composite node
    {
        using parent_t =
            tbb::flow::composite_node<std::tuple<Frame>, std::tuple<tbb::flow::continue_msg, tbb::flow::continue_msg>>;

        using dimensions_t = typename Frame::point_t;

        struct chunk
        {
            chunk() = default;
            chunk(Frame const& frm) :
                m_memory(frm.get_buffer_ptr()),
                m_data(frm.data),
                m_size_in_bytes(frm.total_size_in_bytes()),
                m_frame_idx(frm.metadata.recv_idx),
                m_is_final(frm.metadata.is_final)
            {
            }

            chunk(std::size_t size_in_bytes, std::size_t frame_idx, bool is_final) :
                m_size_in_bytes(size_in_bytes), m_frame_idx(frame_idx), m_is_final(is_final)
            {
                // TODO Use a better allocator
                std::allocator<std::byte> alloc;

                m_memory = boost::allocate_shared<std::byte[]>(alloc, size_in_bytes);
                m_data = m_memory.get();
            }

            void resize(std::size_t size_in_bytes)
            {
                // support only a sub-set of the current data
                assert(size_in_bytes <= m_size_in_bytes);
                m_size_in_bytes = size_in_bytes;
            }

            frame::buffer_ptr_t m_memory;      //<! A pointer with reference counting to the allocated memory
            void* m_data = {nullptr};          //<! Pointer to the beginning of the data
            std::size_t m_size_in_bytes = {0}; //<! Size of the chunk in bytes

            std::size_t m_frame_idx = 0; //!< Index in the frame sequence (starting from 0)
            bool m_is_final = false;     //!< true if the chunk is the last one in the sequence
        };

        using aggregate_node_t = tbb::flow::multifunction_node<Frame, std::tuple<Frame>>;
        using comp_node_t = tbb::flow::function_node<Frame, chunk>;
        using write_chunk_node_t =
            tbb::flow::multifunction_node<chunk, std::tuple<tbb::flow::continue_msg, tbb::flow::continue_msg>>;

        aggregate_node_t aggregate_node;
        std::optional<comp_node_t> comp_node;
        write_chunk_node_t write_chunk_node;

        struct aggregate_body
        {
            aggregate_body(int nb_frames_per_chunk) : m_nb_frames_per_chunk(nb_frames_per_chunk) {}

            void operator()(Frame const& in, typename aggregate_node_t::output_ports_type& ports)
            {
                // If new chunk required
                if (m_frame_counter == 0) {
                    // TODO: preallocate chunks in a pool
                    m_chunk = frame(in.width(), in.height() * m_nb_frames_per_chunk, in.pixel_type());
                    m_chunk.metadata = in.metadata;
                }

                // Append data to the chunk
                boost::gil::point_t top_left = {0, in.height() * m_frame_counter};
                boost::gil::copy_pixels(lima::const_view(in),
                                        boost::gil::subimage_view(lima::view(m_chunk), top_left, in.dimensions()));

                m_frame_counter++;

                // If chunk is full
                if (m_frame_counter == m_nb_frames_per_chunk) {
                    // Reset counter for next chunk
                    m_frame_counter = 0;

                    std::get<0>(ports).try_put(m_chunk);
                }

                // If this is the latest frame
                if (in.metadata.is_final) {
                    // Flag the chunk to final
                    m_chunk.metadata.is_final = true;

                    std::get<0>(ports).try_put(m_chunk);
                }
            }

            int m_nb_frames_per_chunk = 10; //<! the number of frames per chunk
            int m_frame_counter = 0;        //<! The number of frames already written in the chunk
            frame m_chunk;                  //<! A stack of frames stored vertically in one large chunk
        };

        struct comp_none_body
        {
            chunk operator()(Frame const& in) { return chunk(in); }
        };

        struct comp_bshuf_body
        {
            chunk operator()(Frame const& in)
            {
                size_t nb_pixels = in.size() * in.nb_channels();
                size_t elem_size = sizeof_pixel(in.pixel_type());
                size_t data_size = nb_pixels * elem_size;

                const size_t header_size = 12;

                // Process in blocks of this many elements. Pass 0 to select automatically(recommended).
                unsigned int block_size = 0;
                size_t bound_size = bshuf_compress_lz4_bound(nb_pixels, elem_size, block_size);

                // Allocate a new chunk
                chunk res(bound_size + header_size, in.metadata.recv_idx, in.metadata.is_final);

                std::uint8_t* comp_data = (std::uint8_t*) res.m_data;

                // Compress
                bshuf_write_uint64_BE(comp_data, data_size);
                comp_data += 8;
                bshuf_write_uint32_BE(comp_data, block_size);
                comp_data += 4;
                int64_t comp_size =
                    bshuf_compress_lz4((const void*) in.data, (void*) comp_data, nb_pixels, elem_size, block_size);
                if (comp_size < 0)
                    LIMA_THROW_EXCEPTION(lima::runtime_error("BSLZ4 Compression failed")
                                         << boost::errinfo_errno(comp_size));

                res.resize(comp_size + header_size);

                return res;
            }
        };

        struct comp_gzip_body
        {
            chunk operator()(Frame const& in)
            {
                size_t elem_size = sizeof_pixel(in.pixel_type());
                size_t data_size = in.size() * elem_size;

                // Process in blocks of this many elements.
                uLong bound_size = compressBound(data_size);

                // Allocate a new chunk
                chunk res(bound_size, in.metadata.recv_idx, in.metadata.is_final);

                Bytef* comp_data = (Bytef*) res.m_data;
                uLongf comp_size = bound_size;

                // Compress
                int ret =
                    compress2(comp_data, &comp_size, (const Bytef*) in.data, (uLong) data_size, compression_level);
                if (ret != Z_OK)
                    LIMA_THROW_EXCEPTION(lima::runtime_error("compress2 error") << boost::errinfo_errno(ret));

                res.resize(comp_size);

                return res;
            }

            unsigned int compression_level = 6;
        };

        struct write_chunk_body
        {
            using writer_t = io::multi_writer<io::h5::writer>;
            write_chunk_body(io::h5::saving_params const& sav_params, frame_info const& frame_info,
                             io::h5::nx_metadata_writer const& metadata_writer) :
                m_writer(std::make_shared<writer_t>(sav_params, frame_info, metadata_writer)),
                m_nb_frames_per_chunk(sav_params.nb_frames_per_chunk)
            {
            }

            void operator()(chunk const& in, typename write_chunk_node_t::output_ports_type& ports)
            {
                m_writer->write_chunk(in.m_data, in.m_size_in_bytes, in.m_frame_idx);

                ++m_nb_saved_chunks;

                // If this the final chunk, set final nb. of chunks
                if (in.m_is_final)
                    m_final_nb_chunks = in.m_frame_idx / m_nb_frames_per_chunk + 1;

                // If the final chunk has been written, check if all other chunks were saved too before closing the file
                if (m_final_nb_chunks && (m_nb_saved_chunks == *m_final_nb_chunks)) {
                    // Close the writer
                    m_writer->close();

                    // Signal the end of the saving
                    std::get<1>(ports).try_put(tbb::flow::continue_msg{});
                }

                std::get<0>(ports).try_put(tbb::flow::continue_msg{});
            };

            std::shared_ptr<writer_t> m_writer;
            std::size_t m_nb_frames_per_chunk;
            std::size_t m_nb_saved_chunks{0};
            std::optional<int> m_final_nb_chunks; //!< The final number of chunks for the latest file
        };

      public:
        io_hdf5_node(tbb::flow::graph& g, io::h5::saving_params const& sav_params, frame_info const& frame_info,
                     io::h5::nx_metadata_writer const& metadata_writer) :
            // Contract
            boost::contract::constructor_precondition<io_hdf5_node<Frame>>([&] {
                BOOST_CONTRACT_ASSERT(sav_params.start_number >= 0);
                BOOST_CONTRACT_ASSERT(sav_params.nb_frames_per_file > 0);
                BOOST_CONTRACT_ASSERT(sav_params.nb_frames_per_chunk > 0);
            }),
            parent_t(g),
            aggregate_node(g, tbb::flow::serial, aggregate_body(sav_params.nb_frames_per_chunk)),
            write_chunk_node(g, tbb::flow::serial, write_chunk_body(sav_params, frame_info, metadata_writer))
        {
            // Construct compression node
            switch (sav_params.compression) {
            case io::h5::compression_enum::none:
                comp_node.emplace(g, tbb::flow::unlimited, comp_none_body());
                break;

            case io::h5::compression_enum::zip:
                comp_node.emplace(g, tbb::flow::unlimited, comp_gzip_body());
                break;

            case io::h5::compression_enum::bshuf_lz4:
                comp_node.emplace(g, tbb::flow::unlimited, comp_bshuf_body());
                break;
            }

            // Check for degenerated case where aggregation is not needed
            if (sav_params.nb_frames_per_chunk == 1) {
                tbb::flow::make_edge(*comp_node, write_chunk_node);
                typename parent_t::input_ports_type input_tuple(*comp_node);
                typename parent_t::output_ports_type output_tuple(tbb::flow::output_port<0>(write_chunk_node),
                                                                  tbb::flow::output_port<1>(write_chunk_node));
                parent_t::set_external_ports(input_tuple, output_tuple);
            } else {
                tbb::flow::make_edge(aggregate_node, *comp_node);
                tbb::flow::make_edge(*comp_node, write_chunk_node);
                typename parent_t::input_ports_type input_tuple(aggregate_node);
                typename parent_t::output_ports_type output_tuple(tbb::flow::output_port<0>(write_chunk_node),
                                                                  tbb::flow::output_port<1>(write_chunk_node));
                parent_t::set_external_ports(input_tuple, output_tuple);
            }
        }
    };

} //namespace processing
} //namespace lima
