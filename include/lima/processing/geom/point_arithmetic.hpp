// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cmath>
#include <iostream>

#include <boost/gil/point.hpp>
#include <boost/rational.hpp>

namespace boost
{
namespace gil
{
    // Additional operators on the Boost.GIL point type
    template <typename T>
    BOOST_FORCEINLINE bool operator<(const point<T>& p1, const point<T>& p2)
    {
        return p1.x < p2.x && p1.y < p2.y;
    }

    // TODO: Rename to scale(const point<T>& p, const point<T>& factor) ?
    template <typename T>
    BOOST_FORCEINLINE point<T> operator*(const point<T>& p1, const point<T>& p2)
    {
        return {p1.x * p2.x, p1.y * p2.y};
    }

    template <typename T>
    BOOST_FORCEINLINE point<T> operator/(const point<T>& p1, const point<T>& p2)
    {
        return {p1.x / p2.x, p1.y / p2.y};
    }

    template <typename T, typename D>
    BOOST_FORCEINLINE point<boost::rational<T>> operator/(point<boost::rational<T>> const& p, D d)
    {
        return {p.x / d, p.y / d};
    }

    template <typename T>
    BOOST_FORCEINLINE point<T> operator%(const point<T>& p1, const point<T>& p2)
    {
        return {p1.x % p2.x, p1.y % p2.y};
    }

} // namespace gil
} //namespace boost

namespace lima
{
// Convert a point with rational coordinate to point with the given type
template <typename T, typename I>
boost::gil::point<T> rational_cast(const boost::gil::point<boost::rational<I>>& p)
{
    return {boost::rational_cast<T>(p.x), boost::rational_cast<T>(p.y)};
}

} //namespace lima