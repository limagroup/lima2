// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/gil/extension/numeric/affine.hpp>

namespace boost
{
namespace gil
{
    /// Scale the given vector (point) according to the given affine transformation matrix
    template <typename T>
    BOOST_FORCEINLINE point<T> scale(matrix3x2<T> const& m, point<T> const& p)
    {
        return {abs(m.a * p.x) + abs(m.c * p.y), abs(m.b * p.x) + abs(m.d * p.y)};
    }

} // namespace gil
} // namespace boost
