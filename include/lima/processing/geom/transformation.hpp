// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/hana/type.hpp>
#include <boost/hana/functional/compose.hpp>
#include <boost/hana/adapt_struct.hpp>
#include <boost/rational.hpp>

#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>
#include <lima/processing/geom/point_arithmetic.hpp>
#include <lima/processing/geom/affine.hpp>

namespace lima
{
namespace processing
{
    namespace geom
    {
        // TODO: rename these to something like is_in_first_quadrant? Should that really throw?
        template <typename T>
        void assert_not_negative(boost::gil::point<T> p, const char* msg)
        {
            if ((p.x < 0) || (p.y < 0))
                throw std::invalid_argument(msg);
        }

        template <typename T>
        void assert_positive(boost::gil::point<T> p, const char* msg)
        {
            if ((p.x <= 0) || (p.y <= 0))
                throw std::invalid_argument(msg);
        }

        template <typename T>
        void assert_contains(rectangle<T> r, typename rectangle<T>::point_t p, const char* msg)
        {
            if (!contains(r, p))
                throw std::invalid_argument(msg);
        }

        /// Basic definitions
        using integral_t = std::ptrdiff_t;
        using rectangle_t = rectangle<integral_t>;
        using rational_t = boost::rational<integral_t>;
        using affine_t = boost::gil::matrix3x2<rational_t>;

        /// Transformations
        struct nop
        {
            static affine_t affine() { return {/*1, 0, 0, 1, 0, 0*/}; }
        };

        struct bin
        {
            using point_t = boost::gil::point<integral_t>;

            point_t factor = {1, 1};

            bin() = default;
            bin(const bin& o) = default;
            bin(point_t f) { set_bin(f); }

            void set_bin(point_t f)
            {
                assert_positive(f, "Non-positive binning factor");
                factor = f;
            }

            void check_input_dimensions(point_t input_dimensions) const
            {
                if (input_dimensions % factor != point_t(0, 0))
                    throw std::invalid_argument("Binning factor/dimension "
                                                "misalignment");
            }

            affine_t affine() const { return {{1, factor.x}, 0, 0, {1, factor.y}, 0, 0}; }
        };

        struct roi
        {
            using point_t = boost::gil::point<integral_t>;

            rectangle_t rect;

            roi() = default;
            roi(const roi& o) = default;
            roi(const rectangle_t& r) { set_roi(r.topleft, r.dimensions); }
            roi(point_t tl, point_t dims) { set_roi(tl, dims); }

            void set_roi(point_t tl, point_t dims)
            {
                assert_not_negative(tl, "Negative roi topleft");
                assert_positive(dims, "Non-positive roi dimensions");
                rect.topleft = tl;
                rect.dimensions = dims;
            }

            void check_input_dimensions(point_t input_dimensions) const
            {
                const rectangle_t image{{0, 0}, input_dimensions};
                assert_contains(image, rect.tl_corner(), "Roi topleft corner outside dimensions");
                assert_contains(image, rect.br_corner(), "Roi bottomright corner outside dimensions");
            }

            point_t output_dimensions(point_t input_dimensions) const { return rect.dimensions; }

            affine_t affine() const { return {1, 0, 0, 1, -rect.topleft.x, -rect.topleft.y}; }

            point_t topleft() const { return rect.topleft; }
            point_t dimensions() const { return rect.dimensions; }
        };

        struct flipped_up_down
        {
            static affine_t affine() { return {1, 0, 0, -1, 0, 0}; }
        };

        struct flipped_left_right
        {
            static affine_t affine() { return {-1, 0, 0, 1, 0, 0}; }
        };

        struct rotated90ccw
        {
            static affine_t affine() { return {0, -1, 1, 0, 0, 0}; }
        };

        struct rotated180
        {
            static affine_t affine() { return {-1, 0, 0, -1, 0, 0}; }
        };

        struct rotated90cw
        {
            static affine_t affine() { return {0, 1, -1, 0, 0, 0}; }
        };

        namespace concepts
        {
            auto has_affine = boost::hana::is_valid([](auto t) -> decltype(t.affine()) {});

            template <typename T>
            constexpr bool has_affine_v = decltype(has_affine(std::declval<T>()))::value;

            auto has_output_dimensions = boost::hana::is_valid(
                [](auto t) -> decltype(t.output_dimensions(std::declval<typename decltype(t)::point_t>())) {});

            template <typename T>
            constexpr bool has_output_dimensions_v = decltype(has_output_dimensions(std::declval<T>()))::value;

            // Centrosymmetric transformations: do not have output_dimensions method
            auto is_centrosymmetric = boost::hana::compose(boost::hana::not_, has_output_dimensions);

            template <typename T>
            constexpr bool is_centrosymmetric_v = decltype(is_centrosymmetric(std::declval<T>()))::value;

        } // namespace concepts

        template <typename T, std::enable_if_t<concepts::has_affine_v<T>, int> = 0>
        affine_t affine(const T& t)
        {
            return t.affine();
        }

        template <typename T>
        auto to_rational(boost::gil::point<T> const& p)
        {
            return boost::gil::point<boost::rational<T>>{p.x, p.y};
        }

        //template <typename T>
        //auto to_rational(boost::gil::point<boost::rational<T>> const& p)
        //{
        //	return p;
        //}

        // Non-centrosymmetric transformations
        template <typename Transformation, typename T,
                  std::enable_if_t<concepts::has_output_dimensions_v<Transformation>, int> = 0>
        auto output_dimensions(const Transformation& t, boost::gil::point<T> const& input_dimensions)
        {
            return t.output_dimensions(input_dimensions);
        }

        template <typename Transformation, typename T,
                  std::enable_if_t<concepts::has_output_dimensions_v<Transformation>, int> = 0>
        auto output_dimensions(const Transformation& t, boost::gil::point<boost::rational<T>> const& input_dimensions)
        {
            // standard manipulation should be performed on full pixels
            return to_rational(t.output_dimensions(rational_cast<T>(input_dimensions)));
        }

        // Centrosymmetric transformations
        template <typename Transformation, typename T,
                  std::enable_if_t<!concepts::has_output_dimensions_v<Transformation>, int> = 0>
        auto output_dimensions(Transformation const& t, boost::gil::point<T> const& input_dimensions)
        {
            return rational_cast<T>(boost::gil::scale(t.affine(), to_rational(input_dimensions)));
        }

        template <typename Transformation, typename T,
                  std::enable_if_t<!concepts::has_output_dimensions_v<Transformation>, int> = 0>
        auto output_dimensions(Transformation const& t, boost::gil::point<boost::rational<T>> const& input_dimensions)
        {
            return boost::gil::scale(t.affine(), input_dimensions);
        }

        inline auto output_dimensions(roi const& r) { return r.rect.dimensions; }

    } // namespace geom
} //namespace processing
} //namespace lima

namespace boost
{
namespace gil
{
    // Centrosymmetric transformations: Affine matrix containing <centering + xform + de+centering>
    template <typename M, typename T>
    auto complete_affine(M const& m, point<T> const& input_dimensions)
    {
        using namespace lima::processing::geom;

        auto disp_affine = [](auto p) { return M{1, 0, 0, 1, -p.x, -p.y}; };

        auto input_pixel_center_offset = to_rational(point_t{1, 1}) / 2;
        auto input_center_offset = to_rational(input_dimensions) / 2;
        auto input_offset = input_center_offset - input_pixel_center_offset;
        auto center_m = disp_affine(input_offset);

        auto output_pixel_center_offset = scale(m, input_pixel_center_offset);
        auto output_center_offset = scale(m, input_center_offset);
        auto output_offset = output_center_offset - output_pixel_center_offset;
        auto de_center_m = disp_affine(-output_offset);

        return center_m * m * de_center_m;
    }

    template <typename M, typename T>
    point<T> complete_transform(M const& m, point<T> const& input_dimensions, point<T> const& p)
    {
        using namespace lima::processing::geom;

        // Apply affine transformmation
        auto cm = complete_affine(m, input_dimensions);
        return lima::rational_cast<T>(transform(cm, to_rational(p)));
    }

} //namespace gil
} //namespace boost
