// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <variant>
#include <type_traits>

#include <boost/hana.hpp>
#include <boost/preprocessor/cat.hpp>

#include <lima/processing/geom/transformation.hpp>

namespace lima
{
namespace processing
{
    namespace geom
    {
#define LIMA_ISO_XFORM_TUPLE(v, h, r) \
    boost::hana::tuple<boost::hana::bool_<v>, boost::hana::bool_<h>, boost::hana::bool_<r>>

#define LIMA_ISO_XFORM_DEFINE(n, v, h, r) using BOOST_PP_CAT(isometric_xform_, n) = LIMA_ISO_XFORM_TUPLE(v, h, r)

        //		      Name		VFlip	HFlip	Rot90
        LIMA_ISO_XFORM_DEFINE(none, false, false, false);
        LIMA_ISO_XFORM_DEFINE(vflip, true, false, false);
        LIMA_ISO_XFORM_DEFINE(hflip, false, true, false);
        LIMA_ISO_XFORM_DEFINE(rot90ccw, false, false, true);
        LIMA_ISO_XFORM_DEFINE(rot180, true, true, false);
        LIMA_ISO_XFORM_DEFINE(rot90cw, true, true, true);
        LIMA_ISO_XFORM_DEFINE(vflip_rot90ccw, true, false, true);
        LIMA_ISO_XFORM_DEFINE(hflip_rot90ccw, false, true, true);

#undef LIMA_ISO_XFORM_DEFINE

        using all_isometric_xforms =
            boost::hana::tuple<isometric_xform_none, isometric_xform_vflip, isometric_xform_hflip,
                               isometric_xform_rot90ccw, isometric_xform_rot180, isometric_xform_rot90cw,
                               isometric_xform_vflip_rot90ccw, isometric_xform_hflip_rot90ccw>;

        // VFlip	HFlip	Rot90
        template <bool V, bool H, bool R>
        using isometric_xform_base_t = LIMA_ISO_XFORM_TUPLE(V, H, R);

        namespace concepts
        {
            template <typename T>
            constexpr bool is_isometric_xform_none = std::is_same_v<std::decay_t<T>, isometric_xform_none>;
        } // namespace concepts

        // Returns the result of applying isometric_xform t1 then t2
        template <bool V1, bool H1, bool R1, bool V2, bool H2, bool R2>
        constexpr auto operator+(const isometric_xform_base_t<V1, H1, R1>& t1,
                                 const isometric_xform_base_t<V2, H2, R2>& t2)
        {
            using T1 = isometric_xform_base_t<V1, H1, R1>;
            using T2 = isometric_xform_base_t<V2, H2, R2>;

            // first treat NOP
            if constexpr (concepts::is_isometric_xform_none<T1>)
                return T2();
            if constexpr (concepts::is_isometric_xform_none<T2>)
                return T1();

            constexpr bool double_rot = R1 & R2;
            constexpr bool v3 = V1 ^ (!R1 & V2 | R1 & H2) ^ double_rot;
            constexpr bool h3 = H1 ^ (!R1 & H2 | R1 & V2) ^ double_rot;
            constexpr bool r3 = R1 ^ R2;
            return isometric_xform_base_t<v3, h3, r3>();
        }

        template <bool V1, bool H1, bool R1, bool V2, bool H2, bool R2>
        constexpr auto operator==(const isometric_xform_base_t<V1, H1, R1>& t1,
                                  const isometric_xform_base_t<V2, H2, R2>& t2)
        {
            constexpr bool equal = (V1 == V2) && (H1 == H2) && (R1 == R2);
            return std::integral_constant<bool, equal>();
        }

        template <bool V1, bool H1, bool R1, bool V2, bool H2, bool R2>
        constexpr auto operator-(const isometric_xform_base_t<V1, H1, R1>& t1,
                                 const isometric_xform_base_t<V2, H2, R2>& t2)
        {
            namespace hana = boost::hana;

            return hana::at_c<0>(hana::filter(all_isometric_xforms(), [&](auto&& t3) { return t3 + t2 == t1; }));
        }

        template <bool V, bool H, bool R>
        constexpr auto inverse(const isometric_xform_base_t<V, H, R>& t)
        {
            return isometric_xform_none() - t;
        }

        template <bool V, bool H, bool R>
        affine_t affine(const isometric_xform_base_t<V, H, R>& t)
        {
            affine_t m;

            if constexpr (V)
                m = m * processing::geom::flipped_up_down::affine();
            if constexpr (H)
                m = m * processing::geom::flipped_left_right::affine();
            if constexpr (R)
                m = m * processing::geom::rotated90ccw::affine();

            return m;
        }

        template <bool V, bool H, bool R, typename T>
        auto output_dimensions(const isometric_xform_base_t<V, H, R>& t, boost::gil::point<T> const& input_dimensions)
        {
            return rational_cast<T>(boost::gil::scale(affine(t), to_rational(input_dimensions)));
        }

        using any_isometric_xform_t =
            std::variant<isometric_xform_none, isometric_xform_vflip, isometric_xform_hflip, isometric_xform_rot90ccw,
                         isometric_xform_rot180, isometric_xform_rot90cw, isometric_xform_vflip_rot90ccw,
                         isometric_xform_hflip_rot90ccw>;

        template <typename T, typename Xform>
        auto apply_isometric_xform(boost::gil::point<T> const& input_dimensions, rectangle<T> const& rect,
                                   const Xform& t)
        {
            affine_t m = affine(t);
            auto p1 = boost::gil::complete_transform(m, input_dimensions, rect.tl_corner());
            auto p2 = boost::gil::complete_transform(m, input_dimensions, rect.br_corner());
            return rectangle<T>::from_corners(p1, p2);
        }

        namespace concepts
        {
            template <typename T1, typename T2>
            constexpr bool is_reverse(const T1& t1, const T2& t2)
            {
                return is_isometric_xform_none<decltype(t1 + t2)>;
            }

        } // namespace concepts

        //inline affine_t affine(const any_isometric_xform_t& t, point_t input_dimensions)
        //{
        //	return std::visit([&input_dimensions](auto&& x) {
        //		return affine(x, input_dimensions);
        //	}, t);
        //}

        affine_t affine(const any_isometric_xform_t& t)
        {
            return std::visit([](auto const& x) { return geom::affine(x); }, t);
        }

        template <typename T>
        auto output_dimensions(const any_isometric_xform_t& t, boost::gil::point<T> const& input_dimensions)
        {
            return std::visit(
                [input_dimensions](auto const& x) { return geom::output_dimensions(x, input_dimensions); }, t);
        }

    } // namespace geom
} //namespace processing
} //namespace lima
