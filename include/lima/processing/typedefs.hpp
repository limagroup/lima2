// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/gil/extension/dynamic_image/dynamic_image_all.hpp>
#include <boost/gil/typedefs.hpp>

namespace lima
{
namespace processing
{
    using gray8_image_t = boost::gil::gray8_image_t;
    using gray16_image_t = boost::gil::gray16_image_t;
    using gray32_image_t = boost::gil::gray32_image_t;

    // Supported image format
    using any_image_t = boost::gil::any_image< //boost::gil::gray4_image_t,
        gray8_image_t, gray16_image_t, gray32_image_t>;

    using gray8_view_t = boost::gil::gray8_view_t;
    using gray16_view_t = boost::gil::gray16_view_t;
    using gray32_view_t = boost::gil::gray32_view_t;

    using any_image_view_t = boost::gil::any_image_view<gray8_view_t, gray16_view_t, gray32_view_t,
                                                        typename boost::gil::dynamic_x_step_type<gray8_view_t>::type,
                                                        typename boost::gil::dynamic_x_step_type<gray16_view_t>::type,
                                                        typename boost::gil::dynamic_x_step_type<gray32_view_t>::type,
                                                        typename boost::gil::dynamic_y_step_type<gray8_view_t>::type,
                                                        typename boost::gil::dynamic_y_step_type<gray16_view_t>::type,
                                                        typename boost::gil::dynamic_y_step_type<gray32_view_t>::type,
                                                        typename boost::gil::transposed_type<gray8_view_t>::type,
                                                        typename boost::gil::transposed_type<gray16_view_t>::type,
                                                        typename boost::gil::transposed_type<gray32_view_t>::type>;

} //namespace processing
} //namespace lima
