// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/mpi/collectives.hpp>

#include <lima/processing/serial/counters.hpp>

namespace lima
{
namespace processing
{
    namespace mpi
    {
        struct counters_combine
        {
            template <typename T>
            counters_accumulator_set<T> operator()(const counters_accumulator_set<T>& lhs,
                                                   const counters_accumulator_set<T>& rhs)
            {
                counters_accumulator_set<T> res;
                res.m_count = lhs.m_count + rhs.m_count;
                res.m_min = std::min(lhs.m_min, rhs.m_min);
                res.m_max = std::max(lhs.m_max, rhs.m_max);
                res.m_sum = lhs.m_sum + rhs.m_sum;
                res.m_sum_sq = lhs.m_sum_sq + rhs.m_sum_sq;
                return res;
            }
        };

        template <typename SrcView>
        counters_result counters(const SrcView& src)
        {
            using pod_t = typename boost::gil::channel_type<typename SrcView::value_type>::type;

            boost::mpi::communicator world;

            counters_accumulator_set<pod_t> local_acc;

            local_acc = boost::gil::for_each_pixel(src, local_acc);

            counters_accumulator_set<pod_t> acc;
            boost::mpi::all_reduce(world, local_acc, acc, counters_combine());

            counters_result res;
            res.m_count = acc.m_count;
            res.m_min = acc.m_min;
            res.m_max = acc.m_max;
            res.m_avg = acc.m_sum / acc.m_count * 0.1f;
            res.m_std = std::sqrt(acc.m_sum_sq / acc.m_count - res.m_avg * res.m_avg);
            res.m_sum = acc.m_sum;

            return res;
        }

        struct counters_obj
        {
            typedef counters_result result_type; // required typedef

            template <typename SrcView>
            counters_result operator()(const SrcView& src) const
            {
                return counters(src);
            }
        };

        // Dynamic version of the algorithm
        template <typename... SrcViews>
        counters_result counters(const boost::gil::any_image_view<SrcViews...>& src)
        {
            return boost::variant2::visit(counters_obj(), src);
        }

    } //namespace mpi
} //namespace processing
} //namespace lima

namespace boost
{
namespace mpi
{
    template <typename T>
    struct is_commutative<std::plus<lima::processing::counters_accumulator_set<T>>,
                          lima::processing::counters_accumulator_set<T>> : mpl::true_
    {
    };

} // namespace mpi
} // namespace boost

// MPI Optimizations
namespace boost
{
namespace mpi
{
    template <typename T>
    struct is_mpi_datatype<lima::processing::counters_accumulator_set<T>> : public mpl::true_
    {
    };
} // namespace mpi
} // namespace boost
