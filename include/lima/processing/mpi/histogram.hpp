// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/color_convert.hpp>
#include <boost/serialization/array.hpp>
#include <boost/mpi/collectives.hpp>

#include <lima/processing/serial/histogram.hpp>

namespace lima
{
namespace processing
{
    namespace mpi
    {
        struct combine_histogram
        {
            histogram_result operator()(const histogram_result& lhs, const histogram_result& rhs)
            {
                histogram_result res;
                std::transform(lhs.begin(), lhs.end(), rhs.begin(), res.begin(), std::plus<int>());
                return res;
            }
        };

        template <typename SrcView>
        histogram_result histogram(const SrcView& src)
        {
            namespace mpi = boost::mpi;
            namespace gil = boost::gil;

            mpi::communicator world;

            histogram_result local_acc = processing::histogram(src);

            histogram_result res;
            mpi::all_reduce(world, local_acc, res, combine_histogram());

            return res;
        }

        struct histogram_obj
        {
            typedef histogram_result result_type; // required typedef

            template <typename SrcView>
            histogram_result operator()(const SrcView& src) const
            {
                return histogram(src);
            }
        };

        // Dynamic version of the algorithm
        template <typename... SrcViews>
        histogram_result histogram(const boost::gil::any_image_view<SrcViews...>& src)
        {
            return boost::variant2::visit(histogram_obj(), src);
        }

    } //namespace mpi
} //namespace processing
} //namespace lima

namespace boost
{
namespace mpi
{
    template <>
    struct is_commutative<std::plus<lima::processing::histogram_result>, lima::processing::histogram_result>
        : mpl::true_
    {
    };

} // namespace mpi
} // namespace boost

// MPI Optimizations
BOOST_IS_MPI_DATATYPE(lima::processing::histogram_result)
