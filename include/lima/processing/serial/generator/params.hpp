// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

namespace lima
{
namespace processing::generator
{
    /// Single peak for Gauss generator
    struct gauss_peak
    {
        double x0 = 1024.0, y0 = 1024.0; //<! The center of the peak
        double fwhm = 128.0;             //<! Full Width at Half Maximum
        double max = 100.0;              //<! The maximum value
    };

    /// Gauss generator params
    struct gauss_params
    {
        std::vector<gauss_peak> peaks{gauss_peak{}}; //!< A vector of peaks
        double grow_factor{0.0};                     //!< The grow factor of the diffraction pattern
    };

    /// Diffraction generator params
    struct diffraction_params
    {
        double x0 = 1024.0, y0 = 1024.0;               //!< The center of the diffraction pattern
        double source_pos_x = 5.0, source_pos_y = 5.0; //!< The source position, relative to the gauss reference frame
        double source_speed_x = 0.0, source_speed_y = 0.0; //!< The source speed (in time units of a frame)
    };

} // namespace processing::generator
} // namespace lima
