// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>
#include <boost/describe/io_structs.hpp>

#include <lima/processing/serial/generator/params.hpp>

namespace lima
{
namespace processing::generator
{
    BOOST_DESCRIBE_STRUCT(gauss_peak, (), (x0, y0, fwhm, max))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(gauss_peak, x0,
        (desc, "x0"),
        (doc, "The x coordinate of the center of the peak"))

    BOOST_ANNOTATE_MEMBER(gauss_peak, y0,
        (desc, "y0"),
        (doc, "The y coordinate of the center of the peak"))

    BOOST_ANNOTATE_MEMBER(gauss_peak, fwhm,
        (desc, "full width hald maximum"),
        (doc, "The full Width at Half Maximum of the peak"))

    BOOST_ANNOTATE_MEMBER(gauss_peak, max,
        (desc, "max value"),
        (doc, "The maximum value at the center of the peak"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(gauss_params, (), (peaks, grow_factor))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(gauss_params, peaks,
        (desc, "vector of gauss_peak"),
        (doc, "A vector of Gaussian peaks"))

    BOOST_ANNOTATE_MEMBER(gauss_params, grow_factor,
        (desc, "grow factor"),
        (doc, "The grow factor for gauss peaks (per frame)"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(diffraction_params, (), (x0, y0, source_pos_x, source_pos_y, source_speed_x, source_speed_y))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(diffraction_params, x0,
        (desc, "x0"),
        (doc, "The x coordinate of the center of the diffraction pattern"))

    BOOST_ANNOTATE_MEMBER(diffraction_params, y0,
        (desc, "y0"),
        (doc, "The y coordinate of the center of the diffraction pattern"))

    BOOST_ANNOTATE_MEMBER(diffraction_params, source_pos_x,
        (desc, "source position x"),
        (doc, "The x coordinate of the source position (relative to the Gauss reference frame)"))

    BOOST_ANNOTATE_MEMBER(diffraction_params, source_pos_y,
        (desc, "source position y"),
        (doc, "The y coordinate of the source position (relative to the Gauss reference frame)"))

    BOOST_ANNOTATE_MEMBER(diffraction_params, source_speed_x,
        (desc, "source speed x"),
        (doc, "The x coordinate of the source speed (in time units of a frame)"))

    BOOST_ANNOTATE_MEMBER(diffraction_params, source_speed_y,
        (desc, "source speed y"),
        (doc, "The y coordinate of the source speed (in time units of a frame)"))

} // namespace processing::generator
} // namespace lima
