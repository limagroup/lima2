// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <optional>

#include <boost/gil/step_iterator.hpp>
#include <boost/gil/algorithm.hpp>
#include <boost/gil/image_view_factory.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>

#include <boost/mp11/algorithm.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/rectangle.hpp>

namespace lima
{
namespace processing
{
    enum class flip_t : int
    {
        none,      //!< No rotation
        up_down,   //!< Up down
        left_right //!< Left right
    };

    enum class rotation_t : int
    {
        none,  //!< No rotation
        cw90,  //!< Clockwise 90
        ccw90, //!< Counter clockwise 90
        r180   //!< 180
    };

    template <typename View>
    using any_transformed_image_view =
        boost::gil::any_image_view<View, typename boost::gil::dynamic_x_step_type<View>::type,
                                   typename boost::gil::dynamic_y_step_type<View>::type,
                                   typename boost::gil::dynamic_xy_step_type<View>::type,
                                   typename boost::gil::dynamic_xy_step_transposed_type<View>::type>;

    template <typename View>
    auto roi_flip_rot(View const& v, std::optional<rectangle_t> const& roi, flip_t const& flip,
                      rotation_t const& rotation)
    {
        // Compute the result type
        using result_t = any_transformed_image_view<View>;
        result_t res(v);

        // Apply ROI
        if (roi)
            res = boost::gil::subimage_view(res, roi->topleft, roi->dimensions);

        // Apply Flip
        switch (flip) {
        case flip_t::up_down:
            res = boost::gil::flipped_up_down_view(res);
            break;
        case flip_t::left_right:
            res = boost::gil::flipped_left_right_view(res);
            break;
        }

        // Apply Rotation
        switch (rotation) {
        case rotation_t::cw90:
            res = boost::gil::rotated90cw_view(res);
            break;
        case rotation_t::ccw90:
            res = boost::gil::rotated90ccw_view(res);
            break;
        case rotation_t::r180:
            res = boost::gil::rotated180_view(res);
            break;
        }

        return res;
    };

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    auto roi_flip_rot(boost::gil::any_image_view<SrcViews...> const& src, std::optional<rectangle_t> const& roi,
                      flip_t const& flip, rotation_t const& rotation)
    {
        using result_t = boost::mp11::mp_flatten<
            boost::mp11::mp_transform<any_transformed_image_view, boost::gil::any_image_view<SrcViews...>>>;

        return boost::variant2::visit(
            [&roi, &flip, &rotation](auto&& v) { return result_t(roi_flip_rot(v, roi, flip, rotation)); }, src);
    }

} //namespace processing
} //namespace lima
