// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>
//#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
//#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
//#include <boost/gil/pixel_numeric_operations.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/frame_view.hpp>

namespace lima
{
namespace processing
{
    template <typename View, typename ConstView>
    void background_substraction(const View& src, const ConstView& background)
    {
        namespace gil = boost::gil;

        // Preconditions
        if (src.dimensions() != background.dimensions())
            LIMA_THROW_EXCEPTION(process_error("Source and background dimensions mismatched"));
        //if (!std::is_same<>)
        //    LIMA_THROW_EXCEPTION(process_error("Source and background pixel type mismatched"));

        gil::transform_pixels(src, background, src,
                              [](auto p, auto bg) { return p > bg ? p - bg : typename View::value_type(0); });
    }

    // Dynamic version of the algorithm
    template <typename... Views>
    void background_substraction(boost::gil::any_image_view<Views...> const& src,
                                 typename boost::gil::any_image_view<Views...>::const_t const& background)
    {
        boost::variant2::visit([](auto&& v, auto&& background) { background_substraction(v, background); }, src,
                               background);
    }

} //namespace processing
} //namespace lima
