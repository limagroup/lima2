// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#if defined(__AVX2__) || defined(__SSE2__)
//Intrinsics for vector extensions
#if defined(_MSC_VER)
#include <intrin.h>
#else
#include <x86intrin.h>
#endif //defined(_MSC_VER)
#endif

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/pixel_numeric_operations.hpp>

#include <lima/exceptions.hpp>

namespace lima
{
namespace processing
{
#if defined(__AVX2__)

    /// Optimized implementation of the 4bit to * bit expension using AVX2.
    /// Requirements:
    ///  -  both src and dst should be 8 bytes aligned
    void exp4bit_avx2(const unsigned char* const src, const boost::gil::gray8_view_t& dst)
    {
        namespace gil = boost::gil;

        const __m256i mlo = _mm256_set1_epi8(0x0F);
        const __m256i shift = _mm256_set1_epi32(4);

        // Assme the image is 1D traversable
        gil::gray8_view_t::x_iterator dst_it = dst.row_begin(0);

        for (int i = 0; i < dst.dimensions().x * dst.dimensions().y / 2; i += sizeof(__m256i)) {
            // Load 32 bytes of dual 4 bit data
            __m256i s1 = _mm256_load_si256(reinterpret_cast<const __m256i*>(&src[i]));

            // Extract the even bytes from the upper 4 bits
            __m256i h1 = _mm256_and_si256(_mm256_srlv_epi32(s1, shift), mlo);

            // Extract the odd bytes from the lower 4 bits
            __m256i h2 = _mm256_and_si256(s1, mlo);

            // Unpack
            __m256i up1 = _mm256_unpacklo_epi8(h1, h2);
            __m256i up2 = _mm256_unpackhi_epi8(h1, h2);

            __m256i res1 = _mm256_permute2x128_si256(up1, up2, 0x20);
            __m256i res2 = _mm256_permute2x128_si256(up1, up2, 0x31);

            // Store 64 bytes of 8 bit data
            _mm256_store_si256(reinterpret_cast<__m256i*>(&dst_it[i * 2]), res1);
            _mm256_store_si256(reinterpret_cast<__m256i*>(&dst_it[i * 2 + sizeof(__m256i)]), res2);
        }
    }

#endif //defined(__AVX2__)

#if defined(__SSE2__)

    /// Optimized implementation of the 4bit to * bit expension using SSE2.
    /// Requirements:
    ///  -  both src and dst should be 4 bytes aligned
    void exp4bit_sse2(const unsigned char* const src, const boost::gil::gray8_view_t& dst)
    {
        namespace gil = boost::gil;

        const __m128i mask = _mm_set1_epi8(0xf);

        // Assume the image is 1D traversable
        gil::gray8_view_t::x_iterator dst_it = dst.row_begin(0);

        for (int i = 0; i < dst.dimensions().x * dst.dimensions().y / 2; i += sizeof(__m128i)) {
            // Load 16 bytes of dual 4 bit data
            __m128i pack4_raw = _mm_load_si128(reinterpret_cast<const __m128i*>(&src[i]));

            __m128i pack4_shr = _mm_srli_epi16(pack4_raw, 4);

            __m128i ilace8_0 = _mm_and_si128(pack4_raw, mask);
            __m128i ilace8_1 = _mm_and_si128(pack4_shr, mask);

            // Unpack
            __m128i pack8_0 = _mm_unpacklo_epi8(ilace8_1, ilace8_0);
            __m128i pack8_1 = _mm_unpackhi_epi8(ilace8_1, ilace8_0);

            // Store 64 bytes of 8 bit data
            _mm_store_si128(reinterpret_cast<__m128i*>(&dst_it[i * 2]), pack8_0);
            _mm_store_si128(reinterpret_cast<__m128i*>(&dst_it[i * 2 + sizeof(__m128i)]), pack8_1);
        }
    }
#endif //defined(__SSE2__)

} //namespace processing
} //namespace lima
