// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>
//#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
//#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
//#include <boost/gil/pixel_numeric_operations.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/frame_view.hpp>

namespace lima
{
namespace processing
{
    template <typename SrcView>
    void flatfield_correction(const SrcView& src, const bpp32fc_view_t& flatfield)
    {
        namespace gil = boost::gil;

        // Preconditions
        if (src.dimensions() != flatfield.dimensions())
            LIMA_THROW_EXCEPTION(process_error("Source and mask flatfield mismatched"));

        gil::transform_pixels(src, flatfield, src, [](auto p, float div) { return p / div; });
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    void flatfield_correction(const boost::gil::any_image_view<SrcViews...>& src, const bpp32fc_view_t& flatfield)
    {
        boost::variant2::visit([&flatfield](auto&& v) { flatfield_correction(v, flatfield); }, src);
    }

} //namespace processing
} //namespace lima
