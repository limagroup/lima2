// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/frame_view.hpp>

namespace lima
{
namespace processing
{
    template <typename SrcView>
    void mask(const SrcView& src, const bpp8c_view_t& mask)
    {
        namespace gil = boost::gil;

        // Preconditions
        if (src.dimensions() != mask.dimensions())
            LIMA_THROW_EXCEPTION(process_error("Source and mask dimensions mismatched"));

        gil::transform_pixels(src, mask, src, [&](auto p1, auto p2) { return p2 == 1 ? 0 : p1; });
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    void mask(const boost::gil::any_image_view<SrcViews...>& src, const bpp8c_view_t& mask_arg)
    {
        boost::variant2::visit([&mask_arg](auto&& v) { mask(v, mask_arg); }, src);
    }

} //namespace processing
} //namespace lima
