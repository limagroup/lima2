// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/pixel_numeric_operations.hpp>

#include <lima/exceptions.hpp>

namespace lima
{
namespace processing
{
    template <typename PixelSrc, typename PixelDst>
    void pixel_accumulate(PixelSrc& p, PixelDst& sum)
    {
        //static_for_each(p, [&sum](auto& p) {
        //	sum = channel_plus_t<
        //		typename channel_type<PixelDst>::type,
        //		typename channel_type<PixelSrc>::type,
        //		typename channel_type<PixelDst>::type>()(sum, p);
        //});

        boost::gil::pixel_plus_t<PixelSrc, PixelDst, PixelDst> op;
        sum = op(p, sum);
    }

    template <unsigned int factor, typename SrcView, typename DstView>
    void binning(const SrcView& src, const DstView& dst)
    {
        using namespace boost::gil;

        gil_function_requires<ImageViewConcept<SrcView>>();
        gil_function_requires<MutableImageViewConcept<DstView>>();
        gil_function_requires<ColorSpacesCompatibleConcept<typename color_space_type<SrcView>::type,
                                                           typename color_space_type<DstView>::type>>();

        constexpr unsigned int size = factor * factor;
        std::array<typename SrcView::xy_locator::cached_location_t, size> cached_locations;

        // Pre-compute the location of the pixels to bin
        typename SrcView::xy_locator src_loc = src.xy_at(0, 0);
        for (int y = 0; y < factor; ++y)
            for (int x = 0; x < factor; ++x)
                cached_locations[y * factor + x] = src_loc.cache_location(x, y);

        // Loop over the source image
        typename DstView::iterator dst_it = dst.begin();
        for (int y_src = 0; y_src < src.height(); y_src += factor) {
            for (int x_src = 0; x_src < src.width(); x_src += factor) {
                typename DstView::value_type sum = *src_loc;
                //typename int sum = *src_loc;
                //static_fill(sum, 0);

                // TODO unroll this
                //for (auto& n : cached_locations)
                //for (int n = 0 ; n < cached_locations.size() ; n++)
                //	pixel_accumulate(src_loc[cached_locations[n]], sum);

                //for (auto& n : cached_locations)

                //constexpr auto nc = typename num_channels<SrcView>::type();

                //for (int n = 1; n < cached_locations.size(); n++)
                //	pixel_accumulate(src_loc[cached_locations[n]], sum);

                for (int n = 1; n < cached_locations.size(); n++)
                    static_for_each(src_loc[cached_locations[n]], [&sum](auto& p) {
                        sum = channel_plus_t<typename channel_type<typename DstView::value_type>::type,
                                             typename channel_type<typename SrcView::value_type>::type,
                                             typename channel_type<typename DstView::value_type>::type>()(sum, p);
                    });

                // This is much faster with MSVC but require a GIL patch
                //for (int n = 1; n < cached_locations.size(); n++)
                //	(*dst_it) += src_loc[cached_locations[n]];

                //// Manually unroll (much better)
                //typename DstView::value_type sum = *src_loc;
                //sum = src_loc[cached_locations[1]]
                //	+ src_loc[cached_locations[2]]
                //	+ src_loc[cached_locations[3]];

                (*dst_it) = sum;

                ++dst_it;
                src_loc.x() += factor;
            }

            src_loc += point<std::ptrdiff_t>(-src.width(), factor);

            //aSrcFirstLinePt = aSrcSecondLinePt;
            //aSrcSecondLinePt = aSrcFirstLinePt + dimensions[0];
        }

        //for (int ys = 0; ys < factor; ++ys, ++y)
        //{
        //	typename SrcView::x_iterator src_it = src.row_begin(y);
        //	typename DestView::x_iterator dst_it = dst.row_begin(y / factor);

        //	for (int x = 0; x < src.width();)
        //		for (int xs = 0; xs < factor; ++xs, ++x)
        //			pixel_add_inplace(dst_it[x / factor], src_it[x]);
        //}
    }

    template <typename SrcView, typename DestView>
    void binning(const SrcView& src, const DestView& dst, unsigned int factor)
    {
        namespace gil = boost::gil;

        switch (factor) {
        case 2:
            binning<2>(src, dst);
            break;
        case 3:
            binning<3>(src, dst);
            break;
        case 4:
            binning<4>(src, dst);
            break;
        default:
            LIMA_THROW_EXCEPTION(process_error("Binning factor not supported"));
        }
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews, typename... DestViews>
    void binning(const boost::gil::any_image_view<SrcViews...>& src,
                 const boost::gil::any_image_view<DestViews...>& dst, unsigned int factor)
    {
        return boost::variant2::visit([factor](auto&& v1, auto&& v2) { return binning(v1, v2, factor); }, src, dst);
    }

} //namespace processing
} //namespace lima
