// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>

#include <lima/exceptions.hpp>

namespace lima
{
namespace processing
{
    template <typename SrcView>
    void normalize(const SrcView& src)
    {
        namespace gil = boost::gil;

        //Check if channel is grayscale
        typedef typename SrcView::value_type value_type;

        double sum = 0.0;
        gil::for_each_pixel(src, [&sum](const value_type& p) { sum += p; });

        double mean = sum / src.size();

        if (mean - 1.0 < 1e-6)
            // Source already normed
            return;

        gil::transform_pixels(src, src, [&mean](const value_type& p) { return p / mean; });
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    void normalize(const boost::gil::any_image_view<SrcViews...>& src)
    {
        return boost::variant2::visit([](auto&& v) { return normalize(v); }, src);
    }

} //namespace processing
} //namespace lima
