// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/apply_operation.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/color_convert.hpp>

#include <lima/exceptions.hpp>

namespace lima
{
namespace processing
{
    using histogram_result = std::array<int, 256>;

    template <typename SrcView>
    histogram_result histogram(const SrcView& src)
    {
        namespace gil = boost::gil;

        histogram_result res;
        res.fill(0);

        gil::for_each_pixel(gil::color_converted_view<boost::gil::gray8_pixel_t>(src),
                            [&](boost::gil::gray8_pixel_t val) { ++res[val]; });

        return res;
    }

    struct histogram_obj
    {
        typedef histogram_result result_type; // required typedef

        template <typename SrcView>
        histogram_result operator()(const SrcView& src) const
        {
            return histogram(src);
        }
    };

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    histogram_result histogram(const boost::gil::any_image_view<SrcViews...>& src)
    {
        return boost::variant2::visit([&](auto&& v) { return histogram(v); }, src);
    }

} //namespace processing
} //namespace lima
