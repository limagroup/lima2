// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <ranges>
#include <vector>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/for_each2.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/frame_typedefs.hpp>
#include <lima/processing/serial/counters.hpp>

namespace lima
{
namespace processing
{
    using reduction_1d_result = std::vector<counters_result>;

    template <typename SrcView>
    reduction_1d_result reduction_1d(const SrcView& src)
    {
        using pod_t = typename SrcView::value_type;

        counters_accumulator_set<pod_t> acc;

        reduction_1d_result res;
        res.reserve(src.height());

        for (std::ptrdiff_t y = 0; y < src.height(); ++y) {
            counters_accumulator_set<pod_t> acc;
            acc = std::for_each(src.row_begin(y), src.row_end(y), acc);
            res.push_back(counters(acc));
        }

        return res;
    }

    template <typename SrcView>
    reduction_1d_result reduction_1d(const SrcView& src, const bpp8c_view_t& mask)
    {
        using pod_t = typename SrcView::value_type;

        assert(src.dimensions() == mask.dimensions());

        reduction_1d_result res;
        res.reserve(src.height());

        for (std::ptrdiff_t y = 0; y < src.height(); ++y) {
            counters_accumulator_set<pod_t> acc;

            for (auto& [val, m] : std::views::zip(std::ranges::subrange(src.row_begin(y), src.row_end(y)),
                                                  std::ranges::subrange(mask.row_begin(y), mask.row_end(y)))) {
                if (m == 1)
                    acc(val);
            }
            res.push_back(reduction_1d(acc));
        }

        return res;
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    reduction_1d_result reduction_1d(const boost::gil::any_image_view<SrcViews...>& src)
    {
        return boost::variant2::visit([](auto&& v) { return reduction_1d(v); }, src);
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    reduction_1d_result reduction_1d(const boost::gil::any_image_view<SrcViews...>& src, const bpp8c_view_t& mask)
    {
        return boost::variant2::visit([&mask](auto&& v) { return reduction_1d(v, mask); }, src);
    }

} //namespace processing
} //namespace lima
