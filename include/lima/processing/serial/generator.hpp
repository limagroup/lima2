// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cmath>
#include <algorithm>
#include <random>
#include <vector>

#include <lima/exceptions.hpp>
#include <lima/core/arc.hpp>
#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>

#include <lima/processing/serial/generator/params.hpp>

namespace lima
{
namespace processing::generator
{
    /// Calculates Gauss(x,y) for a given peak at given position
    ///
    /// \param[in] x     X-coord
    /// \param[in] y     Y-coord
    /// \param[in] x0    X-coord of the center
    /// \param[in] y0    Y-coord of the center
    /// \param[in] fwhm  Full Width at Half Maximum
    /// \param[in] max   the central maximum value
    /// \return Gauss(x,y)
    inline double gauss_2d(std::ptrdiff_t x, std::ptrdiff_t y, double x0, double y0, double fwhm, double max)
    {
        const double SGM_FWHM = 0.42466090014400952136075141705144; // 1/(2*sqrt(2*ln(2)))

        double sigma = SGM_FWHM * std::fabs(fwhm);
        double r2 = (x - x0) * (x - x0) + (y - y0) * (y - y0);
        return max * std::exp(-r2 / (2 * sigma * sigma));
    }

    /// Calculates the summary intensity at given position
    ///
    /// \param[in] x     X-coord
    /// \param[in] y     Y-coord
    /// \param[in] x0    X-coord of the center
    /// \param[in] y0    Y-coord of the center
    /// \return intensity
    ///
    inline double diffract(std::ptrdiff_t x, std::ptrdiff_t y, double x0, double y0)
    {
        const double pi = 3.14159265358979323846;

        double r = std::sqrt((x - x0) * (x - x0) + (y - y0) * (y - y0));
        double w = (2 * pi / 100 * (0.5 + r / 500));
        double ar = (r >= 300) ? (r - 300) : 0;
        double a = std::exp(-std::pow(ar / 500, 4.0)) / (r / 5000 + 0.1);
        return a * std::pow(std::cos(r * w), 20.0);
    }

    /// Calculates if a given position is within the given arc
    ///
    /// \param[in] x     X-coord
    /// \param[in] y     Y-coord
    /// \param[in] x0    X-coord of the center
    /// \param[in] y0    Y-coord of the center
    /// \param[in] r1    inside radius of the arc
    /// \param[in] r2    outside radius of the arc
    /// \param[in] a1    X-coord of the center
    /// \param[in] a2    Y-coord of the center
    /// \return 1 if in arc, 0 otherwise
    ///
    inline std::uint8_t within_arc(std::ptrdiff_t x, std::ptrdiff_t y, double x0, double y0, double r1, double r2,
                                   double a1, double a2)
    {
        assert(r1 < r2);
        assert(a1 < a2);

        std::uint8_t res = 0;

        double angle_diff = a2 - a1;
        double r1_sqr = r1 * r1;
        double r2_sqr = r2 * r2;

        double r_sqr = pow(x - x0, 2) + pow(y - y0, 2);
        double X = x - x0;
        double Y = y - y0;
        double angle = 0.;

        // If point inside the ring [r1, r2]
        if (r1_sqr < r_sqr && r_sqr <= r2_sqr) {
            if (fabs(X - 1e-6) > 1e-6)
                angle = atan(Y / X) * 180 / M_PI;
            else
                angle = Y > 0 ? 90 : -90;

            if (X < 0) {
                if (Y > 0)
                    angle += 180.;
                else
                    angle -= 180;
            }
            double angle_min_diff = angle - a1;
            if (angle_diff > 0) {
                if (angle_min_diff < 0)
                    angle_min_diff += 360;
                res = angle_min_diff >= 0 && angle_min_diff <= angle_diff;
            } else {
                if (angle_min_diff > 0)
                    angle_min_diff -= 360;
                res = angle_min_diff <= 0 && angle_min_diff >= angle_diff;
            }
        }

        return res;
    }

    struct generate_arc
    {
        using const_t = generate_arc;
        using value_type = std::uint8_t;
        using reference = value_type;
        using const_reference = value_type;
        using argument_type = point_t;
        using result_type = reference;

        static const bool is_mutable = false;

        generate_arc(arc_t const& arc) : m_arc(arc) { m_bounding_box = bounding_box(arc); }

        result_type operator()(point_t const& p) const
        {
            result_type res = 0;

            if (within(p, m_bounding_box))
                res = within_arc(p.x, p.y, m_arc.center.x, m_arc.center.y, m_arc.r1, m_arc.r2, m_arc.a1, m_arc.a2);

            return res;
        }

        rectangle_t m_bounding_box;
        arc_t m_arc;
    };

    typedef boost::gil::virtual_2d_locator<generate_arc, false> generate_arc_locator_t;
    typedef boost::gil::image_view<generate_arc_locator_t> generate_arc_view_t;

    struct generate_gauss
    {
        using const_t = generate_gauss;
        using value_type = float;
        using reference = value_type;
        using const_reference = value_type;
        using argument_type = point_t;
        using result_type = reference;

        static const bool is_mutable = false;

        generate_gauss(int frame_nr, gauss_params const& params) : m_frame_nr(frame_nr), m_params(params) {}

        result_type operator()(point_t const& p) const
        {
            double res = 0.0;

            // For each peaks
            for (auto&& peak : m_params.peaks)
                // Add gaussian
                res += gauss_2d(p.x, p.y, peak.x0, peak.y0, peak.fwhm, peak.max);

            // Apply grow factor
            res *= (1.0 + m_params.grow_factor * m_frame_nr);

            return result_type(res);
        }

        int m_frame_nr;
        gauss_params m_params;
    };

    typedef boost::gil::virtual_2d_locator<generate_gauss, false> generate_gauss_locator_t;
    typedef boost::gil::image_view<generate_gauss_locator_t> generate_gauss_view_t;

    struct generate_diffraction
    {
        using const_t = generate_diffraction;
        using value_type = float;
        using reference = value_type;
        using const_reference = value_type;
        using argument_type = point_t;
        using result_type = reference;

        static const bool is_mutable = false;

        generate_diffraction(int frame_nr, gauss_params const& p1, diffraction_params const& p2) :
            m_params(p2), m_gauss(frame_nr, p1)
        {
        }

        result_type operator()(point_t const& p) const
        {
            using gauss_coord = std::ptrdiff_t;
            gauss_coord gx = m_params.source_pos_x + m_frame_nr * m_params.source_speed_x;
            gauss_coord gy = m_params.source_pos_y + m_frame_nr * m_params.source_speed_y;
            double res = m_gauss({gx, gy});

            // Apply diffraction pattern
            res *= diffract(p.x, p.y, m_params.x0, m_params.y0);

            return result_type(res);
        }

        diffraction_params m_params;
        generate_gauss m_gauss;
        int m_frame_nr;
    };

    using generate_diffraction_locator_t = boost::gil::virtual_2d_locator<generate_diffraction, false>;
    using generate_diffraction_view_t = boost::gil::image_view<generate_diffraction_locator_t>;

    // Generator adaptor that adds noise given random number distribution
    template <typename Generator, typename Distribution>
    struct generate_with_noise
    {
        using const_t = generate_with_noise<Generator, Distribution>;
        using value_type = typename Generator::value_type;
        using reference = typename Generator::reference;
        using const_reference = typename Generator::const_reference;
        using argument_type = typename Generator::argument_type;
        using result_type = typename Generator::result_type;

        static const bool is_mutable = false;

        generate_with_noise(Generator const& generator, Distribution const& distribution) :
            m_generator(generator), m_distribution(distribution)
        {
        }

        result_type operator()(point_t const& p) const
        {
            std::random_device rd{};
            std::mt19937_64 gen{rd()};

            auto noise = m_distribution(gen);
            return result_type(m_generator(p) + noise);
        }

        mutable Distribution m_distribution;
        Generator m_generator;
    };

    template <typename Generator>
    auto make_generated_view(point_t const& dimensions, Generator const& generator)
    {
        using generate_locator_t = boost::gil::virtual_2d_locator<Generator, false>;
        using generate_view_t = boost::gil::image_view<generate_locator_t>;

        generate_locator_t loc(lima::point_t(0, 0), lima::point_t(1, 1), generator);
        generate_view_t view(dimensions, loc);

        return view;
    }

    template <typename Generator>
    auto make_generated_view(point_t const& topleft, point_t const& dimensions, Generator const& generator)
    {
        using generate_locator_t = boost::gil::virtual_2d_locator<Generator, false>;
        using generate_view_t = boost::gil::image_view<generate_locator_t>;

        generate_locator_t loc(topleft, lima::point_t(1, 1), generator);
        generate_view_t view(dimensions, loc);

        return view;
    }

    template <typename Generator, typename Distribution>
    auto make_generated_with_noise_view(point_t const& dimensions, Generator const& generator,
                                        Distribution const& distribution = std::normal_distribution<float>())
    {
        using generator_t = generate_with_noise<Generator, Distribution>;
        using generate_locator_t = boost::gil::virtual_2d_locator<generator_t, false>;
        using generate_view_t = boost::gil::image_view<generate_locator_t>;

        generate_locator_t loc(lima::point_t(0, 0), lima::point_t(1, 1), generator_t(generator, distribution));
        generate_view_t view(dimensions, loc);

        return view;
    }

} // namespace processing::generator
} // namespace lima
