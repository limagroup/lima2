// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstdint>
#include <limits>
#include <type_traits>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/for_each2.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/frame_typedefs.hpp>

namespace lima
{
namespace processing
{
    struct counters_result
    {
        int m_count = 0;
        float m_min = 0.0f;
        float m_max = 0.0f;
        float m_avg = 0.0f;
        float m_std = 0.0f;
        double m_sum = 0.0;

        friend bool operator==(counters_result const& lhs, counters_result const& rhs) = default;
        friend bool operator!=(counters_result const& lhs, counters_result const& rhs) = default;
    };

    template <typename T, class Enable = void>
    struct counters_accumulator_set
    {
        counters_accumulator_set() :
            m_count(0),
            m_min(std::numeric_limits<std::uint32_t>::max()),
            m_max(std::numeric_limits<std::uint32_t>::min()),
            m_sum(0),
            m_sum_sq(0)
        {
        }

        typedef void result_type;

        void operator()(std::uint32_t val)
        {
            m_count += 1;
            m_min = std::min(m_min, val);
            m_max = std::max(m_max, val);
            m_sum += val;
            m_sum_sq += val * val;
        }

        template <class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            ar& m_count;
            ar& m_min;
            ar& m_max;
            ar& m_sum;
            ar& m_sum_sq;
        }

        std::uint32_t m_count;
        std::uint32_t m_min;
        std::uint32_t m_max;
        std::uint64_t m_sum;
        std::uint64_t m_sum_sq;
    };

    // Specialize for floating point pixel type
    template <typename T>
    struct counters_accumulator_set<T, typename std::enable_if_t<std::is_floating_point_v<T>>>
    {
        counters_accumulator_set() :
            m_count(0),
            m_min(std::numeric_limits<double>::max()),
            m_max(std::numeric_limits<double>::min()),
            m_sum(0.0),
            m_sum_sq(0.0)
        {
        }

        typedef void result_type;

        void operator()(double val)
        {
            m_count += 1;
            m_min = std::min(m_min, val);
            m_max = std::max(m_max, val);
            m_sum += val;
            m_sum_sq += val * val;
        }

        template <class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            ar& m_count;
            ar& m_min;
            ar& m_max;
            ar& m_sum;
            ar& m_sum_sq;
        }

        std::uint32_t m_count;
        double m_min;
        double m_max;
        double m_sum;
        double m_sum_sq;
    };

    template <typename T>
    counters_result counters(counters_accumulator_set<T> const& acc)
    {
        counters_result res;

        // Just in case all pixels were masked
        if (acc.m_count == 0)
            return res;

        res.m_count = acc.m_count;
        res.m_min = acc.m_min;
        res.m_max = acc.m_max;
        res.m_avg = acc.m_sum / (double) acc.m_count;
        res.m_std = std::sqrt(acc.m_sum_sq / (double) acc.m_count - res.m_avg * res.m_avg);
        res.m_sum = acc.m_sum;
        return res;
    }

    template <typename SrcView>
    counters_result counters(const SrcView& src)
    {
        using pod_t = typename SrcView::value_type;

        counters_accumulator_set<pod_t> acc;

        acc = boost::gil::for_each_pixel(src, acc);

        return counters(acc);
    }

    template <typename SrcView>
    counters_result counters(const SrcView& src, const bpp8c_view_t& mask)
    {
        using pod_t = typename SrcView::value_type;

        assert(src.dimensions() == mask.dimensions());

        counters_accumulator_set<pod_t> acc;

        boost::gil::for_each_pixel(src, mask, [&acc](auto val, auto m) {
            if (m == 1)
                acc(val);
        });

        return counters(acc);
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    counters_result counters(const boost::gil::any_image_view<SrcViews...>& src)
    {
        return boost::variant2::visit([](auto&& v) { return counters(v); }, src);
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews>
    counters_result counters(const boost::gil::any_image_view<SrcViews...>& src, const bpp8c_view_t& mask)
    {
        return boost::variant2::visit([&mask](auto&& v) { return counters(v, mask); }, src);
    }

} //namespace processing
} //namespace lima
