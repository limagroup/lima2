// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <array>
#include <functional>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>

#include <lima/exceptions.hpp>

namespace lima
{
namespace processing
{
    template <typename SrcView, typename DestView>
    void sum(const SrcView& src, const DestView& dst)
    {
        namespace gil = boost::gil;

        boost::gil::transform_pixels(src, dst, dst, [](auto lhs, auto rhs) { return lhs + rhs; });
    }

    // Dynamic version of the algorithm
    template <typename... SrcViews, typename... DestViews>
    void sum(const boost::gil::any_image_view<SrcViews...>& src, const boost::gil::any_image_view<DestViews...>& dst)
    {
        return boost::variant2::visit([](auto&& v1, auto&& v2) { return sum(v1, v2); }, src, dst);
    }

} //namespace processing
} //namespace lima
