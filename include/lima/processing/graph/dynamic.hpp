// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory>
#include <optional>
#include <string>
#include <unordered_map>

#include <boost/graph/directed_graph.hpp>
#include <boost/property_tree/ptree.hpp>

#include <tbb/flow_graph.h>

#include <lima/io/h5/params.hpp>
#include <lima/processing/nodes/io_hdf5.hpp>

namespace lima
{
namespace processing
{
    /// A dynamic processing graph
    /// \tparam InputNode A TBB Input node
    template <typename InputNode>
    struct dynamic_graph
    {
        using frame_t = typename InputNode::output_type;

        using node_t = tbb::flow::function_node<frame_t, frame_t>;

        // Define graph properties
        struct vertex_prop
        {
            int id;
            boost::property_tree::ptree ptree; // a property tree holding the configuration of the process
            std::shared_ptr<node_t> node;
        };

        // Define the graph and asociated types
        using graph_t = boost::directed_graph<vertex_prop>;
        using vertex_descriptor_t = typename boost::graph_traits<graph_t>::vertex_descriptor;
        using vertex_bundle_t = typename boost::vertex_bundle_type<graph_t>::type;

        dynamic_graph() : m_flow_graph(m_tg_ctxt) {}

        ~dynamic_graph() { m_flow_graph.wait_for_all(); }

        // Deserialize the graph
        void from_ptree(boost::property_tree::ptree const& tree)
        {
            std::unordered_map<int, vertex_descriptor_t> vertices_lut; //Temporary LUT for deserializing

            for (const auto& process : tree.get_child("processes")) {
                auto v = g.add_vertex();
                g[v].ptree = process.second;

                // Insert in the LUT (for later use with connections)
                boost::property_tree::ptree const& p = g[v].ptree;
                vertices_lut.insert({p.get<int>("id"), v});
            }

            for (const auto& connection : tree.get_child("connections")) {
                int src = connection.second.get<int>("src");
                int dst = connection.second.get<int>("dst");

                g.add_edge(vertices_lut[src], vertices_lut[dst]);
            }
        }

        template <typename Factory, typename SourceNode>
        void init(Factory const& fab, SourceNode&& src)
        {
            m_flow_graph.reset();

            // For each vertex, connect receivers
            auto [vbegin, vend] = boost::vertices(g);
            std::for_each(vbegin, vend, [this, &src, &fab](auto v) {
                boost::property_tree::ptree const& p = g[v].ptree;
                g[v].node = fab.create(p.get<std::string>("type"), std::as_const(g[v].ptree));

                //If this is a source
                if (boost::in_degree(v, g) == 0)
                    tbb::flow::make_edge(src, *g[v].node);

                // Connect to all the upstream processes (the arity of the process should match the number of incoming edges)
                auto [ebegin, eend] = boost::in_edges(v, g);
                std::for_each(ebegin, eend,
                              [&](auto e) { tbb::flow::make_edge(*g[boost::source(e, g)].node, *g[v].node); });
            });

            src.activate();
        }

        void wait_for_all() { m_flow_graph.wait_for_all(); }

        void cancel()
        {
            // See Cancel a Graph Explicitly
            // https://software.intel.com/en-us/node/517371
            m_tg_ctxt.cancel_group_execution();
            m_flow_graph.wait_for_all();
        }

        graph_t g;

        tbb::task_group_context m_tg_ctxt;
        tbb::flow::graph m_flow_graph;

        bool is_initialized = false;

        tbb::flow::graph m_graph;
        //std::optional<InputNode> m_input_node;
    };

} // namespace processing
} // namespace lima
