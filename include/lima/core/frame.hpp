// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cassert>
#include <cstdint>
#include <cstring>

#include <memory>
#include <memory_resource>
#include <utility>
#include <variant>

#include <boost/smart_ptr/shared_ptr.hpp>

#include <boost/json/value.hpp>

#include <lima/core/point.hpp>
#include <lima/core/pixel.hpp>
#include <lima/core/frame_info.hpp>
#include <lima/core/frame_metadata.hpp>

namespace lima
{

// Lima1 equivalent
// shared: deleter calls allocator free + callback;
// mapped: deleter calls callback

///
/// \warning This class has pointer semantics / shallow copy. Use clone() to get an actual copy of the frame.
/// \warning Pixels are not default constructed nor destructed
class frame
{
  public:
    static const std::size_t num_dimensions = 3;
    using allocator_t = std::pmr::polymorphic_allocator<std::byte>;
    using coord_t = std::ptrdiff_t;
    using point_t = point<coord_t>;
    using buffer_ptr_t = boost::shared_ptr<std::byte[]>;

    // Construct empty
    explicit frame() : m_align_in_bytes(0) {}

    // Construct mapped
    template <typename Deleter>
    explicit frame(const point_t& dims, const pixel_enum& pixel, std::byte* memory, std::ptrdiff_t rowsize,
                   Deleter deleter) :
        m_dimensions(dims), m_pixel(pixel), m_memory(memory, deleter), data(memory), rowsize(rowsize)
    {
    }

    template <typename Deleter>
    explicit frame(const point_t& dims, std::size_t nb_channels, const pixel_enum& pixel, std::byte* memory,
                   std::ptrdiff_t rowsize, Deleter deleter) :
        m_dimensions(dims),
        m_nb_channels(nb_channels),
        m_pixel(pixel),
        m_memory(memory, deleter),
        data(memory),
        rowsize(rowsize)
    {
    }

    template <typename Deleter>
    explicit frame(coord_t width, coord_t height, const pixel_enum& pixel, std::byte* memory, std::ptrdiff_t rowsize,
                   Deleter deleter) :
        m_dimensions(width, height), m_pixel(pixel), m_memory(memory, deleter), data(memory), rowsize(rowsize)
    {
    }

    template <typename Deleter>
    explicit frame(coord_t width, coord_t height, std::size_t nb_channels, const pixel_enum& pixel, std::byte* memory,
                   std::ptrdiff_t rowsize, Deleter deleter) :
        m_dimensions(width, height),
        m_nb_channels(nb_channels),
        m_pixel(pixel),
        m_memory(memory, deleter),
        data(memory),
        rowsize(rowsize)
    {
    }

    // Construct allocated
    frame(const point_t& dims, const pixel_enum& pixel, std::size_t alignment = 0, const allocator_t& alloc_in = {}) :
        frame(dims, 1, pixel, alignment, alloc_in)
    {
    }

    frame(coord_t width, coord_t height, const pixel_enum& pixel, std::size_t alignment = 0,
          const allocator_t& alloc_in = {}) :
        frame(width, height, 1, pixel, alignment, alloc_in)
    {
    }

    frame(point_t const& dims, std::size_t nb_channels, const pixel_enum& pixel, std::size_t alignment = 0,
          const allocator_t& alloc_in = {}) :
        m_dimensions(dims), m_nb_channels(nb_channels), m_pixel(pixel), m_align_in_bytes(alignment), m_alloc(alloc_in)
    {
        allocate(dims, nb_channels, pixel);
    }

    frame(coord_t width, coord_t height, std::size_t nb_channels, const pixel_enum& pixel, std::size_t alignment = 0,
          const allocator_t& alloc_in = {}) :
        frame({width, height}, nb_channels, pixel, alignment, alloc_in)
    {
    }

    // Copy constructor
    frame(frame const&) = default;

    // Copy assign operator
    frame& operator=(const frame& rhs)
    {
        frame tmp(rhs);
        swap(tmp);
        return *this;
    }

    allocator_t& get_allocator() { return m_alloc; }
    allocator_t const& get_allocator() const { return m_alloc; }

    // required by MutableContainerConcept
    void swap(frame& frm);

    // Create a new, independent copy of the frame
    frame clone(std::size_t alignment = 0, const allocator_t& alloc_in = {}) const
    {
        frame new_frame(m_dimensions, m_nb_channels, m_pixel, alignment, alloc_in);
        std::memcpy(new_frame.data, data, total_size_in_bytes());
        new_frame.metadata = metadata;
        new_frame.attributes = attributes;
        return new_frame;
    }

    // Recreate without Allocator
    void recreate(const point_t& dims, const pixel_enum& pixel, std::size_t alignment = 0)
    {
        recreate(dims, 1, pixel, alignment);
    }

    void recreate(coord_t width, coord_t height, const pixel_enum& pixel, std::size_t alignment = 0)
    {
        recreate(width, height, 1, pixel, alignment);
    }

    void recreate(const point_t& dims, std::size_t nb_channels, const pixel_enum& pixel, std::size_t alignment = 0);

    void recreate(coord_t width, coord_t height, std::size_t nb_channels, const pixel_enum& pixel,
                  std::size_t alignment = 0)
    {
        recreate(point_t(width, height), nb_channels, pixel, alignment);
    }

    // Recreate with Allocator
    void recreate(const point_t& dims, const pixel_enum& pixel, std::size_t alignment, const allocator_t& alloc_in)
    {
        recreate(dims, 1, pixel, alignment, alloc_in);
    }

    void recreate(coord_t width, coord_t height, const pixel_enum& pixel, std::size_t alignment,
                  const allocator_t& alloc_in)
    {
        recreate(width, height, 1, pixel, alignment, alloc_in);
    }

    void recreate(const point_t& dims, std::size_t nb_channels, const pixel_enum& pixel, std::size_t alignment,
                  const allocator_t& alloc_in);

    void recreate(coord_t width, coord_t height, std::size_t nb_channels, const pixel_enum& pixel,
                  std::size_t alignment, const allocator_t& alloc_in)
    {
        recreate(point_t(width, height), nb_channels, pixel, alignment, alloc_in);
    }

    const point_t& dimensions() const { return m_dimensions; }
    coord_t width() const { return m_dimensions.x; }
    coord_t height() const { return m_dimensions.y; }
    std::size_t size() const { return m_dimensions.x * m_dimensions.y; }
    pixel_enum pixel_type() const { return m_pixel; }
    std::size_t nb_channels() const { return m_nb_channels; }
    bool is_homogeneous() const { return true; }

    /// Returns true if the frame is empty (no memory allocated / mapped)
    bool empty() const { return data == nullptr; }

    /// Returns the underlying memory buffer (reference counted)
    buffer_ptr_t get_buffer_ptr() const { return m_memory; }

    /// Returns the size of a row in bytes including padding for alignement
    std::size_t row_size_in_bytes() const { return rowsize; }

    /// Returns the size of a frame channel in bytes including padding for alignement
    std::size_t channel_size_in_bytes() const { return rowsize * height(); }

    /// Returns the total size of the frame in bytes including padding for alignement
    std::size_t total_size_in_bytes() const { return rowsize * height() * nb_channels(); }

    /// Returns true if all pixels are zeros
    bool is_zero() const;

    // Declared public for friend view() and const_view()
    std::byte* data = {nullptr};  //<! Pointer to the beginning of the data
    std::ptrdiff_t rowsize = {0}; //<! Row size in memory units

  private:
    point_t m_dimensions;                   //<! Frame dimensions
    pixel_enum m_pixel = pixel_enum::gray8; //<! Pixel type
    std::size_t m_nb_channels = 1;          //<! Number of channels / bands

    allocator_t m_alloc;                 //<! The polymorphic memory allocator
    std::size_t m_align_in_bytes = {0};  //<! Required memory alignement for rows
    buffer_ptr_t m_memory;               //<! A pointer with reference counting to the allocated memory
    std::size_t m_allocated_bytes = {0}; //<! Number of allocated bytes in memory (might be zero if mapped memory)

    /// Allocate memory
    void allocate(point_t const& dims, std::size_t nb_channels, pixel_enum const& pixel);

    /// Return the number of bytes per row
    std::ptrdiff_t get_row_size_in_bytes(coord_t width, pixel_enum const& pixel) const;

    /// Returns C pointer to the given channel
    std::byte* get_raw_data(std::size_t channel_idx) { return data + channel_size_in_bytes() * channel_idx; }

    /// Return the total allocated (or to be allocated) size in bytes
    std::ptrdiff_t total_allocated_size_in_bytes(point_t const& dims, std::size_t nb_channels,
                                                 pixel_enum const& pixel) const;

  public:
    using attributes_t = boost::json::value;
    using metadata_t = frame_metadata;

    metadata_t metadata;     //<! Static typed metadata
    attributes_t attributes; //<! Dynamic attributes
};

inline void swap(frame& frm1, frame& frm2)
{
    frm1.swap(frm2);
}

} // namespace lima
