// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if defined(LIMA_ENABLE_CUDA)

#include <memory_resource>

#include <cuda.h>

namespace lima
{
/// A resource that uses `cudaMallocHost` to allocate memory on host
class cuda_resource : public std::pmr::memory_resource
{
    const int m_node;

  public:
    cuda_resource(int node = 0) noexcept : m_node{node} {};

    void* do_allocate(size_t bytes, size_t alignment) override
    {
        void* res;
        if (cudaMallocHost(&res, bytes) != cudaSuccess)
            throw std::;

        //TODO Manage alignement
        return res;
    }
    void do_deallocate(void* ptr, size_t bytes, size_t alignment) override { numa_free(ptr); }
    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override { return this == &other; }
};

} //namespace lima

#endif //defined(LIMA_ENABLE_CUDA)
