// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if defined(LIMA_ENABLE_NUMA)

#include <memory_resource>

#include <numa.h>

namespace lima
{
/// A resource that uses `numa_alloc_onnode` to allocate memory on a specific numa node
class numa_resource : public std::pmr::memory_resource
{
    const int m_node;

  public:
    numa_resource(int node = 0) noexcept : m_node{node} {};

    void* do_allocate(size_t bytes, size_t alignment) override { return numa_alloc_onnode(bytes, m_node); }
    void do_deallocate(void* ptr, size_t bytes, size_t alignment) override { numa_free(ptr, bytes); }
    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override { return this == &other; }
};

} //namespace lima

#endif //defined(LIMA_ENABLE_NUMA)
