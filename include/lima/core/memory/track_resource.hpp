// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory_resource>

namespace lima
{
/// A memory resource that keeps track fo the allocated memory.
///
/// nb_bytes is the number of bytes allocated by this instance.
class track_resource : public std::pmr::memory_resource
{
  public:
    explicit track_resource(std::pmr::memory_resource* up = std::pmr::get_default_resource()) : m_upstream{up} {}

    void* do_allocate(size_t bytes, size_t alignment) override
    {
        void* ret = m_upstream->allocate(bytes, alignment);
        if (ret)
            nb_bytes += bytes;
        return ret;
    }
    void do_deallocate(void* ptr, size_t bytes, size_t alignment) override
    {
        if (ptr)
            nb_bytes -= bytes;
        m_upstream->deallocate(ptr, bytes, alignment);
    }
    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override { return this == &other; }

    std::size_t nb_bytes = 0;
    std::pmr::memory_resource* m_upstream;
};

} //namespace lima
