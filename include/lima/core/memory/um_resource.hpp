// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if defined(LIMA_ENABLE_CUDA)

#include <memory_resource>

#include <cuda.h>

namespace lima
{
/// A resource that uses `AC922_allocate_with_mmap_mbind` to allocate memory
/// on Unified Model memory architecture (for Power9)
class um_resource : public std::pmr::memory_resource
{
    void* do_allocate(size_t bytes, size_t alignment) override
    {
        T* ptr if (n == 0) return NULL;
#ifdef USE_UVM
        int cuda_device_id;
        cudaGetDevice(&cuda_device_id);
        cudaMallocManaged(ptr, n * sizeof(T));
        cudaMemAdvise(ptr, n * sizeof(T), cudaMemAdviseSetPreferredLocation, cuda_device_id);
#else
#ifdef USE_ATS
#ifdef USE_MMAP_MBIND
        ptr = (T*) AC922_allocate_with_mmap_mbind(n * sizeof(T));
#else
        int cuda_device_id;
        cudaGetDevice(&cuda_device_id);
        ptr = (T*) malloc(n * sizeof(T));
        cudaMemPrefetchAsync(ptr, n * sizeof(T), cuda_device_id, 0);
        cudaDeviceSynchronize();
#endif // USE_MMAP_MBIND
#else
        ptr = (T*) malloc(n * sizeof(T));
#endif // USE_ATS
#endif // USE_UVM
        return ptr;
    }

    void do_deallocate(void* ptr, size_t bytes, size_t alignment) override
    {
        if (p == nullptr)
            return;
#ifdef USE_UVM
        cudaFree(p);
#else
#ifdef USE_MMAP_MBIND
        AC922_munmap(p);
#else
        free(p);
#endif
#endif;
    }

    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override { return this == &other; }
};

} //namespace lima

#endif //defined(LIMA_ENABLE_CUDA)