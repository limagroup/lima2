// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>
#include <boost/describe/io_structs.hpp>

#include <lima/core/point.hpp>

namespace boost::gil
{
BOOST_DESCRIBE_STRUCT(point<std::ptrdiff_t>, (), (x, y))

// clang-format off
BOOST_ANNOTATE_MEMBER(point<std::ptrdiff_t>, x,
    (doc, "x"),
    (desc, "The position in the horizontal direction"))

BOOST_ANNOTATE_MEMBER(point<std::ptrdiff_t>, y,
    (doc, "y"),
    (desc, "The position in the vertical direction"))
// clang-format on

BOOST_DESCRIBE_STRUCT(point<float>, (), (x, y))

// clang-format off
BOOST_ANNOTATE_MEMBER(point<float>, x,
    (doc, "x"),
    (desc, "The position in the horizontal direction"))

BOOST_ANNOTATE_MEMBER(point<float>, y,
    (doc, "y"),
    (desc, "The position in the vertical direction"))
// clang-format on

BOOST_DESCRIBE_STRUCT(point<int>, (), (x, y))

// clang-format off
BOOST_ANNOTATE_MEMBER(point<int>, x,
    (doc, "x"),
    (desc, "The position in the horizontal direction"))

BOOST_ANNOTATE_MEMBER(point<int>, y,
    (doc, "y"),
    (desc, "The position in the vertical direction"))
// clang-format on

using boost::describe::operator<<;

} // namespace boost::gil
