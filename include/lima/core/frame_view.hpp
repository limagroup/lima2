// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/gil/pixel_pod.hpp>
#include <boost/gil/image_view.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_typedefs.hpp>

namespace lima
{

template <typename... Views>
using frame_view = boost::gil::any_image_view<Views...>;

using view_t = frame_view<bpp8_view_t, bpp8s_view_t, bpp16_view_t, bpp16s_view_t, bpp32_view_t, bpp32s_view_t,
                          bpp32f_view_t, bpp64f_view_t>;

using const_view_t = typename view_t::const_t;

#define LIMA_VIEW_EMPLACE_IMPL(v) \
    res.emplace<v>(frm.dimensions(), typename v::locator(typename v::x_iterator(data), frm.rowsize));

// Returns a mutable view
inline view_t view(frame& frm, int channel_idx = 0)
{
    assert(channel_idx < frm.nb_channels());

    view_t res;

    std::byte* data = frm.data + frm.channel_size_in_bytes() * channel_idx;

    switch (frm.pixel_type()) {
    case pixel_enum::gray8:
        LIMA_VIEW_EMPLACE_IMPL(bpp8_view_t)
        break;
    case pixel_enum::gray8s:
        LIMA_VIEW_EMPLACE_IMPL(bpp8s_view_t)
        break;
    case pixel_enum::gray16:
        LIMA_VIEW_EMPLACE_IMPL(bpp16_view_t)
        break;
    case pixel_enum::gray16s:
        LIMA_VIEW_EMPLACE_IMPL(bpp16s_view_t)
        break;
    case pixel_enum::gray32:
        LIMA_VIEW_EMPLACE_IMPL(bpp32_view_t)
        break;
    case pixel_enum::gray32s:
        LIMA_VIEW_EMPLACE_IMPL(bpp32s_view_t)
        break;
    case pixel_enum::gray32f:
        LIMA_VIEW_EMPLACE_IMPL(bpp32f_view_t)
        break;
    case pixel_enum::gray64f:
        LIMA_VIEW_EMPLACE_IMPL(bpp64f_view_t)
        break;
    default:
        assert(false);
    }

    return res;
}

template <typename View>
View view(frame& frm, int channel_idx = 0)
{
    return boost::variant2::get<View>(view(frm, channel_idx));
}

// Returns a const view
inline const_view_t const_view(frame const& frm, int channel_idx = 0)
{
    assert(channel_idx < frm.nb_channels());

    const_view_t res;

    // Get channel data
    std::byte* data = frm.data + frm.channel_size_in_bytes() * channel_idx;

    switch (frm.pixel_type()) {
    case pixel_enum::gray8:
        LIMA_VIEW_EMPLACE_IMPL(bpp8c_view_t)
        break;
    case pixel_enum::gray8s:
        LIMA_VIEW_EMPLACE_IMPL(bpp8sc_view_t)
        break;
    case pixel_enum::gray16:
        LIMA_VIEW_EMPLACE_IMPL(bpp16c_view_t)
        break;
    case pixel_enum::gray16s:
        LIMA_VIEW_EMPLACE_IMPL(bpp16sc_view_t)
        break;
    case pixel_enum::gray32:
        LIMA_VIEW_EMPLACE_IMPL(bpp32c_view_t)
        break;
    case pixel_enum::gray32s:
        LIMA_VIEW_EMPLACE_IMPL(bpp32sc_view_t)
        break;
    case pixel_enum::gray32f:
        LIMA_VIEW_EMPLACE_IMPL(bpp32fc_view_t)
        break;
    case pixel_enum::gray64f:
        LIMA_VIEW_EMPLACE_IMPL(bpp64fc_view_t)
        break;
    default:
        assert(false);
    }

    return res;
}

template <typename View>
View const_view(frame const& frm, int channel_idx = 0)
{
    return boost::variant2::get<View>(const_view(frm, channel_idx));
}

#undef LIMA_VIEW_EMPLACE_IMPL

} //namespace lima
