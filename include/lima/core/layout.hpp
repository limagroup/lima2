// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <algorithm>
#include <vector>

#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>

namespace lima
{
// A layout defines the layout of partial images (layout_item) in the final image
// Source images may be:
//  - image in memory
//  - HDF5 file (if layout is used to generate VDS)
//  - MPI buffer identified by the process rank
//
// A layout is basically a container of items
struct layout
{
    using coord_t = std::ptrdiff_t;
    using point_t = point<std::ptrdiff_t>;
    using rectangle_t = rectangle<std::ptrdiff_t>;

    struct item
    {
        // Position in the destination
        point_t dst_topleft;

        // Selection within the source
        rectangle_t src_roi;

        //bool in_layout() const { return (src_dimensions.x > 0) && (src_dimensions.y > 0); }

        //rectangle_t src_bounding_box() const { return {src_topleft, src_dimensions}; }

        //rectangle_t dst_bounding_box() const { return {dst_topleft, get_dst_dimensions()}; }

        //point_t dst_dimensions() const { return processing::geom::output_dimensions(xform, src_dimensions); }
    };

    layout() = default;

    layout(point_t const& dimensions, std::vector<item>&& items) : m_dimensions(dimensions), m_items(std::move(items))
    {
    }

    layout(coord_t width, coord_t height, std::vector<item>&& items) :
        m_dimensions(width, height), m_items(std::move(items))
    {
    }

    point_t dimensions() const { return m_dimensions; }
    std::size_t nb_items() const { return m_items.size(); }

    // Basic algorithms
    template <typename UnaryFunction>
    UnaryFunction for_each_item(UnaryFunction func) const
    {
        std::for_each(m_items.begin(), m_items.end(), func);
        return func;
    }

    std::vector<item> m_items;
    point_t m_dimensions;
};

} //namespace lima
