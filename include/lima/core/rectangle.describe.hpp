// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>
#include <boost/describe/io_structs.hpp>

#include <lima/core/rectangle.hpp>
#include <lima/core/point.describe.hpp>

namespace lima
{
BOOST_DESCRIBE_STRUCT(rectangle<std::ptrdiff_t>, (), (topleft, dimensions))

// clang-format off
BOOST_ANNOTATE_MEMBER(rectangle<std::ptrdiff_t>, topleft,
    (doc, "top left corner coordinate"),
    (desc, "The top left corner coordinate of the region of interest"))

BOOST_ANNOTATE_MEMBER(rectangle<std::ptrdiff_t>, dimensions,
    (doc, "dimensions"),
    (desc, "The dimensions of the region of interest"))
// clang-format on

using boost::describe::operator<<;

} //namespace lima
