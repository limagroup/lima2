// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>
#include <boost/describe/io_structs.hpp>

#include <lima/core/enums.describe.hpp>
#include <lima/core/point.describe.hpp>

#include <lima/core/frame_info.hpp>

namespace lima
{
BOOST_DESCRIBE_STRUCT(frame_info, (), (m_dimensions, m_nb_channels, m_pixel_type))

// clang-format off
BOOST_ANNOTATE_MEMBER(frame_info, m_dimensions,
    (name, "dimensions"),
    (desc, "frame dimensions"),
    (doc, "The frame dimensions [px]"))

BOOST_ANNOTATE_MEMBER(frame_info, m_nb_channels,
    (name, "nb_channels"),
    (desc, "frame nb channels"),
    (doc, "The number of channels"))

BOOST_ANNOTATE_MEMBER(frame_info, m_pixel_type,
    (name, "pixel_type"),
    (desc, "frame pixel type"),
    (doc, "The pixel type (bitdepth)"))
// clang-format on

using boost::describe::operator<<;

} // namespace lima
