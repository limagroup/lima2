// Copyright 2005-2007 Adobe Systems Incorporated
//
// Distributed under the Boost Software License, Version 1.0
// See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt

#pragma once

#include <cstddef>
#include <cmath>

#include <type_traits>

#include <boost/gil/point.hpp>
#include <boost/gil/utilities.hpp>

namespace lima
{
// PointModel
using boost::gil::axis_value;
using boost::gil::point;

// Common type to represent 2D dimensions or in-memory size of image or view.
using boost::gil::point_t;

// PointAlgorithm
using boost::gil::iceil;
using boost::gil::ifloor;
using boost::gil::iround;

template <typename T>
BOOST_FORCEINLINE boost::gil::point<T> abs(const boost::gil::point<T>& p)
{
    using std::abs;
    return {abs(p.x), abs(p.y)}; // Get boost::abs via ADL for rational types or default to std::abs
}

template <typename T>
BOOST_FORCEINLINE void swap_dimensions(boost::gil::point<T>& p)
{
    T tmp = p.x;
    p.x = p.y;
    p.y = tmp;
}

} // namespace lima
