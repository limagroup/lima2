// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/serialization/serialization.hpp>

#include <lima/core/point.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for point
    template <typename Archive, typename T>
    void serialize(Archive& ar, lima::point<T>& p, const unsigned int version)
    {
        ar& p.x;
        ar& p.y;
    }

} // namespace serialization
} // namespace boost
