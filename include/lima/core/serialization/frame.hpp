// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/binary_object.hpp>

#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>
#include <lima/core/frame.hpp>

#include <lima/core/serialization/point.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for frame
    template <class Archive>
    void save(Archive& ar, const lima::frame& frm, unsigned int)
    {
        ar << frm.dimensions();
        ar << frm.pixel_type();
        ar << boost::serialization::make_binary_object(frm.data, frm.dimensions().y * frm.rowsize);
    }

    template <class Archive>
    void load(Archive& ar, lima::frame& frm, unsigned int)
    {
        lima::frame::point_t dimensions;
        lima::pixel_enum pixel;

        ar >> dimensions;
        ar >> pixel;
        frm.recreate(dimensions, pixel);

        ar >> boost::serialization::make_binary_object(frm.data, frm.dimensions().y * frm.rowsize);
    }

} // namespace serialization
} // namespace boost

// outside of any namespace
BOOST_SERIALIZATION_SPLIT_FREE(lima::frame)
