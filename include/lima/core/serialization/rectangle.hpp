// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/serialization/serialization.hpp>

#include <lima/core/rectangle.hpp>
#include <lima/core/serialization/point.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for rectangle
    template <typename Archive, typename T>
    void serialize(Archive& ar, lima::rectangle<T>& p, const unsigned int version)
    {
        ar& p.topleft;
        ar& p.dimensions;
    }

} // namespace serialization
} // namespace boost
