// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

#include <lima/core/layout.hpp>
#include <lima/core/serialization/point.hpp>
#include <lima/core/serialization/rectangle.hpp>

namespace boost
{
namespace serialization
{
    /// Template specialization for layout::item
    template <typename Archive>
    void serialize(Archive& ar, lima::layout::item& l, const unsigned int version)
    {
        ar& l.dst_topleft;
        ar& l.src_roi;
    }

    /// Template specialization for layout
    template <typename Archive>
    void serialize(Archive& ar, lima::layout& l, const unsigned int version)
    {
        ar& l.m_dimensions;
        ar& l.m_items;
    }

} // namespace serialization
} // namespace boost
