// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <numbers>

#include <lima/exceptions.hpp>
#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>

namespace lima
{
/// Arc
template <typename T>
struct arc
{
    using value_type = T;
    using point_t = point<T>;

    arc() = default;

    /// Contruct an arcfrom its center coordinate, radius and angles
    arc(point_t center_arg, T r1, T r2, T a1, T a2) : center(center_arg), r1(r1), r2(r2), a1(a1), a2(a2)
    {
        if (not(r1 < r2))
            LIMA_THROW_EXCEPTION(lima::invalid_argument("Precondition r1 < r2 failed"));

        if (not(a1 < a2))
            LIMA_THROW_EXCEPTION(lima::invalid_argument("Precondition a1 < a2 failed"));
    }
    arc(T x, T y, T r1, T r2, T a1, T a2) : center(x, y), r1(r1), r2(r2), a1(a1), a2(a2) {}

    point_t center;
    T r1, r2;
    T a1, a2;
};

using arc_t = arc<float>;

template <typename T>
rectangle_t inline bounding_box(arc<T> const& arc)
{
    using namespace std::numbers;

    assert(arc.r1 < arc.r2);
    assert(arc.a1 < arc.a2);

    std::vector<double> x, y;

    // 6 points to consider: the compass points on the circle and North, East, South and West
    std::array<double, 6> r{arc.r2, arc.r2, arc.r2, arc.r2, arc.r2, arc.r2};
    std::array<double, 6> a{arc.a1, arc.a2, 0., 90., 180., 270.};

    for (int i = 0; i < 6; ++i) {
        // Filter out those of these six points, start, end, north, east, south, west that are outside the sector defined
        if (arc.a1 <= a[i] && a[i] <= arc.a2) {
            x.push_back(arc.center.x + r[i] * cos(a[i] * pi / 180.));
            y.push_back(arc.center.y + r[i] * sin(a[i] * pi / 180.));
        }
    }

    using std::ranges::max_element;
    using std::ranges::min_element;

    std::ptrdiff_t xmin = std::lround(*min_element(x)), ymin = std::lround(*min_element(y));
    std::ptrdiff_t xmax = std::lround(*max_element(x)), ymax = std::lround(*max_element(y));

    return {{xmin, ymin}, {xmax - xmin, ymax - ymin}};
}

template <typename T>
bool operator==(const arc<T>& a, const arc<T>& b)
{
    return (a.center == b.center) && (a.radius == b.radius) && (a.angles == b.angles);
}

template <typename T>
bool operator!=(const arc<T>& a, const arc<T>& b)
{
    return !(a == b);
}

} //namespace lima
