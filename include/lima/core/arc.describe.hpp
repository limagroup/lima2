// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/core/arc.hpp>
#include <lima/core/point.describe.hpp>

namespace lima
{
BOOST_DESCRIBE_STRUCT(arc<float>, (), (center, r1, r2, a1, a2))

// clang-format off
BOOST_ANNOTATE_MEMBER(arc<float>, center,
    (doc, "center coordinate"),
    (desc, "The center coordinate of the arc"))

BOOST_ANNOTATE_MEMBER(arc<float>, r1,
    (doc, "inside radius"),
    (desc, "The inside radius of the arc"))

BOOST_ANNOTATE_MEMBER(arc<float>, r2,
    (doc, "outside radius"),
    (desc, "The outside radius of the arc"))

BOOST_ANNOTATE_MEMBER(arc<float>, a1,
    (doc, "start angle"),
    (desc, "The start angles of the arc"))

BOOST_ANNOTATE_MEMBER(arc<float>, a2,
    (doc, "end angle"),
    (desc, "The end angles of the arc"))
// clang-format on

} //namespace lima
