// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstdint>

// Forward definition
namespace boost
{
namespace gil
{
    template <typename>
    class memory_based_step_iterator;
    template <typename>
    class memory_based_2d_locator;
    template <typename>
    class image_view;
    template <typename, bool, typename>
    class image;
} // namespace gil
} // namespace boost

#define LIMA_DEFINE_TYPEDEFS(B, CM, CS)                                                                               \
    using CS##B##_pixel_t = CM;                                                                                       \
    using CS##B##c_pixel_t = CM const;                                                                                \
    using CS##B##_ref_t = CM&;                                                                                        \
    using CS##B##c_ref_t = CM const&;                                                                                 \
    using CS##B##_ptr_t = CS##B##_pixel_t*;                                                                           \
    using CS##B##c_ptr_t = CS##B##c_pixel_t*;                                                                         \
    using CS##B##_step_ptr_t = boost::gil::memory_based_step_iterator<CS##B##_ptr_t>;                                 \
    using CS##B##c_step_ptr_t = boost::gil::memory_based_step_iterator<CS##B##c_ptr_t>;                               \
    using CS##B##_loc_t = boost::gil::memory_based_2d_locator<boost::gil::memory_based_step_iterator<CS##B##_ptr_t>>; \
    using CS##B##c_loc_t =                                                                                            \
        boost::gil::memory_based_2d_locator<boost::gil::memory_based_step_iterator<CS##B##c_ptr_t>>;                  \
    using CS##B##_step_loc_t =                                                                                        \
        boost::gil::memory_based_2d_locator<boost::gil::memory_based_step_iterator<CS##B##_step_ptr_t>>;              \
    using CS##B##c_step_loc_t =                                                                                       \
        boost::gil::memory_based_2d_locator<boost::gil::memory_based_step_iterator<CS##B##c_step_ptr_t>>;             \
    using CS##B##_view_t = boost::gil::image_view<CS##B##_loc_t>;                                                     \
    using CS##B##c_view_t = boost::gil::image_view<CS##B##c_loc_t>;                                                   \
    using CS##B##_step_view_t = boost::gil::image_view<CS##B##_step_loc_t>;                                           \
    using CS##B##c_step_view_t = boost::gil::image_view<CS##B##c_step_loc_t>;                                         \
    using CS##B##_image_t = boost::gil::image<CS##B##_pixel_t, false, std::allocator<unsigned char>>;

namespace lima
{

///  Built-in typedefs
LIMA_DEFINE_TYPEDEFS(8, std::uint8_t, bpp)
LIMA_DEFINE_TYPEDEFS(8s, std::int8_t, bpp)
LIMA_DEFINE_TYPEDEFS(16, std::uint16_t, bpp)
LIMA_DEFINE_TYPEDEFS(16s, std::int16_t, bpp)
LIMA_DEFINE_TYPEDEFS(32, std::uint32_t, bpp)
LIMA_DEFINE_TYPEDEFS(32s, std::int32_t, bpp)
LIMA_DEFINE_TYPEDEFS(32f, float, bpp)
LIMA_DEFINE_TYPEDEFS(64f, double, bpp)

} //namespace lima
