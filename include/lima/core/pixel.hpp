// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/enums.hpp>
#include <lima/core/frame_typedefs.hpp>

namespace lima
{

/// Returns the sizeof of a pixel in byte
inline std::size_t sizeof_pixel(pixel_enum p)
{
    std::size_t res = 0;

    switch (p) {
    case pixel_enum::gray8:
        res = sizeof(bpp8_pixel_t);
        break;
    case pixel_enum::gray8s:
        res = sizeof(bpp8s_pixel_t);
        break;
    case pixel_enum::gray16:
        res = sizeof(bpp16_pixel_t);
        break;
    case pixel_enum::gray16s:
        res = sizeof(bpp16s_pixel_t);
        break;
    case pixel_enum::gray32:
        res = sizeof(bpp32_pixel_t);
        break;
    case pixel_enum::gray32s:
        res = sizeof(bpp32s_pixel_t);
        break;
    case pixel_enum::gray32f:
        res = sizeof(bpp32f_pixel_t);
        break;
    case pixel_enum::gray64f:
        res = sizeof(bpp64f_pixel_t);
        break;
    default:
        assert(false);
    }

    return res;
}

} // namespace lima
