// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace lima
{
/// Reset level
enum class reset_level_enum : short
{
    soft, //!< Software reset
    hard  //!< Software and hardware reset
};

///  Acquisition mode
enum class acq_mode_enum : short
{
    normal, //!< Single image
    concat  //!< Multiple frame concatenated in a single image (over time)
};

/// Trigger mode
enum class trigger_mode_enum : short
{
    internal, //!< Internal trigger
    software, //!< Software trigger
    external, //!< External trigger
    gate      //!< External gate trigger
};

/// Acquisition state
enum class acq_state_enum : short
{
    idle,     //!< Idle
    prepared, //!< Prepared
    running,  //!< Running
    stopped,  //!< Stopped
    fault,    //!< Fault
    terminate //!< Terminate
};

/// Shutter mode
enum class shutter_mode_enum : short
{
    manual,
    auto_frame,
    auto_sequence
};

/// Autoexposure mode
enum auto_exposure_mode_enum : short
{
    off,
    on
};

/// Flip mode
enum class flip_enum : short
{
    none,
    left_right,
    up_down
};

/// Rotation mode
enum class rotation_enum : short
{

    none,
    r90cw,
    r180,
    r90ccw
};

/// Direction mode
enum class direction_enum : short
{
    horizontal,
    vertical
};

/// Pixel = channel, color space and layout
// Lets assume homogneous (same channel per color) pixels for now
// And standard layout (rgb not bgr)
enum class pixel_enum
{
    gray8,
    gray8s,
    gray16,
    gray16s,
    gray32,
    gray32s,
    gray32f,
    gray64f
};

} // namespace lima
