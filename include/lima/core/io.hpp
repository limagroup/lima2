// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <iostream>

#include <boost/core/typeinfo.hpp>

#include <lima/core/arc.hpp>
#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>
#include <lima/core/frame.hpp>
#include <lima/core/layout.hpp>

namespace boost::gil
{
template <typename T>
std::ostream& operator<<(std::ostream& os, point<T> const& p)
{
    os << "point<" << boost::core::demangled_name(typeid(T)) << ">";
    os << "{" << p.x << "," << p.x << "}";
    return os;
}
} // namespace boost::gil

namespace lima
{
template <typename T>
std::ostream& operator<<(std::ostream& os, rectangle<T> const& r)
{
    os << "rectangle<" << boost::core::demangled_name(typeid(T)) << ">";
    os << "{" << r.topleft.x << "," << r.topleft.y << "},"
       << "{" << r.dimensions.x << "x" << r.dimensions.y << "}";
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, arc<T> const& a)
{
    os << "arc<" << boost::core::demangled_name(typeid(T)) << ">";
    os << "{" << a.center.x << "," << a.center.y << "},"
       << "[" << a.r1 << " < " << a.r2 << "]"
       << "[" << a.a1 << " < " << a.a2 << "]";
    return os;
}

inline std::ostream& operator<<(std::ostream& os, frame const& frm)
{
    // TODO add pixel type
    os << frm.dimensions();
    return os;
}

inline std::ostream& operator<<(std::ostream& os, layout const& l)
{
    // TODO add layout items
    os << l.dimensions() << "[" << l.nb_items() << "] ";
    return os;
}

} // namespace lima
