// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <iostream>
#include <type_traits>

#include <boost/describe.hpp>
#include <boost/describe/io_enums.hpp>
#include <boost/mp11.hpp>

#include <lima/core/enums.hpp>
#include <lima/core/pixel.hpp>

namespace lima
{
BOOST_DESCRIBE_ENUM(reset_level_enum, soft, hard)
BOOST_DESCRIBE_ENUM(acq_state_enum, idle, prepared, running, stopped, fault, terminate)
BOOST_DESCRIBE_ENUM(acq_mode_enum, normal, concat)
BOOST_DESCRIBE_ENUM(trigger_mode_enum, internal, software, external, gate)
BOOST_DESCRIBE_ENUM(shutter_mode_enum, manual, auto_frame, auto_sequence)
BOOST_DESCRIBE_ENUM(auto_exposure_mode_enum, on, off)

BOOST_DESCRIBE_ENUM(flip_enum, none, left_right, up_down)
BOOST_DESCRIBE_ENUM(rotation_enum, none, r90cw, r180, r90ccw)
BOOST_DESCRIBE_ENUM(direction_enum, horizontal, vertical)
BOOST_DESCRIBE_ENUM(pixel_enum, gray8, gray8s, gray16, gray16s, gray32, gray32s, gray32f, gray64f)

using boost::describe::operator<<;
using boost::describe::operator>>;

} // namespace lima
