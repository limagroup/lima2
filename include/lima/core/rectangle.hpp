// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/point.hpp>

namespace lima
{
/// Rectangle
template <typename T>
struct rectangle
{
    using value_type = T;
    using point_t = point<T>;

    rectangle() = default;

    /// Contruct a rectangle from its topleft coordinate and dimensions
    rectangle(point_t topleft_arg, point_t dimensions_arg) : topleft(topleft_arg), dimensions(dimensions_arg) {}
    rectangle(T x, T y, T width, T height) : topleft(x, y), dimensions(width, height) {}

    /// Returns true if the length in at least one direction is 0
    bool is_degenerated() const { return (dimensions.x <= 0) || (dimensions.y <= 0); }

    /// Returns the top left corner point for this rectangle
    point_t tl_corner() const { return topleft; }

    /// Returns the top right corner point for this rectangle
    point_t tr_corner() const { return topleft + point_t(dimensions.x - 1, 0); }

    /// Returns the bottom left corner point for this rectangle
    point_t bl_corner() const { return topleft + point_t(0, dimensions.y - 1); }

    /// Returns the bottom right corner point for this rectangle
    point_t br_corner() const { return topleft + dimensions - point_t(1, 1); }

    //bool includes_point(point_t p) const
    //{
    //	return is_point_in_image(p - topleft, dimensions);
    //}

    /// Returns width
    value_type width() const { return dimensions.x; }

    /// Returns height
    value_type height() const { return dimensions.y; }

    /// Returns width * height
    value_type area() const { return dimensions.x * dimensions.y; }

    /// Shrinks the rectangle by shrinking its border by num
    void shrink(value_type num)
    {
        topleft += {num, num};
        dimensions -= {num, num};
    }

    /// Grows the rectangle by expanding its border by num
    void grow(value_type num)
    {
        topleft -= {num, num};
        dimensions += {num, num};
    }

    /// Contruct rectangle from opposite corners
    static rectangle from_corners(rectangle::point_t p1, rectangle::point_t p2)
    {
        point_t tl(std::min(p1.x, p2.x), std::min(p1.y, p2.y));
        point_t dim = abs(p2 - p1) + point_t(1, 1);
        return {tl, dim};
    }

    point_t topleft;
    point_t dimensions;
};

template <typename T>
bool operator==(const rectangle<T>& a, const rectangle<T>& b)
{
    return (a.topleft == b.topleft) && (a.dimensions == b.dimensions);
}

template <typename T>
bool operator!=(const rectangle<T>& a, const rectangle<T>& b)
{
    return !(a == b);
}

// Returns true if two rectangles overlap
template <typename T>
bool overlaps(const rectangle<T>& lhs, const rectangle<T>& rhs)
{
    auto l1 = lhs.tl_corner();
    auto r1 = lhs.br_corner();
    auto l2 = rhs.tl_corner();
    auto r2 = rhs.br_corner();

    // If one rectangle is on left side of other
    if (l1.x > r2.x || l2.x > r1.x)
        return false;

    // If one rectangle is above other
    if (l1.y > r2.y || l2.y > r1.y)
        return false;

    return true;
}

/// Returns true if a given rectangle contains a given point
template <typename T>
bool contains(rectangle<T> const& r, typename rectangle<T>::point_t const& p)
{
    auto p1 = p - r.topleft;
    auto p2 = r.dimensions;

    return (p1.x < p2.x && p1.y < p2.y);
}

/// Returns true if a given rectangle lhs contains a given rectangle rhs
template <typename T>
bool contains(rectangle<T> const& lhs, rectangle<T> const& rhs)
{
    auto l1 = lhs.tl_corner();
    auto r1 = lhs.br_corner();
    auto l2 = rhs.tl_corner();
    auto r2 = rhs.br_corner();

    return (l1.x <= l2.x && r1.x >= r2.x && l1.y <= l2.y && r1.y >= r2.y);
}

/// Returns true if a given point is within a given rectangle
template <typename T>
bool within(typename rectangle<T>::point_t const& p, rectangle<T> const& r)
{
    return contains(r, p);
}

/// Returns true if a given rectangle lhs is within a given rectangle rhs
template <typename T>
bool within(rectangle<T> const& lhs, rectangle<T> const& rhs)
{
    return contains(rhs, lhs);
}

/// Returns the intersection of the two given rectangles
///
/// \note Returns a degenerated rectangle if they do not overlap
template <typename T>
rectangle<T> intersection(const rectangle<T>& a, const rectangle<T>& b)
{
    using point_t = typename rectangle<T>::point_t;

    // Trivial cases first
    if (!overlaps(a, b))
        return {};
    else if (a == b)
        return a;

    //auto topleft = std::max(a.topleft, b.topleft);
    auto top_left = point_t{std::max(a.tl_corner().x, b.tl_corner().x), std::max(a.tl_corner().y, b.tl_corner().y)};
    auto bottom_right = point_t{std::min(a.br_corner().x, b.br_corner().x), std::min(a.br_corner().y, b.br_corner().y)};
    point_t dimensions = bottom_right - top_left + point_t{1, 1};
    return {top_left, dimensions};
}

// Common type to represent 2D dimensions or in-memory extend of image or view.
using rectangle_t = rectangle<std::ptrdiff_t>;

} //namespace lima
