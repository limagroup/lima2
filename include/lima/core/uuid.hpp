// Copyright (C) 2019 Alejandro Homs, Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

//#include <cstdint>
//#include <array>

//#define BOOST_UUID_USE_SSE41
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

namespace lima
{
/// Universally Unique Identifiers
//using uuid = std::array<std::uint8_t, 16>;

//TODO Remove Boost.UUID from interface
using uuid = boost::uuids::uuid;

} //namespace lima
