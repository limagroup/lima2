// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstdint>

#include <lima/core/point.hpp>
#include <lima/core/pixel.hpp>

namespace lima
{

struct frame_info
{
    using coord_t = std::ptrdiff_t;
    using point_t = point<coord_t>;

    frame_info() = default;
    frame_info(const point_t& dims, std::size_t nb_channels, const pixel_enum& pixel) :
        m_dimensions(dims), m_nb_channels(nb_channels), m_pixel_type(pixel)
    {
    }
    frame_info(coord_t width, coord_t height, std::size_t nb_channels, const pixel_enum& pixel) :
        m_dimensions(width, height), m_nb_channels(nb_channels), m_pixel_type(pixel)
    {
    }

    const point_t& dimensions() const { return m_dimensions; }
    coord_t width() const { return m_dimensions.x; }
    coord_t height() const { return m_dimensions.y; }
    std::size_t size() const { return m_dimensions.x * m_dimensions.y; }
    pixel_enum pixel_type() const { return m_pixel_type; }
    std::size_t nb_channels() const { return m_nb_channels; }

    point_t m_dimensions;                        //<! Frame dimensions
    std::size_t m_nb_channels = 1;               //<! Number of channels
    pixel_enum m_pixel_type = pixel_enum::gray8; //<! Pixel type
};

inline bool operator==(const frame_info& a, const frame_info& b)
{
    return (a.m_dimensions == b.m_dimensions)      // Same dimension
           && (a.m_nb_channels == b.m_nb_channels) // number of channels
           && (a.m_pixel_type == b.m_pixel_type);  // and pixel type
}

inline bool operator!=(const frame_info& a, const frame_info& b)
{
    return !(a == b);
}

} // namespace lima
