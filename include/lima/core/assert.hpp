// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <stdexcept>

namespace lima
{
inline void assert_throw(bool cond, const char* what)
{
    if (!cond)
        throw std::invalid_argument(what);
}

#define assert_return(cond) \
    if (!(cond))            \
        return false;

} //namespace lima
