// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstdint>
#include <cstring>

#include <iostream>
#include <algorithm>

#include <lima/exceptions.hpp>
#include <lima/utils/lengthof.hpp>

namespace lima
{
namespace log
{
    // We define our own channels aka application domains
    // The most significant byte is the domain (predefined bellow)
    // The less significant byte is the sub-domain and can be customized
    class channel_name
    {
        using value_type = std::uint16_t;

      public:
        /*explicit*/ channel_name(const char* name, value_type val) : m_name(name), m_val(val) {}
        /*explicit*/ operator value_type&() noexcept { return m_val; }
        /*explicit*/ operator const value_type&() const noexcept { return m_val; }

        // Equality is redefined here
        bool operator==(channel_name const& rhs) const noexcept
        {
            const std::uint8_t* v1 = reinterpret_cast<const std::uint8_t*>(&m_val);
            const std::uint8_t* v2 = reinterpret_cast<const std::uint8_t*>(&rhs.m_val);
            return (v1[0] & v2[0]) && (v1[1] & v2[1]);
        }

        bool operator==(const char* name) const noexcept { return strcmp(m_name, name) == 0; }

        /// Returns the name
        const char* name() const { return m_name; }

      private:
        value_type m_val;
        const char* m_name;
    };

    namespace app_domain
    {
        inline static const channel_name core = {"core", 0x01FF};
        inline static const channel_name ctl = {"ctl", 0x02FF};
        inline static const channel_name acq = {"acq", 0x04FF};
        inline static const channel_name proc = {"proc", 0x08FF};
        inline static const channel_name io = {"io", 0x10FF};
        inline static const channel_name det = {"det", 0x20FF};
        inline static const channel_name sdk = {"sdk", 0x40FF};
        inline static const channel_name all = {"all", 0xFFFF};

        inline channel_name from_string(std::string const& name)
        {
            static const channel_name channels[] = {core, ctl, acq, proc, io, det, sdk, all};

            const channel_name* end = channels + lengthof(channels);
            const channel_name* it =
                std::find_if(channels, end, [&name](channel_name const& ch) { return ch.name() == name; });

            if (it == end)
                LIMA_THROW_EXCEPTION(lima::runtime_error("Invalid channel name"));

            return *it;
        }

    } //namespace app_domain

    // The operator puts a human-friendly representation of the channel to the stream
    inline std::ostream& operator<<(std::ostream& os, channel_name ch)
    {
        os << ch.name();
        return os;
    }

} // namespace log
} //namespace lima
