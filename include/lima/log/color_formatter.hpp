// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/log/expressions.hpp>
#include <boost/log/expressions/formatters/named_scope.hpp>
#include <boost/log/support/date_time.hpp>

#include <lima/log/keywords.hpp>
#include <lima/log/severity_level.hpp>

namespace lima
{
namespace log
{

    inline void coloring_formatter(boost::log::record_view const& rec, boost::log::formatting_ostream& strm)
    {
        namespace expr = boost::log::expressions;

        auto severity = boost::log::extract<severity_level>("Severity", rec);
        if (severity) {
            // Set the color
            switch (severity.get()) {
            case severity_level::warning:
                strm << "\033[33m";
                break;
            case severity_level::error:
            case severity_level::fatal:
                strm << "\033[31m";
                break;
            default:
                break;
            }
        }

        strm << "[" << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S.%f")(rec) << "]"
             << "[" << rec[process_id] << "]"
             << "[" << rec[thread_id] << "]"
             << "[" << rec[file] << ":" << rec[line] << "]"
             << "[" << severity << "|" << rec[channel] << "]"
             << "[" << rec[named_scope] << "] " << rec[expr::smessage];

        if (severity) {
            // Restore the default color
            strm << "\033[0m";
        }
    }

} // namespace log
} //namespace lima
