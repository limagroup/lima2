// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

#include <boost/log/attributes/mutable_constant.hpp>

namespace lima
{
namespace log
{
    // Define the attribute keywords
    BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", severity_level)
    BOOST_LOG_ATTRIBUTE_KEYWORD(channel, "Channel", channel_name)
    BOOST_LOG_ATTRIBUTE_KEYWORD(file, "File", std::string)
    BOOST_LOG_ATTRIBUTE_KEYWORD(line, "Line", int)

    BOOST_LOG_ATTRIBUTE_KEYWORD(named_scope, "Scope", boost::log::attributes::named_scope::value_type)
    BOOST_LOG_ATTRIBUTE_KEYWORD(process_id, "ProcessID", boost::log::attributes::current_process_id::value_type)
    BOOST_LOG_ATTRIBUTE_KEYWORD(thread_id, "ThreadID", boost::log::attributes::current_thread_id::value_type)
} // namespace log
} // namespace lima