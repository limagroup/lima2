// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>
#include <string>

#include <boost/parameter/keyword.hpp>

// #include <boost/log/attributes/mutable_constant.hpp>
#include <boost/log/core.hpp>
// #include <boost/log/expressions.hpp>
// #include <boost/log/expressions/formatters/named_scope.hpp>
// #include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
// #include <boost/log/support/date_time.hpp>
// #include <boost/log/utility/manipulators/add_value.hpp>
// #include <boost/log/utility/setup/file.hpp>
// #include <boost/log/utility/setup/console.hpp>
// #include <boost/log/utility/setup/common_attributes.hpp>

#include <lima/log/severity_level.describe.hpp>
#include <lima/log/channel.hpp>
#include <lima/utils/lengthof.hpp>

namespace lima
{
namespace log
{
    /// Logger tag
    ///
    /// This tag can be used to acquire the logger that is used with logging macros.
    /// This may be useful when the logger is used with other macros which require a logger.
    struct logger
    {
        // Define our logger type
        using logger_type = boost::log::sources::severity_channel_logger_mt<severity_level, channel_name>;

        /// Returns a reference to the trivial logger instance
        static logger_type& get() { return boost::log::sources::aux::logger_singleton<logger>::get(); }

        // Implementation details - never use these
        enum registration_line_t
        {
            registration_line = __LINE__
        };
        static const char* registration_file() { return __FILE__; }
        static logger_type construct_logger()
        {
            return logger_type(boost::log::keywords::severity = severity_level::error,
                               boost::log::keywords::channel = app_domain::all);
        }
    };

    /// Init logger
    void init();

    /// Setting up console sink
    void add_console_log();

    /// Setting up file sink
    void add_file_log(std::filesystem::path const& log_file_path, std::filesystem::path const& log_file_prefix);

    void set_filter(severity_level sev, channel_name chan);

    inline constexpr size_t file_name_offset() { return lengthof(__FILE__) - lengthof("include/lima/log/logger.hpp"); }

} // namespace log
} //namespace lima
