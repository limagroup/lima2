// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace lima
{
namespace log
{
    // We define our own severity levels
    enum class severity_level : uint32_t
    {
        trace,
        debug,
        info,
        warning,
        error,
        fatal
    };

} // namespace log
} // namespace lima
