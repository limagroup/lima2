// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/mpi.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>

namespace lima
{
/// Each process needs to register with the topology object and declare its role
struct topology
{
    ///
    enum class role
    {
        master,
        control,
        daq,
        processing
    };

    topology(role r)
    {
        // Gather rank and role from all participating processes
        boost::mpi::all_gather(m_world, std::make_pair(m_world.rank(), r), m_roles);
    }

    boost::mpi::communicator m_world;

    std::vector<std::pair<int, role>> m_roles;
};

} //namespace lima
