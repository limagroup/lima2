// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/log/sources/record_ostream.hpp>         // For BOOST_LOG_STREAM_WITH_PARAMS
#include <boost/log/attributes/named_scope.hpp>         // For BOOST_LOG_NAMED_SCOPE
#include <boost/log/utility/manipulators/add_value.hpp> // For add_value

#include <lima/log/logger.hpp>

/// Some macros to hide Boost.Log usage from the rest of the code
#define LIMA_LOG(lvl, chan)                                                                             \
    BOOST_LOG_STREAM_WITH_PARAMS(::lima::log::logger::get(),                                            \
                                 (::boost::log::keywords::severity = ::lima::log::severity_level::lvl)( \
                                     ::boost::log::keywords::channel = ::lima::log::app_domain::chan))  \
        << ::boost::log::add_value("Line", __LINE__)                                                    \
        << ::boost::log::add_value("File", &__FILE__[::lima::log::file_name_offset()])

#define LIMA_LOG_SCOPE(scope) BOOST_LOG_NAMED_SCOPE(scope)
