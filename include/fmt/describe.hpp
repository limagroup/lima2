// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(FMT_DESCRIBE_HPP)
#define FMT_DESCRIBE_HPP

#include <type_traits>

#include <boost/describe.hpp>
#include <boost/mp11.hpp>

#include <fmt/format.h>

namespace fmt
{
// Generic formatter for Described structs
template <typename T, typename Char>
struct formatter<T, Char, typename std::enable_if_t<boost::describe::has_describe_members<T>::value>>
{
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(T const& t, FormatContext& ctx) const -> decltype(ctx.out())
    {
        auto out = ctx.out();
        out = fmt::format_to(out, "{{");
        boost::mp11::mp_for_each<
            boost::describe::describe_members<T, boost::describe::mod_any_access | boost::describe::mod_inherited>>(
            [&, first = true](auto D) mutable {
                if (!first)
                    out = fmt::format_to(out, ", ");
                first = false;
                out = fmt::format_to(out, ".{} = {}", D.name, t.*D.pointer);
            });
        out = fmt::format_to(out, "}}");
        return out;
    }
};

// Generic formatter for Described enumerations
template <typename E, typename Char>
struct formatter<E, Char, typename std::enable_if_t<boost::describe::has_describe_enumerators<E>::value>>
{
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(E const& e, FormatContext& ctx) const -> decltype(ctx.out())
    {
        auto out = ctx.out();
        char const* r = "(unnamed)";

        boost::mp11::mp_for_each<boost::describe::describe_enumerators<E>>([&](auto D) {
            if (e == D.value)
                r = D.name;
        });

        return fmt::format_to(out, "{}", r);
    }
};

} //namespace fmt

#endif //!defined(FMT_DESCRIBE_HPP)
