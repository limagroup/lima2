// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#if !defined(FMT_UUID_HPP)
#define FMT_UUID_HPP

#include <boost/uuid/uuid_io.hpp>

#include <fmt/format.h>

namespace fmt
{
// Formatter for std::filesystem::path
template <>
struct formatter<boost::uuids::uuid>
{
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(boost::uuids::uuid const& uuid, FormatContext& ctx) const -> decltype(ctx.out())
    {
        return fmt::format_to(ctx.out(), "{}", boost::uuids::to_string(uuid));
    }
};

} //namespace fmt

#endif //!defined(FMT_UUID_HPP)
