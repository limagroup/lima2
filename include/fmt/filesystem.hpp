// Formatting library for C++ - std support
//
// Copyright (c) 2012 - present, Victor Zverovich
// All rights reserved.
//
// For the license information refer to format.h.

#if !defined(FMT_FILESYSTEM_HPP)
#define FMT_FILESYSTEM_HPP

#include <filesystem>

#include <fmt/format.h>

namespace fmt
{
// Formatter for std::filesystem::path
template <>
struct formatter<std::filesystem::path>
{
    char presentation = 's';

    constexpr auto parse(format_parse_context& ctx)
    {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end
            && (*it == '/' || *it == '.' || *it == 'c' || *it == 'f' || *it == 's' || *it == 'n' || *it == 'p'
                || *it == 'e'))
            presentation = *it++;

        if (it != end && *it != '}')
            throw fmt::format_error("invalid format");

        return it;
    }

    template <typename FormatContext>
    auto format(const std::filesystem::path& p, FormatContext& ctx) const -> decltype(ctx.out())
    {
        switch (presentation) {
        case '/':
            return fmt::format_to(ctx.out(), "{}", p.has_root_path() ? p.root_path().string() : p.string());
        case '.':
            return fmt::format_to(ctx.out(), "{}", p.has_relative_path() ? p.relative_path().string() : p.string());
        case 's':
            return fmt::format_to(ctx.out(), "{}", p.string());
        case 'n':
            return fmt::format_to(ctx.out(), "{}", p.stem().string());
        case 'f':
            return fmt::format_to(ctx.out(), "{}", p.filename().string());
        case 'e':
            return fmt::format_to(ctx.out(), "{}", p.extension().string());
        case 'c':
            return fmt::format_to(ctx.out(), "{}", std::filesystem::weakly_canonical(p).string());
        default:
            return fmt::format_to(ctx.out(), "{}", p.string());
        }
    }
};

} //namespace fmt

#endif //!defined(FMT_FILESYSTEM_HPP)
