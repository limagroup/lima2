// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#if !defined(FMT_LIMA_HPP)
#define FMT_LIMA_HPP

#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>

#include <fmt/format.h>

namespace fmt
{
template <typename T>
struct formatter<lima::point<T>>
{
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(lima::point<T> const& p, FormatContext& ctx) const -> decltype(ctx.out())
    {
        return fmt::format_to(ctx.out(), "({},{})", p.x, p.y);
    }
};

template <typename T>
struct formatter<lima::rectangle<T>>
{
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(lima::rectangle<T> const& r, FormatContext& ctx) const -> decltype(ctx.out())
    {
        return fmt::format_to(ctx.out(), "({},{})({}x{})", r.topleft.x, r.topleft.y, r.dimensions.x, r.dimensions.y);
    }
};

} //namespace fmt

#endif //FMT_LIMA_HPP
