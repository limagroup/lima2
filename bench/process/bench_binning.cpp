// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <benchmark/benchmark.h>

#include <boost/gil/extension/io/tiff/old.hpp>

#include <lima/processing/serial/binning.hpp>

using namespace boost::gil;

static void new_binning(benchmark::State& state)
{
    gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    gray16_image_t output_image(input_image.dimensions() / 2, 0);

    auto n = num_channels<gray16_image_t>::type();

    for (auto _ : state)
        // The code to benchmark
        lima::processing::binning<2>(const_view(input_image), view(output_image));

    tiff_write_view("lena_grayscale_binned_new.tiff", const_view(output_image));
}
BENCHMARK(new_binning);

template <class T>
static T max_value(const T&)
{
    return T((1ULL << (8 * sizeof(T))) - 1);
}

template <class INPUT, class OUTPUT>
//static void _binning2x2(INPUT* __restrict aSrcData, OUTPUT* __restrict aDstData, int Factor)
static void _binning2x2(INPUT* aSrcData, OUTPUT* aDstData, int Factor)
{
    int dimensions[] = {512, 512};

    INPUT* aSrcFirstLinePt = aSrcData;
    INPUT* aSrcSecondLinePt = aSrcFirstLinePt + dimensions[0];
    OUTPUT* aDstPt = aDstData;
    OUTPUT MAX_VALUE = max_value(*aDstPt);
    for (int lineId = 0; lineId < dimensions[1]; lineId += 2) {
        for (int columnId = 0; columnId < dimensions[0];
             columnId += 2, ++aDstPt, aSrcFirstLinePt += 2, aSrcSecondLinePt += 2) {
            unsigned long long result =
                *aSrcFirstLinePt + *(aSrcFirstLinePt + 1) + *aSrcSecondLinePt + *(aSrcSecondLinePt + 1);
            if (result > MAX_VALUE)
                *aDstPt = MAX_VALUE;
            else
                *aDstPt = OUTPUT(result);
        }
        aSrcFirstLinePt = aSrcSecondLinePt;
        aSrcSecondLinePt = aSrcFirstLinePt + dimensions[0];
    }
    dimensions[0] >>= 1;
    dimensions[1] >>= 1;
    if (Factor > 2)
        _binning2x2<OUTPUT, OUTPUT>(aDstData, aDstData, Factor >> 1);
}

static void old_binning(benchmark::State& state)
{
    gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    gray16_image_t output_image(input_image.dimensions() / 2);

    auto input_size = input_image.dimensions();
    unsigned char* raw_input_image = new unsigned char[input_size.x * input_size.y];

    auto output_size = input_image.dimensions() / 2;
    unsigned short* raw_output_image = new unsigned short[output_size.x * output_size.y];

    std::copy(const_view(input_image).begin(), const_view(input_image).end(), raw_input_image);

    for (auto _ : state)
        // The code to benchmark
        _binning2x2<unsigned char, unsigned short>(raw_input_image, raw_output_image, 2);

    std::copy(raw_output_image, raw_output_image + (output_size.x * output_size.y), view(output_image).begin());

    tiff_write_view("lena_grayscale_binned_old.tiff", const_view(output_image));
}
BENCHMARK(old_binning);

BENCHMARK_MAIN();
