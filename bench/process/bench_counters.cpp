// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdint>

#include <benchmark/benchmark.h>

#include <lima/processing/serial/counters.hpp>

using namespace boost::gil;

static void counters(benchmark::State& state)
{
    gray16_image_t input_image(2048, 2048);
    boost::gil::generate_pixels(view(input_image), [i = std::uint16_t{0}]() mutable {
        i++;
        return gray16_pixel_t{i};
    });

    lima::processing::counters_result res;

    for (auto _ : state)
        // The code to benchmark
        res = lima::processing::counters(const_view(input_image));

    if ((res.m_min != 0) && (res.m_max != 65535) && (res.m_count != 2048 * 2048) && (res.m_avg != 32767.5f)
        && (res.m_std != 18918.61361860324f))
        state.SkipWithError("Invalid result!");
}
BENCHMARK(counters);

BENCHMARK_MAIN();
