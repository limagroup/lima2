// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdint>
#include <random>

#include <boost/gil.hpp>

#include <benchmark/benchmark.h>

using namespace boost::gil;

namespace boost
{
namespace gil
{
    BOOST_GIL_DEFINE_BASE_TYPEDEFS(64f, double, gray)

}
} // namespace boost

static void random_generator_int(benchmark::State& state)
{
    gray16_image_t input_image(2048, 2048);

    std::random_device rd;  // Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
    std::normal_distribution<> dis{1, 1};

    for (auto _ : state)
        // The code to benchmark
        boost::gil::generate_pixels(view(input_image), [&gen, &dis]() mutable { return dis(gen); });
}
BENCHMARK(random_generator_int);

static void random_generator_double(benchmark::State& state)
{
    gray64f_image_t input_image(2048, 2048);

    std::random_device rd;     // Will be used to obtain a seed for the random number engine
    std::mt19937_64 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
    std::normal_distribution<> dis{1, 1};

    for (auto _ : state)
        // The code to benchmark
        boost::gil::generate_pixels(view(input_image), [&gen, &dis]() mutable { return dis(gen); });
}
BENCHMARK(random_generator_double);

BENCHMARK_MAIN();
