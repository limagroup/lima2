// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdint>

#include <benchmark/benchmark.h>

#include <boost/gil.hpp>
#include <boost/gil/for_each2.hpp>

#include <lima/exceptions.hpp>

using namespace boost::gil;
using namespace lima;

template <typename SrcView>
void mask_branch(const SrcView& src, const gray8c_view_t& mask)
{
    // Preconditions
    if (src.dimensions() != mask.dimensions())
        LIMA_THROW_EXCEPTION(process_error("Source and mask dimensions mismatched"));

    transform_pixels(src, mask, src, [&](auto p1, auto p2) { return p2 == 1 ? 0 : p1; });
}

static void mask_branch(benchmark::State& state)
{
    gray8_image_t input_image(2048, 2048);
    generate_pixels(view(input_image), [i = std::uint16_t{0}]() mutable {
        i++;
        return gray8_pixel_t{i};
    });

    gray8_image_t mask_image(2048, 2048);
    generate_pixels(view(input_image), [i = std::uint8_t{0}]() mutable {
        i++;
        return gray8_pixel_t{i};
    });

    for (auto _ : state)
        // The code to benchmark
        mask_branch(view(input_image), const_view(mask_image));

    if (!all_off_pixel(const_view(input_image), const_view(mask_image),
                       [](auto p1, auto p2) { return p1 || ((p1 == 0) && (p2 == 0)); }))
        state.SkipWithError("Invalid result!");
}
BENCHMARK(mask_branch);

template <typename SrcView>
void mask_logical_and(const SrcView& src, const gray8c_view_t& mask)
{
    // Preconditions
    if (src.dimensions() != mask.dimensions())
        LIMA_THROW_EXCEPTION(process_error("Source and mask dimensions mismatched"));

    transform_pixels(src, mask, src, [&](auto p1, auto p2) { return p1 & p2; });
}

static void mask_logical_and(benchmark::State& state)
{
    gray8_image_t input_image(2048, 2048);
    boost::gil::generate_pixels(view(input_image), [i = std::uint8_t{0}]() mutable {
        i++;
        return gray8_pixel_t{i};
    });

    gray8_image_t mask_image(2048, 2048);
    boost::gil::generate_pixels(view(input_image), [i = std::uint8_t{0}]() mutable {
        i++;
        return gray8_pixel_t{i};
    });

    for (auto _ : state)
        // The code to benchmark
        mask_logical_and(view(input_image), const_view(mask_image));

    if (!all_off_pixel(const_view(input_image), const_view(mask_image),
                       [](auto p1, auto p2) { return p1 || ((p1 == 0) && (p2 == 0)); }))
        state.SkipWithError("Invalid result!");
}
BENCHMARK(mask_logical_and);

#if defined(__AVX__)
#include <immintrin.h>

template <typename SrcView>
void mask_avx(const SrcView& src, const gray8c_view_t& mask)
{
    // Preconditions
    if (src.dimensions() != mask.dimensions())
        LIMA_THROW_EXCEPTION(process_error("Source and mask dimensions mismatched"));

    unsigned char* src_ptr = &(at_c<0>(src[0]));
    unsigned char const* mask_ptr = &(at_c<0>(mask[0]));
    int size = src.size();

    if (((long) src_ptr & 31) || ((long) mask_ptr & 31))
        LIMA_THROW_EXCEPTION(process_error("Source and mask not 256 bit aligned"));

    __m256i cmp_to_0 = _mm256_set_epi32(0, 0, 0, 0, 0, 0, 0, 0);
    for (int i = size; i >= 32; i -= 32, src_ptr += 32, mask_ptr += 32) {
        __m256i mask_src = _mm256_stream_load_si256((__m256i*) mask_ptr);
        __m256i src_register = _mm256_stream_load_si256((__m256i*) src_ptr);
        __m256i mask_register = _mm256_cmpgt_epi8(mask_src, cmp_to_0);
        src_register = _mm256_and_si256(src_register, mask_register);
        _mm256_stream_si256((__m256i*) src_ptr, src_register);
    }
}

static void mask_avx(benchmark::State& state)
{
    gray8_image_t input_image(2048, 2048, 32);
    boost::gil::generate_pixels(view(input_image), [i = std::uint8_t{0}]() mutable {
        i++;
        return gray8_pixel_t{i};
    });

    gray8_image_t mask_image(2048, 2048, 32);
    boost::gil::generate_pixels(view(input_image), [i = std::uint8_t{0}]() mutable {
        i++;
        return gray8_pixel_t{i};
    });

    for (auto _ : state)
        // The code to benchmark
        mask_avx(view(input_image), const_view(mask_image));

    if (!all_off_pixel(const_view(input_image), const_view(mask_image),
                       [](auto p1, auto p2) { return p1 || ((p1 == 0) && (p2 == 0)); }))
        state.SkipWithError("Invalid result!");
}
BENCHMARK(mask_avx);

#endif // defined(__AVX__)

BENCHMARK_MAIN();
