// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdint>
#include <random>

#include <benchmark/benchmark.h>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/io.hpp>

using namespace boost::numeric::ublas;

template <typename T>
void generate(matrix<T>& m, double fill_factor)
{
    std::random_device rd;
    std::mt19937 gen(rd());

    // uniform_int_distribution: N4659 29.6.1.1 [rand.req.genl]/1e requires one of
    // short, int, long, long long, unsigned short, unsigned int, unsigned long, or unsigned long long
    std::uniform_int_distribution<std::uint16_t> dis(1, std::numeric_limits<T>::max());

    //For each pixel
    for (unsigned i = 0; i < m.size1(); ++i)
        for (unsigned j = 0; j < m.size2(); ++j) {
            // Get rand value
            auto val = dis(gen);

            // Discard pixel values acording to the fill factor
            if (val < std::numeric_limits<T>::max() * fill_factor)
                m(i, j) = val;
        }
}

static void convert_dense_to_mapped(benchmark::State& state)
{
    matrix<std::uint16_t> in(1024, 1024);
    mapped_matrix<std::uint16_t> out(1024, 1024);

    generate(in, 0.3);

    for (auto _ : state)
        // The code to benchmark
        out = in;
}
BENCHMARK(convert_dense_to_mapped);

static void convert_dense_to_compressed(benchmark::State& state)
{
    matrix<std::uint16_t> in(1024, 1024);
    compressed_matrix<std::uint16_t> out(1024, 1024);

    generate(in, 0.3);

    for (auto _ : state)
        // The code to benchmark
        out = in;
}
BENCHMARK(convert_dense_to_compressed);

static void convert_dense_to_coordinate(benchmark::State& state)
{
    matrix<std::uint16_t> in(1024, 1024);
    coordinate_matrix<std::uint16_t> out(1024, 1024);

    generate(in, 0.3);

    for (auto _ : state)
        // The code to benchmark
        out = in;
}
BENCHMARK(convert_dense_to_coordinate);

BENCHMARK_MAIN();
