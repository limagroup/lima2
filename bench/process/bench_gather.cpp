// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cassert>
#include <cstring>
#include <cstdint>

#include <algorithm>
#include <memory>

#include <boost/mpi.hpp>

#include <benchmark/benchmark.h>

#include <lima/core/layout.hpp>
#include <lima/core/serialization/layout.hpp>
#include <lima/utils/lengthof.hpp>

namespace mpi = boost::mpi;

template <typename T>
struct matrix2
{
    using value_type = T;
    using reference = T&;

    matrix2(int const dim[2]) : data_(std::make_unique<T[]>(dim[0] * dim[1]))
    {
        dim_[0] = dim[0];
        dim_[1] = dim[1];
    }

    reference operator()(int i, int j) const
    {
        assert((i < width()) && (j < height()));
        return data_[i + j * width()];
    }

    int size() const { return width() * height(); }
    operator T*() { return data_.get(); }

    int width() const { return dim_[0]; }
    int height() const { return dim_[1]; }

    void print()
    {
        for (int j = 0; j < dim_[1]; j++) {
            for (int i = 0; i < dim_[0]; i++) {
                std::cout << operator()(i, j) << ' ';
            }
            std::cout << std::endl;
        }
    }

    void fill(value_type v) { std::fill(data_.get(), data_.get() + size(), v); }

    int dim_[2];
    std::unique_ptr<T[]> data_ = nullptr;
};

static void gather(benchmark::State& state)
{
    using value_type = std::uint16_t; // Let's assume 16 bit grayscale pixel
    auto mpi_datatype = MPI_UNSIGNED_SHORT;

    mpi::communicator world;
    double max_elapsed_second;

    int res;
    const int global_dim[] = {1024, 1024};
    const int local_dim[] = {256, 256};
    const int starts[] = {0, 0};

    const int grid_size[] = {global_dim[0] / local_dim[0], global_dim[1] / local_dim[1]};
    const int nb_receivers = grid_size[0] * grid_size[1];

    assert(grid_size[0] == 4);
    assert(grid_size[1] == 4);

    world.barrier();

    if (world.size() != nb_receivers) {
        fprintf(stderr, "Only works with np=%d for now\n", nb_receivers);
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    // Define an MPI type for the partial images
    MPI_Datatype partial_image_type1, partial_image_type2;
    res = MPI_Type_create_subarray(2, global_dim, local_dim, starts, MPI_ORDER_C, mpi_datatype, &partial_image_type1);
    res = MPI_Type_create_resized(partial_image_type1, 0, local_dim[0] * sizeof(value_type),
                                  &partial_image_type2); //Resized image as a multiple of row width
    res = MPI_Type_commit(&partial_image_type2);

    matrix2<value_type> local(local_dim);
    local.fill(world.rank());

    while (state.KeepRunning()) {
        // Do the work and time it on each proc
        auto start = std::chrono::high_resolution_clock::now();

        if (world.rank() == 0) {
            lima::layout l = {};
            mpi::broadcast(world, l, 0);

            matrix2<value_type> global(global_dim);
            memset(&global(0, 0), world.rank(), global.size());

            // How many pieces of data everyone has, in units of blocks
            matrix2<int> recvcounts(grid_size);
            // The starting point of everyone's data
            matrix2<int> displs(grid_size);

            int disp = 0;
            for (int j = 0; j < grid_size[1]; j++) {
                for (int i = 0; i < grid_size[0]; i++) {
                    recvcounts(i, j) = 1;
                    displs(i, j) = disp;
                    disp += 1;
                }
                disp += ((global.width() / grid_size[1]) - 1) * grid_size[1];
            }

            MPI_Gatherv(local, local.size(), mpi_datatype, global, recvcounts, displs, partial_image_type2, 0, world);

        } else {
            // Get layout
            lima::layout l;
            mpi::broadcast(world, l, 0);

            MPI_Gatherv(local, local.size(), mpi_datatype, NULL, 0, 0, 0, 0, world);
        }

        auto end = std::chrono::high_resolution_clock::now();
        // Now get the max time across all procs:
        // for better or for worse, the slowest processor is the one that is
        // holding back the others in the benchmark.
        auto const duration = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
        auto elapsed_seconds = duration.count();
        MPI_Allreduce(&elapsed_seconds, &max_elapsed_second, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        state.SetIterationTime(max_elapsed_second);
    }

    MPI_Type_free(&partial_image_type2);

    world.barrier();
}
BENCHMARK(gather)->UseManualTime();

// This reporter does nothing.
// We can use it to disable output from all but the root process
class NullReporter : public ::benchmark::BenchmarkReporter
{
  public:
    NullReporter() {}
    virtual bool ReportContext(const Context&) { return true; }
    virtual void ReportRuns(const std::vector<Run>&) {}
    virtual void Finalize() {}
};

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    ::benchmark::Initialize(&argc, argv);

    if (rank == 0) {
        ::benchmark::RunSpecifiedBenchmarks();
    } else {
        // reporting from other processes is disabled by passing a custom reporter
        NullReporter null;
        ::benchmark::RunSpecifiedBenchmarks(&null);
    }

    MPI_Finalize();
    return 0;
}