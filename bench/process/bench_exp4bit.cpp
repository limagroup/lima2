// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <benchmark/benchmark.h>

#include <boost/align.hpp>
#include <boost/gil/image.hpp>
#include <boost/gil/typedefs.hpp>

#include <lima/processing/serial/exp4bit.hpp>

using namespace boost::gil;

#if defined(__AVX2__)
typedef std::vector<unsigned char, boost::alignment::aligned_allocator<unsigned char, sizeof(__m256i)>> buffer_t;
#elif defined(__SSE2__)
typedef std::vector<unsigned char, boost::alignment::aligned_allocator<unsigned char, sizeof(__m128i)>> buffer_t;
#else
typedef std::vector<unsigned char> buffer_t;
#endif

const unsigned int chip_size = 256;

static void pack_pixel_depth_8bit_to_4bit(const buffer_t& src, buffer_t& dest)
{
    for (int i = 0; i < chip_size * chip_size; i += 2) {
        dest[i / 2] = (src[i] << 4) | src[i + 1];
    }
}

static void unpack_pixel_depth_4bit_to_8bit(const buffer_t& src, buffer_t& dest)
{
    for (int i = 0; i < chip_size * chip_size / 2; i++) {
        dest[i * 2] = src[i] >> 4;
        dest[i * 2 + 1] = src[i] & 0x0F;
    }
}

static void exp4bit(benchmark::State& state)
{
    buffer_t frame_8bit_in(chip_size * chip_size);
    buffer_t frame_8bit_out(chip_size * chip_size);
    buffer_t frame_4bit(chip_size * chip_size / 2);

    // Generate a frame
    for (unsigned int i = 0; i < chip_size * chip_size; i++) {
        //frame_8bit_in[i] = i % 0x10;
        frame_8bit_in[i] = std::rand() % 0x10;
    }

    // Pack the frame to 4 bit
    pack_pixel_depth_8bit_to_4bit(frame_8bit_in, frame_4bit);

    // Test reference method
    std::fill(frame_8bit_out.begin(), frame_8bit_out.end(), 0);

    for (auto _ : state)
        // The code to benchmark
        unpack_pixel_depth_4bit_to_8bit(frame_4bit, frame_8bit_out);
}
BENCHMARK(exp4bit);

#if defined(__AVX2__)
static void exp4bit_avx2(benchmark::State& state)
{
    buffer_t frame_8bit_in(chip_size * chip_size);
    buffer_t frame_8bit_out(chip_size * chip_size);
    buffer_t frame_4bit(chip_size * chip_size / 2);

    // Generate a frame
    for (unsigned int i = 0; i < chip_size * chip_size; i++) {
        //frame_8bit_in[i] = i % 0x10;
        frame_8bit_in[i] = std::rand() % 0x10;
    }

    // Define an image type with an aligned memory allocator
    typedef image<gray8_pixel_t, false, boost::alignment::aligned_allocator<unsigned char, sizeof(__m256i)>>
        gray8_image_t;
    gray8_image_t output_image(point_t{chip_size, chip_size});

    for (auto _ : state)
        // The code to benchmark
        lima::processing::exp4bit_avx2(&frame_4bit[0], view(output_image));
}
BENCHMARK(exp4bit_avx2);
#endif //defined(__AVX2__)

#if defined(__SSE2__)
static void exp4bit_sse2(benchmark::State& state)
{
    buffer_t frame_8bit_in(chip_size * chip_size);
    buffer_t frame_8bit_out(chip_size * chip_size);
    buffer_t frame_4bit(chip_size * chip_size / 2);

    // Generate a frame
    for (unsigned int i = 0; i < chip_size * chip_size; i++) {
        //frame_8bit_in[i] = i % 0x10;
        frame_8bit_in[i] = std::rand() % 0x10;
    }

    // Define an image type with an aligned memory allocator
    typedef image<gray8_pixel_t, false, boost::alignment::aligned_allocator<unsigned char, sizeof(__m128i)>>
        gray8_image_t;
    gray8_image_t output_image(point_t{chip_size, chip_size});

    for (auto _ : state)
        // The code to benchmark
        lima::processing::exp4bit_sse2(&frame_4bit[0], view(output_image));
}
BENCHMARK(exp4bit_sse2);
#endif //defined(__SSE2__)

BENCHMARK_MAIN();
