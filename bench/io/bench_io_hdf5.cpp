// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdint>

#include <memory>
#include <vector>

#include <benchmark/benchmark.h>

#include <boost/gil/extension/io/tiff/old.hpp>

#define __TBB_NO_IMPLICIT_LINKAGE 1
#include <tbb/flow_graph.h>
#include <tbb/task_arena.h>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/io/hdf5.hpp>

using namespace boost::gil;

// Benchmark the compression of a 512x512 image (just to get an idea of the
// CPU time)
static void graph_io_bshuf_lz4(benchmark::State& state)
{
    using namespace tbb;

    gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    using buffer_t = std::vector<std::uint8_t>;
    using buffer_ptr_t = std::shared_ptr<buffer_t>;

    buffer_ptr_t res;

    auto in = const_view(input_image);

    for (auto _ : state) {
        unsigned int block_size =
            0; //Process in blocks of this many elements. Pass 0 to select automatically (recommended).
        size_t bound_size = bshuf_compress_lz4_bound(in.size(), sizeof(gray8_pixel_t), block_size);

        res = std::make_shared<buffer_t>(bound_size);

        size_t comp_size = bound_size;
        std::uint8_t* comp_data = res->data();

        size_t data_size = in.size();

        // Compress
        bshuf_write_uint64_BE(comp_data, data_size);
        bshuf_write_uint32_BE(comp_data + 8, block_size);

        size_t elem_size = sizeof(typename lima::bpp8_view_t::value_type);
        comp_size =
            bshuf_compress_lz4((const void*) &in(0, 0), (void*) (comp_data + 12), data_size, elem_size, block_size);

        res->resize(comp_size + 12);
    }
}
BENCHMARK(graph_io_bshuf_lz4);

using frame_t = lima::frame;

struct chunk
{
    chunk(int id_arg, size_t size_arg) : id(id_arg), data(size_arg) {}

    std::vector<std::uint8_t> data;
    int id;
};

static auto input_node(tbb::flow::graph& g, int nb_frames)
{
    gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);
    auto dim = input_image.dimensions();

    lima::frame input_frame(dim.x, dim.y, lima::pixel_enum::gray8);
    copy_pixels(const_view(input_image), lima::view(input_frame));

    return tbb::flow::input_node<frame_t>(g, [input_frame, nb_frames, idx = 0](tbb::flow_control& fc) mutable {
        auto res = input_frame;
        res.metadata.idx = idx;
        idx++;

        if (idx >= nb_frames)
            fc.stop();

        return res;
    });
}

using dimensions_t = lima::point<std::ptrdiff_t>;

static void graph_io_hdf5_raw(benchmark::State& state)
{
    using namespace tbb;

    flow::graph g;
    const int nb_frames = 10000;
    auto src = input_node(g, nb_frames);

    lima::io::h5::saving_params params;
    params.nb_frames_per_file = nb_frames;
    lima::io::h5::writer writer("test_writer_raw.h5", params, {dimensions_t{512, 512}, 1, lima::pixel_enum::gray8});

    flow::function_node<frame_t> write_view(
        g, flow::serial, [&writer](frame_t in) { writer.write_view(lima::view(in), in.metadata.idx); });

    flow::make_edge(src, write_view);

    for (auto _ : state) {
        // The code to benchmark
        src.activate();
        g.wait_for_all();
    }
}
BENCHMARK(graph_io_hdf5_raw);

static void graph_io_hdf5_chunk_raw(benchmark::State& state)
{
    using namespace tbb;

    flow::graph g;
    const int nb_frames = 10000;
    auto src = input_node(g, nb_frames);

    const int nb_frames_per_chunk = 10;

    using chunk_ptr_t = std::shared_ptr<chunk>;

    chunk_ptr_t chunk_ptr;
    flow::multifunction_node<frame_t, std::tuple<chunk_ptr_t>> aggr(
        g, flow::serial, [&nb_frames_per_chunk, chunk_ptr, frame_counter = 0](frame_t const& in, auto& ports) mutable {
            // If new chunk required
            if (frame_counter == 0)
                // TODO: preallocate chunks in a pool
                chunk_ptr = std::make_shared<chunk>(in.metadata.idx, in.size() * nb_frames_per_chunk);

            // Append data to the chunk
            boost::variant2::visit(
                [chunk_ptr, frame_counter, &in](auto const& view) {
                    // TODO: Use gil::copy_pixels()
                    memcpy(chunk_ptr->data.data() + view.size() * frame_counter, &view(0, 0), view.size());
                },
                lima::const_view(in));

            frame_counter++;

            // If chunk is full
            if (frame_counter == nb_frames_per_chunk) {
                // Reset counter for next chunk
                frame_counter = 0;

                std::get<0>(ports).try_put(chunk_ptr);
            }
        });

    lima::io::h5::saving_params params;
    params.nb_frames_per_file = nb_frames;
    params.nb_frames_per_chunk = nb_frames_per_chunk;
    lima::io::h5::writer writer("test_writer_raw.h5", params, {dimensions_t{512, 512}, 1, lima::pixel_enum::gray8});

    flow::function_node<chunk_ptr_t> write_chunk(
        g, flow::serial, [&writer](chunk_ptr_t in) { writer.write_chunk(in->data.data(), in->data.size(), in->id); });

    flow::make_edge(src, aggr);
    flow::make_edge(flow::output_port<0>(aggr), write_chunk);

    for (auto _ : state) {
        // The code to benchmark
        src.activate();
        g.wait_for_all();
    }
}
BENCHMARK(graph_io_hdf5_chunk_raw);

static void graph_io_hdf5_comp(benchmark::State& state)
{
    using namespace tbb;

    flow::graph g;
    const int nb_frames = 10000;
    auto src = input_node(g, nb_frames);

    using chunk_ptr_t = std::shared_ptr<chunk>;

    flow::function_node<frame_t, chunk_ptr_t> comp(g, flow::unlimited, [](frame_t const& in) {
        //Allocate a new buffer
        unsigned int block_size =
            0; //Process in blocks of this many elements. Pass 0 to select automatically(recommended).
        size_t bound_size = bshuf_compress_lz4_bound(in.size(), sizeof(gray8_pixel_t), block_size);

        auto res = std::make_shared<chunk>(in.metadata.idx, bound_size);

        std::uint8_t* comp_data = res->data.data();

        // Compress
        bshuf_write_uint64_BE(comp_data, in.size());
        bshuf_write_uint32_BE(comp_data + 8, block_size);

        size_t elem_size = sizeof(typename lima::bpp8_view_t::value_type);
        size_t comp_size = boost::variant2::visit(
            [block_size, comp_data, elem_size](auto const& view) {
                return bshuf_compress_lz4((const void*) &view(0, 0), (void*) (comp_data + 12), view.size(), elem_size,
                                          block_size);
            },
            lima::const_view(in));

        res->data.resize(comp_size + 12);

        return res;
    });

    lima::io::h5::saving_params params;
    params.nb_frames_per_file = nb_frames;
    params.compression = lima::io::h5::compression_enum::bshuf_lz4;
    lima::io::h5::writer writer("test_writer_raw.h5", params, {dimensions_t{512, 512}, 1, lima::pixel_enum::gray8});

    flow::function_node<chunk_ptr_t> write_chunk(
        g, flow::serial, [&writer](chunk_ptr_t in) { writer.write_chunk(in->data.data(), in->data.size(), in->id); });

    flow::make_edge(src, comp);
    flow::make_edge(comp, write_chunk);

    for (auto _ : state) {
        // The code to benchmark
        src.activate();
        g.wait_for_all();
    }
}
BENCHMARK(graph_io_hdf5_comp);

static void graph_io_hdf5_chunk_comp(benchmark::State& state)
{
    using namespace tbb;

    flow::graph g;
    const int nb_frames = 10000;
    auto src = input_node(g, nb_frames);

    //flow::sequencer_node<frame_t> ordering(g, [](frame_t const& in) { return in.id; });

    const int nb_frames_per_chunk = 10;

    using chunk_ptr_t = std::shared_ptr<chunk>;

    chunk_ptr_t chunk_ptr;
    flow::multifunction_node<frame_t, std::tuple<chunk_ptr_t>> aggr(
        g, flow::serial, [&nb_frames_per_chunk, chunk_ptr, frame_counter = 0](frame_t const& in, auto& ports) mutable {
            // If new chunk required
            if (frame_counter == 0)
                // TODO: preallocate chunks in a pool
                chunk_ptr = std::make_shared<chunk>(in.metadata.idx, in.size() * nb_frames_per_chunk);

            // Append data to the chunk
            boost::variant2::visit(
                [chunk_ptr, frame_counter, &in](auto const& view) {
                    // TODO: Use gil::copy_pixels()
                    memcpy(chunk_ptr->data.data() + view.size() * frame_counter, &view(0, 0), view.size());
                },
                lima::const_view(in));

            frame_counter++;

            // If chunk is full
            if (frame_counter == nb_frames_per_chunk) {
                // Reset counter for next chunk
                frame_counter = 0;

                std::get<0>(ports).try_put(chunk_ptr);
            }
        });

    flow::function_node<chunk_ptr_t, chunk_ptr_t> comp(
        g, flow::unlimited, [&nb_frames_per_chunk](chunk_ptr_t const& in) mutable {
            //Allocate a new buffer
            unsigned int block_size =
                0; //Process in blocks of this many elements. Pass 0 to select automatically(recommended).
            size_t bound_size = bshuf_compress_lz4_bound(in->data.size(), sizeof(gray8_pixel_t), block_size);

            auto res = std::make_shared<chunk>(in->id, bound_size);

            std::uint8_t* comp_data = res->data.data();

            // Compress
            bshuf_write_uint64_BE(comp_data, in->data.size());
            bshuf_write_uint32_BE(comp_data + 8, block_size);

            size_t elem_size = sizeof(typename lima::bpp8_view_t::value_type);
            size_t comp_size = bshuf_compress_lz4((const void*) in->data.data(), (void*) (comp_data + 12),
                                                  in->data.size(), elem_size, block_size);

            res->data.resize(comp_size + 12);

            return res;
        });

    lima::io::h5::saving_params params;
    params.nb_frames_per_file = nb_frames;
    params.nb_frames_per_chunk = nb_frames_per_chunk;
    params.compression = lima::io::h5::compression_enum::bshuf_lz4;
    lima::io::h5::writer writer("test_writer_raw.h5", params, {dimensions_t{512, 512}, 1, lima::pixel_enum::gray8});

    flow::function_node<chunk_ptr_t> write_chunk(g, flow::serial, [&writer, &nb_frames_per_chunk](chunk_ptr_t in) {
        writer.write_chunk(in->data.data(), in->data.size(), in->id);
    });

    flow::make_edge(src, aggr);
    flow::make_edge(flow::output_port<0>(aggr), comp);
    flow::make_edge(comp, write_chunk);

    for (auto _ : state) {
        // The code to benchmark
        src.activate();
        g.wait_for_all();
    }
}
BENCHMARK(graph_io_hdf5_chunk_comp);

BENCHMARK_MAIN();
