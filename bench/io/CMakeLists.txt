# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_executable(bench_io_hdf5
  bench_io_hdf5.cpp
)

target_link_libraries(bench_io_hdf5
    lima_core
    benchmark::benchmark
)
