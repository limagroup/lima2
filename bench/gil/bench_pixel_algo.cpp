// Copyright (C) 2018-2019 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <algorithm>
#include <iostream>
#include <vector>

#include <benchmark/benchmark.h>

#include <boost/gil.hpp>
#include <boost/gil/pixel_numeric_operations.hpp>

using namespace boost::gil;

static void std_algo(benchmark::State& state)
{
    std::vector<int16_t> img(1024 * 1024, 0);

    for (auto _ : state)
        // The code to benchmark
        for (auto&& p : img)
            p += 1;

    if (!std::all_of(img.begin(), img.end(), [&state](auto p) { return p == state.iterations(); }))
        state.SkipWithError("std_algo wrong result");
}
BENCHMARK(std_algo);

static void gil_algo(benchmark::State& state)
{
    using namespace std::placeholders;
    namespace gil = boost::gil;

    gil::gray16_image_t img(1024, 1024, gray16_pixel_t{0});

    //Pixel operation
    auto op = std::bind(gil::pixel_plus_t<gray16_pixel_t, gray16_pixel_t, gray16_pixel_t>(), _1, gray16_pixel_t{1});

    for (auto _ : state)
        // The code to benchmark
        gil::transform_pixels(gil::const_view(img), gil::view(img), op);

    if (!std::all_of(gil::view(img).begin(), gil::view(img).end(),
                     [&state](auto p) { return p == state.iterations(); }))
        state.SkipWithError("gil_algo wrong result");
}
BENCHMARK(gil_algo);

BENCHMARK_MAIN();
