# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_executable(bench_pixel_algo
    bench_pixel_algo.cpp
)

target_link_libraries(bench_pixel_algo
    benchmark::benchmark
    Boost::boost
)
