// Copyright (C) 2020 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/image.hpp>

#include "safe_container.hpp"

namespace gil = boost::gil;

template <typename Image, template <typename T, typename C> class Stack, class Capacity = void>
class image_pool
{
  public:
    using image_t = Image;
    using view_t = typename image_t::view_t;
    using image_ptr = std::shared_ptr<image_t>;
    using stack_t = Stack<image_ptr, Capacity>;

    class frame_view : public view_t
    {
      public:
        frame_view(image_ptr&& im) : view_t(im->_view), m_image(std::move(im)) {}
        virtual ~frame_view() {}

      private:
        friend class image_pool;
        image_ptr m_image;
    };

    using frame_view_ptr = std::shared_ptr<frame_view>;

    image_pool(std::size_t nb_images) { m_stack.reserve(nb_images); }

    template <typename... Args>
    void add_image(Args&&... args)
    {
        m_stack.push(std::forward<Args>(args)...);
    }

    bool get_frame_view(frame_view_ptr& frame)
    {
        image_ptr image;
        if (!m_stack.pop(image))
            return false;

        frame = std::move(std::make_shared<managed_view>(*this, std::move(image)));
        return true;
    }

    bool empty() { return m_stack.empty(); }

  private:
    class managed_view : public frame_view
    {
      public:
        managed_view(image_pool& p, image_ptr&& im) : frame_view(std::move(im)), m_pool(p) {}
        ~managed_view() { m_pool.release(*this); }

      private:
        friend class image_pool;
        image_pool& m_pool;
    };

    void release(frame_view& f) { m_stack.push(std::move(f.m_image)); }

    stack_t m_stack;
};

namespace safe_container
{
template <typename T, class Capacity>
using image_stack = stack<T>;

template <typename Image, typename Capacity = void>
using image_pool = image_pool<Image, image_stack, Capacity>;
} // namespace safe_container

namespace lock_free
{
template <typename Image, class Capacity>
using image_pool = image_pool<Image, stack, Capacity>;
} // namespace lock_free
