// Copyright (C) 2020 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/lockfree/stack.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <stack>
#include <queue>

#include <mutex>
#include <condition_variable>

namespace safe_container
{
template <typename T, template <typename P> class Container>
constexpr bool is_stack = std::is_same_v<Container<T>, std::stack<T>>;

template <typename T, template <typename P> class Container, std::enable_if_t<is_stack<T, Container>, int> = 0>
T& next(Container<T>& c)
{
    return c.top();
}

template <typename T, template <typename P> class Container, std::enable_if_t<!is_stack<T, Container>, int> = 0>
T& next(Container<T>& c)
{
    return c.front();
}

template <typename T, template <typename P> class Container>
class container
{
  public:
    using container_t = Container<T>;

    void reserve(std::size_t n) {}

    template <class... Args>
    void push(Args&&... args)
    {
        std::lock_guard<std::mutex> l(m_mutex);
        m_container.emplace(std::forward<Args>(args)...);
    }

    bool pop(T& v)
    {
        std::lock_guard<std::mutex> l(m_mutex);
        if (m_container.empty())
            return false;

        v = std::move(next(m_container));
        m_container.pop();
        return true;
    }

    bool empty()
    {
        std::lock_guard<std::mutex> l(m_mutex);
        return m_container.empty();
    }

  private:
    std::mutex m_mutex;
    container_t m_container;
};

template <typename T>
using queue = container<T, std::queue>;
template <typename T>
using stack = container<T, std::stack>;

} // namespace safe_container

namespace lock_free
{
namespace lockfree = boost::lockfree;

template <size_t N>
using capacity = lockfree::capacity<N>;

template <typename T, template <typename P, class C> class Container, class Capacity>
constexpr bool is_stack = std::is_same_v<Container<T, Capacity>, lockfree::stack<T, Capacity>>;

template <typename T, template <typename P, class C> class Container, class Capacity,
          std::enable_if_t<is_stack<T, Container, Capacity>, int> = 0>
bool do_push(Container<T, Capacity>& c, const T& v)
{
    return c.bounded_push(v);
}

template <typename T, template <typename P, class C> class Container, class Capacity,
          std::enable_if_t<!is_stack<T, Container, Capacity>, int> = 0>
bool do_push(Container<T, Capacity>& c, const T& v)
{
    return c.push(v);
}

template <typename T, template <typename P, class C> class Container, class Capacity>
class container
{
  public:
    using container_t = Container<T, Capacity>;

    void reserve(std::size_t n) {}

    void push(const T& v)
    {
        if (!do_push(m_container, v))
            throw std::runtime_error("Container full");
    }

    bool pop(T& v) { return m_container.pop(v); }

    bool empty() { return m_container.empty(); }

  private:
    container_t m_container;
};

template <typename T, class C>
using queue = container<T, lockfree::spsc_queue, C>;

template <typename T, class C>
using stack = container<T, lockfree::stack, C>;

} // namespace lock_free
