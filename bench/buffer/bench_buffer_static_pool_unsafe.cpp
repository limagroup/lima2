// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/pool/static_pool_unsafe.hpp>

#include "bench_buffer_pool.hpp"

#ifdef BOOST_HAS_UNISTD_H
using allocator = boost::pinned_user_allocator_malloc_free;
#else
using allocator = boost::default_user_allocator_malloc_free;
#endif

using unsafe_pool_t = boost::unsafe::static_pool<allocator>;

static void bench_alloc_pool_unsafe_256(benchmark::State& state)
{
    bench_alloc_pool<unsafe_pool_t, buffer_size_256_sq>(state);
}
BENCHMARK(bench_alloc_pool_unsafe_256);

static void bench_alloc_pool_unsafe_2048(benchmark::State& state)
{
    bench_alloc_pool<unsafe_pool_t, buffer_size_2048_sq>(state);
}
BENCHMARK(bench_alloc_pool_unsafe_2048);

static void bench_alloc_pool_ordered_unsafe_256(benchmark::State& state)
{
    bench_alloc_pool_ordered<unsafe_pool_t, buffer_size_256_sq>(state);
}
BENCHMARK(bench_alloc_pool_ordered_unsafe_256);

static void bench_alloc_pool_ordered_unsafe_2048(benchmark::State& state)
{
    bench_alloc_pool_ordered<unsafe_pool_t, buffer_size_2048_sq>(state);
}
BENCHMARK(bench_alloc_pool_ordered_unsafe_2048);

static void bench_alloc_pool_memset_unsafe_256(benchmark::State& state)
{
    bench_alloc_pool_memset<unsafe_pool_t, buffer_size_256_sq>(state);
}
BENCHMARK(bench_alloc_pool_memset_unsafe_256);

static void bench_alloc_pool_memset_unsafe_2048(benchmark::State& state)
{
    bench_alloc_pool_memset<unsafe_pool_t, buffer_size_2048_sq>(state);
}
BENCHMARK(bench_alloc_pool_memset_unsafe_2048);
