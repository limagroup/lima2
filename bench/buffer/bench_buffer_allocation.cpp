// Copyright (C) 2019 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

/*
  Note on usage: each of these benchmarks will benefit from the "hot" memory
  assignments by the OS during the previous benchmark(s). If "cold" benchmarks
  are desired, they can by executed with the following bash command:

  (export PATH="$(pwd)/build/bench/buffer:${PATH}"   # replace accordingly
   exe="bench_buffer_allocation"
   skip_extra_info=0
   for t in $(${exe} --benchmark_list_tests); do
     if [ ${skip_extra_info} -eq 0 ]; then
       pipe_cmd="cat"
       skip_extra_info=1
     else
       pipe_cmd="grep ${t}"
     fi
     ${exe} --benchmark_filter="${t}\$" 2>&1 | ${pipe_cmd}
   done)
 */

#include <benchmark/benchmark.h>

#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <memory>
#include <ratio>
#include <sstream>

#include <unistd.h>   // sysconf
#include <malloc.h>   // mallopt
#include <sys/mman.h> // mlock

class bad_alloc : public std::bad_alloc
{
  public:
    bad_alloc(std::string m) : msg(std::string("bad_alloc: ") + m) {}

    virtual const char* what() const noexcept override { return msg.c_str(); }

  private:
    std::string msg;
};

#define BENCHMARK_US(x) BENCHMARK(x)->Unit(benchmark::kMicrosecond)

const std::size_t two_mega = 2 * 1024 * 1024;

void init_one_buffer(void* p, std::size_t len)
{
    typedef unsigned long ulong;
    auto to_ulongs = [](auto x) { return x / sizeof(ulong); };

    if (!std::align(alignof(ulong), sizeof(ulong), p, len))
        throw std::invalid_argument("init_one_buffer: len too small");

    // write at the start of the buffer to force its first page
    ulong* up = reinterpret_cast<ulong*>(p);
    *up = 0;

    // write at the begining of all the other buffer pages
    static const std::size_t page_size = sysconf(_SC_PAGESIZE);
    static const std::size_t page_ulongs = to_ulongs(page_size);

    ulong* end = up + to_ulongs(len);
    up -= to_ulongs(reinterpret_cast<ulong>(up) % page_size);
    for (up += page_ulongs; up < end; up += page_ulongs)
        *up = 0;
}

inline void write_one_buffer(void* p, std::size_t len)
{
    std::memset(p, 0x5a, len);
}

static void* alloc_one_buffer_malloc(std::size_t len)
{
    void* p = std::malloc(len);
    if (!p) {
        std::ostringstream error;
        error << "Error in malloc(" << len << "): " << std::strerror(errno);
        throw bad_alloc(error.str());
    }
    return p;
}

static void mlock_one_buffer(void* p, std::size_t len)
{
    int ret = mlock(p, len);
    if (ret != 0) {
        std::ostringstream error;
        error << "Error in mlock(" << len << "): " << std::strerror(errno) << ". ";
        error << "Please check your /etc/security/limits[.d] memlock quotas (ulimit -l) or CAP_IPC_LOCK";
        throw bad_alloc(error.str());
    }
}

struct malloced_buffer_list
{
    using buffer_data = std::tuple<void*, size_t, bool>;
    std::vector<buffer_data> buffer_list;

    // Disable the free-space-on-top-of-heap trim threshold:
    // free will always give back unused memory to OS (through malloc_trim)
    malloced_buffer_list() { mallopt(M_TRIM_THRESHOLD, -1); }

    ~malloced_buffer_list()
    {
        for (auto d : buffer_list) {
            void* p = std::get<0>(d);
            std::size_t len = std::get<1>(d);
            bool locked = std::get<2>(d);
            if (locked)
                munlock(p, len);
            std::free(p);
        }
    }

    void add_buffer(std::size_t len) { _add(len, false, false); }

    void add_init_buffer(std::size_t len) { _add(len, true, false); }

    void add_lock_buffer(std::size_t len) { _add(len, false, true); }

    void _add(std::size_t len, bool init, bool lock)
    {
        void* p = alloc_one_buffer_malloc(len);
        if (init)
            init_one_buffer(p, len);
        if (lock)
            mlock_one_buffer(p, len);
        buffer_list.emplace_back(std::tuple(p, len, lock));
    }
};

static void bench_alloc_one_buffer_malloc(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;

    for (auto _ : state)
        l.add_buffer(len);
}
BENCHMARK_US(bench_alloc_one_buffer_malloc);

static void bench_alloc_one_buffer_malloc_init(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;

    for (auto _ : state)
        l.add_init_buffer(len);
}
BENCHMARK_US(bench_alloc_one_buffer_malloc_init);

static void bench_alloc_one_buffer_malloc_mlock(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;

    std::size_t i = 0;
    for (auto _ : state)
        l.add_lock_buffer(len);
}
BENCHMARK_US(bench_alloc_one_buffer_malloc_mlock);

static void bench_alloc_500_buffer_malloc(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;
    len *= 500;

    for (auto _ : state)
        l.add_buffer(len);
}
BENCHMARK_US(bench_alloc_500_buffer_malloc)->Iterations(16);

static void bench_alloc_500_buffer_multi_malloc_init(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;
    std::size_t rep = 500;

    for (auto _ : state)
        for (std::size_t i = 0; i < rep; ++i)
            l.add_init_buffer(len);
}
BENCHMARK_US(bench_alloc_500_buffer_multi_malloc_init);

static void bench_alloc_500_buffer_multi_malloc_mlock(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;
    std::size_t rep = 500;

    for (auto _ : state)
        for (std::size_t i = 0; i < rep; ++i)
            l.add_lock_buffer(len);
}
BENCHMARK_US(bench_alloc_500_buffer_multi_malloc_mlock);

static void bench_alloc_500_buffer_single_malloc_init(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;
    len *= 500;

    std::size_t i = 0;
    for (auto _ : state) {
        if (++i == 16) {
            state.SkipWithError("Too much memory consumed");
            break;
        }
        l.add_init_buffer(len);
    }
}
BENCHMARK_US(bench_alloc_500_buffer_single_malloc_init);

static void bench_alloc_500_buffer_single_malloc_mlock(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;
    len *= 500;

    std::size_t i = 0;
    for (auto _ : state) {
        if (++i == 16) {
            state.SkipWithError("Too much memory consumed");
            break;
        }
        l.add_lock_buffer(len);
    }
}
BENCHMARK_US(bench_alloc_500_buffer_single_malloc_mlock);

const std::size_t WriteIterations = 1000;

static void do_bench_write_buffer(benchmark::State& state, const malloced_buffer_list& l)
{
    auto it = l.buffer_list.begin(), end = l.buffer_list.end();
    std::size_t bytes = 0;
    for (auto _ : state) {
        if (it == end)
            throw std::invalid_argument("writing too much buffers");
        void* p = std::get<0>(*it);
        std::size_t len = std::get<1>(*it);
        ++it;
        write_one_buffer(p, len);
        bytes += len;
    }

    state.SetBytesProcessed(bytes);
}

static void bench_alloc_one_buffer_write_cold(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;

    for (std::size_t i = 0; i < WriteIterations; ++i)
        l.add_buffer(len);

    do_bench_write_buffer(state, l);
}
BENCHMARK_US(bench_alloc_one_buffer_write_cold)->Iterations(WriteIterations);

static void bench_alloc_one_buffer_write_hot_init(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;

    for (std::size_t i = 0; i < WriteIterations; ++i)
        l.add_init_buffer(len);

    do_bench_write_buffer(state, l);
}
BENCHMARK_US(bench_alloc_one_buffer_write_hot_init)->Iterations(WriteIterations);

static void bench_alloc_one_buffer_write_hot_mlock(benchmark::State& state)
{
    malloced_buffer_list l;
    auto len = two_mega;

    for (std::size_t i = 0; i < WriteIterations; ++i)
        l.add_lock_buffer(len);

    do_bench_write_buffer(state, l);
}
BENCHMARK_US(bench_alloc_one_buffer_write_hot_mlock)->Iterations(WriteIterations);

BENCHMARK_MAIN();
