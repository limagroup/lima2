// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <benchmark/benchmark.h>

#include <cstring>

#include <array>

static const std::size_t buffer_size_64_sq = 64 * 64;
static const std::size_t buffer_size_256_sq = 256 * 256;
static const std::size_t buffer_size_1024_sq = 1024 * 1024;
static const std::size_t buffer_size_2048_sq = 2048 * 2048;

static constexpr std::size_t nb_buffers = 100;

template <class Pool, std::size_t buffer_size>
void bench_alloc_pool(benchmark::State& state)
{
    Pool pool(buffer_size, nb_buffers);
    std::array<char*, nb_buffers> ptrs;

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs)
            ptr = static_cast<char*>(pool.malloc());

        for (auto& ptr : ptrs)
            pool.free(ptr);
    }
}

template <class Pool, std::size_t buffer_size>
void bench_alloc_pool_ordered(benchmark::State& state)
{
    Pool pool(buffer_size, nb_buffers);
    char* ptr;

    for (auto _ : state) {
        // The code to benchmark
        ptr = static_cast<char*>(pool.ordered_malloc(nb_buffers));
        pool.ordered_free(ptr, nb_buffers);
    }
}

template <class Pool, std::size_t buffer_size>
void bench_alloc_pool_memset(benchmark::State& state)
{
    Pool pool(buffer_size, nb_buffers);
    std::array<char*, nb_buffers> ptrs;

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs) {
            ptr = static_cast<char*>(pool.malloc());
            memset(ptr, 0, buffer_size);
        }

        for (auto& ptr : ptrs)
            pool.free(ptr);
    }
}
