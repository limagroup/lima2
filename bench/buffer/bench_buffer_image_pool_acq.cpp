// Copyright (C) 2020 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <benchmark/benchmark.h>

#include "image_pool.hpp"

#include <chrono>
#include <thread>
#include <functional>
#include <future>
#include <type_traits>
#include <iostream>

#define BENCHMARK_MT(x) BENCHMARK(x)->UseManualTime()

using namespace std::literals::chrono_literals;

enum benchmark_task
{
    BenchDaq,
    BenchProc,
};

enum benchmark_type
{
    BenchGet,
    BenchPut,
};

template <benchmark_task, benchmark_type>
class bench_state;

namespace detail
{
template <bool Active, benchmark_type Type>
struct state;

struct active_state
{
    benchmark::State& state;

    active_state(benchmark::State& s, std::size_t /*nb_iterations*/) : state(s) {}

    auto begin() { return state.begin(); }
    auto end() { return state.end(); }

    void StartTiming() { t0 = std::chrono::high_resolution_clock::now(); }
    void StopTiming()
    {
        timestamp t = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = t - t0;
        state.SetIterationTime(elapsed.count());
    }

    void SkipWithError(const char* s) { state.SkipWithError(s); }

  private:
    using timestamp = std::chrono::time_point<std::chrono::high_resolution_clock>;
    timestamp t0;
};

template <>
struct state<true, BenchGet> : public active_state
{
    state(benchmark::State& s, std::size_t nb_iterations) : active_state(s, nb_iterations) {}

    void startGet() { StartTiming(); }
    void finishGet() { StopTiming(); }
    void startPut() {}
    void finishPut() {}
};

template <>
struct state<true, BenchPut> : public active_state
{
    state(benchmark::State& s, std::size_t nb_iterations) : active_state(s, nb_iterations) {}

    void startGet() {}
    void finishGet() {}
    void startPut() { StartTiming(); }
    void finishPut() { StopTiming(); }
};

class null_state
{
  public:
    struct iterator
    {
        std::size_t i;
        std::size_t& operator*() { return i; }
        iterator& operator++()
        {
            ++i;
            return *this;
        }
    };

    null_state(benchmark::State& /*s*/, std::size_t nb_iterations) : m_i(0), m_nb_iterations(nb_iterations) {}

    iterator begin() { return {m_i}; }
    iterator end() { return {m_nb_iterations}; }

    void startGet() {}
    void finishGet() {}
    void startPut() {}
    void finishPut() {}

    void SkipWithError(const char* s)
    {
        std::cerr << "Error: " << s << std::endl;
        m_nb_iterations = m_i;
    }

  private:
    std::size_t m_i;
    std::size_t m_nb_iterations;
};

bool operator!=(const null_state::iterator& a, const null_state::iterator& b)
{
    return a.i != b.i;
}

template <>
struct state<false, BenchGet> : public null_state
{
    state(benchmark::State& s, std::size_t nb_iterations) : null_state(s, nb_iterations) {}
};

template <>
struct state<false, BenchPut> : public null_state
{
    state(benchmark::State& s, std::size_t nb_iterations) : null_state(s, nb_iterations) {}
};
} // namespace detail

template <class Image, template <typename I> class Pool, template <typename T> class Queue,
          benchmark_task BenchmarkTask, benchmark_type BenchmarkType>
class acquisition
{
  public:
    using pool_t = Pool<Image>;
    using frame_view_ptr = typename pool_t::frame_view_ptr;
    using frame_t = std::tuple<std::size_t, frame_view_ptr>;
    using queue_t = Queue<frame_t>;

    acquisition(benchmark::State& state, std::size_t nb_frames, pool_t& pool, queue_t& queue) :
        m_state(state),
        m_nb_frames(nb_frames),
        m_pool(pool),
        m_queue(queue),
        m_proc(std::async(std::launch::async, &acquisition::proc_task, this)),
        m_daq(std::async(std::launch::async, &acquisition::daq_task, this))
    {
    }

  private:
    using task_t = std::future<void>;

    using daq_state = detail::state<BenchmarkTask == BenchDaq, BenchmarkType>;
    using proc_state = detail::state<BenchmarkTask == BenchProc, BenchmarkType>;

    void daq_task()
    {
        daq_state state(m_state, m_nb_frames);
        std::size_t i = 0;
        for (auto _ : state) {
            while (m_pool.empty())
                std::this_thread::sleep_for(10us);
            state.startGet();
            typename pool_t::frame_view_ptr frame_view;
            if (!m_pool.get_frame_view(frame_view))
                state.SkipWithError("Empty pool");
            state.finishGet();
            state.startPut();
            m_queue.push(frame_t(i, std::move(frame_view)));
            state.finishPut();
            ++i;
        }
    }

    void proc_task()
    {
        proc_state state(m_state, m_nb_frames);
        std::size_t i = 0;
        for (auto _ : state) {
            while (m_queue.empty())
                std::this_thread::sleep_for(10us);

            state.startGet();
            {
                frame_t frame;
                if (!m_queue.pop(frame))
                    state.SkipWithError("Empty queue");
                state.finishGet();
                auto frame_idx = std::get<0>(frame);
                if (frame_idx != i)
                    std::cerr << "Invalid frame: " << frame_idx << ", expected: " << i << std::endl;
                state.startPut();
            }
            state.finishPut();
            ++i;
        }
    }

    benchmark::State& m_state;
    pool_t& m_pool;
    queue_t& m_queue;
    std::size_t m_nb_frames;
    task_t m_daq;
    task_t m_proc;
};

#define ACQ_NB_FRAMES (1 * 1024 * 1024)

#define BENCHMARK_MT_ACQ_FRAMES(x) BENCHMARK_MT(x)->Iterations(ACQ_NB_FRAMES)

template <template <typename I> class Pool, template <typename T> class Queue, int buffer_images, benchmark_task Task,
          benchmark_type Type>
void bench_acq(benchmark::State& state)
{
    using image_t = gil::gray16_image_t;
    using acquisition_t = acquisition<image_t, Pool, Queue, Task, Type>;
    using pool_t = typename acquisition_t::pool_t;
    using queue_t = typename acquisition_t::queue_t;
    using point_t = typename image_t::view_t::point_t;

    point_t dims(1024, 1024);
    pool_t pool(buffer_images);
    for (std::size_t i = 0; i != buffer_images; ++i)
        pool.add_image(std::make_shared<image_t>(dims));

    queue_t queue;
    acquisition_t acq(state, ACQ_NB_FRAMES, pool, queue);
}

const std::size_t buffer_images = 128;

template <benchmark_task Task, benchmark_type Type>
void bench_safe_container(benchmark::State& state)
{
    bench_acq<safe_container::image_pool, safe_container::queue, buffer_images, Task, Type>(state);
}

static void bench_safe_container_daq_get(benchmark::State& state)
{
    bench_safe_container<BenchDaq, BenchGet>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_safe_container_daq_get);

static void bench_safe_container_daq_put(benchmark::State& state)
{
    bench_safe_container<BenchDaq, BenchPut>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_safe_container_daq_put);

static void bench_safe_container_proc_get(benchmark::State& state)
{
    bench_safe_container<BenchProc, BenchGet>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_safe_container_proc_get);

static void bench_safe_container_proc_put(benchmark::State& state)
{
    bench_safe_container<BenchProc, BenchPut>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_safe_container_proc_put);

using capacity_t = lock_free::capacity<buffer_images>;

template <typename T>
using image_pool_t = lock_free::image_pool<T, capacity_t>;

template <typename T>
using queue_t = lock_free::queue<T, capacity_t>;

template <benchmark_task Task, benchmark_type Type>
void bench_lockfree_container(benchmark::State& state)
{
    bench_acq<image_pool_t, queue_t, buffer_images, Task, Type>(state);
}

static void bench_lockfree_container_daq_get(benchmark::State& state)
{
    bench_lockfree_container<BenchDaq, BenchGet>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_lockfree_container_daq_get);

static void bench_lockfree_container_daq_put(benchmark::State& state)
{
    bench_lockfree_container<BenchDaq, BenchPut>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_lockfree_container_daq_put);

static void bench_lockfree_container_proc_get(benchmark::State& state)
{
    bench_lockfree_container<BenchProc, BenchGet>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_lockfree_container_proc_get);

static void bench_lockfree_container_proc_put(benchmark::State& state)
{
    bench_lockfree_container<BenchProc, BenchPut>(state);
}
BENCHMARK_MT_ACQ_FRAMES(bench_lockfree_container_proc_put);

BENCHMARK_MAIN();
