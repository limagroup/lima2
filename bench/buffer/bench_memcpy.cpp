// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <benchmark/benchmark.h>

#include <iostream>
#include <cstring>
#include <memory>

static void bench_memcpy(benchmark::State& state)
{
    const size_t Kilo = 1024;
    const size_t len = state.range(0) * Kilo;

    using buffer_ptr = std::unique_ptr<char[]>;
    buffer_ptr src = buffer_ptr(new char[len]);
    buffer_ptr dst = buffer_ptr(new char[len]);
    std::memset(src.get(), 0, len);
    std::memset(dst.get(), 0, len);

    size_t bytes = 0;
    for (auto _ : state) {
        // The code to benchmark
        std::memcpy(dst.get(), src.get(), len);
        bytes += len;
    }

    state.SetBytesProcessed(bytes);
}
BENCHMARK(bench_memcpy)
    ->Threads(1)
    ->Threads(2)
    ->Threads(4)
    ->Threads(6)
    ->Threads(8)
    ->Arg(128)
    ->Arg(256)
    ->Arg(512)
    ->Arg(256)
    ->Arg(1024)
    ->Arg(2048);

BENCHMARK_MAIN();
