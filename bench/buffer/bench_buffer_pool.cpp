// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "bench_buffer_pool.hpp"

#include <atomic>

static void bench_alloc_malloc(benchmark::State& state)
{
    std::size_t buffer_size = state.range(0);
    std::array<char*, nb_buffers> ptrs;

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs)
            ptr = static_cast<char*>(std::malloc(buffer_size));

        for (auto& ptr : ptrs)
            std::free(ptr);
    }
}
BENCHMARK(bench_alloc_malloc)->Arg(buffer_size_256_sq)->Arg(buffer_size_2048_sq);

static void bench_alloc_malloc_hot(benchmark::State& state)
{
    std::size_t buffer_size = state.range(0);
    std::array<char*, nb_buffers> ptrs;

    {
        for (auto& ptr : ptrs)
            ptr = static_cast<char*>(std::malloc(buffer_size));

        for (auto& ptr : ptrs)
            std::free(ptr);
    }

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs)
            ptr = static_cast<char*>(std::malloc(buffer_size));

        for (auto& ptr : ptrs)
            std::free(ptr);
    }
}
BENCHMARK(bench_alloc_malloc_hot)->Arg(buffer_size_256_sq)->Arg(buffer_size_2048_sq);

static void bench_alloc_malloc_memset(benchmark::State& state)
{
    std::size_t buffer_size = state.range(0);
    std::array<char*, nb_buffers> ptrs;

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs) {
            ptr = static_cast<char*>(std::malloc(buffer_size));
            memset(ptr, 0, buffer_size);
        }

        for (auto& ptr : ptrs)
            std::free(ptr);
    }
}
BENCHMARK(bench_alloc_malloc_memset)->Arg(buffer_size_256_sq)->Arg(buffer_size_2048_sq);

static void bench_empty_loop(benchmark::State& state)
{
    std::array<char*, nb_buffers> ptrs;

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs)
            ;

        for (auto& ptr : ptrs)
            ;
    }
}
BENCHMARK(bench_empty_loop);

static void bench_memory_barrier(benchmark::State& state)
{
    std::array<char*, nb_buffers> ptrs;

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs)
            std::atomic_thread_fence(std::memory_order_seq_cst);

        for (auto& ptr : ptrs)
            std::atomic_thread_fence(std::memory_order_seq_cst);
    }
}
BENCHMARK(bench_memory_barrier);

BENCHMARK_MAIN();
