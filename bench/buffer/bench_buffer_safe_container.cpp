// Copyright (C) 2020 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "bench_buffer_pool.hpp"
#include "safe_container.hpp"

template <template <typename T> class Stack>
void bench_alloc_stack(benchmark::State& state)
{
    using image_t = void*;
    using stack_t = Stack<image_t>;

    std::size_t buffer_size = state.range(0);

    stack_t stack;
    stack.reserve(buffer_size);
    for (std::size_t i = 0; i != nb_buffers; ++i)
        stack.push(std::malloc(buffer_size));

    std::array<char*, nb_buffers> ptrs;

    for (auto _ : state) {
        // The code to benchmark
        for (auto& ptr : ptrs)
            if (!stack.pop(reinterpret_cast<void*&>(ptr)))
                state.SkipWithError("Empty stack");

        for (auto& ptr : ptrs)
            stack.push(ptr);
    }
}

static void bench_alloc_safe_stack(benchmark::State& state)
{
    bench_alloc_stack<safe_container::stack>(state);
}
BENCHMARK(bench_alloc_safe_stack)->Arg(buffer_size_256_sq)->Arg(buffer_size_2048_sq);

template <typename T>
using stack_t = lock_free::stack<T, lock_free::capacity<nb_buffers>>;

static void bench_alloc_lockfree_stack(benchmark::State& state)
{
    bench_alloc_stack<stack_t>(state);
}
BENCHMARK(bench_alloc_lockfree_stack)->Arg(buffer_size_256_sq)->Arg(buffer_size_2048_sq);
