// Copyright (C) 2020 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <benchmark/benchmark.h>

#include "image_pool.hpp"

const std::size_t buffer_images = 100;

template <template <typename T> class Pool>
void bench_image_pool(benchmark::State& state)
{
    using image_t = gil::gray16_image_t;
    using pool_t = Pool<image_t>;
    using point_t = image_t::view_t::point_t;
    using frame_view_ptr = typename pool_t::frame_view_ptr;

    pool_t pool(buffer_images);
    point_t dims(1024, 1024);
    for (std::size_t i = 0; i != buffer_images; ++i)
        pool.add_image(std::make_shared<image_t>(dims));

    std::vector<frame_view_ptr> ptrs;
    ptrs.reserve(buffer_images);

    for (auto _ : state) {
        // The code to benchmark
        for (std::size_t i = 0; i != buffer_images; ++i) {
            frame_view_ptr frame;
            if (!pool.get_frame_view(frame))
                state.SkipWithError("Pool is empty");
            ptrs.emplace_back(std::move(frame));
        }

        if (!pool.empty())
            state.SkipWithError("After pushing: pool is not empty");

        ptrs.clear();
    }
}

void bench_safe_image_pool(benchmark::State& state)
{
    bench_image_pool<safe_container::image_pool>(state);
}
BENCHMARK(bench_safe_image_pool);

template <typename T>
using image_pool_t = lock_free::image_pool<T, lock_free::capacity<buffer_images>>;

void bench_lockfree_image_pool(benchmark::State& state)
{
    bench_image_pool<image_pool_t>(state);
}
BENCHMARK(bench_lockfree_image_pool);

BENCHMARK_MAIN();
