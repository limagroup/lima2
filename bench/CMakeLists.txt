# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

if (CMAKE_BUILD_TYPE STREQUAL "Release" OR CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")

add_subdirectory(buffer)
add_subdirectory(gil)
add_subdirectory(io)
add_subdirectory(process)

else()

message(WARNING "Benchmarks build requested but build type is not 'Release', skipping benchmarks build")

endif()