#!/bin/bash

set +e

# Work out the newest common ancestor between the detached HEAD that this CI job
# has checked out, and the upstream target branch.
# `${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}` is only defined if we’re running in
# a merge request pipeline; fall back to `${CI_DEFAULT_BRANCH}` otherwise.

git config --global --add safe.directory /builds/limagroup/lima2

echo "Target branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME:-${CI_DEFAULT_BRANCH}}"
newest_common_ancestor_sha=$(git merge-base origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME:-${CI_DEFAULT_BRANCH}} HEAD)
echo "Ancestor commit: $newest_common_ancestor_sha" 

# Apply git clang-format
clang_format_error=$(.gitlab-ci/git-clang-format --commit $newest_common_ancestor_sha --extensions "cpp,hpp" --quiet --diff | grep -v --color=never "no modified files to format")

# Check for linter errors
if [ -n "$clang_format_error" ]; then
    echo "Detected formatting issues; please fix" && exit 1
else
    echo "Formatting is correct"
fi
