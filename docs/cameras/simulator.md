# Simulator

The simulator is designed to help you getting started with Lima and to test/play Lima without any hardware.

The simulator provides two modes of operations:

 - **generator** generates frames with diffraction patterns and a set of parameters can be tuned to change those patterns like for instance the number and position of gaussian peaks;
 - **loader** reads frames from files.

Both modes have a prefetched variant, where the frames are prefetched in memory before the acquisition is started. This feature allows to simulate high frame rates detectors.
