# Development Environment

There is obviously not a single way to setup a cross platform devenv. In the following sections, a solution with CMake, WSL, Visual Studio and Conda is presented.

## Windows Subsystem for Linux

The Windows Subsystem for Linux lets developers run GNU/Linux environment -- including most command-line tools, utilities, and applications -- directly on Windows, unmodified, without the overhead of a virtual machine. [Read more...](https://docs.microsoft.com/en-us/windows/wsl/about)

Installation and initialization of the distro is [documented here](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

In the following notes we assume a Ubuntu/Debian distro.

````{admonition} Start ssh-server
:class: tip

The sshd service is not started by default and `ssh` is used by Visual Studio to initiate the remote build and debugging sessions. You may need to adjust the `sshd` port in `/etc/ssh/sshd_config`.

```
sudo service ssh start
```
````

## Visual Studio

Here are a couple of articles that covers installation and usage of Visual Studio and CMake together. Pretty powerful stuff.

 - [Using Visual Studio for Cross Platform C++ Development Targeting Windows and Linux](https://blogs.msdn.microsoft.com/vcblog/2018/11/26/using-visual-studio-for-cross-platform-c-development-targeting-windows-and-linux/) ;
 - [CMake projects in Visual C++](https://docs.microsoft.com/fr-fr/cpp/ide/cmake-tools-for-visual-cpp?view=vs-2017) ;
 - [Configure a Linux CMake project](https://docs.microsoft.com/en-us/cpp/linux/cmake-linux-project?view=vs-2017) ;
 - [Customize build and debug tasks for "Open Folder" development](https://docs.microsoft.com/fr-fr/visualstudio/ide/customize-build-and-debug-tasks-in-visual-studio?view=vs-2017)

### Adding clang support to Visual Studio

The more compilers, the better. Clang works equally on Linux and Windows is fast and shows pertinent error messages.

Install LLVM from the [official download section](http://releases.llvm.org/download.html).

Then get and install [the LLVM toolchain extension for Visual Studio 2017](https://marketplace.visualstudio.com/items?itemName=LLVMExtensions.llvm-toolchain)

## Conda packages

A complete `lima2` environment ca be installed from `requirements-conda.yaml`:

```
conda env create -f requirements-conda.yaml
```

There is no conda package that supports Boost.MPI so this specific library need to be either built manually or installed from ESRF conda channel.

## MPI

On Linux use your distro package manager or Conda, e.g.:

```
conda install mpich -c conda-forge
```

On Windows, use [MSMPI from Microsoft](https://docs.microsoft.com/en-us/message-passing-interface/microsoft-mpi).
