# Hardware interface

Note that the implementation a service (server) and the stub (client) are interchangeable which allows some flexibility to choose whether a component is distributed or not in the system.

## Control service

Configure and control the acquisition.

### Synopsis

```c++
/// Interface of the Control RPC service
struct control
{
	using init_params_t = /* Initialization parameters */ ;
	using acq_params_t = /* Acquisition parameters */;
	using xfer_params_t = /* Transfer parameters */;

	/// Validate the acquisition parameters and returns true if valid
	bool validate_acq_params(acq_params_t) const;

	/// Prepare the acquisition
	void prepare(acq_params_t acq_params, xfer_params_t xfer_params);

	/// Start acquisition
	void start();

	/// Stop acquisition
	void stop();

	/// Notification on camera event
	rpc::event<rpc::buffered_policy, int> on_event;
};
```


## Acquisition service

Responsible of the transfer of the data from the detector to a processing pipeline.

### Synopsis

```c++
/// Interface of the Acquisition RPC service
struct acquisition_interface
{
	using init_params_t = /* Initialization parameters */ ;
	using acq_params_t = /* Acquisition parameters */;
	using xfer_params_t = /* Transfer parameters */;
  
	using any_image_view_t = /* Image views supported by the detector */;

	/// Prepare the acquisition (e.g. allocate buffers)
	void prepare(acq_params_t acq_params, xfer_params_t xfer_params);

	/// Start acquisition
	void start();
	
	/// Stop acquisition
	void stop();

	/// Returns the number of images acquired
	int nb_images_acquired();

	/// Notification on image ready
	rpc::event<lima::rpc::unbuffered_policy, any_image_view_t> on_image_ready;

	/// Notification on end of acquisition
	rpc::event<rpc::callback_policy, int> on_end_acq;
};
```

## Processing service

Create and manage processing pipelines.

```c++
/// Interface of the Processing RPC service
struct processing_interface
{
	using any_image_view_t = /* Image views supported */;

	/// A processing pipeline
	using pipeline_t =  /* Pipeline */;

	/// Identifier of the acquition used to retrieve the associated pipeline
	using pipeline_id_t = /* UUID */;

	/// Status of the pipeline
	using pipeline_status_t = /* TBD */;

	/// Create a new processing pipeline with a given image type and pipeline description
	pipeline_id_t create_pipeline(any_image_view_t view, std::string pipeline_description);

	/// Close the receiver end of a pipeline
	void close_pipeline(pipeline_id_t id);

	/// Cancel a pipeline (remaining data to process would be lost)
	void cancel_pipeline(pipeline_id_t id);

	/// Send an image to a pipeline
	void send_image(pipeline_id_t id, any_image_view_t view);

	/// Get pipeline status
	pipeline_status_t status(pipeline_id_t id);

	// Notification on end of processing of a closed pipeline
	rpc::event<rpc::buffered_policy, pipeline_id_t> on_pipeline_end;
};
```
