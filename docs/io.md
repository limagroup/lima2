# I/O

This library provides an easy to use interface for reading and writing various image formats as well as TBB Fow Graph nodes. Mostly writing actually.

## High level API

The high level interface is design to write one liner

### Writting a frame

Writting an image in an HDF5 file can be done as follows:

```cpp
#include  <lima/io/hdf5.hpp>

rgb8_image_t input_image;

lima::io::h5_write_view("lena_grayscale.h5"s, gil::const_view(input_image));
lima::io::h5_zip_write_view("lena_grayscale_zip.h5"s, gil::const_view(input_image));
lima::io::h5_bshuf_lz4_write_view("lena_grayscale_bs.h5"s, gil::const_view(input_image));
```

## TBB Fow Graph nodes

The nodes are based on the Writer available in the I/O library and can be used to save frames from with a processing Graph.

### HDF5

```cpp
#include  <lima/processing/nodes/io_hdf5.hpp>
``` 

Here is quick example of two nodes graph, an `input_node` and an `io_hdf5_node`:

```cpp
flow::graph g;

// Given a frame type
using frame_t = lima::frame<lima::basic_metadata, gil::gray8_view_t, gil::gray16_view_t>;

// Define an input node that will generate input frames
const int nb_frames = 10000;
tbb::flow::input_node<frame_t> src(
    g,
    [&input_frame, nb_frames, idx = 0](frame_t& out) mutable {
        if (idx < nb_frames) {
            // Allocate a 512x512 8bit image
            auto input_image = std::make_shared<gil::gray8_image_t>(512, 512);
                
            // TODO: Fill the image
                
            // Returns the frame and fill metadata
            out = frame_t(gil::view(*input_image), input_image);
            out.metadata.idx = idx;
            idx++;
            return true;
        } else
            return false;
    },
    false);

std::filesystem::path base_path = ".";
std::string filename_prefix = "test";
std::string filename_suffix = ".h5";
int start_number = 0;
lima::io::file_exists_policy file_exists = lima::io::file_exists_policy::overwrite;
int nb_frames_per_file = 100;
int nb_frames_per_chunk = 10;

// Define an hdf5 node that will save input frames
lima::processing::io_hdf5_node<frame_t> saving(
    g,
    base_path,
    filename_prefix,
    filename_suffix,
    start_number,
    file_exists,
    nb_frames,
    nb_frames_per_file,
    nb_frames_per_chunk,
    lima::io::h5::get_frame_info(input_frame));

// Link saving node to source
tbb::flow::make_edge(src, saving);

src.activate();
g.wait_for_all();
```

## Framework 

Extending the IO framework is meant to be simple and straightforward.

### The Writer interface




### Multi-file Writer adapter

The `io::multi<typename Writer>` adapter augments a writer with multi-file capabilities, where the file rotation (nb of frames per file) and filename (prefix, numbering, suffix) are configurable.

A multifile adapter is contructed with

```cpp
multi::multi(
    std::filesystem::path base_path,
    std::string filename_prefix,
    std::string filename_suffix,
    int start_number,
    file_exists_policy file_exists_policy_arg,
    int nb_frames,
    int nb_frames_per_file,
    int nb_frames_per_chunk,
    frame_info_t const& frame_info,
    Args... args)
```

## HDF5
