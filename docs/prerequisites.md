# Prerequisites

## C++ standard

Most modern compilers have [good support for C++ 20](https://en.cppreference.com/w/cpp/compiler_support#cpp20), so it sounds reasonable to make use of the new language features offered by this standard. Moreover, some third party libraries were refactored to take advantage of  this standard (sometimes giving up compatibility with C++03).

## Build system

We use "modern" CMake extensively and provide [config-file package](https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html#config-file-packages) for downstream projects.

## Dependencies from Conda

Lima2 depends on third party libraries and tools that are all provided by the Conda cross platform package manager. These dependencies can be installed independently of the system libraries (in environment), where CMake build system will find them.
