# Control API

The control API is the high level API of Lima build on top of the Processing and Acquisition APIs. It's responsibility is to prepare the camera and processing pipeline, and control the acquisition with commands like start, stop, trigger...

The Control API has the following roles:

 * Prepare the camera for acquisition according to user settings
 * Prepare a processing pipeline according to actual hardware acquisition settings and user settings
 * Start / Stop acquisition
 * Orchestrate the components of the distributed system (acquisition, processing)

``` mermaid::

   sequenceDiagram
       Client->>Client: Instantiate params
       Client->>Control: Prepare
       Control-->>Client: Prepared!
       Client->>Control: Start
       loop Every frames
           Control-->>Client: Trigger ready
           Client->>Control: Trigger  
           Control-->>Client: New frame ready
       end
```

``` mermaid::

   graph TD
       B["Control API"]
       B-->C["Processing API"]
       B-->D["Acquisition API"];
```

A major shift from Lima1 is the ability to run a new acquisition while the previous is still processing. Current and previous processing can be retrieved by their acquisition id.

## Synopsis

```c++
template <typename Camera>
class control
{  
  // Defines the supported image types (a vector of types)
  typedef ... images_t;

  // Defines the type of the initialization parameters
  typedef ... init_params_t;

  // Define the type of the acquisition parameters
  typedef ... acq_params_t;

  // An identifier of the acquisition
  typedef ... acq_uid_t;

  // Defines the type of the processing
  typedef ... processing_t;

  // Constructor forwards the init params to the camera
  void control(const init_params_t& params);

  // Prepare the camera for acquisition
  // Returns the id for the prepared acquisition
  acq_uid_t prepare(const acq_params_t& params);

  // Start the acquisition
  void start();

  // Software trigger if the camera supports it
  void soft_trigger();

  // Stop the acquisition
  void stop();

  // Abort the acquisition
  void abort();

  // Reset the detector
  void reset();

  // Returns the current acquisition parameters
  acq_params_t acq_params() const;

  // Returns the processing for the given acquisition
  processing_t& const processing(acq_uid_t) const;

  // Returns static detector informations
  det_info_t det_info() const;

  // Register for a specific event, the condition_variable will be
  // notified when the event occurs.  
  void register_event(event_t, condition_variable);
};
```

# Usage

*TODO*.

```c++
mpi::environment env(argc, argv, mpi::threading::level::multiple);
mpi::communicator world;

using control_t = lima::control<camera_t>;

control_t::init_params_t init_params;
control_t::acq_params_t acq_params;

// Fill both init_params anb acq_params with relevant params

// Create the control
control_t ct(world, init_params);

// Prepare the acquisition
ct.prepare_acq(acq_params);

auto state_change_rcv = ct.register_state_change();

// Old school with state polling
using transition_table_t = typename control_t::transition_table_t;
if (ct.is_state(transition_table_t::prepared))
{
	// Tell the camera to start acquisition
	ct.start_acq();

	// Wait for the end of the acquisition
	while (ct.is_state(transition_table_t::running))
		// Blocking wait for the notification of state change
		state_change_rcv.get();
}
```
