# Python Binding

## The Lima module

The lima module provides binding for objects that are shared between cameras, e.g. the Frame object.

### lima.Frame

```python
import lima

# Create a Frame
frm = lima.Frame(10, 10, lima.GRAY32F)

# Interaction with numpy
import numpy as np

# Map the frame as nparray (without copy)
data = np.array(frm, copy=False)

# Use numpy algo to modify frame data
np.ndarray.fill(data, 1.1)
print(data)
```