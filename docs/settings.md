# Settings

> Settings are things the program asks the user to set.

## Setting

### Strong typedefs

Also known as [opaque typedefs](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2004/n1706.pdf).

> Proper type design can completely remove entire classes of errors.

### Constrained Strong typedefs
