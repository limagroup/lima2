# Distributed Computing

High performance detectors generate more data than a single acquisition backend can handle. The solution consists in running multiple coordinated acquisition backends, each backend supporting only part of dataflow.

## MPI Introduction

Here is a brief introduction to MPI.

### The message passing programming model 

An MPI application is a group of autonomous processes, each executing its own code written in a classic language (C++, Python).
All the program variables are private and reside in the local memory of each process. Each process has the possibility of executing different parts of a program. A variable is exchanged between two or several processes via a programmed call to specific MPI subroutines.

#### Multiple Data Single Program

In this configuration, every processes in the cluster run the same program. The program can have multiple facets or roles which are assigned according to process rank.

#### Multiple Data Multiple Program

MPI also have the ability run tasks that can be a different program that executes different part of the workflow. For example, one task can be a C++ program and another a python.

### Communications

#### Point to Point

#### Collective

#### RPC

### MPI Streams

## Frame dispatching

Several dispatching strategy are to be considered.

### Partial frame dispatching

Each submodule is connected to one (or more) backend that gets

### Full frame dispatching


## Executables

LImA
