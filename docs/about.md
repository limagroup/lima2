# About

## Introduction

The architecture of the library aims at clearly separating hardware specific code from common software configuration and features, like setting standard acquisition parameters (exposure time, external trigger), file saving and image processing.

LImA is a C++ library but the library also comes with a [Python](https://www.python.org/) binding. A [PyTango](https://pytango.readthedocs.io) device server for remote control is provided as well.

We provide Conda binary package for Windows and Linux for some cameras. Check out our [Conda channel](https://anaconda.org/esrf-bcu).

LImA is a very active project and many developments are ongoing and available from [GitHub](https://github.com/esrf-bliss/LImA). You can find stable version releases through git branches and tags on [Github releases](https://github.com/esrf-bliss/LImA/releases).

If you want to get in touch with the LIMA community, please send an email to lima@esrf.fr. You may also want to subscribe to our mailing list by sending a message to [sympa@esrf.fr](mailto:sympa@esrf.fr?subject=subscribe%20lima) with ``subscribe lima`` as subject.
