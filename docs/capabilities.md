# Capabilities

*TODO*.

## Sync

### Shutter

## Geometry transformation

### ROI

### Binning

### Flip

### Rotate

### Offset

## Saving

## Video



## Events


## New capabilities

- Image reconstruction: building complete images produced by multimodule segmented detectors into single memory buffers.
- Image splitting: dispatching images among several backend computers so that each computer receives a different part of the image, for instance for data processing purposes.
- Image extraction: transferring only regions of interest (ROIs) of the images in order to reduce data volume.
- Image extension: including zero pixels in the destination buffers to represent the area corresponding to intermodule gaps or dead areas.
- Image sampling: reducing data volume and the image resolution by transferring only a sampling of the pixels in the image.
- Image binning: lossless data volume reduction by pixel binning procedures
- Image mirroring: to cope with a detector built with symmetric modules
- Image rotation: to cope with a detector built with rotated modules
