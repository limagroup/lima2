# Producer Consumer

The problem describes two "processes", the producer and the consumer, who share a common, fixed-size buffer used as a queue.

## FIFO Implementations

### Boost.LockFree

See [Boost.LockFree](https://www.boost.org/doc/libs/release/libs/lockfree/).

```
#include <boost/lockfree/spsc_queue.hpp>
```

### ITBB Concurrent Queue

See [Concurrent Queue classes](https://software.intel.com/en-us/node/506076) and [When Not to Use Queues](https://software.intel.com/en-us/node/506083).


### MPI streams
