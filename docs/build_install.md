# Build and Install

## Install Conda packages

We provide [Conda](https://conda.io) binary packages for some cameras. This is, by far, the easiest way to get started with LImA2. Running:

``` bash
conda install --channel esrf-bcu --channel conda-forge lima2
```

would install a fully loaded LImA2 server and all its dependencies with the Simulator camera plugin. The camera comes as a python module but is also C++ development package that includes header files and [CMake package config](https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html) files.

The client part of the project can be installed in a similar way, potenatially on a different conputer and in a different Conda environment, with:

``` bash
conda install --channel esrf-bcu --channel conda-forge lima2-client
```

Refer to the [Lima2 Client](https://limagroup.gitlab-pages.esrf.fr/lima2-client/) documentation for more information.

## Build from source

First, you need to get the source from [Gitlab](https://gitlab.esrf.fr/limagroup/lima2).

### Get dependencies

We provide Conda packages for all the dependencies of the project. Some packages already exists in other Conda channels (e.g. Conda Forge) so the order of the channel priorities matters: the `esrf-bcu` channel should always come first.

``` bash
mamba env create --name lima2 --file requirements-conda.yaml
```

The dev environment provides the `cmake`, `ninja`, `cxx-compiler` packages which is everything needed to get started.

### Using CMake

Building and installing the project with the same options than the Conda package can be achieved in one step with:

``` bash
conda activate lima2
PREFIX=$CONDA_PREFIX conda/build.sh
```

The steps can be decomposed in:

Run `cmake` in the build directory to configure the build system:

``` bash
cmake -Bbuild -H. -GNinja
   [-DCMAKE_INSTALL_PREFIX=<desired installation path>]
   [-DPYTHON_SITE_PACKAGES_DIR=<desired python installation path>]
   -DLIMA_ENABLE_TIFF=ON
   -DLIMA_CAMERA_BASLER=ON
   -DLIMA_ENABLE_PYTHON=true
```

Then compile and install:

``` bash
cmake --build
sudo cmake --build --target install
```
