======================================
LImA 2 - Library for Image Acquisition
======================================

LImA (stands for **L** ibrary for **Im** age **A** cquisition) is a project for the unified control of 2D detectors. It is used in production in `ESRF Beamlines <https://www.esrf.eu/about/synchrotron-science/beamline>`_ and in other places.

Note that this documentation is also available in pdf and epub format.

.. toctree::
  :maxdepth: 2
  :caption: About

  about
  motivation
  design
  release_notes

.. .. _compilation:
..
.. .. _installation:
..
.. toctree::
  :maxdepth: 2
  :caption: Installation

  prerequisites
  build_install
  getting_started
  
.. toctree::
  :maxdepth: 2
  :caption: Control

  control
  
.. toctree::
  :maxdepth: 2
  :caption: Processing
  
  processing
  geometric_transformations

.. toctree::
  :maxdepth: 2
  :caption: Camera

  camera
  state_machine
  buffer
  capabilities

.. toctree::
  :maxdepth: 2
  :caption: Ancillary

  logging

.. toctree::
  :maxdepth: 2
  :caption: Built-in

  cameras
  processes
  
.. toctree::
  :maxdepth: 1
  :caption: Reference Manual

  cpp_api
  python_api

.. toctree::
  :maxdepth: 1
  :caption: Developer Doc

  core
  hardware
  channels
  io
  utils

.. toctree::
  :maxdepth: 3
  :caption: Contributing

  devenv
  tests
  benchmarks

.. _Python: http://python.org
.. _PyTango: http://github.com/tango-cs/pytango
