# State Machine

A finite-state machine (FSM) is an abstract machine that can be in exactly one of a finite number of states at any given time. The FSM can change from one state to another in response to some external events; the change from one state to another is called a transition. An FSM is defined by a list of its states, its initial state, and the conditions for each transition.

## Transition table

![](assets/daq_fsm.png)

this described machine is translated into C++ with the Embedded DSL (Domain Specific Languages) offered by the SML library:

```
*idle + event<prepare>[validate_params] / action_store_prop {} = preparing,
preparing + sml::on_entry<_> / action_prepare_acq {},
preparing = prepared,
prepared + event<start> = running,
running + sml::on_entry<_> / action_start_acq {},
running + sml::on_exit<_> / action_stop_acq {},
running + event<stop> = idle,
running + event<fault> = X,
```

## Replicated State Machine

![](assets/replicated_fsm.png)
