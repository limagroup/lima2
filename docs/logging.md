# Logging

Most of the time Lima run on a remote site, leaving the developer little chance to monitor their execution and figure out the reasons for their failure, once it should happen. Moreover, even the local debugging may become problematic if the application behavior depends heavily on asynchronous side events, such as a device feedback or another process activity.

This is where logging can help.

## Setting up Logging

A logging sink is configured to generate a log to file with a daily rotation interval or a maximum size of MB whichever come first.

```bash
$ cat lima_%N.log

[2019-11-22 18:01:46.626520][0x00000684][0x00007fffff4c0740][test/core/test_logging.cpp:18][]: A trace severity message without scope
[2019-11-22 18:01:46.627491][0x00000684][0x00007fffff4c0740][test/core/test_logging.cpp:30][test_named_scope_logging]: Hello from the test_named_scope_logging scope!
```

### Severity Levels

Lima defines the following severity levels, aka `severity_level`, ordered from the most to the lesser verbose:

 * `trace`
 * `debug`
 * `info`
 * `warning`
 * `error` (default)
 * `fatal`
 
### Channels for domain of application

 Lima defines the following channels, aka `domain`, that can be combined to filter several channels:

 * `core`,
 * `ctl`,
 * `acq`,
 * `det`,
 * `sdk`
 * `proc`

A special channel `domain::all` let's you get logs from every channels (default).

### Attributes

The following attributes are defined by default.

 * LineID
 * TimeStamp
 * ProcessID
 * ThreadID

### Filtering

By default, only the logs with severity over `error` from all channels are logged. To set the severity and channel filters:

```c++
// Filter with level >= debug and from all channels
lima::log::set_filter(log::severity_level::debug, log::domain::all);

// Filter with level >= info and from domains core and ctl
lima::log::set_filter(log::severity_level::info, log::domain::core & log::domain::ctl);
```

## Adding logs to the codebase

```c++
#include <lima/logging.hpp>`
```

Lima provides `LIMA_LOG`, a macro that accepts a severity level and channel. It returns in a stream-like object that supports insertion operator:

```c++
// The first arg is the severity level, the second arg is the channel
LIMA_LOG(debug, core) << "A debug severity message without scope";
```

A scope attribute is added to the log when available. The scope is defined with `LIMA_LOG_SCOPE(scope)`:

```c++
void named_scope_logging()
{
    LIMA_LOG_SCOPE("named_scope_logging");

    LIMA_LOG(trace, core) << "Hello from the function named_scope_logging!";
}
```
