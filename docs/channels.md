# Channels

The goal of this library is to provide a model to communicate and synchronize threads via message passing. This chapter introduces the usage of the Channel library of Lima2.

## Usage

Channels come in two flavors:

  - unbuffered channels
  - unbounded, buffered channels

```c++
// Create a channel of type int
auto channel = lima::make_buffered_channel<int>();

// Channel is a pair (sender, receiver) so with structured binding
auto [sender, receiver] = channel;

// Send a value through the channel
sender.set_value(s);

// Get value from the receiver (here blocking)
T evt = event_recv.get();
std::cout << "Got " << evt << "\n";

// Get value from the receiver (here non-blocking)
std::optional<T> evt = event_recv.try_get();
if (evt)
    std::cout << "Got " << evt << "\n";
```

Channels are used internally to pass events from the RPC client event loop to the client API where the receiver part of the channel is exposed.
