# Cameras

The list of the available cameras and supported platform is given in the following table:

| Name | Windows | Linux |
|:- |:-:|:-:|
| [Simulator](cameras/simulator.md) | x | x |
