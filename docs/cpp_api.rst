C++ API Reference
=================

Detector API
------------

.. doxygenclass:: lima::detectors::simulator::control
  :project: common
  :members:

.. doxygenclass:: lima::detectors::simulator::acquisition
  :project: common
  :members:

Allocators
----------

.. doxygenclass:: lima::numa_resource
  :project: common
  :members:

.. doxygenclass:: lima::cuda_resource
  :project: common
  :members:

.. doxygenclass:: lima::um_resource
  :project: common
  :members:

IO
--

HDF5
^^^^

.. doxygenclass:: lima::io::h5::file
  :project: common
  :members:

.. doxygenclass:: lima::io::h5::group
  :project: common
  :members:

.. doxygenclass:: lima::io::h5::dataset
  :project: common
  :members:

.. doxygenfunction:: lima::io::h5::link_soft
  :project: common
