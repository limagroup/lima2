# Getting Started

Lima2 is based on Message Passing Interface (MPI), a framework to develop parallel application on distributed memory systems. Lima2 is a so called multiple programs, multiple data (MPMD) application where the programs have different responsibilities:

 - control (1 process)
 - acquisition (n processes)
 - processing (n processes)

MPI defines a [standardized API](https://www.mpi-forum.org/) that is implemented by  multiple projects such OpenMPI, MPICH or MVAPICH.

Running an application requires to install an MPI library on all the node of the clusters. The conda packages are build against the following distributions:

 - MPICH on Linux
 - MS-MPI on Windows

## Tango Device Servers

Lima2 comes with a tango device server `lima2_tango` executable that can be used to control a camera (`Control` class) and receive the data (`Receiver` class). The implementation is camera specific and is provided by a plugin (a shared library) that is discovered and loaded at runtime.

The plugin folder is specified with the `LIMA2_PLUGIN_FOLDER` environment variable, and is usually set to `$CONDA_PREFIX/lib/plugins` when deploying in a Conda environment.

## Running the MPI application

MPI provides the  `mpirun` command which arguments are the executables to run. To keep things simple, running one control process, one acquisition process would look like this:

``` bash
TANGO_HOST="l-cb184-1:20000" LIMA2_PLUGIN_FOLDER=$CONDA_PREFIX/lib/plugins mpirun -n 1 lima2_tango id00_eiger2 --log-level=info : -n 1 --map-by numa:PE=0 lima2_tango id00_eiger2 -v4 --log-level=info
```

The placement of the process in terms of computer, CPU core and affinity can be controlled with `mpirun` parameters.

The `lima2_tango` executable is designed to support multiple cameras, hence the first (positional) parameter is the camera name.

```bash
$ lima2_tango --help
Log level set to warning
Usage: lima2_tango <INSTANCE> [options]
Allowed options:
  --help                     Produce help message
  --debug                    Stop the server at the beginning to attach
                             debugger
  --plugin-folder arg (=.)   Plugin folder
  --log-level arg (=warning) Logging level [trace=0, debug, info, warning,
                             error, fatal=5]
  --log-domain arg (=all)    Logging domain [core, ctl, acq,proc, io...]
```
