// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// This file contains a test boilerplate for checking that every public header
// is self-contained and does not have any missing #include-s.

#define LIMA_TEST_INCLUDE_HEADER() <lima/detectors/simulator/LIMA_TEST_HEADER>

#include LIMA_TEST_INCLUDE_HEADER()

int main()
{
    return 0;
}
