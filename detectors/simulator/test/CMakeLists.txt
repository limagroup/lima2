# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_executable(test_simulator
    main.cpp
    test_generator.cpp
)

target_precompile_headers(test_simulator
    REUSE_FROM lima_core)

target_link_libraries(test_simulator
    simulator
    Boost::unit_test_framework
)

add_test(NAME test_simulator COMMAND test_simulator)

if(LIMA_ENABLE_TEST_HEADERS)
    add_subdirectory(header)
endif(LIMA_ENABLE_TEST_HEADERS)