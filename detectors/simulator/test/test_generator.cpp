// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <string>

#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/io/hdf5.hpp>

#include "../src/generator.hpp"

namespace gil = boost::gil;
namespace test_data = boost::unit_test::data;

//typedef std::tuple<gil::gray8_image_t, gil::gray16_image_t, gil::gray32_image_t> image_types;

//// clang-format off
//template <typename T> struct file_suffix;
//template <> struct file_suffix<gil::gray8_image_t>  { inline static const char value[] = "gray8";  };
//template <> struct file_suffix<gil::gray16_image_t> { inline static const char value[] = "gray16"; };
//template <> struct file_suffix<gil::gray32_image_t> { inline static const char value[] = "gray32"; };
//template <typename T> constexpr const char* file_suffix_v = file_suffix<T>::value;
//
//template <typename T> struct pixel_fromtype;
//template <> struct pixel_fromtype<gil::gray8_image_t>  { inline static lima::pixel_enum value = lima::pixel_enum::gray8;  };
//template <> struct pixel_fromtype<gil::gray16_image_t> { inline static lima::pixel_enum value = lima::pixel_enum::gray16; };
//template <> struct pixel_fromtype<gil::gray32_image_t> { inline static lima::pixel_enum value = lima::pixel_enum::gray32; };
//template <typename T> constexpr lima::pixel_enum pixel_fromtype_v = pixel_fromtype<T>::value;
//// clang-format on

namespace lima
{

inline const char* file_suffix(pixel_enum p)
{
    switch (p) {
    case pixel_enum::gray8:
        return "gray8";
    case pixel_enum::gray16:
        return "gray16";
    case pixel_enum::gray32:
        return "gray32";
    case pixel_enum::gray32f:
        return "gray32f";
    }

    return nullptr;
}

pixel_enum pixel_types[] = {pixel_enum::gray8, pixel_enum::gray16, pixel_enum::gray32, pixel_enum::gray32f};

} // namespace lima

BOOST_DATA_TEST_CASE(test_generator, test_data::make(lima::pixel_types))
{
    using namespace std::string_literals;
    using namespace lima::detectors::simulator::generator;

    // Find the pixel_type
    auto pixel_type = sample;

    // Prepare a buffer to fill
    int width = 1024;
    int height = 1024;

    lima::point_t dims{width, height};
    lima::frame buffer(dims, pixel_type);

    init_params init_pars{width, height};
    gauss_params gauss_pars = {{{dims.x / 2., dims.y / 2., 128.0, 100}}, 0.0};
    diffraction_params diffraction_pars{dims.x / 2., dims.y / 2.};
    exec_params exec_pars{generator_type_enum::gauss, gauss_pars, diffraction_pars, pixel_type};
    generator gauss_gen(init_pars, exec_pars);
    exec_pars.type = generator_type_enum::diffraction;
    generator diffraction_gen(init_pars, exec_pars);

    // Apply generator (gauss or diffraction)
    auto gauss_visitor = [&](auto const& view) { return gauss_gen.get_frame(0); };

    auto diffraction_visitor = [&](auto const& view) { return diffraction_gen.get_frame(0); };

    boost::variant2::visit(gauss_visitor, lima::view(buffer));

    lima::io::h5_bshuf_lz4_write_frame("gauss_"s + file_suffix(pixel_type) + ".h5", buffer);

    boost::variant2::visit(diffraction_visitor, lima::view(buffer));

    lima::io::h5_bshuf_lz4_write_frame("diffraction_"s + file_suffix(pixel_type) + ".h5", buffer);
}
