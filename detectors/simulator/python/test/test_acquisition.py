import time
import lima
import simulator as sim

# Create detector control object
acq = sim.Acquisition();

def on_state_change(state):
    print(f"[ACQ] on_state_change {state}")

acq.register_on_state_change(on_state_change)

# Prepare acquisition
acq_params = sim.AcqParams()
info = acq.prepare_acq(acq_params)

def on_frame_ready(frame):
    print(f"[ACQ] frame #{frame.metadata.idx} ready")

acq.register_on_frame_ready(on_frame_ready)

# Start acquisition
acq.start_acq()

while (acq.state == sim.AcqState.running):
    print("[ACQ] running")
    time.sleep(0.1)
    print(f"[ACQ] {acq.nb_frames_xferred} frames transferred")
