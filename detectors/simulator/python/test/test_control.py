import time
import lima
import simulator as sim

# Create detector control object
init_params = sim.InitParams()
ctrl = sim.Control(init_params, True);

def on_state_change(state):
    print(f"[CTL] on_state_change {state}")

ctrl.register_on_state_change(on_state_change)

# Prepare acquisition
acq_params = sim.AcqParams()
ctrl.prepare_acq(acq_params)

# Start acquisition
ctrl.start_acq()

while (ctrl.state == sim.AcqState.running):
    print("[CTL] Acquisition running")
    time.sleep(0.1)
    print(f"[CTL] {ctrl.nb_frames_acquired} frames acquired")