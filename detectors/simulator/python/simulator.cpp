// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <sstream>
#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/functional.h>

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/filesystem.hpp>
#include <fmt/ranges.h>
#include <fmt/describe.hpp>

#include <boost/mp11.hpp>

#include <lima/detectors/simulator.describe.hpp>

namespace py = pybind11;
namespace describe = boost::describe;

PYBIND11_MODULE(simulator, m)
{
    using namespace pybind11::literals;
    using namespace lima::detectors::simulator;

    m.doc() = "simulator camera plugin"; // optional module docstring

    using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

    auto described_class = [](auto& class_) {
        using type_t = typename std::decay_t<decltype(class_)>::type;
        static_assert(boost::describe::has_describe_members<type_t>::value);

        // Constructor
        class_.def(py::init<>());

        // Member accessors
        boost::mp11::mp_for_each<describe::describe_members<type_t, describe::mod_any_access>>([&class_](auto D) {
            using A = boost::describe::annotate_member<decltype(D)>;
            auto doc = boost::describe::annotation_by_name_v<A, Doc>;
            class_.def_readwrite(D.name, D.pointer);
        });

        // Repr
        class_.def("__repr__", [&class_](type_t const& t) { return fmt::format("{}", t); });
    };

    // Automatic binding for described enums
    auto described_enum = [](auto& enum_) {
        using type_t = typename std::decay_t<decltype(enum_)>::type;

        boost::mp11::mp_for_each<boost::describe::describe_enumerators<type_t>>(
            [&](auto D) { enum_.value(D.name, D.value); });
        enum_.export_values();
    };

    py::class_<control::init_params_t> init_params(m, "InitParams");
    described_class(init_params);

    py::class_<control::acq_params_t> acq_params(m, "AcqParams");
    described_class(acq_params);

    //Bind the simulator control class
    py::class_<control> ctrl(m, "Control");
    ctrl.def(py::init<const control::init_params_t&>(), "init_params"_a)
        .def("prepare_acq", &control::prepare_acq, "Prepare the acquisition", "acq_params"_a)
        .def("start_acq", &control::start_acq, "Start the acquisition")
        .def("stop_acq", &control::stop_acq, "Stop the acquisition")
        .def("reset_acq", &control::reset_acq, "Reset the detector")
        .def_property_readonly("state", &control::state, "Returns the current state of the FSM")
        .def_property_readonly("nb_frames_acquired", &control::nb_frames_acquired,
                               "Returns the number of frames acquired")
        .def_property_readonly("version", &control::version, "Returns version informations")
        .def_property_readonly("det_info", &control::det_info, "Returns detector informations")
        .def("register_on_state_change", &control::register_on_state_change,
             "Register a callack for on change of state event");

    //Bind the simulator acquisition class
    py::class_<acquisition> acq(m, "Acquisition");
    acq.def(py::init<>())
        .def("prepare_acq", &acquisition::prepare_acq, "Prepare the acquisition", "acq_params"_a)
        .def("start_acq", &acquisition::start_acq, "Start the acquisition")
        .def("stop_acq", &acquisition::stop_acq, "Stop the acquisition")
        .def_property_readonly("state", &acquisition::state, "Returns the current state of the FSM")
        .def_property_readonly("nb_frames_xferred", &acquisition::nb_frames_xferred,
                               "Returns the number of frames transferred")
        .def("register_on_state_change", &acquisition::register_on_state_change,
             "Register a callack for on change of state event")
        .def("register_on_start_acq", &acquisition::register_on_start_acq,
             "Register a callack for on start of acquisition event")
        .def("register_on_frame_ready", &acquisition::register_on_frame_ready,
             "Register a callack for on frame ready event")
        .def("register_on_end_acq", &acquisition::register_on_end_acq,
             "Register a callack for on end of acquisition event");

    //Bind the reset status
    py::enum_<lima::reset_level_enum> reset(m, "ResetLevel");
    described_enum(reset);

    //Bind the state
    py::enum_<lima::acq_state_enum> state(m, "AcqState");
    described_enum(state);
}
