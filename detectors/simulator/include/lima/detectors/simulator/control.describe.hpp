// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/enums.describe.hpp>

#include <lima/hw/capabilities.describe.hpp>
#include <lima/hw/info.describe.hpp>
#include <lima/hw/status.describe.hpp>

#include <lima/detectors/simulator/params.describe.hpp>

#include <lima/detectors/simulator/control.hpp>