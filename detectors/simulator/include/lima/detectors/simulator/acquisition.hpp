// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <functional>
#include <memory>
#include <memory_resource>

#if defined(LIMA_HAS_PROPAGATE_CONST)
#include <experimental/propagate_const>
#endif

#include <lima/detectors/simulator/config.hpp>

#include <lima_simulator_export.h>

namespace lima
{
namespace detectors::simulator
{
    /// The acquisition class is the high level interface for data acquisition
    class LIMA_SIMULATOR_EXPORT acquisition
    {
      public:
        using init_params_t = typename config::init_params_t;
        using acq_params_t = typename config::acq_params_t;
        using acq_info_t = typename config::acq_info_t;
        using data_t = typename config::data_t;

        acquisition(std::pmr::polymorphic_allocator<std::byte> alloc = std::pmr::polymorphic_allocator<std::byte>());
        ~acquisition();             // defined in the implementation file, where impl is a complete type
        acquisition(acquisition&&); // defined in the implementation file
        acquisition(const acquisition&) = delete;
        acquisition& operator=(acquisition&&); // defined in the implementation file
        acquisition& operator=(const acquisition&) = delete;

        /// \{
        /// \name MPI

        /// Return the rank of the acquisition in the world communicator
        int world_rank() const;

        /// Return the rank of the acquisition in the acquisition communicator
        int recv_rank() const;

        /// \}

        /// \{
        /// \name Acquisition

        /// Prepare acquisition and returns information for the processing
        acq_info_t prepare_acq(acq_params_t const& acq_params);

        /// Start acquisition
        void start_acq();

        /// Stop acquisition
        void stop_acq();

        /// Called when all receivers acquisition has ended
        void close_acq();

        /// Reset acquisition
        void reset_acq();

        /// \}

        /// \{
        /// \name Progress

        /// Returns the number of frames transferred
        int nb_frames_xferred() const;

        /// \}

        /// \{
        /// \name State

        /// Returns the state of the control
        acq_state_enum state() const;

        /// Register a callack for on change of state event
        void register_on_state_change(std::function<void(acq_state_enum)> cbk);

        /// \}

        /// \{
        /// \name Callbacks

        /// Register a callack for on start of acquisition event
        void register_on_start_acq(std::function<void()> cbk);

        /// Register a callack for on frame ready event
        void register_on_frame_ready(std::function<void(data_t)> cbk);

        /// Register a callack for on end of acquisition event
        void register_on_end_acq(std::function<void(int)> cbk);

        /// \}

      private:
        class impl;

#if defined(LIMA_HAS_PROPAGATE_CONST)
        std::experimental::propagate_const< // const-forwarding pointer wrapper
            std::unique_ptr<                // unique-ownership opaque pointer
                impl>>
            m_pimpl; // to the forward-declared implementation class
#else
        std::unique_ptr< // unique-ownership opaque pointer
            impl>
            m_pimpl; // to the forward-declared implementation class
#endif
    };

} // namespace detectors::simulator
} //namespace lima
