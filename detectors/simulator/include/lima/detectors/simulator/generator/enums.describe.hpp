// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe.hpp>

#include <boost/describe/io_enums.hpp>

#include <lima/detectors/simulator/generator/enums.hpp>

namespace lima
{
namespace detectors::simulator::generator
{
    BOOST_DESCRIBE_ENUM(generator_type_enum, gauss, diffraction)

} // namespace detectors::simulator::generator
} // namespace lima
