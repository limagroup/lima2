// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

#include <lima/core/enums.hpp>

#include <lima/processing/serial/generator/params.hpp>

#include <lima/detectors/simulator/generator/enums.hpp>

namespace lima
{
namespace detectors::simulator::generator
{
    using namespace lima::processing::generator;

    /// Initialization parameters: defined once in the application
    struct init_params
    {
        int width = 2048;
        int height = 2048;
    };

    /// Runtime parameters: defined each time the generator is instantiated
    struct exec_params
    {
        generator_type_enum type = generator_type_enum::gauss; //!< The type of generator
        gauss_params gauss;                                    //!< The Gauss generator parameters
        diffraction_params diffraction;                        //!< The Diffraction generator parameters
        pixel_enum pixel_type = pixel_enum::gray8;             //!< The pixel type (layout, bitdepth)
        size_t nb_channels = 1;                                //!< The numbe of channels
    };

} // namespace detectors::simulator::generator
} // namespace lima
