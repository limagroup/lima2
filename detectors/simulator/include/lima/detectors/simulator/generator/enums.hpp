// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace lima
{
namespace detectors::simulator::generator
{
    /// Generator type
    enum class generator_type_enum : int
    {
        gauss,
        diffraction
    };

} // namespace detectors::simulator::generator
} // namespace lima
