// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>
#include <boost/describe/io_structs.hpp>

#include <lima/processing/serial/generator/params.describe.hpp>

#include <lima/detectors/simulator/generator/enums.describe.hpp>
#include <lima/detectors/simulator/generator/params.hpp>

namespace lima
{
namespace detectors::simulator::generator
{
    BOOST_DESCRIBE_STRUCT(init_params, (), (width, height))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(init_params, width,
        (desc, "frame width"),
        (doc, "The width of the frame in pixel"))

    BOOST_ANNOTATE_MEMBER(init_params, height,
        (desc, "frame height"),
        (doc, "The height of the frame in pixel"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(exec_params, (), (type, gauss, diffraction, pixel_type, nb_channels))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(exec_params, type,
        (desc, "type of generator"),
        (doc, "The type of generator [gauss, diffraction]"))

    BOOST_ANNOTATE_MEMBER(exec_params, gauss,
        (desc, "Gauss generator params"),
        (doc, "The configuration of the Gauss generator"))

    BOOST_ANNOTATE_MEMBER(exec_params, diffraction,
        (desc, "Diffraction generator params"),
        (doc, "The configuration of the Diffraction generator"))

    BOOST_ANNOTATE_MEMBER(exec_params, pixel_type,
        (desc, "pixel type"),
        (doc, "The pixel type (channel, color space and layout) [gray8, gray16, gray32, gray32f]"))

    BOOST_ANNOTATE_MEMBER(exec_params, nb_channels,
        (desc, "nb channels"),
        (doc, "The number of channels or band"))
    // clang-format on

    using boost::describe::operator<<;

} // namespace detectors::simulator::generator
} // namespace lima
