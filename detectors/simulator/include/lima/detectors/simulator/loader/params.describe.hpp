// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>
#include <boost/describe/io_structs.hpp>

#include <lima/io/h5/params.describe.hpp>

#include <lima/detectors/simulator/loader/params.hpp>

namespace lima
{
namespace detectors::simulator::loader
{
    BOOST_DESCRIBE_STRUCT(exec_params, (io::h5::reader::params_t), ())

    using boost::describe::operator<<;

} // namespace detectors::simulator::loader
} // namespace lima
