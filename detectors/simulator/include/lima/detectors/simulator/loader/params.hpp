// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

#include <lima/io/h5/reader.hpp>

namespace lima
{
namespace detectors::simulator::loader
{
    using namespace std::string_literals;

    /// Execution parameters: defined each time the loader is instantiated
    struct exec_params : io::h5::reader::params_t // loading_params
    {
    };

} // namespace detectors::simulator::loader
} // namespace lima
