// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

#include <lima/core/enums.hpp>
#include <lima/core/rectangle.hpp>

#include <lima/hw/params.hpp>

#include <lima/detectors/simulator/enums.hpp>
#include <lima/detectors/simulator/generator/params.hpp>
#include <lima/detectors/simulator/loader/params.hpp>

namespace lima
{
namespace detectors::simulator
{
    /// Initialization parameters specific to the camera
    struct init_params
    {
        generator::init_params generator;
    };

    /// Acquisition parameters specific to the detector
    struct detector_params
    {
        image_source_enum image_source = image_source_enum::generator; //!< The image source: [generator, loader]
        generator::exec_params generator;                              //!< The generator parameters
        loader::exec_params loader;                                    //!< The loader parameters
        int nb_prefetch_frames = 1; //!< The number of frames pre-computed (ring buffer)
        int dropped_frame_idx = 0;  //!< The index of the dropped frame (0 means no drop)
        int failed_frame_idx = 0;   //<! The number of frames acquired before frame failed (0 means no error)
    };

    struct acq_params : hw::acq_params
    {
        detector_params det; //!< Detector parameters
    };

} // namespace detectors::simulator
} // namespace lima
