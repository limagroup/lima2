// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/frame.hpp>
#include <lima/core/frame_info.hpp>

#include <lima/hw/params.hpp>

#include <lima/detectors/simulator/enums.hpp>
#include <lima/detectors/simulator/params.hpp>

namespace lima
{
namespace detectors::simulator
{
    /// Configuration of the camera plugin
    struct config
    {
        /// Initialization parameters specific to the camera
        using init_params_t = init_params;

        /// Acquisition parameters
        using acq_params_t = acq_params;

        /// Acquisition information passed to processing
        using acq_info_t = frame_info;

        /// Output data type of the acquisition
        using data_t = frame;

        /// Detector information
        using det_info_t = hw::info;
    };
} // namespace detectors::simulator
} // namespace lima
