// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>
#include <boost/describe/io_structs.hpp>

#include <lima/hw/params.describe.hpp>

#include <lima/detectors/simulator/generator/params.describe.hpp>
#include <lima/detectors/simulator/loader/params.describe.hpp>
#include <lima/detectors/simulator/enums.describe.hpp>
#include <lima/detectors/simulator/params.hpp>

namespace lima
{
namespace detectors::simulator
{
    BOOST_DESCRIBE_STRUCT(init_params, (), (generator))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(init_params, generator,
        (desc, "generator init params"),
        (doc, "Generator initialization parameters: defined once in the application"))
    // clang-format on

    BOOST_DESCRIBE_STRUCT(detector_params, (),
                          (image_source, generator, loader, nb_prefetch_frames, dropped_frame_idx, failed_frame_idx))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(detector_params, image_source,
        (desc, "simulator image source"),
        (doc, "The image source: [generator, loader]"))

    BOOST_ANNOTATE_MEMBER(detector_params, generator,
        (desc, "generator exec params"),
        (doc, "Generator execution paramaters: defined on each instantiation"))

    BOOST_ANNOTATE_MEMBER(detector_params, loader,
        (desc, "loader exec params"),
        (doc, "Loader execution paramaters: defined on each instantiation"))

    BOOST_ANNOTATE_MEMBER(detector_params, nb_prefetch_frames,
        (desc, "nb prefetched frames"),
        (doc, "Number of frames prefetched in the simulator circular buffer"))

    BOOST_ANNOTATE_MEMBER(detector_params, dropped_frame_idx,
        (desc, "dropped frame idx"),
        (doc, "Number of frames before frames get dropped (0 means no drop)"))

    BOOST_ANNOTATE_MEMBER(detector_params, failed_frame_idx,
        (desc, "failed frame idx"),
        (doc, "Number of frames before frames acquisition failed (0 means no error)"))

    // clang-format on

    BOOST_DESCRIBE_STRUCT(acq_params, (hw::acq_params), (det))

    // clang-format off
    BOOST_ANNOTATE_MEMBER(acq_params, det,
        (desc, "detector"),
        (doc, "The HW specific parameters of the detector"))
    // clang-format on

    using boost::describe::operator<<;

} // namespace detectors::simulator
} // namespace lima
