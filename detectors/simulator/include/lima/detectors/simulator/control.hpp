// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory>
#include <string>

#if defined(LIMA_HAS_PROPAGATE_CONST)
#include <experimental/propagate_const>
#endif

#include <lima/core/enums.hpp>
#include <lima/hw/capabilities.hpp>
#include <lima/hw/info.hpp>
#include <lima/hw/status.hpp>

#include <lima/detectors/simulator/config.hpp>
#include <lima_simulator_export.h>

namespace lima
{
namespace detectors::simulator
{
    // The control class is the high level interface for detector control
    class LIMA_SIMULATOR_EXPORT control
    {
      public:
        using init_params_t = typename config::init_params_t;
        using acq_params_t = typename config::acq_params_t;
        using det_info_t = hw::info;
        using det_capabilities_t = hw::capabilities;
        using det_status_t = hw::status;

        /// Construct the control and broadcast the camera init parameters
        control(init_params_t const& init_params);
        ~control();         // defined in the implementation file, where impl is a complete type
        control(control&&); // defined in the implementation file
        control(const control&) = delete;
        control& operator=(control&&); // defined in the implementation file
        control& operator=(const control&) = delete;

        /// \{
        /// \name MPI

        /// Return the rank of the control in the world communicator
        int world_rank() const;

        /// \}

        /// \{
        /// \name Acquisition

        /// Prepare acquisition
        void prepare_acq(acq_params_t const& acq_params);

        /// Start acquisition
        void start_acq();

        /// Software trigger if the camera supports it
        void soft_trigger();

        /// Stop acquisition
        void stop_acq();

        /// Called when all receivers acquisition has ended
        void close_acq();

        /// Reset camera
        void reset_acq(reset_level_enum level);

        /// \}

        /// \{
        /// \name Progress

        /// Returns the number of frames acquired
        int nb_frames_acquired() const;

        /// \}

        /// \{
        /// \name State

        /// Returns the state of the control
        acq_state_enum state() const;

        /// Register a callack for a change of state event
        void register_on_state_change(std::function<void(acq_state_enum)> cbk);

        /// \}

        /// \{
        /// \name General Info

        /// Returns the version of the camera plugin
        std::string version() const;

        /// Returns the detector information
        det_info_t det_info() const;

        /// Returns hardware capabilities
        det_capabilities_t det_capabilities() const;

        /// Returns the status of the detector
        det_status_t det_status() const;

        /// }

        /// \{
        /// \name Detector specific

        /// Initialize the detector head
        void initialize();

        /// Set HV on/off
        void high_voltage(bool on_off);

        /// Returns the current HV on/off state
        bool high_voltage() const;

        /// }

      private:
        class impl;

#if defined(LIMA_HAS_PROPAGATE_CONST)
        std::experimental::propagate_const< // const-forwarding pointer wrapper
            std::unique_ptr<                // unique-ownership opaque pointer
                impl>>
            m_pimpl; // to the forward-declared implementation class
#else
        std::unique_ptr< // unique-ownership opaque pointer
            impl>
            m_pimpl; // to the forward-declared implementation class
#endif
    };

} // namespace detectors::simulator
} //namespace lima
