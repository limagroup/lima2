// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/detectors/simulator/control.hpp>
#include <lima/detectors/simulator/acquisition.hpp>
#include <lima_simulator_export.h>

#if defined(LIMA_HAS_PROPAGATE_CONST)
#include <experimental/propagate_const>
#endif

namespace lima
{
namespace detectors::simulator
{
    struct LIMA_SIMULATOR_EXPORT detector
    {
        using control_t = control;
        using acquisition_t = acquisition;

        /// Name of the camera
        static constexpr const char* const name = "simulator";
    };

} // namespace detectors::simulator
} // namespace lima
