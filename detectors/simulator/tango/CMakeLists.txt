# Copyright (C) 2021 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

# Control
add_library(lima2_${PROJECT_NAME}_ctrl_tango SHARED
    control.cpp
)

target_include_directories(lima2_${PROJECT_NAME}_ctrl_tango
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}
    PRIVATE include)

target_link_libraries(lima2_${PROJECT_NAME}_ctrl_tango 
    PUBLIC lima_tango_headers
    PRIVATE simulator_ctrl
)

set_target_properties(lima2_${PROJECT_NAME}_ctrl_tango PROPERTIES
    INTERPROCEDURAL_OPTIMIZATION_RELEASE ON
    CXX_VISIBILITY_PRESET hidden
    VISIBLITY_INLINES_HIDDEN hidden
)

set_target_properties(lima2_${PROJECT_NAME}_ctrl_tango PROPERTIES
    VERSION "${PROJECT_VERSION}"
    SOVERSION "${PACKAGE_VERSION_MAJOR}.${PACKAGE_VERSION_MINOR}"
)

# Receiver
add_library(lima2_${PROJECT_NAME}_recv_tango SHARED
    receiver.cpp
)

target_include_directories(lima2_${PROJECT_NAME}_recv_tango
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}
    PRIVATE include)

target_link_libraries(lima2_${PROJECT_NAME}_recv_tango 
    PUBLIC lima_tango_headers
    PRIVATE simulator_ctrl
    PRIVATE simulator_recv
)

set_target_properties(lima2_${PROJECT_NAME}_recv_tango PROPERTIES
    INTERPROCEDURAL_OPTIMIZATION_RELEASE ON
    CXX_VISIBILITY_PRESET hidden
    VISIBLITY_INLINES_HIDDEN hidden
)

set_target_properties(lima2_${PROJECT_NAME}_recv_tango PROPERTIES
    VERSION "${PROJECT_VERSION}"
    SOVERSION "${PACKAGE_VERSION_MAJOR}.${PACKAGE_VERSION_MINOR}")

# Install   
install(
    TARGETS lima2_${PROJECT_NAME}_ctrl_tango lima2_${PROJECT_NAME}_recv_tango
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/plugins   # .so files are libraries
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/plugins
)
