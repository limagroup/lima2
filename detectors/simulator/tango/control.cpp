// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/dll/alias.hpp>

#include <lima/tango/control.hpp>
#include <lima/tango/control_class.hpp>

#include <lima/detectors/simulator.describe.hpp>

using control_t = lima::detectors::simulator::control;

// Explicitely instantiate template for Device
template class lima::tango::control<control_t>;

// Explicitely instantiate template for DeviceClass
template class lima::tango::control_class<control_t>;

// Specific commands
class initialize_command : public Tango::Command
{
  public:
    initialize_command() : Tango::Command("Initialize", Tango::DEV_VOID, Tango::DEV_VOID, "", "", Tango::OPERATOR) {}

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_DEBUG << "initialize::execute(): arrived" << std::endl;

        static_cast<lima::tango::control<control_t>*>(dev)->m_ctrl->initialize();

        return new CORBA::Any();
    }
};

// Specific attributes
class high_voltage_attr : public Tango::Attr
{
  public:
    high_voltage_attr() : Tango::Attr("high_voltage", Tango::DEV_BOOLEAN, Tango::READ_WRITE) {}

    void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
    {
        TANGO_LOG_DEBUG << "Reading attribute " << attr.get_name() << std::endl;

        Tango::DevBoolean* t_val = new Tango::DevBoolean;
        *t_val = static_cast<lima::tango::control<control_t>*>(dev)->m_ctrl->high_voltage();
        attr.set_value(t_val, 1, 0, true);
    }

    void write(Tango::DeviceImpl* dev, Tango::WAttribute& attr) override
    {
        TANGO_LOG_DEBUG << "Writing attribute " << attr.get_name() << std::endl;

        // Retrieve write value
        Tango::DevBoolean t_val;
        attr.get_write_value(t_val);

        static_cast<lima::tango::control<control_t>*>(dev)->m_ctrl->high_voltage(t_val);
    }

    //bool is_allowed(Tango::DeviceImpl* dev, Tango::AttReqType ty) override { return true; }
};

// Factory method
static lima::tango::control_class<control_t>* create()
{
    std::vector<Tango::Command*> additional_commands = {new initialize_command()};
    std::vector<Tango::Attr*> additional_attributes = {new high_voltage_attr()};
    return lima::tango::control_class<control_t>::init("LimaSimulatorControl", additional_commands,
                                                       additional_attributes);
}

BOOST_DLL_ALIAS(create,      // <-- this function is exported with...
                create_class // <-- ...this alias name
)