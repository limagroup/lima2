// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/dll/alias.hpp>

#include <lima/tango/receiver.hpp>
#include <lima/tango/receiver_class.hpp>

#include <lima/detectors/simulator.describe.hpp>

using acquisition_t = lima::detectors::simulator::acquisition;

// Explicitely instantiate template
template class lima::tango::receiver<acquisition_t>;

// Explicitely instantiate template for DeviceClass
template class lima::tango::receiver_class<acquisition_t>;

// Factory method
static lima::tango::receiver_class<acquisition_t>* create()
{
    return lima::tango::receiver_class<acquisition_t>::init("LimaSimulatorReceiver");
}

BOOST_DLL_ALIAS(create,      // <-- this function is exported with...
                create_class // <-- ...this alias name
)