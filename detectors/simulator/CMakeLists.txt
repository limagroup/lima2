# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

project(simulator VERSION 0.1.0)

string(TOUPPER "${PROJECT_NAME}" PROJECT_NAME_UPPER)
set(_option_name LIMA_CAMERA_${PROJECT_NAME_UPPER})

option(${_option_name} "Add Support for Camera ${PROJECT_NAME_UPPER}" ON)

if(${_option_name})

add_subdirectory(src)
add_subdirectory(test)

# Tango
if(LIMA_ENABLE_TANGO)
    add_subdirectory(tango)
endif()

# Python
if(LIMA_ENABLE_PYTHON)
    add_subdirectory(python)
endif(LIMA_ENABLE_PYTHON)

endif(${_option_name})
