// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/gil/extension/dynamic_image/apply_operation.hpp>

#include <lima/core/pixel.hpp>

#include <lima/io/multi.hpp>
#include <lima/io/hdf5.hpp>

#include <lima/detectors/simulator/loader/params.hpp>

namespace lima
{
namespace detectors::simulator::loader
{
    struct loader
    {
        using reader_t = io::multi_reader<io::h5::reader>;

        loader(io::h5::reader::params_t const& reader_params, int rank) :
            m_reader(reader_params, rank), m_dimensions(m_reader.dimensions()), m_pixel_type(m_reader.pixel_type())
        {
        }

        point_t dimensions() { return m_dimensions; }

        size_t nb_channels() { return 1; }

        pixel_enum pixel_type() { return m_pixel_type; }

        // visitor will be called with generated image view
        frame get_frame(int frame_nr) { return m_reader.read_frame(frame_nr); }

        reader_t m_reader;
        point_t m_dimensions;
        pixel_enum m_pixel_type;
    };
} // namespace detectors::simulator::loader
} // namespace lima
