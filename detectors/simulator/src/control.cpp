// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "control_impl.hpp"

namespace lima
{
namespace detectors::simulator
{
    /// Implementation of the control
    class control::impl : public simulator::control_impl
    {
        using parent_t = simulator::control_impl;

        using parent_t::parent_t;
    };

    // Pimpl boilerplate
    control::control(init_params_t const& init_params) : m_pimpl{std::make_unique<impl>(init_params)} {}

    control::control(control&&) = default;

    control::~control() = default;

    control& control::operator=(control&&) = default;

    int control::world_rank() const { return m_pimpl->world_rank(); }

    void control::prepare_acq(acq_params_t const& acq_params) { m_pimpl->prepare(acq_params); }

    void control::start_acq() { m_pimpl->start(); }

    void control::soft_trigger() { m_pimpl->trigger(); }

    void control::stop_acq() { m_pimpl->stop(); }

    void control::close_acq() { m_pimpl->close(); }

    void control::reset_acq(reset_level_enum level) { m_pimpl->reset(level); }

    int control::nb_frames_acquired() const { return m_pimpl->nb_frames_acquired(); }

    acq_state_enum control::state() const { return m_pimpl->state(); }

    void control::register_on_state_change(std::function<void(acq_state_enum)> cbk)
    {
        m_pimpl->register_on_state_change(cbk);
    }

    std::string control::version() const { return m_pimpl->version(); }

    control::det_info_t control::det_info() const { return m_pimpl->det_info(); }

    control::det_capabilities_t control::det_capabilities() const { return m_pimpl->det_capabilities(); }

    control::det_status_t control::det_status() const { return m_pimpl->det_status(); }

    void control::initialize() { m_pimpl->initialize(); }

    void control::high_voltage(bool on_off) { return m_pimpl->high_voltage(on_off); }

    bool control::high_voltage() const { return m_pimpl->high_voltage(); }
} // namespace detectors::simulator
} // namespace lima
