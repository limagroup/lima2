// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/exceptions.hpp>
#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>

#include <lima/processing/serial/generator.hpp>

#include <lima/detectors/simulator/generator/params.hpp>

namespace lima
{
namespace detectors::simulator::generator
{
    // The default channel converter maps the range of the value of the source to the destination,
    // this is not what we want here
    struct channel_converter_round
    {
        template <typename Ch1, typename Ch2>
        void operator()(const Ch1& src, Ch2& dst) const
        {
            dst = std::round(src);
        }
    };

    struct generator
    {
        generator(init_params const& init_pars, exec_params const& exec_pars) :
            m_init_params(init_pars), m_generator_params(exec_pars)
        {
        }

        point_t dimensions() { return {m_init_params.width, m_init_params.height}; }

        pixel_enum pixel_type() { return m_generator_params.pixel_type; }

        size_t nb_channels() { return m_generator_params.nb_channels; }

        frame get_frame(int frame_idx)
        {
            frame res(dimensions(), pixel_type());

            boost::variant2::visit(
                [frame_idx, generator_params = m_generator_params](auto view) {
                    using pixel_type = typename decltype(view)::value_type;
                    auto channel_conv = channel_converter_round();

                    switch (generator_params.type) {
                    case generator_type_enum::gauss: {
                        generate_gauss gen(frame_idx, generator_params.gauss);
                        auto src = make_generated_view(view.dimensions(), gen);
                        boost::gil::copy_pixels(boost::gil::color_converted_view<pixel_type>(src, channel_conv), view);
                        break;
                    }

                    case generator_type_enum::diffraction: {
                        generate_diffraction gen(frame_idx, generator_params.gauss, generator_params.diffraction);
                        auto src = make_generated_view(view.dimensions(), gen);
                        boost::gil::copy_pixels(boost::gil::color_converted_view<pixel_type>(src, channel_conv), view);
                        break;
                    }

                    default:
                        //TODO: Normalize exceptions
                        LIMA_THROW_EXCEPTION(std::runtime_error("Invalid generator mode"));
                    }
                },
                lima::view(res));

            return res;
        }

        init_params m_init_params;
        exec_params m_generator_params;
    };
} // namespace detectors::simulator::generator
} // namespace lima
