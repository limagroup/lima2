// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <future>
#include <string>
#include <thread>

#include <lima/hw/info.describe.hpp>
#include <lima/hw/capabilities.describe.hpp>
#include <lima/hw/status.describe.hpp>

#include "control_impl.hpp"

using namespace std::string_literals;

namespace lima
{
namespace detectors::simulator
{
    std::string control_impl::send_cmd(std::string cmd)
    {
        // simulate camera command
        return "yes my Lord"s;
    }

    void control_impl::hw_prepare(acq_params_t const& acq_params)
    {
        // Validate parameters

        // State machine
        m_trigger_mode = acq_params.acq.trigger_mode;

        // Simulate camera preparation
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    void control_impl::hw_start() { m_trigger_count = 0; }

    void control_impl::hw_trigger()
    {
        bool is_final = false;
        boost::mpi::communicator world;

        // Send the trigger to one receiver
        int rcv_idx = m_trigger_count % (world.size() - 1) + 1;
        LIMA_LOG(trace, ctl) << "Send trigger to receiver " << rcv_idx;

        // Received in acquisition_impl::hw_get_frame()
        world.send(rcv_idx, 0, is_final);

        m_trigger_count += 1;
    }

    void control_impl::hw_stop() {}

    void control_impl::hw_close() {}

    void control_impl::hw_reset() {}

    hw::info control_impl::det_info() const
    {
        hw::info res;
        res.plugin = "Simulator";
        res.model = "Top Model";
        res.pixel_size = {100, 100};
        res.dimensions = {m_init_params.generator.width, m_init_params.generator.height};

        return res;
    }

    hw::status control_impl::det_status() const
    {
        hw::status res;

        res.humidity = 42.0;
        res.temperature = 37.2;
        res.high_voltage_state = m_hv_on_off ? "ON" : "OFF";
        res.state = "Ready";

        auto now = std::chrono::system_clock::now();
        res.time = fmt::format("{:%FT%TZ}", std::chrono::floor<std::chrono::seconds>(now));

        return res;
    }

    hw::capabilities control_impl::det_capabilities() const
    {
        using namespace std::chrono_literals;

        hw::capabilities res;
        res.expo_time_range = {1us, 10s};
        res.latency_time_range = {150ns, 1s};
        res.frame_time_range = {10us, 3600s};
        res.trigger_modes = {trigger_mode_enum::internal, trigger_mode_enum::software};
        res.nb_frames_per_trigger_range = {1, 12345};

        return res;
    }

    int control_impl::nb_frames_acquired() const
    {
        return 0; // TODO
    }
} // namespace detectors::simulator
} // namespace lima
