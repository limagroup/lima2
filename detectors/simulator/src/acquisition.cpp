// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "acquisition_impl.hpp"

namespace lima
{
namespace detectors::simulator
{
    /// Implementation of the acquisition
    class acquisition::impl : public acquisition_impl
    {
        using parent_t = acquisition_impl;

        using parent_t::parent_t;
    };

    // Pimpl boilerplate

    acquisition::acquisition(std::pmr::polymorphic_allocator<std::byte> alloc) : m_pimpl{std::make_unique<impl>(alloc)}
    {
    }

    acquisition::acquisition(acquisition&&) = default;

    acquisition::~acquisition() = default;

    acquisition& acquisition::operator=(acquisition&&) = default;

    int acquisition::world_rank() const { return m_pimpl->world_rank(); }

    int acquisition::recv_rank() const { return m_pimpl->recv_rank(); }

    acquisition::acq_info_t acquisition::prepare_acq(acq_params_t const& acq_params)
    {
        return m_pimpl->prepare(acq_params);
    }

    void acquisition::start_acq() { m_pimpl->start(); }

    void acquisition::stop_acq() { m_pimpl->stop(); }

    void acquisition::close_acq() { m_pimpl->close(); }

    void acquisition::reset_acq() { m_pimpl->reset(); }

    int acquisition::nb_frames_xferred() const { return m_pimpl->nb_frames_xferred(); }

    acq_state_enum acquisition::state() const { return m_pimpl->state(); }

    void acquisition::register_on_state_change(std::function<void(acq_state_enum)> cbk)
    {
        m_pimpl->register_on_state_change(cbk);
    }

    void acquisition::register_on_start_acq(std::function<void()> cbk) { m_pimpl->register_on_start_acq(cbk); }

    void acquisition::register_on_frame_ready(std::function<void(data_t)> cbk)
    {
        m_pimpl->register_on_frame_ready(cbk);
    }

    void acquisition::register_on_end_acq(std::function<void(int)> cbk) { m_pimpl->register_on_end_acq(cbk); }

} // namespace detectors::simulator
} // namespace lima
