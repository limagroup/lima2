// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <future>
#include <string>
#include <thread>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/algorithm.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/core/frame_view.hpp>

#include "acquisition_impl.hpp"
#include "generator.hpp"
#include "loader.hpp"

using namespace std::string_literals;

namespace gil = boost::gil;

namespace boost
{
template <class Tag, class T>
class error_info;

using errinfo_frame_idx = error_info<struct errinfo_frame_idx_, int>;

} // namespace boost

namespace lima
{
namespace detectors::simulator
{
    acquisition_impl::acquisition_impl(allocator_t const& alloc) : m_alloc(alloc), m_current_frame(m_buffer.end())
    {
        bcast_det_info();
    }

    config::acq_info_t acquisition_impl::hw_prepare(acq_params_t const& acq_params)
    {
        LIMA_LOG_SCOPE("hw_prepare")
        LIMA_LOG(trace, det) << fmt::format("acq_params = {}", acq_params);

        // Preconditions for detector params

        if (acq_params.det.nb_prefetch_frames < 1)
            LIMA_THROW_EXCEPTION(lima::invalid_argument("nb_prefetch_frames is less than 1"));

        using any_source_t = std::variant<generator::generator, loader::loader>;

        auto any_src = [init_params = m_init_params, acq_params]() -> any_source_t {
            if (acq_params.det.image_source == image_source_enum::generator) {
                if (acq_params.det.generator.gauss.peaks.empty())
                    LIMA_LOG(warning, det) << "Generator configured with NO peaks";
                return generator::generator(init_params.generator, acq_params.det.generator);
            } else {
                int rank = boost::mpi::communicator().rank() - 1;
                return loader::loader(acq_params.det.loader, rank);
            }
        }();

        point_t image_dims = std::visit([](auto& src) { return src.dimensions(); }, any_src);
        size_t nb_channels = std::visit([](auto& src) { return src.nb_channels(); }, any_src);
        pixel_enum pixel_type = std::visit([](auto& src) { return src.pixel_type(); }, any_src);

        rectangle_t roi{{0, 0}, image_dims}; // A full frame ROI by default

        // Preconditions for ROI params
        if (!acq_params.img.roi.is_degenerated()) {
            if (!within(acq_params.img.roi, roi))
                LIMA_THROW_EXCEPTION(lima::invalid_argument("ROI is not within the detector extend"));
            else
                roi = acq_params.img.roi;
        }

        // Preconditions for Binning params
        if ((acq_params.img.binning.x < 1) || (acq_params.img.binning.y < 1))
            LIMA_THROW_EXCEPTION(lima::invalid_argument("Binning is less than 1"));

        if ((roi.dimensions.x % acq_params.img.binning.x) || (roi.dimensions.y % acq_params.img.binning.y))
            LIMA_THROW_EXCEPTION(lima::invalid_argument("Frame dimension (or ROI) is not a multiple of binning"));

        m_nb_frames = acq_params.acq.nb_frames;
        bool endless = (m_nb_frames == 0);
        m_nb_recv_frames = m_nb_frames / nb_receivers() + (m_nb_frames % nb_receivers() > recv_rank() ? 1 : 0);
        LIMA_LOG(trace, det) << "endless=" << endless << ", nb_recv_frames=" << m_nb_recv_frames;

        int nb_prefetch_frames = acq_params.det.nb_prefetch_frames;
        int nb_recv_prefetch_frames =
            nb_prefetch_frames / nb_receivers() + (nb_prefetch_frames % nb_receivers() > recv_rank() ? 1 : 0);
        bool need_prefetch = (endless || (m_nb_recv_frames > 0));
        LIMA_LOG(trace, det) << "nb_recv_prefetch_frames=" << nb_recv_prefetch_frames << ", "
                             << "need_prefetch=" << need_prefetch;
        if (need_prefetch && (nb_recv_prefetch_frames == 0)) {
            std::ostringstream err;
            err << "Receiver " << recv_rank() << ": not enough nb_prefetch_frames: need at least one frame";
            LIMA_THROW_EXCEPTION(lima::invalid_argument(err.str()));
        }

        m_expo_time = acq_params.acq.expo_time;
        m_latency_time = acq_params.acq.latency_time;
        m_trigger_mode = acq_params.acq.trigger_mode;
        m_nb_frames_per_trigger = acq_params.acq.nb_frames_per_trigger;

        m_dropped_frame_idx = acq_params.det.dropped_frame_idx;
        m_failed_frame_idx = acq_params.det.failed_frame_idx;

        // Resize the circular buffer
        if (!endless)
            m_buffer.resize(std::min(m_nb_recv_frames, nb_recv_prefetch_frames));
        else
            m_buffer.resize(nb_recv_prefetch_frames);

        auto apply_hw_image_proc = [roi, bin = acq_params.img.binning, flip = acq_params.img.flip](auto const& src,
                                                                                                   auto const& dst) {
            // ROI -> Binning -> Flip
            //
            // Compute the frame dimensions
            point_t binned_roi_dims(roi.dimensions.x / bin.x, roi.dimensions.y / bin.y);

            using SrcView = std::decay_t<decltype(src)>;
            SrcView src_roi(binned_roi_dims, src.xy_at(roi.topleft));

            // TODO: perform image binning

            switch (flip) {
            case flip_enum::left_right:
                gil::copy_pixels(gil::flipped_left_right_view(src), dst);
                break;
            case flip_enum::up_down:
                gil::copy_pixels(gil::flipped_up_down_view(src), dst);
                break;
            default:
                gil::copy_pixels(src, dst);
            }
        };

        std::visit(
            [&](auto&& src) {
                int frame_idx = recv_rank();
                for (auto&& frm : m_buffer) {
                    // Resize the frame
                    frm.recreate(roi.dimensions, nb_channels, pixel_type);

                    LIMA_LOG(trace, det) << "Generating frame " << frame_idx;

                    // Fill each channels of the frame (with the same data for now)
                    for (size_t n = 0; n < nb_channels; n++)
                        boost::variant2::visit([&](auto&& src, auto&& dst) { apply_hw_image_proc(src, dst); },
                                               lima::const_view(src.get_frame(frame_idx)), lima::view(frm, n));

                    // Increment frame index
                    frame_idx += nb_receivers();
                }
            },
            any_src);

        // Reset the current frame iterator
        m_current_frame = m_buffer.begin();

        return {roi.dimensions, nb_channels, pixel_type};
    }

    void acquisition_impl::hw_start() { m_nb_generated_frames = 0; }

    void acquisition_impl::hw_stop()
    {
        if (m_trigger_mode == trigger_mode_enum::software) {
            // Broadcast a final trigger to itself to unblock hw_get_frame
            bool is_final = true;
            boost::mpi::communicator world;
            world.send(world.rank(), 0, is_final);
        }
    }

    void acquisition_impl::hw_close() {}

    /// Get image view
    config::data_t acquisition_impl::hw_get_frame()
    {
        data_t res;

        LIMA_LOG(trace, det) << "Getting frame " << m_nb_generated_frames << "/"
                             << (m_nb_recv_frames > 0 ? std::to_string(m_nb_recv_frames) : "inf");

        if (m_nb_frames && !(m_nb_generated_frames < m_nb_recv_frames)) {
            LIMA_LOG(trace, det) << "Returning final frame";

            res.metadata.is_final = true;
            return res;
        }

        // If trigger is software or (simulated) external
        bool is_final = false;
        if ((m_trigger_mode == trigger_mode_enum::software) && !(m_nb_generated_frames % m_nb_frames_per_trigger)) {
            LIMA_LOG(trace, det) << "Waiting for trigger from control";

            // Wait for trigger from control
            boost::mpi::communicator world;
            world.recv(boost::mpi::any_source, 0, is_final);

            LIMA_LOG(trace, det) << "Got" << (is_final ? " FINAL" : "") << " trigger from control";
        } else {
            // Wait for the other RR receivers, the first iteration only those before
            int other_rr_recvs = m_nb_generated_frames ? (nb_receivers() - 1) : recv_rank();
            if (other_rr_recvs)
                std::this_thread::sleep_for((m_expo_time + m_latency_time) * other_rr_recvs);
        }

        // Simulate dropped frame
        if (m_dropped_frame_idx && (m_nb_generated_frames == m_dropped_frame_idx)) {
            LIMA_LOG(warning, det) << "Dropped frame " << m_dropped_frame_idx;

            // Step to next frame
            next_frame();
        }

        // Simulate failed frame
        if (m_failed_frame_idx && (m_nb_generated_frames == m_failed_frame_idx))
            LIMA_THROW_EXCEPTION(lima::runtime_error("Failed to get frame")
                                 << boost::errinfo_frame_idx(m_failed_frame_idx));

        // Simulate exposure time
        std::this_thread::sleep_for(m_expo_time);

        // Fill the frame
        assert(m_current_frame != m_buffer.end());
        res = *m_current_frame;
        res.metadata.idx = recv_rank() + m_nb_generated_frames * nb_receivers();
        res.metadata.is_final = is_final;

        // Step to next frame
        next_frame();

        // Simulate readout latency
        std::this_thread::sleep_for(m_latency_time);

        return res;
    }

    void acquisition_impl::next_frame()
    {
        // Increment the number of generated images
        m_nb_generated_frames++;

        // Increment the current frame iterator
        m_current_frame++;
        if (m_current_frame == m_buffer.end())
            m_current_frame = m_buffer.begin();
    }

} // namespace detectors::simulator
} // namespace lima
