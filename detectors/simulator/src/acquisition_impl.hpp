// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory>

#include <fmt/chrono.h>
#include <fmt/ranges.h>
#include <fmt/describe.hpp>
#include <fmt/filesystem.hpp>
#include <fmt/uuid.hpp>

#include <lima/hw/acquisition_fsm.hpp>
#include <lima/hw/acquisition_mpi.hpp>
#include <lima/hw/acquisition_thread.hpp>

#include <lima/detectors/simulator/config.hpp>
#include <lima/detectors/simulator/acquisition.describe.hpp>

#include <lima_simulator_export.h>

#if defined(LIMA_HAS_PROPAGATE_CONST)
#include <experimental/propagate_const>
#endif

namespace lima
{
namespace detectors::simulator
{
    // Acquisition part of the detector
    class acquisition_impl : public hw::acquisition_init_mpi<acquisition_impl, config>, //
                             public hw::acquisition_thread<acquisition_impl, config>,   //
                             public hw::acquisition_fsm<acquisition_impl, config>       //
    {
        // Necessary to keep the implementation private
        using base_t = hw::acquisition_thread<acquisition_impl, config>;
        friend base_t;

        using allocator_t = std::pmr::polymorphic_allocator<std::byte>;

        using init_params_t = typename config::init_params_t;
        using acq_params_t = typename config::acq_params_t;
        using det_info_t = typename config::det_info_t;
        using acq_info_t = typename config::acq_info_t;
        using data_t = typename config::data_t;

      public:
        acquisition_impl(allocator_t const& alloc);

      private:
        /// Private implementation
        /// \{
        /// Prepare the acquisition (e.g. allocate buffers)
        acq_info_t hw_prepare(acq_params_t const& acq_params);

        /// Start acquisition loop
        void hw_start();

        /// Stop acquisition loop
        void hw_stop();

        /// Close acquisition
        void hw_close();

        /// Get acquisition info from the latest prepare
        acq_info_t hw_acq_info();

        /// Get frame
        data_t hw_get_frame();
        /// }

        // Step to next frame
        void next_frame();

        // config::init_params_t m_init_params;
        // config::det_info_t m_det_info;

        allocator_t m_alloc;

        int m_nb_frames;                          //<! The total number of frames
        int m_nb_recv_frames;                     //<! The number of expected frames for this receiver
        std::chrono::microseconds m_expo_time;    //<! The exposure time
        std::chrono::microseconds m_latency_time; //<! The latency time
        trigger_mode_enum m_trigger_mode;         //<! The current trigger mode
        int m_nb_frames_per_trigger;              //<! The number of frames per trigger

        int m_dropped_frame_idx = 0; //<! The number of frames acquired before frame dropped
        int m_failed_frame_idx = 0;  //<! The number of frames acquired before frame failed

        using circular_buffer_t = std::vector<data_t>;

        circular_buffer_t m_buffer;                        //<! A circular buffer of generated frames
        circular_buffer_t::const_iterator m_current_frame; //<! The current frame iterator in the circular buffer
        int m_nb_generated_frames = 0;                     //<! Keep track of the number of generated frames
    };

} // namespace detectors::simulator
} // namespace lima
