// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

#include <fmt/chrono.h>
#include <fmt/ranges.h>
#include <fmt/describe.hpp>
#include <fmt/filesystem.hpp>
#include <fmt/uuid.hpp>

#include <lima/hw/control_fsm.hpp>
#include <lima/hw/control_mpi.hpp>

#include <lima/project_version.h>

#include <lima/detectors/simulator/config.hpp>
#include <lima/detectors/simulator/control.describe.hpp>

#include <lima_simulator_export.h>

#if defined(LIMA_HAS_PROPAGATE_CONST)
#include <experimental/propagate_const>
#endif

namespace lima
{
namespace detectors::simulator
{
    // Control part of the detector
    class control_impl : public hw::control_init_mpi<control_impl, config>, //
                         public hw::control_fsm<control_impl, config>       //
    {
        // Necessary to keep the implementation private
        using base_t = hw::control_fsm<control_impl, config>;
        friend base_t;

        using init_params_t = typename config::init_params_t;
        using acq_params_t = typename config::acq_params_t;
        using det_info_t = typename config::det_info_t;
        using det_capabilities_t = hw::capabilities;
        using det_status_t = hw::status;

      public:
        control_impl(init_params_t const& init_params) : m_init_params(init_params), control_init_mpi(init_params)
        {
            bcast_det_info();
        }

        /// My camera specific command
        std::string send_cmd(std::string cmd);

        /// Returns the number of frames already acquired
        int nb_frames_acquired() const;

        /// General Info
        /// \{
        /// Returns the layout of the detector
        void hw_layout();

        /// Returns (static) information about the hardware
        det_info_t det_info() const;

        /// Returns hardware capabilities
        det_capabilities_t det_capabilities() const;

        /// Returns the status of the detector
        det_status_t det_status() const;

        /// Returns the version of the camera plugin
        std::string version() const { return LIMA_VERSION; }
        /// }

        /// \{
        /// \name Detector specific
        /// Initialize the detector head
        void initialize()
        {
            using namespace std::chrono_literals;
            LIMA_LOG(info, det) << "Initializing detector...";
            std::this_thread::sleep_for(1s);
            LIMA_LOG(info, det) << "Initialized detector.";
        }

        /// Set HV on/off
        void high_voltage(bool on_off)
        {
            LIMA_LOG(info, det) << "high_voltage turned " << (on_off ? "ON" : "OFF");
            m_hv_on_off = on_off;
        }

        /// Reutnr the current HV on/off state
        bool high_voltage() const
        {
            LIMA_LOG(info, det) << "high_voltage currently implemented " << (m_hv_on_off ? "ON" : "OFF");
            return m_hv_on_off;
        }
        /// }

      private:
        /// Private implementation
        /// \{
        /// Validate parameters
        bool hw_validate_acq_params(acq_params_t const& acq_params) const { return true; }

        /// Prepare acquisition
        void hw_prepare(acq_params_t const& acq_params);

        /// Start acquisition
        void hw_start();

        // Software trigger if the camera supports it
        void hw_trigger();

        /// Stop acquisition
        void hw_stop();

        /// Close acquisition
        void hw_close();

        /// Reset detector
        void hw_reset();
        /// }

        config::init_params_t m_init_params;

        hw::info m_hw_info;
        hw::capabilities m_hw_cap;

        trigger_mode_enum m_trigger_mode;

        unsigned int m_trigger_count = 0; //!< Number of triggers since start (used to distribute among receivers)

        bool m_hv_on_off = false;
    };

} // namespace detectors::simulator
} // namespace lima
