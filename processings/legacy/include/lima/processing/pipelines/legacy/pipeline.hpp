// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include <lima/core/frame.hpp>
#include <lima/core/frame_info.hpp>

#include <lima/hw/info.hpp>
#include <lima/hw/params.hpp>

#include <lima/processing/nodes/roi_counters_result.hpp>
#include <lima/processing/nodes/roi_profiles_result.hpp>
#include <lima/processing/pipelines/legacy/params.hpp>

#include <pipeline_legacy_export.h>

namespace lima
{
namespace processing::pipelines
{
    namespace legacy
    {
        enum class state_enum
        {
            idle,
            prepared,
            running,
            fault
        };

        /// Progress counters
        struct counters
        {
            int nb_frames_source = 0;
            int nb_frames_input = 0;
            int nb_frames_processed = 0;
            int nb_frames_counters = 0;
            int nb_frames_profiles = 0;
            int nb_frames_saved = 0;
        };

        using finished_callback_t = std::function<void(std::optional<std::string>)>;

        class PIPELINE_LEGACY_EXPORT pipeline
        {
          public:
            static constexpr char const* const uid = "classic";

            using frame_info_t = frame_info;
            using proc_params_t = proc_params;
            using acq_params_t = hw::acquisition_params;
            using det_info_t = hw::info;
            using progress_counters_t = counters;
            using input_t = frame;

            pipeline(frame_info_t const& frame_info, proc_params_t const& proc_params, acq_params_t const& acq_params,
                     det_info_t const& det_info);
            ~pipeline();          // defined in the implementation file, where impl is a complete type
            pipeline(pipeline&&); // defined in the implementation file
            pipeline(const pipeline&) = delete;
            pipeline& operator=(pipeline&&); // defined in the implementation file
            pipeline& operator=(const pipeline&) = delete;

            /// Activate the processing (start poping data from the queue)
            void activate();

            /// Return the finished state
            bool is_finished() const;

            /// Register on_finished callback
            void register_on_finished(finished_callback_t on_finished);

            /// Abort the pipeline
            void abort();

            /// Process a frame
            void process(input_t const& frm);

            /// Process multiple frames
            void process(std::vector<input_t> const& frms)
            {
                for (auto&& frm : frms)
                    process(frm);
            }

            /// Returns the progress counters
            progress_counters_t progress_counters() const;

            /// Pop the available ROI statistics
            std::vector<roi_counters_result> pop_roi_statistics();

            /// Returns the size of the ROI statistics buffer
            std::size_t nb_roi_statistics() const;

            /// Pop the available ROI profiles
            std::vector<roi_profiles_result> pop_roi_profiles();

            /// Returns the size of the ROI statistics buffer
            std::size_t nb_roi_profiles() const;

            /// Returns frame for the given index
            std::optional<frame> get_frame(std::size_t frame_idx = -1) const;
            std::optional<frame> get_input_frame(std::size_t frame_idx = -1) const;

            /// Returns the frame info at various stage of the pipeline
            frame_info_t input_frame_info() const;
            frame_info_t processed_frame_info() const;

            /// Returns the current state of the pipeline
            state_enum state() const;

            /// Returns the version of the pipeline plugin
            std::string version() const;

          private:
            class impl;

#if defined(LIMA_HAS_PROPAGATE_CONST)
            std::experimental::propagate_const< // const-forwarding pointer wrapper
                std::unique_ptr<                // unique-ownership opaque pointer
                    impl>>
                m_pimpl; // to the forward-declared implementation class
#else
            std::unique_ptr< // unique-ownership opaque pointer
                impl>
                m_pimpl; // to the forward-declared implementation class
#endif
        };
    } // namespace legacy
} // namespace processing::pipelines
} // namespace lima
