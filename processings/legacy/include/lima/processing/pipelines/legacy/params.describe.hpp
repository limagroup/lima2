// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/arc.describe.hpp>
#include <lima/core/rectangle.describe.hpp>
#include <lima/hw/params.describe.hpp>
#include <lima/hw/info.describe.hpp>
#include <lima/io/h5/params.describe.hpp>

#include <lima/processing/pipelines/legacy/params.hpp>

// This file is part of the user interface of the library and should not include any private dependencies
// or other thing that expose implementation details

namespace lima
{
namespace processing::pipelines
{
    namespace legacy
    {
        BOOST_DESCRIBE_STRUCT(fifo_params, (), (nb_fifo_frames))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(fifo_params, nb_fifo_frames,
            (desc, "number of frames in FIFO"),
            (doc, "The number of frames in the Processing FIFO"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(thread_params, (), (nb_threads, numa_node))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(thread_params, nb_threads,
            (desc, "number of threads"),
            (doc, "The number of threads in the Processing"))

        BOOST_ANNOTATE_MEMBER(thread_params, numa_node,
            (desc, "numa node"),
            (doc, "The numa node of Processing FIFO"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(buffer_params, (),
                              (nb_roi_statistics_buffer, nb_roi_profiles_buffer, nb_frames_buffer,
                               nb_input_frames_buffer))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(buffer_params, nb_roi_statistics_buffer,
            (desc, "nb roi statistics in buffer"),
            (doc, "The initial capacity of ROI statistics buffer"))

        BOOST_ANNOTATE_MEMBER(buffer_params, nb_roi_profiles_buffer,
            (desc, "nb roi profiles in buffer"),
            (doc, "The initial capacity of ROI profiles buffer"))

        BOOST_ANNOTATE_MEMBER(buffer_params, nb_frames_buffer,
            (desc, "number of frames in buffer"),
            (doc, "The number of frames in the Circular buffer"))

        BOOST_ANNOTATE_MEMBER(buffer_params, nb_input_frames_buffer,
            (desc, "number of input frames in buffer"),
            (doc, "The number of input frames in the Circular buffer"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(geometry_params, (), (enabled, roi, rotation, flip))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(geometry_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the geometry correction"))

        BOOST_ANNOTATE_MEMBER(geometry_params, roi,
            (desc, "roi"),
            (doc, "Rectangular ROI for cropping"))

        BOOST_ANNOTATE_MEMBER(geometry_params, rotation,
            (desc, "rotation"),
            (doc, "Rotation [none, r90cw, r180, r90ccw] "))

        BOOST_ANNOTATE_MEMBER(geometry_params, flip,
            (desc, "flip"),
            (doc, "Flip [none, left_right, up_down"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(accumulation_params, (), (enabled, nb_frames, pixel_type))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(accumulation_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the accumulation"))

        BOOST_ANNOTATE_MEMBER(accumulation_params, nb_frames,
            (desc, "nb frames"),
            (doc, "The number of frames to accumulate"))

        BOOST_ANNOTATE_MEMBER(accumulation_params, pixel_type,
            (desc, "pixel type"),
            (doc, "The output pixel type of the accumulated frame"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(roi_statistics_params, (), (enabled, rect_rois, arc_rois))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(roi_statistics_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the ROI statistics"))

        BOOST_ANNOTATE_MEMBER(roi_statistics_params, rect_rois,
            (desc, "rectangular rois"),
            (doc, "A vector of rectangular ROIs for the statistics"))

        BOOST_ANNOTATE_MEMBER(roi_statistics_params, arc_rois,
            (desc, "arc rois"),
            (doc, "A vector of arc ROIs for the statistics"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(roi_profiles_params, (), (enabled, rois, directions))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(roi_profiles_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the ROI statistics"))

        BOOST_ANNOTATE_MEMBER(roi_profiles_params, rois,
            (desc, "profiles"),
            (doc, "A vector of rectangular ROIs"))

        BOOST_ANNOTATE_MEMBER(roi_profiles_params, directions,
            (desc, "directions"),
            (doc, "A vector of direction of the profiles"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(mask_params, (), (enabled, path))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(mask_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the mask"))

        BOOST_ANNOTATE_MEMBER(mask_params, path,
            (desc, "path"),
            (doc, "The path to the mask image [masked value = 1]"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(flatfield_params, (), (enabled, path))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(flatfield_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the flatfield correction"))

        BOOST_ANNOTATE_MEMBER(flatfield_params, path,
            (desc, "path"),
            (doc, "The path to the flatfield image"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(background_params, (), (enabled, path))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(background_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the background substraction"))

        BOOST_ANNOTATE_MEMBER(background_params, path,
            (desc, "path"),
            (doc, "The path to the background image"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(saving_params, (io::h5::saving_params), (enabled))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(saving_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the saving stream"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(proc_params, (),
                              (fifo, thread, geometry, accumulation, buffers, mask, flatfield, background, statistics,
                               profiles, saving, det_info))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(proc_params, fifo,
            (desc, "fifo parameters"),
            (doc, "The processing FIFO parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, thread,
            (desc, "thread parameters"),
            (doc, "The processing thread parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, buffers,
            (desc, "buffers parameters"),
            (doc, "The size of the circular buffers"))

        BOOST_ANNOTATE_MEMBER(proc_params, geometry,
            (desc, "geometry parameters"),
            (doc, "The geometry correction"))

        BOOST_ANNOTATE_MEMBER(proc_params, accumulation,
            (desc, "geometry parameters"),
            (doc, "The geometry correction"))
            
        BOOST_ANNOTATE_MEMBER(proc_params, mask,
            (desc, "mask parameters"),
            (doc, "The mask processing parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, flatfield,
            (desc, "flatfield parameters"),
            (doc, "The flatfield correction parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, background,
            (desc, "background parameters"),
            (doc, "The background substraction parameters"))
                
        BOOST_ANNOTATE_MEMBER(proc_params, statistics,
            (desc, "roi statistics parameters"),
            (doc, "The ROI statistics parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, profiles,
            (desc, "roi profiles parameters"),
            (doc, "The ROI profiles parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, saving,
            (desc, "saving parameters"),
            (doc, "The HDF5 saving parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, det_info,
            (desc, "detector info"),
            (doc, "The detector information"))
        // clang-format on

    } // namespace legacy
} // namespace processing::pipelines
} // namespace lima
