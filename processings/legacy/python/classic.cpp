// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <sstream>
#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/filesystem.hpp>
#include <fmt/ranges.h>
#include <fmt/describe.hpp>

#include <boost/mp11.hpp>

#include <lima/processing/pipelines/legacy.describe.hpp>

namespace py = pybind11;
namespace describe = boost::describe;

PYBIND11_MODULE(classic, m)
{
    using namespace pybind11::literals;
    using namespace lima::processing::pipelines::legacy;

    m.doc() = "CLASSIC pipeline plugin"; // optional module docstring

    using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

    auto described_class = [](auto& class_) {
        using type_t = typename std::decay_t<decltype(class_)>::type;
        static_assert(boost::describe::has_describe_members<type_t>::value);

        // Constructor
        class_.def(py::init<>());

        // Member accessors
        boost::mp11::mp_for_each<
            describe::describe_members<type_t, describe::mod_any_access | describe::mod_inherited>>([&class_](auto D) {
            using A = boost::describe::annotate_member<decltype(D)>;
            auto doc = boost::describe::annotation_by_name_v<A, Doc>;
            class_.def_readwrite(D.name, D.pointer);
        });

        // Repr
        class_.def("__repr__", [&class_](type_t const& t) { return fmt::format("{}", t); });
    };

    // Automatic binding for described enums
    auto described_enum = [](auto& enum_) {
        using type_t = typename std::decay_t<decltype(enum_)>::type;

        boost::mp11::mp_for_each<boost::describe::describe_enumerators<type_t>>(
            [&](auto D) { enum_.value(D.name, D.value); });
        enum_.export_values();
    };

    py::class_<pipeline::proc_params_t> proc_params(m, "Params");
    described_class(proc_params);

    // Define the saving_params params
    py::class_<saving_params> saving_params(m, "SavingParams");
    described_class(saving_params);

    py::class_<roi_statistics_params> roi_statistics_params(m, "RoiStatisticsParams");
    described_class(roi_statistics_params);

    //Bind the xpcs pipeline class
    py::class_<pipeline> proc(m, "Pipeline");
    proc.def(py::init<const pipeline::frame_info_t&, const pipeline::proc_params_t&, const pipeline::acq_params_t&,
                      const pipeline::det_info_t&>(),
             "frame_info"_a, "proc_params"_a, "acq_params"_a, "det_info"_a)
        .def("activate", &pipeline::activate, "Activate the pipeline")
        .def("abort", &pipeline::abort, "Abort the pipeline")
        .def("process", py::overload_cast<pipeline::input_t const&>(&pipeline::process), "Process a frame", "frame"_a)
        .def_property_readonly("is_finished", &pipeline::is_finished, "Returns True if pipeline is finished")
        .def("register_on_finished", &pipeline::register_on_finished, "Register a callack for on finished event")
        .def_property_readonly("progress_counters", &pipeline::progress_counters, "Returns the progress counters")
        .def_property_readonly("input_frame_info", &pipeline::input_frame_info, "Input frame info")
        .def_property_readonly("processed_frame_info", &pipeline::processed_frame_info, "Processed frame info")
        .def_property_readonly("version", &pipeline::version, "Returns version informations");

    //Bind the xpcs counters class
    py::class_<counters> counters(m, "Counters");
    described_class(counters);
}
