import time
import numpy as np

from lima import Frame, FrameInfo, Pixel, FileExistsPolicy
from classic import Pipeline, Params

params = Params()

# Saving params
params.saving.file_exists_policy = FileExistsPolicy.overwrite
params.saving.nb_frames_per_file = 10
params.saving.enabled = True

frame_info = FrameInfo(width=2048, height=1024, nb_channels=1, pixel=Pixel.gray16)

classic = Pipeline(frame_info, params)

def on_finished(error):
    if error:
        print(f"ERROR! {error}")
    else:
        print(f"FINISHED!")
        

classic.register_on_finished(on_finished)

classic.activate()

nb_frames = 20

for i in range(0, nb_frames):
    frm = Frame(width=2048, height=1024, nb_channels=1, pixel=Pixel.gray16)
    data = np.array(frm, copy=False)
    #np.ndarray.fill(data, i, dtype=np.uint16)
    data.fill(i)

    frm.metadata.idx = i
    frm.metadata.recv_idx = i
    frm.metadata.is_final = False if i < (nb_frames - 1) else True

    classic.process(frm)

    print(f"{classic.progress_counters}")

while not classic.is_finished:
    time.sleep(1)
