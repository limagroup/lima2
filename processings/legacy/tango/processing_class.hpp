// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>
#include <string>

#include <lima/tango/processing_class.hpp>

#include "processing.hpp"

namespace lima::tango::legacy
{
/// The processing_class singleton definition
class BOOST_SYMBOL_VISIBLE processing_class : public tango::processing_class<processing_class>
{
  public:
    using base_t = tango::processing_class<processing_class>;
    using processing_t = lima::processing::pipelines::legacy::pipeline;
    using device_t = processing;

    template <typename T1, typename T2>
    friend class lima::tango::singleton_device_class; // To give access to protected constuctor

  protected:
    processing_class(std::string const& name) : base_t(name)
    {
        TANGO_LOG_INFO << "Entering processing_class constructor" << std::endl;

        set_default_property();
        write_class_property();

        TANGO_LOG_INFO << "Leaving processing_class constructor" << std::endl;
    }

    /// Create the command object(s) and store them in the command list
    void command_factory() override;

    /// Create the attribute object(s) and store them in the attribute list
    void attribute_factory(std::vector<Tango::Attr*>&) override;

    /// Create the pipe object(s) and store them in the pipe list
    void pipe_factory() override;

    /// Properties management
    ///{

    ///  Set class description fields as property in database
    void write_class_property();

    /// Set default property (class and device) for wizard.
    /// For each property, add to wizard property name and description.
    /// If default value has been set, add it to wizard property and
    /// store it in a DbDatum.
    void set_default_property();

    ///}

    /// Create a device object
    processing_device* create_device(std::string const& instance_name) override;
};

} // namespace lima::tango::legacy
