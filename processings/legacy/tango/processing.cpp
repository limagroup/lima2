// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/json/filesystem.hpp>
#include <boost/json/describe.hpp>
#include <boost/json/chrono.hpp>
#include <boost/json/parse.hpp>

#include <lima/log/severity_level.describe.hpp>
#include <lima/log/logger.hpp>
#include <lima/hw/params.describe.hpp>
#include <lima/utils/type_traits.hpp>

#include <lima/tango/attribute_lock_guard.hpp>
#include <lima/tango/convert.hpp>
#include <lima/tango/data_array.hpp>

#include "processing.hpp"

namespace lima::tango::legacy
{
void processing::init_device()
{
    DEBUG_STREAM << "processing::init_device() create device " << device_name << std::endl;

    // Get the device properties from database
    // Device property data members
    int rank = 0;

    Tango::DbData db_data;
    db_data.push_back(Tango::DbDatum("frame_info"));
    db_data.push_back(Tango::DbDatum("proc_params"));
    db_data.push_back(Tango::DbDatum("acq_params"));
    db_data.push_back(Tango::DbDatum("det_info"));

    get_db_device()->get_property(db_data);

    //// Decode plugin params
    std::string json;

    db_data[0] >> json;
    frame_info_t frame_info = boost::json::value_to<frame_info_t>(boost::json::parse(json));

    db_data[1] >> json;
    proc_params_t proc_params = boost::json::value_to<proc_params_t>(boost::json::parse(json));

    db_data[2] >> json;
    acq_params_t acq_params = boost::json::value_to<acq_params_t>(boost::json::parse(json));

    db_data[3] >> json;
    det_info_t det_info = boost::json::value_to<det_info_t>(boost::json::parse(json));

    m_proc = std::make_unique<processing_t>(frame_info, proc_params, acq_params, det_info);
    if (!m_proc)
        Tango::Except::throw_exception("LIMA_Exception", "Failed to construct processing", "processing::prepare");

    // Register callbacks
    m_proc->register_on_finished([this](auto error) {
        //log4tango::Logger* log = dev->get_logger();

        if (error) {
            DEBUG_STREAM << "processing FAILED " << *error << std::endl;

            // Update property
            m_last_error = *error;

            // Puesh event
            Tango::DevString* last_error = new Tango::DevString;
            *last_error = Tango::string_dup(error->c_str());

            {
                attribute_lock_guard lock(this->get_device_attr()->get_attr_by_name("last_error"));
                this->push_data_ready_event("last_error");
            }
        }

        Tango::DevBoolean* is_finished = new Tango::DevBoolean(true);
        try {
            INFO_STREAM << "processing::push_data_ready_event(is_finished)" << std::endl;
            {
                attribute_lock_guard lock(this->get_device_attr()->get_attr_by_name("is_finished"));
                this->push_data_ready_event("is_finished");
            }
            INFO_STREAM << "processing::push_data_ready_event(is_finished) DONE" << std::endl;
        } catch (CORBA::Exception& e) {
            ERROR_STREAM << "processing::push_data_ready_event(is_finished) FAILED" << std::endl;
            Tango::Except::print_exception(e);
        } catch (...) {
            ERROR_STREAM << "processing::push_data_ready_event(is_finished) FAILED" << std::endl;
        }
    });
}

void processing::always_executed_hook()
{
    //DEBUG_STREAM << "processing::always_executed_hook()  " << device_name << std::endl;

    // code always executed before all requests
}

void processing::read_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    //DEBUG_STREAM << "processing::read_attr_hardware(vector<long> &attr_list) entering... " << std::endl;

    // Add your own code
}

void processing::write_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    //DEBUG_STREAM << "processing::write_attr_hardware(vector<long> &attr_list) entering... " << std::endl;

    // Add your own code
}

void processing::add_dynamic_attributes()
{
    // Add your own code to create and add dynamic attributes if any
}

bool processing::is_finished() const
{
    //DEBUG_STREAM << "processing::is_finished() entering... " << std::endl;
    return m_proc->is_finished();
}

std::string processing::last_error() const
{
    //DEBUG_STREAM << "processing::last_error() entering... " << std::endl;
    return m_last_error;
}

Tango::DevEncoded processing::pop_roi_statistics() const
{
    //DEBUG_STREAM << "processing::pop_roi_statistics() entering... " << std::endl;

    struct roi_counter
    {
        int frame_idx;
        int recv_idx;
        float min;
        float max;
        float avg;
        float std;
        double sum;
    };

    try {
        auto stats = m_proc->pop_roi_statistics();

        // Denormalize ROI counters data structure
        std::vector<roi_counter> buffer;
        std::size_t nb_frames = stats.size();
        std::size_t nb_rois = stats.empty() ? 0 : stats.front().rois.size();
        buffer.reserve(nb_frames * nb_rois);
        // For each frame statistics
        for (auto&& stat : stats)
            // For each rois of the frame
            for (auto&& roi : stat.rois)
                buffer.emplace_back(
                    roi_counter{stat.frame_idx, stat.recv_idx, roi.m_min, roi.m_max, roi.m_avg, roi.m_std, roi.m_sum});

        return make_devencoded(buffer);

    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "processing::pop_roi_statistics");
    }
}

Tango::DevEncoded processing::pop_roi_profiles() const
{
    //DEBUG_STREAM << "processing::pop_roi_profiles() entering... " << std::endl;

    struct roi_counter
    {
        int frame_idx;
        int recv_idx;
        float min;
        float max;
        float avg;
        float std;
        double sum;
    };

    try {
        auto profiles = m_proc->pop_roi_profiles();

        // if (profiles.empty())
        //     Tango::Except::throw_exception("LIMA_Exception", "No ROI profiles available",
        //                                    "processing::pop_roi_profiles");

        std::vector<roi_counter> buffer;
        std::size_t nb_frames = profiles.size();
        std::size_t nb_cnts =
            profiles.empty() ? 0
                             : std::accumulate(profiles.front().rois.begin(), profiles.front().rois.end(), 0,
                                               [](std::size_t lhs, lima::processing::reduction_1d_result const& rhs) {
                                                   return lhs + rhs.size();
                                               });

        buffer.reserve(nb_frames * nb_cnts);
        // For each frame statistics
        for (auto&& profile : profiles)
            // For each rois of the frame
            for (auto&& roi : profile.rois) {
                for (auto&& cnt : roi)
                    buffer.push_back(
                        {profile.frame_idx, profile.recv_idx, cnt.m_min, cnt.m_max, cnt.m_avg, cnt.m_std, cnt.m_sum});
            }

        return make_devencoded(buffer);

    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "processing::pop_roi_profiles");
    }
}

Tango::DevEncoded processing::get_frame(std::size_t frame_idx) const
{
    //DEBUG_STREAM << "processing::get_frame() entering... " << std::endl;

    try {
        auto frm = m_proc->get_frame(frame_idx);

        if (frm)
            return make_devencoded(*frm);
        else
            Tango::Except::throw_exception("LIMA_Exception", "Unavailable frame", "processing::get_frame");

    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "processing::get_frame");
    }
}

Tango::DevEncoded processing::get_input_frame(std::size_t frame_idx) const
{
    //DEBUG_STREAM << "processing::get_raw_frame() entering... " << std::endl;

    try {
        auto frm = m_proc->get_input_frame(frame_idx);

        if (frm)
            return make_devencoded(*frm);
        else
            Tango::Except::throw_exception("LIMA_Exception", "Unavailable frame", "processing::get_input_frame");
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "processing::get_input_frame");
    }
}

void processing::add_dynamic_commands()
{
    // Add your own code to create and add dynamic commands if any
}

// Additional Methods

} // namespace lima::tango::legacy
