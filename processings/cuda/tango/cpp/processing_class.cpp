// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/json/chrono.hpp>
#include <boost/json/filesystem.hpp>
#include <boost/json/describe.hpp>
#include <boost/json/schema_from.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/mp11.hpp>

#include <lima/hw/params.describe.hpp>
#include <lima/utils/type_traits.hpp>

#include <lima/tango/base_attribute.hpp>
#include <lima/tango/base_command.hpp>
#include <lima/tango/convert.hpp>

#include <cuda/processing_class.hpp>

namespace lima::tango::cuda
{
processing_class* processing_class::_instance = nullptr;

processing_class::processing_class(std::string name) : device_class(name)
{
    TANGO_LOG_INFO << "Entering processing_class constructor" << std::endl;

    set_default_property();
    write_class_property();

    TANGO_LOG_INFO << "Leaving processing_class constructor" << std::endl;
}

processing_class* processing_class::init(std::string const& name)
{
    if (_instance == nullptr) {
        try {
            _instance = new processing_class(name);
        } catch (std::bad_alloc&) {
            throw;
        }
    }
    return _instance;
}

processing_class* processing_class::instance()
{
    if (_instance == nullptr) {
        std::cerr << "Class is not initialised !" << std::endl;
        exit(-1);
    }
    return _instance;
}

//=========================================
//	Define classes for attributes
//=========================================

//=========================================
// Define classes for commands
//=========================================

class get_frame_command : public Tango::Command
{
  public:
    get_frame_command() :
        Tango::Command("getFrame", Tango::DEV_LONG, Tango::DEV_ENCODED, "Frame Index", "A DevEncoded image",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "get_frame_command::execute(): arrived" << std::endl;

        Tango::DevLong frame_idx;
        extract(in_any, frame_idx);

        Tango::DevEncoded res = static_cast<processing*>(dev)->get_frame(frame_idx);

        return insert(new Tango::DevEncoded(res));
    }
};

//===================================================================
// Properties management
//===================================================================

Tango::DbDatum processing_class::get_class_property(std::string& prop_name)
{
    for (unsigned int i = 0; i < cl_prop.size(); i++)
        if (cl_prop[i].name == prop_name)
            return cl_prop[i];
    // if not found, returns  an empty DbDatum
    return Tango::DbDatum(prop_name);
}

Tango::DbDatum processing_class::get_default_device_property(std::string& prop_name)
{
    for (unsigned int i = 0; i < dev_def_prop.size(); i++)
        if (dev_def_prop[i].name == prop_name)
            return dev_def_prop[i];
    // if not found, return  an empty DbDatum
    return Tango::DbDatum(prop_name);
}

Tango::DbDatum processing_class::get_default_class_property(std::string& prop_name)
{
    for (unsigned int i = 0; i < cl_def_prop.size(); i++)
        if (cl_def_prop[i].name == prop_name)
            return cl_def_prop[i];
    // if not found, return  an empty DbDatum
    return Tango::DbDatum(prop_name);
}

void processing_class::set_default_property()
{
    // Set Default Class Properties

    // Set Default device Properties
}

void processing_class::write_class_property()
{
    // First time, check if database used
    if (Tango::Util::_UseDb == false)
        return;

    Tango::DbData data;
    std::string classname = get_name();
    std::string header;
    std::string::size_type start, end;

    // Put title
    Tango::DbDatum title("ProjectTitle");
    std::string str_title("LIMA2");
    title << str_title;
    data.push_back(title);

    // Put Description
    Tango::DbDatum description("Description");
    std::vector<std::string> str_desc;
    str_desc.push_back("LIMA2 Processing Class");
    description << str_desc;
    data.push_back(description);

    //  Put inheritance
    Tango::DbDatum inher_datum("InheritedFrom");
    std::vector<std::string> inheritance;
    inheritance.push_back("TANGO_BASE_CLASS");
    inher_datum << inheritance;
    data.push_back(inher_datum);

    // Call database and and values
    get_db_class()->put_property(data);
}

//===================================================================
// Factory methods
//===================================================================

void processing_class::device_factory(const Tango::DevVarStringArray* devlist_ptr)
{
    // create devices and add it into the device list
    for (unsigned long i = 0; i < devlist_ptr->length(); i++) {
        TANGO_LOG_INFO << "device name : " << (*devlist_ptr)[i].in() << std::endl;
        device_list.push_back(new device_t(this, (*devlist_ptr)[i]));
    }

    // manage dynamic attributes if any
    erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

    // export devices to the outside world
    for (unsigned long i = 1; i <= devlist_ptr->length(); i++) {
        // add dynamic attributes if any
        device_t* dev = static_cast<device_t*>(device_list[device_list.size() - i]);
        dev->add_dynamic_attributes();

        // Add it into the device list
        device_list.push_back(dev);

        // check before if database used.
        if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
            export_device(dev);
        else
            export_device(dev, dev->get_name().c_str());
    }
}

void processing_class::attribute_factory(std::vector<Tango::Attr*>& att_list)
{
    // Add proc_params JSON attribute
    {
        using proc_params_attr_t = JsonAttr<device_t, &device_t::m_proc_params>;
        proc_params_attr_t* attr = new proc_params_attr_t("proc_params", Tango::DEV_STRING, Tango::READ_WRITE);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Processing parameters";
        attr_prop.label = "proc_params";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add proc_params JSON schema attribute property
    {
        // Generate the schema for the plugin proc_params
        boost::json::value schema = boost::json::schema_from<typename device_t::proc_params_t>("proc_params");

        // Add a "schema" class attribute property
        Tango::DbDatum db_nb_attrs("proc_params");
        Tango::DbDatum db_schema("schema");

        db_nb_attrs << 1; // One "schema" property for attribute "params"
        std::string schema_str =
            boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
        db_schema << schema_str;

        // Push to the DB
        Tango::DbData db_data;
        db_data.push_back(db_nb_attrs);
        db_data.push_back(db_schema);

        get_db_class()->put_attribute_property(db_data);
    }

    // Add counters JSON attribute
    {
        using counters_attr_t = JsonReadAttr<device_t, &processing::counters>;
        counters_attr_t* attr = new counters_attr_t("counters", Tango::DEV_STRING, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Processing counters";
        attr_prop.label = "counters";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add progress_counters JSON schema attribute property
    {
        // Generate the schema for the plugin acq_params
        boost::json::value schema =
            boost::json::schema_from<typename device_t::progress_counters_t>("progress_counters");

        // Add a "schema" class attribute property
        Tango::DbDatum db_nb_attrs("progress_counters");
        Tango::DbDatum db_schema("schema");

        db_nb_attrs << 1; // One "schema" property for attribute "params"
        std::string schema_str =
            boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
        db_schema << schema_str;

        // Push to the DB
        Tango::DbData db_data;
        db_data.push_back(db_nb_attrs);
        db_data.push_back(db_schema);

        get_db_class()->put_attribute_property(db_data);
    }

    // Create a list of static attributes
    create_static_attribute_list(get_class_attr()->get_attr_list());
}

void processing_class::pipe_factory() {}

void processing_class::command_factory()
{
    command_list.emplace_back(new get_frame_command());

    // TODO Add get_frame
}

//===================================================================
// Dynamic attributes related methods
//===================================================================

void processing_class::create_static_attribute_list(std::vector<Tango::Attr*>& att_list)
{
    for (unsigned long i = 0; i < att_list.size(); i++) {
        std::string att_name(att_list[i]->get_name());
        transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
        defaultAttList.push_back(att_name);
    }

    TANGO_LOG_INFO << defaultAttList.size() << " attributes in default list" << std::endl;
}

void processing_class::erase_dynamic_attributes(const Tango::DevVarStringArray* devlist_ptr,
                                                std::vector<Tango::Attr*>& att_list)
{
    Tango::Util* tg = Tango::Util::instance();

    for (unsigned long i = 0; i < devlist_ptr->length(); i++) {
        Tango::DeviceImpl* dev_impl = tg->get_device_by_name(((std::string)(*devlist_ptr)[i]).c_str());
        device_t* dev = static_cast<device_t*>(dev_impl);

        std::vector<Tango::Attribute*>& dev_att_list = dev->get_device_attr()->get_attribute_list();
        for (auto ite_att = dev_att_list.begin(); ite_att != dev_att_list.end(); ++ite_att) {
            std::string att_name((*ite_att)->get_name_lower());
            if ((att_name == "state") || (att_name == "status"))
                continue;
            auto ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
            if (ite_str == defaultAttList.end()) {
                TANGO_LOG_INFO << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i]
                               << std::endl;
                Tango::Attribute& att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
                dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
                --ite_att;
            }
        }
    }
}

Tango::Attr* processing_class::get_attr_object_by_name(std::vector<Tango::Attr*>& att_list, std::string attname)
{
    for (auto&& att : att_list)
        if (att->get_name() == attname)
            return att;

    // Attr does not exist
    return NULL;
}

} // namespace lima::tango::cuda
