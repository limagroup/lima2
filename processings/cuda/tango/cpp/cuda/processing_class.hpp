// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>
#include <string>

#include <lima/tango/device_class.hpp>

#include "processing.hpp"

namespace lima::tango::cuda
{
/// The processing_class singleton definition
class processing_class : public device_class
{
  public:
    using processing_t = lima::processing::pipelines::cuda::pipeline;
    using device_t = processing;

    //	write class properties data members
    Tango::DbData cl_prop;
    Tango::DbData cl_def_prop;
    Tango::DbData dev_def_prop;

    // Non-copiable
    processing_class(const processing_class&) = delete;
    processing_class& operator=(const processing_class&) = delete;
    processing_class(processing_class&&) = delete;
    processing_class& operator=(processing_class&&) = delete;

    /// Create the object if not already done.Otherwise, just return a pointer to the object
    ///
    /// \param name	The class name
    static processing_class* init(std::string const& name);

    /// Singleton access, check if object already created, and return a pointer to the object
    static processing_class* instance();

    /// Clean up
    ~processing_class() { _instance = nullptr; }

    /// Get the class property for specified name
    Tango::DbDatum get_class_property(std::string&) override;

    /// Return the default value for device property
    Tango::DbDatum get_default_device_property(std::string&) override;

    /// Return the default value for class property
    Tango::DbDatum get_default_class_property(std::string&) override;

  protected:
    processing_class(std::string name);

    /// Create the command object(s) and store them in the command list
    void command_factory();

    /// Create the attribute object(s) and store them in the attribute list
    void attribute_factory(std::vector<Tango::Attr*>&);

    /// Create the pipe object(s) and store them in the pipe list
    void pipe_factory();

    /// Properties management
    ///{

    ///  Set class description fields as property in database
    void write_class_property();

    /// Set default property (class and device) for wizard.
    /// For each property, add to wizard property name and description.
    /// If default value has been set, add it to wizard property and
    /// store it in a DbDatum.
    void set_default_property();

    ///}

    std::string get_cvstag();
    std::string get_cvsroot();

    static processing_class* _instance; //!< Singleton instance

  private:
    /// Factory methods

    /// Create the device object(s) and store them in the device list
    void device_factory(const Tango::DevVarStringArray*) override;

    /// Create the a list of static attributes
    ///
    /// \param att_list	the ceated attribute list
    void create_static_attribute_list(std::vector<Tango::Attr*>&);

    /// Delete the dynamic attributes if any
    ///
    /// \param devlist_ptr the device list pointer
    /// /// \param  list of all attributes
    void erase_dynamic_attributes(const Tango::DevVarStringArray*, std::vector<Tango::Attr*>&);

    /// Returns Tango::Attr * object found by name
    Tango::Attr* get_attr_object_by_name(std::vector<Tango::Attr*>& att_list, std::string attname);

    std::vector<std::string> defaultAttList; //!< Default attibute list
};

} // namespace lima::tango::cuda
