// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tango.h>

#include <lima/processing/pipelines/cuda.describe.hpp>

namespace lima::tango::cuda
{
class processing : public TANGO_BASE_CLASS
{
  public:
    using processing_t = lima::processing::pipelines::cuda::pipeline;
    using proc_params_t = typename processing_t::proc_params_t;
    using progress_counters_t = typename processing_t::progress_counters_t;

    // Constructors and destructors
    processing(Tango::DeviceClass* cl, const char* name) : TANGO_BASE_CLASS(cl, name) { init_device(); }
    processing(Tango::DeviceClass* cl, const char* name, const char* description) :
        TANGO_BASE_CLASS(cl, name, description)
    {
        init_device();
    }
    ~processing() { delete_device(); }

    /// Miscellaneous methods
    ///{
    /// Will be called at device destruction or at init command
    void delete_device() { DEBUG_STREAM << "processing::delete_device() " << device_name << std::endl; }

    /// Initialize the device
    void init_device() override;

    /// Always executed method before execution command method
    void always_executed_hook();
    ///}

    /// Attribute related methods
    ///{

    /// Hardware acquisition for attributes
    virtual void read_attr_hardware(std::vector<long>& attr_list);

    /// Hardware writing for attributes
    virtual void write_attr_hardware(std::vector<long>& attr_list);

    /// Add dynamic attributes if any
    void add_dynamic_attributes();
    ///}

    /// Command related methods
    ///{

    /// Returns the progress counters
    progress_counters_t counters() const { return m_proc->get_counters(); }

    /// Returns a frame for a given index
    Tango::DevEncoded get_frame(std::size_t frame_idx) const;

    /// Add dynamic commands if any
    void add_dynamic_commands();

    ///}

    // Attribute data members
    std::shared_ptr<processing_t> m_proc;
    proc_params_t m_proc_params;

  private:
};

// Additional Classes Definitions

} // namespace lima::tango::cuda
