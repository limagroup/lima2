// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/exception/diagnostic_information.hpp>
#include <boost/mp11.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/string_generator.hpp>

#include <lima/log/severity_level.describe.hpp>
#include <lima/log/logger.hpp>
#include <lima/hw/params.describe.hpp>
#include <lima/utils/type_traits.hpp>

#include <lima/tango/convert.hpp>
#include <lima/tango/data_array.hpp>

#include <cuda/processing.hpp>

namespace lima::tango::cuda
{
void processing::init_device()
{
    DEBUG_STREAM << "processing::init_device() create device " << device_name << std::endl;
}

void processing::always_executed_hook()
{
    DEBUG_STREAM << "processing::always_executed_hook()  " << device_name << std::endl;

    // code always executed before all requests
}

void processing::read_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    DEBUG_STREAM << "processing::read_attr_hardware(vector<long> &attr_list) entering... " << std::endl;

    // Add your own code
}

void processing::write_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    DEBUG_STREAM << "control::write_attr_hardware(vector<long> &attr_list) entering... " << std::endl;

    // Add your own code
}

void processing::add_dynamic_attributes()
{
    // Add your own code to create and add dynamic attributes if any
}

struct roi_counter
{
    std::size_t frame_idx;
    float min;
    float max;
    float avg;
    float std;
};

Tango::DevEncoded processing::get_frame(std::size_t frame_idx) const
{
    auto frm = m_proc->get_frame(frame_idx);

    return make_devencoded(frm);
}

void processing::add_dynamic_commands()
{
    // Add your own code to create and add dynamic commands if any
}

// Additional Methods

} // namespace lima::tango::cuda
