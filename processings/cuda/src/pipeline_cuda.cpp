// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <atomic>
#include <future>

#include <boost/lockfree/spsc_queue.hpp>

#include <lima/logging.hpp>
#include <lima/utils/math.hpp>

#include <lima/processing/pipelines/cuda.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace cuda
    {
        /// Implementation of the receiver
        class pipeline::impl
        {
            using frame_t = frame;

          public:
            using proc_params_t = proc_params;

            impl(int rank, frame_info const& frame_info, hw::acq_params const& acq_params,
                 proc_params_t const& proc_params) :
                m_fifo(proc_params.fifo.nb_fifo_frames), m_frames_buffer(100)
            {
                using namespace std::chrono_literals;

                m_cuda = std::async(std::launch::async, [this, stop = false]() mutable {
                    // If input is flagged to stop, return early
                    if (stop)
                        return (int) m_nb_frames_source;

                    // Poll for available frames
                    data_t frame;
                    while (!m_fifo.pop(frame))
                        ;

                    // Limit the number of log traces given the order of magnitude of nb frames
                    if (!(res.metadata.idx % order_of_magnitude_base10(res.metadata.idx)))
                        LIMA_LOG(trace, proc) << "Processing thread got frame " << frame.metadata.idx;

                    m_nb_frames_source++;

                    // TODO Call CUDA kernel
                    std::this_thread::sleep_for(1ms);

                    {
                        // Push processed frame to buffer
                        const std::lock_guard<std::mutex> lock(m_frames_buffer_mutex);
                        m_frames_buffer.push_back(frame);
                    }

                    m_nb_frames_processed++;

                    // If the frame is final, flag the input to stop next run
                    if (frame.metadata.is_final) {
                        LIMA_LOG(trace, proc) << "Processing thread got final frame";
                        stop = true;
                    }

                    return (int) m_nb_frames_source;
                });

                LIMA_LOG(trace, proc) << "Processing constructed";
            }

            /// Activate the processing (start poping data from the queue)
            void activate()
            {
                m_finished_future = std::async(std::launch::async, [this] {
                    std::exception_ptr eptr;

                    try {
                        LIMA_LOG(trace, proc) << "Processing finished";
                    } catch (...) {
                        LIMA_LOG(error, proc) << "Processing failed";
                        eptr = std::current_exception();
                    }

                    // Set the is_finished flag and call the callback if registered
                    m_is_finished = true;
                    if (m_on_finished)
                        m_on_finished(eptr);
                });
            }

            /// Returns true when the processing has finished
            bool is_finished() const { return m_is_finished; }

            /// Register on_finished callback
            void register_on_finished(finished_callback_t on_finished) { m_on_finished = on_finished; }

            /// Abort the pipeline
            void abort() {}

            /// Push the given frame to the FIFO of frames to process
            void process(frame_t const& frm) { m_fifo.push(frm); }

            counters get_counters() const { return {(int) m_nb_frames_source, (int) m_nb_frames_processed}; }

            frame_t get_frame(std::size_t frame_idx = -1) const
            {
                frame_t res;

                // If frames buffer is empty, returns early
                if (m_frames_buffer.empty())
                    return res;

                const std::lock_guard<std::mutex> lock(m_frames_buffer_mutex);

                // If frame_idx == -1 returns the latest frame in the buffer
                if (frame_idx == -1)
                    res = m_frames_buffer.back();
                else {
                    auto it = std::find_if(m_frames_buffer.begin(), m_frames_buffer.end(),
                                           [frame_idx](frame_t const& frm) { return frame_idx == frm.metadata.idx; });
                    if (it != m_frames_buffer.end())
                        res = *it;
                }

                return res;
            }

          private:
            // Queue of frame (runtime sized)
            boost::lockfree::spsc_queue<data_t> m_fifo;
            std::future<int> m_cuda;

            // Counters
            std::atomic_int m_nb_frames_source = 0;
            std::atomic_int m_nb_frames_processed = 0;

            // Frames
            boost::circular_buffer<frame_t> m_frames_buffer;
            mutable std::mutex m_frames_buffer_mutex;

            // Finished
            std::atomic_bool m_is_finished{false};
            finished_callback_t m_on_finished;
            std::future<void> m_finished_future; // last member
        };

        // Pimpl boilerplate
        pipeline::pipeline(int rank, frame_info const& frame_info, hw::acq_params const& acq_params,
                           proc_params_t const& proc_params) :
            m_pimpl{std::make_unique<impl>(rank, frame_info, acq_params, proc_params)}
        {
        }

        pipeline::pipeline(pipeline&&) = default;

        pipeline::~pipeline() = default;

        pipeline& pipeline::operator=(pipeline&&) = default;

        void pipeline::activate() { m_pimpl->activate(); }

        bool pipeline::is_finished() const { return m_pimpl->is_finished(); }

        void pipeline::register_on_finished(finished_callback_t on_finished)
        {
            m_pimpl->register_on_finished(on_finished);
        }

        void pipeline::abort() { m_pimpl->abort(); }

        void pipeline::process(data_t const& frm) { m_pimpl->process(frm); }

        counters pipeline::get_counters() const { return m_pimpl->get_counters(); }

        pipeline::data_t pipeline::get_frame(std::size_t frame_idx) const { return m_pimpl->get_frame(frame_idx); }

    } // namespace cuda
} // namespace processing::pipelines
} // namespace lima
