// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/hw/params.describe.hpp>
#include <lima/io/h5/params.describe.hpp>

#include <lima/processing/pipelines/cuda/params.hpp>

// This file is part of the user interface of the library and should not include any private dependencies
// or other thing that expose implementation details

namespace lima
{
namespace processing::pipelines
{
    namespace cuda
    {
        BOOST_DESCRIBE_STRUCT(fifo_params, (), (nb_fifo_frames))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(fifo_params, nb_fifo_frames,
            (desc, "number of frames in FIFO"),
            (doc, "The number of frames in the Processing FIFO"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(kernel_params, (), (threshold))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(kernel_params, threshold,
            (desc, "energy threshold"),
            (doc, "The mean energy threshold to discard frames"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(proc_params, (), (fifo, kernel))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(proc_params, fifo,
            (desc, "fifo parameters"),
            (doc, "The processing FIFO parameters"))
            
        BOOST_ANNOTATE_MEMBER(proc_params, kernel,
            (desc, "kernel parameters"),
            (doc, "The CUDA kernel parameters"))
        // clang-format on

    } // namespace cuda
} // namespace processing::pipelines
} // namespace lima
