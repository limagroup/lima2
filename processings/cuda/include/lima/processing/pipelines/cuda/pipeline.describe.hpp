// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/processing/pipelines/cuda/pipeline.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace cuda
    {
        BOOST_DESCRIBE_STRUCT(counters, (), (nb_frames_source, nb_frames_processed))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(counters, nb_frames_source,
            (desc, "nb frames source"),
            (doc, "The number of frame poped from the FIFO"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_processed,
            (desc, "number of frames processed"),
            (doc, "The number of frames processed on GPU"))
        // clang-format on

    } // namespace cuda
} // namespace processing::pipelines

} // namespace lima
