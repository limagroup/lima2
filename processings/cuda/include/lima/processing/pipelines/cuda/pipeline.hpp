// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory>

#include <lima/core/frame_info.hpp>
#include <lima/hw/params.hpp>

#include <lima/processing/pipelines/cuda/params.hpp>

#include <pipeline_cuda_export.h>

namespace lima
{
namespace processing::pipelines
{
    namespace cuda
    {
        struct counters
        {
            int nb_frames_source = 0;
            int nb_frames_processed = 0;
        };

        using finished_callback_t = std::function<void(std::exception_ptr)>;

        struct PIPELINE_CUDA_EXPORT pipeline
        {
          public:
            static constexpr char const* const uid = "Cuda";

            using proc_params_t = proc_params;
            using progress_counters_t = counters;

            using data_t = frame;

            pipeline(int rank, frame_info const& frame_info, hw::acq_params const& acq_params,
                     proc_params_t const& proc_params);
            ~pipeline();          // defined in the implementation file, where impl is a complete type
            pipeline(pipeline&&); // defined in the implementation file
            pipeline(const pipeline&) = delete;
            pipeline& operator=(pipeline&&); // defined in the implementation file
            pipeline& operator=(const pipeline&) = delete;

            /// Activate the processing (start poping data from the queue)
            void activate();

            /// Return the finished state
            bool is_finished() const;

            /// Register on_finished callback
            void register_on_finished(finished_callback_t on_finished);

            /// Returns when the processing has finished
            void wait_for_all();

            /// Abort the pipeline
            void abort();

            /// Process a frame
            void process(data_t const& frm);

            /// Returns the progress counters
            progress_counters_t get_counters() const;

            /// Returns frame for the given index
            data_t get_frame(std::size_t frame_idx = -1) const;

          private:
            class impl;

#if defined(LIMA_HAS_PROPAGATE_CONST)
            std::experimental::propagate_const< // const-forwarding pointer wrapper
                std::unique_ptr<                // unique-ownership opaque pointer
                    impl>>
                m_pimpl; // to the forward-declared implementation class
#else
            std::unique_ptr< // unique-ownership opaque pointer
                impl>
                m_pimpl; // to the forward-declared implementation class
#endif
        };
    } // namespace cuda
} // namespace processing::pipelines
} // namespace lima
