// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/serialization/vector.hpp> // for rois_settings

#include <lima/core/frame.hpp>
#include <lima/core/frame_info.hpp>

#include <lima/hw/params.hpp>
#include <lima/io/h5/params.hpp>

// This file is part of the user interface of the library and should not include any private dependencies
// or other thing that expose implementation details

namespace lima
{
namespace processing::pipelines
{
    namespace cuda
    {
        struct kernel_params
        {
            double threshold = 1.0;
        };

        struct fifo_params
        {
            int nb_fifo_frames = 100;
        };

        struct proc_params
        {
            fifo_params fifo;
            kernel_params kernel;
        };
    } // namespace cuda
} // namespace processing::pipelines
} // namespace lima
