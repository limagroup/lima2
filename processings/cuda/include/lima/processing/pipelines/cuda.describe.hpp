// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/frame_info.describe.hpp>

#include <lima/processing/pipelines/cuda/params.describe.hpp>
#include <lima/processing/pipelines/cuda/pipeline.describe.hpp>
