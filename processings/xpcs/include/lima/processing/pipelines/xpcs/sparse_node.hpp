// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <limits>
#include <memory>

#include <tbb/flow_graph.h>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

#include <lima/processing/pipelines/xpcs/sparse_frame.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace xpcs
    {
        template <typename ConstView>
        void sparsify(const ConstView& src, sparse_frame& dst)
        {
            using value_type = ConstView::value_type;

            value_type nodata_value = std::numeric_limits<value_type>::max();

            boost::gil::for_each_pixel_position(src, [origin = src.xy_at(0, 0), &dst, nodata_value](auto loc) {
                if (*loc != nodata_value && *loc > 0) {
                    dst.pixel_indices.push_back(std::distance(origin.x(), loc.x()));
                    dst.pixel_values.push_back(*loc);
                }
            });
        }

        template <typename... Views>
        void sparsify(boost::gil::any_image_view<Views...> const& src, sparse_frame& dst)
        {
            boost::variant2::visit([&dst](auto&& v) { return sparsify(v, dst); }, src);
        }

        struct sparse_node : public tbb::flow::function_node<frame, sparse_frame>
        {
            using parent_t = tbb::flow::function_node<frame, sparse_frame>;
            using dimensions_t = typename sparse_frame::point_t;

            sparse_node(tbb::flow::graph& g, size_t concurrency) :
                parent_t(g, concurrency, [](frame in) {
                    sparse_frame res(in.dimensions());
                    res.metadata = in.metadata;
                    res.attributes = in.attributes;

                    //LIMA_LOG(trace, proc) << "Start sparse " << in.metadata.idx;

                    sparsify(lima::const_view(in), res);

                    //LIMA_LOG(trace, proc) << "End sparse " << in.metadata.idx;

                    return res;
                })
            {
            }
        };

    } // namespace xpcs
} // namespace processing::pipelines
} // namespace lima
