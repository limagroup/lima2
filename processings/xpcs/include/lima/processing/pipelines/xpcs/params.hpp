// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

#include <lima/core/arc.hpp>
#include <lima/core/enums.hpp>
#include <lima/core/rectangle.hpp>

#include <lima/hw/params.hpp>
#include <lima/hw/info.hpp>

#include <lima/io/h5/params.hpp>

// This file is part of the user interface of the library and should not include any private dependencies
// or other thing that expose implementation details

namespace lima
{
namespace processing::pipelines
{
    namespace xpcs
    {
        struct fifo_params
        {
            int nb_fifo_frames = 100;
        };

        struct thread_params
        {
            int nb_threads = 16;
            int numa_node = 0;
        };

        struct geometry_params
        {
            bool enabled = false;
            rectangle_t roi;
            rotation_enum rotation;
            flip_enum flip;
        };

        struct accumulation_params
        {
            bool enabled = false;
            int nb_frames = 1;
            pixel_enum pixel_type = pixel_enum::gray32;
        };

        struct roi_statistics_params
        {
            bool enabled = false;
            std::vector<rectangle_t> rect_rois = {};
            std::vector<arc_t> arc_rois = {};
        };

        struct roi_profiles_params
        {
            bool enabled = false;
            std::vector<rectangle_t> rois = {};
            std::vector<direction_enum> directions = {};
        };

        struct mask_params
        {
            bool enabled = false;
            std::filesystem::path path;
        };

        struct flatfield_params
        {
            bool enabled = false;
            std::filesystem::path path;
        };

        struct background_params
        {
            bool enabled = false;
            std::filesystem::path path;
        };

        struct buffer_params
        {
            int nb_roi_statistics_buffer = 100;
            int nb_roi_profiles_buffer = 100;
            int nb_input_frames_buffer = 1;
            int nb_frames_buffer = 100;
            int nb_sparse_frames_buffer = 100;
        };

        struct saving_dense_params : io::h5::saving_params
        {
            bool enabled = true;
        };

        struct saving_sparse_params : io::h5::saving_params
        {
            saving_sparse_params() { filename_prefix = "lima2_sparse"; }
            bool enabled = true;
        };

        struct proc_params
        {
            fifo_params fifo;
            thread_params thread;
            buffer_params buffers;
            geometry_params geometry;
            accumulation_params accumulation;
            mask_params mask;
            flatfield_params flatfield;
            background_params background;
            roi_statistics_params statistics;
            roi_profiles_params profiles;
            saving_dense_params saving_dense;
            saving_sparse_params saving_sparse;
            hw::info det_info;
        };
    } // namespace xpcs
} // namespace processing::pipelines
} // namespace lima
