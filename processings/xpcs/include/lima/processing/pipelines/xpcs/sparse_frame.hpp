// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cmath>
#include <vector>

#include <boost/json/value.hpp>

#include <lima/core/enums.hpp>
#include <lima/core/pixel.hpp>
#include <lima/core/point.hpp>
#include <lima/core/frame_metadata.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace xpcs
    {
        struct sparse_frame
        {
            static const std::size_t num_dimensions = 2;
            using coord_t = std::ptrdiff_t;
            using point_t = point<coord_t>;

            using attributes_t = boost::json::value;
            using metadata_t = frame_metadata;

            // Construct empty
            explicit sparse_frame() = default;

            sparse_frame(const point_t& dims) : m_dimensions(dims) { init(dims.x * dims.y); }

            sparse_frame(coord_t width, coord_t height) : m_dimensions(width, height) { init(width * height); }

            void init(std::size_t max_nb_pixels)
            {
                const double fill_factor = 0.05;

                pixel_indices.reserve(std::lround(max_nb_pixels * 0.05));
                pixel_values.reserve(std::lround(max_nb_pixels * 0.05));
            }

            const point_t& dimensions() const { return m_dimensions; }
            coord_t width() const { return m_dimensions.x; }
            coord_t height() const { return m_dimensions.y; }
            std::size_t size() const { return m_dimensions.x * m_dimensions.y; }
            std::size_t nb_pixels() const { return pixel_indices.size(); }
            pixel_enum pixel_type() const { return pixel_enum::gray16; }

            std::size_t size_in_bytes() const
            {
                return pixel_indices.size() * sizeof(int) + pixel_values.size() * sizeof(float);
            }

            std::vector<int> pixel_indices;
            std::vector<std::uint16_t> pixel_values;

            point_t m_dimensions;    //<! Frame dimensions
            metadata_t metadata;     //<! Static typed metadata
            attributes_t attributes; //<! Dynamic attributes encoded as string
        };

    } // namespace xpcs
} // namespace processing::pipelines
} // namespace lima
