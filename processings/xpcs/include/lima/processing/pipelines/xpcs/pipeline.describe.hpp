// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/processing/pipelines/xpcs/pipeline.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace xpcs
    {
        BOOST_DESCRIBE_STRUCT(counters, (),
                              (nb_frames_source, nb_frames_input, nb_frames_processed, nb_frames_counters,
                               nb_frames_profiles, nb_frames_dense_saved, nb_frames_sparse_saved))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(counters, nb_frames_source,
            (desc, "nb frames source"),
            (doc, "The number of frame poped from the FIFO"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_input,
            (desc, "nb frames input"),
            (doc, "The number of input frame"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_processed,
            (desc, "nb frames processed"),
            (doc, "The number of processed frame"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_counters,
            (desc, "nb frames roi counters"),
            (doc, "The number of frame processed with ROI counters"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_profiles,
            (desc, "nb frames roi profiles"),
            (doc, "The number of frame processed with ROI profiles"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_dense_saved,
            (desc, "nb dense frames saved"),
            (doc, "The number of frame saved to file"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_sparse_saved,
            (desc, "nb sparse frames saved"),
            (doc, "The number of frame saved to file"))
        // clang-format on

    } // namespace xpcs
} // namespace processing::pipelines

} // namespace lima
