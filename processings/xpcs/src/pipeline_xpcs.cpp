// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <atomic>
#include <optional>
#include <stdexcept>
#include <future>

#include <boost/exception/error_info.hpp>
#include <boost/exception/errinfo_frame_idx.hpp>

#include <tbb/concurrent_queue.h>

#include <fmt/chrono.h>
#include <fmt/ranges.h>
#include <fmt/describe.hpp>
#include <fmt/filesystem.hpp>

#include <lima/core/frame_info.describe.hpp>
#include <lima/hw/params.describe.hpp>

#include <lima/logging.hpp>
#include <lima/io/check.hpp>
#include <lima/processing/typedefs.hpp>
#include <lima/processing/nodes/accumulation.hpp>
#include <lima/processing/nodes/checkpoint.hpp>
#include <lima/processing/nodes/io_hdf5.hpp>
#include <lima/processing/nodes/crop.hpp>
#include <lima/processing/nodes/mask.hpp>
#include <lima/processing/nodes/background.hpp>
#include <lima/processing/nodes/flatfield.hpp>
#include <lima/processing/nodes/roi_counters.hpp>
#include <lima/processing/nodes/roi_profiles.hpp>
#include <lima/processing/nodes/circular_buffer.hpp>
#include <lima/processing/nodes/unbounded_buffer.hpp>
#include <lima/processing/nodes/error.hpp>
#include <lima/utils/math.hpp>

#include <lima/processing/pipelines/xpcs.hpp>
#include <lima/processing/pipelines/xpcs/params.describe.hpp>
#include <lima/processing/pipelines/xpcs/sparse_node.hpp>
#include <lima/processing/pipelines/xpcs/io_hdf5_sparse_node.hpp>

#include <xpcs/project_version.h>

namespace lima
{
namespace processing::pipelines
{
    namespace detail
    {
        /// Read an HDF5 data into a frame
        inline void read_h5_dset(std::filesystem::path const& h5_path, frame& out, std::string dataset_path)
        {
            namespace h5 = lima::io::h5;

            // Open an existing file
            auto file = h5::file::open(h5_path);

            // Open an existing dataset
            auto dataset = h5::dataset::open(file, dataset_path);
            auto dtype = dataset.datatype();

            // Check the dimensions
            auto dataspace = dataset.space();
            const int ndims = dataspace.ndims();
            //assert(ndims == frame::num_dimensions);

            hsize_t dims[ndims];
            dataspace.dims(dims, nullptr);

            //hsize_t size = std::accumulate(dims, dims + ndims, 1, [](hsize_t lhs, hsize_t rhs) { return lhs * rhs; });
            out.recreate(dims[1], dims[0], dtype.pixel_type());

            // Read the dataset
            dataset.read(out.data, dtype);
        }
    } // namespace detail

    namespace xpcs
    {
        /// Implementation of the pipeline
        class pipeline::impl
        {
          public:
            using input_t = lima::frame;
            using proc_params_t = proc_params;

            impl(frame_info_t const& frame_info, proc_params_t const& proc_params, acq_params_t const& acq_params,
                 det_info_t const& det_info) :
                m_input_frame_info(frame_info),
                m_processed_frame_info(frame_info),
                m_proc_params(proc_params),
                m_metadata_writer(acq_params, det_info),
                m_frames_buffer(proc_params.buffers.nb_frames_buffer),
                m_input_frames_buffer(proc_params.buffers.nb_input_frames_buffer),
                m_sparse_frames_buffer(proc_params.buffers.nb_sparse_frames_buffer),
                m_fill_factor_buffer(proc_params.buffers.nb_sparse_frames_buffer),
                m_roi_statistics_buffer(proc_params.buffers.nb_roi_statistics_buffer),
                m_roi_profiles_buffer(proc_params.buffers.nb_roi_profiles_buffer)
            {
                LIMA_LOG(trace, det) << fmt::format("frame_info = {}", frame_info);
                LIMA_LOG(trace, det) << fmt::format("proc_params = {}", proc_params);

                // Set the FIFO capacity
                m_fifo.set_capacity(proc_params.fifo.nb_fifo_frames);

                // Check output paths
                io::check_base_path(proc_params.saving_dense.base_path);
                io::check_base_path(proc_params.saving_sparse.base_path);

                LIMA_LOG(trace, proc) << "Processing constructed";

                // Compute the size of the processed frame
                if (proc_params.geometry.enabled)
                    m_processed_frame_info.m_dimensions = m_proc_params.geometry.roi.dimensions;
                if (proc_params.accumulation.enabled)
                    m_processed_frame_info.m_pixel_type = m_proc_params.accumulation.pixel_type;
                if (proc_params.geometry.enabled)
                    if ((proc_params.geometry.rotation == rotation_enum::r90cw)
                        || (proc_params.geometry.rotation == rotation_enum::r90ccw))
                        swap_dimensions(m_processed_frame_info.m_dimensions);
            }

            ~impl()
            {
                // Abort the processing. The normal way is to send and empty frame tagged is_final.
                if (!m_is_finished)
                    abort();

                // Make sure that the processing thread joins
                if (m_finished_future.valid())
                    m_finished_future.get();

                LIMA_LOG(trace, proc) << "Processing destructed";
            }

            /// Run the processing (start poping data from the queue)
            void activate()
            {
                // Create computing resssources that have the same lifetime than the computation
                // AKA GPU ressources

                // Not possible to use task_arena here since
                //  - task_arena.execute() is synchronous
                //  - task_arena.enqueue() does not handle exception
                m_finished_future = std::async(std::launch::async, [this] {
                    auto numa_nodes = oneapi::tbb::info::numa_nodes();
                    tbb::numa_node_id numa_index = tbb::task_arena::automatic;

                    tbb::task_arena arena;
                    arena.initialize(tbb::task_arena::constraints(numa_index, m_proc_params.thread.nb_threads));

                    std::optional<std::string> error;
                    // Run a TBB flow graph in a given arena
                    arena.execute([&]() {
                        tbb::flow::graph graph(m_task_group_context);

                        tbb::flow::input_node<frame> src(graph, [this, stop = false](tbb::flow_control& fc) mutable {
                            input_t res;

                            // If input is flagged to stop, return early
                            if (stop) {
                                fc.stop();
                                return res;
                            }

                            // Poll for available frames
                            m_fifo.pop(res);

                            // Limit the number of log traces given the order of magnitude of nb frames
                            if (!(res.metadata.idx % order_of_magnitude_base10(res.metadata.idx)))
                                LIMA_LOG(trace, proc) << "Processing thread got frame " << res.metadata.idx;

#if !defined(NDEBUG)
                            if (res.is_zero())
                                LIMA_LOG(trace, proc) << "All pixels are 0";
                            else
                                LIMA_LOG(trace, proc) << "Some pixels are not 0";
#endif

                            // If acquisition is interrupted or failed, hw_get_frame() is cancelled and returned frame might be empty
                            if (res.empty() && res.metadata.is_final) {
                                LIMA_LOG(trace, proc) << "Processing thread got empty frame (acquisition cancelled)";
                                fc.stop();
                                return res;
                            }

                            // If the frame is final, flag the input to stop next run
                            if (res.metadata.is_final) {
                                LIMA_LOG(trace, proc) << "Processing thread got final frame";
                                stop = true;
                            }

                            m_nb_frames_source++;

                            return res;
                        });

                        // Frames buffer node
                        circular_buffer_node<frame> input_frames_buffer(graph, tbb::flow::unlimited,
                                                                        m_input_frames_buffer);
                        circular_buffer_node<frame> processed_frames_buffer(graph, tbb::flow::unlimited,
                                                                            m_frames_buffer);

                        tbb::flow::function_node<frame, frame> deep_copy1(graph, tbb::flow::unlimited,
                                                                          [](frame in) { return in.clone(); });
                        tbb::flow::function_node<frame, frame> deep_copy2(graph, tbb::flow::unlimited,
                                                                          [](frame in) { return in.clone(); });

                        std::optional<accumulation_node> accumulation;
                        std::optional<crop_node> crop;

                        checkpoint_node<frame> checkpoint_raw(graph, tbb::flow::unlimited, m_nb_frames_raw);
                        checkpoint_node<frame> checkpoint_processed(graph, tbb::flow::unlimited, m_nb_frames_processed);

                        // Geometry
                        if (m_proc_params.geometry.enabled) {
                            crop.emplace(graph, tbb::flow::unlimited, m_proc_params.geometry.roi);
                        }

                        // Accumulation
                        if (m_proc_params.accumulation.enabled) {
                            accumulation.emplace(graph, m_proc_params.accumulation.nb_frames,
                                                 m_proc_params.accumulation.pixel_type);
                        }

                        // Mask
                        std::optional<mask_node> mask;
                        if (m_proc_params.mask.enabled) {
                            frame mask_frame;
                            detail::read_h5_dset(m_proc_params.mask.path, mask_frame, "mask");
                            mask.emplace(graph, tbb::flow::unlimited, mask_frame);
                        }

                        // Background
                        std::optional<background_node> background;
                        if (m_proc_params.background.enabled) {
                            frame background_frame;
                            detail::read_h5_dset(m_proc_params.background.path, background_frame, "background");
                            background.emplace(graph, tbb::flow::unlimited, background_frame);
                        }

                        // Flatfield
                        std::optional<flatfield_node> flatfield;
                        if (m_proc_params.flatfield.enabled) {
                            frame flatfield_frame;
                            detail::read_h5_dset(m_proc_params.flatfield.path, flatfield_frame, "flatfield");
                            flatfield.emplace(graph, tbb::flow::unlimited, flatfield_frame);
                        }

                        // ROI statistics node
                        std::optional<roi_counters_node<frame>> roi_counters;
                        std::optional<tbb::flow::sequencer_node<roi_counters_result>> roi_counters_sequencer;
                        std::optional<unbounded_buffer_node<roi_counters_result>> roi_counters_buffer;
                        std::optional<checkpoint_node<roi_counters_result>> roi_counters_checkpoint;
                        if (m_proc_params.statistics.enabled) {
                            roi_counters.emplace(graph, tbb::flow::unlimited, m_proc_params.statistics.rect_rois,
                                                 m_proc_params.statistics.arc_rois);
                            roi_counters_sequencer.emplace(
                                graph, [](roi_counters_result const& rc) -> size_t { return rc.recv_idx; });
                            roi_counters_buffer.emplace(graph, tbb::flow::serial, m_roi_statistics_buffer);
                            roi_counters_checkpoint.emplace(graph, tbb::flow::unlimited, m_nb_frames_counters);
                        }

                        // ROI profiles node
                        std::optional<roi_profiles_node<frame>> roi_profiles;
                        std::optional<tbb::flow::sequencer_node<roi_profiles_result>> roi_profiles_sequencer;
                        std::optional<unbounded_buffer_node<roi_profiles_result>> roi_profiles_buffer;
                        std::optional<checkpoint_node<roi_profiles_result>> roi_profiles_checkpoint;
                        if (m_proc_params.profiles.enabled) {
                            roi_profiles.emplace(graph, tbb::flow::unlimited, m_proc_params.profiles.rois,
                                                 m_proc_params.profiles.directions);
                            roi_profiles_sequencer.emplace(
                                graph, [](roi_profiles_result const& rc) -> size_t { return rc.recv_idx; });
                            roi_profiles_buffer.emplace(graph, tbb::flow::serial, m_roi_profiles_buffer);
                            roi_profiles_checkpoint.emplace(graph, tbb::flow::unlimited, m_nb_frames_profiles);
                        }

                        // Sparse
                        sparse_node sparse(graph, tbb::flow::unlimited);
                        circular_buffer_node<sparse_frame> sparse_frame_buffer(graph, tbb::flow::unlimited,
                                                                               m_sparse_frames_buffer);
                        tbb::flow::sequencer_node<sparse_frame> sparse_frame_sequencer(
                            graph, [](auto const& frm) -> size_t { return frm.metadata.recv_idx; });
                        using fill_factor_node =
                            tbb::flow::function_node<sparse_frame, fill_factor_result, tbb::flow::lightweight>;
                        fill_factor_node fill_factor(
                            graph, tbb::flow::serial, [](auto const& frm) -> fill_factor_result {
                                return {frm.metadata.idx, frm.metadata.recv_idx, frm.nb_pixels()};
                            });
                        unbounded_buffer_node<fill_factor_result> fill_factor_buffer(graph, tbb::flow::serial,
                                                                                     m_fill_factor_buffer);

                        // Saving dense
                        std::optional<io_hdf5_node<frame>> io_hdf5;
                        std::optional<checkpoint_node<tbb::flow::continue_msg>> checkpoint_hdf5;
                        if (m_proc_params.saving_dense.enabled) {
                            io_hdf5.emplace(graph, m_proc_params.saving_dense, m_processed_frame_info,
                                            m_metadata_writer);
                            checkpoint_hdf5.emplace(graph, tbb::flow::unlimited, m_nb_frames_dense_saved,
                                                    m_proc_params.saving_dense.nb_frames_per_chunk);
                        } else
                            m_nb_frames_dense_saved = -1;

                        // Saving sparse
                        std::optional<io_hdf5_sparse_node> io_hdf5_sparse;
                        std::optional<checkpoint_node<tbb::flow::continue_msg>> checkpoint_hdf5_sparse;
                        if (m_proc_params.saving_sparse.enabled) {
                            io_hdf5_sparse.emplace(graph, m_proc_params.saving_sparse, m_processed_frame_info,
                                                   m_metadata_writer);
                            checkpoint_hdf5_sparse.emplace(graph, tbb::flow::unlimited, m_nb_frames_sparse_saved,
                                                           m_proc_params.saving_sparse.nb_frames_per_chunk);
                        } else
                            m_nb_frames_sparse_saved = -1;

                        // Connect graph
                        //tbb::flow::make_edge(src, deep_copy1);
                        tbb::flow::sender<frame>* previous = &src;

                        if (m_proc_params.buffers.nb_input_frames_buffer) {
                            tbb::flow::make_edge(src, deep_copy2);
                            tbb::flow::make_edge(deep_copy2, input_frames_buffer);
                            tbb::flow::make_edge(input_frames_buffer, checkpoint_raw);
                        }
                        if (m_proc_params.geometry.enabled) {
                            tbb::flow::make_edge(*previous, *crop);
                            previous = &*crop;
                        }
                        if (m_proc_params.accumulation.enabled) {
                            tbb::flow::make_edge(*previous, *accumulation);
                            previous = &tbb::flow::output_port<0>(*accumulation);
                        }
                        if (m_proc_params.mask.enabled) {
                            tbb::flow::make_edge(*previous, *mask);
                            previous = &*mask;
                        }
                        if (m_proc_params.background.enabled) {
                            tbb::flow::make_edge(*previous, *background);
                            previous = &*background;
                        }
                        if (m_proc_params.flatfield.enabled) {
                            tbb::flow::make_edge(*previous, *flatfield);
                            previous = &*flatfield;
                        }

                        tbb::flow::make_edge(*previous, processed_frames_buffer);
                        tbb::flow::make_edge(processed_frames_buffer, checkpoint_processed);

                        if (m_proc_params.saving_dense.enabled) {
                            tbb::flow::make_edge(*previous, *io_hdf5);
                            tbb::flow::make_edge(*io_hdf5, *checkpoint_hdf5);
                        }

                        if (m_proc_params.statistics.enabled) {
                            tbb::flow::make_edge(*previous, *roi_counters);
                            tbb::flow::make_edge(*roi_counters, *roi_counters_sequencer);
                            tbb::flow::make_edge(*roi_counters_sequencer, *roi_counters_buffer);
                            tbb::flow::make_edge(*roi_counters_buffer, *roi_counters_checkpoint);
                        }

                        if (m_proc_params.profiles.enabled) {
                            tbb::flow::make_edge(*previous, *roi_profiles);
                            tbb::flow::make_edge(*roi_profiles, *roi_profiles_sequencer);
                            tbb::flow::make_edge(*roi_profiles_sequencer, *roi_profiles_buffer);
                            tbb::flow::make_edge(*roi_profiles_buffer, *roi_profiles_checkpoint);
                        }

                        tbb::flow::make_edge(*previous, sparse);
                        tbb::flow::make_edge(sparse, sparse_frame_buffer);
                        tbb::flow::make_edge(sparse, sparse_frame_sequencer);
                        tbb::flow::make_edge(sparse_frame_sequencer, fill_factor);
                        tbb::flow::make_edge(fill_factor, fill_factor_buffer);

                        if (m_proc_params.saving_sparse.enabled) {
                            tbb::flow::make_edge(sparse, *io_hdf5_sparse);
                            tbb::flow::make_edge(*io_hdf5_sparse, *checkpoint_hdf5_sparse);
                        }

                        src.activate();

                        try {
                            graph.wait_for_all();
                            LIMA_LOG(trace, proc) << "Processing finished";
                        } catch (std::exception const& e) {
                            LIMA_LOG(error, proc) << "Processing failed " << e.what();
                            error = e.what();
                        } catch (...) {
                            LIMA_LOG(error, proc) << "Processing failed";
                            error = "Unknown error";
                        }
                    });

                    // Set the is_finished flag and call the callback if registered
                    m_is_finished = true;
                    if (m_on_finished)
                        m_on_finished(error);
                });

                LIMA_LOG(trace, proc) << "Processing activated";
            }

            /// Returns true when the processing has finished
            bool is_finished() const { return m_is_finished; }

            /// Register on_finished callback
            void register_on_finished(finished_callback_t on_finished) { m_on_finished = on_finished; }

            /// Abort the pipeline
            void abort() { m_task_group_context.cancel_group_execution(); }

            /// Push the given frame to the FIFO of frames to process
            void process(input_t const& frm)
            {
                if (frm.metadata.is_final)
                    m_fifo.push(frm); // Wait push

                else if (!m_fifo.try_push(frm)) {
                    LIMA_LOG(error, det) << "FIFO full: frame #" << frm.metadata.idx << " dropped";
                    LIMA_THROW_EXCEPTION(lima::runtime_error("Pipeline FIFO is full")
                                         << boost::errinfo_frame_idx(frm.metadata.idx));
                }
            }

            counters progress_counters() const
            {
                counters res = {(int) m_nb_frames_source,      (int) m_nb_frames_raw,
                                (int) m_nb_frames_processed,   (int) m_nb_frames_counters,
                                (int) m_nb_frames_profiles,    (int) m_nb_frames_dense_saved,
                                (int) m_nb_frames_sparse_saved};

                LIMA_LOG(trace, proc) << "progress_counters()"
                                      << " nb_frames_source=" << m_nb_frames_source
                                      << " nb_frames_raw=" << m_nb_frames_raw
                                      << " nb_frames_processed=" << m_nb_frames_processed
                                      << " nb_frames_counters=" << m_nb_frames_counters
                                      << " nb_frames_profiles=" << m_nb_frames_profiles
                                      << " nb_frames_dense_saved=" << m_nb_frames_dense_saved
                                      << " nb_frames_sparse_saved=" << m_nb_frames_sparse_saved;

                return res;
            }

            /// Pop the available of ROI statistics
            std::vector<roi_counters_result> pop_roi_statistics()
            {
                const std::lock_guard<std::mutex> lock(m_roi_statistics_buffer.mutex);

                std::vector<roi_counters_result> res;
                std::swap(m_roi_statistics_buffer.buffer, res);

                LIMA_LOG(trace, proc) << "pop_roi_statistics nb_frames=" << res.size();

                return res;
            }

            /// Returns the size of the ROI statistics
            std::size_t nb_roi_statistics() const { return m_roi_statistics_buffer.size(); }

            /// Pop the available ROI profiles
            std::vector<roi_profiles_result> pop_roi_profiles()
            {
                const std::lock_guard<std::mutex> lock(m_roi_profiles_buffer.mutex);

                std::vector<roi_profiles_result> res;
                std::swap(m_roi_profiles_buffer.buffer, res);

                LIMA_LOG(trace, proc) << "pop_roi_profiles nb_frames=" << res.size();

                return res;
            }

            /// Returns the size of the ROI profiles
            std::size_t nb_roi_profiles() const { return m_roi_profiles_buffer.size(); }

            /// Pop the available fill factor values
            std::vector<fill_factor_result> pop_fill_factors()
            {
                const std::lock_guard<std::mutex> lock(m_fill_factor_buffer.mutex);

                std::vector<fill_factor_result> res;
                std::swap(m_fill_factor_buffer.buffer, res);

                LIMA_LOG(trace, proc) << "pop_fill_factors nb_frames=" << res.size();

                return res;
            }

            /// Returns the size of the fill factors
            std::size_t nb_fill_factors() const { return m_fill_factor_buffer.size(); }

            /// Returns processed frame for the given index
            std::optional<frame> get_frame(std::size_t frame_idx = -1) const
            {
                LIMA_LOG(trace, proc) << "get_frame(frame_idx=" << frame_idx << ")";
                if (frame_idx == -1)
                    return m_frames_buffer.back();
                else
                    return m_frames_buffer.find([frame_idx](auto frm) { return frame_idx == frm.metadata.idx; });
            }

            /// Returns raw frame for the given index
            std::optional<frame> get_input_frame(std::size_t frame_idx = -1) const
            {
                LIMA_LOG(trace, proc) << "get_input_frame(frame_idx=" << frame_idx << ")";
                if (frame_idx == -1)
                    return m_input_frames_buffer.back();
                else
                    return m_input_frames_buffer.find([frame_idx](auto frm) { return frame_idx == frm.metadata.idx; });
            }

            /// Returns sparse frame for the given index
            std::optional<sparse_frame> get_sparse_frame(std::size_t frame_idx = -1) const
            {
                LIMA_LOG(trace, proc) << "get_sparse_frame(frame_idx=" << frame_idx << ")";
                if (frame_idx == -1)
                    return m_sparse_frames_buffer.back();
                else
                    return m_sparse_frames_buffer.find([frame_idx](auto frm) { return frame_idx == frm.metadata.idx; });
            }

            /// Returns the frame info at various stage of the pipeline
            frame_info_t input_frame_info() const { return m_input_frame_info; }
            frame_info_t processed_frame_info() const { return m_processed_frame_info; }

            /// Returns the current state of the pipeline
            state_enum state() const { return m_state; }

            /// Returns the version of the camera plugin
            std::string version() const { return XPCS_VERSION; }

          private:
            state_enum m_state = state_enum::idle;

            frame_info_t m_input_frame_info;
            frame_info_t m_processed_frame_info;
            proc_params_t m_proc_params;

            io::h5::nx_metadata_writer m_metadata_writer;

            // TBB Flow Graph
            tbb::task_group_context m_task_group_context;

            // Queue of frame (runtime sized)
            tbb::concurrent_bounded_queue<input_t> m_fifo;

            // Circular buffer
            ts_circular_buffer<input_t> m_input_frames_buffer;
            ts_circular_buffer<frame> m_frames_buffer;
            ts_circular_buffer<sparse_frame> m_sparse_frames_buffer;

            // Unbounded buffer
            ts_vector<roi_counters_result> m_roi_statistics_buffer;
            ts_vector<roi_profiles_result> m_roi_profiles_buffer;
            ts_vector<fill_factor_result> m_fill_factor_buffer;

            // Progress counters
            std::atomic_int m_nb_frames_source = 0;
            std::atomic_int m_nb_frames_raw = 0;
            std::atomic_int m_nb_frames_processed = 0;
            std::atomic_int m_nb_frames_counters = 0;
            std::atomic_int m_nb_frames_profiles = 0;
            std::atomic_int m_nb_frames_dense_saved = 0;
            std::atomic_int m_nb_frames_sparse_saved = 0;
            std::atomic_int m_nb_frames_error = 0;

            // Finished
            std::atomic_bool m_is_finished{false};
            finished_callback_t m_on_finished;
            std::future<void> m_finished_future; // last member
        };

        // Pimpl boilerplate
        pipeline::pipeline(frame_info_t const& frame_info, proc_params_t const& proc_params,
                           acq_params_t const& acq_params, det_info_t const& det_info) :
            m_pimpl{std::make_unique<impl>(frame_info, proc_params, acq_params, det_info)}
        {
        }

        pipeline::pipeline(pipeline&&) = default;

        pipeline::~pipeline() = default;

        pipeline& pipeline::operator=(pipeline&&) = default;

        std::string pipeline::version() const { return m_pimpl->version(); }

        void pipeline::activate() { m_pimpl->activate(); }

        bool pipeline::is_finished() const { return m_pimpl->is_finished(); }

        void pipeline::register_on_finished(finished_callback_t on_finished)
        {
            m_pimpl->register_on_finished(on_finished);
        }

        void pipeline::abort() { m_pimpl->abort(); }

        void pipeline::process(input_t const& frm) { m_pimpl->process(frm); }

        counters pipeline::progress_counters() const { return m_pimpl->progress_counters(); }

        std::vector<roi_counters_result> pipeline::pop_roi_statistics() { return m_pimpl->pop_roi_statistics(); }

        std::size_t pipeline::nb_roi_statistics() const { return m_pimpl->nb_roi_statistics(); }

        std::vector<roi_profiles_result> pipeline::pop_roi_profiles() { return m_pimpl->pop_roi_profiles(); }

        std::size_t pipeline::nb_roi_profiles() const { return m_pimpl->nb_roi_profiles(); }

        std::vector<fill_factor_result> pipeline::pop_fill_factors() { return m_pimpl->pop_fill_factors(); }

        std::size_t pipeline::nb_fill_factors() const { return m_pimpl->nb_fill_factors(); }

        std::optional<frame> pipeline::get_frame(std::size_t frame_idx) const { return m_pimpl->get_frame(frame_idx); }

        std::optional<frame> pipeline::get_input_frame(std::size_t frame_idx) const
        {
            return m_pimpl->get_input_frame(frame_idx);
        }

        std::optional<sparse_frame> pipeline::get_sparse_frame(std::size_t frame_idx) const
        {
            return m_pimpl->get_sparse_frame(frame_idx);
        }

        pipeline::frame_info_t pipeline::input_frame_info() const { return m_pimpl->input_frame_info(); }

        pipeline::frame_info_t pipeline::processed_frame_info() const { return m_pimpl->processed_frame_info(); }

    } // namespace xpcs
} // namespace processing::pipelines
} // namespace lima
