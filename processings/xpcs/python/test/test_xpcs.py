from sys import base_prefix
import pytest

import time
import numpy as np
import os
import os.path
from lima import Frame, FrameInfo, Pixel, FileExistsPolicy
from xpcs import Pipeline, Params

frame_info = FrameInfo(width=16, height=8, nb_channels=1, pixel=Pixel.gray16)

def create_and_run(nb_frames, frame_info, params):

    xpcs = Pipeline(frame_info, params)
    assert(xpcs.input_frame_info == frame_info)
    assert(xpcs.processed_frame_info == frame_info)

    def on_finished(error):
        if error:
            print(f"ERROR! {error}")
        else:
            print(f"FINISHED!")
        

    xpcs.register_on_finished(on_finished)

    xpcs.activate()

    for i in range(0, nb_frames):
        frm = Frame(width=frame_info.width, height=frame_info.height, nb_channels=frame_info.nb_channels, pixel=frame_info.pixel_type)
        data = np.array(frm, copy=False)
        data.fill(i)

        frm.metadata.idx = i
        frm.metadata.recv_idx = i
        frm.metadata.is_final = False if i < (nb_frames - 1) else True

        xpcs.process(frm)        

    while not xpcs.is_finished:
        time.sleep(1)
        
    return xpcs

def test_crop():

def test_accumulation():
    
    nb_frames = 20

    params = Params()

    # Accumulation params
    params.accumulation.nb_frames = 5
    params.accumulation.enabled = True
    params.accumulation.pixel_type = frame_info.pixel_type
    
    # Saving params
    params.saving_dense.filename_prefix = "output_dense"
    params.saving_dense.file_exists_policy = FileExistsPolicy.overwrite
    params.saving_dense.nb_frames_per_file = 10
    params.saving_dense.enabled = True

    params.saving_sparse.filename_prefix = "output_sparse"
    params.saving_sparse.file_exists_policy = FileExistsPolicy.overwrite
    params.saving_sparse.nb_frames_per_file = 10
    params.saving_sparse.enabled = False
    
    xpcs = create_and_run(nb_frames, frame_info, params)
    
    pc = xpcs.progress_counters
   
    print(f"progress_counters: {pc}")
    
    assert(pc.nb_frames_input == nb_frames)
    assert(pc.nb_frames_processed == nb_frames / 5)
    
def filename(saving_params) -> os.PathLike:
    filename = f"{saving_params.filename_prefix}_0_{saving_params.filename_suffix}"
    return path.join(saving_params.base_prefix, filename)
    
