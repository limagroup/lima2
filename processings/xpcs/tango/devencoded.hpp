// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstring>
#include <vector>

#include <tango.h>

#include <lima/processing/pipelines/xpcs/sparse_frame.hpp>

namespace lima::tango
{

#pragma pack(push, 1)

struct sparse_header
{
    static const int header_len = 16;

    std::uint16_t width = 0;
    std::uint16_t height = 0;
    std::uint32_t nb_peaks = 0;
    std::uint32_t frame_idx = 0;
};

#pragma pack(pop)

inline Tango::DevEncoded make_devencoded(processing::pipelines::xpcs::sparse_frame const& frm)
{
    Tango::DevEncoded res;
    res.encoded_format = "SPARSE_FRAME";
    res.encoded_data.length(sizeof(sparse_header) + frm.size_in_bytes());

    sparse_header header;
    header.width = frm.width();
    header.height = frm.height();
    header.nb_peaks = frm.nb_pixels();
    header.frame_idx = frm.metadata.idx;

    unsigned char* buffer = res.encoded_data.get_buffer();

    std::memcpy(buffer, &header, sizeof(sparse_header));
    buffer += sizeof(sparse_header);
    std::copy(frm.pixel_indices.begin(), frm.pixel_indices.end(), reinterpret_cast<int*>(buffer));
    buffer += frm.pixel_indices.size() * sizeof(int);
    std::copy(frm.pixel_values.begin(), frm.pixel_values.end(), reinterpret_cast<std::uint16_t*>(buffer));
    buffer += frm.pixel_values.size() * sizeof(std::uint16_t);

    return res;
}

} // namespace lima::tango