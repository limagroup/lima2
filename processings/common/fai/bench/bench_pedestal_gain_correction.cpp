// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <random>

#include <benchmark/benchmark.h>

#include "../test/opencl_fixture.hpp"

static void pedestal_gain_correction(benchmark::State& state)
{
    namespace bcl = boost::compute;

    try {
        opencl_device ocl;

        // Create context
        bcl::context context(ocl.device);

        // Build the kernel
        bcl::program program = bcl::program::create_with_source_file("pedestal_gain_correction.cl", context);
        try {
            // attempt to compile to program
            program.build();
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            state.SkipWithError(program.build_log().c_str());
        }

        // Create 2 queues to which we will push commands for the device.
        bcl::command_queue queue1(context, ocl.device); // For HtD
        bcl::command_queue queue2(context, ocl.device); // For Compute and DtH

        // Input on host
        const unsigned int length = 2048 * 2048;
        const unsigned int nb_gain = 3;
        const unsigned int nb_buffers = 2; // Double buffering

        // Create pinned buffers on the host
        const cl_mem_flags mem_flags = CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR;
        bcl::buffer buffer_in_h(context, sizeof(std::uint16_t) * length * nb_buffers, mem_flags);
        bcl::buffer buffer_gain_h(context, sizeof(float) * length * nb_gain, mem_flags);
        bcl::buffer buffer_pedestal_h(context, sizeof(float) * length * nb_gain, mem_flags);
        bcl::buffer buffer_res_h(context, sizeof(float) * length * nb_buffers, mem_flags);

        // Get mapped pointers to pinned input host buffers
        std::uint16_t* input = (std::uint16_t*) queue1.enqueue_map_buffer(buffer_in_h, CL_MAP_WRITE, 0,
                                                                          sizeof(std::uint16_t) * length * nb_buffers);
        float* gain =
            (float*) queue1.enqueue_map_buffer(buffer_gain_h, CL_MAP_WRITE, 0, sizeof(float) * length * nb_gain);
        float* pedestal =
            (float*) queue1.enqueue_map_buffer(buffer_pedestal_h, CL_MAP_WRITE, 0, sizeof(float) * length * nb_gain);
        float* res =
            (float*) queue1.enqueue_map_buffer(buffer_res_h, CL_MAP_READ, 0, sizeof(float) * length * nb_buffers);

        // Init input data
        std::random_device rd;
        std::uniform_int_distribution<std::uint16_t> gain_dist(0, 0x0002);
        std::uniform_int_distribution<std::uint16_t> val_dist(0, 0x03FF);
        std::generate_n(input, length, [&rd, &gain_dist, &val_dist]() {
            std::uint16_t gain = gain_dist(rd);
            assert(gain < 0x0003);
            gain = (gain == 2) ? 3 : gain;
            std::uint16_t val = val_dist(rd);
            assert(gain < 0x0400);
            return (gain << 14) | val;
        });

        // Init gain
        std::fill_n(gain, length, 1.0f);
        std::fill_n(gain + length, length, 2.0f);
        std::fill_n(gain + length * 2, length, 3.0f);

        // Init pedestal
        std::fill_n(pedestal, length * nb_gain, 0.0f);

        //print(input, length);
        //print(gain, length * nb_gain);
        //print(pedestal, length * nb_gain);

        // Create buffers on the device
        bcl::buffer buffer_in_d(context, sizeof(std::uint16_t) * length * nb_buffers, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_gain_d(context, sizeof(float) * length * nb_gain, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_pedestal_d(context, sizeof(float) * length * nb_gain, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_res_d(context, sizeof(float) * length * nb_buffers, CL_MEM_READ_ONLY);

        // Create 2 identical kernels for each queues
        bcl::kernel kernel1(program, "pedestal_gain_correction");
        bcl::kernel kernel2(program, "pedestal_gain_correction");

        // Warmup
        // Write input to the device
        queue1.enqueue_write_buffer(buffer_in_d, 0, sizeof(std::uint16_t) * length, input);
        queue1.enqueue_write_buffer(buffer_gain_d, 0, sizeof(float) * length * nb_gain, gain);
        queue1.enqueue_write_buffer(buffer_pedestal_d, 0, sizeof(float) * length * nb_gain, pedestal);

        // Run the kernel
        kernel1.set_args(buffer_in_d, buffer_gain_d, buffer_pedestal_d, buffer_res_d, length);
        queue1.enqueue_1d_range_kernel(kernel1, 0, length, 0);

        // Read result from the device to array
        queue1.enqueue_read_buffer(buffer_res_d, 0, sizeof(float) * length, res);

        // Make sure queues are empty
        queue1.finish();

        // Process data
        const int nb_cycles = 1;
        for (auto _ : state) {
            // Write input to the device (non-blocking)
            queue1.enqueue_write_buffer_async(buffer_in_d, 0, sizeof(std::uint16_t) * length, input);

            // Run the kernel
            kernel1.set_args(buffer_in_d, buffer_gain_d, buffer_pedestal_d, buffer_res_d, length);
            queue1.enqueue_1d_range_kernel(kernel1, 0, length, 0);

            // Write input to the device (non-blocking)
            queue2.enqueue_write_buffer_async(buffer_in_d, sizeof(std::uint16_t) * length,
                                              sizeof(std::uint16_t) * length, input + length);

            // Run the kernel
            kernel2.set_args(buffer_in_d, buffer_gain_d, buffer_pedestal_d, buffer_res_d, length);
            queue2.enqueue_1d_range_kernel(kernel2, 0, length, 0);

            // Read result from the device to array
            queue1.enqueue_read_buffer_async(buffer_res_d, 0, sizeof(float) * length, res);

            // Read result from the device to array
            queue2.enqueue_read_buffer_async(buffer_res_d, sizeof(float) * length, sizeof(float) * length,
                                             res + length);
        }

        // Compute expected result
        std::vector<float> expected(nb_buffers * length);
        std::transform(input, input + nb_buffers * length, expected.begin(),
                       [&pedestal, &gain, i = 0](auto in) mutable {
                           // gain level : 2 MSB bits
                           unsigned short lvl = in >> 14;
                           unsigned short val = in & 0x3fff;

                           // G0 = 0b00, G1 = 0b01, G2 = 0b11
                           lvl = (lvl == 3) ? 2 : lvl;

                           float noise = pedestal[i + lvl * length];
                           float factor = gain[i + lvl * length];

                           i++;

                           return (val - noise) * factor;
                       });

        // Check result
        if (!std::equal(res, res + nb_buffers * length, expected.begin(), expected.end()))
            state.SkipWithError("pedestal_gain_correction wrong result");

    } catch (bcl::opencl_error& ex) {
        std::cerr << "CAUGHT: " << ex.error_string() << std::endl;
    } catch (std::exception& ex) {
        std::cerr << "CAUGHT: " << ex.what() << std::endl;
    }
}
BENCHMARK(pedestal_gain_correction);

BENCHMARK_MAIN();
