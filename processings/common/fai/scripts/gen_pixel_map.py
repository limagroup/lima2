import os
import argparse
import re
import numpy as np
import h5py as h5
from contextlib import contextmanager
from tempfile import mkstemp
from subprocess import run

parser = argparse.ArgumentParser(description='Generate detector pixel maps.')
parser.add_argument('--verbose', action='store_true', help='Verbose output')
parser.add_argument('geom_name', help='Geometry name: 500k|4M|16M')
parser.add_argument('conda_base', help='Conda base directory')
parser.add_argument('sls_conda_env', help='SlsDetector conda environment')
parser.add_argument('src_fmt', help='source format: Raw|AsmNG|AsmWG')
parser.add_argument('tgt_fmt', help='target format: Raw|AsmNG|AsmWG')
parser.add_argument('pixel_map', help='output pixel map')
parser.add_argument('map_dim', type=int, help='output map dimentions: 1|2')

args = parser.parse_args()

def size_2_shape(x):
    return [x[1], x[0]]

Geometry = {
    '500k': (1, 1),
    '4M':   (2, 4),
    '16M':  (4, 8),
}

ModSize = np.array((1024, 512))

geom = Geometry[args.geom_name]
det_size = geom * ModSize

det_type = 'jungfraux2'

@contextmanager
def safe_mkstemp(*args, **kws):
    ifile, ifname = mkstemp(*args, **kws)
    try:
        yield ifile, ifname
    finally:
        os.remove(ifname)

def exec_geometry_assembler(det_size, nb_frames = 1, gen_type = '',
                            src_fname = '', src_fmt = 'Raw',
                            tgt_fname = '', tgt_fmt = '', dtype = 'uint16',
                            gap_pixel_val = -1):
    det_size_str = f'{det_size[0]},{det_size[1]}'
    prog_args = (f'{det_type} {det_size_str} {nb_frames} "{gen_type}" '
                 f'{src_fmt} {dtype} "{src_fname}" '
                 f'{tgt_fmt} {dtype} "{tgt_fname}" {gap_pixel_val}')
    prog = 'sls_geometry_assembler'
    cmd = ''
    if args.sls_conda_env:
        cmd += f'. {args.conda_base}/etc/profile.d/conda.sh && ' \
               f'conda activate {args.sls_conda_env} && '
    cmd += f'{prog} {prog_args}'
    if args.verbose:
        print(f'Executing: {cmd}')
    p = run(['bash', '-c', cmd], capture_output=True, text=True)
    if p.stderr:
        print(f'Info:\n{p.stderr}')
    if args.verbose:
        print(f'Output:\n{p.stdout}')
    if p.returncode != 0:
        raise RuntimeError(f'{prog} failed: ret={p.returncode}')
    return p.stdout.split('\n')

xy_re = re.compile('XY<(?P<x>[0-9]+),(?P<y>[0-9]+)>')
def decode_xy(s):
    m = xy_re.match(s)
    if not m:
        raise RuntimeError(f'Invalid output: {s}')
    return tuple(map(int, (m.group('x'), m.group('y'))))

dims_mod_corners_map = {}

def query_geometry_assembler(det_size):
    global dims_mod_corners_map
    if dims_mod_corners_map:
        return dims_mod_corners_map

    for fmt in ['AsmNG', 'AsmWG']:
        out_lines = exec_geometry_assembler(det_size, tgt_fmt=fmt)
        dims_line = out_lines[0].strip()
        dims = []
        for t in dims_line.split():
            dims.append(decode_xy(t))
        mod_corners_line = out_lines[1].strip()
        o, s = mod_corners_line.split()[0].split('x')
        all_xy = [decode_xy(p) for x in mod_corners_line.split()
                               for p in x.split('x')]
        mod_corners = list(zip(all_xy[0::2], all_xy[1::2]))
        dims_mod_corners_map[fmt] = dims, mod_corners
    return dims_mod_corners_map

dims_mod_corners = query_geometry_assembler(det_size)
(raw_mod_size, asm_ng_det_size), mod_corners_ng = dims_mod_corners['AsmNG']
(raw_mod_size, asm_wg_det_size), mod_corners_wg = dims_mod_corners['AsmWG']
nb_frames = 1
gen_type = 'pixel_idx'
iname = ''
dtype = 'int32' if args.tgt_fmt == 'AsmWG' else 'uint32'
with safe_mkstemp() as mks_data:
    ofile_fd, oname = mks_data
    os.close(ofile_fd)
    exec_geometry_assembler(det_size, nb_frames, gen_type, iname, args.src_fmt,
                            oname, args.tgt_fmt, dtype)
    asm_size = asm_wg_det_size if args.tgt_fmt == 'AsmWG' else asm_ng_det_size
    out_pixels = np.prod(asm_size)
    exp_osize = nb_frames * out_pixels * np.dtype(dtype).itemsize
    osize = os.stat(oname).st_size
    if osize != exp_osize:
        raise RuntimeError(f'Invalid size: {osize}, expected {exp_osize}')
    data = np.fromfile(oname, dtype)
    if args.map_dim == 2:
        mod_shape = np.array(size_2_shape(ModSize))
        geom_shape = np.array(size_2_shape(geom))
        det_shape = np.array(size_2_shape(det_size))
        if args.tgt_fmt == 'Raw':
            out_shape = (geom_shape.prod(), 1) * mod_shape
        elif args.tgt_fmt == 'AsmNG':
            out_shape = det_shape
        else:
            out_shape = size_2_shape(asm_wg_det_size)
        data.resize(out_shape)

with h5.File(args.pixel_map, 'w') as f:
    f.create_dataset('/data', data=data)
