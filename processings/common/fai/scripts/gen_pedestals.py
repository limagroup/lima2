#!/usr/bin/env python

import argparse
import numpy as np
import h5py as h5
import functools
from multiprocessing.pool import Pool
from multiprocessing.sharedctypes import RawArray

nb_gains = 3

parser = argparse.ArgumentParser(description='Process pedestals for each gain.')
for g in range(nb_gains):
    parser.add_argument(f'path_gain{g}',
                        help=f'pedestal acquisition for gain{g}')
parser.add_argument('pedestals', help='output file with pedestals')

args = parser.parse_args()

data_location_prefix = '/entry_0000/instrument'

block_lines = 32

pedestal = None

for g in range(nb_gains):
    fname = getattr(args, f'path_gain{g}')
    print(f'Reading {fname} ...')
    with h5.File(fname, 'r') as file_gain:
        in_data = None
        for instrument in file_gain[data_location_prefix]:
            path = f'{data_location_prefix}/{instrument}/data'
            try:
                in_data = file_gain[path]
                break
            except:
                pass
        else:
            raise RuntimeError(f'Could not find data in file {fname}')

        height, width = in_data.shape[1:]
        if pedestal is None:
            ctypes_len = nb_gains * height * width
            array = RawArray('f', ctypes_len)
            np_shape = (nb_gains, height, width)
            pedestal = np.frombuffer(array, 'float32').reshape(np_shape)
        out_data = pedestal[g]

        def ave(first_line):
            lines = slice(first_line, first_line + block_lines)
            out_data[lines] = np.average(in_data[:,lines] & 0x3FFF, axis=0)

        with Pool() as p:
            p.map(ave, range(0, height, block_lines))

with h5.File(args.pedestals, 'w') as output:
    print(f'Writing {args.pedestals} ...')
    output.create_dataset('data', data=pedestal)
