#!/usr/bin/env python

import argparse
import numpy as np
import h5py as h5

parser = argparse.ArgumentParser(description='Query information on sparse data')
parser.add_argument('--peak_index', type=int, default=-1,
                    help='Index of the peak in CSR')
parser.add_argument('--print_peak_index', action='store_true',
                    help='Print the index of the frame')
parser.add_argument('--frame_index', type=int, default=-1,
                    help='Index of the frame')
parser.add_argument('sparse_data_file',
                    help='Input sparse data file')
parser.add_argument('sparse_data_path', nargs='?',
                    help='HDF5/Nexus path to the sparse dataset')

DefaultDataPaths = ['/entry_0000/measurement/data', '/data']

def size_2_shape(x):
    return [x[1], x[0]]

shape_2_size = size_2_shape

args = parser.parse_args()

with h5.File(args.sparse_data_file) as f:
    if args.sparse_data_path:
        data_path = args.sparse_data_path
    else:
        for data_path in DefaultDataPaths:
            try:
                m = f[data_path]['mask']
                break
            except:
                pass
        else:
            raise ValueError('Could not find HDF5/Nexus dataset')

    print(f'Reading sparse data from {data_path} dataset')
    data = f[data_path]
    det_size = shape_2_size(data['mask'].shape)
    nb_frames = data['frame_ptr'].shape[0] - 1
    print(f'Detector size: {det_size}, number of frames: {nb_frames}')

    def get_peak_info(idx):
        frame = max(0, np.searchsorted(data['frame_ptr'], idx) - 1)
        pixel_index = data['index'][idx]
        y, x = divmod(pixel_index, det_size[0])
        val = data['intensity'][idx]
        return dict(idx=idx, frame=frame, coord=(x, y), val=val)

    index_list = []
    if args.peak_index >= 0:
        index_list = [args.peak_index]
    elif args.frame_index >= 0:
        frame = args.frame_index
        start, stop = data['frame_ptr'][frame:frame + 2]
        index_list = range(start, stop)

    info = [get_peak_info(idx) for idx in index_list]

def print_info(info):
    first = 0 if args.print_peak_index else 1
    cols = [('idx', ''), ('frame', ''), ('coord', ''), ('val', ':.1f')][first:]
    headers = [x.capitalize() for x, y in cols]
    fields = []
    width = [0] * len(cols)
    for i in info:
        line = []
        for w, (c, fmt) in enumerate(cols):
            f = f'{{{c}{fmt}}}'
            s = f.format(**i)
            width[w] = max(len(s), width[w])
            line.append(s)
        fields.append(line)
    for f in fields:
        l = []
        for h, c, w in zip(headers, f, width):
            l.append('%s: %*s' % (h, w, c))
        print(', '.join(l))

def sort_info(l):
    def key(x):
        return '{frame:05d} {coord[1]:04d} {coord[0]:04d}'.format(**x)
    return sorted(l, key=key)

print_info(sort_info(info))
