#!/usr/bin/env python

import os
import argparse
import numpy as np
import h5py as h5
from pyFAI.detectors import Detector

parser = argparse.ArgumentParser(description='Generate detector config.')
parser.add_argument('--pixel_map', help='Geometry raw-in-asm pixel map')
parser.add_argument('--det_size', help='Detector gemetry: width,height')
parser.add_argument('det_config_asm',
                    help='output detector assembled config file name')
parser.add_argument('det_config_raw', nargs='?',
                    help='output detector raw config file name')

args = parser.parse_args()

def size_2_shape(x):
    return [x[1], x[0]]

Geometry = {
    '500k': (1, 1),
    '4M':   (2, 4),
    '16M':  (4, 8),
}

ChipSize = np.array((256, 256))
ModChips = np.array((4, 2))
ModSize = ChipSize * ModChips
module_shape = np.array(size_2_shape(ModSize))

InterChipGap = np.array((2, 2))
InterModGap = (9, 36)
J16QuadDisp = 34

pixel_size = 75e-6

geom_asm_ng = False
if args.pixel_map:
    with h5.File(args.pixel_map) as f:
        geom_pixel_idx = f['data'][:]

    det_shape = geom_pixel_idx.shape
    geom_asm_ng = True

    for name, g in Geometry.items():
        geom = np.array(g)
        det_size_ng = geom * ModSize
        gap_pixels = InterChipGap * (ModChips - 1) * geom + InterModGap * (geom - 1)
        if name == '16M':
            gap_pixels += np.array((2, 2)) * J16QuadDisp
        det_size_wg = det_size_ng + gap_pixels
        print(f'det_size_ng={det_size_ng}, det_size_wg={det_size_wg}')
        if size_2_shape(det_size_ng) == list(det_shape):
            print(f'Detector: {name}-NG')
        if size_2_shape(det_size_wg) == list(det_shape):
            print(f'Detector: {name}-WG')
            geom_asm_ng = False
            break
    else:
        raise RuntimeError(f'No detector found with shape {det_shape}')

    module_assembly = size_2_shape(geom)
    nb_mods = np.prod(module_assembly)
    stack_shape = tuple((nb_mods, 1) * module_shape)
    input_shape = stack_shape if geom_asm_ng else det_shape
    print(f"input: {input_shape}, output: {det_shape}")
elif args.det_size:
    det_size = [int(x) for x in args.det_size.split(',')]
    det_shape = size_2_shape(det_size)
    print(f"input/output: {det_shape}")
else:
    raise ValueError('Need one option among pixel_map and det_size')

JF_asm = Detector(pixel1=pixel_size, pixel2=pixel_size, max_shape=det_shape)
print(JF_asm)
os.system(f'rm -rf {args.det_config_asm}')
JF_asm.save(args.det_config_asm)

if not (geom_asm_ng and args.det_config_raw):
    exit(0)

JF_stack = Detector(pixel1=pixel_size, pixel2=pixel_size, max_shape=stack_shape)
print(JF_stack)

corners_stack = JF_stack.get_pixel_corners()
corners_asm = JF_asm.get_pixel_corners()
JF_stack.IS_CONTIGUOUS = False

# merge first two dimensions for a linear stacked perspective
flat_corners_stack = corners_stack.reshape([-1] + list(corners_stack.shape[2:]))
mod_h = module_shape[0]
for lines in [slice(l, l + mod_h) for l in range(0, det_shape[0], mod_h)]:
    flat_corners_stack[geom_pixel_idx[lines]] = corners_asm[lines]

JF_stack.set_pixel_corners(corners_stack)
os.system(f'rm -rf {args.det_config_raw}')
JF_stack.save(args.det_config_raw)
