import argparse
import numpy as np
import h5py as h5
from functools import reduce

parser = argparse.ArgumentParser(description='Decode OpenCL input parameters.')
parser.add_argument('--pixel_map', help='Geometry raw-in-asm pixel map')
parser.add_argument('--occurrence', type=int, default=30,
                    help='Hot event occurrence, in percent')
parser.add_argument('data', help='input data frames')
parser.add_argument('threshold', type=float, help='threshold value for mask')
parser.add_argument('mask', help='output mask map')

args = parser.parse_args()

data_location_prefix = '/entry_0000/instrument'

mask = None
if args.pixel_map:
    with h5.File(args.pixel_map) as f:
        pixel_map = f['/data'][:]
        mask = pixel_map < 0

with h5.File(args.data) as f:
    in_data = None
    for instrument in f[data_location_prefix]:
        path = f'{data_location_prefix}/{instrument}/data'
        try:
            in_data = f[path]
            break
        except:
            pass
    else:
        raise RuntimeError(f'Could not find data in file {fname}')

    if mask is None:
        mask = np.zeros(in_data.shape[-2:], 'bool')

    min_occ = args.occurrence * len(in_data) / 100
    hot_mask = (in_data[:] >= args.threshold).sum(0) >= min_occ
    mask |= hot_mask

with h5.File(args.mask, 'w') as f:
    f.create_dataset('/data', data=mask)
