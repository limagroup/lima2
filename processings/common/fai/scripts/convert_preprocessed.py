import numpy as np
import h5py as h5

def size_2_shape(x):
    return [x[1], x[0]]

data_dir = '/tmp'
nb_frames = 10
in_fnames = [f'{data_dir}/preprocessed_{i}.bin' for i in range(nb_frames)]
out_fname = f'{data_dir}/preprocessed.h5'

det_size = (1024, 512)
det_shape = size_2_shape(det_size)
in_dtype = 'float32'

frame_data_shape = det_shape + [4]
data = np.zeros([nb_frames] + frame_data_shape, in_dtype)

for fname, d in zip(in_fnames, data):
    d[:,:] = np.fromfile(fname, in_dtype).reshape(frame_data_shape)
    
with h5.File(out_fname, 'w') as out_file:
    out_file.create_dataset('/data', data=data)
