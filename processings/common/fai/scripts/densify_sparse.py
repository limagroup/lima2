import argparse
import h5py as h5
import numpy as np

parser = argparse.ArgumentParser(description='Densify a sparse dataset.')
group = parser.add_mutually_exclusive_group()
group.add_argument('--only_peaks', action='store_true',
                    help='Only include the peaks')
group.add_argument('--only_background', action='store_true',
                    help='Only include the background')
group.add_argument('--only_mask', action='store_true',
                    help='Only include the pixel mask')
group.add_argument('--only_radius2d', action='store_true',
                    help='Only include the radius')
parser.add_argument('--masked_value', type=float, default='0.0',
                    help='Masked pixel value: float or "NaN"')
parser.add_argument('in_sparse', help='Input sparse filename')
parser.add_argument('out_dense', help='Output dense filename')

args = parser.parse_args()

one_frame = args.only_mask or args.only_radius2d

add_peaks = not (args.only_background or args.only_mask or args.only_radius2d)

data_location_prefix = '/entry_0000/instrument'

with h5.File(args.in_sparse, 'r') as h5_in_file:
    sparse_data = None
    for instrument in h5_in_file[data_location_prefix]:
        path = f'{data_location_prefix}/{instrument}/data'
        try:
            sparse_data = h5_in_file[path]
            break
        except:
            pass
    else:
        raise RuntimeError(f'Could not find data in file {args.in_sparse}')

    background_avg = sparse_data['background_avg']
    frame_ptr = sparse_data['frame_ptr']
    index = sparse_data['index']
    intensity = sparse_data['intensity']
    mask = sparse_data['mask']
    radius = sparse_data['radius']

    nb_bins = len(radius)
    nb_frames = 1 if one_frame else len(background_avg)

    radial_function = None
    if args.only_radius2d:
        radial_function = radius
    elif args.only_peaks:
        radial_function = np.zeros(radius.shape, 'float32')
    elif args.only_mask:
        radial_function = np.ones(radius.shape, 'float32')

    if radial_function is not None:
        radial_array = np.zeros((nb_frames, nb_bins), 'float32')
        radial_array[:] = radial_function
    else:
        radial_array = background_avg[:nb_frames]

    height, width = mask.shape
    print(f'Re-densifying {nb_frames} frame(s) of {width}x{height} pixels '
          f'from {nb_bins} bins...')

    frames = []
    masked = np.where(np.logical_not(np.isfinite(mask)))
    for idx, radial_spectrum in enumerate(radial_array):
        dense = np.interp(mask, radius, radial_spectrum)
        if add_peaks:
            flat = dense.ravel()
            start, stop = frame_ptr[idx:idx+2]
            flat[index[start:stop]] = intensity[start:stop]
        dense[masked] = args.masked_value
        frames.append(dense.astype(intensity.dtype))

with h5.File(args.out_dense, 'w') as h5_out_file:
    h5_out_file.create_dataset('data', data=np.array(frames))
    
