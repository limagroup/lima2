import argparse
import numpy as np
import h5py as h5
from multiprocessing.pool import Pool
from multiprocessing.sharedctypes import RawArray

parser = argparse.ArgumentParser(description='Decode OpenCL input parameters.')
parser.add_argument('--mask', help='mask map')
parser.add_argument('--pixel_rev_map',
                    help='Geometry asm-in-raw pixel (rev) map')
parser.add_argument('csr', help='input CSR for clipping')
parser.add_argument('csr_map', help='output map from CSR')
parser.add_argument('r_center', help='input R-Center map')
parser.add_argument('r_center_map', help='output R-Center assembled map')

args = parser.parse_args()

ModSize = (1024, 512)

def size_2_shape(x):
    return [x[1], x[0]]

shape_2_size = size_2_shape

mask = None
if args.mask:
    with h5.File(args.mask) as f:
        mask = f['/data'][:]

geom_pixel_idx = None
if args.pixel_rev_map:
    with h5.File(args.pixel_rev_map) as f:
        geom_pixel_idx = f['data'][:]
    det_pixels = geom_pixel_idx.shape[0]
    det_size = None
elif mask is not None:
    det_size = shape_2_size(mask.shape)
    det_pixels = np.prod(det_size)
else:
    raise ValueError('None of mask nor pixel_rev_map options provided')

with h5.File(args.csr) as f:
    csr_data = f['data'][:]
    csr_indices = f['indices'][:]
    csr_indptr = f['indptr'][:]

nb_pixels = csr_indices.size
if nb_pixels != det_pixels:
    raise ValueError(f'Det. pixels ({det_pixels}) / nb_pixels ({nb_pixels}) '
                     f'mismatch')
if det_size is None:
    for name, g in Geometry.items():
        geom = np.array(g)
        det_size = geom * ModSize
        if det_size.prod() == nb_pixels:
            print(f'Detector: {name}')
            break
    else:
        raise RuntimeError(f'No detector found for {nb_pixels} pixels')

nb_bins = csr_indptr.size - 1
ctypes_type = np.ctypeslib.as_ctypes_type(csr_data.dtype)
ctypes_len = nb_bins * nb_pixels
array = RawArray(ctypes_type, ctypes_len)
data_map = np.frombuffer(array, csr_data.dtype).reshape((nb_bins, nb_pixels))
if mask is not None:
    data_map[:,mask.flatten() == True] = -1

def fill_bin(r):
    indices = np.arange(*csr_indptr[r:r+2])
    pixels = csr_indices[indices]
    if geom_pixel_idx is not None:
        pixels = geom_pixel_idx[pixels]
    data_map[r][pixels] += csr_data[indices]

with Pool() as p:
    p.map(fill_bin, range(nb_bins))

with h5.File(args.csr_map, 'w') as of:
        map_shape = [nb_bins] + size_2_shape(det_size)
        of.create_dataset('data', data=data_map.reshape(map_shape))

with h5.File(args.r_center) as f:
    mod_shape = size_2_shape(ModSize)
    r_center = f['data'][:].flatten()
    r_center_map = np.zeros(nb_pixels, r_center.dtype)
    indices = np.indices(r_center.shape)
    if geom_pixel_idx is not None:
        indices = geom_pixel_idx[indices]
    r_center_map[indices] = r_center

    with h5.File(args.r_center_map, 'w') as of:
        d = r_center_map.reshape(size_2_shape(det_size))
        of.create_dataset('data', data=d)
