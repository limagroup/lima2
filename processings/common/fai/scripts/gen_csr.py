#!/usr/bin/env python

import argparse
import numpy as np
import h5py as h5
#import scipy.sparse as sp


parser = argparse.ArgumentParser(description='Generate CSR files.')
parser.add_argument('det_config', help='input detector config file name')
parser.add_argument('sample_distance', type=float,
                    help='sample-to-detector distance [in ?]')
parser.add_argument('beam_position', help='position of the beam in pixels: x,y')
parser.add_argument('nb_bins', type=int,
                    help='number of radial bins for FAI')
parser.add_argument('csr', help='output CSR for clipping')
parser.add_argument('bin_centers', help='output bin R-Center relation')
parser.add_argument('r_center', help='output R-Center map')

args = parser.parse_args()

#from pyFAI.detectors import Detector,
from pyFAI.detectors import NexusDetector
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator

JF = NexusDetector(args.det_config)
beam_position = [int(x) for x in args.beam_position.split(',')]
ai = AzimuthalIntegrator(detector=JF)
ai.setFit2D(args.sample_distance, *beam_position)
csr = ai.setup_CSR(None, args.nb_bins, unit="r_m", split="no")

#sp.save_npz('csr.npz', sp.csr_matrix(csr.lut))
#np.save('bin_centers.npy', csr.bin_centers, csr.bin_centers)
#np.save('r_center.npy', ai._cached_array["r_center"])

with h5.File(args.csr, 'w') as f:
    f.create_dataset('/data', data=csr.lut[0])
    f.create_dataset('/indices', data=csr.lut[1])
    f.create_dataset('/indptr', data=csr.lut[2])

with h5.File(args.bin_centers, 'w') as f:
    f.create_dataset('/data', data=csr.bin_centers)

with h5.File(args.r_center, 'w') as f:
    f.create_dataset('/data', data=ai._cached_array["r_center"])
