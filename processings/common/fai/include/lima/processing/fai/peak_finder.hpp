// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cassert>

#include <functional>
#include <memory>
#include <filesystem>
#include <type_traits>

#include <boost/compute.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

#include <lima/processing/fai/enums.hpp>
#include <lima/processing/fai/typedefs.hpp>

namespace lima
{
namespace processing::fai
{
    namespace bcl = boost::compute;

    inline auto build_with_source_file(bcl::context& context, const std::filesystem::path& cl_source_path,
                                       const std::string& file, const std::string& options = std::string())
    {
        std::string final_options = options;
        auto file_path = file;
        if (!cl_source_path.string().empty()) {
            std::string sep = final_options.empty() ? "" : " ";
            final_options += sep + "-I" + cl_source_path.string();
            file_path = cl_source_path / file;
        }
        bcl::program res = bcl::program::create_with_source_file(file_path, context);
        try {
            // attempt to compile to program
            res.build(final_options);
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            std::cerr << res.build_log();
            LIMA_THROW_EXCEPTION(std::runtime_error(res.build_log()));
        }

        return res;
    }

    template <typename T>
    inline auto make_peak_finder(bcl::context& context,                                         //
                                 std::size_t width, std::size_t height,                         //
                                 error_model_enum error_model,                                  // FAI
                                 vector<float> const& variance,                                 // Preprocessing
                                 vector<bool> mask,                                             //
                                 vector<float> const& dark, vector<float> const& dark_variance, //
                                 vector<float> const& flat,                                     //
                                 vector<float> const& solid_angle,                              //
                                 vector<float> const& polarization,                             //
                                 vector<float> const& absorption,                               //
                                 float dummy, float delta_dummy,                                //
                                 float normalization_factor,                                    //
                                 vector<float> const& csr_data,                                 // Sigma clipping
                                 vector<int> const& csr_indices,                                //
                                 vector<int> const& csr_indptr,                                 //
                                 float cutoff_clip,                                             //
                                 int cycle,                                                     //
                                 float empty,                                                   //
                                 vector<float> const& radius2d,                                 // Peak finding
                                 vector<float> const& radius1d,                                 //
                                 float noise,                                                   //
                                 float cutoff_pick,                                             //
                                 int acc_nb_frames_reset,                                       // Accumulation
                                 int acc_nb_frames_xfer,
                                 std::filesystem::path const& cl_source_path, // Path to the CL sources
                                 bcl::command_queue& queue)                   //
    {
        const std::size_t nb_pixels = width * height;
        const std::size_t nb_bins = csr_indptr.size() - 1;

        // Preconditions
        if (normalization_factor == 0.0)
            LIMA_THROW_EXCEPTION(lima::invalid_argument("peak_finder: fai.normalization_factor is 0"));

        auto build_program = [&cl_source_path](auto&& context, auto&& file,
                                               const std::string& options = std::string()) {
            return build_with_source_file(context, cl_source_path, file, options);
        };

        // Stage 0 - Initialization
        auto stage0_program = build_program(context, "init.cl");
        bcl::kernel stage0(stage0_program, "init");

        // Stage 1 - Conversion to float
        constexpr bool need_convert_to_float = !std::is_same_v<T, float>;
        bool do_norm = (normalization_factor != 1.0);
        bcl::vector<float> norm_d(1, normalization_factor, queue);
        bcl::vector<float> input_d(nb_pixels, context);

        // Stage 2 - Preprocessing
        auto stage2_program = build_program(context, "preprocess.cl");
        bcl::vector<float> variance_d(variance, queue);
        bcl::vector<float> dark_d(dark, queue);
        bcl::vector<float> dark_variance_d(dark_variance, queue);
        bcl::vector<float> flat_d(flat, queue);
        bcl::vector<float> solid_angle_d(solid_angle, queue);
        bcl::vector<float> polarization_d(polarization, queue);
        bcl::vector<float> absorption_d(absorption, queue);
        bcl::vector<char> mask_d(mask.size(), context);
        bool do_dark = !dark_d.empty();
        bool do_dark_variance = !dark_variance_d.empty();
        bool do_flat = !flat_d.empty();
        bool do_solid_angle = !solid_angle_d.empty();
        bool do_polarization = !polarization_d.empty();
        bool do_absorption = !absorption_d.empty();
        bool do_mask = !mask_d.empty();
        bool do_dummy = false;
        bool do_acc = (acc_nb_frames_xfer > 0);
        bool do_acc_reset = do_acc && (acc_nb_frames_reset > 0);
        bcl::vector<bcl::float4_> preprocessed_d(nb_pixels, context); //!< Preprocessed data
        bcl::copy(mask.begin(), mask.end(), mask_d.begin(), queue);
        bcl::kernel stage2(stage2_program, "corrections4");

        // Stage 3 - Sigma clipping
        constexpr int sigma_clip_wg_size = 32;
        auto stage3_program = build_program(context, "ocl_azim_CSR.cl");
        bcl::vector<float> csr_data_d(csr_data, queue);
        bcl::vector<int> csr_indices_d(csr_indices, queue);
        bcl::vector<int> csr_indptr_d(csr_indptr, queue);
        bcl::vector<bcl::float8_> summed_d(nb_bins, context);
        bcl::vector<float> stderrmean_d(nb_bins, context);
        bcl::local_buffer<bcl::float8_> sigma_clip_shared_d(sigma_clip_wg_size);
        bcl::kernel stage3(stage3_program, "csr_sigma_clip4");

        // Stage 4 - Peak finding
        constexpr int peak_finder_wg_size = 1024;
        auto peak_finder_nb_groups = (nb_pixels - 1) / peak_finder_wg_size + 1;
        const std::size_t peak_finder_nb_pixels = peak_finder_nb_groups * peak_finder_wg_size;
        auto stage4_program = build_program(context, "sparsify.cl");
        bcl::vector<float> radius2d_d(radius2d, queue);
        bcl::vector<float> radius1d_d(radius1d, queue);
        bcl::local_buffer<int> peak_finder_shared_d(peak_finder_wg_size);
        float radius_min = 0.0;
        float radius_max = std::numeric_limits<float>::max();
        bcl::vector<int> counter_d(1, context);
        bcl::kernel stage4(stage4_program, "find_intense");

        // Stage 5 - Accumulate
        auto acc_nb_pixels = do_acc ? nb_pixels : 0;
        bcl::vector<float> acc_corr_d(acc_nb_pixels, 0.0f, queue); //!< Accum pixels after correction
        bcl::vector<float> acc_peak_d(acc_nb_pixels, 0.0f, queue); //!< Accum pixels after peak find

        std::uint32_t curr_acc_frames = 0;

        return [=,                                                    //
                norm = std::move(norm_d),                             //
                input_d = std::move(input_d),                         //
                                                                      //
                variance = std::move(variance_d),                     //
                dark = std::move(dark_d),                             //
                dark_variance = std::move(dark_variance_d),           //
                flat = std::move(flat_d),                             //
                solid_angle = std::move(solid_angle_d),               //
                polarization = std::move(polarization),               //
                absorption = std::move(absorption_d),                 //
                mask = std::move(mask_d),                             //
                preprocessed = std::move(preprocessed_d),             //
                                                                      //
                csr_data = std::move(csr_data_d),                     //
                csr_indices = std::move(csr_indices_d),               //
                csr_indptr = std::move(csr_indptr_d),                 //
                summed = std::move(summed_d),                         //
                stderrmean = std::move(stderrmean_d),                 //
                sigma_clip_shared = std::move(sigma_clip_shared_d),   //
                                                                      //
                radius2d = std::move(radius2d_d),                     //
                radius1d = std::move(radius1d_d),                     //
                peak_finder_shared = std::move(peak_finder_shared_d), //
                counter = std::move(counter_d),                       //
                                                                      //
                acc_corr_d = std::move(acc_corr_d),                   //
                acc_peak_d = std::move(acc_peak_d)                    //
        ](bcl::command_queue& queue,                                  //
               vector<T> const& input,                                // Input
               vector<bcl::float4_>& preprocessed_dbg,                // Preprocessed
               vector<bcl::float8_>& cliped_dbg,                      // Cliped (after sigma clip)
               vector<bcl::float4_>& found_dbg,                       // Found (after peak find)
               vector<int>& peak_idx, vector<float>& peak_val,        // Outputs Peaks
               vector<float>& bkg_avg, vector<float>& bkg_std,        //         Background
               vector<float>& acc_corr, vector<float>& acc_peak,      //         Accumulation
               bool acc_always_xfer) mutable {
            // Check if reset is needed (0 means "accumulate indefinitely")
            if (do_acc_reset && (curr_acc_frames > 0) && (curr_acc_frames % acc_nb_frames_reset == 0)) {
                curr_acc_frames = 0;
                bcl::fill(acc_corr_d.begin(), acc_corr_d.end(), 0.0f, queue);
                bcl::fill(acc_peak_d.begin(), acc_peak_d.end(), 0.0f, queue);
            }

            // Stage 0 - Initialization (might not be necessary)
            stage0.set_args(bkg_avg, bkg_std, summed, (unsigned int) nb_bins);
            queue.enqueue_1d_range_kernel(stage0, 0, nb_bins, 0 /*max = 1024 for Tesla*/);

            // Stage 1 - Convert to float / normalization
            if (need_convert_to_float || do_norm) {
                BOOST_COMPUTE_CLOSURE(float, convert_to_float, (T v), (norm), { return (float) v / norm[0]; });
                bcl::transform(input.begin(), input.end(), input_d.begin(), convert_to_float, queue);
            } else
                bcl::copy(input.begin(), input.end(), input_d.begin(), queue);

            // Stage 2 - Preprocessing
            stage2.set_args(input_d, (char) error_model, /*variance*/ input_d, (char) do_dark, dark,
                            (char) do_dark_variance, dark_variance, (char) do_flat, flat, (char) do_solid_angle,
                            solid_angle, (char) do_polarization, polarization, (char) do_absorption, absorption,
                            (char) do_mask, mask, (char) do_dummy, dummy, delta_dummy, 1.0f, (char) do_acc, acc_corr_d,
                            preprocessed, (unsigned int) nb_pixels);
            queue.enqueue_1d_range_kernel(stage2, 0, nb_pixels, 0);

            // DBG
            if (!preprocessed_dbg.empty())
                bcl::copy(preprocessed.begin(), preprocessed.end(), preprocessed_dbg.begin(), queue);

            // Stage 3 - Sigma clipping
            stage3.set_args(preprocessed, csr_data, csr_indices, csr_indptr, cutoff_clip, cycle, (char) error_model,
                            empty, summed, bkg_avg, bkg_std, stderrmean, sigma_clip_shared);
            queue.enqueue_1d_range_kernel(stage3, 0, nb_bins * sigma_clip_wg_size, // One workgroup par bin
                                          sigma_clip_wg_size);

            // DBG
            if (!cliped_dbg.empty())
                bcl::copy(summed.begin(), summed.end(), cliped_dbg.begin(), queue);

            // Stage 4 - Peak finding
            bcl::fill_async(counter.begin(), counter.end(), 0, queue);
            stage4.set_args(preprocessed, radius2d, radius1d, bkg_avg, bkg_std, radius_min, radius_max, cutoff_pick,
                            noise, (unsigned int) nb_pixels, (unsigned int) nb_bins, counter, peak_idx, peak_val,
                            (char) do_acc, acc_peak_d, peak_finder_shared);
            queue.enqueue_1d_range_kernel(stage4, 0, peak_finder_nb_pixels, peak_finder_wg_size);

            int nb_peaks;
            bcl::copy(counter.begin(), counter.end(), &nb_peaks, queue);

            // DBG
            if (!found_dbg.empty())
                bcl::copy(preprocessed.begin(), preprocessed.end(), found_dbg.begin(), queue);

            // Stage 5 - Copy peaks
            BOOST_COMPUTE_CLOSURE(float, copy_peaks, (unsigned int idx), (preprocessed),
                                  { return preprocessed[idx].s1; });
            bcl::transform(peak_idx.begin(), peak_idx.begin() + nb_peaks, peak_val.begin(), copy_peaks, queue);

            // Stage 6 - Accumulate corr & peaks
            std::size_t ret_acc_frames = 0;
            if (do_acc) {
                // Increment nb of accumulated frames
                curr_acc_frames++;

                // Check if transfer is needed
                if ((curr_acc_frames % acc_nb_frames_xfer == 0) || acc_always_xfer) {
                    ret_acc_frames = curr_acc_frames;
                    bcl::copy(acc_corr_d.begin(), acc_corr_d.end(), acc_corr.begin(), queue);
                    bcl::copy(acc_peak_d.begin(), acc_peak_d.end(), acc_peak.begin(), queue);
                }
            }

            return std::make_tuple(std::size_t(nb_peaks), ret_acc_frames);
        };
    };

    //using peak_finder = decltype(make_peak_finder(std::declval<bcl::context>(), std::declval<bcl::command_queue>()));
    using ret_t = std::tuple<std::size_t, std::size_t>;
    template <typename T>
    using peak_finder = std::function<ret_t(bcl::command_queue&,                    //
                                            vector<T> const&,                       // Input
                                            vector<bcl::float4_>& preprocessed_dbg, // Preprocessed
                                            vector<bcl::float8_>& cliped_dbg,       // Summed (after sigma clip)
                                            vector<bcl::float4_>& found_dbg,        // Finded (after peak find)
                                            vector<int>&, vector<float>&,           // Outputs Peaks
                                            vector<float>&, vector<float>&,         //         Background
                                            vector<float>&, vector<float>&, bool    //         Accumulation
                                            )>;

} // namespace processing::fai
} // namespace lima
