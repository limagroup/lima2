// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/compute/allocator/pinned_allocator.hpp>
#include <boost/compute/container/vector.hpp>

namespace lima
{
namespace processing::fai
{
    // A host vector with pinned memory
    template <typename T>
    using vector = boost::compute::vector<T, boost::compute::pinned_allocator<T>>;
} // namespace processing::fai
} // namespace lima