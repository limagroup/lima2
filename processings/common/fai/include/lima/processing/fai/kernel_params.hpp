// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cassert>
#include <filesystem>

#include <boost/json.hpp>
#include <boost/json/filesystem.hpp>

#include <boost/exception/errinfo_file_name.hpp>
#include <boost/compute/command_queue.hpp>
#include <boost/compute/algorithm/transform.hpp>
#include <boost/scope/scope_fail.hpp>

#include <lima/io/h5/wrapper.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

#include <lima/processing/fai/enums.hpp>
#include <lima/processing/fai/typedefs.hpp>

namespace lima
{
namespace processing::fai
{
    struct kernel_params
    {
        kernel_params(boost::compute::context context) :
            variance(context),
            mask(context),
            dark(context),
            dark_variance(context),
            flat(context),
            solid_angle(context),
            polarization(context),
            absorption(context),
            csr_data(context),
            csr_indices(context),
            csr_indptr(context),
            radius2d(context),
            radius1d(context)
        {
        }

        error_model_enum error_model = error_model_enum::poisson;    // FAI
        vector<float> variance;                                      // Preprocessing
        vector<bool> mask;                                           //
        vector<float> dark;                                          //
        vector<float> dark_variance;                                 //
        vector<float> flat;                                          //
        vector<float> solid_angle;                                   //
        vector<float> polarization;                                  //
        vector<float> absorption;                                    //
        float dummy;                                                 //
        float delta_dummy;                                           //
        float normalization_factor;                                  //
        vector<float> csr_data;                                      //
        vector<int> csr_indices;                                     //
        vector<int> csr_indptr;                                      //
        float cutoff_clip;                                           // Sigma clipping
        int cycle;                                                   //
        float empty;                                                 //
        vector<float> radius2d;                                      // Peak finding
        vector<float> radius1d;                                      //
        float noise;                                                 //
        float cutoff_pick;                                           //
        int acc_nb_frames_reset;                                     // Accumulation
        int acc_nb_frames_xfer;                                      //
    };

    namespace detail
    {
        /// Read an HDF5 data into an OpenCL buffer
        template <typename T>
        void read_h5_dset(std::filesystem::path const& h5_path, vector<T>& out, std::string dataset_path,
                          boost::compute::command_queue& queue = boost::compute::system::default_queue())
        {
            namespace h5 = lima::io::h5;
            namespace bs = boost::scope;

            std::string full_dataset_path = std::string(h5_path) + "::" + dataset_path;
            LIMA_LOG(info, proc) << "Reading HDF5 dataset " << full_dataset_path;

            bs::scope_fail end_fail([&]() { LIMA_LOG(error, proc) << "Error reading " << full_dataset_path; });

            // Open an existing file
            LIMA_LOG(trace, proc) << "Opening HDF5 file " << h5_path;
            auto file = h5::file::open(h5_path);

            // Open an existing dataset
            LIMA_LOG(trace, proc) << "Opening HDF5 dataset " << full_dataset_path;
            auto dataset = h5::dataset::open(file, dataset_path);

            // Check the dimensions
            auto dataspace = dataset.space();
            const int ndims = dataspace.ndims();
            hsize_t dims[ndims];
            dataspace.dims(dims, nullptr);

            hsize_t size = std::accumulate(dims, dims + ndims, 1, [](hsize_t lhs, hsize_t rhs) { return lhs * rhs; });
            out.resize(size);
            auto&& buffer = out.get_buffer();
            LIMA_LOG(trace, proc) << "OCL array for " << full_dataset_path << ": size=" << size << " (" << buffer.size()
                                  << " bytes)";

            // Read the dataset
            void* ptr = queue.enqueue_map_buffer(buffer, CL_MAP_WRITE, 0, buffer.size());
            auto dtype = h5::predef_datatype::create<T>();
            LIMA_LOG(trace, proc) << "Reading HDF5 dataset " << full_dataset_path;
            dataset.read(ptr, dtype);
            queue.enqueue_unmap_buffer(out.get_buffer(), ptr);
        }
    } // namespace detail

    // Read the input data / parameters of the openCL kernels
    inline kernel_params read_kernel_params(error_model_enum error_model,             // FAI
                                            std::filesystem::path variance_path,      //
                                            std::filesystem::path mask_path,          // Preprocessing
                                            std::filesystem::path dark_path,          //
                                            std::filesystem::path dark_variance_path, //
                                            std::filesystem::path flat_path,          //
                                            std::filesystem::path solid_angle_path,   //
                                            std::filesystem::path polarization_path,  //
                                            std::filesystem::path absorption_path,    //
                                            float dummy, float delta_dummy,           //
                                            float normalization_factor,               //
                                            std::filesystem::path csr_path,           //
                                            float cutoff_clip,                        // Sigma clipping
                                            int cycle,                                //
                                            float empty,                              //
                                            std::filesystem::path radius2d_path,      // Peak finding
                                            std::filesystem::path radius1d_path,      //
                                            float noise, float cutoff_pick,           //
                                            int acc_nb_frames_reset,                  // Accumulation
                                            int acc_nb_frames_xfer,                   //
                                            boost::compute::context& context, boost::compute::command_queue& queue)
    {
        kernel_params res(context);

        res.error_model = error_model;
        res.dummy = dummy;
        res.delta_dummy = delta_dummy;
        res.normalization_factor = normalization_factor;
        res.cutoff_clip = cutoff_clip;
        res.cycle = cycle;
        res.empty = empty;
        res.noise = noise;
        res.cutoff_pick = cutoff_pick;
        res.acc_nb_frames_reset = acc_nb_frames_reset;
        res.acc_nb_frames_xfer = acc_nb_frames_xfer;

        if (!mask_path.empty())
            detail::read_h5_dset(mask_path, res.mask, "/data", queue);

        if (!dark_path.empty())
            detail::read_h5_dset(dark_path, res.dark, "/data", queue);

        if (!dark_variance_path.empty())
            detail::read_h5_dset(dark_variance_path, res.dark_variance, "/data", queue);

        if (!flat_path.empty())
            detail::read_h5_dset(flat_path, res.flat, "/data", queue);

        if (!solid_angle_path.empty())
            detail::read_h5_dset(solid_angle_path, res.solid_angle, "/data", queue);

        if (!polarization_path.empty())
            detail::read_h5_dset(polarization_path, res.polarization, "/data", queue);

        if (!absorption_path.empty())
            detail::read_h5_dset(absorption_path, res.absorption, "/data", queue);

        // Sigma clipping
        detail::read_h5_dset(csr_path, res.csr_data, "/data", queue);
        detail::read_h5_dset(csr_path, res.csr_indices, "/indices", queue);
        detail::read_h5_dset(csr_path, res.csr_indptr, "/indptr", queue);

        // Peak finding
        detail::read_h5_dset(radius2d_path, res.radius2d, "/data", queue);
        detail::read_h5_dset(radius1d_path, res.radius1d, "/data", queue);

        // Check whether the indices in the CSR are correct
        //assert(std::none_of(res.csr_indices.begin(), res.csr_indices.end(),
        //             [](int idx) { return idx < 0 && idx >= (2048 * 2048); }));

        return res;
    }

    // Parse the JSON configuration of the openCL kernels and read the input data / parameters
    inline kernel_params parse_kernel_params(std::filesystem::path const& config_path, boost::compute::context& context,
                                             boost::compute::command_queue& queue)
    {
        std::ifstream config_file(config_path);
        if (!config_file.is_open())
            LIMA_THROW_EXCEPTION(lima::io_error("Failed to open config file") << boost::errinfo_file_name(config_path));
        std::string config_json(std::istreambuf_iterator<char>(config_file), {});
        boost::json::value config_value = boost::json::parse(config_json);
        // This helper function deduces the type and assigns the value with the matching key
        auto extract = [obj = config_value.as_object()](auto& t, boost::json::string_view key) {
            t = boost::json::value_to<std::decay_t<decltype(t)>>(obj.at(key));
        };
        kernel_params res(context);
        //extract(error_model, "error_model");
        error_model_enum error_model = error_model_enum::azimuthal;
        float dummy;
        extract(dummy, "dummy");
        float delta_dummy;
        extract(delta_dummy, "delta_dummy");
        float normalization_factor;
        extract(normalization_factor, "normalization_factor");
        float cutoff_clip;
        extract(cutoff_clip, "cutoff_clip");
        int cycle;
        extract(cycle, "cycle");
        float empty;
        extract(empty, "empty");
        float noise;
        extract(noise, "noise");
        float cutoff_pick;
        extract(cutoff_pick, "cutoff_pick");
        int acc_nb_frames_reset;
        extract(acc_nb_frames_reset, "acc_nb_frames_reset");
        int acc_nb_frames_xfer;
        extract(acc_nb_frames_xfer, "acc_nb_frames_xfer");
        // Preprocessing
        std::filesystem::path variance_path;
        std::string mask_path;
        extract(mask_path, "mask_path");
        std::filesystem::path dark_path;
        extract(dark_path, "dark_path");
        std::filesystem::path dark_variance_path;
        extract(dark_variance_path, "mask_path");
        std::filesystem::path flat_path;
        extract(flat_path, "flat_path");
        std::filesystem::path solid_angle_path;
        extract(solid_angle_path, "solid_angle_path");
        std::filesystem::path polarization_path;
        extract(polarization_path, "polarization_path");
        std::filesystem::path absorption_path;
        extract(absorption_path, "absorption_path");
        // Sigma clipping
        std::string csr_path;
        extract(csr_path, "csr_path");
        // Peak finding
        std::string radius2d_path;
        extract(radius2d_path, "radius2d_path");
        std::string radius1d_path;
        extract(radius1d_path, "radius1d_path");

        return read_kernel_params(error_model,          // FAI
                                  variance_path,        //
                                  mask_path,            // Preprocessing
                                  dark_path,            //
                                  dark_variance_path,   //
                                  flat_path,            //
                                  solid_angle_path,     //
                                  polarization_path,    //
                                  absorption_path,      //
                                  dummy, delta_dummy,   //
                                  normalization_factor, //
                                  csr_path,             //
                                  cutoff_clip,          // Sigma clipping
                                  cycle,                //
                                  empty,                //
                                  radius2d_path,        // Peak finding
                                  radius1d_path,        //
                                  noise, cutoff_pick,   //
                                  acc_nb_frames_reset,  // Accumulation
                                  acc_nb_frames_xfer,   //
                                  context, queue);
    }

} // namespace processing::fai
} // namespace lima
