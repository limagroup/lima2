// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe.hpp>
#include <boost/describe/io_enums.hpp>

#include <lima/processing/fai/enums.hpp>

namespace lima
{
namespace processing::fai
{
    BOOST_DESCRIBE_ENUM(error_model_enum, no_var, variance, poisson, azimuthal, hybrid)

    using boost::describe::operator<<;
    using boost::describe::operator>>;

} // namespace processing::fai
} // namespace lima
