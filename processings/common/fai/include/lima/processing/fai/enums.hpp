// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

namespace lima
{
namespace processing::fai
{
    enum class error_model_enum
    {
        no_var,
        variance,
        poisson,
        azimuthal,
        hybrid,
    };
} // namespace processing::fai
} // namespace lima
