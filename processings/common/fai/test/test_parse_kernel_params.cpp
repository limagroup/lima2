// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_TEST_MODULE psi
#include <boost/test/unit_test.hpp>
#include <boost/compute.hpp>

#include <lima/processing/fai/kernel_params.hpp>

namespace fai = lima::processing::fai;

// $ test_parse_kernel_params -- ~/config/kernels.json

BOOST_AUTO_TEST_CASE(test_parse_kernel_params)
{
    BOOST_TEST_REQUIRE(boost::unit_test::framework::master_test_suite().argc == 2);
    std::filesystem::path kernel_params_path = boost::unit_test::framework::master_test_suite().argv[1];

    // get the default compute device
    boost::compute::device gpu = boost::compute::system::default_device();
    boost::compute::context context(gpu);
    boost::compute::command_queue queue(context, gpu);

    fai::kernel_params params = fai::parse_kernel_params(kernel_params_path, context, queue);
}