// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <iostream>
#include <vector>
#include <memory>
#include <random>

#include "opencl_fixture.hpp"

const size_t module_width = 1024;
const size_t module_height = 512;

BOOST_FIXTURE_TEST_CASE(test_opencl_csr_sigma_clip, opencl_device)
{
    namespace bcl = boost::compute;

    try {
        // Create context
        bcl::context context(device);

        // Build the kernel
        constexpr int wg_size = 32;
        bcl::program program = bcl::program::create_with_source_file("ocl_azim_CSR.cl", context);
        try {
            // attempt to compile to program
            program.build();
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            BOOST_TEST_REQUIRE(false, program.build_log());
        }

        // Create buffers on the device
        bcl::vector<bcl::float4_> in_d;
        bcl::vector<float> coefs_d;
        bcl::vector<int> indices_d;
        bcl::vector<int> indptr_d;
        bcl::vector<bcl::float8_> summed_d;
        bcl::vector<float> averint_d;
        bcl::vector<float> stdevpix_d;
        bcl::vector<float> stderrmean_d;
        bcl::local_buffer<bcl::float8_> shared_d(wg_size);

        // Create queue to which we will push commands for the device.
        bcl::command_queue queue(context, device);

        const int nb_bins = 100;
        const float cutoff = 1.0f;
        const int cycle = 1;
        const int error_model = 3; // fai::error_model_enum::azimuthal
        const float empty = -1.0f;

        // Run the kernel
        bcl::kernel kernel(program, "csr_sigma_clip4");
        kernel.set_args(in_d, coefs_d, indices_d, indptr_d, cutoff, cycle, (char) error_model, empty, summed_d,
                        averint_d, stdevpix_d, stderrmean_d, shared_d);

        const bcl::extents<2> global_work_offset = {0, 0};
        const bcl::extents<2> global_work_size = {module_width, module_height};
        const bcl::extents<2> local_work_size = {nb_bins * wg_size, wg_size};
        queue.enqueue_nd_range_kernel(kernel, global_work_offset, global_work_size, local_work_size);

        queue.finish();

    } catch (bcl::opencl_error& ex) {
        std::cerr << "CAUGHT: " << ex.error_string() << std::endl;
    } catch (std::exception& ex) {
        std::cerr << "CAUGHT: " << ex.what() << std::endl;
    }
}
