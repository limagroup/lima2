// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <cstddef>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include <vector>
#include <memory>
#include <random>

#include "jungfrau.hpp"
#include "opencl_fixture.hpp"

// Assemble raw image from UDP datagrams on GPU
BOOST_FIXTURE_TEST_CASE(test_opencl_jungfrau_datagram, opencl_device)
{
    namespace bcl = boost::compute;

    try {
        // Create context
        bcl::context context(device);

        // Getting information about used queue and device
        const size_t max_compute_units = device.get_info<CL_DEVICE_MAX_COMPUTE_UNITS>();
        const size_t max_work_group_size = device.get_info<CL_DEVICE_MAX_WORK_GROUP_SIZE>();

        // Build the kernel
        bcl::program program = bcl::program::create_with_source_file("jungfrau_datagram.cl", context);
        try {
            // attempt to compile to program
            program.build();
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            BOOST_TEST_REQUIRE(false, program.build_log());
        }

        std::ifstream in("jungfrau-1-frame.payload", std::ios_base::binary);
        std::vector<char> payload((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());

        // Quick check if we get correct datagrams
        for (auto it = payload.begin(); it != payload.end(); it += datagram_size) {
            sls_detector_header* header = (sls_detector_header*) &(*it);
            BOOST_REQUIRE_EQUAL(header->frameNumber, 1);
            BOOST_REQUIRE_EQUAL(header->detType, 3);
            BOOST_REQUIRE_EQUAL(header->version, 2);
            BOOST_REQUIRE_LE(header->packetNumber, nb_datagrams_per_image);
        }

        bcl::kernel kernel1(program, "create_lut");
        bcl::kernel kernel2(program, "reorder");

        // Create buffers on the device
        bcl::vector<char> payload_d(payload.size(), context);
        bcl::vector<char> lut_d(nb_datagrams_per_image, context);
        bcl::vector<std::uint16_t> out_d(module_width * module_height, context);

        bcl::command_queue queue(context, device);

        bcl::copy(payload.begin(), payload.end(), payload_d.begin(), queue);

        kernel1.set_args(payload_d.get_buffer(), lut_d.get_buffer(), (int) datagram_size, (int) nb_datagrams_per_image);

        {
            const size_t global_work_size[2] = {1, nb_datagrams_per_image};
            queue.enqueue_nd_range_kernel(kernel1, 2, 0, global_work_size, 0);
        }

        ////{ DEBUG
        //std::vector<char> lut(nb_datagrams_per_image);
        //bcl::copy(lut_d.begin(), lut_d.end(), lut.begin(), queue);

        //std::for_each(lut.begin(), lut.end(), [](auto v) { std::cout << (int) v << " "; });
        ////}

        kernel2.set_args(payload_d.get_buffer(), out_d.get_buffer(), lut_d.get_buffer(), (int) module_width,
                         (int) module_height);

        {
            const bcl::extents<2> global_work_offset = {0, 0};
            const bcl::extents<2> global_work_size = {module_width, nb_datagrams_per_image};
            const bcl::extents<2> local_work_size = {module_width, 1};
            queue.enqueue_nd_range_kernel(kernel2, global_work_offset, global_work_size, local_work_size);
        }

        std::vector<std::uint16_t> out(module_width * module_height);
        bcl::copy(out_d.begin(), out_d.end(), out.begin(), queue);

        queue.finish();

    } catch (bcl::opencl_error& ex) {
        std::cerr << "CAUGHT: " << ex.error_string() << std::endl;
    } catch (std::exception& ex) {
        std::cerr << "CAUGHT: " << ex.what() << std::endl;
    }
}