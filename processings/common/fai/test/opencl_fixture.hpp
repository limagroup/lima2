// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <optional>
#include <stdexcept>
#include <vector>

#define CL_TARGET_OPENCL_VERSION 120
#include <boost/compute.hpp>

struct opencl_device
{
    opencl_device(std::size_t idx = 0)
    {
        namespace bcl = boost::compute;

        // Filter for a 3.0 platform and set it as the default
        auto platforms = bcl::system::platforms();
        if (platforms.empty()) {
            throw std::runtime_error("No platform found. Check OpenCL installation!");
        }

        std::optional<bcl::platform> plat;
        for (auto& p : platforms) {
            std::cout << "Plat: " << p.name() << "\n";
            if (p.check_version(3, 0)) {
                plat = p;
            }
        }
        if (!plat) {
            throw std::runtime_error("No OpenCL 3.0 platform found.");
        }

        // Get the list of GPU devices of the platform
        auto devices = plat->devices(CL_DEVICE_TYPE_GPU);
        if (devices.empty()) {
            throw std::runtime_error("No device found. Check OpenCL installation!");
        }
        device = devices[idx];
    }

    boost::compute::device device;
};
