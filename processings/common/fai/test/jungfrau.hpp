typedef struct
{
    uint64_t frameNumber;
    uint32_t expLength;
    uint32_t packetNumber;
    uint64_t bunchId;
    uint64_t timestamp;
    uint16_t modId;
    uint16_t row;
    uint16_t column;
    uint16_t reserved;
    uint32_t debug;
    uint16_t roundRNumber;
    uint8_t detType;
    uint8_t version;
} sls_detector_header;

const size_t module_width = 1024;
const size_t module_height = 512;

const size_t nb_lines_per_datagrams = 4;                                      // 4 lines per UDP Datagram
const size_t nb_datagrams_per_image = module_height / nb_lines_per_datagrams; // 128 UDP Datagrams per image

const size_t header_size = sizeof(sls_detector_header);
const size_t data_size = nb_lines_per_datagrams * module_width * sizeof(uint16_t);
const size_t datagram_size = header_size + data_size;