// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <cstdint>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include <memory>
#include <random>
#include <string>
#include <vector>

#include "opencl_fixture.hpp"

template <typename T>
using vector_pinned = boost::compute::vector<T, boost::compute::pinned_allocator<T>>;

BOOST_FIXTURE_TEST_CASE(test_opencl_pedestal_gain, opencl_device)
{
    namespace bcl = boost::compute;

    try {
        // Create context
        bcl::context context(device);

        // Build the kernel
        bcl::program program = bcl::program::create_with_source_file("pedestal_gain_correction.cl", context);
        try {
            // attempt to compile to program
            program.build();
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            BOOST_TEST_REQUIRE(false, program.build_log());
        }

        // Input on host
        const unsigned int length = 1024;
        const unsigned int nb_gain = 3;

        std::vector<float> gain(length * nb_gain);
        std::fill_n(gain.begin(), length, 1.0f);
        std::fill_n(gain.begin() + length, length, 2.0f);
        std::fill_n(gain.begin() + length * 2, length, 3.0f);

        std::vector<float> pedestal(length * nb_gain);
        std::fill_n(pedestal.begin(), length * nb_gain, 0.0f);

        std::vector<std::uint16_t> in(length);
        std::random_device rd;
        std::uniform_int_distribution<std::uint16_t> gain_dist(0, 0x0002);
        std::uniform_int_distribution<std::uint16_t> val_dist(0, 0x03FF);
        std::generate_n(in.begin(), length, [&rd, &gain_dist, &val_dist]() {
            std::uint16_t gain = gain_dist(rd);
            assert(gain < 0x0003);
            gain = (gain == 2) ? 3 : gain;
            std::uint16_t val = val_dist(rd);
            assert(gain < 0x0400);
            return (gain << 14) | val;
        });

        //print(input, length);
        //print(gain, length * nb_gain);
        //print(pedestal, length * nb_gain);

        // Create buffers on the device
        bcl::vector<std::uint16_t> in_d(length, context);
        bcl::vector<float> gain_d(length * nb_gain, context);
        bcl::vector<float> pedestal_d(length * nb_gain, context);
        bcl::vector<float> res_d(length, context);

        // Create queue to which we will push commands for the device.
        bcl::command_queue queue(context, device);

        // Write input to the device
        bcl::copy(in.begin(), in.end(), in_d.begin(), queue);
        bcl::copy(gain.begin(), gain.end(), gain_d.begin(), queue);
        bcl::copy(pedestal.begin(), pedestal.end(), pedestal_d.begin(), queue);

        // Run the kernel
        bcl::kernel kernel(program, "pedestal_gain_correction");
        kernel.set_args(in_d, gain_d, pedestal_d, res_d, length);
        queue.enqueue_1d_range_kernel(kernel, 0, length, 0);

        // read result from the device to array
        std::vector<float> res(length);
        bcl::copy(res_d.begin(), res_d.end(), res.begin(), queue);

        std::vector<float> expected(length);

        std::transform(in.begin(), in.end(), expected.begin(), [&pedestal, &gain, i = 0](auto in) mutable {
            // gain level : 2 MSB bits
            unsigned short lvl = in >> 14;
            unsigned short val = in & 0x3fff;

            // G0 = 0b00, G1 = 0b01, G2 = 0b11
            lvl = (lvl == 3) ? 2 : lvl;

            float noise = pedestal[i + lvl * length];
            float factor = gain[i + lvl * length];

            i++;

            return (val - noise) * factor;
        });

        BOOST_CHECK_EQUAL_COLLECTIONS(res.begin(), res.end(), expected.begin(), expected.end());

    } catch (bcl::opencl_error& ex) {
        std::cerr << "CAUGHT: " << ex.error_string() << std::endl;
    } catch (std::exception& ex) {
        std::cerr << "CAUGHT: " << ex.what() << std::endl;
    }
}

BOOST_FIXTURE_TEST_CASE(test_opencl_pedestal_gain_overlap, opencl_device)
{
    namespace bcl = boost::compute;

    try {
        // Create context
        bcl::context context(device);

        // Build the kernel
        bcl::program program = bcl::program::create_with_source_file("pedestal_gain_correction.cl", context);
        try {
            // attempt to compile to program
            program.build();
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            BOOST_TEST_REQUIRE(false, program.build_log());
        }

        bcl::kernel kernel1(program, "pedestal_gain_correction");
        bcl::kernel kernel2(program, "pedestal_gain_correction");

        // Create 2 queues to which we will push commands for the device.
        bcl::command_queue queue1(context, device); // For HtD
        bcl::command_queue queue2(context, device); // For Compute and DtH

        // Constants
        const unsigned int length = 2048 * 2048;
        const unsigned int nb_gain = 3;
        const unsigned int nb_buffers = 2; // Double buffering

        // Create pinned buffers on the host
        vector_pinned<std::uint16_t> in_h(length * nb_buffers, context);
        vector_pinned<float> gain_h(length * nb_gain * nb_buffers, context);
        vector_pinned<float> pedestal_h(length * nb_gain * nb_buffers, context);
        vector_pinned<float> res_h(length * nb_buffers, context);

        // Get mapped pointers to pinned input host buffers
        std::uint16_t* in = static_cast<std::uint16_t*>(
            queue1.enqueue_map_buffer(in_h.get_buffer(), CL_MAP_WRITE, 0, in_h.get_buffer().size()));
        float* gain = static_cast<float*>(
            queue1.enqueue_map_buffer(gain_h.get_buffer(), CL_MAP_WRITE, 0, gain_h.get_buffer().size()));
        float* pedestal = static_cast<float*>(
            queue1.enqueue_map_buffer(pedestal_h.get_buffer(), CL_MAP_WRITE, 0, pedestal_h.get_buffer().size()));
        float* res = static_cast<float*>(
            queue1.enqueue_map_buffer(res_h.get_buffer(), CL_MAP_READ, 0, res_h.get_buffer().size()));

        // Init input data
        std::random_device rd;
        std::uniform_int_distribution<std::uint16_t> gain_dist(0, 0x0002);
        std::uniform_int_distribution<std::uint16_t> val_dist(0, 0x03FF);
        std::generate_n(in, length, [&rd, &gain_dist, &val_dist]() {
            std::uint16_t gain = gain_dist(rd);
            assert(gain < 0x0003);
            gain = (gain == 2) ? 3 : gain;
            std::uint16_t val = val_dist(rd);
            assert(gain < 0x0400);
            return (gain << 14) | val;
        });

        // Init gain
        std::fill_n(gain, length, 1.0f);
        std::fill_n(gain + length, length, 2.0f);
        std::fill_n(gain + length * 2, length, 3.0f);

        // Init pedestal
        std::fill_n(pedestal, length * nb_gain, 0.0f);

        //print(input, length);
        //print(gain, length * nb_gain);
        //print(pedestal, length * nb_gain);

        // Create buffers on the device
        bcl::buffer buffer_in_d[nb_buffers] = {bcl::buffer(context, sizeof(std::uint16_t) * length, CL_MEM_WRITE_ONLY),
                                               bcl::buffer(context, sizeof(std::uint16_t) * length, CL_MEM_WRITE_ONLY)};
        bcl::buffer buffer_gain_d(context, sizeof(float) * length * nb_gain, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_pedestal_d(context, sizeof(float) * length * nb_gain, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_res_d[nb_buffers] = {bcl::buffer(context, sizeof(float) * length, CL_MEM_READ_ONLY),
                                                bcl::buffer(context, sizeof(float) * length, CL_MEM_READ_ONLY)};

        // Write params to the device
        queue1.enqueue_write_buffer(buffer_gain_d, 0, sizeof(float) * length * nb_gain, gain);
        queue1.enqueue_write_buffer(buffer_pedestal_d, 0, sizeof(float) * length * nb_gain, pedestal);

        // Process data
        const int nb_cycles = 2;
        for (int i = 0; i < nb_cycles; i++) {
            // Mid Phase 0

            // Write input to the device (non-blocking)
            queue1.enqueue_write_buffer_async(buffer_in_d[0], 0, sizeof(std::uint16_t) * length, in);

            // Start Phase 1

            // Run the kernel
            kernel1.set_args(buffer_in_d[0], buffer_gain_d, buffer_pedestal_d, buffer_res_d[0], length);
            queue1.enqueue_1d_range_kernel(kernel1, 0, length, 0);

            // Write input to the device (non-blocking)
            queue2.enqueue_write_buffer_async(buffer_in_d[1], 0, sizeof(std::uint16_t) * length, in + length);

            // Start Phase 2

            // Run the kernel
            kernel2.set_args(buffer_in_d[1], buffer_gain_d, buffer_pedestal_d, buffer_res_d[1], length);
            queue2.enqueue_1d_range_kernel(kernel2, 0, length, 0);

            // Read result from the device to array
            queue1.enqueue_read_buffer_async(buffer_res_d[0], 0, sizeof(float) * length, res);

            // Start Phase 0 (Rolls over)

            // Read result from the device to array
            queue2.enqueue_read_buffer_async(buffer_res_d[1], 0, sizeof(float) * length, res + length);
        }

        // Compute expected result
        std::vector<float> expected(length);
        std::transform(in, in + length, expected.begin(), [&pedestal, &gain, i = 0](auto in) mutable {
            // gain level : 2 MSB bits
            unsigned short lvl = in >> 14;
            unsigned short val = in & 0x3fff;

            // G0 = 0b00, G1 = 0b01, G2 = 0b11
            lvl = (lvl == 3) ? 2 : lvl;

            float noise = pedestal[i + lvl * length];
            float factor = gain[i + lvl * length];

            i++;

            return (val - noise) * factor;
        });

        BOOST_CHECK_EQUAL_COLLECTIONS(res, res + length, expected.begin(), expected.end());

    } catch (bcl::opencl_error& ex) {
        std::cerr << "CAUGHT: " << ex.error_string() << std::endl;
    } catch (std::exception& ex) {
        std::cerr << "CAUGHT: " << ex.what() << std::endl;
    }
}