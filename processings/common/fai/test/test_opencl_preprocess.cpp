// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <iostream>
#include <vector>
#include <memory>
#include <random>

#include "opencl_fixture.hpp"

BOOST_FIXTURE_TEST_CASE(test_opencl_preprocess, opencl_device)
{
    namespace bcl = boost::compute;

    try {
        // Create context
        bcl::context context(device);

        // Build the kernel
        bcl::program program = bcl::program::create_with_source_file("preprocess.cl", context);
        try {
            // attempt to compile to program
            program.build();
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            BOOST_TEST_REQUIRE(false, program.build_log());
        }

        bcl::kernel kernel1(program, "corrections4");
        bcl::command_queue queue1(context, device); // For HtD

        // Constants
        const unsigned int length = 2048 * 2048;
        const unsigned int nb_buffers = 2; // Double buffering

        // Inputs
        int error_model = 2; // fai::error_model_enum::poisson;
        bool do_dark = true;
        bool do_dark_variance = true;
        bool do_flat = true;
        bool do_mask = true;
        bool do_dummy = true;
        float dummy = -1.0f;
        float delta_dummy = 1e-6f;
        float normalization_factor = 1.01f;

        // Create pinned buffers on the host
        const cl_mem_flags mem_flags = CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR;
        bcl::buffer buffer_in_h(context, sizeof(float) * length * nb_buffers, mem_flags);
        bcl::buffer buffer_variance_h(context, sizeof(float) * length * nb_buffers, mem_flags);
        bcl::buffer buffer_dark_h(context, sizeof(float) * length, mem_flags);
        bcl::buffer buffer_dark_variance_h(context, sizeof(float) * length, mem_flags);
        bcl::buffer buffer_flat_h(context, sizeof(float) * length, mem_flags);
        bcl::buffer buffer_mask_h(context, sizeof(std::uint8_t) * length, mem_flags);
        bcl::buffer buffer_res_h(context, sizeof(bcl::float4_) * length * nb_buffers, mem_flags);

        // Get mapped pointers to pinned input host buffers
        float* input = (float*) queue1.enqueue_map_buffer(buffer_in_h, CL_MAP_WRITE, 0, buffer_in_h.size());
        float* variance =
            (float*) queue1.enqueue_map_buffer(buffer_variance_h, CL_MAP_WRITE, 0, buffer_variance_h.size());
        float* dark = (float*) queue1.enqueue_map_buffer(buffer_dark_h, CL_MAP_WRITE, 0, buffer_dark_h.size());
        //float* dark_variance = (float*) queue1.enqueue_map_buffer(buffer_dark_variance_h, CL_MAP_WRITE, 0,
        //                                                                buffer_dark_variance_h.size());
        float* flat = (float*) queue1.enqueue_map_buffer(buffer_flat_h, CL_MAP_WRITE, 0, buffer_flat_h.size());
        std::uint8_t* mask =
            (std::uint8_t*) queue1.enqueue_map_buffer(buffer_mask_h, CL_MAP_WRITE, 0, buffer_mask_h.size());
        bcl::float4_* res =
            (bcl::float4_*) queue1.enqueue_map_buffer(buffer_res_h, CL_MAP_READ, 0, buffer_res_h.size());

        // Init input data
        std::random_device rd;
        std::uniform_int_distribution<int> data_dist(0, 1000);

        std::vector<float> clean(length);
        std::generate_n(clean.begin(), length, [&rd, &data_dist]() { return data_dist(rd) / 1000.0; });
        std::generate_n(dark, length, [&rd, &data_dist]() { return data_dist(rd) / 100000.0; });
        std::transform(clean.begin(), clean.end(), dark, input, [](auto a, auto b) { return a + b; });

        // Create buffers on the device
        bcl::buffer buffer_in(context, sizeof(float) * length, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_variance(context, sizeof(float) * length, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_dark(context, sizeof(float) * length, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_dark_variance(context, sizeof(float) * length, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_flat(context, sizeof(float) * length, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_mask(context, sizeof(std::uint8_t) * length, CL_MEM_WRITE_ONLY);
        bcl::buffer buffer_res(context, sizeof(bcl::float4_) * length, CL_MEM_READ_ONLY);

        //bcl::vector<float> buffer_in(length, context);
        //bcl::vector<float> buffer_variance(length, context);
        //bcl::vector<float> buffer_dark(length, context);
        //bcl::vector<float> buffer_dark_variance(length, context);
        //bcl::vector<float> buffer_flat(length, context);
        //bcl::vector<std::uint8_t> buffer_mask(length, context);
        //bcl::vector<bcl::float4_> buffer_res(length, context);

        // Write input to the device
        queue1.enqueue_write_buffer(buffer_in, 0, sizeof(float) * length, input);
        queue1.enqueue_write_buffer(buffer_variance, 0, sizeof(float) * length, variance);
        queue1.enqueue_write_buffer(buffer_dark, 0, sizeof(float) * length, dark);
        queue1.enqueue_write_buffer(buffer_flat, 0, sizeof(float) * length, flat);
        queue1.enqueue_write_buffer(buffer_mask, 0, sizeof(std::uint8_t) * length, mask);

        // Run the kernel
        kernel1.set_args(buffer_in, (char) error_model, buffer_variance, (char) do_dark, buffer_dark, (char) do_flat,
                         buffer_flat, (char) do_mask, buffer_mask, (char) do_dummy, dummy, delta_dummy,
                         normalization_factor, buffer_res, length);
        queue1.enqueue_1d_range_kernel(kernel1, 0, length, 0);

        // Read result from the device to array
        queue1.enqueue_read_buffer(buffer_res, 0, buffer_res.size(), res);

        std::vector<bcl::float4_> expected(length);
        std::transform(clean.begin(), clean.end(), expected.begin(),
                       [](auto in) mutable { return bcl::float4_(in, 1, 0, 1); });

        //BOOST_CHECK_EQUAL_COLLECTIONS(res, res + length, expected.begin(), expected.end());

    } catch (bcl::opencl_error& ex) {
        std::cerr << "CAUGHT: " << ex.error_string() << std::endl;
    } catch (std::exception& ex) {
        std::cerr << "CAUGHT: " << ex.what() << std::endl;
    }
}
