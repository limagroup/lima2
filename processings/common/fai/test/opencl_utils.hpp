// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <fstream>
#include <sstream>
#include <string>

inline std::string slurp(std::ifstream& in)
{
    std::ostringstream sstr;
    sstr << in.rdbuf();
    return sstr.str();
}

template <typename T>
void print(T const* const input, size_t length)
{
    std::copy(input, input + length, std::ostream_iterator<T>(std::cout, ", "));
    std::cout << std::endl;
}