# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

if (LIMA_ENABLE_OPENCL)

find_package(OpenCL REQUIRED)

add_executable(test_psi_opencl_processing
    main.cpp
    test_opencl_simple.cpp
    test_opencl_datagram.cpp
    test_opencl_pedestal_gain_correction.cpp
    test_opencl_preprocess.cpp
    test_opencl_csr_sigma_clip.cpp
)

target_link_libraries(test_psi_opencl_processing
    lima_core
    OpenCL::OpenCL
    Boost::unit_test_framework
)

add_test(NAME test_psi_opencl_processing COMMAND test_psi_opencl_processing)

add_executable(test_parse_kernel_params
    test_parse_kernel_params.cpp
)

target_link_libraries(test_parse_kernel_params
    processing_fai
    Boost::unit_test_framework
    Boost::json
)

endif (LIMA_ENABLE_OPENCL)