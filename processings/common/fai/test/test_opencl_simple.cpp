// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>

#include "opencl_fixture.hpp"

BOOST_FIXTURE_TEST_CASE(test_opencl_simple, opencl_device)
{
    namespace bcl = boost::compute;

    try {
        std::ostringstream os;
        os << "Using device: " << device.name();
        BOOST_TEST_MESSAGE(os.str());

        // Create context
        bcl::context context({device});

        // Source code
        const char source[] = BOOST_COMPUTE_STRINGIZE_SOURCE(
            __kernel void simple_add(__global const int* A, __global const int* B, __global int* C) {
                C[get_global_id(0)] = A[get_global_id(0)] + B[get_global_id(0)];
            });

        bcl::program program = bcl::program::create_with_source(source, context);
        try {
            // attempt to compile to program
            program.build();
        } catch (boost::compute::opencl_error& e) {
            // program failed to compile, print out the build log
            BOOST_TEST_REQUIRE(false, program.build_log());
        }

        // Input on host
        const int length = 10;
        const int A[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const int B[] = {0, 1, 2, 0, 1, 2, 0, 1, 2, 0};

        // Create buffers on the device
        bcl::buffer buffer_A(context, sizeof(int) * length, CL_MEM_READ_WRITE);
        bcl::buffer buffer_B(context, sizeof(int) * length, CL_MEM_READ_WRITE);
        bcl::buffer buffer_C(context, sizeof(int) * length, CL_MEM_READ_WRITE);

        // Create queue to which we will push commands for the device.
        bcl::command_queue queue(context, device);

        // Write arrays A and B to the device
        queue.enqueue_write_buffer(buffer_A, 0, buffer_A.size(), A);
        queue.enqueue_write_buffer(buffer_B, 0, buffer_B.size(), B);

        // Run the kernel
        bcl::kernel kernel(program, "simple_add");

        kernel.set_args(buffer_A, buffer_B, buffer_C);
        queue.enqueue_1d_range_kernel(kernel, 0, length, 0);

        int C[length];

        //read result C from the device to array C
        queue.enqueue_read_buffer(buffer_C, 0, buffer_C.size(), C);

        int expected[] = {0, 2, 4, 3, 5, 7, 6, 8, 10, 9};
        BOOST_CHECK_EQUAL_COLLECTIONS(C, C + length, expected, expected + length);

    } catch (std::exception& ex) {
        std::cerr << "CAUGHT: " << ex.what() << std::endl;
    }
}