class SparseFrame:
    def __init__(self, shape, index, intensity, background_avg, background_std):
        self._shape = shape
        self.index = index
        self.intensity = intensity
        self._dtype = self.intensity.dtype
        self._background_avg = background_avg
        self._background_std = background_std
        self.__common_init()

    def __init__(self, shape, tuple):
        self._shape = shape
        self.index = tuple[0]
        self.intensity = tuple[1]
        self._dtype = self.intensity.dtype
        self._background_avg = tuple[2]
        self._background_std = tuple[3]
        self.__common_init()

    def __common_init(self):
        self._mask = None
        self._dummy = None
        self._radial = None
        self._unit = None
        self._has_dark_correction = None
        self._has_flat_correction = None
        self._normalization_factor = None
        self._polarization_factor = None
        self._metadata = None
        self._percentile = None
        self._method = None
        self._method_called = None
        self._compute_engine = None
        self._cutoff = None
        self._background_cycle = None
        self._noise = None
        self._radial_range = None
