import sys
import numpy as np
import h5py as h5
import scipy.sparse as sp
import peak_finder as pf

from sparse_frame import SparseFrame

def save_sparse(sparse_filename, frames):
    print("Saving")

    index = np.concatenate([i.index for i in frames]).astype(np.uint32)
    intensity = np.concatenate([i.intensity for i in frames])

    with h5.File(sparse_filename, 'w') as f:
        f.create_dataset('peak_index', data=index)
        f.create_dataset('peak_intensity', data=intensity)
        f.create_dataset('background_avg', data=frames[0].background_avg)
        f.create_dataset('background_std', data=frames[0].background_std)


def process_frames(csr_filename, data_filename, sparse_filename):
    # Params
    gpu_device_idx = 0
    width = 2048
    height = 2048
    dummy = 0.0
    delta_dummy = 0.0
    # variance = (raw-pedestal)/gain + std(pedestal)^2/gain^2
    # mask = np.ndarray(0, np.bool)
    dark = np.ndarray(0, np.float32)
    dark_variance = np.ndarray(0, np.float32)
    flat = np.ndarray(0, np.float32)
    solid_angle = np.ndarray(0, np.float32)
    polarization = np.ndarray(0, np.float32)
    absorption = np.ndarray(0, np.float32)
    normalization_factor = 1.0
    cutoff_clip = 5.0
    cycle = 5
    noise = 1.0
    cutoff_pick = 3.0

    # Read the LUT
    csr = sp.load_npz(csr_filename)
    bin_centers = np.load('bin_centers.npy')
    r_center = np.load('r_center.npy')

    with h5.File(data_filename, 'r') as in_file, h5.File('debug.h5', 'w') as debug_file, h5.File('mask.h5', 'r') as mask_file, h5.File('gains_201810_inv.h5', 'r') as gain_file, h5.File('pedestal_20190323_1022_avg.h5', 'r') as ped_file:
        dataset = in_file['data']
        #nb_frames = dataset.shape[0];
        nb_frames = 1
        frame_size = dataset.shape[1:]
        nb_bins = csr.indptr.shape[0] - 1

        preprocessed = debug_file.create_dataset('preprocessed', shape=(nb_frames, frame_size[0], frame_size[1], 4), dtype=np.float32)
        summed = debug_file.create_dataset('summed', shape=(nb_frames, nb_bins, 8), dtype=np.float32)
        found = debug_file.create_dataset('found', shape=(nb_frames, frame_size[0], frame_size[1], 4), dtype=np.float32)

        assert bin_centers.size == nb_bins
        assert r_center.size == frame_size[0] * frame_size[1]

        #breakpoint()
        gain = gain_file['data'][:3,:,:]
        pedestal = ped_file['data']
        variance = np.full(frame_size, 1.0, np.float32) #not used
        mask = mask_file['data']

        print(f"Processing {nb_frames} frames ")

        # Create peak finder callable
        finder = pf.PeakFinder(gpu_device_idx,
                               width, height, nb_bins,
                               pf.poisson,
                               variance,
                               mask,
                               dark,
                               dark_variance,
                               flat,
                               solid_angle,
                               polarization,
                               absorption,
                               dummy, delta_dummy,
                               normalization_factor,
                               csr.data, csr.indices, csr.indptr,
                               cutoff_clip,
                               cycle,
                               r_center,
                               bin_centers,
                               noise,
                               cutoff_pick,
                               "detectors/psi/processing/src")

        frames = []
        for i in range(0, nb_frames):
            preprocessed_dbg, summed_dbg, found_dbg, index, intensity, back_avg, back_std = finder(dataset[i,:,:])
            print(f"found {index.size} peaks")
            frames.append(SparseFrame(dataset.shape, index, intensity, back_avg, back_std))
            preprocessed[i,:,:] = preprocessed_dbg.reshape((2048, 2048, 4))
            summed[i,:] = summed_dbg
            found[i,:,:] = found_dbg.reshape((2048, 2048, 4))

    save_sparse(sparse_filename, frames)

def main():
    if (len(sys.argv) > 3) and(sys.argv[1] != "-"):
        print("Press ENTER to continue")
        input()

        process_frames(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print('Usage: <CSR npz file> <dense H5 file> <sparse H5 file>')

if __name__ == "__main__":
    main()
