// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <optional>
#include <tuple>
#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include <fmt/format.h>

#include <lima/processing/fai/peak_finder.hpp>

namespace py = pybind11;
namespace bcl = boost::compute;
namespace fai = lima::processing::fai;

// Mini logging
template <typename T, int ExtraFlags>
void log(const char* str, py::array_t<T, ExtraFlags> const& expr)
{
    std::cout << str << ": py::array_t[" << expr.size() << "]" << std::endl;
}

template <typename T>
void log(const char* str, T expr)
{
    std::cout << str << ": " << expr << std::endl;
}

#define LOG(expr) log((#expr), (expr))

namespace pybind11
{
// NumPy dense array using the C (row-major) ordering
template <typename T>
using c_array_t = py::array_t<T, py::array::c_style | py::array::forcecast>;
} // namespace pybind11

namespace pyfai
{
template <typename T>
using vector = bcl::vector<T, bcl::pinned_allocator<T>>;

// Copy numpy array to opencl buffer (slow since not pinned)
template <typename T1, typename T2, typename Allocator, int ExtraFlags>
auto copy_async(py::array_t<T1, ExtraFlags> const& in, bcl::vector<T2, Allocator>& out, bcl::command_queue& queue)
{
    assert(in.size() == out.size());

    // Request a buffer descriptor from Python
    py::buffer_info buf = in.request();
    auto ptr = static_cast<T2*>(buf.ptr);

    return bcl::copy(ptr, ptr + in.size(), out.begin(), queue);
}

// Copy opencl buffer to numpy array (slow since not pinned)
template <typename T1, typename T2, typename Allocator, int ExtraFlags>
auto copy_async(bcl::vector<T1, Allocator> const& in, py::array_t<T2, ExtraFlags>& out, bcl::command_queue& queue)
{
    assert(in.size() == out.size());

    // Request a buffer descriptor from Python
    py::buffer_info buf = out.request();
    auto ptr = static_cast<T1*>(buf.ptr);

    return bcl::copy(in.begin(), in.end(), ptr, queue);
}

// Copy opencl buffer to numpy array (slow since not pinned)
template <typename T, typename Allocator, int ExtraFlags>
auto copy_n(bcl::vector<T, Allocator> const& in, std::size_t n, py::array_t<T, ExtraFlags>& out,
            bcl::command_queue& queue)
{
    assert(n == out.size());

    // Request a buffer descriptor from Python
    py::buffer_info buf = out.request();
    auto ptr = static_cast<T*>(buf.ptr);

    return bcl::copy_n(in.begin(), n, ptr, queue);
}

struct peak_finder
{
    using pixel_t = std::uint16_t;

    peak_finder(bcl::context const& context_arg,                                     // The index of the GPU device
                std::size_t width, std::size_t height,                               // Image dimensions
                std::size_t nb_bins,                                                 // FAI
                fai::error_model_enum error_model,                                   // Error model
                float dummy, float delta_dummy,                                      // Dummy
                py::c_array_t<float> variance_np,                                    // Variance
                py::c_array_t<bool> mask_np,                                         // Mask
                py::c_array_t<float> dark_np, py::c_array_t<float> dark_variance_np, // Dark
                py::c_array_t<float> flat_np,                                        // Flat
                py::c_array_t<float> solid_angle_np,                                 // Solid angle
                py::c_array_t<float> polarization_np,                                // Polarization
                py::c_array_t<float> absorption_np,                                  // Absorption
                float normalization_factor,                                          //
                py::c_array_t<float> csr_data_np, py::c_array_t<int> csr_indices_np, //
                py::c_array_t<int> csr_indptr_np,                                    // Sigma clipping
                float cutoff_clip,                                                   //
                int cycle,                                                           //
                float empty,                                                         //
                py::c_array_t<float> radius2d_np,                                    // Peak finding
                py::c_array_t<float> radius1d_np,                                    //
                float noise,                                                         //
                float cutoff_pick,                                                   //
                int acc_nb_frames_reset,                                             // Accumulation
                int acc_nb_frames_xfer,                                              //
                std::string const& cl_source_path                                    // CL source file path
                ) :
        context(context_arg),
        raw_d(width * height, context_arg),
        preprocessed_dbg_d(width * height),
        cliped_dbg_d(nb_bins),
        found_dbg_d(width * height),
        peak_indices_d(width * height, context_arg),
        peak_values_d(width * height, context_arg),
        background_avg_d(nb_bins, context_arg),
        background_std_d(nb_bins, context_arg),
        acc_corr_d(width * height, context_arg),
        acc_peak_d(width * height, context_arg)
    {
        // Create queue
        queue = bcl::command_queue{context, context.get_device()};

        //{ Numpy / opencl buffer glue not necessary in the final code
        vector<float> variance(variance_np.is_none() ? 0 : variance_np.size(), context);
        vector<bool> mask(mask_np.is_none() ? 0 : mask_np.size(), context);
        vector<float> dark(dark_np.is_none() ? 0 : dark_np.size(), context);
        vector<float> dark_variance(dark_variance_np.is_none() ? 0 : dark_variance_np.size(), context);
        vector<float> flat(flat_np.is_none() ? 0 : flat_np.size(), context);
        vector<float> solid_angle(solid_angle_np.is_none() ? 0 : solid_angle_np.size(), context);
        vector<float> polarization(polarization_np.is_none() ? 0 : polarization_np.size(), context);
        vector<float> absorption(absorption_np.is_none() ? 0 : absorption_np.size(), context);
        vector<float> csr_data(csr_data_np.is_none() ? 0 : csr_data_np.size(), context);
        vector<int> csr_indices(csr_indices_np.is_none() ? 0 : csr_indices_np.size(), context);
        vector<int> csr_indptr(csr_indptr_np.is_none() ? 0 : csr_indptr_np.size(), context);
        vector<float> radius2d(radius2d_np.is_none() ? 0 : radius2d_np.size(), context);
        vector<float> radius1d(radius1d_np.is_none() ? 0 : radius1d_np.size(), context);

        copy_async(variance_np, variance, queue);
        copy_async(mask_np, mask, queue);
        copy_async(dark_np, dark, queue);
        copy_async(dark_variance_np, dark_variance, queue);
        copy_async(flat_np, flat, queue);
        copy_async(solid_angle_np, solid_angle, queue);
        copy_async(polarization_np, polarization, queue);
        copy_async(absorption_np, absorption, queue);
        copy_async(csr_data_np, csr_data, queue);
        copy_async(csr_indices_np, csr_indices, queue);
        copy_async(csr_indptr_np, csr_indptr, queue);
        copy_async(radius2d_np, radius2d, queue);
        copy_async(radius1d_np, radius1d, queue);
        //}

        // Create the peakfinder
        pf = fai::make_peak_finder<pixel_t>(context,                               //
                                            width, height,                         //
                                            error_model,                           // FAI
                                            variance,                              // Preprocessing
                                            mask, dark, dark_variance, flat,       //
                                            solid_angle, polarization, absorption, //
                                            dummy, delta_dummy,                    //
                                            normalization_factor,                  //
                                            csr_data, csr_indices, csr_indptr,     // Sigma clipping
                                            cutoff_clip,                           //
                                            cycle,                                 //
                                            empty,                                 //
                                            radius2d, radius1d,                    // Peak finding
                                            noise,                                 //
                                            cutoff_pick,                           //
                                            acc_nb_frames_reset,                   // Accumulation
                                            acc_nb_frames_xfer,                    //
                                            cl_source_path,                        // CL source file path
                                            queue);
    }

    ~peak_finder() { queue.finish(); }

    auto apply(py::c_array_t<pixel_t> raw, bool force_acc = false)
    {
        // Write input to the device (non-blocking)
        copy_async(raw, raw_d, queue);

        // Run the kernels
        auto [nb_peaks, nb_acc_frames] =
            pf(queue, raw_d, preprocessed_dbg_d, cliped_dbg_d, found_dbg_d, peak_indices_d, peak_values_d,
               background_avg_d, background_std_d, acc_corr_d, acc_peak_d, force_acc);

        py::c_array_t<float> preprocessed({(int) preprocessed_dbg_d.size(), 4});
        py::c_array_t<float> cliped({(int) cliped_dbg_d.size(), 8});
        py::c_array_t<float> found({(int) found_dbg_d.size(), 4});

        py::c_array_t<int> peak_indices(nb_peaks);
        py::c_array_t<float> peak_values(nb_peaks);
        py::c_array_t<float> background_avg(background_avg_d.size());
        py::c_array_t<float> background_std(background_std_d.size());
        py::c_array_t<float> acc_corr(nb_acc_frames ? acc_corr_d.size() : 0);
        py::c_array_t<float> acc_peak(nb_acc_frames ? acc_peak_d.size() : 0);

        // Read result from the device to array (non-blocking)
        copy_n(peak_indices_d, nb_peaks, peak_indices, queue);
        copy_n(peak_values_d, nb_peaks, peak_values, queue);
        copy_async(background_avg_d, background_avg, queue);
        copy_async(background_std_d, background_std, queue);
        if (nb_acc_frames) {
            copy_async(acc_corr_d, acc_corr, queue);
            copy_async(acc_peak_d, acc_peak, queue);
        }

        copy_async(preprocessed_dbg_d, preprocessed, queue);
        copy_async(cliped_dbg_d, cliped, queue);
        copy_async(found_dbg_d, found, queue);

        return std::make_tuple(preprocessed, cliped, found, peak_indices, peak_values, background_avg, background_std,
                               nb_acc_frames, acc_corr, acc_peak);
    }

    fai::vector<pixel_t> raw_d;
    fai::vector<bcl::float4_> preprocessed_dbg_d;
    fai::vector<bcl::float8_> cliped_dbg_d;
    fai::vector<bcl::float4_> found_dbg_d;
    fai::vector<int> peak_indices_d;
    fai::vector<float> peak_values_d;
    fai::vector<float> background_avg_d;
    fai::vector<float> background_std_d;
    fai::vector<float> acc_corr_d;
    fai::vector<float> acc_peak_d;

    bcl::context context;
    bcl::command_queue queue;
    fai::peak_finder<pixel_t> pf;
};

} // namespace pyfai

//template <typename T>
//void py_vector(py::module& m, const char* name)
//{
//    using vector_t = pyfai::vector<T>;
//
//    py::class_<vector_t>(m, name, py::buffer_protocol())
//        .def(py::init<>())
//        .def(py::init<std::size_t>())
//        .def(py::init([](py::array_t<T> array, bcl::context context) {
//            // Request a buffer descriptor from Python
//            py::buffer_info buf = array.request();
//            return vector_t(static_cast<T*>(buf.ptr), context);
//        }))
//        .def("size", &vector_t::size, "Returns the number of elements in the vector.")
//        .def("__repr__", [](const vector_t& v) { return fmt::format("<clVector [{}]>", v.size()); });
//}

PYBIND11_MODULE(peak_finder, m)
{
    using namespace pybind11::literals;

    m.doc() = "pyFAI in C++"; // optional module docstring

    py::enum_<fai::error_model_enum>(m, "ErrorModel")
        .value("no_var", fai::error_model_enum::no_var)
        .value("variance", fai::error_model_enum::variance)
        .value("poisson", fai::error_model_enum::poisson)
        .value("azimuthal", fai::error_model_enum::azimuthal)
        .value("hybrid", fai::error_model_enum::hybrid)
        .export_values();

    py::class_<pyfai::peak_finder>(m, "PeakFinder", py::dynamic_attr())
        .def(py::init([](std::size_t gpu_device_idx,                                    // The index of the GPU device
                         std::size_t width, std::size_t height,                         //
                         std::size_t nb_bins,                                           //
                         fai::error_model_enum error_model,                             // FAI
                         py::c_array_t<float> variance,                                 // Variance
                         py::c_array_t<bool> mask,                                      // Mask
                         py::c_array_t<float> dark, py::c_array_t<float> dark_variance, // Dark
                         py::c_array_t<float> flat,                                     // Flat
                         py::c_array_t<float> solid_angle,                              // Solid angle
                         py::c_array_t<float> polarization,                             // Polarization
                         py::c_array_t<float> absorption,                               // Absorption
                         float dummy, float delta_dummy,                                // Dummy
                         float normalization_factor,                                    //
                         py::c_array_t<float> csr_data, py::c_array_t<int> csr_indices,
                         py::c_array_t<int> csr_indptr,    // CSR
                         float cutoff_clip,                //
                         int cycle,                        //
                         float empty,                      //
                         py::c_array_t<float> radius2d,    // Peak finding
                         py::c_array_t<float> radius1d,    //
                         float noise,                      //
                         float cutoff_pick,                //
                         int acc_nb_frames_reset,          // Accumulation
                         int acc_nb_frames_xfer,           //
                         std::string const& cl_source_path // Path to the CL source files
                      ) {
                 // Filter for a 1.2 platform and set it as the default
                 auto platforms = bcl::system::platforms();
                 if (platforms.empty()) {
                     throw std::runtime_error("No platform found. Check OpenCL installation!");
                 }

                 std::optional<bcl::platform> plat;
                 for (auto& p : platforms) {
                     std::cout << "Plat: " << p.name() << "\n";
                     if (p.check_version(1, 2)) {
                         plat = p;
                     }
                 }
                 if (!plat) {
                     throw std::runtime_error("No OpenCL 1.2 platform found.");
                 }

                 // Get the list of GPU devices of the platform
                 auto devices = plat->devices(CL_DEVICE_TYPE_GPU);
                 if (devices.empty()) {
                     throw std::runtime_error("No device found. Check OpenCL installation!");
                 }
                 bcl::device device = devices[gpu_device_idx];

                 // Getting information about used queue and device
                 const size_t max_compute_units = device.get_info<CL_DEVICE_MAX_COMPUTE_UNITS>();
                 const size_t max_work_group_size = device.get_info<CL_DEVICE_MAX_WORK_GROUP_SIZE>();

                 std::cout << "Dev: " << device.name() << std::endl;

                 // Create context
                 bcl::context context(device);

                 LOG(variance);
                 LOG(mask);
                 LOG(csr_data);
                 LOG(csr_indices);
                 LOG(csr_indptr);
                 LOG(radius2d);
                 LOG(radius1d);

                 return pyfai::peak_finder(context,                               //
                                           width, height,                         //
                                           nb_bins,                               // FAI
                                           error_model,                           //
                                           dummy, delta_dummy,                    //
                                           variance,                              // Preprocessing
                                           mask, dark, dark_variance, flat,       //
                                           solid_angle, polarization, absorption, //
                                           normalization_factor,                  //
                                           csr_data, csr_indices, csr_indptr,     // Sigma clipping
                                           cutoff_clip,                           //
                                           cycle,                                 //
                                           empty,                                 //
                                           radius2d, radius1d,                    // Peak finding
                                           noise,                                 //
                                           cutoff_pick,                           //
                                           acc_nb_frames_reset,                   // Accumulation
                                           acc_nb_frames_xfer,                    //
                                           cl_source_path);                       // CL source file path
             }),
             "Construct a PeakFinder callable", //
             "gpu_device_idx"_a, "width"_a, "height"_a, "nb_bins"_a, "error_model"_a = fai::error_model_enum::poisson,
             "dummy"_a, "delta_dummy"_a, "variance"_a, "mask"_a, "dark"_a, "dark_variance"_a, "flat"_a, "solidangle"_a,
             "polarization"_a, "absorption"_a, "normalization_factor"_a = 1.0, "csr_data"_a, "csr_indices"_a,
             "csr_indptr"_a, "cutoff_clip"_a = 5.0, "cycle"_a = 5, "empty"_a = -1.0, "radius2d"_a, "radius1d"_a,
             "noise"_a = 0.5, "cutoff_pick"_a = 3.0, "acc_nb_frames_reset"_a = 0, "acc_nb_frames_xfer"_a = 0,
             "cl_source_path"_a = "detectors/psi/processing/src")
        .def("__call__", &pyfai::peak_finder::apply,
             "Perform a sigma-clipping iterative filter within each bin along each row.", "data"_a,
             "force_acc"_a = false)
        .def("__repr__", [](const pyfai::peak_finder& pf) {
            return fmt::format("<PeakFinder on '{}'>", pf.context.get_device().name());
        });
}
