kernel void init(global float* array0, global float* array1, global float8* array2, unsigned int length)
{
    int i = get_global_id(0);
    //Global memory guard for padding
    if (i < length) {
        array0[i] = 0.0f;
        array1[i] = 0.0f;
        array2[i] = (float8)(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
    }
}
