typedef uchar uint8_t;
typedef ushort uint16_t;
typedef uint uint32_t;
typedef ulong uint64_t;

/**
    @short  structure for a Detector Packet or Image Header
    @li frameNumber is the frame number
    @li expLength is the subframe number (32 bit eiger) or real time
    exposure time in 100ns (others)
    @li packetNumber is the packet number
    @li bunchId is the bunch id from beamline
    @li timestamp is the time stamp with 10 MHz clock
    @li modId is the unique module id (unique even for left, right, top,
    bottom)
    @li row is the row index in the complete detector system
    @li column is the column index in the complete detector system
    @li reserved is reserved
    @li debug is for debugging purposes
    @li roundRNumber is the round robin set number
    @li detType is the detector type see :: detectorType
    @li version is the version number of this structure format
*/

typedef struct
{
    uint64_t frameNumber;
    uint32_t expLength;
    uint32_t packetNumber;
    uint64_t bunchId;
    uint64_t timestamp;
    uint16_t modId;
    uint16_t row;
    uint16_t column;
    uint16_t reserved;
    uint32_t debug;
    uint16_t roundRNumber;
    uint8_t detType;
    uint8_t version;
} sls_detector_header;

__constant size_t module_width = 1024;
__constant size_t module_height = 512;

__constant size_t nb_lines_per_datagrams = 4;                                      // 4 lines per UDP Datagram
__constant size_t nb_datagrams_per_image = module_height / nb_lines_per_datagrams; // 128 UDP Datagrams per image

__constant size_t header_size = sizeof(sls_detector_header);
__constant size_t data_size = nb_lines_per_datagrams * module_width * sizeof(uint16_t);
__constant size_t datagram_size = header_size + data_size;

// \arg width datagram_size
// \arg height nb_datagrams_per_image
__kernel void create_lut(__global char* datagrams, __global char* glut, int width, int height)
{
    size_t j = get_global_id(1);

    if (j < height) {
        __global sls_detector_header* header = (__global sls_detector_header*) (datagrams + j * width);
        glut[j] = header->packetNumber + header->row * nb_datagrams_per_image / 2;  //Half module
    }
}

// \arg width nb of colums in the target image
// \arg height nb of rows in the target image
__kernel void reorder(__global uint16_t* datagrams, __global uint16_t* image, __global char* glut, int width,
                      int height)
{
    size_t i = get_global_id(0);
    size_t j = get_global_id(1);

    if (i < width && j < nb_datagrams_per_image) {
        __global uint16_t* data = (__global uint16_t*) (datagrams + header_size + j * datagram_size);

        int jj = nb_lines_per_datagrams * glut[j];

        // Jungfrau send 4 lines at once
        image[i + (jj + 0) * width] = data[i + 0 * width];
        image[i + (jj + 1) * width] = data[i + 1 * width];
        image[i + (jj + 2) * width] = data[i + 2 * width];
        image[i + (jj + 3) * width] = data[i + 3 * width];
    }
}
