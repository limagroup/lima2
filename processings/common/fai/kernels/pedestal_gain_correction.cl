__kernel void pedestal_gain_correction(__global const unsigned short* in, __global const float* gain, __global const float* pedestal, __global float* res, unsigned int length)
{
    size_t i = get_global_id(0);
    float result = 0.0f;

    // bound check
    if (i < length)
    {
        // gain level : 2 MSB bits
        unsigned short lvl = in[i] >> 14;
        unsigned short val = in[i];

        // G0 = 0b00, G1 = 0b01, G2 = 0b11
        lvl = (lvl == 3) ? 2 : lvl;

        float noise = pedestal[i + lvl * length];
        float factor = gain[i + lvl * length];

        //printf(" %x %x %x %f %f ", i, lvl, val, noise, factor);

        result = (val - noise) * factor;

        res[i] = result;
    }
}