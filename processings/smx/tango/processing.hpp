// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tango.h>

#include <boost/exception/diagnostic_information.hpp>

#include <lima/tango/processing_device.hpp>

#include <lima/processing/pipelines/smx.describe.hpp>

namespace lima::tango::smx
{
class BOOST_SYMBOL_VISIBLE processing : public processing_device
{
  public:
    using processing_t = lima::processing::pipelines::smx::pipeline;
    using frame_info_t = typename processing_t::frame_info_t;
    using proc_params_t = typename processing_t::proc_params_t;
    using acq_params_t = typename processing_t::acq_params_t;
    using det_info_t = typename processing_t::det_info_t;
    using progress_counters_t = typename processing_t::progress_counters_t;

    // Constructors and destructors
    processing(Tango::DeviceClass* cl, std::string const& name) : processing_device(cl, name) { init_device(); }
    processing(Tango::DeviceClass* cl, std::string const& name, std::string const& description) :
        processing_device(cl, name, description)
    {
        init_device();
    }
    ~processing() { delete_device(); }

    /// Miscellaneous methods
    ///{
    /// Will be called at device destruction or at init command
    void delete_device() override { DEBUG_STREAM << "processing::delete_device() " << device_name << std::endl; }

    /// Initialize the device
    void init_device() override;

    /// Always executed method before execution command method
    void always_executed_hook() override;
    ///}

    /// Attribute related methods
    ///{

    /// Hardware acquisition for attributes
    virtual void read_attr_hardware(std::vector<long>& attr_list) override;

    /// Hardware writing for attributes
    virtual void write_attr_hardware(std::vector<long>& attr_list) override;

    /// Add dynamic attributes if any
    void add_dynamic_attributes();
    ///}

    /// Processing control interface
    ///{

    bool is_finished() const override;

    std::string last_error() const override;

    void activate() override
    {
        try {
            m_proc->activate();
        } catch (std::exception& e) {
            Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                           "processing::activate");
        }
    }

    void process(frame frm) override
    {
        try {
            m_proc->process(frm);
        } catch (std::exception& e) {
            Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                           "processing::process");
        }
    }

    void abort() override
    {
        try {
            m_proc->abort();
        } catch (std::exception& e) {
            Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                           "processing::abort");
        }
    }

    ///}

    /// Processing data access interface
    ///{

    /// Returns the progress counters
    progress_counters_t progress_counters() const { return m_proc->progress_counters(); }

    /// Returns the radius1d
    Tango::DevEncoded radius1d() const;

    /// Returns the radius2d_mask
    Tango::DevEncoded radius2d_mask() const;

    /// Pop ROI counters/profiles from the buffer
    Tango::DevEncoded pop_roi_statistics() const;
    Tango::DevEncoded pop_roi_profiles() const;

    int nb_roi_statistics() const { return m_proc->nb_roi_statistics(); }
    int nb_roi_profiles() const { return m_proc->nb_roi_profiles(); }

    /// Pop peak counters from the buffer
    Tango::DevEncoded pop_peak_counters() const;

    /// Returns a frame for a given index
    Tango::DevEncoded get_frame(std::size_t frame_idx) const;

    /// Returns a sparse frame for a given index
    Tango::DevEncoded get_sparse_frame(std::size_t frame_idx) const;

    frame_info_t input_frame_info() const { return m_proc->input_frame_info(); }
    frame_info_t processed_frame_info() const { return m_proc->processed_frame_info(); }

    /// Returns an accumulated frame after (hw) correction
    Tango::DevEncoded get_acc_corrected(std::size_t frame_idx) const;

    /// Returns an accumulated frame after reduction
    Tango::DevEncoded get_acc_peaks(std::size_t frame_idx) const;

    /// Add dynamic commands if any
    void add_dynamic_commands();

    ///}

    // Attribute data members
    std::string m_last_error;

  private:
    std::unique_ptr<processing_t> m_proc;
};

// Additional Classes Definitions

} // namespace lima::tango::smx
