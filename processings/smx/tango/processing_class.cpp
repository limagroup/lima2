// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/dll/alias.hpp>

#include <boost/json/chrono.hpp>
#include <boost/json/filesystem.hpp>
#include <boost/json/describe.hpp>
#include <boost/json/schema_from.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/mp11.hpp>

#include <lima/hw/params.describe.hpp>
#include <lima/utils/type_traits.hpp>

#include <lima/tango/base_attribute.hpp>
#include <lima/tango/base_command.hpp>
#include <lima/tango/convert.hpp>

#include "processing_class.hpp"

namespace lima::tango::smx
{

//=========================================
//	Define classes for attributes
//=========================================

template <typename Device, auto Pm>
class DevEncodedReadAttr : public Tango::Attr
{
  public:
    using Tango::Attr::Attr;

    void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
    {
        //DEBUG_STREAM << "Reading attribute " << D.name << std::endl;
        Device* d = static_cast<Device*>(dev);

        Tango::DevEncoded* t_val = new Tango::DevEncoded;
        if constexpr (std::is_member_function_pointer_v<decltype(Pm)>)
            *t_val = (d->*Pm)();
        else
            *t_val = d->*Pm;

        // Set the attribute value
        attr.set_value(t_val);
    }

    bool is_allowed(Tango::DeviceImpl* dev, Tango::AttReqType ty) override { return true; }
};

//=========================================
// Define classes for commands
//=========================================

class pop_roi_statistics_command : public Tango::Command
{
  public:
    pop_roi_statistics_command() :
        Tango::Command("popRoiStatistics", Tango::DEV_VOID, Tango::DEV_ENCODED, "",
                       "A DevEncoded structured array of ROI [frame_idx, recv_idx, min, max, avg, std, sum]",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "pop_roi_statistics_command::execute(): arrived" << std::endl;

        Tango::DevEncoded res = static_cast<processing*>(dev)->pop_roi_statistics();

        return insert(new Tango::DevEncoded(res));
    }
};

class pop_roi_profiles_command : public Tango::Command
{
  public:
    pop_roi_profiles_command() :
        Tango::Command("popRoiProfiles", Tango::DEV_VOID, Tango::DEV_ENCODED, "",
                       "A DevEncoded structured array of ROI profiles [frame_idx, recv_idx, min, max, avg, std, sum]",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "pop_roi_profiles_command::execute(): arrived" << std::endl;

        Tango::DevEncoded res = static_cast<processing*>(dev)->pop_roi_profiles();

        return insert(new Tango::DevEncoded(res));
    }
};

class pop_peak_counters_command : public Tango::Command
{
  public:
    pop_peak_counters_command() :
        Tango::Command("popPeakCounters", Tango::DEV_VOID, Tango::DEV_ENCODED, "",
                       "A DevEncoded structured array of peaks [frame_idx, recv_idx, nb_peaks]", Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "pop_peak_counters_command::execute(): arrived" << std::endl;

        Tango::DevEncoded res = static_cast<processing*>(dev)->pop_peak_counters();

        return insert(new Tango::DevEncoded(res));
    }
};

class get_frame_command : public Tango::Command
{
  public:
    get_frame_command() :
        Tango::Command("getFrame", Tango::DEV_LONG, Tango::DEV_ENCODED, "Frame Index", "A DevEncoded frame",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "get_frame_command::execute(): arrived" << std::endl;

        Tango::DevLong frame_idx;
        extract(in_any, frame_idx);

        Tango::DevEncoded res = static_cast<processing*>(dev)->get_frame(frame_idx);

        return insert(new Tango::DevEncoded(res));
    }
};

class get_sparse_frame_command : public Tango::Command
{
  public:
    get_sparse_frame_command() :
        Tango::Command("getSparseFrame", Tango::DEV_LONG, Tango::DEV_ENCODED, "Frame Index",
                       "A DevEncoded sparse frame", Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "get_sparse_frame_command::execute(): arrived" << std::endl;

        Tango::DevLong frame_idx;
        extract(in_any, frame_idx);

        Tango::DevEncoded res = static_cast<processing*>(dev)->get_sparse_frame(frame_idx);

        return insert(new Tango::DevEncoded(res));
    }
};

class get_acc_corrected_command : public Tango::Command
{
  public:
    get_acc_corrected_command() :
        Tango::Command("getAccCorrected", Tango::DEV_LONG, Tango::DEV_ENCODED, "Frame Index", "A DevEncoded frame",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "get_acc_corrected_command::execute(): arrived" << std::endl;

        Tango::DevLong frame_idx;
        extract(in_any, frame_idx);

        Tango::DevEncoded res = static_cast<processing*>(dev)->get_acc_corrected(frame_idx);

        return insert(new Tango::DevEncoded(res));
    }
};

class get_acc_peaks_command : public Tango::Command
{
  public:
    get_acc_peaks_command() :
        Tango::Command("getAccPeaks", Tango::DEV_LONG, Tango::DEV_ENCODED, "Frame Index", "A DevEncoded frame",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "get_acc_peaks_command::execute(): arrived" << std::endl;

        Tango::DevLong frame_idx;
        extract(in_any, frame_idx);

        Tango::DevEncoded res = static_cast<processing*>(dev)->get_acc_peaks(frame_idx);

        return insert(new Tango::DevEncoded(res));
    }
};

//===================================================================
// Properties management
//===================================================================

void processing_class::set_default_property()
{
    // Set Default Class Properties

    // Set Default device Properties
}

void processing_class::write_class_property()
{
    // First time, check if database used
    if (Tango::Util::_UseDb == false)
        return;

    Tango::DbData data;
    std::string classname = get_name();
    std::string header;
    std::string::size_type start, end;

    // Put title
    Tango::DbDatum title("ProjectTitle");
    std::string str_title("LIMA2");
    title << str_title;
    data.push_back(title);

    // Put Description
    Tango::DbDatum description("Description");
    std::vector<std::string> str_desc;
    str_desc.push_back("LIMA2 Processing Class");
    description << str_desc;
    data.push_back(description);

    //  Put inheritance
    Tango::DbDatum inher_datum("InheritedFrom");
    std::vector<std::string> inheritance;
    inheritance.push_back("TANGO_BASE_CLASS");
    inher_datum << inheritance;
    data.push_back(inher_datum);

    // Call database and add values
    get_db_class()->put_property(data);
}

//===================================================================
// Factory methods
//===================================================================

processing_device* processing_class::create_device(std::string const& instance_name)
{
    processing* dev = new processing(this, instance_name);

    // Add it into the device list
    device_list.push_back(dev);

    // Export devices to the outside world
    if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
        export_device(dev);
    else
        export_device(dev, dev->get_name().c_str());

    return dev;
}

void processing_class::attribute_factory(std::vector<Tango::Attr*>& att_list)
{
    // Add proc_params JSON schema attribute property
    {
        // Generate the schema for the plugin proc_params
        boost::json::object schema = boost::json::schema_from<typename device_t::proc_params_t>("proc_params");
        boost::json::object& properties = schema["properties"].as_object();
        // clang-format off
        properties.insert({{
            "class_name", {
                {"type", "string"},
                {"description", "Name of the processing class"},
                {"default", "LimaProcessingSmx"}
            }
        }});
        // clang-format on

        // Add a "schema" class attribute property
        Tango::DbDatum db_nb_attrs("proc_params");
        Tango::DbDatum db_schema("schema");

        db_nb_attrs << 1; // One "schema" property for attribute "params"
        std::string schema_str =
            boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
        db_schema << schema_str;

        // Push to the DB
        Tango::DbData db_data;
        db_data.push_back(db_nb_attrs);
        db_data.push_back(db_schema);

        get_db_class()->put_attribute_property(db_data);
    }

    // Add progress_counters JSON attribute
    {
        using counters_attr_t = JsonReadAttr<device_t, &processing::progress_counters>;
        counters_attr_t* attr = new counters_attr_t("progress_counters", Tango::DEV_STRING, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Processing counters";
        attr_prop.label = "progress_counters";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add progress_counters JSON schema attribute property
    {
        // Generate the schema for the plugin progress_counters
        boost::json::value schema =
            boost::json::schema_from<typename device_t::progress_counters_t>("progress_counters");

        // Add a "schema" class attribute property
        Tango::DbDatum db_nb_attrs("progress_counters");
        Tango::DbDatum db_schema("schema");

        db_nb_attrs << 1; // One "schema" property for attribute "progress_counters"
        std::string schema_str =
            boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
        db_schema << schema_str;

        // Push to the DB
        Tango::DbData db_data;
        db_data.push_back(db_nb_attrs);
        db_data.push_back(db_schema);

        get_db_class()->put_attribute_property(db_data);
    }

    // Add radius1d DevEncoded attribute
    {
        using radius1d_attr_t = DevEncodedReadAttr<device_t, &processing::radius1d>;
        Tango::Attr* attr = new radius1d_attr_t("radius1d", Tango::DEV_ENCODED, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Radial distance";
        attr_prop.label = "radius1d";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add radius2d_mask DevEncoded attribute
    {
        using radius2d_mask_attr_t = DevEncodedReadAttr<device_t, &processing::radius2d_mask>;
        Tango::Attr* attr = new radius2d_mask_attr_t("radius2d_mask", Tango::DEV_ENCODED, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Mask and pixel radius";
        attr_prop.label = "radius2d_mask";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add input_frame_info JSON attribute
    {
        using input_frame_info_t = JsonReadAttr<device_t, &processing::input_frame_info>;
        input_frame_info_t* attr = new input_frame_info_t("input_frame_info", Tango::DEV_STRING, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Input frame info";
        attr_prop.label = "input_frame_info";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add processed_frame_info JSON attribute
    {
        using processed_frame_info_t = JsonReadAttr<device_t, &processing::processed_frame_info>;
        processed_frame_info_t* attr =
            new processed_frame_info_t("processed_frame_info", Tango::DEV_STRING, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Processed frame info";
        attr_prop.label = "processed_frame_info";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add is_finished attribute
    att_list.push_back(new is_finished_attr());

    // Add last_error attribute
    att_list.push_back(new last_error_attr());

    // Add nb_roi_statistics attribute
    class nb_roi_statistics_attr : public Tango::Attr
    {
      public:
        nb_roi_statistics_attr() : Attr("nb_roi_statistics", Tango::DEV_LONG, Tango::READ){};
        virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
        {
            int nb_roi_statistics = (static_cast<device_t*>(dev))->nb_roi_statistics();

            Tango::DevLong* t_nb_roi_statistics = new Tango::DevLong();
            tango_encode(nb_roi_statistics, *t_nb_roi_statistics);

            att.set_value(t_nb_roi_statistics, 1, 0, true);
        }
    };

    att_list.push_back(new nb_roi_statistics_attr());

    // Add nb_roi_profiles attribute
    class nb_roi_profiles_attr : public Tango::Attr
    {
      public:
        nb_roi_profiles_attr() : Attr("nb_roi_profiles", Tango::DEV_LONG, Tango::READ){};
        virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
        {
            int nb_roi_profiles = (static_cast<device_t*>(dev))->nb_roi_profiles();

            Tango::DevLong* t_nb_roi_profiles = new Tango::DevLong();
            tango_encode(nb_roi_profiles, *t_nb_roi_profiles);

            att.set_value(t_nb_roi_profiles, 1, 0, true);
        }
    };

    att_list.push_back(new nb_roi_profiles_attr());
}

void processing_class::pipe_factory() {}

void processing_class::command_factory()
{
    command_list.emplace_back(new pop_roi_statistics_command());
    command_list.emplace_back(new pop_roi_profiles_command());
    command_list.emplace_back(new pop_peak_counters_command());
    command_list.emplace_back(new get_frame_command());
    command_list.emplace_back(new get_sparse_frame_command());
    command_list.emplace_back(new get_acc_corrected_command());
    command_list.emplace_back(new get_acc_peaks_command());
}

// Factory method
static processing_class* create()
{
    return processing_class::init("LimaProcessingSmx");
}

BOOST_DLL_ALIAS(create,      // <-- this function is exported with...
                create_class // <-- ...this alias name
)

} // namespace lima::tango::smx
