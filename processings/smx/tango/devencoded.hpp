// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstring>
#include <vector>

#include <tango.h>

#include <lima/processing/pipelines/smx/sparse_frame.hpp>

namespace lima::tango
{

#pragma pack(push, 1)

struct smx_sparse_header
{
    static const int header_len = 16;

    std::uint16_t width = 0;
    std::uint16_t height = 0;
    std::uint32_t nb_bins = 0;
    std::uint32_t nb_peaks = 0;
    std::uint32_t frame_idx = 0;
};

#pragma pack(pop)

inline Tango::DevEncoded make_devencoded(processing::pipelines::smx::sparse_frame const& frm)
{
    Tango::DevEncoded res;
    res.encoded_format = "SMX_SPARSE_FRAME";
    res.encoded_data.length(sizeof(smx_sparse_header) + frm.size_in_bytes());

    smx_sparse_header header;
    header.width = frm.width();
    header.height = frm.height();
    header.nb_bins = frm.nb_bins();
    header.nb_peaks = frm.nb_peaks();
    header.frame_idx = frm.metadata.idx;

    unsigned char* buffer = res.encoded_data.get_buffer();

    std::memcpy(buffer, &header, sizeof(smx_sparse_header));
    buffer += sizeof(smx_sparse_header);
    std::copy(frm.peak_indices.begin(), frm.peak_indices.end(), reinterpret_cast<int*>(buffer));
    buffer += frm.peak_indices.size() * sizeof(int);
    std::copy(frm.peak_values.begin(), frm.peak_values.end(), reinterpret_cast<float*>(buffer));
    buffer += frm.peak_values.size() * sizeof(float);
    std::copy(frm.background_avg.begin(), frm.background_avg.end(), reinterpret_cast<float*>(buffer));
    buffer += frm.background_avg.size() * sizeof(float);
    std::copy(frm.background_std.begin(), frm.background_std.end(), reinterpret_cast<float*>(buffer));

    return res;
}

} // namespace lima::tango