# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_library(pipeline_${PROJECT_NAME}_tango SHARED
    processing.cpp
    processing_class.cpp
)

target_include_directories(pipeline_${PROJECT_NAME}_tango
    PUBLIC "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
    PUBLIC "$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>"
)

target_link_libraries(pipeline_${PROJECT_NAME}_tango PUBLIC
    lima_core 
    lima_tango_headers
    pipeline_smx
)

# Set version and output name
set_target_properties(pipeline_${PROJECT_NAME}_tango PROPERTIES
    OUTPUT_NAME "lima_${PROJECT_NAME}_tango"
    VERSION "${PROJECT_VERSION}"
    SOVERSION "${PACKAGE_VERSION_MAJOR}.${PACKAGE_VERSION_MINOR}")

install(
    TARGETS pipeline_${PROJECT_NAME}_tango
    EXPORT "${TARGETS_EXPORT_NAME}"
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/plugins   # .so files are libraries
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/plugins   # .dll files are binaries
)