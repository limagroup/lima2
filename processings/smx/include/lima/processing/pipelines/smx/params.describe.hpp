// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/arc.describe.hpp>
#include <lima/core/rectangle.describe.hpp>
#include <lima/hw/params.describe.hpp>
#include <lima/hw/info.describe.hpp>
#include <lima/io/h5/params.describe.hpp>

#include <lima/processing/fai/enums.describe.hpp>

#include <lima/processing/pipelines/smx/params.hpp>

// This file is part of the user interface of the library and should not include any private dependencies
// or other thing that expose implementation details

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        BOOST_DESCRIBE_STRUCT(fifo_params, (), (nb_fifo_frames))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(fifo_params, nb_fifo_frames,
            (desc, "number of frames in FIFO"),
            (doc, "The number of frames in the Processing FIFO"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(thread_params, (), (nb_threads, numa_node))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(thread_params, nb_threads,
            (desc, "number of threads"),
            (doc, "The number of threads in the Processing"))

        BOOST_ANNOTATE_MEMBER(thread_params, numa_node,
            (desc, "numa node"),
            (doc, "The numa node of Processing FIFO"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(buffer_params, (),
                              (nb_roi_statistics_buffer, nb_roi_profiles_buffer, nb_peak_counters_buffer,
                               nb_frames_buffer))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(buffer_params, nb_roi_statistics_buffer,
            (desc, "nb roi statistics in buffer"),
            (doc, "The initial capacity of ROI statistics buffer"))

        BOOST_ANNOTATE_MEMBER(buffer_params, nb_roi_profiles_buffer,
            (desc, "nb roi profiles in buffer"),
            (doc, "The initial capacity of ROI profiles buffer"))

        BOOST_ANNOTATE_MEMBER(buffer_params, nb_peak_counters_buffer,
            (desc, "number of peaks in buffer"),
            (doc, "The number of peaks in the Circular buffer"))

        BOOST_ANNOTATE_MEMBER(buffer_params, nb_frames_buffer,
            (desc, "number of frames in buffer"),
            (doc, "The number of frames in the Circular buffer"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(roi_statistics_params, (), (enabled, rect_rois, arc_rois))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(roi_statistics_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the ROI statistics"))

        BOOST_ANNOTATE_MEMBER(roi_statistics_params, rect_rois,
            (desc, "rectangular rois"),
            (doc, "A vector of rectangular ROIs for the statistics"))

        BOOST_ANNOTATE_MEMBER(roi_statistics_params, arc_rois,
            (desc, "arc rois"),
            (doc, "A vector of arc ROIs for the statistics"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(roi_profiles_params, (), (enabled, rois, directions))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(roi_profiles_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the ROI statistics"))

        BOOST_ANNOTATE_MEMBER(roi_profiles_params, rois,
            (desc, "profiles"),
            (doc, "A vector of rectangular ROIs"))

        BOOST_ANNOTATE_MEMBER(roi_profiles_params, directions,
            (desc, "directions"),
            (doc, "A vector of direction of the profiles"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(gpu_params, (), (platform_idx, device_idx))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(gpu_params, platform_idx,
            (desc, "platform idx"),
            (doc, "The index of the GPU platform in the list of OpenCL 3.0 platforms"))

        BOOST_ANNOTATE_MEMBER(gpu_params, device_idx,
            (desc, "device idx"),
            (doc, "The index of the GPU device in the list of OpenCL 3.0 devices"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(fai_params, (),
                              (error_model, variance_path, mask_path, dark_path, dark_variance_path, flat_path,
                               solid_angle_path, polarization_path, absorption_path, dummy, delta_dummy,
                               normalization_factor, csr_path, cutoff_clip, cycle, radius2d_path, radius1d_path, noise,
                               cutoff_pick, acc_nb_frames_reset, acc_nb_frames_xfer, cl_source_path))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(fai_params, error_model,
            (desc, "error model"),
            (doc, "The error model used [poisson, azimuthal]"))

        BOOST_ANNOTATE_MEMBER(fai_params, variance_path,
            (desc, "variance path"),
            (doc, "The path to the variance data"))

        BOOST_ANNOTATE_MEMBER(fai_params, mask_path,
            (desc, "mask path"),
            (doc, "The mask to the variance data [masked value = 1]"))

        BOOST_ANNOTATE_MEMBER(fai_params, dark_path,
            (desc, "dark path"),
            (doc, "The path to the variance data"))
        
        BOOST_ANNOTATE_MEMBER(fai_params, dark_variance_path,
            (desc, "dark variance path"),
            (doc, "The path to the dark variance data"))

        BOOST_ANNOTATE_MEMBER(fai_params, flat_path,
            (desc, "flat path"),
            (doc, "The path to the flat data"))

        BOOST_ANNOTATE_MEMBER(fai_params, solid_angle_path,
            (desc, "solid angle path"),
            (doc, "The path to the solid angle data"))

        BOOST_ANNOTATE_MEMBER(fai_params, polarization_path,
            (desc, "polarization path"),
            (doc, "The path to the polarization data"))

        BOOST_ANNOTATE_MEMBER(fai_params, absorption_path,
            (desc, "absorption path"),
            (doc, "The path to the absorption data"))

        BOOST_ANNOTATE_MEMBER(fai_params, dummy,
            (desc, "dummy"),
            (doc, "The nodata value"))

        BOOST_ANNOTATE_MEMBER(fai_params, delta_dummy,
            (desc, "delta_dummy"),
            (doc, "A delta headroom around the nodata value"))

        BOOST_ANNOTATE_MEMBER(fai_params, normalization_factor,
            (desc, "normalization factor"),
            (doc, "A normalization factor applied to all pixels"))

        BOOST_ANNOTATE_MEMBER(fai_params, csr_path,
            (desc, "csr path"),
            (doc, "The path to the CSR matrix file"))

        BOOST_ANNOTATE_MEMBER(fai_params, cutoff_clip,
            (desc, "cutoff_clip"),
            (doc, "Cutoff value of sigma-clipping"))

        BOOST_ANNOTATE_MEMBER(fai_params, cycle,
            (desc, "cycle"),
            (doc, "Max number of cycle of sigma-clipping"))

        BOOST_ANNOTATE_MEMBER(fai_params, empty,
            (desc, "todo"),
            (doc, "TODO"))

        BOOST_ANNOTATE_MEMBER(fai_params, radius2d_path,
            (desc, "radius2d path"),
            (doc, "The path to the radius2d data"))

        BOOST_ANNOTATE_MEMBER(fai_params, radius1d_path,
            (desc, "radius1d path"),
            (doc, "The path to the radius1d data"))

        BOOST_ANNOTATE_MEMBER(fai_params, noise,
            (desc, "noise"),
            (doc, "The path to the radius1d data"))

        BOOST_ANNOTATE_MEMBER(fai_params, cutoff_pick,
            (desc, "cutoff_pick"),
            (doc, "Cutoff value of peak finding"))

        BOOST_ANNOTATE_MEMBER(fai_params, acc_nb_frames_reset,
            (desc, "acc nb frames reset"),
            (doc, "The number of frames when the accumulators are reset"))

        BOOST_ANNOTATE_MEMBER(fai_params, acc_nb_frames_xfer,
            (desc, "acc nb frames xfer"),
            (doc, "The number of frames when the accumulators are transferred to host"))

        BOOST_ANNOTATE_MEMBER(fai_params, cl_source_path,
            (desc, "cl_source_path"),
            (doc, "The path to the OpenCL kernels"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(saving_params, (io::h5::saving_params), (enabled))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(saving_params, enabled,
            (desc, "enabled"),
            (doc, "Enable/disable the saving stream"))
        // clang-format on

        BOOST_DESCRIBE_STRUCT(proc_params, (),
                              (fifo, thread, buffers, saving_dense, saving_sparse, saving_accumulation_corrected,
                               saving_accumulation_peak, gpu, fai, statistics, profiles))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(proc_params, fifo,
            (desc, "fifo parameters"),
            (doc, "The processing FIFO parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, thread,
            (desc, "thread parameters"),
            (doc, "The processing thread parameters"))
            
        BOOST_ANNOTATE_MEMBER(proc_params, buffers,
            (desc, "buffers parameters"),
            (doc, "The circulatr buffer parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, saving_dense,
            (desc, "saving parameters raw"),
            (doc, "The HDF5 saving parameters for the raw frame"))

        BOOST_ANNOTATE_MEMBER(proc_params, saving_sparse,
            (desc, "saving parameters assembled"),
            (doc, "The HDF5 saving parameters for the assembled frame"))

        BOOST_ANNOTATE_MEMBER(proc_params, saving_accumulation_corrected,
            (desc, "saving parameters accumulation corrected"),
            (doc, "The HDF5 saving parameters for the accumulation of corrected frames"))

        BOOST_ANNOTATE_MEMBER(proc_params, saving_accumulation_peak,
            (desc, "saving parameters accumulation peak"),
            (doc, "The HDF5 saving parameters for the accumulation of found peak frames"))

        BOOST_ANNOTATE_MEMBER(proc_params, gpu,
            (desc, "gpu params"),
            (doc, "The GPU (OpenCL) parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, fai,
            (desc, "peak finder params"),
            (doc, "The peak finder (Fast Azimuthal Integrations) parameters"))
            
        BOOST_ANNOTATE_MEMBER(proc_params, statistics,
            (desc, "roi statistics parameters"),
            (doc, "The ROI statistics parameters"))

        BOOST_ANNOTATE_MEMBER(proc_params, profiles,
            (desc, "roi profiles parameters"),
            (doc, "The ROI profiles parameters"))
        // clang-format on

    } // namespace smx
} // namespace processing::pipelines
} // namespace lima
