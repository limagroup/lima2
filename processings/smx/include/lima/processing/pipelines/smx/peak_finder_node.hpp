// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory>
#include <variant>

#include <boost/gil/extension/dynamic_image/apply_operation.hpp>

#include <tbb/flow_graph.h>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>

#include <lima/core/enums.hpp>
#include <lima/core/frame_view.hpp>

#include <lima/processing/pipelines/smx/sparse_frame.hpp>

#include <lima/processing/fai/kernel_params.hpp>
#include <lima/processing/fai/peak_finder.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        namespace bcl = boost::compute;

        using accum_result_t = std::tuple<lima::frame, lima::frame>;

        struct peak_finder_node : public tbb::flow::multifunction_node<frame, std::tuple<sparse_frame, accum_result_t>>
        {
            using parent_t = tbb::flow::multifunction_node<frame, std::tuple<sparse_frame, accum_result_t>>;

            template <typename V>
            struct peak_finder_body
            {
                using view_t = V;
                using const_view_t = typename V::const_t;
                using pixel_t = typename V::value_type;

                peak_finder_body(std::size_t width, std::size_t height, fai::kernel_params const& params,
                                 std::filesystem::path const& cl_source_path, bcl::context& context,
                                 bcl::command_queue& queue) :
                    context(context),
                    queue(queue),
                    nb_pixels(width * height),
                    nb_bins(params.csr_indptr.size() - 1),
                    input_d(nb_pixels, context),                             // Input
                    peak_indices_d(nb_pixels, context),                      // Outputs Peaks
                    peak_values_d(nb_pixels, context),                       //
                    background_avg_d(nb_bins, context),                      // Background
                    background_std_d(nb_bins, context),                      //
                    acc_nb_frames_reset(params.acc_nb_frames_reset),         // Accumulation
                    acc_nb_frames_xfer(params.acc_nb_frames_xfer),           // Accumulation
                    acc_corr_d(acc_nb_frames_xfer ? nb_pixels : 0, context), //
                    acc_peak_d(acc_nb_frames_xfer ? nb_pixels : 0, context), //
                    debug(false),
                    preprocessed_d(debug ? nb_pixels : 0), // Preprocessed
                    cliped_d(debug ? nb_bins : 0),         // Cliped (after sigma clip)
                    found_d(debug ? nb_pixels : 0)         // Found (after peak find)
                {
                    LIMA_LOG(trace, proc) << "nb_pixels:" << nb_pixels;
                    LIMA_LOG(trace, proc) << "nb_bins:" << nb_bins;

                    // Create the peakfinder for the kernel params
                    pf = fai::make_peak_finder<pixel_t>(
                        context,       //
                        width, height, //
                        params.error_model, params.variance, params.mask, params.dark, params.dark_variance,
                        params.flat, params.solid_angle, params.polarization, params.absorption, params.dummy,
                        params.delta_dummy, params.normalization_factor, params.csr_data, params.csr_indices,
                        params.csr_indptr, params.cutoff_clip, params.cycle, params.empty, params.radius2d,
                        params.radius1d, params.noise, params.cutoff_pick, acc_nb_frames_reset,
                        acc_nb_frames_xfer, //
                        cl_source_path, queue);

                    LIMA_LOG(trace, proc) << "Peak finder created";
                }

                void operator()(lima::frame const& in, typename parent_t::output_ports_type& ports) const
                {
                    if (in.empty()) {
                        LIMA_LOG(trace, proc) << "Skipping empty data";
                        return;
                    }

                    assert(nb_pixels == in.size());

                    // Write input to the device (non-blocking)
                    //copy_async(in.m_data, input_d, queue);
                    auto v_in = lima::const_view<const_view_t>(in);
                    bcl::copy(v_in.begin(), v_in.end(), input_d.begin(), queue);

                    // Run the kernels
                    auto force_acc = in.metadata.is_final;
                    auto [nb_peaks, nb_acc_frames] = pf(queue, input_d,                    //
                                                        preprocessed_d, cliped_d, found_d, //
                                                        peak_indices_d, peak_values_d, background_avg_d,
                                                        background_std_d, acc_corr_d, acc_peak_d, force_acc);
                    //{
                    //    std::ofstream of("/tmp/preprocessed_" + std::to_string(in.metadata.idx) + ".bin");
                    //    const char* d = reinterpret_cast<const char*>(preprocessed_dbg.data());
                    //    of.write(d, preprocessed_dbg.size() * sizeof(preprocessed_dbg[0]));
                    //}

                    LIMA_LOG(trace, proc) << nb_peaks << " peaks found";

                    auto dims = in.dimensions();
                    auto pixel_type = in.pixel_type();

                    sparse_frame sparse(dims, nb_peaks, nb_bins);

                    // Copy metadata
                    sparse.metadata = in.metadata;
                    sparse.attributes = in.attributes;

                    // Read result from the device to array (non-blocking)
                    bcl::copy_n(peak_indices_d.begin(), nb_peaks, sparse.peak_indices.begin(), queue);
                    bcl::copy_n(peak_values_d.begin(), nb_peaks, sparse.peak_values.begin(), queue);
                    bcl::copy(background_avg_d.begin(), background_avg_d.end(), sparse.background_avg.begin(), queue);
                    bcl::copy(background_std_d.begin(), background_std_d.end(), sparse.background_std.begin(), queue);

                    std::get<0>(ports).try_put(sparse);

                    if (nb_acc_frames) {
                        LIMA_LOG(trace, proc) << "Accumulation available";
                        auto acc_pixel = pixel_enum::gray32f;
                        auto acc = std::make_tuple(lima::frame(dims, acc_pixel), lima::frame(dims, acc_pixel));
                        auto& [corr, peak] = acc;

                        auto copy_acc = [&](auto& s, auto& f) {
                            // Copy metadata - TODO: include nb_acc_frames
                            f.metadata = in.metadata;
                            f.metadata.idx = acc_frame_idx;
                            f.metadata.recv_idx = acc_frame_idx;
                            f.attributes = in.attributes;
                            // Copy data
                            auto view = lima::view<lima::bpp32f_view_t>(f);
                            bcl::copy(s.begin(), s.end(), &view(0, 0), queue);
                        };
                        copy_acc(acc_corr_d, corr);
                        copy_acc(acc_peak_d, peak);
                        ++acc_frame_idx;
                        std::get<1>(ports).try_put(acc);
                    }
                }

                bcl::context context;
                mutable bcl::command_queue queue;

                std::size_t nb_pixels;
                std::size_t nb_bins;
                int acc_nb_frames_reset;
                int acc_nb_frames_xfer;

                fai::peak_finder<pixel_t> pf;

                mutable fai::vector<pixel_t> input_d;        // Input
                mutable fai::vector<int> peak_indices_d;     // Outputs Peaks
                mutable fai::vector<float> peak_values_d;    //
                mutable fai::vector<float> background_avg_d; // Background
                mutable fai::vector<float> background_std_d; //
                mutable fai::vector<float> acc_corr_d;       // Accumulation
                mutable fai::vector<float> acc_peak_d;       //
                mutable std::size_t acc_frame_idx{0};        //

                // Following outputs are usually used only for debugging
                bool debug;
                mutable fai::vector<bcl::float4_> preprocessed_d; // Preprocessed
                mutable fai::vector<bcl::float8_> cliped_d;       // Cliped (after sigma clip)
                mutable fai::vector<bcl::float4_> found_d;        // Found (after peak find)
            };

            template <typename T>
            struct peak_finder_body_variant;

            template <typename... Views>
            struct peak_finder_body_variant<boost::gil::any_image_view<Views...>>
            {
                using type = std::variant<peak_finder_body<Views>...>;
            };

            using any_peak_finder_body_t = peak_finder_body_variant<lima::view_t>::type;

            // use shared_ptr in order to avoid copy arrays in the peak_finder instance and the body
            using body_ptr_t = std::shared_ptr<any_peak_finder_body_t>;

            template <typename... Args>
            body_ptr_t make_body(pixel_enum pixel, Args&&... args)
            {
                lima::frame frm(1, 1, 1, pixel);
                return boost::variant2::visit(
                    [&](auto v) {
                        using B = peak_finder_body<decltype(v)>;
                        return std::make_shared<any_peak_finder_body_t>(std::in_place_type_t<B>(),
                                                                        std::forward<Args>(args)...);
                    },
                    lima::view(frm));
            }

            peak_finder_node(tbb::flow::graph& g, std::size_t width, std::size_t height, pixel_enum pixel,
                             fai::kernel_params const& params, std::filesystem::path const& cl_source_path,
                             bcl::context& context, bcl::command_queue& queue) :
                parent_t(g, tbb::flow::serial /*serial fo now*/,
                         [body_ptr = make_body(pixel, width, height, params, cl_source_path, context, queue)](
                             auto&& in, auto&& ports) { std::visit([&](auto& b) { b(in, ports); }, *body_ptr); })
            {
            }
        };

    } // namespace smx
} // namespace processing::pipelines
} // namespace lima
