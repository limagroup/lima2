// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <lima/core/arc.hpp>
#include <lima/core/enums.hpp>
#include <lima/core/rectangle.hpp>

#include <lima/hw/params.hpp>
#include <lima/io/h5/params.hpp>

#include <lima/processing/fai/enums.hpp>

// This file is part of the user interface of the library and should not include any private dependencies
// or other thing that expose implementation details

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        struct fifo_params
        {
            int nb_fifo_frames = 100;
        };

        struct thread_params
        {
            int nb_threads = 16;
            int numa_node = 0;
        };

        struct buffer_params
        {
            int nb_roi_statistics_buffer = 100;
            int nb_roi_profiles_buffer = 100;
            int nb_peak_counters_buffer = 100;
            int nb_frames_buffer = 100;
        };

        struct roi_statistics_params
        {
            bool enabled = false;
            std::vector<rectangle_t> rect_rois = {};
            std::vector<arc_t> arc_rois = {};
        };

        struct roi_profiles_params
        {
            bool enabled = false;
            std::vector<rectangle_t> rois = {};
            std::vector<direction_enum> directions = {};
        };

        struct gpu_params
        {
            std::size_t platform_idx = 0;
            std::size_t device_idx = 0;
        };

        struct fai_params
        {
            fai::error_model_enum error_model = fai::error_model_enum::poisson;    // FAI
            std::filesystem::path variance_path;                                   // Preprocessing
            std::filesystem::path mask_path;                                       //
            std::filesystem::path dark_path;                                       //
            std::filesystem::path dark_variance_path;                              //
            std::filesystem::path flat_path;                                       //
            std::filesystem::path solid_angle_path;                                //
            std::filesystem::path polarization_path;                               //
            std::filesystem::path absorption_path;                                 //
            float dummy = 0.0f;                                                    //
            float delta_dummy = 0.0f;                                              //
            float normalization_factor = 0.0f;                                     //
            std::filesystem::path csr_path;                                        //
            float cutoff_clip = 3.0f;                                              // Sigma clipping
            int cycle = 4;                                                         //
            float empty = 0.0f;                                                    //
            std::filesystem::path radius2d_path;                                   // Peak finding
            std::filesystem::path radius1d_path;                                   //
            float noise = 0.0f;                                                    //
            float cutoff_pick = 0.0f;                                              //
            int acc_nb_frames_reset = 0;                                           //
            int acc_nb_frames_xfer = 0;                                            //
            std::filesystem::path cl_source_path = "detectors/processing/psi/src"; // CL source file path
        };

        struct saving_params : io::h5::saving_params
        {
            bool enabled = true;
        };

        struct proc_params
        {
            fifo_params fifo;
            thread_params thread;
            buffer_params buffers;
            saving_params saving_dense;
            saving_params saving_sparse;
            saving_params saving_accumulation_corrected;
            saving_params saving_accumulation_peak;
            gpu_params gpu;
            fai_params fai;
            roi_statistics_params statistics;
            roi_profiles_params profiles;

            proc_params()
            {
                saving_dense.filename_prefix = "lima2_dense";
                saving_sparse.filename_prefix = "lima2_sparse";
                saving_accumulation_corrected.filename_prefix = "lima2_acc_corrected";
                saving_accumulation_peak.filename_prefix = "lima2_acc_peak";
            }
        };

    } // namespace smx
} // namespace processing::pipelines
} // namespace lima
