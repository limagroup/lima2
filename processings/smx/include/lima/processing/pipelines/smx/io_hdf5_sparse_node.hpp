// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <filesystem>
#include <memory>

#include <tbb/flow_graph.h>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/io/compression/bshuf_lz4.hpp>
#include <lima/io/compression/zip.hpp>

#include <lima/processing/pipelines/smx/sparse_frame.hpp>
#include <lima/processing/nodes/io_hdf5.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        struct io_hdf5_sparse_node
            : public tbb::flow::composite_node<std::tuple<sparse_frame>, std::tuple<tbb::flow::continue_msg>>
        {
            using parent_t = tbb::flow::composite_node<std::tuple<sparse_frame>, std::tuple<tbb::flow::continue_msg>>;
            using dimensions_t = typename sparse_frame::point_t;

            using sequencer_node_t = tbb::flow::sequencer_node<sparse_frame>;
            using writer_node_t = tbb::flow::function_node<sparse_frame, tbb::flow::continue_msg>;

            sequencer_node_t sequencer_node;
            writer_node_t writer_node;

            struct writer_body
            {
                using writer_t = io::multi_writer<io::h5::writer_fai_sparse>;

                writer_body(io::h5::saving_params const& params, frame_info const& frame_info, int nb_bins,
                            lima::frame radius1d, lima::frame radius2d_mask) :
                    m_writer(std::make_shared<writer_t>(params, frame_info, nb_bins, radius1d, radius2d_mask)),
                    m_nb_frames_per_chunk(params.nb_frames_per_chunk)
                {
                }

                tbb::flow::continue_msg operator()(sparse_frame const& in)
                {
                    m_writer->apply(in.metadata.recv_idx, [&](auto& writer, int frame_idx) {
                        writer.write_frame_peaks(frame_idx, in.peak_indices, in.peak_values);
                        writer.write_background_avg(frame_idx, in.background_avg);
                        writer.write_background_std(frame_idx, in.background_std);
                    });

                    return tbb::flow::continue_msg{};
                }

                std::shared_ptr<writer_t> m_writer;

                int m_nb_frames_per_chunk = 0;
            };

            io_hdf5_sparse_node(tbb::flow::graph& g, io::h5::saving_params const& params, frame_info const& frame_info,
                                int nb_bins, lima::frame radius1d, lima::frame radius2d_mask) :
                parent_t(g),
                sequencer_node(g, [](sparse_frame const& frm) -> size_t { return frm.metadata.recv_idx; }),
                writer_node(g, 1 /*serial for now*/, writer_body(params, frame_info, nb_bins, radius1d, radius2d_mask))
            {
                tbb::flow::make_edge(sequencer_node, writer_node);
                typename parent_t::input_ports_type input_tuple(sequencer_node);
                typename parent_t::output_ports_type output_tuple(writer_node);
                parent_t::set_external_ports(input_tuple, output_tuple);
            }
        };

    } // namespace smx
} // namespace processing::pipelines
} // namespace lima
