// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/describe/annotations.hpp>

#include <lima/processing/pipelines/smx/pipeline.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        BOOST_DESCRIBE_STRUCT(counters, (),
                              (nb_frames_source, nb_frames_counters, nb_frames_profiles, nb_frames_sparsified,
                               nb_frames_sparse_saved, nb_frames_dense_saved, nb_frames_acc_corrected_saved,
                               nb_frames_acc_peak_saved))

        // clang-format off
        BOOST_ANNOTATE_MEMBER(counters, nb_frames_source,
            (desc, "nb frames source"),
            (doc, "The number of frame poped from the FIFO"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_counters,
            (desc, "nb frames roi counters"),
            (doc, "The number of frame processed with ROI counters"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_profiles,
            (desc, "nb frames roi profiles"),
            (doc, "The number of frame processed with ROI profiles"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_sparsified,
            (desc, "nb frames roi counters"),
            (doc, "The number of frame sparsified with FAI"))
            
        BOOST_ANNOTATE_MEMBER(counters, nb_frames_sparse_saved,
            (desc, "nb frames sparse saved"),
            (doc, "The number of sparse frame saved to file"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_dense_saved,
            (desc, "nb frames dense saved"),
            (doc, "The number of dense frame saved to file"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_acc_corrected_saved,
            (desc, "nb frames accumulation corrected saved"),
            (doc, "The number of corrected accumulation frame saved to file"))

        BOOST_ANNOTATE_MEMBER(counters, nb_frames_acc_peak_saved,
            (desc, "nb frames accumulation peak saved"),
            (doc, "The number of peak accumulation frame saved to file"))
        // clang-format on

    } // namespace smx
} // namespace processing::pipelines

} // namespace lima
