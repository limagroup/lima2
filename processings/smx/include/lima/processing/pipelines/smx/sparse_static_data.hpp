// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <boost/gil/algorithm.hpp>

#include <lima/io/hdf5.hpp>
#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/core/frame_view.hpp>

#include <cmath>

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        namespace detail
        {
            lima::frame h5_read_file(lima::pixel_enum pixel, int dataset_dims, std::filesystem::path file_path,
                                     lima::io::h5::path const& data_path = "/data")
            {
                namespace h5 = lima::io::h5;

                LIMA_LOG(trace, proc) << "Loading file: " << file_path << " [" << data_path.c_str() << "] ...";

                // load data from H5 file
                auto file = h5::file::open(file_path);

                // Open an existing dataset
                auto dataset = h5::dataset::open(file, data_path);

                // Check the dimensions
                assert((dataset_dims > 0) && (dataset_dims <= 3));
                auto dataspace = dataset.space();
                const int ndims = dataspace.ndims();
                hsize_t dims[ndims];
                dataspace.dims(dims, nullptr);

                // Last dimension defines the nb of colums
                const int last_dim = dataset_dims - 1;
                auto cols = dims[last_dim];
                auto rows =
                    std::accumulate(dims, dims + last_dim, 1, [](hsize_t lhs, hsize_t rhs) { return lhs * rhs; });

                // Allocate the image memory
                lima::frame res(cols, rows, pixel);

                // Read the dataset
                dataset.read(res.data, h5::datatype(pixel));

                LIMA_LOG(trace, proc) << "File loaded with " << cols << "x" << rows << " pixels";

                return res;
            }
        } // namespace detail

        auto h5_read_radius1d(std::filesystem::path radius1d_file_path,
                              lima::io::h5::path const& radius1d_data_path = "/data")
        {
            return detail::h5_read_file(pixel_enum::gray32f, 1, radius1d_file_path, radius1d_data_path);
        }

        auto h5_read_radius2d_mask(std::filesystem::path radius2d_file_path, std::filesystem::path mask_file_path,
                                   lima::io::h5::path const& radius2d_data_path = "/data",
                                   lima::io::h5::path const& mask_data_path = "/data")
        {
            auto mask = detail::h5_read_file(pixel_enum::gray8, 2, mask_file_path, mask_data_path);
            auto radius2d = detail::h5_read_file(pixel_enum::gray32f, 2, radius2d_file_path, radius2d_data_path);

            auto&& mask_dims = mask.dimensions();
            auto&& out_dims = radius2d.dimensions();
            LIMA_LOG(trace, proc) << "Radius-2d loaded with " << out_dims.x << "x" << out_dims.y << " pixels";
            if (out_dims != mask_dims)
                LIMA_THROW_EXCEPTION(std::runtime_error("Dimension mismatch between mask & radius-2d datas"));

            boost::gil::transform_pixels(const_view<bpp32fc_view_t>(radius2d), const_view<bpp8c_view_t>(mask),
                                         view<bpp32f_view_t>(radius2d), [](auto p1, auto p2) { return p2 ? NAN : p1; });

            return radius2d;
        }

    } // namespace smx
} // namespace processing::pipelines
} // namespace lima
