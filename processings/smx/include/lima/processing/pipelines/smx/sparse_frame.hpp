// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <memory_resource>
#include <vector>

#include <boost/json/value.hpp>

#include <lima/core/enums.hpp>
#include <lima/core/pixel.hpp>
#include <lima/core/point.hpp>
#include <lima/core/frame_metadata.hpp>

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        struct sparse_frame
        {
            static const std::size_t num_dimensions = 2;
            using coord_t = std::ptrdiff_t;
            using point_t = point<coord_t>;

            using attributes_t = boost::json::value;
            using metadata_t = frame_metadata;

            // Construct empty
            explicit sparse_frame() = default;

            sparse_frame(const point_t& dims, std::size_t nb_peaks, std::size_t nb_bins) :
                m_dimensions(dims),
                peak_indices(nb_peaks),
                peak_values(nb_peaks),
                background_avg(nb_bins),
                background_std(nb_bins)
            {
            }

            sparse_frame(coord_t width, coord_t height, std::size_t nb_peaks, std::size_t nb_bins) :
                m_dimensions(width, height),
                peak_indices(nb_peaks),
                peak_values(nb_peaks),
                background_avg(nb_bins),
                background_std(nb_bins)
            {
            }

            const point_t& dimensions() const { return m_dimensions; }
            coord_t width() const { return m_dimensions.x; }
            coord_t height() const { return m_dimensions.y; }
            std::size_t size() const { return m_dimensions.x * m_dimensions.y; }
            std::size_t nb_peaks() const { return peak_indices.size(); }
            std::size_t nb_bins() const { return background_avg.size(); }
            pixel_enum pixel_type() const { return pixel_enum::gray32f; }

            std::size_t size_in_bytes() const
            {
                return peak_indices.size() * sizeof(int) + peak_values.size() * sizeof(float)
                       + background_avg.size() * sizeof(float) + background_std.size() * sizeof(float);
            }

            std::vector<int> peak_indices;
            std::vector<float> peak_values;
            std::vector<float> background_avg;
            std::vector<float> background_std;

            point_t m_dimensions;    //<! Frame dimensions
            metadata_t metadata;     //<! Static typed metadata
            attributes_t attributes; //<! Dynamic attributes encoded as string
        };

    } // namespace smx
} // namespace processing::pipelines
} // namespace lima
