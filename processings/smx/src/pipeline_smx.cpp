// Copyright (C) 2018 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <atomic>
#include <optional>
#include <mutex>
#include <stdexcept>
#include <future>

#include <tbb/concurrent_queue.h>

#include <fmt/chrono.h>
#include <fmt/ranges.h>
#include <fmt/describe.hpp>
#include <fmt/filesystem.hpp>

#include <boost/exception/error_info.hpp>
#include <boost/exception/errinfo_frame_idx.hpp>
#include <boost/scope/scope_fail.hpp>

#include <lima/exceptions.hpp>
#include <lima/logging.hpp>
#include <lima/io/check.hpp>
#include <lima/core/frame_info.describe.hpp>
#include <lima/hw/params.describe.hpp>

#include <lima/processing/nodes/checkpoint.hpp>
#include <lima/processing/nodes/io_hdf5.hpp>
#include <lima/processing/nodes/roi_counters.hpp>
#include <lima/processing/nodes/roi_profiles.hpp>
#include <lima/processing/nodes/circular_buffer.hpp>
#include <lima/processing/nodes/unbounded_buffer.hpp>
#include <lima/utils/math.hpp>

#include <lima/processing/pipelines/smx.hpp>
#include <lima/processing/pipelines/smx/params.describe.hpp>

#include <lima/processing/pipelines/smx/sparse_frame.hpp>
#include <lima/processing/pipelines/smx/sparse_static_data.hpp>
#include <lima/processing/pipelines/smx/peak_finder_node.hpp>
#include <lima/processing/pipelines/smx/io_hdf5_sparse_node.hpp>

#include <smx/project_version.h>

// Map Boost.GIL type to its OpenCL counter parts
BOOST_COMPUTE_TYPE_NAME(lima::bpp8_pixel_t, uchar)
BOOST_COMPUTE_TYPE_NAME(lima::bpp16_pixel_t, ushort)
BOOST_COMPUTE_TYPE_NAME(lima::bpp32_pixel_t, uint)

namespace lima
{
namespace processing::pipelines
{
    namespace smx
    {
        class pipeline::impl
        {
            using input_t = frame;

          public:
            using proc_params_t = proc_params;

            using accum_split_node = tbb::flow::split_node<std::tuple<frame, frame>>;
            using peak_counter_node =
                tbb::flow::function_node<sparse_frame, peaks_counter_result, tbb::flow::lightweight>;

            impl(frame_info_t const& frame_info, proc_params_t const& proc_params, acq_params_t const& acq_params,
                 det_info_t const& det_info) :
                m_frame_info(frame_info),
                m_proc_params(proc_params),
                m_metadata_writer(acq_params, det_info),
                m_frames_buffer(m_proc_params.buffers.nb_frames_buffer),
                m_sparse_frames_buffer(m_proc_params.buffers.nb_frames_buffer),
                m_acc_corrected_buffer(m_proc_params.buffers.nb_frames_buffer),
                m_acc_peak_buffer(m_proc_params.buffers.nb_frames_buffer),
                m_peaks_buffer(m_proc_params.buffers.nb_peak_counters_buffer),
                m_roi_statistics_buffer(m_proc_params.buffers.nb_roi_statistics_buffer),
                m_roi_profiles_buffer(m_proc_params.buffers.nb_roi_profiles_buffer)
            {
                LIMA_LOG(trace, det) << fmt::format("frame_info = {}", frame_info);
                LIMA_LOG(trace, det) << fmt::format("proc_params = {}", proc_params);

                // Set the FIFO capacity
                m_fifo.set_capacity(proc_params.fifo.nb_fifo_frames);

                // Check output paths
                io::check_base_path(proc_params.saving_dense.base_path);

                // Open OCL device
                auto platforms = bcl::system::platforms();
                if (platforms.empty())
                    LIMA_THROW_EXCEPTION(lima::runtime_error("No platform found. Check OpenCL installation!"));

                for (int p_idx = 0; auto&& p : platforms)
                    LIMA_LOG(trace, proc) << "OpenCL Plat #" << p_idx++ << ": " << p.name();

                if (m_proc_params.gpu.platform_idx >= platforms.size())
                    LIMA_THROW_EXCEPTION(lima::runtime_error("No platform found. Check OpenCL installation!"));

                bcl::platform plat = platforms[m_proc_params.gpu.platform_idx];
                if (!plat.check_version(3, 0)) {
                    LIMA_THROW_EXCEPTION(lima::runtime_error("Platform is not OpenCL 3.0."));
                }

                // Get the list of GPU devices of the platform
                auto devices = plat.devices(CL_DEVICE_TYPE_GPU);
                if (devices.empty()) {
                    LIMA_THROW_EXCEPTION(lima::runtime_error("No device found. Check OpenCL installation!"));
                }

                for (int d_idx = 0; auto&& d : devices)
                    LIMA_LOG(trace, proc) << "OpenCL Device #" << d_idx++ << ": " << d.name();

                if (m_proc_params.gpu.device_idx >= devices.size()) {
                    LIMA_THROW_EXCEPTION(lima::runtime_error("Device not found"));
                }
                m_device = devices[m_proc_params.gpu.device_idx];

                // Getting information about used queue and device
                const size_t max_compute_units = m_device.get_info<CL_DEVICE_MAX_COMPUTE_UNITS>();
                const size_t max_work_group_size = m_device.get_info<CL_DEVICE_MAX_WORK_GROUP_SIZE>();

                LIMA_LOG(trace, proc) << "Dev: " << m_device.name() << " max_compute_units: " << max_compute_units
                                      << " max_work_group_size: " << max_work_group_size;

                LIMA_LOG(trace, proc) << "Processing constructed";
            }

            ~impl()
            {
                // Abort the processing. The normal way is to send and empty frame tagged is_final.
                if (!m_is_finished)
                    abort();

                // Make sure that the processing thread joins
                if (m_finished_future.valid())
                    m_finished_future.get();

                LIMA_LOG(trace, proc) << "Processing destructed";
            }

            /// Activate the processing (start poping data from the queue)
            void activate()
            {
                // Not possible to use task_arena here since
                //  - task_arena.execute() is synchronous
                //  - task_arena.enqueue() does not handle exception
                m_finished_future = std::async(std::launch::async, [this] {
                    // Create computing resssources that have the same lifetime than the computation
                    // AKA GPU ressources

                    // Create OCL context
                    bcl::context context(m_device);

                    // Create OCL queue
                    bcl::command_queue queue(context, context.get_device());

                    auto numa_nodes = oneapi::tbb::info::numa_nodes();
                    tbb::numa_node_id numa_index = tbb::task_arena::automatic;

                    tbb::task_arena arena;
                    arena.initialize(tbb::task_arena::constraints(numa_index, m_proc_params.thread.nb_threads));

                    std::optional<std::string> error;
                    // Run a TBB flow graph in a given arena
                    arena.execute([&]() {
                        namespace bs = boost::scope;
                        bs::scope_fail end_fail(
                            [&]() { LIMA_LOG(error, proc) << "Error building SMX pipeline graph"; });

                        tbb::flow::graph graph(m_task_group_context);

                        tbb::flow::input_node<frame> src(graph, [this, stop = false](tbb::flow_control& fc) mutable {
                            frame res;

                            // If input is flagged to stop, return early
                            if (stop) {
                                fc.stop();
                                return res;
                            }

                            // Poll for available frames
                            m_fifo.pop(res);

                            // Limit the number of log traces given the order of magnitude of nb frames
                            if (!(res.metadata.idx % order_of_magnitude_base10(res.metadata.idx)))
                                LIMA_LOG(trace, proc) << "Processing thread got frame " << res.metadata.idx;

                            // If acquisition is interrupted or failed, hw_get_frame() is cancelled and returned frame might be empty
                            if (res.empty() && res.metadata.is_final) {
                                LIMA_LOG(trace, proc) << "Processing thread got final frame (acquisition cancelled)";
                                fc.stop();
                                return res;
                            }

                            // If the frame is final, flag the input to stop next run
                            if (res.metadata.is_final) {
                                LIMA_LOG(trace, proc) << "Processing thread got final frame";
                                stop = true;
                            }

                            m_nb_frames_source++;

                            return res;
                        });

                        // Frames buffer node
                        circular_buffer_node<frame> frames_buffer(graph, tbb::flow::unlimited, m_frames_buffer);

                        // Sequencer ensuring ordered injection in peak finder
                        tbb::flow::sequencer_node<frame> sequencer(
                            graph, [](frame const& frm) -> size_t { return frm.metadata.recv_idx; });

                        // Peak finder node
                        fai::kernel_params params = fai::read_kernel_params(
                            m_proc_params.fai.error_model, m_proc_params.fai.variance_path, m_proc_params.fai.mask_path,
                            m_proc_params.fai.dark_path, m_proc_params.fai.dark_variance_path,
                            m_proc_params.fai.flat_path, m_proc_params.fai.solid_angle_path,
                            m_proc_params.fai.polarization_path, m_proc_params.fai.absorption_path,
                            m_proc_params.fai.dummy, m_proc_params.fai.delta_dummy,
                            m_proc_params.fai.normalization_factor, m_proc_params.fai.csr_path,
                            m_proc_params.fai.cutoff_clip, m_proc_params.fai.cycle, m_proc_params.fai.empty,
                            m_proc_params.fai.radius2d_path, m_proc_params.fai.radius1d_path, m_proc_params.fai.noise,
                            m_proc_params.fai.cutoff_pick, m_proc_params.fai.acc_nb_frames_reset,
                            m_proc_params.fai.acc_nb_frames_xfer, context, queue);

                        LIMA_LOG(trace, proc) << "Peak finder kernel parameters parsed";

                        auto&& [dense_x, dense_y] = m_frame_info.dimensions();
                        peak_finder_node peak_finder(graph, dense_x, dense_y, m_frame_info.pixel_type(), params,
                                                     m_proc_params.fai.cl_source_path, context, queue);
                        peak_counter_node peak_counter(
                            graph, tbb::flow::serial, [](auto const& frm) -> peaks_counter_result {
                                return {frm.metadata.idx, frm.metadata.recv_idx, frm.nb_peaks()};
                            });
                        unbounded_buffer_node<peaks_counter_result> peak_counters_buffer(graph, tbb::flow::serial,
                                                                                         m_peaks_buffer);
                        checkpoint_node<sparse_frame> checkpoint_peak_finder(graph, tbb::flow::unlimited,
                                                                             m_nb_frames_sparsified);

                        // Sparse frames buffer node
                        circular_buffer_node<sparse_frame> sparse_frames_buffer(graph, tbb::flow::unlimited,
                                                                                m_sparse_frames_buffer);

                        int nb_bins = params.csr_indptr.size() - 1;

                        // Dense HDF5 node
                        std::optional<io_hdf5_node<frame>> io_hdf5_dense;
                        std::optional<checkpoint_node<tbb::flow::continue_msg>> checkpoint_hdf5_dense;
                        if (m_proc_params.saving_dense.enabled) {
                            io_hdf5_dense.emplace(graph, m_proc_params.saving_dense, m_frame_info, m_metadata_writer);
                            checkpoint_hdf5_dense.emplace(graph, tbb::flow::unlimited, m_nb_frames_dense_saved);
                        } else
                            m_nb_frames_dense_saved = -1;

                        m_radius1d = h5_read_radius1d(m_proc_params.fai.radius1d_path);
                        m_radius2d_mask =
                            h5_read_radius2d_mask(m_proc_params.fai.radius2d_path, m_proc_params.fai.mask_path);

                        // Sparse HDF5 node
                        std::optional<io_hdf5_sparse_node> io_hdf5_sparse;
                        std::optional<checkpoint_node<tbb::flow::continue_msg>> checkpoint_hdf5_sparse;
                        if (m_proc_params.saving_sparse.enabled) {
                            io_hdf5_sparse.emplace(graph, m_proc_params.saving_sparse, m_frame_info, nb_bins,
                                                   m_radius1d, m_radius2d_mask);
                            checkpoint_hdf5_sparse.emplace(graph, tbb::flow::unlimited, m_nb_frames_sparse_saved);
                        } else
                            m_nb_frames_sparse_saved = -1;

                        // Accumulation HDF5 node
                        std::optional<accum_split_node> accum_split;
                        std::optional<circular_buffer_node<frame>> acc_corrected_buffer;
                        std::optional<circular_buffer_node<frame>> acc_peak_buffer;
                        std::optional<io_hdf5_node<frame>> acc_corrected_saving;
                        std::optional<checkpoint_node<tbb::flow::continue_msg>> checkpoint_hdf5_acc_corrected;
                        std::optional<io_hdf5_node<frame>> acc_peak_saving;
                        std::optional<checkpoint_node<tbb::flow::continue_msg>> checkpoint_hdf5_acc_peak;

                        bool do_acc = (m_proc_params.fai.acc_nb_frames_xfer > 0);
                        if (do_acc) {
                            accum_split.emplace(graph);
                            //auto acc_nb_frames =
                            //    (acq_params.xfer.time_slice.count - 1) / m_proc_params.fai.acc_nb_frames_xfer + 1;
                            acc_corrected_buffer.emplace(graph, tbb::flow::unlimited, m_acc_corrected_buffer);
                            acc_peak_buffer.emplace(graph, tbb::flow::unlimited, m_acc_peak_buffer);
                            if (m_proc_params.saving_accumulation_corrected.enabled) {
                                acc_corrected_saving.emplace(
                                    graph, m_proc_params.saving_accumulation_corrected,
                                    frame_info_t{m_frame_info.dimensions(), 1, pixel_enum::gray32f}, m_metadata_writer);
                                checkpoint_hdf5_acc_corrected.emplace(graph, tbb::flow::unlimited,
                                                                      m_nb_frames_acc_corrected_saved);
                            } else
                                m_nb_frames_acc_corrected_saved = -1;
                            if (m_proc_params.saving_accumulation_peak.enabled) {
                                acc_peak_saving.emplace(graph, m_proc_params.saving_accumulation_peak,
                                                        frame_info_t{m_frame_info.dimensions(), 1, pixel_enum::gray32f},
                                                        m_metadata_writer);
                                checkpoint_hdf5_acc_peak.emplace(graph, tbb::flow::unlimited,
                                                                 m_nb_frames_acc_peak_saved);
                            } else
                                m_nb_frames_acc_peak_saved = -1;
                        }

                        // ROI counters node (optional)
                        std::optional<roi_counters_node<frame>> roi_counters;
                        std::optional<tbb::flow::sequencer_node<roi_counters_result>> roi_counters_sequencer;
                        std::optional<unbounded_buffer_node<roi_counters_result>> roi_counters_buffer;
                        std::optional<checkpoint_node<roi_counters_result>> roi_counters_checkpoint;

                        if (m_proc_params.statistics.enabled) {
                            roi_counters.emplace(graph, tbb::flow::unlimited, m_proc_params.statistics.rect_rois,
                                                 m_proc_params.statistics.arc_rois);
                            roi_counters_sequencer.emplace(
                                graph, [](roi_counters_result const& rc) -> size_t { return rc.recv_idx; });
                            roi_counters_buffer.emplace(graph, tbb::flow::serial, m_roi_statistics_buffer);
                            roi_counters_checkpoint.emplace(graph, tbb::flow::unlimited, m_nb_frames_counters);
                        }

                        // ROI profiles node
                        std::optional<roi_profiles_node<frame>> roi_profiles;
                        std::optional<tbb::flow::sequencer_node<roi_profiles_result>> roi_profiles_sequencer;
                        std::optional<unbounded_buffer_node<roi_profiles_result>> roi_profiles_buffer;
                        std::optional<checkpoint_node<roi_profiles_result>> roi_profiles_checkpoint;
                        if (m_proc_params.profiles.enabled) {
                            roi_profiles.emplace(graph, tbb::flow::unlimited, m_proc_params.profiles.rois,
                                                 m_proc_params.profiles.directions);
                            roi_profiles_sequencer.emplace(
                                graph, [](roi_profiles_result const& rc) -> size_t { return rc.recv_idx; });
                            roi_profiles_buffer.emplace(graph, tbb::flow::serial, m_roi_profiles_buffer);
                            roi_profiles_checkpoint.emplace(graph, tbb::flow::unlimited, m_nb_frames_profiles);
                        }

                        // Graph topology
                        tbb::flow::make_edge(src, frames_buffer);
                        tbb::flow::make_edge(src, sequencer);
                        tbb::flow::make_edge(sequencer, peak_finder);
                        auto&& [sparse, acc] = peak_finder.output_ports();
                        tbb::flow::make_edge(sparse, sparse_frames_buffer);

                        if (m_proc_params.saving_sparse.enabled) {
                            tbb::flow::make_edge(sparse, *io_hdf5_sparse);
                            tbb::flow::make_edge(*io_hdf5_sparse, *checkpoint_hdf5_sparse);
                        }

                        tbb::flow::make_edge(sparse, peak_counter);
                        tbb::flow::make_edge(peak_counter, peak_counters_buffer);
                        tbb::flow::make_edge(sparse, checkpoint_peak_finder);

                        if (do_acc) {
                            tbb::flow::make_edge(acc, *accum_split);
                            auto&& [corr, peak] = accum_split->output_ports();

                            tbb::flow::make_edge(corr, *acc_corrected_buffer);
                            tbb::flow::make_edge(peak, *acc_peak_buffer);
                            if (m_proc_params.saving_accumulation_corrected.enabled) {
                                tbb::flow::make_edge(corr, *acc_corrected_saving);
                                tbb::flow::make_edge(*acc_corrected_saving, *checkpoint_hdf5_acc_corrected);
                            }
                            if (m_proc_params.saving_accumulation_peak.enabled) {
                                tbb::flow::make_edge(peak, *acc_peak_saving);
                                tbb::flow::make_edge(*acc_peak_saving, *checkpoint_hdf5_acc_peak);
                            }
                        }

                        if (m_proc_params.saving_dense.enabled) {
                            tbb::flow::make_edge(src, *io_hdf5_dense);
                            tbb::flow::make_edge(*io_hdf5_dense, *checkpoint_hdf5_dense);
                        }

                        if (m_proc_params.statistics.enabled) {
                            tbb::flow::make_edge(src, *roi_counters);
                            tbb::flow::make_edge(*roi_counters, *roi_counters_sequencer);
                            tbb::flow::make_edge(*roi_counters_sequencer, *roi_counters_buffer);
                            tbb::flow::make_edge(*roi_counters_buffer, *roi_counters_checkpoint);
                        }

                        if (m_proc_params.profiles.enabled) {
                            tbb::flow::make_edge(src, *roi_profiles);
                            tbb::flow::make_edge(*roi_profiles, *roi_profiles_sequencer);
                            tbb::flow::make_edge(*roi_profiles_sequencer, *roi_profiles_buffer);
                            tbb::flow::make_edge(*roi_profiles_buffer, *roi_profiles_checkpoint);
                        }

                        src.activate();

                        try {
                            graph.wait_for_all();
                            LIMA_LOG(trace, proc) << "Processing finished";
                        } catch (std::exception const& e) {
                            LIMA_LOG(error, proc) << "Processing failed " << e.what();
                            error = e.what();
                        } catch (...) {
                            LIMA_LOG(error, proc) << "Processing failed";
                            error = "Unknown error";
                        }
                    });

                    // Set the is_finished flag and call the callback if registered
                    m_is_finished = true;
                    if (m_on_finished)
                        m_on_finished(error);
                });

                LIMA_LOG(trace, proc) << "Processing activated";
            }

            /// Returns true when the processing has finished
            bool is_finished() const { return m_is_finished; }

            /// Register on_finished callback
            void register_on_finished(finished_callback_t on_finished) { m_on_finished = on_finished; }

            /// Abort the pipeline
            void abort() { m_task_group_context.cancel_group_execution(); }

            /// Push the given frame to the FIFO of frames to process
            void process(input_t const& frm)
            {
                if (frm.metadata.is_final)
                    m_fifo.push(frm); // Wait push

                else if (!m_fifo.try_push(frm)) {
                    LIMA_LOG(error, det) << "FIFO full: frame #" << frm.metadata.idx << " dropped";
                    LIMA_THROW_EXCEPTION(lima::runtime_error("Pipeline FIFO is full")
                                         << boost::errinfo_frame_idx(frm.metadata.idx));
                }
            }

            /// Returns the radius1d
            frame radius1d() const { return m_radius1d; }

            /// Returns the radius2d combined with the mask
            frame radius2d_mask() const { return m_radius2d_mask; }

            /// Returns the progress counters
            counters progress_counters() const
            {
                return {(int) m_nb_frames_source,
                        (int) m_nb_frames_counters,
                        (int) m_nb_frames_profiles,
                        (int) m_nb_frames_sparsified,
                        (int) m_nb_frames_sparse_saved,
                        (int) m_nb_frames_dense_saved,
                        (int) m_nb_frames_acc_corrected_saved,
                        (int) m_nb_frames_acc_peak_saved};
            }

            /// Pop the available ROI statistics
            std::vector<roi_counters_result> pop_roi_statistics()
            {
                const std::lock_guard<std::mutex> lock(m_roi_statistics_buffer.mutex);

                std::vector<roi_counters_result> res;
                std::swap(m_roi_statistics_buffer.buffer, res);

                LIMA_LOG(trace, proc) << "pop_roi_statistics nb_frames=" << res.size();

                return res;
            }

            /// Returns the size of the ROI statistics
            std::size_t nb_roi_statistics() const { return m_roi_statistics_buffer.size(); }

            /// Pop the available ROI profiles
            std::vector<roi_profiles_result> pop_roi_profiles()
            {
                const std::lock_guard<std::mutex> lock(m_roi_profiles_buffer.mutex);

                std::vector<roi_profiles_result> res;
                std::swap(m_roi_profiles_buffer.buffer, res);

                LIMA_LOG(trace, proc) << "pop_roi_profiles nb_frames=" << res.size();

                return res;
            }

            /// Returns the size of the ROI profiles buffer
            std::size_t nb_roi_profiles() const { return m_roi_profiles_buffer.size(); }

            /// Pop the available peak counters
            std::vector<peaks_counter_result> pop_peak_counters()
            {
                const std::lock_guard<std::mutex> lock(m_peaks_buffer.mutex);

                std::vector<peaks_counter_result> res;
                std::swap(m_peaks_buffer.buffer, res);

                LIMA_LOG(trace, proc) << "pop_peak_counters nb_frames=" << res.size();

                return res;
            }

            /// Returns the size of the ROI statistics
            std::size_t nb_peak_counters() const { return m_peaks_buffer.size(); }

            /// Returns frame for the given index
            std::optional<frame> get_frame(std::size_t frame_idx = -1) const
            {
                if (frame_idx == -1)
                    return m_frames_buffer.back();
                else
                    return m_frames_buffer.find([frame_idx](auto frm) { return frame_idx == frm.metadata.idx; });
            }

            /// Returns sparse frame for the given index
            std::optional<sparse_frame> get_sparse_frame(std::size_t frame_idx = -1) const
            {
                if (frame_idx == -1)
                    return m_sparse_frames_buffer.back();
                else
                    return m_sparse_frames_buffer.find([frame_idx](auto frm) { return frame_idx == frm.metadata.idx; });
            }

            /// Returns the accumulated frame after correction
            std::optional<frame> get_acc_corrected(std::size_t frame_idx = -1) const
            {
                if (frame_idx == -1)
                    return m_acc_corrected_buffer.back();
                else
                    return m_acc_corrected_buffer.find([frame_idx](auto frm) { return frame_idx == frm.metadata.idx; });
            }

            /// Returns the accumulated frame after peak finding
            std::optional<frame> get_acc_peaks(std::size_t frame_idx = -1) const
            {
                if (frame_idx == -1)
                    return m_acc_peak_buffer.back();
                else
                    return m_acc_peak_buffer.find([frame_idx](auto frm) { return frame_idx == frm.metadata.idx; });
            }

            /// Returns the frame info at various stage of the pipeline
            frame_info_t input_frame_info() const { return m_frame_info; }
            frame_info_t processed_frame_info() const
            {
                frame_info_t proc(m_frame_info);
                proc.m_pixel_type = pixel_enum::gray32f;
                return proc;
            }

            /// Returns the current state of the pipeline
            state_enum state() const { return m_state; }

            /// Returns the version of the camera plugin
            std::string version() const { return SMX_VERSION; }

          private:
            state_enum m_state = state_enum::idle;

            frame_info_t m_frame_info;
            proc_params_t m_proc_params;

            io::h5::nx_metadata_writer m_metadata_writer;

            // OpenCL device
            bcl::device m_device;

            // TBB Flow Graph
            tbb::task_group_context m_task_group_context;

            // Queue of frame (runtime sized)
            tbb::concurrent_bounded_queue<input_t> m_fifo;

            // Circular buffer
            ts_circular_buffer<frame> m_frames_buffer;
            ts_circular_buffer<sparse_frame> m_sparse_frames_buffer;
            ts_circular_buffer<frame> m_acc_corrected_buffer;
            ts_circular_buffer<frame> m_acc_peak_buffer;

            // Unbounded buffer
            ts_vector<roi_counters_result> m_roi_statistics_buffer;
            ts_vector<roi_profiles_result> m_roi_profiles_buffer;
            ts_vector<peaks_counter_result> m_peaks_buffer;

            // Static data
            frame m_radius1d;
            frame m_radius2d_mask;

            // Counters
            std::atomic_int m_nb_frames_source = 0;
            std::atomic_int m_nb_frames_counters = 0;
            std::atomic_int m_nb_frames_profiles = 0;
            std::atomic_int m_nb_frames_sparsified = 0;
            std::atomic_int m_nb_frames_sparse_saved = 0;
            std::atomic_int m_nb_frames_dense_saved = 0;
            std::atomic_int m_nb_frames_acc_corrected_saved = 0;
            std::atomic_int m_nb_frames_acc_peak_saved = 0;

            // Finished
            std::atomic_bool m_is_finished{false};
            finished_callback_t m_on_finished;
            std::future<void> m_finished_future; // last member
        };

        // Pimpl boilerplate
        pipeline::pipeline(frame_info_t const& frame_info, proc_params_t const& proc_params,
                           acq_params_t const& acq_params, det_info_t const& det_info) :
            m_pimpl{std::make_unique<impl>(frame_info, proc_params, acq_params, det_info)}
        {
        }

        pipeline::pipeline(pipeline&&) = default;

        pipeline::~pipeline() = default;

        pipeline& pipeline::operator=(pipeline&&) = default;

        std::string pipeline::version() const { return m_pimpl->version(); }

        void pipeline::activate() { m_pimpl->activate(); }

        bool pipeline::is_finished() const { return m_pimpl->is_finished(); }

        void pipeline::register_on_finished(finished_callback_t on_finished)
        {
            m_pimpl->register_on_finished(on_finished);
        }

        void pipeline::abort() { m_pimpl->abort(); }

        void pipeline::process(input_t const& frm) { m_pimpl->process(frm); }

        frame pipeline::radius1d() const { return m_pimpl->radius1d(); }

        frame pipeline::radius2d_mask() const { return m_pimpl->radius2d_mask(); }

        counters pipeline::progress_counters() const { return m_pimpl->progress_counters(); }

        std::vector<roi_counters_result> pipeline::pop_roi_statistics() { return m_pimpl->pop_roi_statistics(); }

        std::size_t pipeline::nb_roi_statistics() const { return m_pimpl->nb_roi_statistics(); }

        std::vector<roi_profiles_result> pipeline::pop_roi_profiles() { return m_pimpl->pop_roi_profiles(); }

        std::size_t pipeline::nb_roi_profiles() const { return m_pimpl->nb_roi_profiles(); }

        std::vector<peaks_counter_result> pipeline::pop_peak_counters() { return m_pimpl->pop_peak_counters(); }

        std::size_t pipeline::nb_peak_counters() const { return m_pimpl->nb_peak_counters(); }

        std::optional<frame> pipeline::get_frame(std::size_t frame_idx) const { return m_pimpl->get_frame(frame_idx); }

        std::optional<sparse_frame> pipeline::get_sparse_frame(std::size_t frame_idx) const
        {
            return m_pimpl->get_sparse_frame(frame_idx);
        }

        std::optional<frame> pipeline::get_acc_corrected(std::size_t frame_idx) const
        {
            return m_pimpl->get_acc_corrected(frame_idx);
        }

        std::optional<frame> pipeline::get_acc_peaks(std::size_t frame_idx) const
        {
            return m_pimpl->get_acc_peaks(frame_idx);
        }

        pipeline::frame_info_t pipeline::input_frame_info() const { return m_pimpl->input_frame_info(); }

        pipeline::frame_info_t pipeline::processed_frame_info() const { return m_pimpl->processed_frame_info(); }

    } // namespace smx
} // namespace processing::pipelines
} // namespace lima
