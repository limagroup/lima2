import numpy as np
from lima import Frame, FrameInfo, Pixel, FileExistsPolicy
from smx import Pipeline, Params

params = Params()
params.saving_dense.file_exists_policy = FileExistsPolicy.overwrite
params.saving_dense.nb_frames_per_file = 10
params.saving_dense.enabled = True

params.saving_sparse.filename_prefix = "output_sparse"
params.saving_sparse.file_exists_policy = FileExistsPolicy.overwrite
params.saving_sparse.nb_frames_per_file = 10
params.saving_sparse.enabled = True

frame_info = FrameInfo()
frame_info.dimensions.x = 2048
frame_info.dimensions.y = 1024

smx = Pipeline(frame_info, params)

def on_finished(error):
    if error:
        print(f"ERROR! {error}")
    else:
        print(f"FINISHED!")
        

smx.register_on_finished(on_finished)

smx.activate()

nb_frames = 20

for i in range(0, nb_frames):
    frm = Frame(2048, 1024, Pixel.gray16, 1)
    data = np.array(frm, copy=False)
    #np.ndarray.fill(data, i, dtype=np.uint16)
    data.fill(i)

    frm.metadata.idx = i
    frm.metadata.recv_idx = i
    frm.metadata.is_final = False if i < (nb_frames - 1) else True

    smx.process(frm)

    print(f"{smx.progress_counters}")
