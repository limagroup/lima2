// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <chrono>
#include <iostream>
#include <variant>
#include <vector>

#include <boost/describe.hpp>
#include <boost/describe/annotations.hpp>

#include <boost/json/schema_from.hpp>

// Type has to implement operator<< to be printable
#include <boost/json/serialize.hpp>

BOOST_AUTO_TEST_CASE(test_schema_from_integer)
{
    auto res = boost::json::schema_from<int>("test_schema_from_integer");

    boost::json::value expected = {{"type", "integer"},
                                   {"default", 0},
                                   {"$schema", "http://json-schema.org/draft-06/schema"},
                                   {"$id", "https://esrf.fr/lima2.schema.json"},
                                   {"title", "test_schema_from_integer"}};

    BOOST_CHECK_EQUAL(res, expected);
}

BOOST_AUTO_TEST_CASE(test_schema_from_variant)
{
    auto res = boost::json::schema_from<std::variant<int, float>>("test_schema_from_variant");

    boost::json::array types = {{{"type", "integer"}, {"default", 0}}, {{"type", "number"}, {"default", 0.}}};
    boost::json::value expected = {{"oneOf", std::move(types)},
                                   {"$schema", "http://json-schema.org/draft-06/schema"},
                                   {"$id", "https://esrf.fr/lima2.schema.json"},
                                   {"title", "test_schema_from_variant"}};

    BOOST_CHECK_EQUAL(res, expected);
}