// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/io.hpp>

using dimensions_t = lima::frame::point_t;

BOOST_AUTO_TEST_CASE(test_frame_default)
{
    {
        lima::frame frm;
        BOOST_CHECK(frm.empty());
    }
}

BOOST_AUTO_TEST_CASE(test_frame_allocated)
{
    dimensions_t const dimensions{256, 128};

    {
        lima::frame frm(dimensions, lima::pixel_enum::gray8);
        BOOST_CHECK_EQUAL(frm.dimensions(), dimensions);
    }

    {
        lima::frame frm(dimensions.x, dimensions.y, lima::pixel_enum::gray32f);
        BOOST_CHECK_EQUAL(frm.dimensions(), dimensions);
    }
}

BOOST_AUTO_TEST_CASE(test_frame_mapped)
{
    std::byte memory[32 * 32];
    dimensions_t const dimensions{32, 32};
    ptrdiff_t const rowsize = 32 * sizeof(std::uint8_t);

    {
        lima::frame frm(dimensions, lima::pixel_enum::gray8s, memory, rowsize,
                        [](auto ptr) { std::cout << "release buffer" << std::endl; });
        BOOST_CHECK_EQUAL(frm.dimensions(), dimensions);
    }
    {
        lima::frame frm(dimensions.x, dimensions.y, lima::pixel_enum::gray8s, memory, rowsize,
                        [](auto ptr) { std::cout << "release buffer" << std::endl; });
        BOOST_CHECK_EQUAL(frm.dimensions(), dimensions);
    }
}

BOOST_AUTO_TEST_CASE(test_frame_recreate)
{
    dimensions_t const dimensions{32, 32};

    {
        lima::frame frm;
        frm.recreate(dimensions, lima::pixel_enum::gray8s);
        BOOST_CHECK_EQUAL(frm.dimensions(), dimensions);
    }
    {
        lima::frame frm;
        frm.recreate(dimensions.x, dimensions.y, lima::pixel_enum::gray16s);
        BOOST_CHECK_EQUAL(frm.dimensions(), dimensions);
    }
}

int nb_close;

BOOST_AUTO_TEST_CASE(test_frame_ref_count)
{
    using frame_t = lima::frame;

    nb_close = 0;
    {
        lima::frame f(1, 1, lima::pixel_enum::gray8s);
        {
            lima::frame copy = f;
        }
        BOOST_CHECK_EQUAL(nb_close, 0);
    }
    BOOST_CHECK_EQUAL(nb_close, 0);

    nb_close = 0;
    {
        std::byte* memory = (std::byte*) 0x12345678;
        dimensions_t const dimensions{8, 8};
        ptrdiff_t const rowsize = 8 * sizeof(std::uint8_t);

        lima::frame f(dimensions, lima::pixel_enum::gray8s, memory, rowsize, [](auto ptr) {
            BOOST_CHECK_EQUAL(ptr, (std::byte*) 0x12345678);
            nb_close++;
        });
        {
            lima::frame copy = f;
        }
        BOOST_CHECK_EQUAL(nb_close, 0);
    }
    BOOST_CHECK_EQUAL(nb_close, 1);
}