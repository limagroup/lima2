// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <string>

#include <boost/test/unit_test.hpp>
#include <boost/exception/info.hpp>
#include <boost/exception/errinfo_file_name.hpp>
#include <boost/exception/diagnostic_information.hpp>

#include <lima/exceptions.hpp>

BOOST_AUTO_TEST_CASE(test_exceptions_boost)
{
    auto oupsy = []() { LIMA_THROW_EXCEPTION(lima::runtime_error("Oupsy")); };

    BOOST_CHECK_THROW(oupsy(), lima::runtime_error);

    auto oupsy_errinfo = []() {
        LIMA_THROW_EXCEPTION(lima::io_error("Oupsy") << boost::errinfo_file_name("readme.txt"));
    };

    BOOST_CHECK_THROW(oupsy_errinfo(), lima::io_error);

    std::string expected = "Throw in function test_exceptions_boost::test_method()::<lambda()>\n"
                           "Dynamic exception type: boost::wrapexcept<lima::io_error>\n"
                           "std::exception::what: Oupsy\n"
                           "[boost::errinfo_file_name_*] = readme.txt\n";

    try {
        oupsy_errinfo();
    } catch (boost::exception& e) {
        auto diag = boost::diagnostic_information(e);
        BOOST_CHECK(!diag.empty());

        auto n = diag.find(":");
        BOOST_CHECK(n != std::string::npos);
        BOOST_CHECK_EQUAL(diag.substr(n + 2), expected);
    }

    try {
        oupsy_errinfo();
    } catch (...) {
        auto diag = boost::current_exception_diagnostic_information();
        BOOST_CHECK(!diag.empty());

        auto n = diag.find(":");
        BOOST_CHECK(n != std::string::npos);
        BOOST_CHECK_EQUAL(diag.substr(n + 2), expected);
    }
}

BOOST_AUTO_TEST_CASE(test_exceptions_std)
{
    auto oupsy = []() { LIMA_THROW_EXCEPTION(std::runtime_error("Oupsy")); };

    BOOST_CHECK_THROW(oupsy(), std::runtime_error);

    std::string expected = "Throw in function test_exceptions_std::test_method()::<lambda()>\n"
                           "Dynamic exception type: boost::wrapexcept<std::runtime_error>\n"
                           "std::exception::what: Oupsy\n";

    try {
        oupsy();
    } catch (std::exception& e) {
        auto diag = boost::diagnostic_information(e);
        BOOST_CHECK(!diag.empty());

        auto n = diag.find(":");
        BOOST_CHECK(n != std::string::npos);
        BOOST_CHECK_EQUAL(diag.substr(n + 2), expected);
    }

    try {
        oupsy();
    } catch (...) {
        auto diag = boost::current_exception_diagnostic_information();
        BOOST_CHECK(!diag.empty());

        auto n = diag.find(":");
        BOOST_CHECK(n != std::string::npos);
        BOOST_CHECK_EQUAL(diag.substr(n + 2), expected);
    }
}
