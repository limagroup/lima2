// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <vector>

#include <boost/test/unit_test.hpp>

#include <lima/core/memory/track_resource.hpp>

BOOST_AUTO_TEST_CASE(test_track_resource)
{
    lima::track_resource res;
    BOOST_CHECK_EQUAL(res.nb_bytes, 0);

    std::pmr::vector<int> v1(&res);
    std::size_t initial = res.nb_bytes;

    v1.resize(10);
    BOOST_CHECK_EQUAL(res.nb_bytes, initial + 10 * sizeof(int));

    v1.clear();
    // Leaves the capacity() of the vector unchanged
    BOOST_CHECK_EQUAL(res.nb_bytes, initial + 10 * sizeof(int));

    std::pmr::vector<double> v2(12, &res);
    BOOST_CHECK_EQUAL(res.nb_bytes, initial * 2 + 10 * sizeof(int) + 12 * sizeof(double));
}
