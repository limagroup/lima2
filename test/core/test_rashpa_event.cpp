// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cassert>

#include <chrono>
#include <future>
#include <iostream>
#include <optional>
#include <string_view>
#include <thread>
#include <variant>

#include <boost/mpi.hpp>
#include <boost/serialization/std_variant.hpp>

namespace mpi = boost::mpi;

#define LOG_CTL std::cout << "[CTL] "
#define LOG_RCV std::cout << "[RCV] "

template <class T>
constexpr std::string_view type_name()
{
    std::string_view p = __PRETTY_FUNCTION__;
#if __cplusplus < 201402
    return std::string_view(p.data() + 36, p.size() - 36 - 1);
#else
    return std::string_view(p.data() + 49, p.find(';', 49) - 49);
#endif
}

struct consensus
{
    bool operator()(bool lhs, bool rhs) const { return lhs && rhs; }
};

namespace boost
{
namespace mpi
{
    template <>
    struct is_commutative<consensus, bool> : mpl::true_
    {
    };

} // namespace mpi
} // namespace boost

namespace rashpa
{
struct event
{
    int data_channel;
    int frame_number;

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& data_channel;
        ar& frame_number;
    }
};

inline std::optional<event> poll_event()
{
    event evt;

    evt.data_channel = 0;
    evt.frame_number = 0;

    return evt;
}
} // namespace rashpa

namespace cmd
{
struct prepare
{
    int nb_frames;

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& nb_frames;
    }
};
struct start
{
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
    }
};
struct stop
{
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
    }
};
struct quit
{
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
    }
};

} //namespace cmd

using any_command_t = std::variant<cmd::prepare, cmd::start, cmd::stop, cmd::quit>;

template <typename>
inline constexpr bool always_false_v = false;

struct control
{
    control(mpi::communicator& world) : m_world(world)
    {
        std::cout << "I am process control " << m_world.rank() << " of " << m_world.size() << "." << std::endl;

        if (std::getenv("LIMA2_MPI_TEST_ATTACH_GDB")) {
            std::cout << std::unitbuf; // enable automatic flushing
            std::cout << "Attach debugger then press <ENTER> key to continue..." << std::endl;
            std::cin.get();

            m_world.barrier();
        }
    }

    ~control()
    {
        // Send the quit command
        quit();
    }

    bool prepare(int nb_frames) const { return send_command(cmd::prepare{nb_frames}); }

    bool start() const { return send_command(cmd::start{}); }

    bool stop() const { return send_command(cmd::stop{}); }

    bool quit() const { return send_command(cmd::quit{}); }

    bool send_command(any_command_t&& cmd) const
    {
        assert(m_world.rank() == 0);

        auto str = std::visit([](auto&& c) { return type_name<std::decay_t<decltype(c)>>(); }, cmd);
        LOG_CTL << "Send command " << str << std::endl;

        // Send the prepare command
        mpi::broadcast(m_world, cmd, 0);

        // Get the result from main receiver
        bool res;
        m_world.recv(1, mpi::any_tag, res);

        return res;
    }

    mpi::communicator m_world;
};

struct receiver
{
    enum class state
    {
        idle,
        prepared,
        running,
        stoping,
        exiting
    };

    receiver(mpi::communicator& world, mpi::communicator& receivers, int master_rank) :
        m_world(world), m_receivers(receivers), m_master_rank(master_rank), m_data_channel(m_receivers.rank())
    {
        std::cout << "I am process receiver " << m_world.rank() << " of " << m_world.size() << "." << std::endl;

        if (std::getenv("LIMA2_MPI_TEST_ATTACH_GDB")) {
            m_world.barrier();
        }
    }

    ~receiver()
    {
        m_event_loop.get();
        LOG_RCV << "receiver bye bye" << std::endl;
    }

    void listen()
    {
        bool done = false;
        while (done == false) {
            // Broadcasted command from control
            any_command_t cmd;
            mpi::broadcast(m_world, cmd, 0);

            bool res = std::visit(
                [this, &done](auto&& cmd) {
                    using T = std::decay_t<decltype(cmd)>;

                    bool res = false;
                    if constexpr (std::is_same_v<T, cmd::prepare>) {
                        LOG_RCV << "Command prepare" << std::endl;
                        res = prepare(cmd.nb_frames);
                    } else if constexpr (std::is_same_v<T, cmd::start>) {
                        LOG_RCV << "Command start" << std::endl;
                        res = start();
                    } else if constexpr (std::is_same_v<T, cmd::stop>) {
                        LOG_RCV << "Command stop" << std::endl;
                        res = stop();
                    } else if constexpr (std::is_same_v<T, cmd::quit>) {
                        LOG_RCV << "Command quit" << std::endl;
                        done = true;
                    } else
                        static_assert(always_false_v<T>, "non-exhaustive visitor!");

                    return res;
                },
                cmd);

            LOG_RCV << "Reducing result" << std::endl;

            // Compute the result (and sync receivers)
            mpi::all_reduce(m_receivers, res, consensus());

            if (m_receivers.rank() == 0) {
                LOG_RCV << "Returning result" << std::endl;

                //Returns the result to controller
                m_world.send(m_master_rank, 0, res);
            }
        }
    }

    bool prepare(int nb_frames)
    {
        using namespace std::chrono_literals;

        LOG_RCV << "Entering prepare" << std::endl;

        std::this_thread::sleep_for(1s);

        m_nb_frames = nb_frames;

        LOG_RCV << "Leaving prepare" << std::endl;

        return true;
    }

    bool start()
    {
        using namespace std::chrono_literals;

        LOG_RCV << "Entering start" << std::endl;

        // If receiver leader
        if (is_leader()) {
            //start thread for polling events
            m_event_loop = std::async(std::launch::async, [this, nb_frames = m_nb_frames]() mutable {
                while ((nb_frames > 0) && (m_end_event_loop == false)) {
                    auto evt = rashpa::poll_event();

                    if (evt) {
                        LOG_RCV << "Poll event " << nb_frames << " " << evt->data_channel << " " << evt->frame_number
                                << std::endl;

                        if (evt->data_channel == m_data_channel)
                            handle_event(*evt);
                        else
                            mpi::broadcast(m_receivers, *evt, 0);

                        nb_frames--;
                    }
                }
                LOG_RCV << "Exiting poll event loop" << std::endl;
            });
        } else {
            rashpa::event evt;
            mpi::broadcast(m_receivers, evt, 0);

            if (evt.data_channel != m_data_channel)
                handle_event(evt);
        }

        LOG_RCV << "Leaving start" << std::endl;

        return true;
    }

    bool stop()
    {
        m_end_event_loop = true;
        m_event_loop.get();

        return true;
    }

    bool is_leader() const { return m_receivers.rank() == 0; }

    void handle_event(rashpa::event const& evt) { std::cout << "Got frame " << evt.frame_number << std::endl; }

    //state m_state;
    mpi::communicator m_world;
    mpi::communicator m_receivers;
    int m_master_rank; //master rank in the world comm

    std::atomic_bool m_end_event_loop = false;
    std::future<void> m_event_loop;

    int m_data_channel; //receiver data channel
    int m_nb_frames;
};

enum class comm_color : int
{
    controler,
    receiver
};

int main(int argc, char* argv[])
{
    mpi::environment env(/*argc, argv, */ boost::mpi::threading::level::multiple);
    mpi::communicator world;

    comm_color color = comm_color::controler;
    if (world.rank() > 0)
        color = comm_color::receiver;

    // group comm for controls and receivers
    mpi::communicator sub = world.split((int) color);

    if (world.rank() == 0) {
        control ctrl{world};
        ctrl.prepare(10);
        ctrl.start();
    } else {
        receiver rcv{world, sub, 0};
        rcv.listen();
    }

    world.barrier();

    return 0;
}
