// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <array>
#include <iostream>
#include <future>
#include <memory>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <boost/circular_buffer.hpp>
#include <boost/gil.hpp>
#include <boost/lockfree/stack.hpp>
#include <boost/lockfree/spsc_queue.hpp>

// Boost customization / extension
#include <boost/pool/static_pool.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_typedefs.hpp>
#include <lima/hw/frame_info.hpp>

namespace lima
{

// The SDK manages the buffers (espia, rashpa)
// Frames are filled in sequence (synchronously or asynchronously)
// The SDK has no mechanism to check for buffer overrun (implemented in the plugin)
struct espia
{
    // Mock of an espia-like SDK
    struct sdk
    {
        sdk(std::size_t nb_buffers) : m_nb_buffers(nb_buffers)
        {
            std::size_t buffer_size = m_nb_buffers * total_size_in_bytes();
            m_hw_buffers.resize(buffer_size);
        }

        hw::frame_info frame_info() { return {dimensions, pixel_enum::gray8}; }

        // Returns true if a given frame number is available
        bool is_available(int frame_id) { return frame_id > m_last_frame_id - (int) m_nb_buffers; }

        // Returns a pointer to the upper left pixel of the next frame
        bpp8_pixel_t* get_frame(int frame_id)
        {
            auto pos = m_head;

            m_head = (m_head + 1) % m_nb_buffers;
            bpp8_pixel_t* pixel_ptr = &m_hw_buffers[pos * total_size_in_bytes()];

            // acquisition
            std::fill_n(pixel_ptr, total_size_in_bytes(), bpp8_pixel_t{1});

            m_last_frame_id = frame_id;

            return pixel_ptr;
        }

        // Returns the index of the buffer from its address
        std::size_t idx_from_ptr(bpp8_pixel_t* pixel_ptr)
        {
            return (pixel_ptr - m_hw_buffers.data()) / total_size_in_bytes();
        }

        static inline std::ptrdiff_t width = 256;
        static inline std::ptrdiff_t height = 256;
        static inline boost::gil::point_t dimensions = {width, height};

        std::size_t total_size_in_bytes() { return frame_info().total_size_in_bytes(); }

        // Hardware buffers allocated by the SDK (here one continguous array of pixels but could be anything)
        std::vector<bpp8_pixel_t> m_hw_buffers;
        std::size_t m_nb_buffers;
        int m_head = 0;
        int m_last_frame_id = 0;
    };

    espia() : hw(10) {}

    auto get_image()
    {
        auto pixel_ptr = hw.get_frame(m_frame_id);
        auto buffer_idx = hw.idx_from_ptr(pixel_ptr);

        // check for buffer overrun
        //if (m_in_use[buffer_idx])
        //	throw std::runtime_error("ERROR: Buffer overrun");
        //else
        //	m_in_use[buffer_idx] = true;

        frame res(
            sdk::dimensions, pixel_enum::gray8, pixel_ptr, sdk::dimensions.x,
            [this, id = m_frame_id, idx = buffer_idx]() {
                std::cout << "Release " << id << std::endl;
                if (!hw.is_available(id))
                    throw std::runtime_error("ERROR: Buffer overrun");
            },
            m_frame_id);

        // Increment frame id
        m_frame_id++;

        return res;
    }

    sdk hw;
    int m_frame_id = 0;
};

// The SDK manages a very limited number of buffers (pco)
// Frames are filled in sequence (synchronously or asynchronously)
// The SDK has a mechanism to check for buffer overrun (need to call release on buffer)
struct pco
{
    // Mock of an PCO like SDK
    struct sdk
    {
        hw::frame_info frame_info() { return {dimensions, pixel_enum::gray8}; }

        // Returns a pair (buffer_idx, pointer to the upper left pixel of the next image)
        auto wait_for_image()
        {
            auto pos = head;

            head = (head + 1) % nb_hw_buffers;
            bpp8_pixel_t* pixel_ptr = &m_hw_buffers[pos * dimensions.x * dimensions.y];

            // check for buffer overrun
            if (m_in_use[pos])
                throw std::runtime_error("ERROR: Buffer overrun");
            else
                m_in_use[pos] = true;

            // acquisition
            std::fill_n(pixel_ptr, total_size_in_bytes(), bpp8_pixel_t{1});

            return std::make_pair(pos, pixel_ptr);
        }

        void release(bpp8_pixel_t* pixel_ptr) { m_in_use[idx_from_ptr(pixel_ptr)] = false; }

        // Hardware buffers allocated by the SDK (here continugous array of pixels)
        static constexpr std::size_t nb_hw_buffers = 100;
        static constexpr std::ptrdiff_t width = 256;
        static constexpr std::ptrdiff_t height = 256;
        static constexpr std::size_t buffer_size = nb_hw_buffers * width * height;
        static inline boost::gil::point_t dimensions = {width, height};

        std::size_t total_size_in_bytes() { return frame_info().total_size_in_bytes(); }

        std::size_t idx_from_ptr(bpp8_pixel_t* pixel_ptr)
        {
            return (pixel_ptr - m_hw_buffers.data()) / total_size_in_bytes();
        }

        std::array<bpp8_pixel_t, buffer_size> m_hw_buffers;
        std::array<bool, buffer_size> m_in_use;

        int head = 0;
    };

    auto get_image()
    {
        m_frame_id++;

        auto [buffer_idx, pixel_ptr] = hw.wait_for_image();

        return frame(
            sdk::dimensions, pixel_enum::gray8, pixel_ptr, sdk::dimensions.x,
            [this, id = m_frame_id, ptr = pixel_ptr]() {
                std::cout << "Release " << id << std::endl;
                hw.release(ptr);
            },
            m_frame_id);
    }

    sdk hw;

    int m_frame_id = 0;
};

// Lima manages the buffers, the SDK provides an allocator
// Buffers are provided by a pool and filled in random order
struct psi
{
    // Mock of an PSI like SDK
    struct sdk
    {
        hw::frame_info frame_info() { return {dimensions, pixel_enum::gray8}; }

        using allocator_t = std::allocator<std::uint8_t>;

        // Fill a provided buffer
        void fill_image(bpp8_pixel_t* pixel_ptr)
        {
            // acquisition
            std::fill_n(pixel_ptr, total_size_in_bytes(), bpp8_pixel_t{1});
        }

        inline static const boost::gil::point_t dimensions = {256, 256};

        std::size_t total_size_in_bytes() { return frame_info().total_size_in_bytes(); }
    };

    // Define a buffer, the SDK tells how to allocate the pixels
    //using gray8_buffer_t = boost::gil::image<bpp8_pixel_t, false, typename sdk::allocator_t>;
    using buffer_pool_t = boost::safe::static_pool<>;
    //using buffer_pool_t = boost::lockfree::stack<boost::lockfree::fixed_sized<true>>;

    psi() : m_buffers(hw.total_size_in_bytes(), 1000) {}

    auto get_image()
    {
        m_frame_id++;

        auto pixel_ptr = (bpp8_pixel_t*) m_buffers.malloc();
        if (pixel_ptr == 0)
            throw std::runtime_error("ERROR: Buffer pool empty");

        hw.fill_image(pixel_ptr);

        return frame(
            sdk::dimensions, pixel_enum::gray8, pixel_ptr, sdk::dimensions.x,
            [this, id = m_frame_id, ptr = pixel_ptr]() {
                std::cout << "Release " << id << std::endl;
                m_buffers.free(ptr);
            },
            m_frame_id);
    }

    // Some buffer container (pool or circular buffer)
    buffer_pool_t m_buffers;

    sdk hw;

    int m_frame_id = 0;
};

} // namespace lima

// This simulates a simple producer-consumer pattern
// "speed" or data rate can be adjusted to simulate, for instance, buffer overrun occurence
//
template <typename Camera>
bool test_buffer(std::chrono::milliseconds daq_speed, std::chrono::milliseconds proc_speed)
{
    using namespace std::chrono_literals;

    using queue_t = boost::lockfree::spsc_queue<frame>;

    Camera cam;
    queue_t queue(100);

    std::atomic_bool done = false, done_daq = false;

    std::cout << "-----\n";

    // Acquisition thread
    auto a1 = std::async(std::launch::async, [&done, &done_daq, &cam, &queue, &daq_speed]() {
        while (!done) {
            std::this_thread::sleep_for(daq_speed);

            // Get an image
            auto v = cam.get_image();

            std::cout << "Acquiring " << v.id() << std::endl;

            //Push it to the queue
            bool res = queue.push(v);
            assert(res);
        }

        done_daq = true;
    });

    // Processing thread
    auto a2 = std::async(std::launch::async, [&done_daq, &cam, &queue, &proc_speed]() {
        while (!done_daq) {
            std::this_thread::sleep_for(proc_speed);

            // Pop a frame
            frame frm;
            bool available = queue.pop(frm);

            if (available)
                //Here we would send the buffer to processing (either locally or remotely)
                std::cout << "Processing " << frm.id() << std::endl;
        }

        while (!queue.empty()) {
            // Pop a frame
            frame frm;
            queue.pop(frm);

            //Here we would send the buffer to processing (either locally or remotely)
            std::cout << "Processing " << frm.id() << std::endl;
        }

        return 0;
    });

    // Acquisition for 5 sec
    std::this_thread::sleep_for(5s);
    done = true;

    // Wait and check for exception
    a1.get(); //may throw std::runtime_error if buffer overrun is detected
    a2.get();

    return queue.empty();
}

BOOST_AUTO_TEST_CASE(test_buffer_espia)
{
    using namespace std::chrono_literals;

    BOOST_CHECK(test_buffer<espia>(1ms, 1ms));
    BOOST_CHECK_THROW(test_buffer<espia>(1ms, 10ms), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(test_buffer_pco)
{
    using namespace std::chrono_literals;

    BOOST_CHECK(test_buffer<pco>(100ms, 150ms));
    BOOST_CHECK_THROW(test_buffer<pco>(100ms, 500ms), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(test_buffer_psi)
{
    using namespace std::chrono_literals;

    BOOST_CHECK(test_buffer<psi>(100ms, 150ms));
    BOOST_CHECK_THROW(test_buffer<psi>(100ms, 500ms), std::runtime_error);
}
