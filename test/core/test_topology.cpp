// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_TEST_MODULE core
#include <boost/test/unit_test.hpp>

#include <lima/topology.hpp>

namespace mpi = boost::mpi;

// This is bad but who cares in a test
namespace std
{
std::ostream& operator<<(std::ostream& os, const lima::topology::role& r)
{
    switch (r) {
    case lima::topology::role::master:
        os << "master";
        break;
    case lima::topology::role::control:
        os << "control";
        break;
    case lima::topology::role::daq:
        os << "daq";
        break;
    case lima::topology::role::processing:
        os << "processing";
        break;
    default:
        os.setstate(std::ios_base::failbit);
    }
    return os;
}

template <typename T1, typename T2>
std::ostream& operator<<(std::ostream& os, const std::pair<T1, T2>& lst)
{
    os << "(" << lst.first << ", " << lst.second << ")";
    return os;
}

} // namespace std

BOOST_AUTO_TEST_CASE(test_topology)
{
    mpi::environment env;
    mpi::communicator world;
    lima::topology::role role;

    if (world.rank() == 0)
        role = lima::topology::role::master;
    else
        role = lima::topology::role::control;

    lima::topology topo(role);

    std::vector<std::pair<int, lima::topology::role>> expected = {{0, lima::topology::role::master},
                                                                  {1, lima::topology::role::control}};
    BOOST_CHECK_EQUAL_COLLECTIONS(topo.m_roles.begin(), topo.m_roles.end(), expected.begin(), expected.end());
}