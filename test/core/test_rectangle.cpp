// Copyright (C) 2019 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <lima/core/rectangle.hpp>
#include <lima/core/io.hpp>

using namespace lima;

// Typedefs
using point_t = point<std::ptrdiff_t>;
using rectangle_t = rectangle<std::ptrdiff_t>;

BOOST_AUTO_TEST_CASE(test_rectangle_overlap)
{
    rectangle_t a, b, res;

    // two identical rects shifted by their diagonal: no overlap
    a = {{10, 10}, {10, 10}};
    b = {a.topleft + a.dimensions, a.dimensions};
    BOOST_CHECK_EQUAL(overlaps(a, b), false);
    BOOST_CHECK_EQUAL(intersection(a, b), intersection(b, a));

    // shift the second towards the first so they do overlap
    point_t shift = {2, 1};
    b.topleft -= shift;
    res = {b.topleft, shift};
    BOOST_CHECK_EQUAL(overlaps(a, b), true);
    BOOST_CHECK_EQUAL(intersection(a, b), res);
    BOOST_CHECK_EQUAL(intersection(b, a), res);

    // identical rects
    b = a;
    BOOST_CHECK_EQUAL(overlaps(a, b), true);
    BOOST_CHECK_EQUAL(intersection(a, b), a);
    BOOST_CHECK_EQUAL(intersection(b, a), a);

    // partial inclusion
    b.dimensions.y += 10;
    BOOST_CHECK_EQUAL(overlaps(a, b), true);
    BOOST_CHECK_EQUAL(intersection(a, b), a);
    BOOST_CHECK_EQUAL(intersection(b, a), a);

    // full inclusion
    a = {{10, 20}, {30, 40}};
    b = {{0, 0}, {50, 80}};
    BOOST_CHECK_EQUAL(overlaps(a, b), true);
    BOOST_CHECK_EQUAL(intersection(a, b), a);
    BOOST_CHECK_EQUAL(intersection(b, a), a);

    // partial overlap
    a = {{10, 20}, {20, 10}};
    b = {{15, 15}, {40, 40}};
    res = {{15, 20}, {15, 10}};
    BOOST_CHECK_EQUAL(overlaps(a, b), true);
    BOOST_CHECK_EQUAL(intersection(a, b), res);
    BOOST_CHECK_EQUAL(intersection(b, a), res);

    // partial overlap
    a = {{10, 20}, {20, 10}};
    b = {{15, 15}, {10, 20}};
    res = {{15, 20}, {10, 10}};
    BOOST_CHECK_EQUAL(overlaps(a, b), true);
    BOOST_CHECK_EQUAL(intersection(a, b), res);
    BOOST_CHECK_EQUAL(intersection(b, a), res);
}
