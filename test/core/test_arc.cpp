// Copyright 2018-2020 Mateusz Loskot <mateusz at loskot dot net>
// Copyright 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <type_traits>

#include <boost/core/typeinfo.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/core/arc.hpp>
#include <lima/core/io.hpp>

BOOST_AUTO_TEST_CASE(test_arc_bounding_box)
{
    {
        lima::arc_t arc = {{0.0, 0.0}, 1, 2, 0, 90};

        auto res = lima::bounding_box(arc);

        lima::rectangle_t expected = {{0, 0}, {2, 2}};
        BOOST_CHECK_EQUAL(res, expected);
    }

    {
        lima::arc_t arc = {{0.0, 0.0}, 1, 2, 45, 135};

        auto res = lima::bounding_box(arc);

        lima::rectangle_t expected = {{-1, 1}, {2, 1}};
        BOOST_CHECK_EQUAL(res, expected);
    }

    {
        lima::arc_t arc = {{0.0, 0.0}, 1, 2, 45, 225};

        auto res = lima::bounding_box(arc);

        lima::rectangle_t expected = {{-2, -1}, {3, 3}};
        BOOST_CHECK_EQUAL(res, expected);
    }

    {
        lima::arc_t arc = {{0.0, 0.0}, 1, 2, -45, 45};

        auto res = lima::bounding_box(arc);

        lima::rectangle_t expected = {{1, -1}, {1, 2}};
        BOOST_CHECK_EQUAL(res, expected);
    }
}