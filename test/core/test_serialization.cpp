// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <string>
#include <sstream>

#include <boost/test/unit_test.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/hana/define_struct.hpp>

#include <boost/gil.hpp>
#include <boost/gil/extension/dynamic_image/algorithm.hpp>
#include <boost/serialization/describe.hpp>

#include <lima/core/io.hpp>
#include <lima/core/serialization/frame.hpp>
#include <lima/core/serialization/layout.hpp>
#include <lima/core/frame_view.hpp>

//struct person
//{
//    BOOST_HANA_DEFINE_STRUCT(person, (std::string, name), (unsigned short, age));
//};
//
//struct toto
//{
//    std::string name;
//    unsigned short age;
//
//    template <class Archive>
//    void serialize(Archive& ar, const unsigned int version)
//    {
//        ar& name;
//        ar& age;
//    }
//};
//
//BOOST_AUTO_TEST_CASE(test_serialization_hana_struct)
//{
//    person p{"John Doe", 44};
//    toto t{"Toto", 12};
//
//    boost::archive::text_oarchive oa(std::cout);
//    oa << p;
//    oa << t;
//}

namespace lima
{
struct person
{
    std::string name;
    unsigned short age;
};

BOOST_DESCRIBE_STRUCT(person, (), (name, age))
} // namespace lima

BOOST_AUTO_TEST_CASE(test_serialization_describe)
{
    lima::person p{"John Doe", 44};

    boost::archive::text_oarchive oa(std::cout);
    oa << p;
}

BOOST_AUTO_TEST_CASE(test_serialization_frame)
{
    std::stringstream ss;

    lima::frame in(10, 10, lima::pixel_enum::gray8), out;

    fill_pixels(view(in), lima::bpp8_pixel_t(7));

    boost::archive::text_oarchive oa(ss);
    oa << in;

    boost::archive::text_iarchive ia(ss);
    ia >> out;

    BOOST_CHECK_EQUAL(in.dimensions(), out.dimensions());
    BOOST_CHECK(equal_pixels(const_view(in), const_view(out)));
}

BOOST_AUTO_TEST_CASE(test_serialization_layout)
{
    std::stringstream ss;

    lima::layout in(10, 10, {}), out;

    boost::archive::text_oarchive oa(ss);
    oa << in;

    boost::archive::text_iarchive ia(ss);
    ia >> out;

    BOOST_CHECK_EQUAL(in.dimensions(), out.dimensions());
    BOOST_CHECK_EQUAL(in.nb_items(), out.nb_items());
}

//BOOST_AUTO_TEST_CASE(test_serialization_gil_view)
//{
//    using namespace boost::gil;
//
//    std::stringstream ss;
//
//    gray8_image_t in(10, 10), out(10, 10);
//
//    fill_pixels(view(in), 7);
//
//    boost::archive::text_oarchive oa(ss);
//    oa << const_view(in);
//
//    boost::archive::text_iarchive ia(ss);
//    ia >> view(out);
//
//    BOOST_CHECK(equal_pixels(const_view(in), const_view(out)));
//}
//
//BOOST_AUTO_TEST_CASE(test_serialization_gil_image)
//{
//    using namespace boost::gil;
//
//    std::stringstream ss;
//
//    gray8_image_t in(10, 10), out(10, 10);
//
//    fill_pixels(view(in), 7);
//
//    boost::archive::text_oarchive oa(ss);
//    oa << in;
//
//    boost::archive::text_iarchive ia(ss);
//    ia >> out;
//
//    BOOST_CHECK(equal_pixels(const_view(in), const_view(out)));
//}