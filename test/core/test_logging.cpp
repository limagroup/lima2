// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_TEST_MODULE logging
#include <boost/test/unit_test.hpp>

#include <regex>

#include <lima/logging.hpp>

using namespace lima;

// Defines a custom application domain
namespace lima
{
namespace log
{
    namespace app_domain
    {
        // Needs to be injected in lima::log::app_domain namespace
        // By convention, the two high bytes are the main domain (aka det)
        inline static const channel_name det_serial = {"det serial", 0x2001};
        inline static const channel_name det_usb = {"det usb", 0x2002};

    } // namespace app_domain
} // namespace log
} // namespace lima

BOOST_AUTO_TEST_CASE(test_logging)
{
    log::init();
    log::add_console_log();

    log::set_filter(log::severity_level::debug, log::app_domain::core);

    LIMA_LOG(info, core) << "1. Info message from core";
    LIMA_LOG(error, ctl) << "1. Error message from control";
    LIMA_LOG(trace, acq) << "1. Trace message from acquisition";

    {
        LIMA_LOG_SCOPE("test_named_scope_logging");

        LIMA_LOG(info, core) << "1. Hello from the test_named_scope_logging scope!";
    }

    LIMA_LOG(trace, core) << "1. Trace message from core";
    LIMA_LOG(fatal, core) << "1. Fatal message from core";

    log::set_filter(log::severity_level::warning, log::app_domain::ctl);

    LIMA_LOG(trace, core) << "2. Trace message from core";
    LIMA_LOG(fatal, ctl) << "2. Fatal message from control";

    log::set_filter(log::severity_level::info, log::app_domain::det);

    LIMA_LOG(info, det_serial) << "3. Info message from camera serial line";
    LIMA_LOG(info, det_usb) << "3. Info message from camera usb line";

    log::set_filter(log::severity_level::info, log::app_domain::det_serial);

    LIMA_LOG(info, det_serial) << "4. Info message from camera serial line";
    LIMA_LOG(info, det_usb) << "4. Info message from camera usb line";
}

// Expected result
//[2020-07-10 12:52:17.346988][0x000030bd][0x00007f648c837740][../test/core/test_logging.cpp:36][info][core][] 1. Info message from core
//[2020-07-10 12:52:17.347075][0x000030bd][0x00007f648c837740][../test/core/test_logging.cpp:43][info][core][test_named_scope_logging] 1. Hello from the test_named_scope_logging scope!
//[2020-07-10 12:52:17.347088][0x000030bd][0x00007f648c837740][../test/core/test_logging.cpp:47][fatal][core][] 1. Fatal message from core
//[2020-07-10 12:52:17.347097][0x000030bd][0x00007f648c837740][../test/core/test_logging.cpp:52][fatal][ctl][] 2. Fatal message from control
//[2020-07-10 12:52:17.347104][0x000030bd][0x00007f648c837740][../test/core/test_logging.cpp:56][info][det serial][] 3. Info message from camera serial line
//[2020-07-10 12:52:17.347110][0x000030bd][0x00007f648c837740][../test/core/test_logging.cpp:57][info][det usb][] 3. Info message from camera usb line
//[2020-07-10 12:52:17.347116][0x000030bd][0x00007f648c837740][../test/core/test_logging.cpp:61][info][det serial][] 4. Info message from camera serial line

// ---
// Log to file
// ---

BOOST_AUTO_TEST_CASE(test_logging_filesink)
{
    // Clean up old log file if present
    std::remove("./test_logging.log");

    log::init();
    log::add_file_log(".", "test_logging.log");
    // core | acq -> warning
    log::set_filter(log::severity_level::warning,
                    log::channel_name("core+acq", log::app_domain::core | log::app_domain::acq));

    LIMA_LOG(info, core) << "info + core" << std::endl;
    LIMA_LOG(warning, core) << "warning + core" << std::endl;

    LIMA_LOG(info, acq) << "info + acq" << std::endl;
    LIMA_LOG(warning, acq) << "warning + acq" << std::endl;
    LIMA_LOG(fatal, acq) << "fatal + acq" << std::endl;

    // Force flush, otherwise some entries will be missing
    boost::log::core::get()->flush();

    std::ifstream log_file("./test_logging.log");
    std::stringstream log_contents_ss;
    log_contents_ss << log_file.rdbuf();
    std::string log_contents = log_contents_ss.str();

    // Check that logs are filtered by severity:
    size_t found_pos;

    // - [info|core] is filtered out
    found_pos = log_contents.find("info + core");
    BOOST_CHECK(found_pos == std::string::npos);

    // - [warning|core] is logged
    found_pos = log_contents.find("warning + core");
    BOOST_CHECK(found_pos != std::string::npos);

    // - [info|acq] is filtered out
    found_pos = log_contents.find("info + acq");
    BOOST_CHECK(found_pos == std::string::npos);

    // - [warning|acq] is logged
    found_pos = log_contents.find("warning + acq");
    BOOST_CHECK(found_pos != std::string::npos);

    // - [fatal|acq] is logged
    found_pos = log_contents.find("fatal + acq");
    BOOST_CHECK(found_pos != std::string::npos);

    // ---

    // Check the log format:
    // - the structure is identifiable: [timestamp][pid][tid][file:line][sev|chan][] msg
    // - the filename, severity, channel and message are consistent
    std::string pattern =
        R"(\[.+\]\[.+\]\[.+\]\[test\/core\/test_logging.cpp:\d+\]\[warning\|core\]\[\] warning \+ core)";
    std::regex log_regex(pattern);

    BOOST_CHECK_MESSAGE(std::regex_search(log_contents, log_regex), "'" << pattern << "' in log contents");
}
