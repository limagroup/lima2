// Copyright (C) 2018 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <chrono>

#include <iostream>
#include <iomanip>
#include <type_traits>
#include <limits>
#include <cmath>

using namespace std::literals;

using milliseconds = std::chrono::milliseconds;
using nanoseconds = std::chrono::nanoseconds;
using picoseconds = std::chrono::duration<std::int64_t, std::pico>;

template <typename T>
void query_arithmetic_type(std::string name)
{
    std::cout << name << ":" << std::endl;
    if (std::is_integral<T>()) {
        std::cout << "  is_integral: " << std::is_integral<T>() << std::endl;
    } else {
        std::cout << "  is_floating_point: " << std::is_floating_point<T>() << std::endl;
    }
    std::cout << "  size: " << sizeof(T) << " "
              << "(" << (sizeof(T) * 8) << " bits)" << std::endl;
}

#define DEFINE_CLOCK(b)                \
    struct b                           \
    {                                  \
        using clock = std::chrono::b;  \
        static const std::string name; \
    };                                 \
    const std::string b::name = #b##s

DEFINE_CLOCK(steady_clock);
DEFINE_CLOCK(system_clock);
DEFINE_CLOCK(high_resolution_clock);

std::string power_of_10(std::intmax_t n)
{
    double l = std::log10(n);
    int power = int(std::floor(l));
    double base = std::pow(10, l - power);
    std::ostringstream os;
    os << base;
    if (power != 0)
        os << "*10^" << power;
    return os.str();
}

template <typename R>
void query_ratio(std::string name)
{
    std::cout << name << ":" << std::endl;
    std::cout << "  num/den: " << power_of_10(R::num) << "/" << power_of_10(R::den) << std::endl;
}

template <typename D>
void query_duration(std::string name)
{
    std::cout << name << ":" << std::endl;
    bool is_ns = std::is_same_v<D, nanoseconds>;
    std::cout << "  is nanoseconds: " << (is_ns ? "true" : "false") << std::endl;
    picoseconds one_tick = std::chrono::duration_cast<picoseconds>(D(1));
    std::cout << "  to picoseconds: " << one_tick.count() << std::endl;
}

template <typename TP>
void query_time_point(std::string name)
{
    using Clock = typename TP::clock;
    using Rep = typename TP::rep;

    std::cout << name << ":" << std::endl;
    const std::size_t N = 1000;
    std::vector<TP> tp_array(N + 1);

    for (auto& t : tp_array)
        t = Clock::now();

    long long acc = 0, acc2 = 0;
    for (std::size_t i = 0; i < N; ++i) {
        Rep d = (tp_array[i + 1] - tp_array[i]).count();
        acc += d;
        acc2 += d * d;
    }
    double ave = double(acc) / N;
    double dev = std::sqrt(double(acc2) / N - ave * ave);
    std::cout << "  diff ave: " << ave << std::endl;
    std::cout << "  diff std: " << dev << std::endl;
}

template <typename NClock>
void test_clock()
{
    using Clock = typename NClock::clock;
    auto& name = NClock::name;

    std::cout << "****** " << name << " ******" << std::endl;
    query_arithmetic_type<typename Clock::rep>(name + "::rep");
    query_ratio<typename Clock::period>(name + "::period");
    query_duration<typename Clock::duration>(name + "::duration");
    query_time_point<typename Clock::time_point>(name + "::time_point");
    std::cout << std::endl;
}

template <typename TP>
std::string precise_time_str(const TP& tp)
{
    using Clock = typename TP::clock;
    auto t = Clock::to_time_t(tp);
    auto tp0 = Clock::from_time_t(t);
    auto ns = std::chrono::duration_cast<nanoseconds>(tp - tp0);
    std::string atime = std::asctime(std::localtime(&t));
    atime.pop_back();
    std::ostringstream os;
    os << atime << " +" << std::setw(9) << std::setfill('0') << ns.count() << " ns";
    return os.str();
}

template <typename NClock1, typename NClock2>
void test_cross_clocks()
{
    auto& name1 = NClock1::name;
    auto& name2 = NClock2::name;

    std::cout << "* " << name1 << " time_point + " << name2 << " duration *" << std::endl;

    using Clock1 = typename NClock1::clock;
    using Clock2 = typename NClock2::clock;

    auto tp1_0 = Clock1::now();
    auto tp2_0 = Clock2::now();

    milliseconds sleep_ms(100);
    std::this_thread::sleep_for(sleep_ms);

    auto tp1_1 = Clock1::now();
    auto tp2_1 = Clock2::now();

    std::cout << "start " << name1 << "::time_point:" << std::endl;
    std::cout << "  tp1_0: " << precise_time_str(tp1_0) << std::endl;
    std::cout << "final " << name1 << "::time_point "
              << "(after " << sleep_ms.count() << " ms):" << std::endl;
    std::cout << "  tp1_1: " << precise_time_str(tp1_1) << std::endl;

    auto d1 = tp1_1 - tp1_0;
    auto d2 = tp2_1 - tp2_0;
    auto d1_d2 = std::chrono::duration_cast<typename Clock1::duration>(d2);

    std::cout << "measured " << name1 << "::duration:" << std::endl;
    std::cout << "  d1: " << std::chrono::duration_cast<nanoseconds>(d1).count() << " ns" << std::endl;
    std::cout << "measured " << name2 << "::duration:" << std::endl;
    std::cout << "  d2: " << std::chrono::duration_cast<nanoseconds>(d2).count() << " ns" << std::endl;

    auto tp1_2 = tp1_0 + d1_d2;
    std::cout << "calculated " << name1 << "::time_point "
              << "(tp1_0 + duration_cast<" << name1 << "::duration>(d2)):" << std::endl;
    std::cout << "  tp1_2: " << precise_time_str(tp1_2) << std::endl;
    std::cout << std::endl;
}

int main(int argc, char* argv[])
{
    query_ratio<nanoseconds::period>("nanoseconds::period");
    nanoseconds one_millisec = 1ms;
    std::cout << "1 ms has " << power_of_10(one_millisec.count()) << " ns" << std::endl;

    test_clock<steady_clock>();

    test_clock<system_clock>();

    test_clock<high_resolution_clock>();

    test_cross_clocks<system_clock, steady_clock>();

    test_cross_clocks<system_clock, high_resolution_clock>();

    return 0;
}
