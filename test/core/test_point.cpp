// Copyright 2018-2020 Mateusz Loskot <mateusz at loskot dot net>
// Copyright 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <type_traits>

#include <boost/core/typeinfo.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/core/point.hpp>
#include <lima/core/io.hpp>

BOOST_AUTO_TEST_CASE(test_default_constructor)
{
    lima::point<int> p;
    BOOST_CHECK_EQUAL(p.x, 0);
    BOOST_CHECK_EQUAL(p.y, 0);
}

BOOST_AUTO_TEST_CASE(test_user_constructor)
{
    lima::point<int> p{1, 2};
    BOOST_CHECK_EQUAL(p.x, 1);
    BOOST_CHECK_EQUAL(p.y, 2);
}

BOOST_AUTO_TEST_CASE(test_copy_constructor)
{
    lima::point<int> p1{1, 2};
    lima::point<int> p2{p1};
    BOOST_CHECK_EQUAL(p1.x, p2.x);
    BOOST_CHECK_EQUAL(p1.y, p2.y);
}

BOOST_AUTO_TEST_CASE(test_copy_assignment_operator)
{
    lima::point<int> p1{1, 2};
    lima::point<int> p2;
    p2 = p1;
    BOOST_CHECK_EQUAL(p1.x, p2.x);
    BOOST_CHECK_EQUAL(p1.y, p2.y);
}

BOOST_AUTO_TEST_CASE(test_index_operator)
{
    lima::point<int> p{1, 2};
    BOOST_CHECK_EQUAL(p[0], 1);
    BOOST_CHECK_EQUAL(p[1], 2);
}

BOOST_AUTO_TEST_CASE(test_addition_operator)
{
    lima::point<int> p1{1, 1};
    lima::point<int> const p2{2, 4};
    auto p3 = p1 + p2;
    BOOST_CHECK_EQUAL(p3.x, 3);
    BOOST_CHECK_EQUAL(p3.y, 5);
}

BOOST_AUTO_TEST_CASE(test_addition_assignment_operator)
{
    lima::point<int> p1{1, 1};
    lima::point<int> const p2{2, 4};
    p1 += p2;
    BOOST_CHECK_EQUAL(p1.x, 3);
    BOOST_CHECK_EQUAL(p1.y, 5);
}

BOOST_AUTO_TEST_CASE(test_subtraction_assignment_operator)
{
    lima::point<int> p1{2, 4};
    lima::point<int> const p2{1, 1};
    p1 -= p2;
    BOOST_CHECK_EQUAL(p1.x, 1);
    BOOST_CHECK_EQUAL(p1.y, 3);
}

BOOST_AUTO_TEST_CASE(test_subtraction_operator)
{
    lima::point<int> p1{2, 4};
    lima::point<int> const p2{1, 1};
    p1 = p1 - p2;
    BOOST_CHECK_EQUAL(p1.x, 1);
    BOOST_CHECK_EQUAL(p1.y, 3);
}

BOOST_AUTO_TEST_CASE(test_unary_minus_operator)
{
    lima::point<int> p1{2, 4};
    auto p2 = -p1;
    BOOST_CHECK_EQUAL(p2.x, -2);
    BOOST_CHECK_EQUAL(p2.y, -4);
    p2 = -p2;
    BOOST_CHECK_EQUAL(p2.x, p1.x);
    BOOST_CHECK_EQUAL(p2.y, p1.y);
}

BOOST_AUTO_TEST_CASE(test_division_assignment_operator)
{
    {
        lima::point<int> p1{2, 4};
        p1 /= 2;
        static_assert(std::is_same<decltype((p1 / short{}).x), int>::value, "!int");
        static_assert(std::is_same<decltype((p1 / int{}).x), int>::value, "!int");
        static_assert(std::is_same<decltype((p1 / float{}).x), float>::value, "!float");
        static_assert(std::is_same<decltype((p1 / double{}).x), double>::value, "!double");
        BOOST_CHECK_EQUAL(p1.x, 1);
        BOOST_CHECK_EQUAL(p1.y, 2);
    }
    // point / d
    {
        lima::point<int> p1{2, 4};
        auto p2 = p1 / float{2};
        static_assert(std::is_same<decltype((p2 / int{}).x), float>::value, "!float");
        static_assert(std::is_same<decltype((p2 / float{}).x), float>::value, "!float");
        static_assert(std::is_same<decltype((p2 / double{}).x), double>::value, "!double");
        BOOST_CHECK_GE(p2.x, 1.0); // means, but >= avoids compiler warning
        BOOST_CHECK_GE(p2.y, 2.0);
    }
}

BOOST_AUTO_TEST_CASE(test_multiplication_operator)
{
    lima::point<int> p1{2, 4};
    p1 *= 2;
    BOOST_CHECK_EQUAL(p1.x, 4);
    BOOST_CHECK_EQUAL(p1.y, 8);

    // point * m
    {
        auto p2 = p1 * int{2};
        static_assert(std::is_same<decltype(p2.x), int>::value, "!int");
        static_assert(std::is_same<decltype(p2.y), int>::value, "!int");
        BOOST_CHECK_EQUAL(p2.x, 8);
        BOOST_CHECK_EQUAL(p2.y, 16);
    }
    // m * point
    {
        auto p2 = double{2} * p1;
        static_assert(std::is_same<decltype(p2.x), double>::value, "!double");
        static_assert(std::is_same<decltype(p2.y), double>::value, "!double");
        BOOST_CHECK_GE(p2.x, 8); // means, but >= avoids compiler warning
        BOOST_CHECK_GE(p2.y, 16);
    }
}

BOOST_AUTO_TEST_CASE(test_multiplication_assignment_operator)
{
    lima::point<int> p1{2, 4};
    // point * m
    {
        auto p2 = p1 * int{2};
        static_assert(std::is_same<decltype(p2.x), int>::value, "!int");
        static_assert(std::is_same<decltype(p2.y), int>::value, "!int");
        BOOST_CHECK_EQUAL(p2.x, 4);
        BOOST_CHECK_EQUAL(p2.y, 8);
    }
    // m * point
    {
        auto p2 = double{2} * p1;
        static_assert(std::is_same<decltype(p2.x), double>::value, "!double");
        static_assert(std::is_same<decltype(p2.y), double>::value, "!double");
        BOOST_CHECK_GE(p2.x, 4); // means, but >= avoids compiler warning
        BOOST_CHECK_GE(p2.y, 8);
    }
}

BOOST_AUTO_TEST_CASE(test_bitwise_left_shift_operator)
{
    lima::point<unsigned int> p{2, 4};
    p = p << 1;
    BOOST_CHECK_EQUAL(p.x, 4u);
    BOOST_CHECK_EQUAL(p.y, 8u);
}

BOOST_AUTO_TEST_CASE(test_bitwise_right_shift_operator)
{
    lima::point<unsigned int> p{2, 4};
    p = p >> 1;
    BOOST_CHECK_EQUAL(p.x, 2u / 2);
    BOOST_CHECK_EQUAL(p.y, 4u / 2);
}

BOOST_AUTO_TEST_CASE(test_equal_to_operator)
{
    lima::point<int> p1{2, 4};
    lima::point<int> p2{2, 4};
    BOOST_CHECK_EQUAL(p1, p2);
}

BOOST_AUTO_TEST_CASE(test_not_equal_to_operator)
{
    lima::point<int> p1{1, 1};
    lima::point<int> p2{2, 4};
    BOOST_CHECK_NE(p1, p2);
}

BOOST_AUTO_TEST_CASE(test_axis_value)
{
    lima::point<int> p1{1, 2};
    lima::point<int> const p2{1, 2};
    BOOST_CHECK_EQUAL(lima::axis_value<0>(p1), p1.x);
    BOOST_CHECK_EQUAL(lima::axis_value<1>(p1), p1.y);
    BOOST_CHECK_EQUAL(lima::axis_value<0>(p2), p2.x);
    BOOST_CHECK_EQUAL(lima::axis_value<1>(p2), p2.y);
}
