// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cassert>

#include <chrono>
#include <iostream>
#include <thread>

#include <boost/mpi.hpp>

namespace mpi = boost::mpi;

#define LOG_CTL std::cout << "[CTL] "
#define LOG_RCV std::cout << "[RCV] "

struct consensus
{
    bool operator()(bool lhs, bool rhs) const { return lhs && rhs; }
};

namespace boost
{
namespace mpi
{
    template <>
    struct is_commutative<consensus, bool> : mpl::true_
    {
    };

} // namespace mpi
} // namespace boost

enum class command
{
    prepare,
    start,
    stop,
    quit
};

struct control
{
    control(mpi::communicator& world) : m_world(world)
    {
        std::cout << "I am process control " << m_world.rank() << " of " << m_world.size() << "." << std::endl;

        if (std::getenv("LIMA2_MPI_TEST_ATTACH_GDB")) {
            std::cout << std::unitbuf; // enable automatic flushing
            std::cout << "Attach debugger then press <ENTER> key to continue..." << std::endl;
            std::cin.get();

            m_world.barrier();
        }
    }

    ~control()
    {
        // Send the quit command
        quit();
    }

    void quit() const
    {
        LOG_CTL << "Send command quit" << std::endl;

        // Send the quit
        command cmd = command::quit;
        mpi::broadcast(m_world, cmd, 0);

        // Get the result from main receiver
        bool res;
        m_world.recv(1, mpi::any_tag, res);
    }

    bool prepare() const
    {
        assert(m_world.rank() == 0);

        LOG_CTL << "Send command prepare" << std::endl;

        // Send the prepare command
        command cmd = command::prepare;
        mpi::broadcast(m_world, cmd, 0);

        // Get the result from main receiver
        bool res;
        m_world.recv(1, mpi::any_tag, res);

        return res;
    }

    mpi::communicator m_world;
};

struct receiver
{
    enum class state
    {
        idle,
        prepared,
        running,
        stoping,
        exiting
    };

    receiver(mpi::communicator& world, mpi::communicator& receivers, int master_rank) :
        m_world(world), m_receivers(receivers), m_master_rank(master_rank)
    {
        std::cout << "I am process receiver " << m_world.rank() << " of " << m_world.size() << "." << std::endl;

        if (std::getenv("LIMA2_MPI_TEST_ATTACH_GDB")) {
            m_world.barrier();
        }
    }

    void listen()
    {
        bool done = false;
        while (done == false) {
            bool res = false;

            // Broadcasted command from control
            command cmd;
            mpi::broadcast(m_world, cmd, 0);

            switch (cmd) {
            case command::prepare:
                LOG_RCV << "Command prepare" << std::endl;
                res = prepare();
                break;
            case command::quit:
                LOG_RCV << "Command quit" << std::endl;
                done = true;
                break;
            default:
                assert(false);
            }

            LOG_RCV << "Reducing result" << std::endl;

            // Compute the result (and sync receivers)
            mpi::all_reduce(m_receivers, res, consensus());

            if (m_receivers.rank() == 0) {
                LOG_RCV << "Returning result" << std::endl;

                //Returns the result to controller
                m_world.send(m_master_rank, 0, res);
            }
        }
    }

    bool prepare()
    {
        using namespace std::chrono_literals;

        LOG_RCV << "Entering prepare" << std::endl;

        std::this_thread::sleep_for(1s);

        LOG_RCV << "Leaving prepare" << std::endl;

        return true;
    }

    //state m_state;
    mpi::communicator m_world;
    mpi::communicator m_receivers;
    int m_master_rank; //master rank in the world comm
};

enum class comm_color : int
{
    controler,
    receiver
};

int main(int argc, char* argv[])
{
    mpi::environment env(/*argc, argv, */ boost::mpi::threading::level::multiple);
    mpi::communicator world;

    comm_color color = comm_color::controler;
    if (world.rank() > 0)
        color = comm_color::receiver;

    // group comm for controls and receivers
    mpi::communicator sub = world.split((int) color);

    if (world.rank() == 0) {
        control ctrl{world};
        ctrl.prepare();
    } else {
        receiver rcv{world, sub, 0};
        rcv.listen();
    }

    world.barrier();

    return 0;
}