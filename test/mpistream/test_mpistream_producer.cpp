// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

//#define BOOST_TEST_MODULE mpistream
//#include <boost/test/unit_test.hpp>

#include <MPIStream.h>

void error_if(int res, const char* msg)
{
    if (res != MPI_SUCCESS)
        throw msg;
}

//BOOST_AUTO_TEST_CASE(test_mpistream_producer)
int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    // Channel
    MPIStream_Channel channel;
    error_if(MPIStream_CreateChannel(1, 0, MPI_COMM_WORLD, &channel), "Failed to create channel");

    // Datatype
    MPI_Datatype type;
    MPI_Type_contiguous(10, MPI_INT, &type);
    MPI_Type_commit(&type);

    // Attach operation
    MPIStream stream;
    error_if(MPIStream_Attach(type, type, type, NULL, &stream, &channel), "Failed to attach stream");

    // Start generating data
    bool done = false;
    while (!done) {
        int buffer[10];
        for (int i = 0; i < 10; i++)
            buffer[i] = i;

        MPIStream_Send(buffer, &stream);
    }

    // Finalize
    MPIStream_Terminate(&stream);
    MPIStream_FreeChannel(&channel);
    MPI_Finalize();

    return 0;
}
