// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <any>
#include <iostream>

#define BOOST_TEST_MODULE utils
#include <boost/test/unit_test.hpp>

#include <lima/utils/factory.hpp>

using namespace lima;

//struct Base { virtual ~Base() {} };

using result_t = std::any;

/// testing
struct A
{
    A(int, char) {}
};
struct B
{
    B(double, bool){}; /*B(B const&) = delete;*/
};

result_t createA(int i, char c)
{
    return new A{i, c};
}
result_t createB(double d, bool b)
{
    return new B{d, b};
}

BOOST_AUTO_TEST_CASE(test_make_function)
{
    auto f1 = detail::make_function([](int) -> result_t { return new A{1, 'a'}; });
    auto f2 = detail::make_function([](int) -> result_t { return new B{1.1, false}; });
}

BOOST_AUTO_TEST_CASE(test_factory_base)
{
    factory<result_t> fab;

    fab.reg("A", &createA);
    fab.reg("B", &createB);

    BOOST_CHECK_NO_THROW({
        result_t base = fab.create("A", 1, 'a');
        auto a = std::any_cast<A*>(base);
    });

    BOOST_CHECK_NO_THROW({
        result_t base = fab.create("B", 1.1, false);
        auto b = std::any_cast<B*>(base);
    });
}

BOOST_AUTO_TEST_CASE(test_factory_std_function_lvalue)
{
    factory<result_t> factory;

    // explicit conversion to std::function
    std::function<result_t(char c)> f = [](char c) -> result_t { return A{1, c}; };

    factory.reg("A", f);

    BOOST_CHECK_NO_THROW(factory.create("A", 'a'));
}

BOOST_AUTO_TEST_CASE(test_factory_std_function_rvalue)
{
    factory<result_t> factory;

    factory.reg("A", std::function<result_t(char c)>([](char c) -> result_t { return A{1, c}; }));

    BOOST_CHECK_NO_THROW(factory.create("A", 'a'));
}

BOOST_AUTO_TEST_CASE(test_factory_lambda)
{
    factory<result_t> factory;

    factory.reg("lambdaA", [](int i) -> result_t { return A{i, 'a'}; });

    BOOST_CHECK_NO_THROW(factory.create("lambdaA", 42));
}

BOOST_AUTO_TEST_CASE(test_factory_exception)
{
    factory<result_t> factory;
    factory.reg("A", &createA);
    BOOST_CHECK_THROW(factory.create("A", 4), std::runtime_error);
    BOOST_CHECK_THROW(factory.create("hihi"), std::runtime_error);
}