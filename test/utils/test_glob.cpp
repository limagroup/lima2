// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <iterator>
#include <vector>

#define BOOST_TEST_MODULE utils
#include <boost/test/unit_test.hpp>

#include <lima/utils/glob.hpp>

// Usage ./test_glob -- test*.h5
BOOST_AUTO_TEST_CASE(test_glob)
{
    using namespace boost::unit_test;

    const std::filesystem::path target_path(".");
    std::string pattern;

    if (framework::master_test_suite().argc == 2)
        pattern = framework::master_test_suite().argv[1];
    else
        pattern = "*.*";

    std::vector<std::filesystem::path> all_matching_files;

    lima::glob(target_path, pattern, std::back_inserter(all_matching_files));

    if (all_matching_files.empty())
        std::cout << "No file found that matches the given pattern" << std::endl;
    else
        for (auto& entry : all_matching_files)
            std::cout << entry.filename() << std::endl;
}
