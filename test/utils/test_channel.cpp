// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <chrono>
#include <thread>

#define BOOST_TEST_MODULE utils
#include <boost/test/unit_test.hpp>

#include <lima/utils/channel.hpp>

enum class state
{
    init,
    idle,
    running,
    fault
};

using namespace std::chrono;

template <class Sender, class Recv, class Rep = int, class Period = std::ratio<1, 1>>
bool test_channel(std::pair<Sender, Recv>&& sender_and_receiver,
                  const duration<Rep, Period>& sleep_duration = duration<Rep, Period>::zero())
{
    auto [sender, receiver] = std::move(sender_and_receiver);

    std::vector<state> recv_states;

    std::thread t1([&]() {
        state s = state::init;
        while (s != state::fault) {
            s = receiver.get();
            recv_states.push_back(s);
            std::cout << "Thread [1] state " << static_cast<int>(s) << std::endl;
        }
        std::cout << "Thread [1] leaving" << std::endl;
    });

    std::vector<state> send_states{state::idle, state::running, state::fault};

    std::thread t2([&]() {
        std::cout << "Thread [2] Before notification" << std::endl;
        for (auto s : send_states) {
            sender.set_value(s);
            std::this_thread::sleep_for(sleep_duration);
        }
        std::cout << "Thread [2] After notification" << std::endl;
    });

    t1.join();
    t2.join();

    bool ok = (recv_states == send_states);
    return ok;
}

BOOST_AUTO_TEST_CASE(test_channel_buffered)
{
    std::cout << "Test notification buffered_state: " << std::endl;
    bool ok = test_channel(lima::channel::make_buffered_channel<state>());
    BOOST_CHECK_EQUAL(ok, true);
    std::cout << " Done!" << std::endl;
}

using namespace std::chrono_literals;

BOOST_AUTO_TEST_CASE(test_channel_unbuffered)
{
    using namespace std::chrono_literals;
    std::cout << "Test notification unbuffered_state" << std::endl;
    bool ok = test_channel(lima::channel::make_unbuffered_channel<state>(), 500ms);
    BOOST_CHECK_EQUAL(ok, true);
    std::cout << " Done!" << std::endl;
}
