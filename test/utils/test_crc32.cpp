// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <variant>

#define BOOST_TEST_MODULE utils
#include <boost/test/unit_test.hpp>

#include <lima/utils/crc32.hpp>

BOOST_AUTO_TEST_CASE(test_crc32)
{
    // Compile time CRC
    static_assert(const_crc32("123456789") == 0xCBF43926);

    // Run time CRC
    BOOST_CHECK_EQUAL(const_crc32("123456789"), 0xCBF43926);
}
