// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_TEST_MODULE utils
#include <boost/test/unit_test.hpp>

#include <lima/utils/smart_handle.hpp>

using handle_t = void*;

handle_t open_resource()
{
    return reinterpret_cast<handle_t>(0x00000001);
}

int nb_close_call = 0;

void close_resource(handle_t h)
{
    nb_close_call++;
}

using unique_handle_t = lima::unique_handle<handle_t, decltype(&::close_resource), ::close_resource>;
using shared_handle_t = lima::shared_handle<unique_handle_t>;

BOOST_AUTO_TEST_CASE(test_unique_handle)
{
    nb_close_call = 0;
    {
        unique_handle_t h;
    }
    BOOST_CHECK_EQUAL(nb_close_call, 0);

    nb_close_call = 0;
    {
        unique_handle_t h(open_resource());
    }
    BOOST_CHECK_EQUAL(nb_close_call, 1);
}

BOOST_AUTO_TEST_CASE(test_shared_handle)
{
    nb_close_call = 0;
    {
        shared_handle_t h;
    }
    BOOST_CHECK_EQUAL(nb_close_call, 0);

    nb_close_call = 0;
    {
        shared_handle_t h1;
        shared_handle_t h2 = h1;
    }
    BOOST_CHECK_EQUAL(nb_close_call, 0);

    nb_close_call = 0;
    {
        shared_handle_t h1(open_resource());
        shared_handle_t h2 = h1;
    }
    BOOST_CHECK_EQUAL(nb_close_call, 1);
}
