// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <variant>

#define BOOST_TEST_MODULE utils
#include <boost/test/unit_test.hpp>

#include <lima/utils/variant_algo.hpp>

// Print type names
class print_name
{
    std::ostream& m_out;

  public:
    print_name(std::ostream& out) : m_out(out) {}

    template <typename T>
    void apply() const
    {
        m_out << "  - " << typeid(T).name() << "\n";
    };
};

BOOST_AUTO_TEST_CASE(test_for_each)
{
    using variant_t = std::variant<int, double>;

    lima::for_each<variant_t>(print_name(std::cout));
}

// Construct camera by name
class by_name
{
    std::string m_name;

  public:
    by_name(std::string name) : m_name(name) {}

    template <typename T>
    bool apply() const
    {
        return typeid(T).name() == m_name;
    };
};

BOOST_AUTO_TEST_CASE(test_construct_matched)
{
    using variant_t = std::variant<int, double>;

    variant_t v;
    lima::construct_matched(v, by_name("double"));

    v.emplace<0>();
}