// Copyright (C) 2020 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "frame_fixture.hpp"

#include <lima/io/hdf5.hpp>

// A fixture that generates a hdf5 file with data
struct hdf5_file_fixture : frame_fixture
{
    hdf5_file_fixture(std::string n = "test_hdf5_data.h5",
                      lima::io::h5::compression_enum c = lima::io::h5::compression_enum::none) :
        file_name(n), comp(c)
    {
        BOOST_TEST_MESSAGE("setup hdf5_file_fixture");
        lima::io::h5::saving_params params; // default constructor: 1 frame/file, 1 frame/chunk
        params.compression = comp;

        lima::io::h5::writer m(file_name, params, input_frame_info);
        m.write_view(lima::const_view(input_frame));
    }

    std::string file_name;
    lima::io::h5::compression_enum comp;
};
