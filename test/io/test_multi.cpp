// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>
#include <string>

#include <boost/test/unit_test.hpp>

#include <lima/io/multi.hpp>

namespace gil = boost::gil;
namespace io = lima::io;

namespace mock
{
struct frame_view
{
    frame_view() = default;
    frame_view(std::size_t idx) : m_idx(idx) {}

    std::size_t idx() const { return m_idx; }

    std::size_t m_idx = 0;
};

struct writer
{
    using params_t = io::saving_params;

    inline static const std::size_t max_nb_frames_per_file = 1000;

    template <typename String, typename... Settings>
    writer(String const& filepath, params_t const& params, Settings const&... settings) : m_filename(filepath)
    {
        std::cout << "opening file " << m_filename << std::endl;
        is_open = true;
    }

    writer(writer& w) = delete;
    writer(writer&& w) = default;

    ~writer()
    {
        std::cout << "closing file " << m_filename << std::endl;
        is_open = false;
    }

    std::filesystem::path filename() const { return m_filename; }

    void write_chunk(const void* chunk_data, std::size_t chunk_size, std::size_t frame_idx = 0U,
                     std::size_t channel_idx = 0U)
    {
    }

    void resize(hsize_t nb_frames) {}

    bool is_open = false;
    std::string m_filename;
};

} // namespace mock

BOOST_AUTO_TEST_CASE(test_task_io_multi_writer_frame_per_frame)
{
    io::saving_params settings{
        "/tmp", io::default_filename_format, "test", 0, ".h5", 1, io::file_exists_policy_enum::overwrite, 3};

    io::multi_writer<mock::writer> multi{settings, 0, 20};

    boost::gil::gray8_image_t img;

    multi.write_chunk(nullptr, 0, 0);
    multi.write_chunk(nullptr, 0, 1);
    multi.write_chunk(nullptr, 0, 2);
    multi.write_chunk(nullptr, 0, 3);
    multi.write_chunk(nullptr, 0, 7);
    multi.write_chunk(nullptr, 0, 19);
}

BOOST_AUTO_TEST_CASE(test_task_io_multi_writer_sequence)
{
    io::saving_params settings{
        "/tmp", io::default_filename_format, "test", 0, ".h5", 1, io::file_exists_policy_enum::overwrite, 100};

    io::multi_writer<mock::writer> multi{settings, 0, 1000};

    boost::gil::gray8_image_t img;

    for (std::size_t i = 0; i < 1000; i++)
        multi.write_chunk(nullptr, 0, i);
}
