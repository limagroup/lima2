// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <string>
#include <vector>

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/io/h5/nexus.hpp>
#include <lima/io/h5/writer.hpp>

namespace io = lima::io;
namespace h5 = lima::io::h5;

BOOST_AUTO_TEST_CASE(test_hdf5_no_metadata_writer)
{
    const std::string filename = "test_no_metadata.h5";

    h5::saving_params params;
    params.nb_frames_per_file = 3;
    h5::writer m(filename, params, {{1024, 1024}, 1, lima::pixel_enum::gray8s});

    h5::file out = h5::file::open(filename);

    //out.open_group(params.nx_entry_name).open_group(params.nx_instrument_name);
}

BOOST_AUTO_TEST_CASE(test_hdf5_nx_metadata_writer)
{
    const std::string filename = "test_nx_metadata.h5";

    h5::saving_params params;
    params.nb_frames_per_file = 3;
    h5::writer m(filename, params, {{1024, 1024}, 1, lima::pixel_enum::gray8s}, h5::nx_metadata_writer());

    h5::file out = h5::file::open(filename);

    //out.open_group(params.nx_entry_name).open_group(params.nx_instrument_name);
}
