// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <vector>

#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/io/hdf5.hpp>

#include "frame_fixture.hpp"

BOOST_FIXTURE_TEST_CASE(test_hdf5_hl, frame_fixture)
{
    lima::io::h5_write_frame("test_hl_raw.h5", input_frame);

    lima::io::h5_zip_write_frame("test_hl_zip.h5", input_frame);

    lima::io::h5_bshuf_lz4_write_frame("test_hl_bshuf_lz4.h5", input_frame);
}
