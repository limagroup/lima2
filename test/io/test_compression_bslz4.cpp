// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <random>

#include <boost/test/unit_test.hpp>

#include <lima/io/compression/bshuf_lz4.hpp>

namespace gil = boost::gil;
namespace comp = lima::io::comp;

BOOST_AUTO_TEST_CASE(test_bslz4)
{
    std::vector<float> input(1024);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(1.0, 2.0);

    std::generate(input.begin(), input.end(), [&dis, &gen]() { return dis(gen); });

    comp::bshuf_lz4_compression lz4;

    comp::buffers_t out_buffers;
    lz4.compression(&input[0], sizeof(float) * input.size(), sizeof(float), out_buffers);
}
