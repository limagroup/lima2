// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <random>

#include <boost/test/unit_test.hpp>

#include <lima/io/compression/zip.hpp>

namespace gil = boost::gil;
namespace comp = lima::io::comp;

BOOST_AUTO_TEST_CASE(test_zip)
{
    std::vector<int> input(1024);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1.0, 2.0);

    std::generate(input.begin(), input.end(), [&dis, &gen]() { return dis(gen); });

    std::vector<std::byte> compressed_output;
    comp::zip_compression::compression(input.data(), sizeof(int) * input.size(), compressed_output);

    std::vector<int> output(1024);
    comp::zip_decompression::decompression(compressed_output, output.data(), sizeof(int) * output.size());

    BOOST_CHECK_EQUAL_COLLECTIONS(input.begin(), input.end(), output.begin(), output.end());
}

BOOST_AUTO_TEST_CASE(test_zip_stream)
{
    std::vector<int> input(1024);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1.0, 2.0);

    std::generate(input.begin(), input.end(), [&dis, &gen]() { return dis(gen); });

    comp::zip_compression zip;

    comp::buffers_t compressed_buffers;
    zip.compression(input.data(), sizeof(int) * input.size(), compressed_buffers);
    zip.end_compression(compressed_buffers);

    // Concatenate all buffers into one to use decompress
    std::vector<std::byte> compressed_output;
    comp::concatenate(compressed_buffers, compressed_output);

    std::vector<int> output(1024);
    comp::zip_decompression::decompression(compressed_output, output.data(), sizeof(int) * output.size());

    BOOST_CHECK_EQUAL_COLLECTIONS(input.begin(), input.end(), output.begin(), output.end());
}