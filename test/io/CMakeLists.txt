# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_executable(test_io
    main.cpp
    test_multi.cpp
)

target_precompile_headers(test_io
    REUSE_FROM lima_core)

target_link_libraries(test_io
    lima_core
    Boost::unit_test_framework
)

add_test(NAME test_io COMMAND test_io)

add_executable(test_io_compression
    main.cpp
    test_compression_zlib.cpp
    test_compression_bslz4.cpp
)

target_precompile_headers(test_io_compression
    REUSE_FROM lima_core)

target_link_libraries(test_io_compression
    lima_core
    Boost::unit_test_framework
)

add_test(NAME test_io_compression COMMAND test_io_compression)

add_executable(test_io_edf
    main.cpp
    test_edf.cpp
)

target_precompile_headers(test_io_edf
    REUSE_FROM lima_core)

target_link_libraries(test_io_edf
    lima_core
    Boost::unit_test_framework
)

add_test(NAME test_io_edf COMMAND test_io_edf WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

if (LIMA_ENABLE_HDF5)
add_executable(test_io_hdf5
    main.cpp
    test_hdf5_wrapper.cpp
    test_hdf5_hl.cpp
    test_hdf5_writer.cpp
    test_hdf5_writer_metadata.cpp
    test_hdf5_reader.cpp
)

target_precompile_headers(test_io_hdf5
    REUSE_FROM lima_core)

target_link_libraries(test_io_hdf5
    lima_core
    Boost::unit_test_framework
)
endif (LIMA_ENABLE_HDF5)
