// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <string>
#include <vector>

#include <boost/test/unit_test.hpp>

#include <lima/io/h5/wrapper.hpp>

using namespace std::string_literals;

BOOST_AUTO_TEST_CASE(test_hdf5_path)
{
    namespace h5 = lima::io::h5;

    {
        h5::path entry("entry");
        std::string data("data");

        auto subpath = entry / "measurements" / data;
        BOOST_CHECK_EQUAL(subpath.c_str(), "entry/measurements/data");
    } // namespace lima::io::h5;

    using namespace h5::path_literals;

    {
        auto path = "root"_p / "measurements";
        BOOST_CHECK_EQUAL(path.c_str(), "root/measurements");
    }

    {
        auto path = "root"_p / "/redundant/separator";
        BOOST_CHECK_EQUAL(path.c_str(), "root/redundant/separator");
    }
}

BOOST_AUTO_TEST_CASE(test_hdf5_wrapper)
{
    namespace h5 = lima::io::h5;
    using namespace h5::path_literals;

    auto f = h5::file::create("test_wrapper.h5");
    f.attrs.create("NX_class", "NXroot"s);

    auto g1 = h5::group::create(f, "root");
    g1.attrs.create("NX_class", "NXroot"s);
    g1.attrs["NX_class"] = "NXentry"s;
    BOOST_CHECK_EQUAL(g1.attrs["NX_class"].string(), "NXentry"s);

    auto g2 = h5::group::create(g1, "nested1");
    auto g3 = h5::group::create(f, "root"_p / "nested2");

    auto ds1 = h5::dataset::create(g1, "outer", H5T_NATIVE_FLOAT);
    auto ds2 = h5::dataset::create(g2, "inner", H5T_NATIVE_INT);

    g1.create_dataset("outer2", 1.0f);
    g3.create_dataset("inner2", 1);
}
