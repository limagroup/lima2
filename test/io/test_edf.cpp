// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <lima/core/frame.hpp>
#include <lima/processing/typedefs.hpp>
#include <lima/io/edf.hpp>

namespace gil = boost::gil;

BOOST_AUTO_TEST_CASE(test_edf)
{
    lima::frame input_image;
    lima::io::edf_read_image("diffraction.edf", input_image);
}
