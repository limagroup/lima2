// Copyright (C) 2018 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <type_traits>

#include <boost/test/unit_test.hpp>

#include <boost/gil/algorithm.hpp>

#include "hdf5_file_fixture.hpp"

#include <lima/io/hdf5.hpp>

namespace io = lima::io;
namespace h5 = lima::io::h5;
namespace gil = boost::gil;

bool frame_equal(lima::frame const& f1, lima::frame const& f2)
{
    if ((f1.pixel_type() != f2.pixel_type()) || (f1.dimensions() != f2.dimensions()))
        return false;
    auto v1 = const_view(f1);
    auto v2 = const_view(f2);
    return boost::variant2::visit(
        [&v2](auto&& rv1) {
            using V = std::decay_t<decltype(rv1)>;
            BOOST_ASSERT(boost::variant2::holds_alternative<V>(v2));
            auto&& rv2 = boost::variant2::get<V>(v2);
            BOOST_ASSERT(rv1.size() == rv2.size());
            return std::equal(rv1.begin(), rv1.end(), rv2.begin(), [](auto&& p1, auto&& p2) { return p1 == p2; });
        },
        v1);
}

BOOST_FIXTURE_TEST_CASE(test_frame_equal, frame_fixture)
{
    BOOST_ASSERT(frame_equal(input_frame, input_frame));
}

BOOST_FIXTURE_TEST_CASE(test_hdf5_reader_raw, hdf5_file_fixture)
{
    h5::reader r(file_name);
    auto file_frame = r.read_frame(0);
    BOOST_ASSERT(frame_equal(file_frame, input_frame));
}
