// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <string>
#include <vector>

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/core/frame_view.hpp>
#include <lima/io/hdf5.hpp>
#include <lima/io/compression/zip.hpp>
#include <lima/processing/typedefs.hpp>

#include "frame_fixture.hpp"

namespace io = lima::io;
namespace h5 = lima::io::h5;

BOOST_FIXTURE_TEST_CASE(test_hdf5_writer_raw, frame_fixture)
{
    h5::saving_params params;
    params.nb_frames_per_file = 3;
    h5::writer m("test_writer_raw.h5", params,
                 {input_frame.dimensions(), input_frame.nb_channels(), input_frame.pixel_type()});
    m.write_view(lima::const_view(input_frame), 0);
    m.write_view(lima::const_view(input_frame), 1);
    m.write_view(lima::const_view(input_frame), 2);
}

BOOST_FIXTURE_TEST_CASE(test_hdf5_writer_zip, frame_fixture)
{
    h5::saving_params params;
    params.nb_frames_per_file = 3;
    params.compression = h5::compression_enum::zip;
    h5::writer m("test_writer_zip.h5", params,
                 {input_frame.dimensions(), input_frame.nb_channels(), input_frame.pixel_type()});
    m.write_view(lima::const_view(input_frame), 0);
    m.write_view(lima::const_view(input_frame), 1);
    m.write_view(lima::const_view(input_frame), 2);
}

BOOST_FIXTURE_TEST_CASE(test_hdf5_writer_bshuf_lz4, frame_fixture)
{
    h5::saving_params params;
    params.nb_frames_per_file = 3;
    params.compression = h5::compression_enum::bshuf_lz4;
    h5::writer m("test_writer_bshuf_lz4.h5", params,
                 {input_frame.dimensions(), input_frame.nb_channels(), input_frame.pixel_type()});
    m.write_view(lima::const_view(input_frame), 0);
    m.write_view(lima::const_view(input_frame), 1);
    m.write_view(lima::const_view(input_frame), 2);
}

BOOST_AUTO_TEST_CASE(test_hdf5_writer_sparse)
{
    const lima::point_t dimensions{2048, 2048};
    const int nb_frames = 100;
    const int nb_bins = 80;
    const int nb_frames_per_chunk = 10;

    std::vector<float> background_avg(nb_frames_per_chunk * nb_bins, 1.0f);
    std::vector<float> background_std(nb_frames_per_chunk * nb_bins, 0.2f);
    std::vector<std::uint32_t> frame_idx;
    std::vector<std::uint32_t> pixel_idx;
    std::vector<float> pixel_val;

    io::comp::zip_compression comp;
    std::vector<std::byte> background_avg_comp_buffer;
    std::vector<std::byte> background_std_comp_buffer;

    comp.compression(background_avg.data(), background_avg.size() * sizeof(float), background_avg_comp_buffer);

    comp.compression(background_std.data(), background_std.size() * sizeof(float), background_std_comp_buffer);

    //TODO
}
