// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <chrono>
#include <future>

#include <boost/blank.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/core/enums.describe.hpp>

#include <lima/hw/acquisition_fsm.hpp>

#include "fsm_log_fixture.hpp"

using namespace std::chrono_literals;

namespace mock
{
namespace
{
    struct acq_params
    {
        bool expect_throw = false;
        // If true, detector will call error during run to send the fsm to fault
        bool expect_run_error = false;
        std::chrono::milliseconds prepare_duration = 10ms;
    };

    struct config
    {
        using data_t = int;
        using init_params_t = boost::blank;
        using acq_params_t = acq_params;
        using acq_info_t = boost::blank;
    };

    struct detector : lima::hw::acquisition_fsm<detector, config>
    {
        using acq_params_t = config::acq_params_t;
        using acq_info_t = config::acq_info_t;

        config::acq_info_t acq_prepare(acq_params_t const& acq_params)
        {
            std::cout << "prepare" << std::endl;

            if (acq_params.expect_throw)
                throw std::runtime_error("oupsy");

            if (acq_params.expect_run_error)
                call_on_error = true;
            else
                call_on_error = false;

            std::this_thread::sleep_for(acq_params.prepare_duration);

            return {};
        }

        void acq_start()
        {
            std::cout << "start" << std::endl;
            if (call_on_error)
                on_error();
        }
        void acq_stop() { std::cout << "stop" << std::endl; }
        void acq_close() { std::cout << "close" << std::endl; }
        void acq_reset() { std::cout << "reset" << std::endl; }

        void register_on_error(std::function<void()> cbk)
        {
            std::cout << "register_on_error" << std::endl;
            on_error = cbk;
        }

        std::function<void()> on_error;

        bool call_on_error = false;
    };
} // namespace
} //namespace mock

using namespace lima;

BOOST_AUTO_TEST_SUITE(s, *boost::unit_test::fixture<fsm_log_fixture>(lima::log::app_domain::acq))

// idle + prepare -> prepared
// prepared + start -> running
// running + close -> idle
BOOST_AUTO_TEST_CASE(test_acquisition_fsm_nominal)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    det.prepare(mock::acq_params{});
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    det.start();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::running);

    det.close();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
}

// Idle FSM stays idle on stop or reset
// idle + stop  -> idle
// idle + reset -> idle
BOOST_AUTO_TEST_CASE(test_acquisition_fsm_idle)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
    det.stop();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
    det.reset();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
}

// Unhandled transition
// prepared + close throws an exception
BOOST_AUTO_TEST_CASE(test_acquisition_fsm_unhandled)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
    // idle + prepare -> prepared
    det.prepare(mock::acq_params{});

    // In prepared state
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    // prepared + close is not a valid transition
    // c.f. acquisition_fsm::process_event for exception signature
    BOOST_CHECK_EXCEPTION(det.close(), lima::runtime_error, [](const lima::runtime_error& ex) {
        return strcmp(ex.what(), "Processing event failed") == 0;
    });

    // State is unchanged
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    // Try a valid transition to check that m_eptr value was properly reset
    BOOST_CHECK_NO_THROW(det.start());

    det.close();
}

// Exception thrown in detector::acq_prepare
BOOST_AUTO_TEST_CASE(test_acquisition_fsm_throw)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    // c.f. acquisition_fsm::process_event for exception signature
    BOOST_CHECK_EXCEPTION(
        det.prepare(mock::acq_params{.expect_throw = true}), lima::runtime_error,
        [](const lima::runtime_error& ex) { return strcmp(ex.what(), "Exception during state transition") == 0; });

    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    // Retry prepare with no throw to check that m_eptr value was properly reset
    BOOST_CHECK_NO_THROW(det.prepare(mock::acq_params{.expect_throw = false}));
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);
}

// Error during run
BOOST_AUTO_TEST_CASE(test_acquisition_fsm_run_error)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    det.prepare(mock::acq_params{.expect_run_error = true});

    // Detector will call acquisition_fsm registered error callback during run,
    // setting the state to fault
    det.start();

    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::fault);

    // Check fault + reset -> idle
    det.reset();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    // Try again without the error
    det.prepare(mock::acq_params{.expect_run_error = false});
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    det.start();

    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::running);
}

// Run start in another thread while prepare is executing
BOOST_AUTO_TEST_CASE(test_acquisition_fsm_thread_safety)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    auto fut = std::async([&det]() {
        std::this_thread::sleep_for(100ms);

        det.start();
    });

    det.prepare(mock::acq_params{.prepare_duration = 1s});

    fut.get();

    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::running);
}

BOOST_AUTO_TEST_SUITE_END()