// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <lima/logging.hpp>

struct fsm_log_fixture
{
    fsm_log_fixture(lima::log::channel_name domain)
    {
        BOOST_TEST_MESSAGE("setup logging");

        // Configure log to facilitate debugging
        lima::log::init();
        lima::log::add_console_log();
        lima::log::set_filter(lima::log::severity_level::trace, domain);
    }
};
