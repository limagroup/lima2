// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <chrono>
#include <future>

#include <boost/blank.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/core/enums.describe.hpp>

#include <lima/hw/control_fsm.hpp>

#include "fsm_log_fixture.hpp"

using namespace std::chrono_literals;

namespace mock
{
namespace
{
    struct acq_params
    {
        struct _acq
        {
            lima::trigger_mode_enum trigger_mode;
        } acq;

        bool expect_throw = false;
        std::chrono::milliseconds prepare_duration = 10ms;
    };

    struct config
    {
        using init_params_t = boost::blank;
        using acq_params_t = acq_params;
    };

    struct detector : lima::hw::control_fsm<detector, config>
    {
        using init_params_t = config::init_params_t;
        using acq_params_t = config::acq_params_t;

        bool hw_validate_acq_params(acq_params_t const& acq_params) const { return true; }
        void hw_prepare(acq_params_t const& acq_params) const
        {
            std::cout << "prepare" << std::endl;
            if (acq_params.expect_throw) {
                throw std::runtime_error(":/");
            }

            std::this_thread::sleep_for(acq_params.prepare_duration);
        }
        void hw_start() { std::cout << "start" << std::endl; }
        void hw_trigger() { std::cout << "trigger" << std::endl; }
        void hw_stop() { std::cout << "stop" << std::endl; }
        void hw_close() { std::cout << "close" << std::endl; }
        void hw_reset() { std::cout << "reset" << std::endl; }

        bool run_error = false;
    };

} //namespace
} //namespace mock

using namespace lima;

BOOST_AUTO_TEST_SUITE(s, *boost::unit_test::fixture<fsm_log_fixture>(lima::log::app_domain::ctl))

BOOST_AUTO_TEST_CASE(test_control_noexcept)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    det.prepare(mock::acq_params{{trigger_mode_enum::internal}});
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    det.start();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::running);

    auto predicate = [](std::runtime_error const& ex) {
        return std::strcmp(ex.what(), "Processing event failed") == 0;
    };
    BOOST_CHECK_EXCEPTION(det.trigger(), std::runtime_error, predicate);

    det.close();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
}

BOOST_AUTO_TEST_CASE(test_control_trigger)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    det.prepare(mock::acq_params{{trigger_mode_enum::software}});
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    det.start();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::running);

    det.trigger();

    det.close();
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
}

// Unhandled transition
// prepared + close throws an exception
BOOST_AUTO_TEST_CASE(test_control_fsm_unhandled)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);
    // idle + prepare -> prepared
    det.prepare(mock::acq_params{{trigger_mode_enum::internal}});

    // In prepared state
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    // prepared + close is not a valid transition
    // c.f. acquisition_fsm::process_event for exception signature
    BOOST_CHECK_EXCEPTION(det.close(), lima::runtime_error, [](const lima::runtime_error& ex) {
        return strcmp(ex.what(), "Processing event failed") == 0;
    });

    // State is unchanged
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);

    // Try a valid transition to check that m_eptr value was properly reset
    BOOST_CHECK_NO_THROW(det.start());

    det.close();
}

// Exception thrown in detector::acq_prepare
BOOST_AUTO_TEST_CASE(test_control_fsm_throw)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    // c.f. acquisition_fsm::process_event for exception signature
    BOOST_CHECK_EXCEPTION(
        det.prepare(mock::acq_params{.expect_throw = true}), lima::runtime_error,
        [](const lima::runtime_error& ex) { return strcmp(ex.what(), "Exception during state transition") == 0; });

    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    // Retry prepare with no throw to check that m_eptr value was properly reset
    BOOST_CHECK_NO_THROW(det.prepare(mock::acq_params{.expect_throw = false}));
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::prepared);
}

// Run start in another thread while prepare is executing
BOOST_AUTO_TEST_CASE(test_control_fsm_thread_safety)
{
    mock::detector det;
    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::idle);

    auto fut = std::async([&det]() {
        std::this_thread::sleep_for(100ms);

        det.start();
    });

    det.prepare(mock::acq_params{.prepare_duration = 1s});

    fut.get();

    BOOST_CHECK_EQUAL(det.state(), acq_state_enum::running);
}

BOOST_AUTO_TEST_SUITE_END()