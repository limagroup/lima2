// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/mpi/histogram.hpp>

namespace gil = boost::gil;

BOOST_AUTO_TEST_CASE(test_histogram)
{
    lima::processing::any_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    auto hist = lima::processing::mpi::histogram(gil::const_view(input_image));
}
