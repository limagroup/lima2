// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cassert>
#include <cstring>
#include <cstdint>

#include <algorithm>
#include <memory>

#include <boost/mpi.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/core/layout.hpp>
#include <lima/core/serialization/layout.hpp>

namespace mpi = boost::mpi;

template <typename T>
struct matrix2
{
    using value_type = T;
    using reference = T&;

    matrix2(int const dim[2]) : data_(std::make_unique<T[]>(dim[0] * dim[1]))
    {
        dim_[0] = dim[0];
        dim_[1] = dim[1];
    }

    reference operator()(int i, int j) const
    {
        assert((i < width()) && (j < height()));
        return data_[i + j * width()];
    }

    int size() const { return width() * height(); }
    operator T*() { return data_.get(); }

    int width() const { return dim_[0]; }
    int height() const { return dim_[1]; }

    void print()
    {
        for (int j = 0; j < dim_[1]; j++) {
            for (int i = 0; i < dim_[0]; i++) {
                std::cout << operator()(i, j) << ' ';
            }
            std::cout << std::endl;
        }
    }

    void fill(value_type v) { std::fill(data_.get(), data_.get() + size(), v); }

    int dim_[2];
    std::unique_ptr<T[]> data_ = nullptr;
};

BOOST_AUTO_TEST_CASE(test_gather)
{
    mpi::communicator world;

    using value_type = std::uint16_t; // Let's assume 16 bit grayscale pixel
    auto mpi_datatype = MPI_UNSIGNED_SHORT;

    int res;
    const int global_dim[] = {1024, 1024};
    const int local_dim[] = {256, 256};
    //const int global_dim[] = {16, 16};
    //const int local_dim[] = {4, 4};
    const int starts[] = {0, 0};

    const int grid_size[] = {global_dim[0] / local_dim[0], global_dim[1] / local_dim[1]};
    const int nb_receivers = grid_size[0] * grid_size[1]; // grid_size * grid_size

    assert(grid_size[0] == 4);
    assert(grid_size[1] == 4);

    //if (world.rank() == 0) {
    //    std::cout << "Attach debugger then press <ENTER> key to continue..." << std::endl;
    //    std::cin.get();
    //}

    world.barrier();

    if (world.size() != nb_receivers) {
        fprintf(stderr, "Only works with np=%d for now\n", nb_receivers);
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    // Define an MPI type for the partial images
    MPI_Datatype partial_image_type1, partial_image_type2;
    res = MPI_Type_create_subarray(2, global_dim, local_dim, starts, MPI_ORDER_C, mpi_datatype, &partial_image_type1);
    res = MPI_Type_create_resized(partial_image_type1, 0, local_dim[0] * sizeof(value_type),
                                  &partial_image_type2); //Resized image as a multiple of row width
    res = MPI_Type_commit(&partial_image_type2);

    matrix2<value_type> local(local_dim);
    local.fill(world.rank());

    if (world.rank() == 0) {
        lima::layout l = {};
        mpi::broadcast(world, l, 0);

        matrix2<value_type> global(global_dim);
        memset(&global(0, 0), world.rank(), global.size());

        // How many pieces of data everyone has, in units of blocks
        matrix2<int> recvcounts(grid_size);
        // The starting point of everyone's data
        matrix2<int> displs(grid_size);

        int disp = 0;
        for (int j = 0; j < grid_size[1]; j++) {
            for (int i = 0; i < grid_size[0]; i++) {
                recvcounts(i, j) = 1;
                displs(i, j) = disp;
                disp += 1;
            }
            disp += ((global.width() / grid_size[1]) - 1) * grid_size[1];
        }

        //printf("Displacement grid:\n");
        //displs.print();

        MPI_Gatherv(local, local.size(), mpi_datatype, global, recvcounts, displs, partial_image_type2, 0, world);

        //printf("Processed grid:\n");
        //global.print();

        for (int j = 0; j < grid_size[1]; j++) {
            for (int i = 0; i < grid_size[0]; i++) {
                for (int l = 0; l < local_dim[1]; l++) {
                    for (int k = 0; k < local_dim[0]; k++) {
                        int m = i * local_dim[0] + k;
                        int n = j * local_dim[1] + l;
                        BOOST_REQUIRE(m < global.width());
                        BOOST_REQUIRE(n < global.height());
                        int rank = i + grid_size[0] * j;
                        BOOST_REQUIRE(rank < world.size());
                        BOOST_REQUIRE(global(m, n) < world.size());
                        BOOST_CHECK_EQUAL(global(m, n), rank);
                    }
                }
            }
        }

    } else {
        // Get layout
        lima::layout l;
        mpi::broadcast(world, l, 0);

        MPI_Gatherv(local, local.size(), mpi_datatype, NULL, 0, 0, 0, 0, world);
    }

    MPI_Type_free(&partial_image_type2);

    world.barrier();
}
