// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <mpi.h>

struct mpi_fixture
{
    mpi_fixture()
    {
        BOOST_TEST_MESSAGE("Init MPI");
        MPI_Init(NULL, NULL);
    }
    ~mpi_fixture()
    {
        BOOST_TEST_MESSAGE("Finalize MPI");
        MPI_Finalize();
    }
};

BOOST_TEST_GLOBAL_FIXTURE(mpi_fixture);
