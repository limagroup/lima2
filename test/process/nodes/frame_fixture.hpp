// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <boost/gil/algorithm.hpp> // for copy_and_convert_pixels

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/processing/serial/generator.hpp>

// A fixture that generates a frame that is used as input for the tests
struct frame_fixture
{
    frame_fixture() : input_frame(2048, 2048, lima::pixel_enum::gray32f)
    {
        using namespace lima::processing::generator;

        BOOST_TEST_MESSAGE("setup frame fixture");

        auto dims = input_frame.dimensions();
        generate_gauss func(0, gauss_params{{{dims.x / 2., dims.y / 2., 128.0, 100}}, 0.0});

        generate_gauss_locator_t loc(lima::point_t(0, 0), lima::point_t(1, 1), func);
        generate_gauss_view_t src(input_frame.dimensions(), loc);

        boost::gil::copy_and_convert_pixels(src, lima::view(input_frame));
    }

    lima::frame input_frame;

    /// Returns a input_node that generate nb_frames
    auto input_node(tbb::flow::graph& g, int nb_frames)
    {
        return tbb::flow::input_node<lima::frame>(g, [this, nb_frames, idx = 0](tbb::flow_control& fc) mutable {
            auto res = input_frame;
            res.metadata.idx = idx;
            idx++;

            if (idx > nb_frames)
                fc.stop();

            return res;
        });
    }
};
