// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/nodes/roi_counters.hpp>

#include "frame_fixture.hpp"

BOOST_FIXTURE_TEST_CASE(test_roi_counters_node, frame_fixture)
{
    using namespace lima::processing;
    using namespace tbb;

    flow::graph g;

    const int nb_frames = 20;
    auto src = input_node(g, nb_frames);

    using roi_counters_node_t = lima::processing::roi_counters_node<lima::frame>;

    std::vector<lima::rectangle_t> rect_rois = {{{1, 1}, {254, 254}}};
    std::vector<lima::arc_t> arc_rois = {{{1024., 1024.}, 256., 512., 45., 135.}};

    roi_counters_node_t roi_counters(g, tbb::flow::serial, rect_rois, arc_rois);

    tbb::flow::make_edge(src, roi_counters);

    src.activate();
    g.wait_for_all();
}
