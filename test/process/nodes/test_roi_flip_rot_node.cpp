// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <lima/core/rectangle.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/nodes/roi_flip_rot.hpp>

#include "frame_fixture.hpp"

BOOST_FIXTURE_TEST_CASE(test_roi_flip_rot_node, frame_fixture)
{
    using namespace lima;
    using namespace lima::processing;
    using namespace tbb;

    flow::graph g;

    const int nb_frames = 4;
    auto src = input_node(g, nb_frames);

    using roi_flip_rot_node_t = roi_flip_rot_node<lima::frame>;

    roi_flip_rot_node_t roi_flip_rot(g, flow::unlimited, rectangle_t{{10, 10}, {50, 50}}, flip_t::none,
                                     rotation_t::none);

    flow::function_node printer(g, flow::serial, [](lima::frame const& frm) {
        std::cout << "Frame #" << frm.metadata.idx << " is transformed!" << std::endl;
        return flow::continue_msg{};
    });

    flow::make_edge(src, roi_flip_rot);
    flow::make_edge(roi_flip_rot, printer);

    src.activate();
    g.wait_for_all();
}
