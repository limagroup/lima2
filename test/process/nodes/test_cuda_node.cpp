// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <exception>
#include <optional>
#include <string>
#include <thread>

#include <cuda/cuda.h>

#include <tbb/flow_graph.h>
#include <tbb/concurrent_queue.h>

template <typename Input>
class async_cuda_node : public tbb::flow::async_node<Input, Input>
{
    using parent_t = tbb::flow::async_node<Input, Input>;
    using input_t = typename parent_t::input_type;
    using output_t = typename parent_t::output_type;
    using gateway_t = typename parent_t::gateway_type;

    struct settings
    {
        std::string module_name;
        std::string class_name;
        std::string params;

        template <typename Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            ar& module_name;
            ar& class_name;
            ar& params;
        }
    };

    struct work
    {
        input_t input;
        gateway_t* gateway;
    };

  public:
    // An asynchronous activity, here calling cuda
    class activity
    {
      public:
        activity(std::string worker_path, settings const& settings) :
            m_service_thread([this, worker_path, settings]() {
                try {
                    // Init the cuda kernel and prepare it for processing

                    bool end_of_work = false;
                    while (!end_of_work) {
                        work w;
                        while (m_work_queue.try_pop(w)) {
                            end_of_work = w.input.is_final;

                            // Send the input to the GPU
                            // Input is in w.input.data

                            // Wait for the processing to be finished

                            // Get the result from the GPU

                            if (result.get())
                                // Send the result back to the graph
                                w.gateway->try_put(w.input);
                            // Signal that work is done
                            w.gateway->release_wait();
                        }
                    }

                } catch (std::exception& ex) {
                    LIMA_LOG(error, proc) << ex.what();
                    m_error = std::current_exception();
                }
            })
        {
        }

        ~activity() { m_service_thread.join(); }

        void submit(const input_t& v, gateway_t& gateway)
        {
            if (m_error)
                std::rethrow_exception(m_error);

            work w = {v, &gateway};
            gateway.reserve_wait();
            m_work_queue.push(w);
        }

      private:
        tbb::concurrent_queue<work> m_work_queue;
        std::thread m_service_thread;
        std::exception_ptr m_error;
    };

  public:
    using settings_t = settings;
    using activity_t = activity;
    using worker_t = worker;

    async_cuda_node(tbb::flow::graph& g, activity_t& activity) :
        parent_t(g, tbb::flow::serial, [&](const input_t& v, gateway_t& gateway) { activity.submit(v, gateway); })
    {
    }
};

// Let's model a frame as a 1D vector for simplicity
template <typename T>
struct frame
{
    int width;
    int height;

    std::vector<T> data;

    bool is_final = false; // A flag to tell the processing graph that the frame is the last one in the sequence
};

int main(int argc, char* argv[])
{
    using namespace tbb;

    // define the frame and frame ptr types
    using frame_t = frame<std::uint8_t>;
    using frame_ptr_t = std::shared_ptr<frame_t>;

    // Create the graph
    flow::graph g;

    // Create the source node
    const int nb_frames = 10000;
    tbb::flow::input_node<frame_ptr_t> src(
        g,
        [nb_frames, idx = 0](frame_ptr_t& out) mutable {
            if (idx < nb_frames) {
                // This is obvioulsy slow, frame_t should be pre-allocated
                out = std::make_shared<frame_t>(512 * 512);
                out->is_final = (idx == (nb_frames - 1));
                idx++;
                return true;
            } else
                return false;
        },
        false);

    // Create the cuda node
    cuda_node<frame_ptr_t> cuda(g, /*parameters*/);

    // Link them
    tbb::flow::make_edge(src, cuda);

    // Run
    src.activate();
    g.wait_for_all();
}
