// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <filesystem>

#include <boost/test/unit_test.hpp>

#include <lima/core/frame.hpp>
#include <lima/core/frame_view.hpp>
#include <lima/hw/info.hpp>
#include <lima/hw/params.hpp>
#include <lima/processing/typedefs.hpp>
#include <lima/processing/nodes/io_hdf5.hpp>

#include "frame_fixture.hpp"

BOOST_FIXTURE_TEST_CASE(test_io_hdf5_node, frame_fixture)
{
    using namespace std::chrono_literals;

    tbb::flow::graph g;

    const int nb_frames = 500;
    auto src = input_node(g, nb_frames);

    lima::io::h5::saving_params params{".",
                                       lima::io::default_filename_format,
                                       "test",
                                       0,
                                       ".h5",
                                       0,
                                       lima::io::file_exists_policy_enum::overwrite,
                                       100,
                                       10,
                                       lima::io::h5::compression_enum::bshuf_lz4};

    // Build Nx metadata
    lima::hw::info det_info = {.plugin = "Dectris",
                               .model = "Eiger2 4M CdTe",
                               .name = "lima2_eiger2",
                               .sn = "007",
                               .pixel_size = {35, 35},
                               .dimensions = {2048, 2048}};

    lima::hw::acquisition_params acq_params = {.nb_frames = 1,
                                               .expo_time = 1s,
                                               .latency_time = 1ms,
                                               .trigger_mode = lima::trigger_mode_enum::software,
                                               .nb_frames_per_trigger = 1};

    lima::processing::io_hdf5_node<lima::frame> saving(
        g, params, {input_frame.dimensions(), input_frame.nb_channels(), input_frame.pixel_type()},
        {acq_params, det_info});

    tbb::flow::make_edge(src, saving);

    src.activate();
    g.wait_for_all();
}

BOOST_FIXTURE_TEST_CASE(test_io_hdf5_sparse_node, frame_fixture)
{
    using namespace std::chrono_literals;

    tbb::flow::graph g;

    const int nb_frames = 500;
    auto src = input_node(g, nb_frames);

    lima::io::h5::saving_params params{".",
                                       lima::io::default_filename_format,
                                       "test",
                                       0,
                                       ".h5",
                                       0,
                                       lima::io::file_exists_policy_enum::overwrite,
                                       100,
                                       10,
                                       lima::io::h5::compression_enum::bshuf_lz4};

    // Build Nx metadata
    lima::hw::info det_info = {.plugin = "Dectris",
                               .model = "Eiger2 4M CdTe",
                               .name = "lima2_eiger2",
                               .sn = "007",
                               .pixel_size = {35, 35},
                               .dimensions = {2048, 2048}};

    lima::hw::acquisition_params acq_params = {.nb_frames = 1,
                                               .expo_time = 1s,
                                               .latency_time = 1ms,
                                               .trigger_mode = lima::trigger_mode_enum::software,
                                               .nb_frames_per_trigger = 1};

    lima::processing::io_hdf5_node<lima::frame> saving(
        g, params, {input_frame.dimensions(), input_frame.nb_channels(), input_frame.pixel_type()},
        {acq_params, det_info});

    tbb::flow::make_edge(src, saving);

    src.activate();
    g.wait_for_all();
}
