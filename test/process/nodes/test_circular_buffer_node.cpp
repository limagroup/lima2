// Copyright (C) 2020 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/nodes/circular_buffer.hpp>

#include "frame_fixture.hpp"

BOOST_FIXTURE_TEST_CASE(test_circular_buffer_node, frame_fixture)
{
    using namespace tbb;

    flow::graph g;

    const int nb_frames = 20;
    auto src = input_node(g, nb_frames);

    using circular_buffer_t = lima::processing::ts_circular_buffer<lima::frame>;
    using circular_buffer_node_t = lima::processing::circular_buffer_node<lima::frame>;

    circular_buffer_t circular_buffer(10);
    circular_buffer_node_t circular_buffer_node(g, tbb::flow::serial, circular_buffer);

    tbb::flow::make_edge(src, circular_buffer_node);

    src.activate();
    g.wait_for_all();
}
