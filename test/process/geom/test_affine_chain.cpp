// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <algorithm>
#include <array>
#include <random>
#include <string_view>
#include <variant>

#define BOOST_TEST_MODULE process_affine
#include <boost/test/unit_test.hpp>

#include <boost/format.hpp>
#include <boost/hana.hpp>
#include <boost/gil.hpp>
#include <boost/gil/extension/dynamic_image/algorithm.hpp>
#include <boost/gil/extension/dynamic_image/any_image_view.hpp>
#include <boost/gil/extension/dynamic_image/image_view_factory.hpp>
#include <boost/gil/extension/numeric/affine.hpp>
//#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/rational.hpp>

#include <lima/core/point.hpp>
#include <lima/core/rectangle.hpp>
#include <lima/core/io.hpp>

#include <lima/processing/geom/affine.hpp>
#include <lima/processing/geom/point_arithmetic.hpp>
#include <lima/processing/serial/binning.hpp>

#include <lima/utils/type_name.hpp>

namespace gil = boost::gil;

using rational_t = boost::rational<std::ptrdiff_t>;
using matrix_t = gil::matrix3x2<rational_t>;
using point_t = gil::point<std::ptrdiff_t>;
using rational_point_t = gil::point<rational_t>;
using rectangle_t = lima::rectangle<std::ptrdiff_t>;
using rational_rectangle_t = lima::rectangle<rational_t>;

struct rot90cw
{
    inline static const matrix_t affine = {0, 1, -1, 0, 0, 0};
    template <typename View>
    auto operator()(View const& v) const
    {
        return gil::rotated90cw_view(v);
    }
};

struct rot180
{
    inline static const matrix_t affine = {-1, 0, 0, -1, 0, 0};
    template <typename View>
    auto operator()(View const& v) const
    {
        return gil::rotated180_view(v);
    }
};

struct rot90ccw
{
    inline static const matrix_t affine = {0, -1, 1, 0, 0, 0};
    template <typename View>
    auto operator()(View const& v) const
    {
        return gil::rotated90ccw_view(v);
    }
};

struct vflip
{
    inline static const matrix_t affine = {1, 0, 0, -1, 0, 0};
    template <typename View>
    auto operator()(View const& v) const
    {
        return gil::flipped_up_down_view(v);
    }
};

struct hflip
{
    inline static const matrix_t affine = {-1, 0, 0, 1, 0, 0};
    template <typename View>
    auto operator()(View const& v) const
    {
        return gil::flipped_left_right_view(v);
    }
};

struct bin2
{
    inline static const matrix_t affine = {{1, 2}, 0, 0, {1, 2}, 0, 0};

    bin2() : buffer(0, 0) {}

    template <typename View>
    auto operator()(View const& v) const
    {
        buffer.recreate(v.dimensions() / 2);
        auto dst = View{gil::view(buffer)};
        lima::processing::binning(v, dst, 2u);
        return View{gil::view(buffer)};
    }

    mutable gil::gray8_image_t buffer;
};

struct bin4
{
    inline static const matrix_t affine = {{1, 4}, 0, 0, {1, 4}, 0, 0};

    bin4() : buffer(0, 0) {}

    template <typename View>
    auto operator()(View const& v) const
    {
        buffer.recreate(v.dimensions() / 4);
        auto dst = View{gil::view(buffer)};
        lima::processing::binning(v, dst, 4u);
        return View{gil::view(buffer)};
    }

    mutable gil::gray8_image_t buffer;
};

namespace detail
{
template <typename T, size_t I>
struct construct_alternative
{
    static T apply(int i)
    {
        if (i == I)
            return std::variant_alternative_t<I, T>{};
        else
            return construct_alternative<T, I - 1>::apply(i);
    }
};

template <typename T>
struct construct_alternative<T, 0>
{
    static T apply(int i) { return std::variant_alternative_t<0, T>{}; }
};

} //namespace detail

// Construct a variant according to the given index in the list of alternatives
template <typename T>
auto construct_alternative(std::size_t i)
{
    return detail::construct_alternative<T, std::variant_size_v<T> - 1>::apply(i);
}

auto point_to_rational(point_t const& p)
{
    return rational_point_t{p.x, p.y};
};

auto rectangle_to_rational(rectangle_t const& r)
{
    return rational_rectangle_t{point_to_rational(r.topleft), point_to_rational(r.dimensions)};
};

BOOST_AUTO_TEST_CASE(test_affine_chain)
{
    using any_xform_t = std::variant<rot90cw, rot180, rot90ccw, vflip, hflip, bin2, bin4>;

    std::array<any_xform_t, std::variant_size_v<any_xform_t>> all_xforms;
    std::generate(all_xforms.begin(), all_xforms.end(),
                  [n = 0]() mutable { return construct_alternative<any_xform_t>(++n); });

    for (int nb_try = 10; nb_try > 0; nb_try--) {
        // Generate a chain of transformation of 5 transformations out of all_xforms
        std::array<any_xform_t, 5> xforms;
        std::sample(all_xforms.begin(), all_xforms.end(), xforms.begin(), 5, std::mt19937{std::random_device{}()});

        // Generate the source image with ROI
        gil::gray8_image_t img(64, 128, gil::gray8_pixel_t{0});

        // We need mutliple of 8 (bin2 * bin4) else the reference results might not be correct
        point_t multiple{8, 8};
        auto p1 = gil::iround(img.dimensions() * (float) std::rand() / RAND_MAX) / multiple * multiple;
        auto p2 = gil::iround(img.dimensions() * (float) std::rand() / RAND_MAX) / multiple * multiple;
        auto src_roi = rectangle_t::from_corners(p1, p2);

        std::cout << "src_roi: " << src_roi.tl_corner() << ", " << src_roi.br_corner() << std::endl;

        gil::fill_pixels(gil::subimage_view(gil::view(img), src_roi.tl_corner(), src_roi.dimensions), 255);
        //tiff_write_view("image_0.tiff", gil::const_view(img));

        gil::any_image_view<boost::gil::gray8_view_t, boost::gil::gray8_step_view_t> view{gil::view(img)};

        // Apply the transformation chain
        int suffix = 1;
        for (auto&& xform : xforms) {
            std::visit([&view](auto&& x) { view = x(view); }, xform);

            //tiff_write_view((boost::format("image_%1%.tiff") % suffix).str(), view);
            //suffix++;
        }

        // Apply
        std::array<std::string, 5> names;
        std::transform(xforms.begin(), xforms.end(), names.begin(), [](auto&& xform) {
            return std::visit([](auto&& x) { return lima::type_name<std::decay_t<decltype(x)>>().substr(7); }, xform);
        });

        std::cout << "xforms: ";
        std::copy(names.begin(), names.end() - 1, std::ostream_iterator<std::string>(std::cout, " * "));
        std::cout << names.back() << std::endl;

        // Get dest ROI
        boost::variant2::visit(
            [&p1, &p2](auto&& v) {
                // Find first pixel (top left)
                p1 = [&v]() -> point_t {
                    std::ptrdiff_t x, y;
                    for (y = 0; y < v.height(); ++y)
                        for (x = 0; x < v.width(); ++x)
                            if (v(x, y) > 0)
                                return {x, y};
                    return {x, y};
                }();

                // Find last pixel (bottom right)
                p2 = [&v]() -> point_t {
                    std::ptrdiff_t x, y;
                    for (y = v.height() - 1; y > 0; --y)
                        for (x = v.width() - 1; x > 0; --x)
                            if (v(x, y) > 0)
                                return {x, y};
                    return {x, y};
                }();
            },
            view);

        auto dst_roi = rectangle_t::from_corners(p1, p2);
        std::cout << "dst_roi: " << dst_roi.tl_corner() << ", " << dst_roi.br_corner() << std::endl;

        // Multiply affine transformation
        matrix_t m;
        for (auto&& xform : xforms) {
            std::visit(
                [&m](auto&& x) {
                    using xform_t = std::decay_t<decltype(x)>;
                    m *= xform_t::affine;
                },
                xform);
        }

        auto m2 = rot90cw::affine * vflip::affine * rot180::affine * bin2::affine * hflip::affine;

        // Compute pixel coordinate of point p in the transformed image
        auto transform = [](matrix_t const& xform, rational_point_t const& src_dimensions,
                            rational_point_t const& dst_dimensions, rational_point_t const& p) -> rational_point_t {
            // Offset from pixel coordinate to geometric coordinate
            rational_point_t pixel_center_offset{rational_t{1, 2}, rational_t{1, 2}};

            // Apply affine transformmation
            return gil::transform(xform, p + pixel_center_offset - src_dimensions / 2)
                   - gil::scale(xform, pixel_center_offset) + dst_dimensions / 2;
        };

        rational_point_t src_dimensions = point_to_rational(img.dimensions());
        rational_point_t dst_dimensions = gil::scale(m, point_to_rational(img.dimensions()));

        // Direct
        auto dst_p1 = transform(m, src_dimensions, dst_dimensions, point_to_rational(src_roi.tl_corner()));
        auto dst_p2 = transform(m, src_dimensions, dst_dimensions, point_to_rational(src_roi.br_corner()));

        auto computed_dst_roi = rectangle_t::from_corners(lima::rational_cast<std::ptrdiff_t>(dst_p1),
                                                          lima::rational_cast<std::ptrdiff_t>(dst_p2));
        BOOST_CHECK_EQUAL(dst_roi, computed_dst_roi);

        // Inverse
        auto src_p1 =
            transform(gil::inverse(m), dst_dimensions, src_dimensions, point_to_rational(dst_roi.tl_corner()));
        auto src_p2 =
            transform(gil::inverse(m), dst_dimensions, src_dimensions, point_to_rational(dst_roi.br_corner()));

        auto computed_src_roi = rational_rectangle_t::from_corners(src_p1, src_p2);
        BOOST_CHECK_EQUAL(rectangle_to_rational(src_roi), computed_src_roi);
    }
}
