# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_executable(test_process_geom
    main.cpp
    #test_transformation.cpp
    test_isometric.cpp
)

target_link_libraries(test_process_geom
    lima_core
    Boost::unit_test_framework
)

add_test(NAME test_process_geom COMMAND test_process_geom)

add_executable(test_process_affine
    test_affine.cpp
)

target_link_libraries(test_process_affine
    lima_core
    Boost::unit_test_framework
)

add_test(NAME test_process_affine
    COMMAND test_process_affine
)

add_executable(test_process_affine_chain
    test_affine_chain.cpp
)

target_link_libraries(test_process_affine_chain
    lima_core
    Boost::unit_test_framework
)

add_test(NAME test_process_affine_chain
    COMMAND test_process_affine_chain
)