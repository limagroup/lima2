// Copyright (C) 2019 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <variant>

#include <lima/processing/geom/affine.hpp>
#include <lima/processing/geom/transformation.hpp>

using namespace lima::processing::geom;

// Typedefs
using integral_t = std::ptrdiff_t;
using rational_t = boost::rational<integral_t>;
using point_t = boost::gil::point<integral_t>;
using affine_t = boost::gil::matrix3x2<rational_t>;

BOOST_AUTO_TEST_CASE(test_concepts)
{
    BOOST_CHECK_EQUAL(concepts::has_affine_v<bin>, true);
    BOOST_CHECK_EQUAL(concepts::has_output_dimensions_v<rotated90ccw>, false);
}

BOOST_AUTO_TEST_CASE(test_basic_transformation)
{
    point_t size{2048, 2048};
    bin bin({2, 4});
    BOOST_CHECK_EQUAL(size / bin.factor, point_t(1024, 512));
    BOOST_CHECK_THROW(bin.check_input_dimensions({3, 3}), std::invalid_argument);
    point_t binned_size = output_dimensions(bin, size);
    BOOST_CHECK_EQUAL(binned_size, size / bin.factor);

    affine_t bin_affine = affine(bin);
    BOOST_CHECK_EQUAL(rational_cast<integral_t>(point_t(512, 512) * bin_affine), point_t(256, 128));

    roi roi({256, 256}, {512, 256});
    point_t roi_size = output_dimensions(roi /*, binned_size*/);
    BOOST_CHECK_EQUAL(roi_size, point_t(512, 256));

    affine_t roi_affine = affine(roi);
    point_t offset(10, 20);
    point_t src_offset = roi.rect.tl_corner() + offset;
    BOOST_CHECK_EQUAL(rational_cast<integral_t>(src_offset * roi_affine), offset);
    BOOST_CHECK_EQUAL(src_offset, rational_cast<integral_t>(offset * inverse(roi_affine)));

    affine_t bin_roi_affine = bin_affine * roi_affine;
    src_offset = src_offset * bin.factor;
    BOOST_CHECK_EQUAL(rational_cast<integral_t>(src_offset * bin_roi_affine), offset);
    BOOST_CHECK_EQUAL(src_offset, rational_cast<integral_t>(offset * inverse(bin_roi_affine)));

    flipped_up_down vflip;
    flipped_left_right hflip;
    point_t vflip_size = output_dimensions(vflip, roi_size);
    point_t hflip_size = output_dimensions(hflip, vflip_size);
    BOOST_CHECK((hflip_size == vflip_size) && (vflip_size == roi_size));
    affine_t bin_roi_flip_affine = (bin_roi_affine * affine(vflip) * affine(hflip));
    offset = roi_size - point_t(1, 1) - offset;
    BOOST_CHECK_EQUAL(rational_cast<integral_t>(src_offset * bin_roi_flip_affine), offset);
    BOOST_CHECK_EQUAL(src_offset, rational_cast<integral_t>(offset * inverse(bin_roi_flip_affine)));

    rotated90ccw rot90;
    point_t rot90_size = output_dimensions(rot90, hflip_size);
    BOOST_CHECK_EQUAL(rot90_size, point_t(hflip_size.y, hflip_size.x));
    affine_t brf_rot90_affine = (bin_roi_flip_affine * affine(rot90));
    offset = point_t(offset.y, rot90_size.y - 1 - offset.x);
    BOOST_CHECK_EQUAL(rational_cast<integral_t>(src_offset * brf_rot90_affine), offset);
    BOOST_CHECK_EQUAL(src_offset, rational_cast<integral_t>(offset * inverse(brf_rot90_affine)));
}

BOOST_AUTO_TEST_CASE(test_geometry_transformation_chain)
{
    using xform_t =
        std::variant<nop, bin, roi, flipped_up_down, flipped_left_right, rotated90ccw, rotated180, rotated90cw>;

    point_t image_size{2048, 2048};
    std::vector<xform_t> seq = {bin({2, 4}), roi({256, 256}, {512, 256}), flipped_up_down(), flipped_left_right(),
                                rotated90ccw()};

    affine_t seq_affine;
    for (auto t : seq) {
        std::visit([&seq_affine](auto&& x) { seq_affine *= affine(x); }, t);
    }

    //auto dims = boost::gil::scale(seq_affine, to_rational(image_size));

    point_t offset(10, 20);
    point_t offset_before_rot(512 - 1 - offset.y, offset.x);
    point_t src_offset = {(256 + (512 - 1 - offset_before_rot.x)) * 2, (256 + (256 - 1 - offset_before_rot.y)) * 4};

    BOOST_CHECK_EQUAL(rational_cast<integral_t>(src_offset * seq_affine), offset);
    BOOST_CHECK_EQUAL(src_offset, rational_cast<integral_t>(offset * inverse(seq_affine)));
}