// Copyright (C) 2019 Alejandro Homs Puron, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <lima/processing/geom/affine.hpp>
#include <lima/processing/geom/isometric.hpp>
#include <lima/core/rectangle.hpp>

using namespace lima::processing::geom;

// Typedefs
using integral_t = std::ptrdiff_t;
using point_t = boost::gil::point<integral_t>;
using rectangle_t = lima::rectangle<integral_t>;
using rational_t = boost::rational<integral_t>;
using matrix_t = boost::gil::matrix3x2<rational_t>;

BOOST_AUTO_TEST_CASE(test_isometric_transformation_get_reverse)
{
#define IS_REVERSE_COND(x, y) (std::is_same_v<decltype(inverse(std::declval<x>())), y>)

#define CHECK_REVERSE(x, y)     BOOST_CHECK((IS_REVERSE_COND(x, y)))
#define CHECK_NOT_REVERSE(x, y) BOOST_CHECK(!(IS_REVERSE_COND(x, y)))

    CHECK_REVERSE(isometric_xform_none, isometric_xform_none);
    CHECK_REVERSE(isometric_xform_vflip, isometric_xform_vflip);
    CHECK_REVERSE(isometric_xform_hflip, isometric_xform_hflip);
    CHECK_REVERSE(isometric_xform_rot90ccw, isometric_xform_rot90cw);
    CHECK_REVERSE(isometric_xform_rot180, isometric_xform_rot180);
    CHECK_REVERSE(isometric_xform_rot90cw, isometric_xform_rot90ccw);
    CHECK_REVERSE(isometric_xform_vflip_rot90ccw, isometric_xform_vflip_rot90ccw);
    CHECK_REVERSE(isometric_xform_hflip_rot90ccw, isometric_xform_hflip_rot90ccw);

    CHECK_NOT_REVERSE(isometric_xform_none, isometric_xform_vflip);
    CHECK_NOT_REVERSE(isometric_xform_hflip, isometric_xform_rot180);
    CHECK_NOT_REVERSE(isometric_xform_vflip_rot90ccw, isometric_xform_hflip_rot90ccw);

#undef IS_REVERSE_COND
#undef CHECK_NOT_REVERSE
#undef CHECK_REVERSE
}

BOOST_AUTO_TEST_CASE(test_isometric_transformation_is_reverse)
{
#define CHECK_REVERSE(x, y)     BOOST_CHECK((concepts::is_reverse(x(), y())))
#define CHECK_NOT_REVERSE(x, y) BOOST_CHECK(!(concepts::is_reverse(x(), y())))

    CHECK_REVERSE(isometric_xform_none, isometric_xform_none);
    CHECK_REVERSE(isometric_xform_vflip, isometric_xform_vflip);
    CHECK_REVERSE(isometric_xform_hflip, isometric_xform_hflip);
    CHECK_REVERSE(isometric_xform_rot90ccw, isometric_xform_rot90cw);
    CHECK_REVERSE(isometric_xform_rot180, isometric_xform_rot180);
    CHECK_REVERSE(isometric_xform_rot90cw, isometric_xform_rot90ccw);
    CHECK_REVERSE(isometric_xform_vflip_rot90ccw, isometric_xform_vflip_rot90ccw);
    CHECK_REVERSE(isometric_xform_hflip_rot90ccw, isometric_xform_hflip_rot90ccw);

    CHECK_NOT_REVERSE(isometric_xform_none, isometric_xform_vflip);
    CHECK_NOT_REVERSE(isometric_xform_hflip, isometric_xform_rot180);
    CHECK_NOT_REVERSE(isometric_xform_vflip_rot90ccw, isometric_xform_hflip_rot90ccw);

#undef CHECK_NOT_REVERSE
#undef CHECK_REVERSE
}

#define ISO_XFORM_1 isometric_xform_none
#define ISO_XFORM_2 isometric_xform_vflip
#define ISO_XFORM_3 isometric_xform_hflip
#define ISO_XFORM_4 isometric_xform_rot90ccw
#define ISO_XFORM_5 isometric_xform_rot180
#define ISO_XFORM_6 isometric_xform_rot90cw
#define ISO_XFORM_7 isometric_xform_vflip_rot90ccw
#define ISO_XFORM_8 isometric_xform_hflip_rot90ccw

BOOST_AUTO_TEST_CASE(test_isometric_transformation_addition)
{
    using namespace concepts;

#define CHECK_ADD(x, y, z) BOOST_CHECK(decltype(std::declval<x>() + std::declval<y>() == std::declval<z>())::value)

    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_1, ISO_XFORM_1);
    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_2, ISO_XFORM_2);
    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_3, ISO_XFORM_3);
    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_4, ISO_XFORM_4);
    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_5, ISO_XFORM_5);
    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_6, ISO_XFORM_6);
    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_7, ISO_XFORM_7);
    CHECK_ADD(ISO_XFORM_1, ISO_XFORM_8, ISO_XFORM_8);

    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_1, ISO_XFORM_2);
    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_2, ISO_XFORM_1);
    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_3, ISO_XFORM_5);
    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_4, ISO_XFORM_7);
    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_5, ISO_XFORM_3);
    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_6, ISO_XFORM_8);
    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_7, ISO_XFORM_4);
    CHECK_ADD(ISO_XFORM_2, ISO_XFORM_8, ISO_XFORM_6);

    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_1, ISO_XFORM_3);
    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_2, ISO_XFORM_5);
    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_3, ISO_XFORM_1);
    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_4, ISO_XFORM_8);
    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_5, ISO_XFORM_2);
    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_6, ISO_XFORM_7);
    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_7, ISO_XFORM_6);
    CHECK_ADD(ISO_XFORM_3, ISO_XFORM_8, ISO_XFORM_4);

    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_1, ISO_XFORM_4);
    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_2, ISO_XFORM_8);
    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_3, ISO_XFORM_7);
    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_4, ISO_XFORM_5);
    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_5, ISO_XFORM_6);
    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_6, ISO_XFORM_1);
    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_7, ISO_XFORM_2);
    CHECK_ADD(ISO_XFORM_4, ISO_XFORM_8, ISO_XFORM_3);

    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_1, ISO_XFORM_5);
    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_2, ISO_XFORM_3);
    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_3, ISO_XFORM_2);
    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_4, ISO_XFORM_6);
    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_5, ISO_XFORM_1);
    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_6, ISO_XFORM_4);
    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_7, ISO_XFORM_8);
    CHECK_ADD(ISO_XFORM_5, ISO_XFORM_8, ISO_XFORM_7);

    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_1, ISO_XFORM_6);
    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_2, ISO_XFORM_7);
    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_3, ISO_XFORM_8);
    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_4, ISO_XFORM_1);
    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_5, ISO_XFORM_4);
    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_6, ISO_XFORM_5);
    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_7, ISO_XFORM_3);
    CHECK_ADD(ISO_XFORM_6, ISO_XFORM_8, ISO_XFORM_2);

    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_1, ISO_XFORM_7);
    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_2, ISO_XFORM_6);
    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_3, ISO_XFORM_4);
    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_4, ISO_XFORM_3);
    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_5, ISO_XFORM_8);
    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_6, ISO_XFORM_2);
    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_7, ISO_XFORM_1);
    CHECK_ADD(ISO_XFORM_7, ISO_XFORM_8, ISO_XFORM_5);

    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_1, ISO_XFORM_8);
    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_2, ISO_XFORM_4);
    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_3, ISO_XFORM_6);
    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_4, ISO_XFORM_2);
    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_5, ISO_XFORM_7);
    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_6, ISO_XFORM_3);
    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_7, ISO_XFORM_5);
    CHECK_ADD(ISO_XFORM_8, ISO_XFORM_8, ISO_XFORM_1);

#undef CHECK_ADD
}

#undef ISO_XFORM_8
#undef ISO_XFORM_7
#undef ISO_XFORM_6
#undef ISO_XFORM_5
#undef ISO_XFORM_4
#undef ISO_XFORM_3
#undef ISO_XFORM_2
#undef ISO_XFORM_1
