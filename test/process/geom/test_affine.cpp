// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_TEST_MODULE process_affine
#include <boost/test/unit_test.hpp>

#include <boost/rational.hpp>
#include <boost/gil/point.hpp>
#include <boost/gil/extension/numeric/affine.hpp>

#include <lima/processing/geom/affine.hpp>

namespace gil = boost::gil;

namespace boost
{
namespace gil
{
    template <typename T>
    std::ostream& boost_test_print_type(std::ostream& os, const point<T>& p)
    {
        os << "x: " << p.x << ", y:" << p.y;
        return os;
    }

    template <typename T, typename D>
    BOOST_FORCEINLINE auto operator/(point<boost::rational<T>> const& p, D d) -> point<boost::rational<T>>
    {
        return {p.x / d, p.y / d};
    }

} // namespace gil
} // namespace boost

BOOST_AUTO_TEST_CASE(test_affine_transformation)
{
    using matrix_t = gil::matrix3x2<double>;
    using point_t = gil::point<std::ptrdiff_t>;

    matrix_t m;
    point_t p;
    gil::point<double> res, expected;

    m = matrix_t::get_scale(2);
    p = point_t(10, 10);

    res = gil::transform(m, p);
    expected = {20., 20.};
    BOOST_CHECK_EQUAL(res, expected);

    m = matrix_t::get_translate(0., 10.);

    res = gil::transform(m, p);
    expected = {10., 20.};
    BOOST_CHECK_EQUAL(res, expected);

    m = matrix_t::get_rotate(3.14159265358979323846 / 2);

    res = gil::transform(m, p);
    expected = {-10., 10.};
    BOOST_CHECK_EQUAL(res, expected);
}

BOOST_AUTO_TEST_CASE(test_matrix_inv)
{
    using matrix_t = gil::matrix3x2<double>;
    using point_t = gil::point<double>;

    // Origin change
    matrix_t mo = matrix_t::get_translate(0, 16);

    // Basis change
    matrix_t mb = matrix_t::get_rotate(3.14159265358979323846 / 2);

    auto m = mo * mb;

    point_t p(10, 10);
    point_t q = gil::transform(gil::inverse(m), p);
    point_t p2 = gil::transform(m, q);

    BOOST_CHECK_EQUAL(p, p2);
}

BOOST_AUTO_TEST_CASE(test_transformation)
{
    using rational_t = boost::rational<std::ptrdiff_t>;
    using matrix_t = gil::matrix3x2<rational_t>;
    using point_t = gil::point<rational_t>;

    constexpr rational_t pi{3126535, 995207};

    // Affine transformations
    matrix_t rot90cw = {0, 1, -1, 0, 0, 0};
    matrix_t rot180 = {-1, 0, 0, -1, 0, 0};
    matrix_t rot90ccw = {0, -1, 1, 0, 0, 0};
    matrix_t vflip = {1, 0, 0, -1, 0, 0};
    matrix_t hflip = {-1, 0, 0, 1, 0, 0};
    matrix_t bin2 = {{1, 2}, 0, 0, {1, 2}, 0, 0};

    // Easy use case with even dimensions
    point_t dimensions = {10, 20};
    point_t rot90_dimensions = {20, 10};
    point_t bin2_dimensions = {5, 10};

    BOOST_CHECK_EQUAL(gil::scale(rot180, dimensions), dimensions);
    BOOST_CHECK_EQUAL(gil::scale(rot90cw, dimensions), rot90_dimensions);
    BOOST_CHECK_EQUAL(gil::scale(rot90ccw, dimensions), rot90_dimensions);
    BOOST_CHECK_EQUAL(gil::scale(vflip, dimensions), dimensions);
    BOOST_CHECK_EQUAL(gil::scale(hflip, dimensions), dimensions);
    BOOST_CHECK_EQUAL(gil::scale(bin2, dimensions), bin2_dimensions);

    // Compute pixel coordinate of point p in the transformed image
    auto transform = [](matrix_t const& xform, point_t const& src_dimensions, point_t const& dst_dimensions,
                        point_t const& p) -> point_t {
        // Offset from pixel coordinate to geometric coordinate
        point_t pixel_center_offset = point_t{rational_t{1, 2}, rational_t{1, 2}};

        // Apply affine transformmation
        return gil::transform(xform, p + pixel_center_offset - src_dimensions / 2)
               - gil::scale(xform, pixel_center_offset) + dst_dimensions / 2;
    };

    // This is our source point for the rest of this test
    point_t p = {5, 5};

    // Single transformations
    BOOST_CHECK_EQUAL(transform(rot90cw, dimensions, rot90_dimensions, p), (point_t{14, 5}));
    BOOST_CHECK_EQUAL(transform(rot180, dimensions, dimensions, p), (point_t{4, 14}));
    BOOST_CHECK_EQUAL(transform(rot90ccw, dimensions, rot90_dimensions, p), (point_t{5, 4}));
    BOOST_CHECK_EQUAL(transform(vflip, dimensions, dimensions, p), (point_t{5, 14}));
    BOOST_CHECK_EQUAL(transform(hflip, dimensions, dimensions, p), (point_t{4, 5}));
    BOOST_CHECK_EQUAL(transform(bin2, dimensions, bin2_dimensions, p), (point_t{{5, 2}, {5, 2}}));

    // Composed transformations
    BOOST_CHECK_EQUAL(transform(vflip * rot90cw, dimensions, gil::scale(vflip * rot90cw, dimensions), p),
                      (point_t{5, 5}));
    BOOST_CHECK_EQUAL(transform(hflip * rot180, dimensions, gil::scale(hflip * rot180, dimensions), p),
                      (point_t{5, 14}));
    BOOST_CHECK_EQUAL(
        transform(hflip * bin2 * rot90ccw, dimensions, gil::scale(hflip * bin2 * rot90ccw, dimensions), p),
        (point_t{{5, 2}, {5, 2}}));
    BOOST_CHECK_EQUAL(transform(rot90cw * vflip * rot180 * bin2 * hflip, dimensions,
                                gil::scale(rot90cw * vflip * rot180 * bin2 * hflip, dimensions), p),
                      (point_t{7, {5, 2}}));

    auto m = rot90cw * vflip * rot180 * bin2 * hflip;

    // More challenging use case with odd dimensions
    dimensions = {7, 9};
    rot90_dimensions = {9, 7};

    BOOST_CHECK_EQUAL(gil::scale(rot90cw, dimensions), rot90_dimensions);
    BOOST_CHECK_EQUAL(gil::scale(rot90ccw, dimensions), rot90_dimensions);

    // Single transformations
    BOOST_CHECK_EQUAL(transform(rot90cw, dimensions, rot90_dimensions, p), (point_t{3, 5}));
    BOOST_CHECK_EQUAL(transform(rot180, dimensions, dimensions, p), (point_t{1, 3}));
    BOOST_CHECK_EQUAL(transform(rot90ccw, dimensions, rot90_dimensions, p), (point_t{5, 1}));
    BOOST_CHECK_EQUAL(transform(vflip, dimensions, dimensions, p), (point_t{5, 3}));
    BOOST_CHECK_EQUAL(transform(hflip, dimensions, dimensions, p), (point_t{1, 5}));

    // Composed transformations
    BOOST_CHECK_EQUAL(transform(vflip * rot90cw, dimensions, gil::scale(vflip * rot90cw, dimensions), p),
                      (point_t{5, 5}));
    BOOST_CHECK_EQUAL(transform(hflip * rot180, dimensions, gil::scale(hflip * rot180, dimensions), p),
                      (point_t{5, 3}));
}

BOOST_AUTO_TEST_CASE(test_inverse_transformation)
{
    using rational_t = boost::rational<std::ptrdiff_t>;
    using matrix_t = gil::matrix3x2<rational_t>;
    using point_t = gil::point<rational_t>;

    constexpr rational_t pi{3126535, 995207};

    // Affine transformations
    matrix_t rot90cw = {0, 1, -1, 0, 0, 0};
    matrix_t rot180 = {-1, 0, 0, -1, 0, 0};
    matrix_t rot90ccw = {0, -1, 1, 0, 0, 0};
    matrix_t vflip = {1, 0, 0, -1, 0, 0};
    matrix_t hflip = {-1, 0, 0, 1, 0, 0};
    matrix_t bin2 = {{1, 2}, 0, 0, {1, 2}, 0, 0};

    // Compute pixel coordinate of point p in the original image
    auto transform = [](matrix_t const& xform, point_t const& src_dimensions, point_t const& dst_dimensions,
                        point_t const& p) -> point_t {
        // Offset from pixel coordinate to geometric coordinate
        point_t pixel_center_offset = point_t{rational_t{1, 2}, rational_t{1, 2}};

        // Apply affine transformmation
        return gil::transform(xform, p + pixel_center_offset - src_dimensions / 2)
               - gil::scale(xform, pixel_center_offset) + dst_dimensions / 2;
    };

    point_t dimensions = {10, 20};
    point_t rot90_dimensions = {20, 10};
    point_t bin2_dimensions = {5, 10};

    // Inverse
    BOOST_CHECK_EQUAL(transform(gil::inverse(rot90cw), rot90_dimensions, dimensions, point_t{14, 5}), (point_t{5, 5}));
    BOOST_CHECK_EQUAL(transform(gil::inverse(bin2), bin2_dimensions, dimensions, point_t{{5, 2}, {5, 2}}),
                      (point_t{5, 5}));
}
