// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>

namespace gil = boost::gil;

BOOST_AUTO_TEST_CASE(test_transpose)
{
    gil::gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    auto transposed = gil::transposed_view(gil::const_view(input_image));

    gil::tiff_write_view("lena_grayscale_transpose.tiff", transposed);
}

BOOST_AUTO_TEST_CASE(test_rotate_cw90)
{
    gil::gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    auto rotated = gil::rotated90cw_view(gil::const_view(input_image));

    gil::tiff_write_view("lena_grayscale_rotate_cw90.tiff", rotated);
}

BOOST_AUTO_TEST_CASE(test_rotate_ccw90)
{
    gil::gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    auto rotated = gil::rotated90ccw_view(gil::const_view(input_image));

    gil::tiff_write_view("lena_grayscale_rotate_ccw90.tiff", rotated);
}

BOOST_AUTO_TEST_CASE(test_rotate_cw90_v2)
{
    gil::gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    gil::gray8_image_t output_image(input_image.dimensions());

    auto transposed = gil::transposed_view(gil::const_view(input_image));
    gil::copy_pixels(transposed, gil::view(output_image));

    gil::tiff_write_view("lena_grayscale_rotate_cw90_v2.tiff",
                         gil::flipped_left_right_view(gil::const_view(output_image)));
}

BOOST_AUTO_TEST_CASE(test_rotate_ccw90_v2)
{
    gil::gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    gil::gray8_image_t output_image(input_image.dimensions());

    auto transposed = gil::transposed_view(gil::const_view(input_image));
    gil::copy_pixels(transposed, gil::view(output_image));

    gil::tiff_write_view("lena_grayscale_rotate_ccw90_v2.tiff",
                         gil::flipped_up_down_view(gil::const_view(output_image)));
}