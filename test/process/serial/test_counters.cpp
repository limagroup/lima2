// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/serial/counters.hpp>

namespace gil = boost::gil;

BOOST_AUTO_TEST_CASE(test_counters)
{
    gil::gray16_image_t input_image(2048, 2048);
    boost::gil::generate_pixels(gil::view(input_image), [i = std::uint16_t{0}]() mutable {
        i++;
        return gil::gray16_pixel_t{i};
    });

    auto counters = lima::processing::counters(gil::const_view(input_image));

    BOOST_CHECK_EQUAL(counters.m_min, 0);
    BOOST_CHECK_EQUAL(counters.m_max, 65535);
    BOOST_CHECK_EQUAL(counters.m_count, 2048 * 2048);
    BOOST_CHECK_CLOSE(counters.m_avg, 32767.5, 0.001);
    BOOST_CHECK_CLOSE(counters.m_std, 18918.61361860324, 0.001);
}

BOOST_AUTO_TEST_CASE(test_counters_with_mask)
{
    gil::gray16_image_t input_image(2048, 2048);
    boost::gil::generate_pixels(gil::view(input_image), [i = std::uint16_t{0}]() mutable {
        i++;
        return gil::gray16_pixel_t{i};
    });

    lima::bpp8_image_t mask_image(2048, 2048);
    boost::gil::fill_pixels(gil::view(mask_image), 1);

    auto counters = lima::processing::counters(gil::const_view(input_image), gil::const_view(mask_image));

    BOOST_CHECK_EQUAL(counters.m_min, 0);
    BOOST_CHECK_EQUAL(counters.m_max, 65535);
    BOOST_CHECK_EQUAL(counters.m_count, 2048 * 2048);
    BOOST_CHECK_CLOSE(counters.m_avg, 32767.5, 0.001);
    BOOST_CHECK_CLOSE(counters.m_std, 18918.61361860324, 0.001);
}
