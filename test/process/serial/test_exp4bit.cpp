// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/align.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/serial/exp4bit.hpp>
#include <lima/processing/typedefs.hpp>

namespace gil = boost::gil;

typedef std::vector<unsigned char, boost::alignment::aligned_allocator<unsigned char, sizeof(__m256i)>> buffer_t;

const unsigned int chip_size = 256;

static void pack_pixel_depth_8bit_to_4bit(const buffer_t& src, buffer_t& dest)
{
    for (int i = 0; i < chip_size * chip_size; i += 2) {
        dest[i / 2] = (src[i] << 4) | src[i + 1];
    }
}

static void unpack_pixel_depth_4bit_to_8bit(const buffer_t& src, buffer_t& dest)
{
    for (int i = 0; i < chip_size * chip_size / 2; i++) {
        dest[i * 2] = src[i] >> 4;
        dest[i * 2 + 1] = src[i] & 0x0F;
    }
}

#if defined(__AVX2__)

BOOST_AUTO_TEST_CASE(test_exp4bit_avx2)
{
    buffer_t frame_8bit_in(chip_size * chip_size);
    buffer_t frame_8bit_out(chip_size * chip_size);
    buffer_t frame_4bit(chip_size * chip_size / 2);

    // Generate a frame
    for (unsigned int i = 0; i < chip_size * chip_size; i++) {
        //frame_8bit_in[i] = i % 0x10;
        frame_8bit_in[i] = std::rand() % 0x10;
    }

    // Pack the frame to 4 bit
    pack_pixel_depth_8bit_to_4bit(frame_8bit_in, frame_4bit);

    // Test reference method
    std::fill(frame_8bit_out.begin(), frame_8bit_out.end(), 0);
    unpack_pixel_depth_4bit_to_8bit(frame_4bit, frame_8bit_out);
    BOOST_TEST(std::equal(frame_8bit_in.begin(), frame_8bit_in.end(), frame_8bit_out.begin()));

    // Define an image type with an aligned memory allocator
    using gray8_image_t =
        gil::image<gil::gray8_pixel_t, false, boost::alignment::aligned_allocator<unsigned char, sizeof(__m256i)>>;
    gray8_image_t output_image(gil::point_t{chip_size, chip_size});

    // Test optimized method
    lima::processing::exp4bit_avx2(&frame_4bit[0], gil::view(output_image));
    BOOST_TEST(std::equal(frame_8bit_in.begin(), frame_8bit_in.end(), gil::const_view(output_image).row_begin(0)));
}

#else //defined(__AVX2__)

BOOST_AUTO_TEST_CASE(test_exp4bit_sse2)
{
    buffer_t frame_8bit_in(chip_size * chip_size);
    buffer_t frame_8bit_out(chip_size * chip_size);
    buffer_t frame_4bit(chip_size * chip_size / 2);

    // Generate a frame
    for (unsigned int i = 0; i < chip_size * chip_size; i++) {
        //frame_8bit_in[i] = i % 0x10;
        frame_8bit_in[i] = std::rand() % 0x10;
    }

    // Pack the frame to 4 bit
    pack_pixel_depth_8bit_to_4bit(frame_8bit_in, frame_4bit);

    // Test reference method
    std::fill(frame_8bit_out.begin(), frame_8bit_out.end(), 0);
    unpack_pixel_depth_4bit_to_8bit(frame_4bit, frame_8bit_out);
    BOOST_TEST(std::equal(frame_8bit_in.begin(), frame_8bit_in.end(), frame_8bit_out.begin()));

    // Define an image type with an aligned memory allocator
    using gray8_image_t =
        gil::image<gil::gray8_pixel_t, false, boost::alignment::aligned_allocator<unsigned char, sizeof(__m256i)>>;
    gray8_image_t output_image(gil::point_t{chip_size, chip_size});

    // Test optimized method
    lima::processing::exp4bit_sse2(&frame_4bit[0], gil::view(output_image));
    BOOST_TEST(std::equal(frame_8bit_in.begin(), frame_8bit_in.end(), gil::const_view(output_image).row_begin(0)));
}

#endif //defined(__AVX2__)