// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/core/frame_view.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/serial/mask.hpp>

namespace gil = boost::gil;

BOOST_AUTO_TEST_CASE(test_mask)
{
    lima::processing::any_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    lima::frame mask_frame(input_image.dimensions(), lima::pixel_enum::gray8);

    // Generate a masks
    gil::fill_pixels(lima::view(mask_frame), std::uint8_t(1));
    gil::fill_pixels(gil::subimage_view(lima::view(mask_frame), {128, 128}, {256, 256}), std::uint8_t(0));

    lima::processing::mask(gil::view(input_image), lima::const_view<lima::bpp8c_view_t>(mask_frame));

    tiff_write_view("lena_grayscale_masked.tiff", gil::const_view(input_image));
}
