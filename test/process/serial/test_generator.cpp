// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdlib> // For std::atof
#include <random>

#include <boost/test/unit_test.hpp>
#include <boost/gil/extension/dynamic_image/algorithm.hpp>

#include <lima/io/hdf5.hpp>
#include <lima/processing/serial/generator.hpp>

using namespace boost::unit_test;

template <typename OutputIterator, typename Size, typename IntType, typename Generator>
void unique_random_generate_n(OutputIterator out, Size count, IntType min, IntType max, Generator& gen)
{
    IntType last_k = min;
    IntType n = max;

    while (count > 0) {
        // Partition the range of numbers to generate from
        unsigned long long range_size = (n - last_k) / count;

        // Initialize random generator
        std::uniform_int_distribution<unsigned long long> rnd(min, range_size);

        // Generate random number
        unsigned long long r = rnd(gen) + last_k + 1;

        // Set last_k to r so that r is not generated again
        last_k = r;
        count--;

        *out = r;
    }
}

BOOST_AUTO_TEST_CASE(test_generator)
{
    BOOST_TEST_REQUIRE(framework::master_test_suite().argc > 5,
                       "Usage: test_generator <gauss_fwhm> <gauss_max> <noise_mean> <noise_std>");

    double gauss_fwhm = std::atof(framework::master_test_suite().argv[1]);
    double gauss_max = std::atof(framework::master_test_suite().argv[2]);
    double noise_mean = std::atof(framework::master_test_suite().argv[3]);
    double noise_std = std::atof(framework::master_test_suite().argv[4]);
    int nb_peaks = std::atoi(framework::master_test_suite().argv[5]);

    lima::frame input_frame(2048, 2048, lima::pixel_enum::gray32f);

    // Background diffraction
    using namespace lima::processing::generator;
    auto dims = input_frame.dimensions();
    generate_gauss func(0, {{{dims.x / 2., dims.y / 2., 128.0, 100}}, 0.0});

    // Background noise
    std::normal_distribution distribution(noise_mean, noise_std);

    // Generate (unique) peak locations
    std::random_device rd;
    std::mt19937_64 gen(rd());

    std::vector<std::size_t> peak_linear_idx;
    std::size_t min = 0;
    std::size_t max = input_frame.size() - 1;
    unique_random_generate_n(std::back_inserter(peak_linear_idx), nb_peaks, min, max, gen);
    for (auto&& idx : peak_linear_idx) {
        auto nb_rows = input_frame.dimensions().x;
        double x0 = idx % nb_rows;
        double y0 = idx / nb_rows;
        func.m_params.peaks.emplace_back(x0, y0, 3.0, 1000.0);
    }

    // Generate images
    boost::gil::copy_and_convert_pixels(make_generated_with_noise_view(input_frame.dimensions(), func, distribution),
                                        lima::view(input_frame));

    lima::io::h5_bshuf_lz4_write_frame("test_generator.h5", input_frame);
}
