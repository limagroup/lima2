// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/numeric/affine.hpp>
#include <boost/test/unit_test.hpp>

namespace gil = boost::gil;

namespace boost
{
namespace gil
{
    template <typename T>
    std::ostream& boost_test_print_type(std::ostream& os, const point<T>& p)
    {
        os << "x: " << p.x << ", y:" << p.y;
        return os;
    }

} // namespace gil
} // namespace boost

BOOST_AUTO_TEST_CASE(test_affine_transformation)
{
    using matrix_t = gil::matrix3x2<double>;
    using point_t = gil::point<std::ptrdiff_t>;

    //matrix_t m = matrix_t::get_scale(2);
    //point_t p(10, 10);

    matrix_t m;
    point_t p;
    gil::point<double> res, expected;

    m = matrix_t::get_scale(2);
    p = point_t(10, 10);

    res = gil::transform(m, p);
    expected = {20., 20.};
    BOOST_CHECK_EQUAL(res, expected);

    m = matrix_t::get_translate(0., 10.);

    res = gil::transform(m, p);
    expected = {10., 20.};
    BOOST_CHECK_EQUAL(res, expected);

    m = matrix_t::get_rotate(3.14159265358979323846 / 2);

    res = gil::transform(m, p);
    expected = {-10., 10.};
    BOOST_CHECK_EQUAL(res, expected);
}

template <typename T>
gil::matrix3x2<T> inv(gil::matrix3x2<T> m)
{
    T determinant = m.a * m.d - m.b * m.c;
    T invdet = 1 / determinant;

    gil::matrix3x2<T> res;
    res.a = m.d * invdet;
    res.b = -m.b * invdet;
    res.c = -m.c * invdet;
    res.d = m.a * invdet;
    res.e = -(m.d * m.e - m.c * m.f) * invdet;
    res.f = (m.b * m.e - m.a * m.f) * invdet;

    return res;
}

BOOST_AUTO_TEST_CASE(test_matrix_inv)
{
    using matrix_t = gil::matrix3x2<double>;
    using point_t = gil::point<double>;

    // Origin change
    matrix_t mo = matrix_t::get_translate(0, 16);

    // Basis change
    matrix_t mb = matrix_t::get_rotate(3.14159265358979323846 / 2);

    auto m = mo * mb;

    point_t p(10, 10);
    point_t q = gil::transform(inv(m), p);
    point_t p2 = gil::transform(m, q);

    BOOST_CHECK_EQUAL(p, p2);
}