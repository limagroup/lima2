// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <chrono>
#include <fstream>
#include <thread>

#include <transwarp.h>

#include <boost/gil/algorithm.hpp>
#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/serial/counters.hpp>
#include <lima/processing/serial/histogram.hpp>
//#include <lima/io/edf.hpp>
#include <lima/typedefs.hpp>

namespace tw = transwarp;
namespace gil = boost::gil;

void save_image_tiff_task(const lima::any_image_t& src)
{
    tiff_write_view("output.tif", gil::const_view(src));
}

lima::any_image_t flipped_left_right_view_task(const lima::any_image_t& src)
{
    lima::any_image_t res = src;

    gil::copy_pixels(flipped_up_down_view(const_view(src)), view(res));

    return res;
}

lima::any_image_t flipped_up_down_view_task(const lima::any_image_t& src)
{
    lima::any_image_t res = src;

    gil::copy_pixels(flipped_up_down_view(const_view(src)), view(res));

    return res;
}

lima::any_image_t rotated180_view_task(const lima::any_image_t& src)
{
    lima::any_image_t res = src;

    gil::copy_pixels(rotated180_view(const_view(src)), view(res));

    return res;
}

lima::any_image_t rotated90cw_view_task(const lima::any_image_t& src)
{
    lima::any_image_t res = src;

    gil::copy_pixels(rotated90cw_view(const_view(src)), view(res));

    return res;
}

lima::any_image_t rotated90ccw_view_task(const lima::any_image_t& src)
{
    lima::any_image_t res = src;

    gil::copy_pixels(rotated90ccw_view(const_view(src)), view(res));

    return res;
}

lima::any_image_t roicounter_view_task(const lima::any_image_t& src)
{
    lima::any_image_t res = src;

    gil::copy_pixels(rotated90ccw_view(const_view(src)), view(res));

    return res;
}

//lima::any_image_t subimage_view_task(const lima::any_image_t& src)
//{
//	lima::any_image_t res = src;
//
//	gil::copy_pixels(subimage_view(const_view(src)), view(res));
//
//	return res;
//}
//
//lima::any_image_t subsampled_view_task(const lima::any_image_t& src)
//{
//	lima::any_image_t res = src;
//
//	gil::copy_pixels(subsampled_view(const_view(src)), view(res));
//
//	return res;
//}

BOOST_AUTO_TEST_CASE(test_pipeline)
{
    try {
        lima::any_image_t input_image;
        tiff_read_image("lena_grayscale.tiff", input_image);

        // Building the task graph
        auto task1 = tw::make_value_task("lena", input_image);
        auto task2 = tw::make_task(tw::consume, "flip_left_right", flipped_left_right_view_task, task1);
        auto task3 = tw::make_task(tw::consume, "rotated90cw", rotated90cw_view_task, task2);
        auto out_task = tw::make_task(tw::consume, "save_image_tiff", save_image_tiff_task, task3);

        // Creating a dot-style graph for visualization
        const auto graph = task3->get_graph();
        std::ofstream("process.dot") << tw::to_string(graph);

        //tw::parallel executor{ 4 };
        tw::sequential executor;

        out_task->schedule_all(executor); // schedules all tasks for execution
        out_task->get();

    } catch (std::exception& ex) {
        std::cerr << "ERROR: " << ex.what() << std::endl;
    }
}
