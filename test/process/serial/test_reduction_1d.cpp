// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/serial/reduction_1d.hpp>

namespace gil = boost::gil;

namespace lima::processing
{
std::ostream& operator<<(std::ostream& os, counters_result const& rhs)
{
    os << rhs.m_count << " ";
    os << rhs.m_min << " ";
    os << rhs.m_max << " ";
    os << rhs.m_avg << " ";
    os << rhs.m_std << " ";
    os << rhs.m_sum << " ";

    return os;
}
} // namespace lima::processing

BOOST_AUTO_TEST_CASE(test_reduction_1d)
{
    gil::gray16_image_t input_image(2048, 2048);
    boost::gil::generate_pixels(gil::view(input_image), [i = std::uint16_t{0}]() mutable {
        i++;
        return gil::gray16_pixel_t{i};
    });

    auto profile = lima::processing::reduction_1d(gil::const_view(input_image));

    lima::processing::counters_result expected_profile{2048, 0, 65535, 32767.5, 18918.61361860324, 8.59843e+07};
    lima::processing::reduction_1d_result expected(input_image.height(), expected_profile);
    BOOST_CHECK_EQUAL_COLLECTIONS(profile.begin(), profile.end(), expected.begin(), expected.end());
}

// BOOST_AUTO_TEST_CASE(test_reduction_1d_with_mask)
// {
//     gil::gray16_image_t input_image(2048, 2048);
//     boost::gil::generate_pixels(gil::view(input_image), [i = std::uint16_t{0}]() mutable {
//         i++;
//         return gil::gray16_pixel_t{i};
//     });

//     lima::bpp8_image_t mask_image(2048, 2048);
//     boost::gil::fill_pixels(gil::view(mask_image), 1);

//     auto counters = lima::processing::reduction_1d(gil::const_view(input_image), gil::const_view(mask_image));

//     BOOST_CHECK_EQUAL(counters.m_min, 0);
//     BOOST_CHECK_EQUAL(counters.m_max, 65535);
//     BOOST_CHECK_EQUAL(counters.m_count, 2048 * 2048);
//     BOOST_CHECK_CLOSE(counters.m_avg, 32767.5, 0.001);
//     BOOST_CHECK_CLOSE(counters.m_std, 18918.61361860324, 0.001);
// }
