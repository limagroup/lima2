// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <cstdlib> // For std::atof
#include <random>

#include <boost/test/unit_test.hpp>

#include <lima/core/io.hpp>
#include <lima/core/frame_typedefs.hpp>
#include <lima/processing/serial/generator.hpp>
#include <lima/io/hdf5.hpp>

using namespace boost::unit_test;

BOOST_AUTO_TEST_CASE(test_arc_view)
{
    using namespace lima::processing::generator;

    lima::bpp8_image_t image(2048, 2048);

    lima::arc_t roi = {1024., 1024., 256., 512., 45., 135.};

    generate_arc generator(roi);

    // Generate arc image
    boost::gil::copy_and_convert_pixels(make_generated_view(image.dimensions(), generator), boost::gil::view(image));

    // Test several critical points
    auto res = boost::gil::const_view(image);

    BOOST_CHECK_EQUAL(res(0, 0), 0);
    BOOST_CHECK_EQUAL(res(2047, 2047), 0);
    BOOST_CHECK_EQUAL(res(1024, 1024 + 257), 1);
    BOOST_CHECK_EQUAL(res(1024, 1024 + 255), 0);
    BOOST_CHECK_EQUAL(res(1024, 1024 + 511), 1);
    BOOST_CHECK_EQUAL(res(1024, 1024 + 513), 0);
}
