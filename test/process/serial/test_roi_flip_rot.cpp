// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/serial/roi_flip_rot.hpp>

namespace gil = boost::gil;

BOOST_AUTO_TEST_CASE(test_roi_flip_rot)
{
    gil::gray8_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    using point_t = gil::point2<std::ptrdiff_t>;

    lima::rectangle_t roi = {{100, 100}, {312, 312}};

    auto res = lima::processing::roi_flip_rot(gil::const_view(input_image), roi, lima::processing::flip_t::left_right,
                                              lima::processing::rotation_t::cw90);

    gil::tiff_write_view("lena_grayscale_roi_flip_rot.tiff", res);
}

BOOST_AUTO_TEST_CASE(test_roi_flip_rot_dynamic)
{
    gil::any_image<gil::gray8_image_t, gil::gray16_image_t> input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    using point_t = gil::point2<std::ptrdiff_t>;

    lima::rectangle_t roi = {{100, 100}, {312, 312}};

    auto res = lima::processing::roi_flip_rot(gil::const_view(input_image), roi, lima::processing::flip_t::left_right,
                                              lima::processing::rotation_t::cw90);

    gil::tiff_write_view("lena_grayscale_roi_flip_rot_dynamic.tiff", res);
}
