// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/gil/extension/io/tiff/old.hpp>
#include <boost/test/unit_test.hpp>

#include <lima/processing/typedefs.hpp>
#include <lima/processing/serial/flatfield_correction.hpp>

namespace gil = boost::gil;

BOOST_AUTO_TEST_CASE(test_flatfield_correction)
{
    lima::processing::any_image_t input_image;
    tiff_read_image("lena_grayscale.tiff", input_image);

    gil::gray32f_image_t flatfield;
    tiff_read_image("flatfield.tiff", flatfield);

    gil::for_each_pixel(gil::view(flatfield), [](auto& p) { p[0] += 1.0f; });

    lima::processing::flatfield_correction(gil::view(input_image), gil::const_view(flatfield));

    tiff_write_view("lena_grayscale_ff_correction.tiff", gil::const_view(flatfield));
}
