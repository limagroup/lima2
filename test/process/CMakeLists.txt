# Copyright (C) 2018 Samuel Debionne, ESRF.

# Use, modification and distribution is subject to the Boost Software
# License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
# http://www.boost.org/LICENSE_1_0.txt)

add_subdirectory(geom)
add_subdirectory(serial)
if(LIMA_ENABLE_MPI)
    add_subdirectory(mpi)
endif(LIMA_ENABLE_MPI)
add_subdirectory(nodes)
