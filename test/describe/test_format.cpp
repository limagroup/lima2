// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <chrono>
#include <iostream>
#include <vector>

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/describe.hpp>
#include <fmt/lima.hpp>

#include <boost/describe.hpp>
#include <boost/describe/annotations.hpp>
#include <boost/mp11.hpp>
#include <boost/callable_traits/return_type.hpp>

#include <lima/core/rectangle.hpp>

namespace lima
{
enum class acq_mode_enum : int
{
    normal,      //!< Single image
    accumulation //!< Multiple image accumulated (over time)
};

BOOST_DESCRIBE_ENUM(acq_mode_enum, normal, accumulation)

using roi_t = rectangle<std::ptrdiff_t>;

struct acquisition
{
    int nb_frames = 1;                                              // POD
    std::chrono::duration<int> expo_time = std::chrono::seconds(1); // Class
    acq_mode_enum acq_mode = acq_mode_enum::normal;                 // Enumerator
    roi_t roi;                                                      // Described class
};

BOOST_DESCRIBE_STRUCT(acquisition, (), (nb_frames, expo_time, acq_mode, roi))
//BOOST_DESCRIBE_STRUCT(acquisition, (), (nb_frames, expo_time, acq_mode))

// clang-format off
BOOST_ANNOTATE_MEMBER(acquisition, nb_frames,
    (desc, "number of frames"),
    (doc, "The number of frames to acquire (0 = continuous acquisition)"))

BOOST_ANNOTATE_MEMBER(acquisition, expo_time,
    (desc, "exposure time"),
    (doc, "The exposure time [s]"))

BOOST_ANNOTATE_MEMBER(acquisition, acq_mode,
    (desc, "acquisition mode"),
    (doc, "The acquistion mode [normal, accumulation]"))

BOOST_ANNOTATE_MEMBER(acquisition, roi,
    (desc, "region of interest"),
    (doc, "The region of interest to transfer"))
// clang-format on

} // namespace lima

BOOST_AUTO_TEST_CASE(test_format)
{
    using namespace std::chrono_literals;
    using namespace lima;

    roi_t roi{{0, 0}, {1024, 1024}};
    acquisition acq{100, 1s, acq_mode_enum::accumulation, roi};

    static_assert(boost::describe::has_describe_members<acquisition>::value);

    std::cout << "\nFormat:\n";
    std::cout << fmt::format("class: {}, enum: {}", acq, acq_mode_enum::accumulation);
}
