// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <boost/describe.hpp>
#include <boost/json.hpp>
#include <boost/json/describe.hpp>

namespace test
{

struct base
{
    std::string m0 = "ABC";
};

BOOST_DESCRIBE_STRUCT(base, (), (m0))

struct bar : base
{
    int m1 = 1;
    double m2 = 2.2;
};

BOOST_DESCRIBE_STRUCT(bar, (base), (m1, m2))

} //namespace test

BOOST_AUTO_TEST_CASE(test_json)
{
    boost::json::value expected = {{"m0", "ABC"}, {"m1", 1}, {"m2", 2.2}};

    BOOST_CHECK_EQUAL(expected, boost::json::value_from(test::bar()));
}
