// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>
#include <boost/test/tools/output_test_stream.hpp>

#include <boost/describe/io_enums.hpp>

/// Reset level
enum class test_enum : int
{
    one,
    two,
    three
};

BOOST_DESCRIBE_ENUM(test_enum, one, two, three)

using boost::describe::operator<<;
using boost::describe::operator>>;

BOOST_AUTO_TEST_CASE(test_describe_enum)
{
    using boost::test_tools::output_test_stream;

    test_enum e = test_enum::one;

    output_test_stream output;
    output << e;

    BOOST_TEST(!output.is_empty(false));
    BOOST_TEST(output.check_length(3, false));
    BOOST_TEST(output.is_equal("one"));

    {
        auto e1 = boost::describe::string_to_enum<test_enum>("one");
        BOOST_CHECK_EQUAL(e1, test_enum::one);
        auto e2 = boost::describe::string_to_enum<test_enum>("two");
        BOOST_CHECK_EQUAL(e2, test_enum::two);
    }
    {
        auto e1 = boost::describe::string_to_enum_nothrow<test_enum>("two");
        BOOST_CHECK(e1);
        BOOST_CHECK_EQUAL(*e1, test_enum::two);
        auto e2 = boost::describe::string_to_enum_nothrow<test_enum>("three");
        BOOST_CHECK(e2);
        BOOST_CHECK_EQUAL(*e2, test_enum::three);
    }
}
