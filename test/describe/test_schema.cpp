// Copyright (C) 2018 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/test/unit_test.hpp>

#include <boost/describe/enum.hpp>
#include <boost/describe/annotations.hpp>
#include <boost/json/chrono.hpp>
#include <boost/json/schema_from.hpp>

#include <array>
#include <chrono>
#include <list>

namespace test
{

enum state_enum
{
    idle,
    ready,
    running,
    fault
};

BOOST_DESCRIBE_ENUM(state_enum, idle, ready, running, fault)

struct point
{
    int x = 0;
    int y = 0;
};

BOOST_DESCRIBE_STRUCT(point, (), (x, y))

// clang-format off
BOOST_ANNOTATE_MEMBER(point, x,
    (desc, "x"),
    (doc, "x coordinate"))

BOOST_ANNOTATE_MEMBER(point, y,
    (desc, "y"),
    (doc, "y coordinate"))
// clang-format on

struct base
{
    std::string m0 = "ABC";
};

BOOST_DESCRIBE_STRUCT(base, (), (m0))

// clang-format off
BOOST_ANNOTATE_MEMBER(base, m0,
    (desc, "m0"),
    (doc, "The m0 member"))
// clang-format on

using namespace std::chrono_literals;

struct bar : base
{
    int m1 = 1;
    double m2 = 2.2;
    point m3 = {2, 2};
    std::array<std::chrono::milliseconds, 2> m4 = {1ms, 2ms};
    bool m5 = true;
    state_enum m6 = state_enum::ready;
    std::vector<state_enum> m7 = {state_enum::idle, state_enum::fault};
    std::vector<point> m8 = {{1, 2}, {3, 4}};
};

BOOST_DESCRIBE_STRUCT(bar, (base), (m1, m2, m3, m4, m5, m6, m7, m8))

// clang-format off
BOOST_ANNOTATE_MEMBER(bar, m1,
    (desc, "m1"),
    (doc, "The m1 member"))

BOOST_ANNOTATE_MEMBER(bar, m2,
    (desc, "m2"),
    (doc, "The m2 member"))

BOOST_ANNOTATE_MEMBER(bar, m3,
    (desc, "m3"),
    (doc, "The m3 member"))

BOOST_ANNOTATE_MEMBER(bar, m4,
    (desc, "m4"),
    (doc, "The m4 member"))

BOOST_ANNOTATE_MEMBER(bar, m5,
    (desc, "m5"),
    (doc, "The m5 member"))

BOOST_ANNOTATE_MEMBER(bar, m6,
    (desc, "m6"),
    (doc, "The m6 member"))

BOOST_ANNOTATE_MEMBER(bar, m7,
    (desc, "m7"),
    (doc, "The m7 member"))

BOOST_ANNOTATE_MEMBER(bar, m8,
    (desc, "m8"),
    (doc, "The m8 member"))
// clang-format on

} //namespace test

BOOST_AUTO_TEST_CASE(test_schema)
{
    static_assert(boost::json::is_described_class2<test::bar>::value);

    // clang-format off
    boost::json::object expected = {
        {"type", "object"},
        {"properties", {
          {"m0", {{"type", "string"}, {"description", "The m0 member"}, {"default", "ABC"}}},
          {"m1", {{"type", "integer"}, {"description", "The m1 member"}, {"default", 1}}},
          {"m2", {{"type", "number"}, {"description", "The m2 member"}, {"default", 2.2E0}}},
          {"m3", {
            {"type", "object"},
            {"properties", {
              {"x", {{"type", "integer"}, {"description", "x coordinate"}, {"default", 2}}},
              {"y", {{"type", "integer"}, {"description", "y coordinate"}, {"default", 2}}},
            }},
            {"additionalProperties", false}
          }},
          {"m4", {
            {"type", "array"},
            {"items", {{"type", "integer"}, {"default", 0}, {"unit", "microseconds"}}},
            {"description", "The m4 member"},
            {"default", {1, 2}},
            {"minItems", 2},
            {"maxItems", 2}
          }},
          {"m5", {{"type", "boolean"}, {"description", "The m5 member"}, {"default", true}}},
          {"m6", {
            {"type", "string"},
            {"enum", {"idle", "ready", "running", "fault"}},
            {"description", "The m6 member"},
            {"default", "ready"}
          }},
          {"m7", {
            {"type", "array"},
            {"items", {
              {"type", "string"}, {"enum", {"idle", "ready", "running", "fault"}}, {"default", "idle"}}},
            {"description", "The m7 member"},
            {"default", {"idle", "fault"}}
          }},
          {"m8", {
            {"type", "array"},
            {"items", {
              {"type", "object"},
              {"properties", {
                {"x", {{"type", "integer"}, {"description", "x coordinate"}, {"default", 0}}},
                {"y", {{"type", "integer"}, {"description", "y coordinate"}, {"default", 0}}},
              }},
              {"additionalProperties", false}
            }},
            {"description", "The m8 member"},
            {"default", {
              {{"x", 1}, {"y", 2}},
              {{"x", 3}, {"y", 4}}
            }}
          }}
        }},
        {"additionalProperties", false},
        {"$schema", "http://json-schema.org/draft-06/schema"},
        {"$id", "https://esrf.fr/lima2.schema.json"},
        {"title", "bar"}};
    // clang-format on

    BOOST_CHECK_EQUAL(expected, boost::json::schema_from<test::bar>("bar", test::bar{}));
}
