import sys

print(sys.path)
print(sys.version)

import gil
import numpy as np

img = gil.Gray8Image(5, 5, 1)
print(img)

img.recreate(10, 10, 0, 1)
print(img)

view = gil.view(img)
print(view)

data = np.array(view, copy=False)
print(data)

anyview = gil.AnyImageView(view)

data = np.array(anyview, copy=False)
print(data)

data.fill(65)

gil.print(view)
