# LImA 2

This sandbox project revisits LImA with modern C++ technics and libraries with the hope that some of it transfuse to the LImA project at some point.

Checkout the [documentation here](https://limagroup.gitlab-pages.esrf.fr/lima2/).

### Bootstrapping the documentation

The source for the documentation is in the `docs` folder. Here are the instructions to built and read it locally. The documentation is built with [Doxygen](http://www.doxygen.org/) and [Sphinx](http://www.sphinx-doc.org). The sphinx template is from [ReadtheDocs](https://docs.readthedocs.io). [Breathe](https://breathe.readthedocs.io) provides a bridge between the Sphinx and Doxygen documentation systems.

```
    conda create -n doc --file docs/requirements-conda.txt -c conda-forge
    conda activate doc
    pip install -r docs/requirements-pip.txt
    cd docs  
    doxygen lima.dox
    make html
```

The html documentation is generated in `docs/.build/html`.
