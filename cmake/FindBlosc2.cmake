# Module for locating c-blosc2
#
# Read-only variables:
#   BLOSC2_FOUND
#     Indicates that the library has been found.
#
#   BLOSC2_INCLUDE_DIRS
#     Points to the libnuma include directory.
#
#   BLOSC2_LIBRARY_DIR
#     Points to the directory that contains the libraries.
#     The content of this variable can be passed to link_directories.
#
#   BLOSC2_LIBRARY
#     Points to the libnuma that can be passed to target_link_libararies.
#
# Copyright (c) 2020 Samuel Debionne

include(FindPackageHandleStandardArgs)

find_path(BLOSC2_INCLUDE_DIRS
  NAMES blosc2.h
  HINTS ${BLOSC2_ROOT_DIR}
  PATH_SUFFIXES include
  DOC "BLOSC2 include directory")

find_library(BLOSC2_LIBRARY
  NAMES blosc2
  HINTS ${BLOSC2_ROOT_DIR}
  DOC "BLOSC2 library")

if (BLOSC2_LIBRARY)
    get_filename_component(BLOSC2_LIBRARY_DIR ${BLOSC2_LIBRARY} PATH)
endif()

mark_as_advanced(BLOSC2_INCLUDE_DIRS BLOSC2_LIBRARY_DIR BLOSC2_LIBRARY)

find_package_handle_standard_args(Blosc2 REQUIRED_VARS BLOSC2_INCLUDE_DIRS BLOSC2_LIBRARY)

if(BLOSC2_FOUND)
    if(NOT TARGET Blosc2::Blosc2)
        add_library(Blosc2::Blosc2 SHARED IMPORTED)
    endif()
    if(BLOSC2_INCLUDE_DIRS)
        set_target_properties(Blosc2::Blosc2 PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${BLOSC2_INCLUDE_DIRS}")
    endif()
    if(EXISTS "${BLOSC2_LIBRARY}")
        set_target_properties(Blosc2::Blosc2 PROPERTIES
            IMPORTED_LINK_INTERFACE_LANGUAGES "C"
            IMPORTED_LOCATION "${BLOSC2_LIBRARY}")
    endif()
endif()
