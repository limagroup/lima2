// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tango.h>

// Command Prepare class definition
template <typename Device>
class prepare_command : public Tango::Command
{
  public:
    prepare_command() : Tango::Command("Prepare", Tango::DEV_STRING, Tango::DEV_VOID, "UUID", "None", Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "prepare_command::execute(): arrived" << std::endl;

        Tango::DevString uuid;
        extract(in_any, uuid);
        ((static_cast<Device*>(dev))->prepare(uuid));

        return new CORBA::Any();
    }
};

// Command Start class definition
template <typename Device>
class start_command : public Tango::Command
{
  public:
    start_command() : Tango::Command("Start", Tango::DEV_VOID, Tango::DEV_VOID, "None", "None", Tango::OPERATOR) {}

    virtual CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any&) override
    {
        TANGO_LOG_INFO << "start_command::execute(): arrived" << std::endl;
        ((static_cast<Device*>(dev))->start());
        return new CORBA::Any();
    }
};

// Command Trigger class definition
template <typename Device>
class trigger_command : public Tango::Command
{
  public:
    trigger_command() : Tango::Command("Trigger", Tango::DEV_VOID, Tango::DEV_VOID, "None", "None", Tango::OPERATOR) {}

    virtual CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any&) override
    {
        TANGO_LOG_INFO << "trigger_command::execute(): arrived" << std::endl;
        ((static_cast<Device*>(dev))->trigger());
        return new CORBA::Any();
    }
};

// Command Stop class definition
template <typename Device>
class stop_command : public Tango::Command
{
  public:
    stop_command() : Tango::Command("Stop", Tango::DEV_VOID, Tango::DEV_VOID, "None", "None", Tango::OPERATOR) {}

    virtual CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any&) override
    {
        TANGO_LOG_INFO << "stop_command::execute(): arrived" << std::endl;
        ((static_cast<Device*>(dev))->stop());
        return new CORBA::Any();
    }
};

// Command Close class definition
template <typename Device>
class close_command : public Tango::Command
{
  public:
    close_command() : Tango::Command("Close", Tango::DEV_VOID, Tango::DEV_VOID, "None", "None", Tango::OPERATOR) {}

    virtual CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any&) override
    {
        TANGO_LOG_INFO << "close_command::execute(): arrived" << std::endl;
        ((static_cast<Device*>(dev))->close());
        return new CORBA::Any();
    }
};

// Command Reset class definition
template <typename Device>
class reset_command : public Tango::Command
{
  public:
    reset_command() : Tango::Command("Reset", Tango::DEV_VOID, Tango::DEV_VOID, "None", "None", Tango::OPERATOR) {}

    virtual CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any&) override
    {
        TANGO_LOG_INFO << "reset_command::execute(): arrived" << std::endl;
        ((static_cast<Device*>(dev))->reset());
        return new CORBA::Any();
    }
};