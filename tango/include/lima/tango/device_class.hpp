// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

#include <tango.h>

#include <boost/lexical_cast.hpp>
#include <boost/describe/annotations.hpp>

#include <lima/utils/type_traits.hpp>

namespace lima::tango
{

struct device_class : public Tango::DeviceClass
{
    using Tango::DeviceClass::DeviceClass;

    Tango::DbDatum get_class_property(std::string& prop_name)
    {
        for (unsigned int i = 0; i < cl_prop.size(); i++)
            if (cl_prop[i].name == prop_name)
                return cl_prop[i];
        // if not found, returns  an empty DbDatum
        return Tango::DbDatum(prop_name);
    }

    Tango::DbDatum get_default_device_property(std::string& prop_name)
    {
        for (unsigned int i = 0; i < dev_def_prop.size(); i++)
            if (dev_def_prop[i].name == prop_name)
                return dev_def_prop[i];
        // if not found, return  an empty DbDatum
        return Tango::DbDatum(prop_name);
    }

    Tango::DbDatum get_default_class_property(std::string& prop_name)
    {
        for (unsigned int i = 0; i < cl_def_prop.size(); i++)
            if (cl_def_prop[i].name == prop_name)
                return cl_def_prop[i];
        // if not found, return  an empty DbDatum
        return Tango::DbDatum(prop_name);
    }

    template <typename T>
    void create_default_property(T const& params)
    {
        std::vector<std::string> vect_data;

        using Desc = BOOST_DESCRIBE_MAKE_NAME(desc);
        using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

        boost::mp11::mp_for_each<
            boost::describe::describe_members<T, boost::describe::mod_any_access | boost::describe::mod_inherited>>(
            [&](auto D) {
                using A = boost::describe::annotate_member<decltype(D)>;
                using param_t = member_type<decltype(D.pointer)>;

                auto val = params.*D.pointer;
                auto desc = boost::describe::annotation_by_name_v<A, Desc>;
                auto doc = boost::describe::annotation_by_name_v<A, Doc>;

                std::string prop_name = D.name;
                std::string prop_desc = doc;
                std::string prop_def = boost::lexical_cast<std::string>(val);
                vect_data.clear();
                vect_data.push_back(prop_def);
                if (prop_def.length() > 0) {
                    Tango::DbDatum data(prop_name);
                    data << vect_data;
                    dev_def_prop.push_back(data);
                    add_wiz_dev_prop(prop_name, prop_desc, prop_def);
                } else
                    add_wiz_dev_prop(prop_name, prop_desc);
            });
    }

    //	write class properties data members
    Tango::DbData cl_prop;
    Tango::DbData cl_def_prop;
    Tango::DbData dev_def_prop;
};

// Implements Singleton
template <typename Derived, typename Base = device_class>
class singleton_device_class : public Base
{
  public:
    // Non-copiable
    singleton_device_class(const singleton_device_class&) = delete;
    singleton_device_class& operator=(const singleton_device_class&) = delete;
    singleton_device_class(singleton_device_class&&) = delete;
    singleton_device_class& operator=(singleton_device_class&&) = delete;

    ~singleton_device_class() { _instance = nullptr; }

    /// Create the object if not already done. Otherwise, just return a pointer to the object
    ///
    /// \param name	The class name
    static Derived* init(std::string const& name,                               //
                         std::vector<Tango::Command*> additional_commands = {}, //
                         std::vector<Tango::Attr*> additional_attributes = {})
    {
        if (_instance == nullptr) {
            try {
                _instance = new Derived(name);
                for (Tango::Command* command : additional_commands)
                    _instance->command_list.emplace_back(command);

                std::vector<Tango::Attr*>& att_list = _instance->get_class_attr()->get_attr_list();
                for (Tango::Attr* attribute : additional_attributes) {
                    attribute->set_disp_level(Tango::OPERATOR);
                    attribute->set_change_event(true, false);
                    att_list.push_back(attribute);
                }
            } catch (std::bad_alloc&) {
                throw;
            }
        }
        return _instance;
    }

    /// Singleton access, check if object already created, and return a pointer to the object
    static Derived* instance()
    {
        if (_instance == nullptr) {
            std::cerr << "Class is not initialised !" << std::endl;
            exit(-1);
        }
        return _instance;
    }

  protected:
    singleton_device_class(std::string const& name) : Base(name) {}

    static inline Derived* _instance = nullptr;
};

} // namespace lima::tango
