// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/json/chrono.hpp>
#include <boost/json/filesystem.hpp>
#include <boost/json/describe.hpp>
#include <boost/json/schema_from.hpp>
#include <boost/mp11.hpp>

#include <fmt/uuid.hpp>

#include <lima/hw/params.describe.hpp>

#include <lima/tango/base_attribute.hpp>
#include <lima/tango/base_command.hpp>
#include <lima/tango/common_attribute.hpp>
#include <lima/tango/processing_device.hpp>
#include <lima/tango/convert.hpp>

namespace lima::tango
{

//=========================================
//	Helper
//=========================================

inline std::vector<std::string> get_proc_class_names()
{
    std::vector<std::string> res;

    auto class_list = *Tango::Util::instance()->get_class_list();

    for (Tango::DeviceClass* device_class : class_list) {
        auto name = std::string_view(device_class->get_name());

        if (name.starts_with("LimaProcessing"))
            res.push_back(device_class->get_name());
    }

    return res;
}

//=========================================
//	Define classes for attributes
//=========================================

template <typename Device>
class proc_class_attr : public Tango::Attr
{
    //using proc_params_t = typename Device::proc_params_t;

  public:
    proc_class_attr() : Attr("proc_class_name", Tango::DEV_ENUM, Tango::READ_WRITE)
    {
        // Add enum labels
        std::vector<std::string> labels = get_proc_class_names();

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.set_enum_labels(labels);
        attr_prop.description = "Class of the processing";
        attr_prop.label = "proc_class_name";

        set_default_properties(attr_prop);
    }
    virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
    {
        TANGO_LOG_INFO << "proc_class_name::read()" << std::endl;

        Tango::DevShort* val = new Tango::DevShort(static_cast<Device*>(dev)->m_proc_class);
        att.set_value(val, 1, 0, true);
    }
    virtual void write(Tango::DeviceImpl* dev, Tango::WAttribute& att)
    {
        TANGO_LOG_INFO << "proc_class_name::write()" << std::endl;

        Device* d = static_cast<Device*>(dev);

        Tango::DevShort t_val;
        att.get_write_value(t_val);

        if (d->m_proc_class != t_val) {
            d->m_proc_class = t_val;
            //// Default construct proc_params
            //d->m_proc_params = boost::mp11::mp_with_index<boost::mp11::mp_size<proc_params_t>>(
            //    d->m_proc_class,
            //    [&](auto I) -> proc_params_t { return std::variant_alternative_t<I, proc_params_t>(); });
        }
    }
};

template <typename Device>
class nb_frames_xferred_attr : public Tango::Attr
{
  public:
    nb_frames_xferred_attr() : Attr("nb_frames_xferred", Tango::DEV_LONG, Tango::READ)
    {
        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "Number of frames transfered";
        attr_prop.label = "nb_frames_xferred";
        set_default_properties(attr_prop);
        set_data_ready_event(true);
    };

    virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
    {
        TANGO_LOG_INFO << "nb_frames_xferred::read()" << std::endl;

        int nb_frames_xferred = (static_cast<Device*>(dev))->nb_frames_xferred();

        Tango::DevLong* t_nb_frames_xferred = new Tango::DevLong();
        tango_encode(nb_frames_xferred, *t_nb_frames_xferred);

        att.set_value(t_nb_frames_xferred, 1, 0, true);
    }
};

template <typename Device>
class recv_rank : public Tango::Attr
{
  public:
    recv_rank() : Attr("recv_rank", Tango::DEV_LONG, Tango::READ){};
    virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
    {
        TANGO_LOG_INFO << "recv_rank::read()" << std::endl;

        int recv_rank = (static_cast<Device*>(dev))->recv_rank();

        Tango::DevLong* t_recv_rank = new Tango::DevLong();
        tango_encode(recv_rank, *t_recv_rank);

        att.set_value(t_recv_rank, 1, 0, true);
    }
};

template <typename Device>
class pipelines_attr : public Tango::SpectrumAttr
{
  public:
    pipelines_attr() : SpectrumAttr("pipelines", Tango::DEV_STRING, Tango::READ, 256){};
    virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
    {
        TANGO_LOG_INFO << "pipelines::read()" << std::endl;

        auto [pipelines, size] = (static_cast<Device*>(dev))->list_pipelines();

        att.set_value(pipelines, size, 0, true); // freed after being send to the client
    }
};

template <typename Device>
class current_pipeline_attr : public Tango::Attr
{
  public:
    current_pipeline_attr() : Attr("current_pipeline", Tango::DEV_STRING, Tango::READ){};
    virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
    {
        TANGO_LOG_INFO << "current_pipeline::read()" << std::endl;

        auto pipeline = (static_cast<Device*>(dev))->current_pipeline();

        att.set_value(pipeline, 1, 0, true);
    }
};

//=========================================
// Define classes for commands
//=========================================

inline auto format_instance_name(std::string const& device_name, std::string const& uuid, int rank)
{
    std::string domain(device_name.begin(), std::find(device_name.begin(), device_name.end(), '/'));
    return fmt::format("{}/limaprocessing/{}@{}", domain, uuid, rank);
}

template <typename Device>
class prepare_command : public Tango::Command
{
  public:
    prepare_command() :
        Tango::Command("Prepare", Tango::DEV_STRING, Tango::DEV_STRING, "Processing UUID", "Tango device name",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "prepare_command::execute(): arrived" << std::endl;

        Tango::DevString uuid;
        extract(in_any, uuid);

        auto td = static_cast<Device*>(dev);
        auto frame_info = td->prepare();

        td->create_pipeline(uuid);

        // Create entry in the database
        auto util = Tango::Util::instance();

        std::string device_name = td->get_name();
        std::vector<std::string> enum_labels = get_proc_class_names();
        std::string class_name = boost::json::value_to<std::string>(td->m_proc_params.at("class_name"));

        // Look up for the processing class by name
        auto class_list = util->get_class_list();
        auto it = std::find_if(class_list->begin(), class_list->end(),
                               [class_name](Tango::DeviceClass* dc) { return class_name == dc->get_name(); });
        if (it == class_list->end())
            Tango::Except::throw_exception("LIMA_Exception", "Processing class " + class_name + " not found", "receiver_class::prepare_command");

        std::string instance_name = format_instance_name(device_name, uuid, td->recv_rank());

        Tango::DbDevInfo dev_info = {instance_name, class_name, util->get_ds_name()};
        Tango::Database* db = util->get_database();
        db->add_device(dev_info);

        // Put new device property
        Tango::DbData db_data;
        {
            Tango::DbDatum prop("frame_info");
            prop << boost::json::serialize(boost::json::value_from(frame_info));
            db_data.push_back(prop);
        }
        {
            Tango::DbDatum prop("proc_params");
            prop << boost::json::serialize(td->m_proc_params);
            db_data.push_back(prop);
        }
        {
            Tango::DbDatum prop("acq_params");
            prop << boost::json::serialize(td->m_acq_params.as_object().at("acq"));
            db_data.push_back(prop);
        }
        {
            Tango::DbDatum prop("det_info");
            //prop << boost::json::serialize(td->m_proc_params);
            prop << "{\"plugin\": \"Simulator\", \"model\": \"Top Model\", \"sn\": \"\", \"pixel_size\": {\"x\": 100, \"y\": 100}, \"dimensions\": {\"x\": 2048, \"y\": 2048}}";
            db_data.push_back(prop);
        }

        db->put_device_property(instance_name, db_data);

        // Construct processing device and export device to the outside world
        auto proc_class = dynamic_cast<processing_device_class*>(*it);
        if (proc_class) {
            processing_device* pipeline = proc_class->create_device(instance_name);
            if (pipeline)
                td->bind_pipeline(pipeline);
        }

        return insert(Tango::string_dup(instance_name));
    }
};

//template <typename Device>
//class bind_command : public Tango::Command
//{
//  public:
//    bind_command() : Tango::Command("Bind", Tango::DEV_STRING, Tango::DEV_VOID, "Processing UUID", "", Tango::OPERATOR)
//    {
//    }
//
//    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
//    {
//        TANGO_LOG_INFO << "bind_command::execute(): arrived" << std::endl;
//
//        //Tango::DevString uuid;
//        //extract(in_any, uuid);
//
//        auto td = static_cast<Device*>(dev);
//
//        //// Create entry in the database
//        //auto util = Tango::Util::instance();
//
//        //std::string device_name = td->get_name();
//
//        //std::vector<std::string> enum_labels = get_proc_class_names();
//
//        //// Look up for the processing class by name
//        //auto class_list = util->get_class_list();
//        //auto it = std::find_if(class_list->begin(), class_list->end(),
//        //                       [proc_class_name = enum_labels[td->m_proc_class]](Tango::DeviceClass* dc) {
//        //                           return proc_class_name == dc->get_name();
//        //                       });
//        //if (td->m_proc_class >= class_list->size())
//        //    Tango::Except::throw_exception("LIMA_Exception", "Processing class not found", "prepare_command::execute");
//
//        //std::string instance_name = format_instance_name(device_name, uuid, td->recv_rank());
//        //TANGO_LOG_INFO << "instance_name: " << instance_name << std::endl;
//
//        //Tango::DbDevInfo dev_info = {instance_name, (*it)->get_name(), util->get_ds_exec_name()};
//        //Tango::Database* db = util->get_database();
//        //db->add_device(dev_info);
//
//        //// Construct processing device and export device to the outside world
//        //Tango::DevVarStringArray* devlist_ptr = new Tango::DevVarStringArray();
//        //devlist_ptr->length(1);
//        //(*devlist_ptr)[0] = Tango::string_dup(instance_name.c_str());
//        //(*it)->device_factory(devlist_ptr);
//
//        //auto device = util->get_device_by_name(instance_name);
//
//        if (td->m_proc) {
//            td->bind_pipeline(td->m_proc);
//
//            //if (proc) {
//            //    // Register the callbacks
//            //    m_recv->register_on_start_acq([proc]() { proc->activate(); });
//            //    m_recv->register_on_frame_ready([proc](auto frame) { proc->process(frame); });
//            //    m_recv->register_on_end_acq([this](int nb_frames) {
//            //        Tango::DevLong* xferred_frames = new Tango::DevLong(nb_frames);
//            //        try {
//            //            DEBUG_STREAM << "receiver::push_change_event(on_acq_end)" << std::endl;
//            //            this->push_change_event("nb_frames_xferred", xferred_frames, 1, 0, true);
//            //        } catch (CORBA::Exception& e) {
//            //            ERROR_STREAM << "receiver::push_change_event(on_acq_end) FAILED" << std::endl;
//            //            Tango::Except::print_exception(e);
//            //        } catch (...) {
//            //            ERROR_STREAM << "receiver::push_change_event(on_acq_end) FAILED" << std::endl;
//            //        }
//            //    });
//            //} else
//            //    Tango::Except::throw_exception("LIMA_Exception", "Failed to construct processing", "receiver::prepare");
//        }
//
//        //return td->prepare(uuid);
//
//        ///Tango::DeviceImpl* proc_dev = util->get_device_by_name(instance_name);
//
//        //proc_dev->m_proc = proc;
//        //proc_dev->m_proc_params = td->m_proc_params;
//
//        //static_cast<typename Device::processing_t*>(proc_dev)->m_proc = proc;
//
//        //return insert(Tango::string_dup(instance_name.c_str()));
//
//        return new CORBA::Any();
//    }
//};

//template <typename Device>
//class create_pipeline_command : public Tango::Command
//{
//  public:
//    create_pipeline_command() :
//        Tango::Command("createPipeline", Tango::DEV_STRING, Tango::DEV_STRING, "UUID of the processing pipeline",
//                       "Tango device name", Tango::OPERATOR)
//    {
//    }
//
//    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
//    {
//        TANGO_LOG_INFO << "create_pipeline_command::execute(): arrived" << std::endl;
//
//        Tango::DevString uuid;
//        extract(in_any, uuid);
//
//        auto td = static_cast<Device*>(dev);
//
//        td->create_pipeline(uuid);
//
//        // Create entry in the database
//        auto util = Tango::Util::instance();
//
//        std::string device_name = td->get_name();
//        std::vector<std::string> enum_labels = get_proc_class_names();
//        std::string class_name = enum_labels[td->m_proc_class];
//
//        // Look up for the processing class by name
//        auto class_list = util->get_class_list();
//        auto it = std::find_if(class_list->begin(), class_list->end(),
//                               [class_name](Tango::DeviceClass* dc) { return class_name == dc->get_name(); });
//        if (td->m_proc_class >= class_list->size())
//            Tango::Except::throw_exception("LIMA_Exception", "Class not found", "receiver_class::create_pipeline");
//
//        std::string instance_name = format_instance_name(device_name, uuid, td->recv_rank());
//
//        Tango::DbDevInfo dev_info = {instance_name, class_name, util->get_ds_name()};
//        Tango::Database* db = util->get_database();
//        db->add_device(dev_info);
//
//        // Construct processing device and export device to the outside world
//        auto proc_class = dynamic_cast<processing_device_class*>(*it);
//        if (proc_class)
//            td->m_proc = proc_class->create_device(instance_name);
//
//        return insert(Tango::string_dup(instance_name));
//    }
//};

template <typename Device>
class erase_pipeline_command : public Tango::Command
{
  public:
    erase_pipeline_command() :
        Tango::Command("erasePipeline", Tango::DEV_STRING, Tango::DEV_VOID, "UUID of the processing pipeline", "",
                       Tango::OPERATOR)
    {
    }

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "erase_pipelines_command::execute(): arrived" << std::endl;

        Tango::DevString uuid;
        extract(in_any, uuid);

        auto td = static_cast<Device*>(dev);

        if (td->is_current_pipeline(uuid))
            Tango::Except::throw_exception("LIMA_Exception", "Cannot erase current pipeline", "receiver_class::erase_pipeline_command");

        std::string device_name = td->get_name();
        std::string domain(device_name.begin(), std::find(device_name.begin(), device_name.end(), '/'));

        // Remove entry in the database
        auto util = Tango::Util::instance();

        std::string instance_name = fmt::format("{}/limaprocessing/{}@{}", domain, uuid, td->recv_rank());
        TANGO_LOG_INFO << "instance_name: " << instance_name << std::endl;

        TANGO_LOG_DEBUG << "Deleting " << instance_name << " from database" << std::endl;

        Tango::Database* db = util->get_database();
        db->delete_device(instance_name);

        TANGO_LOG_DEBUG << "Getting " << instance_name << " device" << std::endl;

        // Destroy device
        Tango::DeviceImpl* proc_dev = Tango::Util::instance()->get_device_by_name(instance_name);

        TANGO_LOG_DEBUG << "Calling device_destroyer on " << instance_name << std::endl;

        proc_dev->get_device_class()->device_destroyer(instance_name);

        TANGO_LOG_DEBUG << "Erasing pipeline for " << uuid << std::endl;

        // Erase from the pipeline
        td->erase_pipeline(uuid);

        TANGO_LOG_INFO << "erase_pipelines_command::execute(): completed" << std::endl;

        return new CORBA::Any();
    }
};

//===================================================================
// Properties management
//===================================================================

template <typename Receiver>
void receiver_class<Receiver>::set_default_property()
{
    //std::vector<std::string> vect_data;

    //// Set Default Class Properties

    //// Set Default device Properties
    //using Desc = BOOST_DESCRIBE_MAKE_NAME(desc);
    //using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

    //auto create_prop = [&](auto params) {
    //    using params_t = std::decay_t<decltype(params)>;

    //    boost::mp11::mp_for_each<boost::describe::describe_members<params_t, boost::describe::mod_any_access>>(
    //        [&](auto D) {
    //            using A = boost::describe::annotate_member<decltype(D)>;
    //            using param_t = member_type<decltype(D.pointer)>;

    //            auto val = params.*D.pointer;
    //            auto desc = boost::describe::annotation_by_name_v<A, Desc>;
    //            auto doc = boost::describe::annotation_by_name_v<A, Doc>;

    //            std::string prop_name = D.name;
    //            std::string prop_desc = doc;
    //            std::string prop_def = boost::lexical_cast<std::string>(val);
    //            vect_data.clear();
    //            vect_data.push_back(prop_def);
    //            if (prop_def.length() > 0) {
    //                Tango::DbDatum data(prop_name);
    //                data << vect_data;
    //                this->dev_def_prop.push_back(data);
    //                this->add_wiz_dev_prop(prop_name, prop_desc, prop_def);
    //            } else
    //                this->add_wiz_dev_prop(prop_name, prop_desc);
    //        });
    //};

    // Generate the schema for the plugin init_params
    boost::json::value schema = boost::json::schema_from<typename device_t::init_params_t>("init_params");

    // Add a "schema" class attribute property
    Tango::DbDatum db_nb_attrs("init_params");
    Tango::DbDatum db_schema("schema");

    db_nb_attrs << 1; // One "schema" property for attribute "params"
    std::string schema_str =
        boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
    db_schema << schema_str;
}

template <typename Receiver>
void receiver_class<Receiver>::write_class_property()
{
    // First time, check if database used
    if (Tango::Util::_UseDb == false)
        return;

    Tango::DbData data;
    std::string classname = this->get_name();
    std::string header;
    std::string::size_type start, end;

    // Put title
    Tango::DbDatum title("ProjectTitle");
    std::string str_title("LIMA2");
    title << str_title;
    data.push_back(title);

    // Put Description
    Tango::DbDatum description("Description");
    std::vector<std::string> str_desc;
    str_desc.push_back("LIMA2 Receiver Class");
    description << str_desc;
    data.push_back(description);

    //  Put inheritance
    Tango::DbDatum inher_datum("InheritedFrom");
    std::vector<std::string> inheritance;
    inheritance.push_back("TANGO_BASE_CLASS");
    inher_datum << inheritance;
    data.push_back(inher_datum);

    // Call database and and values
    this->get_db_class()->put_property(data);
}

//===================================================================
// Factory methods
//===================================================================

template <typename Receiver>
void receiver_class<Receiver>::device_factory(const Tango::DevVarStringArray* devlist_ptr)
{
    // Create devices and add it into the device list
    for (unsigned long i = 0; i < devlist_ptr->length(); i++) {
        TANGO_LOG_DEBUG << "Device name : " << (*devlist_ptr)[i].in() << std::endl;
        this->device_list.push_back(new device_t(this, (*devlist_ptr)[i]));
    }

    // Manage dynamic attributes if any
    erase_dynamic_attributes(devlist_ptr, this->get_class_attr()->get_attr_list());

    // Export devices to the outside world
    for (unsigned long i = 1; i <= devlist_ptr->length(); i++) {
        // Add dynamic attributes if any
        device_t* dev = static_cast<device_t*>(this->device_list[this->device_list.size() - i]);
        dev->add_dynamic_attributes();

        // Check before if database used.
        if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
            this->export_device(dev);
        else
            this->export_device(dev, dev->get_name().c_str());
    }
}

template <typename Receiver>
void receiver_class<Receiver>::attribute_factory(std::vector<Tango::Attr*>& att_list)
{
    // using Desc = BOOST_DESCRIBE_MAKE_NAME(desc);
    // using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);
    //
    // auto create_attr = [&](auto D) {
    //     using A = boost::describe::annotate_member<decltype(D)>;
    //     using param_t = member_type<decltype(D.pointer)>;
    //
    //     auto desc = boost::describe::annotation_by_name_v<A, Desc>;
    //     auto doc = boost::describe::annotation_by_name_v<A, Doc>;
    //
    //     constexpr long dtype = tango_dtype_v<param_t>;
    //
    //     // If data type is supported
    //     if constexpr (dtype) {
    //         GenericAttr* attr = new GenericAttr(D.name, tango_dtype_v<param_t>, Tango::READ_WRITE);
    //
    //         Tango::UserDefaultAttrProp attr_prop;
    //
    //         // If enumeration, add labels
    //         if constexpr (std::is_enum_v<param_t>) {
    //             std::vector<std::string> labels;
    //
    //             boost::mp11::mp_for_each<boost::describe::describe_enumerators<param_t>>(
    //                 [&labels](auto D) { labels.push_back(D.name); });
    //
    //             attr_prop.set_enum_labels(labels);
    //         }
    //
    //         attr_prop.description = doc;
    //         attr_prop.label = desc;
    //         // description
    //         // label
    //         // unit
    //         // standard_unit
    //         // display_unit
    //         // format
    //         // max_value
    //         // min_value
    //         // max_alarm
    //         // min_alarm
    //         // max_warning
    //         // min_warning
    //         // delta_t
    //         // delta_val
    //
    //         attr->set_default_properties(attr_prop);
    //         // Not Polled
    //         attr->set_disp_level(Tango::OPERATOR);
    //         // Not Memorized
    //         attr->set_change_event(true, false);
    //
    //         // Setup the read implementation
    //         attr->read_impl = [&D](Tango::DeviceImpl* dev, Tango::Attribute& attr) {
    //             //TANGO_LOG_INFO << "Reading attribute " << D.name << std::endl;
    //
    //             device_t* recv = static_cast<device_t*>(dev);
    //             auto& val = recv->m_acq_params.*D.pointer;
    //
    //             // Set the attribute value
    //             tango_dtype_t<param_t> t_val;
    //             tango_encode(val, t_val);
    //             attr.set_value(&t_val);
    //         };
    //
    //         // Setup the write implementation
    //         attr->write_impl = [&D](Tango::DeviceImpl* dev, Tango::WAttribute& attr) {
    //             //TANGO_LOG_INFO << "Writting attribute " << D.name << std::endl;
    //
    //             device_t* recv = static_cast<device_t*>(dev);
    //             auto& val = recv->m_acq_params.*D.pointer;
    //
    //             // Retrieve write value
    //             tango_dtype_t<param_t> t_val;
    //             attr.get_write_value(t_val);
    //             tango_decode(t_val, val);
    //         };
    //
    //         att_list.push_back(attr);
    //     }
    // };

    // Add world_rank attribute
    {
        auto attr = new world_rank<device_t>();

        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "MPI world rank";
        attr_prop.label = "world_rank";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add recv_rank attribute
    {
        auto attr = new recv_rank<device_t>();

        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "MPI recv rank";
        attr_prop.label = "recv_rank";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add nb_frames_xferred attribute
    {
        auto attr = new nb_frames_xferred_attr<device_t>();

        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "Number of frames transferred";
        attr_prop.label = "nb_frames_xferred";

        attr->set_default_properties(attr_prop);
        // Not Polled
        attr->set_disp_level(Tango::OPERATOR);
        // Not Memorized
        attr->set_change_event(true, false);

        att_list.push_back(attr);
    }

    // Add current_pipeline attribute
    {
        auto attr = new current_pipeline_attr<device_t>();

        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "The current pipeline UUID";
        attr_prop.label = "current_pipeline";

        attr->set_default_properties(attr_prop);
        // Not Polled
        attr->set_disp_level(Tango::OPERATOR);
        // Not Memorized
        attr->set_change_event(true, false);

        att_list.push_back(attr);
    }

    // Add pipelines attribute
    {
        auto attr = new pipelines_attr<device_t>();

        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "A list of processing UUIDs";
        attr_prop.label = "pipelines";

        attr->set_default_properties(attr_prop);
        // Not Polled
        attr->set_disp_level(Tango::OPERATOR);
        // Not Memorized
        attr->set_change_event(true, false);

        att_list.push_back(attr);
    }

    // Add acq_params JSON attribute
    {
        using acq_params_attr_t = JsonAttr<device_t, &device_t::m_acq_params>;
        acq_params_attr_t* attr = new acq_params_attr_t("acq_params", Tango::DEV_STRING, Tango::READ_WRITE);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Acquisition parameters";
        attr_prop.label = "acq_params";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add acq_params JSON schema attribute property
    {
        // Generate the schema for the plugin acq_params
        boost::json::value schema = boost::json::schema_from<typename device_t::acq_params_t>("acq_params");

        // Add a "schema" class attribute property
        Tango::DbDatum db_nb_attrs("acq_params");
        Tango::DbDatum db_schema("schema");

        db_nb_attrs << 1; // One "schema" property for attribute "params"
        std::string schema_str =
            boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
        db_schema << schema_str;

        // Push to the DB
        Tango::DbData db_data;
        db_data.push_back(db_nb_attrs);
        db_data.push_back(db_schema);

        this->get_db_class()->put_attribute_property(db_data);
    }

    // Add acq_state attribute
    {
        struct AcqStateEnumAttr : public EnumAttr<acq_state_enum>
        {
            AcqStateEnumAttr() : EnumAttr<acq_state_enum>("acq_state", "State of the receiver") {}

            void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
            {
                attr.set_value(static_cast<device_t*>(dev)->acq_state(), 1, 0, true);
            }
        };

        auto attr = new AcqStateEnumAttr();

        // Not Polled
        attr->set_disp_level(Tango::OPERATOR);
        // Not Memorized
        attr->set_change_event(true, false);

        att_list.push_back(attr);
    }

    // Add proc_class_name attribute
    {
        auto attr = new proc_class_attr<device_t>();

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Processing class name";
        attr_prop.label = "proc_class_name";

        auto enum_labels = get_proc_class_names();
        attr_prop.set_enum_labels(enum_labels);

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add proc_params JSON attribute
    {
        using proc_params_attr_t = JsonAttr<device_t, &device_t::m_proc_params>;
        proc_params_attr_t* attr = new proc_params_attr_t("proc_params", Tango::DEV_STRING, Tango::READ_WRITE);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Processing parameters";
        attr_prop.label = "proc_params";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Create a list of static attributes
    create_static_attribute_list(this->get_class_attr()->get_attr_list());
}

template <typename Receiver>
void receiver_class<Receiver>::pipe_factory()
{
}

template <typename Receiver>
void receiver_class<Receiver>::command_factory()
{
    // Receiver commands
    this->command_list.emplace_back(new prepare_command<device_t>());
    //this->command_list.emplace_back(new bind_command<device_t>());
    this->command_list.emplace_back(new start_command<device_t>());
    this->command_list.emplace_back(new stop_command<device_t>());
    this->command_list.emplace_back(new close_command<device_t>());
    this->command_list.emplace_back(new reset_command<device_t>());

    // Processing commands
    //this->command_list.emplace_back(new create_pipeline_command<device_t>());
    //this->command_list.emplace_back(new abort_pipeline_command<device_t>());
    this->command_list.emplace_back(new erase_pipeline_command<device_t>());
}

//===================================================================
// Dynamic attributes related methods
//===================================================================

template <typename Receiver>
void receiver_class<Receiver>::create_static_attribute_list(std::vector<Tango::Attr*>& att_list)
{
    for (unsigned long i = 0; i < att_list.size(); i++) {
        std::string att_name(att_list[i]->get_name());
        transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
        defaultAttList.push_back(att_name);
    }

    TANGO_LOG_INFO << defaultAttList.size() << " attributes in default list" << std::endl;
}

template <typename Receiver>
void receiver_class<Receiver>::erase_dynamic_attributes(const Tango::DevVarStringArray* devlist_ptr,
                                                        std::vector<Tango::Attr*>& att_list)
{
    Tango::Util* tg = Tango::Util::instance();

    for (unsigned long i = 0; i < devlist_ptr->length(); i++) {
        Tango::DeviceImpl* dev_impl = tg->get_device_by_name(((std::string)(*devlist_ptr)[i]).c_str());
        device_t* dev = static_cast<device_t*>(dev_impl);

        std::vector<Tango::Attribute*>& dev_att_list = dev->get_device_attr()->get_attribute_list();
        for (auto ite_att = dev_att_list.begin(); ite_att != dev_att_list.end(); ++ite_att) {
            std::string att_name((*ite_att)->get_name_lower());
            if ((att_name == "state") || (att_name == "status"))
                continue;
            auto ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
            if (ite_str == defaultAttList.end()) {
                TANGO_LOG_INFO << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i]
                               << std::endl;
                Tango::Attribute& att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
                dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
                --ite_att;
            }
        }
    }
}

template <typename Receiver>
Tango::Attr* receiver_class<Receiver>::get_attr_object_by_name(std::vector<Tango::Attr*>& att_list, std::string attname)
{
    for (auto&& att : att_list)
        if (att->get_name() == attname)
            return att;

    // Attr does not exist
    return NULL;
}

} // namespace lima::tango
