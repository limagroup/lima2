// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstring>
#include <cstdint>

#include <string>

#include <tango.h>

namespace lima::tango
{
/// C++ / Tango mapping type trait

template <typename T, class Enable = void>
struct tango_dtype
{
    static const long tango_value = 0; // Unsupported data type
};
template <>
struct tango_dtype<bool, void>
{
    using value_type = bool;
    static const long tango_value = Tango::DEV_BOOLEAN;
    using tango_type = Tango::DevBoolean;
};
template <>
struct tango_dtype<std::int16_t, void>
{
    using value_type = std::int16_t;
    static const long tango_value = Tango::DEV_SHORT;
    using tango_type = Tango::DevShort;
};
template <>
struct tango_dtype<std::uint16_t, void>
{
    using value_type = std::uint16_t;
    static const long tango_value = Tango::DEV_USHORT;
    using tango_type = Tango::DevUShort;
};
template <>
struct tango_dtype<std::int32_t, void>
{
    using value_type = std::int32_t;
    static const long tango_value = Tango::DEV_LONG;
    using tango_type = Tango::DevLong;
};
template <>
struct tango_dtype<std::uint32_t, void>
{
    using value_type = std::uint32_t;
    static const long tango_value = Tango::DEV_ULONG;
    using tango_type = Tango::DevULong;
};
template <>
struct tango_dtype<float, void>
{
    using value_type = float;
    static const long tango_value = Tango::DEV_FLOAT;
    using tango_type = Tango::DevFloat;
};
template <>
struct tango_dtype<double, void>
{
    using value_type = double;
    static const long tango_value = Tango::DEV_DOUBLE;
    using tango_type = Tango::DevDouble;
};
template <>
struct tango_dtype<std::string, void>
{
    using value_type = std::string;
    static const long tango_value = Tango::DEV_STRING;
    using tango_type = Tango::DevString;
};
template <typename T>
struct tango_dtype<T, std::enable_if_t<std::is_enum_v<T>>>
{
    using value_type = T;
    static const long tango_value = Tango::DEV_ENUM;
    using tango_type = Tango::DevEnum;
};
template <typename Rep, typename Period>
struct tango_dtype<std::chrono::duration<Rep, Period>, void>
{
    using value_type = std::chrono::duration<Rep, Period>;
    static const long tango_value = Tango::DEV_ULONG64;
    using tango_type = Tango::DevULong64;
};

/// Returns the Tango type value given a C++ type
template <typename T>
constexpr long tango_dtype_v = tango_dtype<T>::tango_value;

/// Returns the Tango type given a C++ type
template <typename T>
using tango_dtype_t = typename tango_dtype<T>::tango_type;

/// Convert from tango type to C++ type and vice versa

// Builtin types
template <typename T>
void tango_encode(T& in, tango_dtype_t<T>& out)
{
    out = reinterpret_cast<tango_dtype_t<T>&>(in);
}
template <typename T>
void tango_decode(tango_dtype_t<T>& in, T& out)
{
    out = reinterpret_cast<T&>(in);
}

// std::string
inline void tango_encode(std::string const& in, Tango::DevString& out)
{
    out = Tango::string_dup(in);
}
inline void tango_decode(Tango::DevString& in, std::string& out)
{
    out = std::string(in);
}

// std::chrono::duration
template <typename Rep, typename Period>
void tango_encode(std::chrono::duration<Rep, Period>& in, Tango::DevULong64& out)
{
    out = in.count();
}
template <typename Rep, typename Period>
void tango_decode(Tango::DevULong64& in, std::chrono::duration<Rep, Period>& out)
{
    out = std::chrono::duration<Rep, Period>(in);
}

} // namespace lima::tango