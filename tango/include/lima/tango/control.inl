// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <numeric> // For std::accumulate

#include <boost/exception/diagnostic_information.hpp>
#include <boost/json/chrono.hpp>
#include <boost/json/describe.hpp>
#include <boost/mp11.hpp>

#include <lima/log/severity_level.describe.hpp>
#include <lima/log/logger.hpp>
#include <lima/hw/params.describe.hpp>
#include <lima/hw/status.describe.hpp>
#include <lima/utils/type_traits.hpp>

#include <lima/tango/attribute_lock_guard.hpp>
#include <lima/tango/convert.hpp>

namespace lima::tango
{
template <typename Control>
void control<Control>::init_device()
{
    DEBUG_STREAM << "control::init_device() create device " << device_name << std::endl;

    // Get the device properties from database
    // Device property data members
    ds_init_params_t ds_init_params;
    init_params_t init_params;
    get_device_property(ds_init_params);

    // Decode plugin params
    boost::json::value val = boost::json::parse(ds_init_params.plugin_params);
    init_params = boost::json::value_to<init_params_t>(val);

    // Construct the controller
    m_ctrl = std::make_unique<control_t>(init_params);

    // Log state change
    m_ctrl->register_on_state_change([this](acq_state_enum state) {
        // Push change event
        try {
            DEBUG_STREAM << "control::push_change_event(acq_state)" << std::endl;
            Tango::DevEnum* dev_state = new Tango::DevEnum(static_cast<Tango::DevShort>(state));
            {
                attribute_lock_guard lock(this->get_device_attr()->get_attr_by_name("acq_state"));
                this->push_change_event("acq_state", dev_state, 1, 0, true);
            }

            DEBUG_STREAM << "control::push_change_event(acq_state) DONE" << std::endl;
        } catch (CORBA::Exception& e) {
            ERROR_STREAM << "control::push_change_event(acq_state) FAILED" << std::endl;
            Tango::Except::print_exception(e);
        } catch (...) {
            ERROR_STREAM << "control::push_change_event(acq_state) FAILED" << std::endl;
        }

        // Update Tango state as well
        set_state(to_tango_state(state));
    });
}

template <typename Control>
void control<Control>::always_executed_hook()
{
    //DEBUG_STREAM << "control::always_executed_hook()  " << device_name << std::endl ;

    // code always executed before all requests
}

template <typename Control>
void control<Control>::read_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    //DEBUG_STREAM << "control::read_attr_hardware(vector<long> &attr_list) entering... " << std::endl ;

    // Add your own code
}

template <typename Control>
void control<Control>::write_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    //DEBUG_STREAM << "control::write_attr_hardware(vector<long> &attr_list) entering... " << std::endl ;

    // Add your own code
}

template <typename Control>
void control<Control>::add_dynamic_attributes()
{
    // Add your own code to create and add dynamic attributes if any
}

template <typename Control>
void control<Control>::prepare(Tango::DevString uuid)
{
    DEBUG_STREAM << "control::prepare(uuid)  - " << device_name << std::endl;

    // NOTE uuid is unused

    try {
        auto acq_params = boost::json::value_to<acq_params_t>(m_acq_params);
        m_ctrl->prepare_acq(acq_params);
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::prepare");
    }
}

template <typename Control>
void control<Control>::start()
{
    DEBUG_STREAM << "control::start()  - " << device_name << std::endl;

    try {
        m_ctrl->start_acq();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::start");
    }
}

template <typename Control>
void control<Control>::trigger()
{
    DEBUG_STREAM << "control::trigger()  - " << device_name << std::endl;

    try {
        m_ctrl->soft_trigger();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "control::soft_trigger");
    }
}

template <typename Control>
void control<Control>::stop()
{
    DEBUG_STREAM << "control::stop()  - " << device_name << std::endl;

    try {
        m_ctrl->stop_acq();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::stop");
    }
}

template <typename Control>
void control<Control>::close()
{
    DEBUG_STREAM << "control::close()  - " << device_name << std::endl;

    try {
        m_ctrl->close_acq();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::close");
    }
}

template <typename Control>
void control<Control>::reset()
{
    DEBUG_STREAM << "control::reset()  - " << device_name << std::endl;

    try {
        m_ctrl->reset_acq(reset_level_enum::soft);
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::reset");
    }
}

template <typename Control>
typename Control::det_info_t control<Control>::det_info()
{
    DEBUG_STREAM << "control::det_info()  - " << device_name << std::endl;

    try {
        return m_ctrl->det_info();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::det_info");
    }
}

template <typename Control>
typename Control::det_status_t control<Control>::det_status()
{
    DEBUG_STREAM << "control::det_status()  - " << device_name << std::endl;

    try {
        return m_ctrl->det_status();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::det_status");
    }
}

template <typename Control>
typename Control::det_capabilities_t control<Control>::det_capabilities()
{
    DEBUG_STREAM << "control::det_capabilities()  - " << device_name << std::endl;

    try {
        return m_ctrl->det_capabilities();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::det_capabilities");
    }
}

template <typename Control>
int control<Control>::world_rank()
{
    return m_ctrl->world_rank();
}

template <typename Control>
acq_state_enum* control<Control>::acq_state() const
{
    //DEBUG_STREAM << "control::acq_state()  - " << device_name << std::endl;

    acq_state_enum* res;

    try {
        acq_state_enum state = m_ctrl->state();
        res = new acq_state_enum(state);
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "control::acq_state");
    }

    return res;
}

template <typename Control>
Tango::DevLong* control<Control>::nb_frames_acquired() const
{
    //DEBUG_STREAM << "control::nb_frames_acquired()  - " << device_name << std::endl;

    Tango::DevLong* res;

    try {
        int nb_frames_acquired = m_ctrl->nb_frames_acquired();
        res = new Tango::DevLong(nb_frames_acquired);
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "control::nb_frames_acquired");
    }

    return res;
}

template <typename Control>
void control<Control>::add_dynamic_commands()
{
    // Add your own code to create and add dynamic commands if any
}

// Additional Methods

} // namespace lima::tango
