// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <type_traits>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/json.hpp>

#include <tango.h>

#include <lima/utils/type_traits.hpp>

#include <lima/tango/convert.hpp>

namespace lima::tango
{

class any_processing
{
    // Defines the common interface of processing
    struct base
    {
        virtual ~base() = default;
        virtual void activate() = 0;
        virtual void process(frame) = 0;
        virtual bool is_finished() const = 0;
        virtual std::string last_error() const = 0;
        virtual void abort() = 0;
    };

    template <typename T>
    struct processing_impl;

  public:
    std::unique_ptr<base> m_impl;

    template <typename T>
    any_processing(T&&);

    bool empty() const { return m_impl->empty(); }
    boost::json::value get(std::string const& key) const
    {
        {
            return m_impl->get(key);
        }
    }
    bool set(std::string const& key, boost::json::value const& value)
    {
        {
            return m_impl->set(key, value);
        }
    }

    void activate() { return m_impl->activate(); }
    void process(frame frm) { return m_impl->process(frm); }
    bool is_finished() const { return m_impl->is_finished(); }
    std::string last_error() const { return m_impl->last_error(); }
    void abort() { return m_impl->abort(); }
};

template <typename T>
any_processing::any_processing(T&& rhs) : m_impl(std::make_unique<processing_impl<T>>(std::move(rhs)))
{
}

template <typename T>
class any_processing::processing_impl : public any_processing::base
{
    T m_concrete;

  public:
    explicit processing_impl(T&& rhs) : m_concrete(std::move(rhs)) {}

    void activate() override { return m_impl->activate(); }
    void process(frame frm) override { return m_impl->process(frm); }
    bool is_finished() const override { return m_impl->is_finished(); }
    std::string last_error() const override { return m_impl->last_error(); }
    void abort() override { return m_impl->abort(); }
};

template <typename T>
std::unique_ptr<T> any_processing_cast(any_processing const& any)
{
    return std::dynamic_pointer_cast<T>(any.m_impl);
}
} // namespace lima::tango