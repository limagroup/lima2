// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <string>

#include <boost/describe/annotations.hpp>

// Control init params
struct ctrl_init_params
{
    std::string plugin_params = "{}"; //!< HW plugin init parameters (JSON)
    std::string receiver_topology = "single";
};

BOOST_DESCRIBE_STRUCT(ctrl_init_params, (), (plugin_params, receiver_topology))

// clang-format off
BOOST_ANNOTATE_MEMBER(ctrl_init_params, plugin_params,
    (desc, "plugin init parameterss"),
    (doc, "Hardware plugin initialization parameters [JSON]"))

BOOST_ANNOTATE_MEMBER(ctrl_init_params, receiver_topology,
    (desc, "receiver topology init parameter"),
    (doc, "Receiver topology parameter (single, round_robin)"))
// clang-format on
