// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>
#include <string>

#include <lima/tango/device_class.hpp>
#include <lima/tango/processing_device.hpp>

namespace lima::tango
{

class processing_device_class : public device_class
{
  public:
    using device_class::device_class;

    virtual processing_device* create_device(std::string const& instance_name) = 0;
};

template <typename Derived>
class processing_class : public tango::singleton_device_class<Derived, processing_device_class>
{
    using base_t = tango::singleton_device_class<Derived, processing_device_class>;

  public:
    using base_t::base_t;

    void device_factory(const Tango::DevVarStringArray* dev_list) override
    {
        // Do not recreate processing device that were not properly removed
    }
};

//=========================================
//	Define classes for attributes
//=========================================

/// Processing is_finished attribute
class is_finished_attr : public Tango::Attr
{
  public:
    is_finished_attr() : Tango::Attr("is_finished", Tango::DEV_BOOLEAN, Tango::READ)
    {
        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "Processing finished state";
        attr_prop.label = "is_finished";
        set_default_properties(attr_prop);
        set_data_ready_event(true);
    }

    void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
    {
        auto d = static_cast<processing_device*>(dev);
        auto is_finished = new Tango::DevBoolean(d->is_finished());
        attr.set_value(is_finished, 1, 0, true);
    }

    bool is_allowed(Tango::DeviceImpl* dev, Tango::AttReqType ty) override { return true; }
};

/// Processing last_error attribute
class last_error_attr : public Tango::Attr
{
  public:
    last_error_attr() : Tango::Attr("last_error", Tango::DEV_STRING, Tango::READ)
    {
        Tango::UserDefaultAttrProp attr_prop;
        attr_prop.description = "Processing error message";
        attr_prop.label = "last_error";
        set_default_properties(attr_prop);
        set_data_ready_event(true);
    }

    void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
    {
        auto d = static_cast<processing_device*>(dev);
        Tango::DevString* last_error = new Tango::DevString;
        *last_error = Tango::string_dup(d->last_error());
        attr.set_value(last_error, 1, 0, true);
    }

    bool is_allowed(Tango::DeviceImpl* dev, Tango::AttReqType ty) override { return true; }
};

//=========================================
// Define classes for commands
//=========================================

class abort_command : public Tango::Command
{
  public:
    abort_command() : Tango::Command("abort", Tango::DEV_VOID, Tango::DEV_VOID, "", "", Tango::OPERATOR) {}

    CORBA::Any* execute(Tango::DeviceImpl* dev, const CORBA::Any& in_any) override
    {
        TANGO_LOG_INFO << "states_command::execute(): arrived" << std::endl;

        (static_cast<processing_device*>(dev))->abort();

        return new CORBA::Any();
    }
};

} // namespace lima::tango