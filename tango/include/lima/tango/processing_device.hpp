// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>

#include <lima/core/frame.hpp>

#include <lima/tango/device.hpp>

namespace lima::tango
{

class processing_device : public device
{
  public:
    using device::device;

    virtual void activate() = 0;

    virtual void process(frame) = 0;

    virtual void process(std::vector<frame> frms)
    {
        for (auto&& frm : frms)
            process(frm);
    }

    virtual bool is_finished() const = 0;

    virtual std::string last_error() const = 0;

    virtual void abort() = 0;
};

//template <typename Derived>
//class basic_processing_device : public processing_device
//{
//  public:
//    using processing_device::processing_device;
//
//    /// Get is_finished attribute
//    bool is_finished() const override { return m_proc->is_finished(); }
//
//    /// Get last_error attribute
//    std::string last_error() const override;
//    void set_last_error(std::string const& what);
//
//    void activate() override { m_proc->activate(); }
//
//    void process(frame frm) override { m_proc->process(frame frm); }
//};

} // namespace lima::tango
