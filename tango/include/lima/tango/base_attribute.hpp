// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <type_traits>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/json.hpp>

#include <tango.h>

#include <lima/utils/type_traits.hpp>

#include <lima/tango/convert.hpp>

namespace lima::tango
{
// Enumeration attribute
template <typename E>
class EnumAttr : public Tango::Attr
{
  public:
    EnumAttr(std::string name, std::string description) : Tango::Attr(name.c_str(), Tango::DEV_ENUM, Tango::READ)
    {
        // Add enum labels
        std::vector<std::string> labels;
        boost::mp11::mp_for_each<boost::describe::describe_enumerators<E>>(
            [&labels](auto D) { labels.push_back(D.name); });

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.set_enum_labels(labels);
        attr_prop.description = description;
        attr_prop.label = name;

        set_default_properties(attr_prop);
    }

    bool is_allowed(Tango::DeviceImpl* dev, Tango::AttReqType ty) override { return true; }
    virtual bool same_type(const std::type_info& in_type) override { return typeid(E) == in_type; }
    virtual std::string get_enum_type() override { return typeid(E).name(); }
};

// JSON attribute
// Read : decode DevString, parse the JSON data and assign it to the C++ datastructure
// Write : serialize the C++ datastructure to JSON data and encode to DevString
template <typename Device, auto Pm>
class JsonAttr : public Tango::Attr
{
  public:
    using Tango::Attr::Attr;

    void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
    {
        //DEBUG_STREAM << "Reading attribute " << attr.get_name() << std::endl;
        Device* d = static_cast<Device*>(dev);

        try {
            boost::json::value val;
            if constexpr (std::is_member_function_pointer_v<decltype(Pm)>)
                val = boost::json::value_from((d->*Pm)());
            else
                val = boost::json::value_from(d->*Pm);

            // Set the attribute value
            Tango::DevString* t_val = new Tango::DevString;
            tango_encode(boost::json::serialize(val), *t_val);
            attr.set_value(t_val, 1, 0, true);
        } catch (std::exception& e) {
            Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "JsonAttr::read");
        }
    }

    void write(Tango::DeviceImpl* dev, Tango::WAttribute& attr) override
    {
        //DEBUG_STREAM << "Writing attribute " << attr.get_name() << std::endl;
        Device* d = static_cast<Device*>(dev);

        // Retrieve write value
        Tango::DevString t_val;
        attr.get_write_value(t_val);
        //tango_decode(t_val, val);

        try {
            boost::json::value val = boost::json::parse(t_val);

            if constexpr (std::is_member_function_pointer_v<decltype(Pm)>)
                d->*Pm(boost::json::value_to<lima::member_type<decltype(Pm)>>(val));
            else
                d->*Pm = boost::json::value_to<lima::member_type<decltype(Pm)>>(val);
        } catch (std::exception& e) {
            Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "JsonAttr::write");
        }
    }

    bool is_allowed(Tango::DeviceImpl* dev, Tango::AttReqType ty) override { return true; }
};

// JSON read-only attribute
// Read : decode DevString, parse the JSON data and assign it to the C++ datastructure
template <typename Device, auto Pm>
class JsonReadAttr : public Tango::Attr
{
  public:
    using Tango::Attr::Attr;

    void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
    {
        //DEBUG_STREAM << "Reading attribute " << attr.get_name() << std::endl;
        Device* d = static_cast<Device*>(dev);

        try {
            boost::json::value val;
            if constexpr (std::is_member_function_pointer_v<decltype(Pm)>)
                val = boost::json::value_from((d->*Pm)());
            else
                val = boost::json::value_from(d->*Pm);

            // Set the attribute value
            Tango::DevString* t_val = new Tango::DevString;
            tango_encode(boost::json::serialize(val), *t_val);
            attr.set_value(t_val, 1, 0, true);
        } catch (std::exception& e) {
            Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "JsonAttr::read");
        }
    }

    bool is_allowed(Tango::DeviceImpl* dev, Tango::AttReqType ty) override { return true; }
};

} // namespace lima::tango
