// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tango.h>

#include <boost/describe.hpp>

#include <lima/tango/device_class.hpp>

namespace lima::tango
{

class device : public Tango::Device_5Impl
{
  public:
    using Device_5Impl::Device_5Impl;

  protected:
    // Read device properties from database
    template <typename T>
    void get_device_property(T& params)
    {
        // Read device properties from database
        Tango::DbData dev_prop;

        // Add a property per member
        boost::mp11::mp_for_each<
            boost::describe::describe_members<T, boost::describe::mod_any_access | boost::describe::mod_inherited>>(
            [&](auto D) { dev_prop.push_back(Tango::DbDatum(D.name)); });

        // Call database and read properties
        if (Tango::Util::instance()->_UseDb == true)
            get_db_device()->get_property(dev_prop);

        // Extract properties
        boost::mp11::mp_for_each<
            boost::describe::describe_members<T, boost::describe::mod_any_access | boost::describe::mod_inherited>>(
            [this, &dev_prop, &params, prop_idx = 0](auto D) mutable {
                auto& prop = params.*D.pointer;

                // And try to extract prop value from database
                if (dev_prop[prop_idx].is_empty() == false)
                    dev_prop[prop_idx] >> prop;
                else {
                    // Get and instance of receiver_class to get class properties
                    auto ds_class = (static_cast<lima::tango::device_class*>(get_device_class()));

                    // Try to initialize prop from class property
                    Tango::DbDatum cl_prop = ds_class->get_class_property(dev_prop[prop_idx].name);
                    if (cl_prop.is_empty() == false)
                        cl_prop >> prop;
                    else {
                        // Try to initialize prop from default device value
                        Tango::DbDatum def_prop = ds_class->get_default_device_property(dev_prop[prop_idx].name);
                        if (def_prop.is_empty() == false)
                            def_prop >> prop;
                    }
                }

                prop_idx++;
            });
    }
};

} // namespace lima::tango