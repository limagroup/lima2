// Copyright (C) 2024 Alejandro Homs, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tango.h>

namespace lima::tango
{

class attribute_lock_guard
{
  public:
    attribute_lock_guard(Tango::Attribute& attr) :
        mutex(attr.get_attr_mutex()), serial_model(attr.get_attr_serial_model())
    {
        if (serial_model == Tango::ATTR_BY_KERNEL)
            mutex->lock();
    }

    ~attribute_lock_guard()
    {
        if (serial_model == Tango::ATTR_BY_KERNEL)
            mutex->unlock();
    }

  private:
    omni_mutex* mutex;
    Tango::AttrSerialModel serial_model;
};

} // namespace lima::tango
