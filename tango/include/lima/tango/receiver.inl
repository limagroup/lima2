// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/exception/diagnostic_information.hpp>
#include <boost/mp11.hpp>
#include <boost/json/describe.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/string_generator.hpp>

#include <lima/log/severity_level.describe.hpp>
#include <lima/log/logger.hpp>
#include <lima/hw/params.describe.hpp>
#include <lima/utils/type_traits.hpp>

#include <lima/tango/attribute_lock_guard.hpp>
#include <lima/tango/convert.hpp>

namespace lima::tango
{
template <typename Receiver>
void receiver<Receiver>::init_device()
{
    DEBUG_STREAM << "receiver::init_device() create device " << device_name << std::endl;

    m_acq_params = boost::json::value_from(acq_params_t{});

    // Construct the receiver
    m_recv = std::make_unique<receiver_t>();

    // Log state change
    m_recv->register_on_state_change([this](acq_state_enum state) {
        // Push change event
        try {
            DEBUG_STREAM << "receiver::push_change_event(acq_state)" << std::endl;
            Tango::DevEnum* dev_state = new Tango::DevEnum(static_cast<Tango::DevShort>(state));
            {
                attribute_lock_guard lock(this->get_device_attr()->get_attr_by_name("acq_state"));
                this->push_change_event("acq_state", dev_state, 1, 0, true);
            }

            DEBUG_STREAM << "receiver::push_change_event(acq_state) DONE" << std::endl;
        } catch (CORBA::Exception& e) {
            ERROR_STREAM << "receiver::push_change_event(acq_state) FAILED" << std::endl;
            Tango::Except::print_exception(e);
        } catch (...) {
            ERROR_STREAM << "receiver::push_change_event(acq_state) FAILED" << std::endl;
        }

        // Update Tango state as well
        set_state(to_tango_state(state));
    });
}

template <typename Receiver>
void receiver<Receiver>::always_executed_hook()
{
    //DEBUG_STREAM << "receiver::always_executed_hook()  " << device_name << std::endl;

    // code always executed before all requests
}

template <typename Receiver>
void receiver<Receiver>::read_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    //DEBUG_STREAM << "receiver::read_attr_hardware(vector<long> &attr_list) entering... " << std::endl;

    // Add your own code
}

template <typename Receiver>
void receiver<Receiver>::write_attr_hardware(TANGO_UNUSED(std::vector<long>& attr_list))
{
    //DEBUG_STREAM << "control::write_attr_hardware(vector<long> &attr_list) entering... " << std::endl;

    // Add your own code
}

template <typename Receiver>
void receiver<Receiver>::add_dynamic_attributes()
{
    // Add your own code to create and add dynamic attributes if any
}

//template <typename AnyProc, typename... Args>
//auto variant_from_index(std::size_t index, typename AnyProc::proc_params_t const& proc_params, Args const&... args)
//    -> AnyProc
//{
//    return boost::mp11::mp_with_index<boost::mp11::mp_size<AnyProc>>(
//        index, [&](auto I) { return AnyProc(std::in_place_index<I>, args..., std::get<I>(proc_params)); });
//}

template <typename Receiver>
receiver<Receiver>::acq_info_t receiver<Receiver>::prepare()
{
    DEBUG_STREAM << "receiver::prepare()  - " << device_name << std::endl;

    try {
        auto acq_params = boost::json::value_to<acq_params_t>(m_acq_params);
        return m_recv->prepare_acq(acq_params);
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "receiver::prepare");
    }
}

//template <typename Receiver>
//void receiver<Receiver>::bind_pipeline(processing_device uuid_arg)
//{
//    DEBUG_STREAM << "receiver::prepare(uuid)  - " << device_name << std::endl;
//
//    std::shared_ptr<processing_api> proc; /* get the receiver */
//
//    try {
//        // Deserialize the current uuid
//        boost::uuids::string_generator gen;
//        boost::uuids::uuid uuid = gen(uuid_arg);
//
//        //if constexpr (is_any_pipeline_v<Processing>) {
//        //    proc = std::make_shared<processing_t>(variant_from_index<processing_t>(
//        //        m_proc_params.index(), m_proc_params, m_recv->recv_rank(), acq_info, m_acq_params));
//        //} else
//        //    proc = std::make_shared<processing_t>(m_recv->recv_rank(), acq_info, m_acq_params, m_proc_params);
//
//        m_current_uuid = uuid;
//
//        if (proc) {
//            auto register_callbacks = [this](auto&& proc) {
//                // Register the callbacks
//                m_recv->register_on_start_acq([&proc]() { proc.activate(); });
//                m_recv->register_on_frame_ready([&proc](auto frame) { proc.process(frame); });
//                m_recv->register_on_end_acq([this](int nb_frames) {
//                    Tango::DevLong* xferred_frames = new Tango::DevLong(nb_frames);
//                    try {
//                        DEBUG_STREAM << "receiver::push_change_event(on_acq_end)" << std::endl;
//                        push_change_event("nb_frames_xferred", xferred_frames, 1, 0, true);
//                    } catch (CORBA::Exception& e) {
//                        ERROR_STREAM << "receiver::push_change_event(on_acq_end) FAILED" << std::endl;
//                        Tango::Except::print_exception(e);
//                    } catch (...) {
//                        ERROR_STREAM << "receiver::push_change_event(on_acq_end) FAILED" << std::endl;
//                    }
//                });
//            };
//
//            //if constexpr (is_any_pipeline_v<Processing>)
//            //    std::visit([this, register_callbacks](auto&& proc) { register_callbacks(proc); }, *proc);
//            //else
//            //    register_callbacks(*proc);
//
//            register_callbacks(*proc);
//        } else
//            Tango::Except::throw_exception("LIMA_Exception", "Failed to construct processing", "receiver::prepare");
//
//    } catch (std::exception& e) {
//        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "receiver::prepare");
//    }
//}

template <typename Receiver>
void receiver<Receiver>::bind_pipeline(processing_device* proc)
{
    DEBUG_STREAM << "receiver::bind_pipeline()  - " << device_name << std::endl;

    // Register the callbacks
    m_recv->register_on_start_acq([proc]() { proc->activate(); });
    m_recv->register_on_frame_ready([proc](auto frame) { proc->process(frame); });
    m_recv->register_on_end_acq([this](int nb_frames) {
        try {
            DEBUG_STREAM << "receiver::push_data_ready_event(on_acq_end)" << std::endl;
            {
                attribute_lock_guard lock(this->get_device_attr()->get_attr_by_name("nb_frames_xferred"));
                this->push_data_ready_event("nb_frames_xferred");
            }
            DEBUG_STREAM << "receiver::push_data_ready_event(on_acq_end) DONE" << std::endl;
        } catch (CORBA::Exception& e) {
            ERROR_STREAM << "receiver::push_data_ready_event(on_acq_end) FAILED" << std::endl;
            Tango::Except::print_exception(e);
        } catch (...) {
            ERROR_STREAM << "receiver::push_data_ready_event(on_acq_end) FAILED" << std::endl;
        }
    });
}

template <typename Receiver>
void receiver<Receiver>::start()
{
    DEBUG_STREAM << "receiver::start()  - " << device_name << std::endl;
    try {
        m_recv->start_acq();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "receiver::start");
    }
}

template <typename Receiver>
void receiver<Receiver>::stop()
{
    DEBUG_STREAM << "receiver::stop()  - " << device_name << std::endl;

    try {
        m_recv->stop_acq();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "receiver::stop");
    }
}

template <typename Receiver>
void receiver<Receiver>::close()
{
    DEBUG_STREAM << "receiver::close()  - " << device_name << std::endl;

    try {
        m_recv->close_acq();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "receiver::close");
    }
}

template <typename Receiver>
void receiver<Receiver>::reset()
{
    DEBUG_STREAM << "receiver::reset()  - " << device_name << std::endl;

    try {
        m_recv->reset_acq();
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "receiver::reset");
    }
}

template <typename Receiver>
acq_state_enum* receiver<Receiver>::acq_state() const
{
    //DEBUG_STREAM << "receiver::acq_state()  - " << device_name << std::endl;

    acq_state_enum* res;

    try {
        acq_state_enum state = m_recv->state();
        res = new acq_state_enum(state);
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true), "receiver::acq_state");
    }

    return res;
}

template <typename Receiver>
std::pair<Tango::DevString*, std::size_t> receiver<Receiver>::list_pipelines()
{
    DEBUG_STREAM << "receiver::list_pipelines()  - " << device_name << std::endl;

    Tango::DevString* res = new Tango::DevString[max_nb_pipelines];
    std::size_t size = 0;

    {
        std::lock_guard<std::mutex> guard(m_procs_mutex);
        size = std::min(m_procs.size(), max_nb_pipelines);

        for (std::size_t i = 0; auto&& proc : m_procs) {
            res[i] = Tango::string_dup(boost::uuids::to_string(proc).c_str());
            i++;
        }
    }

    return std::make_pair(res, size);
}

template <typename Receiver>
void receiver<Receiver>::create_pipeline(Tango::DevString uuid)
{
    DEBUG_STREAM << "receiver::create_pipeline()  - " << device_name << std::endl;
    try {
        // Deserialize the current uuid
        boost::uuids::string_generator gen;

        {
            std::lock_guard<std::mutex> guard(m_procs_mutex);
            auto [iterator, inserted] = m_procs.insert(gen(uuid));
            if (!inserted)
                LIMA_THROW_EXCEPTION(lima::runtime_error("Failed to create processing, uuid already exists"));
        }
        
        //Update the current pipeline
        m_current_uuid = gen(uuid);
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "receiver::create_pipeline");
    }
}

template <typename Receiver>
Tango::DevString* receiver<Receiver>::current_pipeline()
{
    DEBUG_STREAM << "receiver::current_pipeline()  - " << device_name << std::endl;

    try {
        Tango::DevString* res = new Tango::DevString();

        if (!m_current_uuid.is_nil())
            *res = Tango::string_dup(boost::uuids::to_string(m_current_uuid));
        else
            *res = Tango::string_dup("");

        return res;
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "receiver::current_pipeline");
    }
}

template <typename Receiver>
void receiver<Receiver>::erase_pipeline(Tango::DevString uuid)
{
    DEBUG_STREAM << "receiver::erase_pipeline()  - " << device_name << std::endl;

    try {
        boost::uuids::string_generator gen;
        {
            std::lock_guard<std::mutex> guard(m_procs_mutex);
            m_procs.erase(gen(uuid));
        }

        auto [pipelines, size] = list_pipelines();
        {
            attribute_lock_guard lock(this->get_device_attr()->get_attr_by_name("pipelines"));
            this->push_change_event("pipelines", pipelines, size, 0, true);
        }        
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "receiver::erase_pipeline");
    }
}

template <typename Receiver>
bool receiver<Receiver>::is_current_pipeline(Tango::DevString uuid) const
{
    try {
        boost::uuids::string_generator gen;
        return gen(uuid) == m_current_uuid;
    } catch (std::exception& e) {
        Tango::Except::throw_exception("LIMA_Exception", boost::diagnostic_information(e, true),
                                       "receiver::is_current_pipeline");
    }
}

template <typename Receiver>
void receiver<Receiver>::add_dynamic_commands()
{
    // Add your own code to create and add dynamic commands if any
}

// Additional Methods

} // namespace lima::tango
