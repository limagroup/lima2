// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tango.h>

#include <lima/tango/convert.hpp>

namespace lima::tango
{

template <typename Device>
class world_rank : public Tango::Attr
{
  public:
    world_rank() : Attr("world_rank", Tango::DEV_LONG, Tango::READ){};
    virtual void read(Tango::DeviceImpl* dev, Tango::Attribute& att)
    {
        int world_rank = (static_cast<Device*>(dev))->world_rank();

        Tango::DevLong* t_world_rank = new Tango::DevLong();
        tango_encode(world_rank, *t_world_rank);

        att.set_value(t_world_rank, 1, 0, true);
    }
};

} // namespace lima::tango