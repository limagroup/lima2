// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <cstring>
#include <vector>

#include <tango.h>

#include <lima/core/frame_typedefs.hpp>
#include <lima/core/frame.hpp>

namespace lima::tango
{
enum class category_enum : std::int32_t
{
    scalar_stack,
    spectrum,
    image,
    spectrum_stack,
    image_stack
};

enum class data_type_enum : std::int32_t
{
    uint8 = 0,
    uint16,
    uint32,
    uint64,
    int8,
    int16,
    int32,
    int64,
    float32,
    float64,
};

enum class endianess_enum : std::uint16_t
{
    little,
    big
};

#pragma pack(push, 1)

struct data_array_header
{
    static const int data_array_header_len = 64;

    std::uint32_t magic = 0x44544159;
    std::uint16_t version = 3;
    std::uint16_t length = data_array_header_len;
    category_enum category;
    data_type_enum data_type;
    endianess_enum endianess;
    std::uint16_t nb_dims;
    std::array<std::int16_t, 6> dims;
    std::array<std::int32_t, 6> stepsbytes;
    std::uint64_t frame_dix = 0;
};

#pragma pack(pop)

inline data_type_enum data_type(pixel_enum px)
{
    data_type_enum res;

    switch (px) {
    case pixel_enum::gray8:
        res = data_type_enum::uint8;
        break;
    case pixel_enum::gray8s:
        res = data_type_enum::int8;
        break;
    case pixel_enum::gray16:
        res = data_type_enum::uint16;
        break;
    case pixel_enum::gray16s:
        res = data_type_enum::int16;
        break;
    case pixel_enum::gray32:
        res = data_type_enum::uint32;
        break;
    case pixel_enum::gray32s:
        res = data_type_enum::int32;
        break;
    case pixel_enum::gray32f:
        res = data_type_enum::float32;
        break;
    default:
        assert(0);
    }

    return res;
}

// clang-format off
inline data_type_enum data_type(bpp8_pixel_t) { return data_type_enum::uint8; }
inline data_type_enum data_type(bpp8s_pixel_t) { return data_type_enum::int8; }
inline data_type_enum data_type(bpp16_pixel_t) { return data_type_enum::uint16; }
inline data_type_enum data_type(bpp16s_pixel_t) { return data_type_enum::int16; }
inline data_type_enum data_type(bpp32_pixel_t) { return data_type_enum::uint32; }
inline data_type_enum data_type(bpp32s_pixel_t) { return data_type_enum::int32; }
inline data_type_enum data_type(bpp32f_pixel_t) { return data_type_enum::float32; }
// clang-format on

inline Tango::DevEncoded make_devencoded(frame const& frm)
{
    Tango::DevEncoded res;
    res.encoded_format = "DATA_ARRAY";
    res.encoded_data.length(sizeof(data_array_header) + frm.nb_channels() * frm.channel_size_in_bytes());

    data_array_header header;
    header.category = category_enum::image;
    header.data_type = data_type(frm.pixel_type());
    header.endianess = endianess_enum::little;
    header.nb_dims = 3;
    header.dims[0] = frm.width();
    header.dims[1] = frm.height();
    header.dims[2] = frm.nb_channels();
    header.stepsbytes[0] = sizeof_pixel(frm.pixel_type());
    header.stepsbytes[1] = frm.rowsize;
    header.stepsbytes[2] = frm.channel_size_in_bytes();
    header.frame_dix = frm.metadata.idx;

    unsigned char* buffer = res.encoded_data.get_buffer();

    std::memcpy(buffer, &header, sizeof(data_array_header));
    std::memcpy(buffer + sizeof(data_array_header), frm.data, frm.channel_size_in_bytes());

    return res;
}

template <typename Loc>
inline Tango::DevEncoded make_devencoded(boost::gil::image_view<Loc> const& view)
{
    using pixel_t = typename boost::gil::image_view<Loc>::value_type;

    assert(view.is_1d_traversable());
    std::size_t size_in_bytes = view.size() * sizeof(pixel_t);

    Tango::DevEncoded res;
    res.encoded_format = "DATA_ARRAY";
    res.encoded_data.length(sizeof(data_array_header) + size_in_bytes);

    data_array_header header;
    header.category = category_enum::image;
    header.data_type = data_type(pixel_t());
    header.endianess = endianess_enum::little;
    header.nb_dims = 2;
    header.dims[0] = view.width();
    header.dims[1] = view.height();
    header.stepsbytes[0] = sizeof(pixel_t);
    header.stepsbytes[1] = view.width() * sizeof(pixel_t);

    unsigned char* buffer = res.encoded_data.get_buffer();

    std::memcpy(buffer, &header, sizeof(data_array_header));
    std::memcpy(buffer + sizeof(data_array_header), &view(0, 0), size_in_bytes);

    return res;
}

//uint8 = 0, uint16, uint32, uint64, int8, int16, int32, int64, float32, float64,

// clang-format off
template <typename T> struct infer_data_type;
template <> struct infer_data_type<std::uint8_t> { static const data_type_enum type = data_type_enum::uint8; };
template <> struct infer_data_type<std::uint16_t> { static const data_type_enum type = data_type_enum::uint16; };
template <> struct infer_data_type<std::uint32_t> { static const data_type_enum type = data_type_enum::uint32; };
template <> struct infer_data_type<std::uint64_t> { static const data_type_enum type = data_type_enum::uint64; };
template <> struct infer_data_type<std::int8_t> { static const data_type_enum type = data_type_enum::int8; };
template <> struct infer_data_type<std::int16_t> { static const data_type_enum type = data_type_enum::int16; };
template <> struct infer_data_type<std::int32_t> { static const data_type_enum type = data_type_enum::int32; };
template <> struct infer_data_type<std::int64_t> { static const data_type_enum type = data_type_enum::int64; };
template <> struct infer_data_type<float> { static const data_type_enum type = data_type_enum::float32; };
template <> struct infer_data_type<double> { static const data_type_enum type = data_type_enum::float64; };

template <typename T> constexpr data_type_enum infer_data_type_v = infer_data_type<T>::type;
// clang-format on

template <typename T>
Tango::DevEncoded make_devencoded(std::vector<T> const& data, category_enum category, std::uint16_t width,
                                  std::uint16_t height)
{
    Tango::DevEncoded res;
    res.encoded_format = "DATA_ARRAY";
    res.encoded_data.length(sizeof(data_array_header) + data.size() * sizeof(T));

    data_array_header header;
    header.category = category;
    header.data_type = infer_data_type_v<T>;
    header.endianess = endianess_enum::little;
    header.nb_dims = 2; //TODO
    header.dims[0] = width;
    header.dims[1] = height;
    header.stepsbytes[0] = sizeof(T);
    header.stepsbytes[1] = width * sizeof(T);

    unsigned char* buffer = res.encoded_data.get_buffer();

    std::memcpy(buffer, &header, sizeof(data_array_header));
    std::memcpy(buffer + sizeof(data_array_header), data.data(), data.size());

    return res;
}

/// Make DevEncoded for Structured Arrays
///
/// \template T is a Described struct
template <typename T>
Tango::DevEncoded make_devencoded(std::vector<T> const& data)
{
    Tango::DevEncoded res;
    res.encoded_format = "STRUCTURED_ARRAY";
    res.encoded_data.length(sizeof(data_array_header) + data.size() * sizeof(T));

    data_array_header header;
    header.category = category_enum::scalar_stack; // not used
    header.data_type = data_type_enum::uint8;      // not used;
    header.endianess = endianess_enum::little;
    header.nb_dims = 1; // not used
    header.dims[0] = data.size();
    header.stepsbytes[0] = sizeof(T);

    unsigned char* buffer = res.encoded_data.get_buffer();

    std::memcpy(buffer, &header, sizeof(data_array_header));
    std::memcpy(buffer + sizeof(data_array_header), data.data(), data.size() * sizeof(T));

    return res;
}

} // namespace lima::tango