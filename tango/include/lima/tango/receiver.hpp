// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <functional>
#include <mutex>
#include <set>
#include <variant>

#include <tango.h>

#include <boost/uuid/nil_generator.hpp>
#include <boost/json/value.hpp>
#include <boost/json/serialize.hpp>

#include <lima/exceptions.hpp>
#include <lima/core/enums.hpp>
#include <lima/core/frame.hpp>

#include <lima/tango/device.hpp>
#include <lima/tango/processing_device.hpp>
#include <lima/tango/init_params.hpp>

namespace lima::tango
{
template <typename Receiver>
class receiver : public device
{
  public:
    using receiver_t = Receiver;

    using init_params_t = typename Receiver::init_params_t;
    using acq_params_t = typename Receiver::acq_params_t;
    using acq_info_t = typename Receiver::acq_info_t;
    using data_t = typename Receiver::data_t;

    static constexpr std::size_t max_nb_pipelines = 256;

    // Constructors and destructors
    receiver(Tango::DeviceClass* cl, const char* name) : device(cl, name) { init_device(); }
    receiver(Tango::DeviceClass* cl, const char* name, const char* description) : device(cl, name, description)
    {
        init_device();
    }
    ~receiver() { delete_device(); }

    /// Miscellaneous methods
    ///{
    /// Will be called at device destruction or at init command
    void delete_device() override { DEBUG_STREAM << "receiver::delete_device() " << device_name << std::endl; }

    /// Initialize the device
    void init_device() override;

    ///// Read database to initialize property data members
    //void get_device_property();

    /// Always executed method before execution command method
    void always_executed_hook() override;
    ///}

    /// Attribute related methods
    ///{

    /// Hardware acquisition for attributes
    virtual void read_attr_hardware(std::vector<long>& attr_list) override;

    /// Hardware writing for attributes
    virtual void write_attr_hardware(std::vector<long>& attr_list) override;

    /// Add dynamic attributes if any
    void add_dynamic_attributes();
    ///}

    /// Command related methods
    ///{

    // Acquisition
    acq_info_t prepare();
    //void bind_pipeline(Tango::DevString uuid);
    void bind_pipeline(processing_device* proc);
    void start();
    void stop();
    void close();
    void reset();

    // Processing
    void create_pipeline(Tango::DevString uuid);
    Tango::DevString* current_pipeline();
    std::pair<Tango::DevString*, std::size_t> list_pipelines();
    void erase_pipeline(Tango::DevString uuid);
    bool is_current_pipeline(Tango::DevString uuid) const;

    /// Add dynamic commands if any
    void add_dynamic_commands();

    ///}

    // Additional Method prototypes
    int world_rank() { return m_recv->world_rank(); }
    int recv_rank() { return m_recv->recv_rank(); }

    acq_state_enum* acq_state() const;

    int nb_frames_xferred() { return m_recv->nb_frames_xferred(); }

    void register_on_start_acq(std::function<void()> cbk) { m_recv->register_on_start_acq(cbk); }
    void register_on_frame_ready(std::function<void(data_t)> cbk) { m_recv->register_on_frame_ready(cbk); }
    void register_on_end_acq(std::function<void(int)> cbk) { m_recv->register_on_end_acq(cbk); }

    // Device property data members
    boost::json::value m_acq_params;
    boost::json::value m_proc_params;

    Tango::DevShort m_proc_class = 0;

  private:
    std::unique_ptr<receiver_t> m_recv;
    std::set<boost::uuids::uuid> m_procs;
    std::mutex m_procs_mutex;
    boost::uuids::uuid m_current_uuid = boost::uuids::nil_uuid();

    static Tango::DevState to_tango_state(acq_state_enum s)
    {
        Tango::DevState res;

        switch (s) {
        case acq_state_enum::idle:
            res = Tango::ON;
            break;

        case acq_state_enum::prepared:
            res = Tango::INIT;
            break;

        case acq_state_enum::running:
        case acq_state_enum::stopped:
            res = Tango::RUNNING;
            break;

        case acq_state_enum::fault:
        case acq_state_enum::terminate:
            res = Tango::FAULT;
            break;

        default:
            res = Tango::DISABLE;
        }

        return res;
    }
};

// Additional Classes Definitions

} // namespace lima::tango

#include <lima/tango/receiver_class.hpp>

#include "receiver.inl"