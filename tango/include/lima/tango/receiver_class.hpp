// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <vector>
#include <string>
#include <string_view>

#include <lima/tango/device_class.hpp>
#include <lima/tango/processing_class.hpp>
#include <lima/tango/receiver.hpp>

namespace lima::tango
{
/// The receiver_class singleton definition
template <typename Receiver>
class receiver_class : public singleton_device_class<receiver_class<Receiver>>
{
  public:
    using base_t = singleton_device_class<receiver_class<Receiver>>;
    using device_t = receiver<Receiver>;

    template <typename T1, typename T2>
    friend class singleton_device_class;

  protected:
    receiver_class(std::string const& name) : base_t(name)
    {
        TANGO_LOG_INFO << "Entering receiver_class constructor" << std::endl;

        set_default_property();
        write_class_property();

        TANGO_LOG_INFO << "Leaving receiver_class constructor" << std::endl;
    }

    /// Create the command object(s) and store them in the command list
    void command_factory();

    /// Create the attribute object(s) and store them in the attribute list
    void attribute_factory(std::vector<Tango::Attr*>&);

    /// Create the pipe object(s) and store them in the pipe list
    void pipe_factory();

    /// Properties management
    ///{

    ///  Set class description fields as property in database
    void write_class_property();

    /// Set default property (class and device) for wizard.
    /// For each property, add to wizard property name and description.
    /// If default value has been set, add it to wizard property and
    /// store it in a DbDatum.
    void set_default_property();

    ///}

    //std::string get_cvstag();
    //std::string get_cvsroot();

  private:
    //// Write class properties data members
    //Tango::DbData cl_prop;
    //Tango::DbData cl_def_prop;
    //Tango::DbData dev_def_prop;

    /// Factory methods

    /// Create the device object(s) and store them in the device list
    void device_factory(const Tango::DevVarStringArray*);

    /// Create the a list of static attributes
    ///
    /// \param att_list	the ceated attribute list
    void create_static_attribute_list(std::vector<Tango::Attr*>&);

    /// Delete the dynamic attributes if any
    ///
    /// \param devlist_ptr the device list pointer
    /// /// \param  list of all attributes
    void erase_dynamic_attributes(const Tango::DevVarStringArray*, std::vector<Tango::Attr*>&);

    /// Returns Tango::Attr * object found by name
    Tango::Attr* get_attr_object_by_name(std::vector<Tango::Attr*>& att_list, std::string attname);

    std::vector<std::string> defaultAttList; //!< Default attibute list
};

} // namespace lima::tango

#include "receiver_class.inl"