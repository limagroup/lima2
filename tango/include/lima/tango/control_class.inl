// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/json/chrono.hpp>
#include <boost/json/filesystem.hpp>
#include <boost/json/describe.hpp>
#include <boost/json/schema_from.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/mp11.hpp>

#include <lima/hw/params.describe.hpp>
#include <lima/hw/info.describe.hpp>
#include <lima/utils/type_traits.hpp>

#include <lima/tango/base_attribute.hpp>
#include <lima/tango/base_command.hpp>
#include <lima/tango/common_attribute.hpp>
#include <lima/tango/convert.hpp>

namespace lima::tango
{

//=========================================
//	Define classes for attributes
//=========================================

//=========================================
// Define classes for commands
//=========================================

//===================================================================
// Properties management
//===================================================================

template <typename Control>
void control_class<Control>::set_default_property()
{
    std::vector<std::string> vect_data;

    // Set Default Class Properties

    // Set Default device Properties
    using Desc = BOOST_DESCRIBE_MAKE_NAME(desc);
    using Doc = BOOST_DESCRIBE_MAKE_NAME(doc);

    auto create_prop = [&](auto params) {
        using params_t = std::decay_t<decltype(params)>;

        boost::mp11::mp_for_each<boost::describe::describe_members<params_t, boost::describe::mod_any_access
                                                                                 | boost::describe::mod_inherited>>(
            [&](auto D) {
                using A = boost::describe::annotate_member<decltype(D)>;
                using param_t = member_type<decltype(D.pointer)>;

                auto val = params.*D.pointer;
                auto desc = boost::describe::annotation_by_name_v<A, Desc>;
                auto doc = boost::describe::annotation_by_name_v<A, Doc>;

                std::string prop_name = D.name;
                std::string prop_desc = doc;
                std::string prop_def = boost::lexical_cast<std::string>(val);
                vect_data.clear();
                vect_data.push_back(prop_def);
                if (prop_def.length() > 0) {
                    Tango::DbDatum data(prop_name);
                    data << vect_data;
                    this->dev_def_prop.push_back(data);
                    this->add_wiz_dev_prop(prop_name, prop_desc, prop_def);
                } else
                    this->add_wiz_dev_prop(prop_name, prop_desc);
            });
    };

    // Add hw_init_params properties
    typename device_t::ds_init_params_t ds_init_params;
    ds_init_params.plugin_params = boost::json::serialize(boost::json::value_from(typename device_t::init_params_t{}));
    create_prop(ds_init_params);

    // Generate the schema for the plugin init_params
    boost::json::value schema = boost::json::schema_from<typename device_t::init_params_t>("init_params");

    // Add a "schema" class attribute property
    Tango::DbDatum db_nb_attrs("init_params");
    Tango::DbDatum db_schema("schema");

    db_nb_attrs << 1; // One "schema" property for attribute "params"
    std::string schema_str =
        boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
    db_schema << schema_str;

    // Push to the DB
    Tango::DbData db_data;
    db_data.push_back(db_nb_attrs);
    db_data.push_back(db_schema);

    this->get_db_class()->put_attribute_property(db_data);
}

template <typename Control>
void control_class<Control>::write_class_property()
{
    // First time, check if database used
    if (Tango::Util::_UseDb == false)
        return;

    Tango::DbData data;
    std::string classname = this->get_name();
    std::string header;
    std::string::size_type start, end;

    // Put title
    Tango::DbDatum title("ProjectTitle");
    std::string str_title("LIMA2");
    title << str_title;
    data.push_back(title);

    // Put Description
    Tango::DbDatum description("Description");
    std::vector<std::string> str_desc;
    str_desc.push_back("LIMA2 Control Class");
    description << str_desc;
    data.push_back(description);

    //  Put inheritance
    Tango::DbDatum inher_datum("InheritedFrom");
    std::vector<std::string> inheritance;
    inheritance.push_back("TANGO_BASE_CLASS");
    inher_datum << inheritance;
    data.push_back(inher_datum);

    // Call database and and values
    this->get_db_class()->put_property(data);
}

//===================================================================
// Factory methods
//===================================================================

template <typename Control>
void control_class<Control>::device_factory(const Tango::DevVarStringArray* devlist_ptr)
{
    // Create devices and add it into the device list
    for (unsigned long i = 0; i < devlist_ptr->length(); i++) {
        TANGO_LOG_DEBUG << "Device name : " << (*devlist_ptr)[i].in() << std::endl;
        this->device_list.push_back(new device_t(this, (*devlist_ptr)[i]));
    }

    // Manage dynamic attributes if any
    erase_dynamic_attributes(devlist_ptr, this->get_class_attr()->get_attr_list());

    // Export devices to the outside world
    for (unsigned long i = 1; i <= devlist_ptr->length(); i++) {
        // Add dynamic attributes if any
        device_t* dev = static_cast<device_t*>(this->device_list[this->device_list.size() - i]);
        dev->add_dynamic_attributes();

        // Check before if database used.
        if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
            this->export_device(dev);
        else
            this->export_device(dev, dev->get_name().c_str());
    }
}

template <typename Control>
void control_class<Control>::attribute_factory(std::vector<Tango::Attr*>& att_list)
{
    //auto create_attr = [&](auto D) {
    //    using A = boost::describe::annotate_member<decltype(D)>;
    //    using param_t = member_type<decltype(D.pointer)>;

    //    auto desc = boost::describe::annotation_by_idx_v<A, 0>;
    //    auto doc = boost::describe::annotation_by_idx_v<A, 1>;

    //    constexpr long dtype = tango_dtype_v<param_t>;

    //    // If data type is supported
    //    if constexpr (dtype) {
    //        GenericAttr* attr = new GenericAttr(D.name, tango_dtype_v<param_t>, Tango::READ_WRITE);

    //        Tango::UserDefaultAttrProp attr_prop;

    //        // If enumeration, add labels
    //        if constexpr (std::is_enum_v<param_t>) {
    //            std::vector<std::string> labels;

    //            boost::mp11::mp_for_each<boost::describe::describe_enumerators<param_t>>(
    //                [&labels](auto D) { labels.push_back(D.name); });

    //            attr_prop.set_enum_labels(labels);
    //        }

    //        attr_prop.description = doc;
    //        attr_prop.label = desc;
    //        // description
    //        // label
    //        // unit
    //        // standard_unit
    //        // display_unit
    //        // format
    //        // max_value
    //        // min_value
    //        // max_alarm
    //        // min_alarm
    //        // max_warning
    //        // min_warning
    //        // delta_t
    //        // delta_val

    //        attr->set_default_properties(attr_prop);
    //        // Not Polled
    //        attr->set_disp_level(Tango::OPERATOR);
    //        // Not Memorized
    //        attr->set_change_event(true, false);

    //        // Setup the read implementation
    //        attr->read_impl = [&D](Tango::DeviceImpl* dev, Tango::Attribute& attr) {
    //            //DEBUG_STREAM << "Reading attribute " << D.name << std::endl;

    //            device_t* ctrl = static_cast<device_t*>(dev);
    //            auto& val = ctrl->m_acq_params.*D.pointer;

    //            // Set the attribute value
    //            tango_dtype_t<param_t> t_val;
    //            tango_encode(val, t_val);
    //            attr.set_value(&t_val);
    //        };

    //        // Setup the write implementation
    //        attr->write_impl = [&D](Tango::DeviceImpl* dev, Tango::WAttribute& attr) {
    //            //DEBUG_STREAM << "Writting attribute " << D.name << std::endl;

    //            device_t* ctrl = static_cast<device_t*>(dev);
    //            auto& val = ctrl->m_acq_params.*D.pointer;

    //            // Retrieve write value
    //            tango_dtype_t<param_t> t_val;
    //            attr.get_write_value(t_val);
    //            tango_decode(t_val, val);
    //        };

    //        att_list.push_back(attr);
    //    }
    //};

    //// Add an attribute per member
    //boost::mp11::mp_for_each<
    //    boost::describe::describe_members<device_t::acq_params_t, boost::describe::mod_any_access>>(create_attr);

    // Add acq_params JSON attribute
    {
        using acq_params_attr_t = JsonAttr<device_t, &device_t::m_acq_params>;
        acq_params_attr_t* attr = new acq_params_attr_t("acq_params", Tango::DEV_STRING, Tango::READ_WRITE);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Acquisition parameters";
        attr_prop.label = "acq_params";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add acq_params JSON schema attribute property
    {
        // Generate the schema for the plugin acq_params
        boost::json::value schema = boost::json::schema_from<typename device_t::acq_params_t>("acq_params");

        // Add a "schema" class attribute property
        Tango::DbDatum db_nb_attrs("acq_params");
        Tango::DbDatum db_schema("schema");

        db_nb_attrs << 1; // One "schema" property for attribute "params"
        std::string schema_str =
            boost::json::serialize(schema); // Workaround https://gitlab.com/tango-controls/cppTango/-/issues/622
        db_schema << schema_str;

        // Push to the DB
        Tango::DbData db_data;
        db_data.push_back(db_nb_attrs);
        db_data.push_back(db_schema);

        this->get_db_class()->put_attribute_property(db_data);
    }

    // Add det_info JSON attribute
    {
        using det_info_attr_t = JsonReadAttr<device_t, &device_t::det_info>;
        det_info_attr_t* attr = new det_info_attr_t("det_info", Tango::DEV_STRING, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Detector information";
        attr_prop.label = "det_info";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add det_status JSON attribute
    {
        using det_status_attr_t = JsonReadAttr<device_t, &device_t::det_status>;
        det_status_attr_t* attr = new det_status_attr_t("det_status", Tango::DEV_STRING, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Detector status";
        attr_prop.label = "det_status";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add det_capabilities JSON attribute
    {
        using det_capabilities_attr_t = JsonReadAttr<device_t, &device_t::det_capabilities>;
        det_capabilities_attr_t* attr = new det_capabilities_attr_t("det_capabilities", Tango::DEV_STRING, Tango::READ);

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Detector capabilities";
        attr_prop.label = "det_capabilities";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add nb_frames_acquired attribute
    {
        struct NbFrameAcquiredAttr : Tango::Attr
        {
            NbFrameAcquiredAttr() : Tango::Attr("nb_frames_acquired", Tango::DEV_LONG, Tango::READ) {}

            void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
            {
                device_t* ctrl = static_cast<device_t*>(dev);
                attr.set_value(ctrl->nb_frames_acquired(), 1, 0, true);
            }
        };

        auto attr = new NbFrameAcquiredAttr();

        Tango::UserDefaultAttrProp attr_prop;

        attr_prop.description = "Number of frame acquired (in the detector)";
        attr_prop.label = "nb_frames_acquired";

        attr->set_default_properties(attr_prop);

        att_list.push_back(attr);
    }

    // Add acq_state attribute
    {
        struct AcqStateEnumAttr : public EnumAttr<acq_state_enum>
        {
            AcqStateEnumAttr() : EnumAttr<acq_state_enum>("acq_state", "State of the controller") {}

            void read(Tango::DeviceImpl* dev, Tango::Attribute& attr) override
            {
                attr.set_value(static_cast<device_t*>(dev)->acq_state(), 1, 0, true);
            }
        };

        auto attr = new AcqStateEnumAttr();

        // Not Polled
        attr->set_disp_level(Tango::OPERATOR);
        // Not Memorized
        attr->set_change_event(true, false);

        att_list.push_back(attr);
    }

    // Create a list of static attributes
    create_static_attribute_list(this->get_class_attr()->get_attr_list());
}

template <typename Control>
void control_class<Control>::pipe_factory()
{
}

template <typename Control>
void control_class<Control>::command_factory()
{
    // Command Prepare
    this->command_list.emplace_back(new prepare_command<device_t>());

    // Command Start
    this->command_list.emplace_back(new start_command<device_t>());

    // Command Trigger
    this->command_list.emplace_back(new trigger_command<device_t>());

    // Command Stop
    this->command_list.emplace_back(new stop_command<device_t>());

    // Command Close
    this->command_list.emplace_back(new close_command<device_t>());

    // Command Reset
    this->command_list.emplace_back(new reset_command<device_t>());
}

//===================================================================
// Dynamic attributes related methods
//===================================================================

template <typename Control>
void control_class<Control>::create_static_attribute_list(std::vector<Tango::Attr*>& att_list)
{
    for (unsigned long i = 0; i < att_list.size(); i++) {
        std::string att_name(att_list[i]->get_name());
        transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
        defaultAttList.push_back(att_name);
    }

    TANGO_LOG_INFO << defaultAttList.size() << " attributes in default list" << std::endl;
}

template <typename Control>
void control_class<Control>::erase_dynamic_attributes(const Tango::DevVarStringArray* devlist_ptr,
                                                      std::vector<Tango::Attr*>& att_list)
{
    Tango::Util* tg = Tango::Util::instance();

    for (unsigned long i = 0; i < devlist_ptr->length(); i++) {
        Tango::DeviceImpl* dev_impl = tg->get_device_by_name(((std::string)(*devlist_ptr)[i]).c_str());
        device_t* dev = static_cast<device_t*>(dev_impl);

        std::vector<Tango::Attribute*>& dev_att_list = dev->get_device_attr()->get_attribute_list();
        for (auto ite_att = dev_att_list.begin(); ite_att != dev_att_list.end(); ++ite_att) {
            std::string att_name((*ite_att)->get_name_lower());
            if ((att_name == "state") || (att_name == "status"))
                continue;
            auto ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
            if (ite_str == defaultAttList.end()) {
                TANGO_LOG_INFO << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i]
                               << std::endl;
                Tango::Attribute& att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
                dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
                --ite_att;
            }
        }
    }
}

template <typename Control>
Tango::Attr* control_class<Control>::get_attr_object_by_name(std::vector<Tango::Attr*>& att_list, std::string attname)
{
    for (auto&& att : att_list)
        if (att->get_name() == attname)
            return att;

    // Attr does not exist
    return NULL;
}

} // namespace lima::tango
