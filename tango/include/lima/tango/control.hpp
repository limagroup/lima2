// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include <tango.h>

#include <boost/json/value.hpp>

#include <lima/hw/info.hpp>
#include <lima/hw/status.hpp>

#include <lima/tango/device.hpp>
#include <lima/tango/device_class.hpp>
#include <lima/tango/init_params.hpp>

namespace lima::tango
{
template <typename Control>
class control : public device
{
  public:
    using control_t = Control;

    using ds_init_params_t = ctrl_init_params;

    using init_params_t = typename Control::init_params_t;
    using acq_params_t = typename Control::acq_params_t;
    using det_info_t = typename Control::det_info_t;
    using det_status_t = typename Control::det_status_t;
    using det_capabilities_t = typename Control::det_capabilities_t;

    // Constructors and destructors
    control(Tango::DeviceClass* cl, const char* name) : device(cl, name) { init_device(); }
    control(Tango::DeviceClass* cl, const char* name, const char* description) : device(cl, name, description)
    {
        init_device();
    }
    ~control() { delete_device(); }

    /// Miscellaneous methods
    ///{
    /// Will be called at device destruction or at init command
    void delete_device() override { DEBUG_STREAM << "control::delete_device() " << device_name << std::endl; }

    /// Initialize the device
    void init_device() override;

    ///// Read database to initialize property data members
    //void get_device_property();

    /// Always executed method before execution command method
    void always_executed_hook() override;
    ///}

    /// Attribute related methods
    ///{

    /// Hardware acquisition for attributes
    virtual void read_attr_hardware(std::vector<long>& attr_list) override;

    /// Hardware writing for attributes
    virtual void write_attr_hardware(std::vector<long>& attr_list) override;

    /// Add dynamic attributes if any
    void add_dynamic_attributes();
    ///}

    /// Command related methods
    ///{

    void prepare(Tango::DevString uuid);
    void start();
    void trigger();
    void stop();
    void close();
    void reset();

    ///}

    /// Status related methods
    ///{

    det_info_t det_info();
    det_status_t det_status();
    det_capabilities_t det_capabilities();
    int world_rank();

    acq_state_enum* acq_state() const;
    Tango::DevLong* nb_frames_acquired() const;

    ///}

    /// Add dynamic commands if any
    void add_dynamic_commands();

    // Attribute data members
    boost::json::value m_acq_params;

    std::unique_ptr<control_t> m_ctrl;

  private:
    static Tango::DevState to_tango_state(acq_state_enum s)
    {
        Tango::DevState res;

        switch (s) {
        case acq_state_enum::idle:
            res = Tango::ON;
            break;

        case acq_state_enum::prepared:
            res = Tango::INIT;
            break;

        case acq_state_enum::running:
        case acq_state_enum::stopped:
            res = Tango::RUNNING;
            break;

        case acq_state_enum::fault:
        case acq_state_enum::terminate:
            res = Tango::FAULT;
            break;

        default:
            res = Tango::DISABLE;
        }

        return res;
    }
};

// Additional Classes Definitions

} // namespace lima::tango

#include "control.inl"
