// Copyright (C) 2021 Samuel Debionne, ESRF.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <chrono>
#include <filesystem>
#include <memory>
#include <system_error>

#include <tango/tango.h>
#include <tango/server/dserversignal.h>

#include <boost/dll/import.hpp> // for import_alias
#include <boost/exception/diagnostic_information.hpp>
#include <boost/program_options.hpp>
#include <boost/stacktrace.hpp>
#include <boost/core/verbose_terminate_handler.hpp>

#if defined(LIMA_TANGO_RANK_SUFFIX)
#include <boost/mpi.hpp>
#endif

#include <lima/logging.hpp>
#include <lima/io/check.hpp>
#include <lima/utils/span.hpp>

// Check handler on terminate: uncaught exceptions, ...
void lima_tango_terminate_handler()
{
    try {
        std::cerr << boost::stacktrace::stacktrace();
    } catch (...) {
    }
    boost::core::verbose_terminate_handler();
}

namespace dll = boost::dll;
namespace po = boost::program_options;
namespace mpi = boost::mpi;

std::string plugin_folder;
std::vector<dll::shared_library> plugins;

int main(int argc, char* argv[])
{
    // install handler on terminate
    std::set_terminate(&lima_tango_terminate_handler);

    // Init logging
    lima::log::init();

    std::string instance;

    // Setup Tango signal handlers before MPI_Init
    Tango::DServerSignal::instance();

    boost::mpi::environment env(boost::mpi::threading::level::multiple);
    boost::mpi::communicator world;

    try {
        // Declare the supported options.
        po::options_description generic("Allowed options");
        // clang-format off
        generic.add_options()
            ("help", "Produce help message")
            ("debug", "Stop the server at the beginning to attach debugger")
            ("plugin-folder", po::value<std::string>(&plugin_folder)->default_value("."), "Plugin folder")
            ("log-level", po::value<lima::log::severity_level>()->default_value(lima::log::severity_level::warning), "Logging level [trace=0, debug, info, warning, error, fatal=5]")
            ("log-domain", po::value<std::string>()->default_value("all"), "Logging domain [core, ctl, acq,proc, io...]")
            ("log-file-path", po::value<std::filesystem::path>()->default_value(std::filesystem::temp_directory_path()), "Log file path [/tmp]")
            ("log-file-filename", po::value<std::filesystem::path>()->default_value("lima2_%N.log"), "Log file name [lima2_%N.log]")
            ;

        po::options_description positional("Positional options");
        positional.add_options()
            ("instance", po::value<std::string>(&instance)->required(), "Device server instance")
            ;
        
        po::positional_options_description p;
        p.add("instance", 1);
        // clang-format on

        po::options_description cmdline_options;
        cmdline_options.add(generic).add(positional);

        po::variables_map vm;

        // Parse command line
        po::parsed_options parsed =
            po::command_line_parser(argc, argv).options(cmdline_options).positional(p).allow_unregistered().run();
        po::store(parsed, vm);

        // Parse env variables
        const std::map<std::string, std::string> lookup_table = {{"LIMA2_PLUGIN_FOLDER", "plugin-folder"}};
        po::store(po::parse_environment(generic,
                                        // prefix name mapper (tolower and '_' to '-')
                                        [lookup_table](std::string const& name) {
                                            std::string res;
                                            auto it = lookup_table.find(name);
                                            if (it != lookup_table.end()) {
                                                res = it->second;
                                            }

                                            return res;
                                        }),
                  vm);

        // Init console logging
        lima::log::add_console_log();

        try {
            // Init file logging
            std::filesystem::path log_file_path = vm["log-file-path"].as<std::filesystem::path>();
            std::filesystem::path log_file_filename = vm["log-file-filename"].as<std::filesystem::path>();

            lima::io::check_base_path(log_file_path);

            lima::log::add_file_log(log_file_path, log_file_filename);
            std::cout << "Log file path set to " << log_file_path / log_file_filename << std::endl;
        } catch (lima::invalid_argument const& ex) {
            std::cerr << "WARNING: failed to init log file\n" << ex.what() << std::endl;
        }

        {
            auto log_level = vm["log-level"].as<lima::log::severity_level>();
            auto log_domain = lima::log::app_domain::from_string(vm["log-domain"].as<std::string>());

            std::cout << "Log level set to " << log_level << std::endl;

            // Turn off buffering
            if (log_level == lima::log::severity_level::trace)
                setvbuf(stdout, NULL, _IONBF, 0);

            // Set log filter according to the log level
            lima::log::set_filter(log_level, log_domain);
        }

        if (vm.count("help")) {
            std::cout << "Usage: " << argv[0] << " <INSTANCE> [options]\n";
            std::cout << generic << "\n";
            return 0;
        }

        vm.notify();

#if defined(LIMA_TANGO_RANK_SUFFIX)
        instance += "_" + std::to_string(world.rank());
#endif

        if (world.rank() == 0) {
            bool attach_debugger = vm.count("debug");

            // Broadcast attach_debugger
            boost::mpi::broadcast(world, attach_debugger, 0);

            if (attach_debugger) {
                std::cout << "Attach debugger then press <ENTER> key to continue..." << std::endl;
                std::cin.get();

                world.barrier();
            }
        } else {
            // Broadcast attach_debugger
            bool attach_debugger = false;
            boost::mpi::broadcast(world, attach_debugger, 0);

            if (attach_debugger)
                world.barrier();
        }

        std::cout << "Starting DS instance " << instance << std::endl;

        // When instantiating every Tango device server simultaneously, we run into
        // the following CORBA exception:
        // > BAD_PARAM CORBA system exception: BAD_PARAM_IndexOutOfRange
        //
        // As a workaround, stagger the initialization by adding a delay based on rank.
        auto delay = world.rank() * std::chrono::milliseconds(100);
        std::this_thread::sleep_for(delay);

        // Collect all the unrecognized options from the first pass. This will include the (positional) server name.
        std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
        std::vector<char*> argv_unrecognized;
        argv_unrecognized.reserve(opts.size());
        argv_unrecognized.push_back(argv[0]);
        argv_unrecognized.push_back(const_cast<char*>(instance.c_str()));
        for (auto&& opt : opts)
            argv_unrecognized.push_back(const_cast<char*>(opt.c_str()));

        // Initialise the device server
        Tango::Util* tg = Tango::Util::init(argv_unrecognized.size(), argv_unrecognized.data());

        // Set serialization model to none (scary)
        tg->set_serial_model(Tango::SerialModel::NO_SYNC);

        // Create the device server singleton which will create everything
        tg->server_init(false);

        // Run the endless loop
        tg->server_run();
    } catch (std::bad_alloc&) {
        std::cerr << "Can't allocate memory to store device object !!!" << std::endl;
        std::cerr << "Exiting" << std::endl;
    } catch (std::exception& e) {
        std::cerr << "Error: " << boost::diagnostic_information(e, true) << std::endl;
        std::cerr << "Exiting" << std::endl;
    } catch (CORBA::Exception& e) {
        std::cerr << "Received a CORBA_Exception:" << std::endl;
        Tango::Except::print_exception(e);

        std::cerr << "Exiting" << std::endl;
    }

    std::cout << "Cleanup... ";

    Tango::Util::instance()->server_cleanup();

    std::cout << "Good bye!" << std::endl;

    return 0;
}

/// Create Tango Class singletons and store it in DServer object.
void Tango::DServer::class_factory()
{
    using pluginapi_create_t = Tango::DeviceClass*();

    const auto extension = dll::shared_library::suffix().native();

    // Searching a folder for files with '.so' or '.dll' extension
    std::filesystem::recursive_directory_iterator endit;
    for (std::filesystem::recursive_directory_iterator it(plugin_folder); it != endit; ++it) {
        if (!std::filesystem::is_regular_file(*it) || std::filesystem::is_symlink(*it)) {
            continue;
        }

        // We found a file. Trying to load it
        std::error_code error;
        dll::shared_library lib(it->path(), error);
        if (error) {
            std::cout << "FAILED loading plugin: " << it->path() << " error: " << dlerror() << std::endl;
            continue;
        }

        if (!lib.has("create_class")) {
            // no such symbol
            std::cout << "FAILED loading plugin: " << it->path() << " error: missing create_class symbol" << std::endl;
            continue;
        }

        std::cout << "Loading plugin: " << it->path() << std::endl;

        // library has symbol, importing...
        auto create_class = dll::import_alias<pluginapi_create_t>(lib, "create_class");

        // Add classes
        Tango::DeviceClass* device_class = create_class();
        if (device_class) {
            std::cout << "Adding class: " << device_class->get_name() << std::endl;
            add_class(device_class);
        } else {
            std::cerr << "FAILED adding class" << std::endl;
        }

        // Keep the plugin library alive
        plugins.push_back(std::move(lib));
    }
}
