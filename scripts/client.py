#!/usr/bin/env python

# A high level telnet client to the control and receivers servers
import argparse
import json
import regex
import base64
import time

import numpy as np
import h5py as h5

from telnetlib import Telnet

class ReceiverTelnet(Telnet):
    def frame(self, uuid, frame_idx):
        self.write(f"frame {uuid.decode()} {frame_idx}\r\n".encode("ascii"))

        buffer = self.read_until(b"}")
        m = regex.search(rb"{((?>[^{}]+|(?R))*)}", buffer)
        while not m:
            buffer += self.read_until(b"}")
            m = regex.search(rb"{((?>[^{}]+|(?R))*)}", buffer)

        res = json.loads(m.group(0))
        data = base64.b64decode(res['data'])
        array = np.reshape(np.frombuffer(data, dtype = np.uint8), (res['width'], res['height']))
        return {
            "idx": res['idx'],
            "data": array
        }

    def counters(self, uuid):
        self.write(f"counters {uuid.decode()}\r\n".encode("ascii"))

        buffer = self.read_until(b"}")
        m = regex.search(rb"{((?>[^{}]+|(?R))*)}", buffer)
        while not m:
            buffer += self.read_until(b"}")
            m = regex.search(rb"{((?>[^{}]+|(?R))*)}", buffer)

        res = json.loads(m.group(0))
        return res

    def roi_counters(self, uuid, frame_idx):
        self.write(f"roi_counters {uuid.decode()} {frame_idx}\r\n".encode("ascii"))

        buffer = self.read_until(b"]")
        m = regex.search(rb"\[((?>[^\[\]]+|(?R))*)\]", buffer)
        while not m:
            buffer += self.read_until(b"]")
            m = regex.search(rb"\[((?>[^\[\]]+|(?R))*)\]", buffer)

        res = json.loads(m.group(0))
        return res


def main(hostname, ctrl_port, recv_port):
    with Telnet(hostname, ctrl_port) as ctrl_tn, ReceiverTelnet(hostname, recv_port) as recv_tn, h5.File('test.h5', 'w') as f:
        #ctrl_tn.set_debuglevel(999)
        #recv_tn.set_debuglevel(999)

        timeout = 300

        nb_frames = 10

        ctrl_tn.write(b"saving file_exists_policy overwrite\r\n")
        ctrl_tn.write(f"acquisition nb_frames {nb_frames}\r\n".encode("ascii"))
        ctrl_tn.write(b"roi_counters clear\r\n")
        ctrl_tn.write(b"roi_counters add \"32 32 64 64\"\r\n")
        ctrl_tn.write(b"prepare\r\n")
        
        ctrl_tn.expect([rb"preparing"], timeout=timeout)
        ctrl_tn.expect([rb"prepared"], timeout=timeout)
        (index, match, data) = ctrl_tn.expect([rb"UUID: (\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b)"], timeout=timeout)
        
        if (index == 0):
            uuid = match.group(1)

            dset = f.create_dataset("mydataset", (nb_frames, 128, 128), dtype='uint8')

            ctrl_tn.write(b"start\r\n")

            n = 0
            while n < nb_frames:
                counters = recv_tn.counters(uuid)
                print(f"counters: {counters}")

                # If some frame avail
                if counters['nb_frames_source'] > 0:
                    frm = recv_tn.frame(uuid, counters['nb_frames_source'] - 1)
                    print(f"frm #{frm['idx']}: {frm['data']}")
                    dset[frm['idx'],:,:] = frm['data']

                # If some roi counters avail
                if counters['nb_frames_counters'] > 0:
                    roi_counters = recv_tn.roi_counters(uuid, counters['nb_frames_counters'] - 1)
                    print(f"roi_counters: {roi_counters}")

                n = counters['nb_frames_source']

                time.sleep(0.5)
            
            #recv_tn.write(f"counters {uuid.decode()}\r\n".encode("ascii"))
            ##(index, match, data) = recv_tn.expect([rb"(?<=\"a\":
            ##)({(?>[^{}]|(?1))*})"], timeout=1)

            #buffer = recv_tn.read_until(b"}")
            #m = False;
            #while not m:
            #    buffer += recv_tn.read_lazy()
            #    m = regex.search(rb"{((?>[^{}]+|(?R))*)}", buffer)

            #counters = json.loads(m.group(0))

        ctrl_tn.write(b"stop\r\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Lima2 telnet client.')
    parser.add_argument('--hostname', default="localhost", help='the telnet server hostname')
    parser.add_argument('--ctrl-port', default=5001, help='the port of the controller')
    parser.add_argument('--recv-port', default=5002, help='the port of the receiver')

    args = parser.parse_args()

    # execute only if run as a script
    main(args.hostname, args.ctrl_port, args.recv_port)